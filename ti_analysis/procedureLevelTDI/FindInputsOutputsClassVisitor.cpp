#include "pass_manager/Plugin_pass.h"
#include "AST_visitor.h"


#include "process_ir/General.h"
#include <list>
#include <vector>

//#include "ResultStorage.cpp"
#include "QueryAnalyserVisitor.cpp"

using namespace AST;
using namespace std;


//GLOBALS (singleton, static, all not working, although compiling)
vector<Method_invocation*> inputMethodInvocations;
vector<Method_invocation*> outputMethodInvocations;
String tmp_CurrentMethodInvocation;

list<Method*> directOutputMethodCallers;
list<Method*> directInputMethodCallers;



template <class Visitor, class PHP_script, class Statement>
class FindInputsOutputsClassVisitor : public Visitor
{
private:

  bool inputMethodFound;
  bool outputMethodFound;
  bool computeCallGraph;

  bool queryFound; //invocation of query method

  list<String> inputMethodNames; 
  list<String> outputMethodNames;


  Method* currentMethod; // currently processing
  PHP_script* script;

public:

  void pre_php_script(PHP_script* in)
  {
    this->script = in;
    inputMethodFound = false;
    outputMethodFound = false;
    computeCallGraph = false;
    queryFound = false;
    

    this->defineInputMethodNames();
    this->defineOutputMethodNames();
   
    //    cout << "pre_php_script called" << endl;
  }

  void post_php_script(PHP_script* in) {
    //    cout << "post_php_script called" << endl;

  }



  /*
   * Define the methods which perform data input into the application.
   */
  void defineInputMethodNames() {
    this->inputMethodNames.push_back("select");
    this->inputMethodNames.push_back("load_relationship");


    //sugarcrm Account
    //this->inputMethodNames.push_back("query");
    this->inputMethodNames.push_back("get_linked_beans");
    this->inputMethodNames.push_back("get_list_view_data");
    this->inputMethodNames.push_back("fetchByAssoc");
    this->inputMethodNames.push_back("getForm");

    //SugarCRM ACL_modules_combined_e1.php
    this->inputMethodNames.push_back("get_linked_beans");
    this->inputMethodNames.push_back("getDefaultActions"); //select statement
    this->inputMethodNames.push_back("getUserActions");
    this->inputMethodNames.push_back("getUserRoles");
    this->inputMethodNames.push_back("process_page");

    //Campaign_modules_combined_e1
    this->inputMethodNames.push_back("get_list_view_data");
    this->inputMethodNames.push_back("retrieve_email_address");
    this->inputMethodNames.push_back("get_related_name");
    this->inputMethodNames.push_back("process_page");
    this->inputMethodNames.push_back("list_view_parse_additional_sections");
    this->inputMethodNames.push_back("create_export_query");
    this->inputMethodNames.push_back("track_log_leads");
    this->inputMethodNames.push_back("get_queue_items");
    this->inputMethodNames.push_back("create_list_count_query");
    this->inputMethodNames.push_back("getDeletedCampaignLogLeadsCount");
    this->inputMethodNames.push_back("campaign_response_by_activity_type");
    this->inputMethodNames.push_back("campaign_response_roi");
    this->inputMethodNames.push_back("campaign_response_chart");
    this->inputMethodNames.push_back("retrieveErrorReportAttachment");
    this->inputMethodNames.push_back("get_message_scope_dom");
    this->inputMethodNames.push_back("get_campaign_mailboxes");
    this->inputMethodNames.push_back("get_campaign_mailboxes_with_stored_options");
    this->inputMethodNames.push_back("get_campaign_urls");
    this->inputMethodNames.push_back("get_subscription_lists_query");
    this->inputMethodNames.push_back("subscribe");
    this->inputMethodNames.push_back("unsubscribe");
    this->inputMethodNames.push_back("campaign_log_mail_merge");
    this->inputMethodNames.push_back("display");
    this->inputMethodNames.push_back("processSearchForm");
    this->inputMethodNames.push_back("create_marketing_summary");
    this->inputMethodNames.push_back("validate_wiz_form");
    this->inputMethodNames.push_back("process_subscriptions_from_request");
    this->inputMethodNames.push_back("fill_in_additional_detail_fields");
    
    //Email_modules_combined
    //?    this->inputMethodNames.push_back("save");
    this->inputMethodNames.push_back("action_Save");
    this->inputMethodNames.push_back("create_new_list_query");
    this->inputMethodNames.push_back("create_queue_items_query");
    this->inputMethodNames.push_back("create_list_query");
    this->inputMethodNames.push_back("get_list_view_data");
    this->inputMethodNames.push_back("is_primary_email_address");
    this->inputMethodNames.push_back("create_export_query");
    this->inputMethodNames.push_back("retrieveEmailAddresses");
    this->inputMethodNames.push_back("retrieveEmailText");
    this->inputMethodNames.push_back("getNotes");
    this->inputMethodNames.push_back("getSystemDefaultEmail");
    this->inputMethodNames.push_back("create_new_list_query");
    this->inputMethodNames.push_back("fill_in_additional_detail_fields");
    this->inputMethodNames.push_back("fill_in_additional_list_fields");
    this->inputMethodNames.push_back("create_export_query");
    this->inputMethodNames.push_back("get_list_view_data");
    this->inputMethodNames.push_back("doesImportedEmailHaveAttachment");
    this->inputMethodNames.push_back("_genereateSearchImportedEmailsQuery");
    this->inputMethodNames.push_back("userSelectTable");
    this->inputMethodNames.push_back("fillPrimaryParentFields");
    this->inputMethodNames.push_back("cids2Links");
    this->inputMethodNames.push_back("getContacts");
    this->inputMethodNames.push_back("getUserContacts");
    this->inputMethodNames.push_back("getDraftAttachments");
    this->inputMethodNames.push_back("distLeastBusy");
    this->inputMethodNames.push_back("getAssignedEmailsCountForUsers");
    this->inputMethodNames.push_back("retrieve");
    this->inputMethodNames.push_back("displayComposeEmail");
    this->inputMethodNames.push_back("findEmailFromBeanIds");
    this->inputMethodNames.push_back("preflightUser");
    this->inputMethodNames.push_back("unserialize");
    this->inputMethodNames.push_back("fetchByAssoc");

    //Employee_module_e1
    this->inputMethodNames.push_back("action_editview");
    this->inputMethodNames.push_back("action_delete");
    this->inputMethodNames.push_back("retrieve_employee_id");
    this->inputMethodNames.push_back("create_export_query");
    this->inputMethodNames.push_back("getEmployeeStatusOptions");
    this->inputMethodNames.push_back("getMessengerTypeOptions");
    this->inputMethodNames.push_back("process_page");

  }

  /*
   * Define the methods which perform data output from the application.
   */
  void defineOutputMethodNames() {
    this->outputMethodNames.push_back("insert");
    this->outputMethodNames.push_back("update");
    this->outputMethodNames.push_back("delete");

    //sugarcrm Account
    this->outputMethodNames.push_back("save");
    this->outputMethodNames.push_back("getForm");
    //    this->outputMethodNames.push_back("query");
    this->outputMethodNames.push_back("create_export_query");
    this->outputMethodNames.push_back("handleSave");

    //ACL_modules_combined_e1
    this->outputMethodNames.push_back("mark_relationships_deleted");
    this->outputMethodNames.push_back("preDisplay");

    //Campaign_modules_combined
    this->outputMethodNames.push_back("clear_campaign_prospect_list_relationship");
    this->outputMethodNames.push_back("save");
    this->outputMethodNames.push_back("mark_deleted");
    this->outputMethodNames.push_back("deleteTestRecords");
    this->outputMethodNames.push_back("createBouncedCampaignLogEntry");
    this->outputMethodNames.push_back("og_campaign_activity");
    this->outputMethodNames.push_back("subscribe");
    this->outputMethodNames.push_back("unsubscribe");
    this->outputMethodNames.push_back("write_mail_merge_log_entry");
    this->outputMethodNames.push_back("track_campaign_prospects");
    this->outputMethodNames.push_back("create_campaign_log_entry");
    this->outputMethodNames.push_back("populate_wizard_bean_from_request");
    this->outputMethodNames.push_back("save");

    //Email_moduels_combined
    this->outputMethodNames.push_back("set_as_sent");
    this->outputMethodNames.push_back("sendEmail");
    this->outputMethodNames.push_back("mark_deleted");
    this->outputMethodNames.push_back("display");
    this->outputMethodNames.push_back("sendEmailTest");
    this->outputMethodNames.push_back("save");
    this->outputMethodNames.push_back("linkEmailToAddress");
    this->outputMethodNames.push_back("delete");
    this->outputMethodNames.push_back("setMailer");
    this->outputMethodNames.push_back("send");
    this->outputMethodNames.push_back("displayEmailFrame");
    this->outputMethodNames.push_back("displayQuickComposeEmailFrame");
    this->outputMethodNames.push_back("setContacts");
    this->outputMethodNames.push_back("removeContacts");
    this->outputMethodNames.push_back("saveNewFolder");
    this->outputMethodNames.push_back("serialize");
    this->outputMethodNames.push_back("deleteEmailCacheForFolders");
    this->outputMethodNames.push_back("emptyTrash");
    this->outputMethodNames.push_back("getImportForm");
    this->outputMethodNames.push_back("save");
    this->outputMethodNames.push_back("displayFolderContents");
    this->outputMethodNames.push_back("displayComposeEmail");
    this->outputMethodNames.push_back("displayComposeEmail");
    this->outputMethodNames.push_back("displaySuccessMessage");
    this->outputMethodNames.push_back("_writeCacheFile");
    this->outputMethodNames.push_back("debug");
    this->outputMethodNames.push_back("display");
    this->outputMethodNames.push_back("getForm");
    this->outputMethodNames.push_back("handleSave");

    //Employee_module_e1
    this->outputMethodNames.push_back("action_editview");
    this->outputMethodNames.push_back("action_delete");
    this->outputMethodNames.push_back("process_page");
    this->outputMethodNames.push_back("populateFromRow");
    this->outputMethodNames.push_back("display");
    this->outputMethodNames.push_back("listViewProcess");


  }

  void pre_method(Method* in) {
    currentMethod = in;

  }

  void post_method(Method* in) {
    if (outputMethodFound) {
      cout << "Direct Output Method Caller: " << *(in->signature->method_name->value);
      cout << "->" << tmp_CurrentMethodInvocation << endl;
      directOutputMethodCallers.push_back(in);
      tmp_CurrentMethodInvocation = "";
      computeCallGraph = true;
      outputMethodFound = false;
    }

    if (inputMethodFound) {
      cout << "Direct Input Method Caller: " << *(in->signature->method_name->value);
      cout << "->" << tmp_CurrentMethodInvocation << endl;
      directInputMethodCallers.push_back(in);
      tmp_CurrentMethodInvocation = "";
      inputMethodFound = false;
    }

  }


  void pre_method_invocation(Method_invocation* in)
  {
    
    //inputs
    list<String>::iterator itInput = inputMethodNames.begin();
    while (itInput != inputMethodNames.end()) {
	String currentName = *itInput;

	if(in->method_name->match(new METHOD_NAME(&currentName)))
	  {
	    //save invocation if not already there
	    if (std::find(inputMethodInvocations.begin(),
			  inputMethodInvocations.end(), in) 
		== inputMethodInvocations.end()) {
	      
	      inputMethodInvocations.push_back(in);
	      inputMethodFound = true;
	      tmp_CurrentMethodInvocation = currentName;
	      cout << "Input method invocation:" << currentName;
	      cout << " ~line:" << in->get_line_number() << endl;
	   
	    } else {
	      // cout << "not added" << endl;
	    }

  	  }
	itInput++;
    }
     
    //outputs
    list<String>::iterator itOutput = outputMethodNames.begin();
    while (itOutput != outputMethodNames.end()) {
	String currentName = *itOutput;

	if(in->method_name->match(new METHOD_NAME(&currentName)))
	  {


	    //save invocation if not already there
	    if (std::find(outputMethodInvocations.begin(),
			  outputMethodInvocations.end(), in) 
		== outputMethodInvocations.end()) {
	      
	      outputMethodInvocations.push_back(in);
	      outputMethodFound = true;
	      tmp_CurrentMethodInvocation = currentName;
	      cout << "Output method invocation:" << currentName;
	      cout << " ~line:" << in->get_line_number() << endl;

	    } else {
	      // cout << "not added" << endl;
	    }
	   

	  }
	itOutput++;
    }  


    //query: check parameters
    if(in->method_name->match(new METHOD_NAME("query"))) {
      cout << "***************************Query found!" << endl;
      queryFound = true;
    }
  }
  
  void pre_actual_parameter(Actual_parameter* in) {
    if(queryFound) {
      getQueryType(currentMethod, in);
    }
  }

  void post_method_invocation(Method_invocation* in) {
    if (queryFound) {
      queryFound = false;
     
    }
  }

  query_type getQueryType(Method* method, Actual_parameter* queryParameter) {

      Expr* expr = queryParameter->expr;
      
      Variable* var = dynamic_cast<Variable*>(expr);
      
      if(var) {
	Variable_name* varname = var->variable_name;
	VARIABLE_NAME* varnameC = dynamic_cast<VARIABLE_NAME*>(varname);
	cout << "** varname= " << *varnameC->value << endl;
    
	QueryAnalyserVisitor<AST::Visitor, 
	  AST::PHP_script, AST::Statement> *qav = 
	  new QueryAnalyserVisitor<AST::Visitor, AST::PHP_script, AST::Statement> (method, var);
	script->visit (qav);  

	query_type qtype = qav->getQueryType();
	String methodName = getMethodName(method);

	switch( qtype ) {
	case INPUT:
	  //this->inputMethodNames.push_back(methodName);
	  //directInputMethodCallers.push_back(method);
	  storeMethodWithoutDuplicates(method, &directInputMethodCallers);
	  cout << "Procedure '" << methodName << "' allows data INPUT"<< endl;
	  break;
	case OUTPUT:
	  //	this->outputMethodNames.push_back(methodName);
	  //directOutputMethodCallers.push_back(method);
	  storeMethodWithoutDuplicates(method, &directOutputMethodCallers);
	  cout << "Procedure '" << methodName << "' allows data OUTPUT"<< endl;
	  break;
	default:
	  cout << "Procedure " << methodName << " is of UNKNOWN type!"<< endl;
	}

      }//fi(var)


  }

  String getMethodName(Method* m) {
    return *(m->signature->method_name->value);
  }

  void storeMethodWithoutDuplicates(Method* m, list<Method*>* methodList) {
    
    list<Method*>::iterator it = std::find(methodList->begin(), methodList->end(), m);
    if (it == methodList->end()) {

      methodList->push_back(m);
    }
  }

};

  

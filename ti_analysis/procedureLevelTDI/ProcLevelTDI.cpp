#include "pass_manager/Plugin_pass.h"
#include "AST_visitor.h"

#include "process_ir/General.h"
#include <list>
#include <vector>
#include <map>



using namespace AST;
using namespace std;

#define TDEBUG 0



/*
 * Procedure level tenant data isolation analyser.
 * Author: Andrei Furda, 2015
 */

class ProcLevelTDI
{
private:

  AST::PHP_script* ast;



  //  list< AST::Statement > tdiProblemStatements;

public:
  
  int numberOfExpressions;

  ProcLevelTDI(AST::PHP_script* ast){

    
    this->ast = ast;
    
  }
    
  void findWarnings(String* methodName,
		    list<String> domainClasses) {

    ProcLevelTDI_WarningVisitor<AST::Visitor, AST::PHP_script, AST::Statement> *dcc = new ProcLevelTDI_WarningVisitor<AST::Visitor, AST::PHP_script, AST::Statement> (methodName, domainClasses);
    ast -> visit (dcc);  

    //this->numberOfExpressions = dcc->numberOfExpressions;
  }

  void findSuspiciousStatements(String* methodName,
				list<String> domainClasses,
				list<String> domainProperties){

    ProcLevelTDI_SuspiciousVisitor<AST::Visitor, AST::PHP_script, AST::Statement> *dcc = new ProcLevelTDI_SuspiciousVisitor<AST::Visitor, AST::PHP_script, AST::Statement> (methodName, domainClasses, domainProperties);
    ast -> visit (dcc);  
  }


  void findBreachStatements(String* methodName, 
			    list<String> domainClasses,
			    list<String> domainProperties,
			    list<String> tenantIDGetterMethods){
    
    ProcLevelTDI_BreachVisitor<AST::Visitor, AST::PHP_script, AST::Statement> *dcc = new ProcLevelTDI_BreachVisitor<AST::Visitor, AST::PHP_script, AST::Statement> (methodName, domainClasses, domainProperties, tenantIDGetterMethods);
    ast -> visit (dcc);  

    this->numberOfExpressions = dcc->numberOfExpressions;
  }



  String getMethodName(Method* m) {
    return *(m->signature->method_name->value);
  }

  String getMethodNames(list<Method*> methodList) {
    String result;

    list<Method*>::iterator it_methodList = methodList.begin();

    while(it_methodList != methodList.end()) {

      String name = getMethodName(*it_methodList);
      result += name;
      result += "; ";
      it_methodList++;
    }
    return result;
      
  }


};

#include "pass_manager/Plugin_pass.h"
#include "AST_visitor.h"


#include "process_ir/General.h"
#include <list>
#include <vector>
#include <map>

using namespace AST;
using namespace std;


//GLOBAL


/**
 * Finds Warnings:
 * - unspecified format parameter type
 * Author: Andrei Furda
 * 
 */

template <class Visitor, class PHP_script, class Statement>
class ProcLevelTDI_WarningVisitor : public Visitor
{
public:
  int numberOfExpressions;

private:
  PHP_script *script;
  bool methodFound;
  bool targetClassNameFound; //DomainObject
  int numberOfProblems;
  int numberOfWarnings;

  String* currentMethod; //currently processed method 

  list<Statement* > statements; //RESULT OUTPUT
  //String* mtTargetClassName;
  list<String> targetClassNames; //domain classes
  list<String> targetVarNames; //variables for tenant isolation

  bool addComment; //annotate code
  String *codeAnnotation; //added to source code as comment
  

public:
  
  ProcLevelTDI_WarningVisitor(String* methodName, 
				list<String> domainClassNames) {
    this->currentMethod = methodName;
    this->targetClassNames = domainClassNames;

  }


  void pre_php_script(PHP_script* in)
  {
    statements.clear();
    methodFound = false;
    
    //    cout << endl << "Warning Level (" << *(this->currentMethod) << ")" << endl;
    script = in;
    numberOfProblems = 0;
    numberOfWarnings = 0;
  }

  list<Statement*> getStatements() {
    return statements;
  }

  void post_php_script(PHP_script* in)
  {

  }



  void post_method(Method* in) {
    if (methodFound && numberOfWarnings > 0) {    
      cout << ">>Warnings: " << numberOfWarnings << "\n" << endl;
    }
    methodFound = false;
    
  }



  void pre_formal_parameter(Formal_parameter* in){
    if (methodFound) {
      //cout << " Formal parameter:" <<  endl;
      Type *type = in->type;
      bool is_ref = in->is_ref;
      Name_with_default* var = in->var;

      if (type == NULL) {
	cout << " Type NULL" << endl;
      } else {
	CLASS_NAME* class_name = type->class_name;
	if (class_name != NULL) {
	  //cout << " Type CLASS_NAME = " << *class_name->get_value_as_string() << endl;
	} else {
	  cout << "(WL) Procedure: " << *(this->currentMethod);
	  cout << " (~l:" << in->get_line_number() << ")" << endl;
	  
	  cout << " *WARNING: Parameter without type declaration: ";
	  cout << *var->variable_name->value << endl;
	  numberOfWarnings++;
	  
	  codeAnnotation = new String("//TODO (MT): specify type of proc. parameters");
	  addComment = true;
	}
      }
    }

  }

  void pre_method(Method* in) {

    if(in->signature->method_name->match(new METHOD_NAME(currentMethod))) {

      methodFound = true;
      
      /*      cout << "(WL) Procedure found: " << *(this->currentMethod);
      cout << " (~l:" << in->get_line_number() << ")" << endl;
      */

    }
  }
  
  void pre_expr(Expr* in) {
    numberOfExpressions++;
  }

  //code annotation
  void pre_statement(Statement* in)
  {
    if (addComment) {
      in->get_comments()->push_back(this->codeAnnotation);
    } 
    addComment = false;
  }

 



};

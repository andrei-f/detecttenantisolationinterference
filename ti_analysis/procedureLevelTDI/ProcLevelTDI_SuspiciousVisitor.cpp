#include "pass_manager/Plugin_pass.h"
#include "AST_visitor.h"


#include "process_ir/General.h"
#include <list>
#include <vector>
#include <map>

using namespace AST;
using namespace std;


//GLOBAL


/**
 * Searches for suspicous statements 
 * which might breach the data isolation property.
 * Author: Andrei Furda, 2015
 * 
 */

template <class Visitor, class PHP_script, class Statement>
class ProcLevelTDI_SuspiciousVisitor : public Visitor
{
public:
  int numberOfExpressions;

private:
  PHP_script *script;
  bool methodFound;
  bool targetClassNameFound; //DomainObject
  int numberOfProblems;

  String* currentMethod; //currently processed method 

  list<Statement* > statements; //RESULT OUTPUT
  //String* mtTargetClassName;
  list<String> targetClassNames; //domain classes
  list<String> targetVarNames; //variables for tenant isolation

  bool addComment; //annotate code
  String *codeAnnotation; //added to source code as comment
  

public:
  
  ProcLevelTDI_SuspiciousVisitor(String* methodName, 
				list<String> domainClassNames,
				list<String> domainPropertyNames) {
    this->currentMethod = methodName;
    this->targetClassNames = domainClassNames;
    this->targetVarNames = domainPropertyNames;

  }


  void pre_php_script(PHP_script* in)
  {
    statements.clear();
    methodFound = false;

    //    cout << endl << "Suspicious Statement Level (" << *(this->currentMethod) << ")" << endl;
    
    //    cout << endl << endl << "Analysing procedure: " 
    //	 << *(this->currentMethod) << endl;// << *(currentMethod->signature->method_name->value) << endl;
    script = in;
    numberOfProblems = 0;
  }

  list<Statement*> getStatements() {
    return statements;
  }

  void post_php_script(PHP_script* in)
  {

  }



  void post_method(Method* in) {
    if (methodFound && numberOfProblems > 0) {    
      cout << ">>Check code warnings: found "<< numberOfProblems << " potential problem(s). \n"<< endl;
    }
    methodFound = false;
    
  }

  void post_assignment(Assignment* in) {
    if (methodFound) {
      //cout << endl << "+Assignment found:";

      //bool is_ref = in->is_ref;
      //cout << " is_ref = " << is_ref << endl;

      Variable* var = in->variable;
      Variable_name* varname = var->variable_name;
      VARIABLE_NAME* varnameC = dynamic_cast<VARIABLE_NAME*>(varname);

      if (varnameC) {
	String* value = varnameC->value;
	//cout << " Varname: " << *(value) << endl;

	std::list<String>::iterator iter = std::find (targetVarNames.begin(), targetVarNames.end(), *value);
	if (iter != targetVarNames.end()) {

	  cout << "(Check code) Procedure: " << *(this->currentMethod);
	  cout << " (~l:" << in->get_line_number() << ")" << endl;
	  
	  cout << " *Assignment to property of potential multi-tenant object: ";
	  cout << *value;
	  cout << " (~l:" << in->get_line_number() << ")" << endl;
	  numberOfProblems++;

	  codeAnnotation = new String("//TODO (MT CHECK): Assignment to property of potential multi-tenant object!");
	  addComment = true;
	}
      }

    }
      
  }


  void pre_return(Return* in){
    if(methodFound) {
      
      Expr* expr = in->expr;
      
      Variable* var = dynamic_cast<Variable*>(expr);
      if (var) {
	//cout << " Return variable:";
	Variable_name* varname = var->variable_name;
	VARIABLE_NAME* varnameC = dynamic_cast<VARIABLE_NAME*>(varname);
	
	if (varnameC) {
	  String* varnameString = varnameC->value;
	  //cout << " Varname: " << *(varnameString);
	  //	  cout << " (~l:" << in->get_line_number() << ")" << endl;
	}
      }
    }

  }

  void pre_target(Target* in) {
    if (methodFound) {

      CLASS_NAME* classname = dynamic_cast<CLASS_NAME*>(in);

      if (classname) {
	String *value = classname->get_value_as_string();
	
	std::list<String>::iterator iter = std::find (targetClassNames.begin(), targetClassNames.end(), *value);
	if (iter != targetClassNames.end()) {
	  //  cout << " *Multi-Tenant object used. Class Name:";
	  //	  cout << *value << endl;
	}
      }

    }
  }

  void pre_formal_parameter(Formal_parameter* in){
    if (methodFound) {
      //cout << " Formal parameter:" <<  endl;
      Type *type = in->type;
      bool is_ref = in->is_ref;
      Name_with_default* var = in->var;

      if (type == NULL) {
	cout << " Type NULL" << endl;
      } else {
	CLASS_NAME* class_name = type->class_name;
	if (class_name != NULL) {
	  //cout << " Type CLASS_NAME = " << *class_name->get_value_as_string() << endl;
	} else {
	  //	  cout << " *WARNING: Parameter without CLASS_NAME: ";
	  //	  cout << *var->variable_name->value << endl;
	}
      }
    }

  }

  void pre_class_name(CLASS_NAME* in) {
    if (methodFound) {
      String *value = in->get_value_as_string();
      
      //	if (value->compare(*mtTargetClassName) == 0) {
      std::list<String>::iterator iter = std::find (targetClassNames.begin(), targetClassNames.end(), *value);
      if (iter != targetClassNames.end()) {

	cout << "(Check) Procedure: " << *(this->currentMethod);
	cout << " (~l:" << in->get_line_number() << ")" << endl;
	
	cout << " *Multi-Tenant classname used: ";
	cout << *value;
	cout << " (~l:" << in->get_line_number() << ")" << endl;
	numberOfProblems++;

	codeAnnotation = new String("//TODO (MT CHECK Code): ensure usage of tenantID");
	addComment = true;
      }
    }
  }

  void pre_new(New* in) {
    if (methodFound) {
      Class_name* class_name = in->class_name;
      CLASS_NAME* classname = dynamic_cast<CLASS_NAME*>(class_name);

      if (classname) {
	String *value = classname->get_value_as_string();

	
	std::list<String>::iterator iter = std::find (targetClassNames.begin(), targetClassNames.end(), *value);
	if (iter != targetClassNames.end()) {

	  cout << "(Check code) Procedure: " << *(this->currentMethod);
	  cout << " (~l:" << in->get_line_number() << ")" << endl;
	  
	  cout << " *Multi-Tenant object instantiated. Class Name: ";
	  cout << *value;
	  cout << " (~l:" << in->get_line_number() << ")" << endl;
	  numberOfProblems++;

	  codeAnnotation = new String("//TODO (MT): ensure usage of tenantID");
	  addComment = true;
	}
      }
    }
  }

  void pre_method(Method* in) {

    if(in->signature->method_name->match(new METHOD_NAME(currentMethod))) {

      methodFound = true;
      
      //      cout << "(SL) Procedure found. Name: " << *(this->currentMethod);
      //      cout << " (~l:" << in->get_line_number() << ")" << endl;

    }
  }
  
  void pre_expr(Expr* in) {
    numberOfExpressions++;
  }

  //code annotation
  void pre_statement(Statement* in)
  {
    if (addComment) {
      in->get_comments()->push_back(this->codeAnnotation);
    } 
    addComment = false;
  }

 



};

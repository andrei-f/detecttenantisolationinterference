#include "pass_manager/Plugin_pass.h"
#include "AST_visitor.h"

#include "process_ir/General.h"
#include "InlineMethodsTransform.cpp"

#define TDEBUG 0

using namespace AST;

template <class Visitor, class PHP_script, class Statement>
class InlineMethodsVisitor : public Visitor
{
private:
  Statement_list* statements;
  PHP_script* ast;

public:

  InlineMethodsVisitor(PHP_script* ast) {
    this->ast = ast;
  }

  PHP_script* getAST() {
    return ast;
  }

  void pre_method_invocation(Method_invocation* in) {
    METHOD_NAME* method_name = dynamic_cast<METHOD_NAME*>(in->method_name);
    if (TDEBUG) cout << method_name->value->c_str() << endl;
    InlineMethod inline_method;
    inline_method.setMethodName(method_name->value->c_str());
    ast->transform_children(&inline_method);
  }

};

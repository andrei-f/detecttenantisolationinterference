#include "AST_transform.h"

#include <pass_manager/Plugin_pass.h>

using namespace AST;

class ReturnTransform : public Transform {
private:
  Variable* var;

public:

  void setVar(Variable* var) {
    this->var = var;
  }

  void pre_return(Return* in, Statement_list* out) {
    // create a new assignment statement to replace the return statement
    Assignment* as = new Assignment(this->var, false, in->expr);
    // attempt to create the new statement
    Eval_expr* stmt = new Eval_expr(dynamic_cast<Expr*>(as));
    // if the new statement was created successfully, replace it, otherise leave the return untouched
    if (stmt) {
      out->push_back(stmt);
    } else {
      out->push_back(in);
    }
  }

};

#include "pass_manager/Plugin_pass.h"
#include "AST_visitor.h"

#include "process_ir/General.h"
#include <list>
#include <vector>
#include <map>



using namespace AST;
using namespace std;

#define TDEBUG 0

/**
 * Computes the call graph backwards.
 *
 * Author: Andrei Furda, 2015
 */

class CallGraphCalculator
{
private:

  AST::PHP_script* ast;

  list< map<Method*, list<Method* > > > callGraph;   //method, callers

public:


  CallGraphCalculator(AST::PHP_script* ast){
    this->ast = ast;
  }
  
  list< map<Method*, list<Method* > > > calculateCallGraph( list<Method*> methods) {

    list<Method*>::iterator itMethods = methods.begin();
    while(itMethods != methods.end()) {
      Method* m = *itMethods;
      calculateCallGraph(m);
      
      itMethods++;
    }
    return this->callGraph;
  }


  void calculateCallGraph(Method* m) {

    if (TDEBUG) cout << endl << "CalculateCallGraph( " << *(m->signature->method_name->value) << " )"<< endl;
    
    DirectCallersVisitor<AST::Visitor, AST::PHP_script, AST::Statement> *dcc = new DirectCallersVisitor<AST::Visitor, AST::PHP_script, AST::Statement> (m);
    ast -> visit (dcc);
    list<Method* > callers = dcc->getDirectCallers();
    
    if (TDEBUG) outputResultsDirectCallersVisitor(callers);

    if (callers.empty()) {
      storeResults(m, callers);
    } else {
    
      list<Method*>::iterator itCallers = callers.begin();
      while (itCallers != callers.end()) {
	Method* caller = *itCallers;

	//Recursion only if the caller has not been processed yet
	if (!methodInCallGraph(caller)) {
	  if (TDEBUG) cout << "before recursion" << endl;
	  storeResults(m, callers);
	  calculateCallGraph(caller); //recursion
	} else {
	  if (TDEBUG) cout << "caller: " <<  getMethodName(caller) << " processed already" << endl;
	  
	  storeResults(m, callers);
	}
	itCallers++;
      }
    }
  }

  bool methodInCallGraph(Method* m) {

    if (TDEBUG) cout << "\n methodInCallGraph searching for " << getMethodName(m) << endl;

    list< map<Method*, list<Method* > > >::iterator it_callGraph = callGraph.begin();
    while (it_callGraph != callGraph.end()) {
      map<Method*, list<Method*> > methodCallersMap = *it_callGraph;
      if (TDEBUG) outputDebugMap(methodCallersMap);
      map<Method*, list<Method*> >::iterator it = methodCallersMap.find(m);
      
      if (it != methodCallersMap.end()) {
	if (TDEBUG) {
	  cout << " found! returning true- MAP:";
	  outputDebugMap(methodCallersMap);
	  outputcallGraph();
	}

	return true;
      }
      it_callGraph++;
    }
    if (TDEBUG) {
      cout << "\n not found." << endl;
      outputcallGraph();
    }
    
    return false;
  }

  list<Method*>  getMethodCallers(Method* m) {
    
    list< map<Method*, list<Method* > > >::iterator it_callGraph = callGraph.begin();
    while (it_callGraph != callGraph.end()) {
      map<Method*, list<Method*> > methodCallersMap = *it_callGraph;
      if (TDEBUG) outputDebugMap(methodCallersMap);
      map<Method*, list<Method*> >::iterator it = methodCallersMap.find(m);
      
      if (it != methodCallersMap.end()) {
	if (TDEBUG) {
	  cout << " found! returning true- MAP:";
	  outputDebugMap(methodCallersMap);
	  if (TDEBUG) outputcallGraph();
	}
	return (*it).second;
      }
      it_callGraph++;
    }
    //    return;
  }

  void removeFromCallGraph(Method* method) {
    if (TDEBUG) cout << "removeFromCallGraph \n" << endl;
    list< map<Method*, list<Method* > > >::iterator it_callGraph = callGraph.begin();
    while (it_callGraph != callGraph.end()) {
      map<Method*, list<Method*> > methodCallersMap = *it_callGraph;
      map<Method*, list<Method*> >::iterator it = methodCallersMap.find(method);
      
      if (it != methodCallersMap.end()) {
	
	callGraph.remove( methodCallersMap );
	return;
      }
      it_callGraph++;
    }


  }
  
  void outputResultsDirectCallersVisitor( list<Method* > callers ) {
    cout << "  Direct callers: " << endl;
    list<Method* >::iterator it = callers.begin();

    while (it != callers.end() ) {
      Method* m = *it;
      cout << " +: " << *(m->signature->method_name->value) << endl;
      it++;
    }
  }

  void storeResults(Method* method, list<Method* > callers) {
    
    if (TDEBUG) cout << "STORE RESULTS" << endl;

    //case 0: callGraph is empty -> store new map
    //case 2: callGraph empty
    if (callGraph.empty()) {
      if (TDEBUG) cout << "call graph empty -> adding new path" << endl;
      
      map< Method* , list<Method* > > resultsMap;
      resultsMap.insert( pair<Method*, list<Method*> > (method, callers));
      if (TDEBUG) cout << "added new entry" << endl;
      callGraph.push_back(resultsMap); 

      return;
    } else {

      //case 1. check if the list contains an entry with the map key "Method"
      if (methodInCallGraph(method)) {
	list<Method*> storedCallers = getMethodCallers(method);
	
	//callers.merge(storedCallers);
	
	list<Method*> *mergedCallers = mergeUniqueMethodLists(callers, storedCallers);
	
	map< Method* , list<Method* > > resultsMap;
	resultsMap.insert( pair<Method*, list<Method*> > (method, callers));
	removeFromCallGraph(method);
	callGraph.push_back(resultsMap);

	if (TDEBUG) cout << "after: " << endl;
	if (TDEBUG) outputcallGraph();
	
	return;
      } else {
	//store the new pair
	map< Method* , list<Method* > > resultsMap;
	resultsMap.insert( pair<Method*, list<Method*> > (method, callers));
	if (TDEBUG) {
	  cout << "added new entry. key:"<< getMethodName(method) << endl;
	  cout << method << endl;
	}
	
	callGraph.push_back(resultsMap); 
	return;
      }
    }
  }

  /* Merges two lists, removing double entries.
   *
   */
  list<Method*>* mergeUniqueMethodLists(list<Method*> list1, list<Method*> list2) {
    list<Method*> *mergedList = new list<Method*> (list1);
    
    list<Method*>::iterator it_list2 = list2.begin();

    while (it_list2 != list2.end()) {
      Method* m = (*it_list2);
      
      list<Method*>::iterator it_merged = find(mergedList->begin(),
				     mergedList->end(),
				     m);
      if (it_merged == mergedList->end()) {
	mergedList->push_back(m);
      }
      
      it_list2++;
    }
    
    return mergedList;

  }


  void outputcallGraph() {
    cout << endl << "---- CALL GRAPH (intermediate): ----" << endl;
    list< map<Method*, list<Method* > > >::iterator it_callGraph = callGraph.begin();
    
    while (it_callGraph != callGraph.end()) {

      map<Method*, list<Method*> > methodCallers = *it_callGraph;

      map<Method*, list<Method*> >::iterator it = methodCallers.begin();
      Method* m = (*it).first;
      String methodName = *(m->signature->method_name->value);
      if (TDEBUG) cout << methodName << " : ";
      
      list<Method*> callers = (*it).second;
      list<Method*>::iterator itCallers = callers.begin();
      while(itCallers!= callers.end()){
	Method *c = (*itCallers);
	String name = *(c->signature->method_name->value);
	cout << name << ";";
	itCallers++;
      }
      it_callGraph++;
      cout << endl;
      
    }
    cout << endl << "----------------------" << endl;
  }
  
  void outputDebugMap(map< Method* , list<Method* > > methodCallers) {
    map< Method* , list<Method* > >::iterator it_methodCallers = methodCallers.begin();

    while(it_methodCallers != methodCallers.end()) {
      Method* m = it_methodCallers->first;

      cout << "\n DebugMap Callee:" << getMethodName(m) << ": ";

      cout << getMethodNames(it_methodCallers->second);


      it_methodCallers++;
    }
    

  }

  String getMethodName(Method* m) {
    return *(m->signature->method_name->value);
  }

  String getMethodNames(list<Method*> methodList) {
    String result;

    list<Method*>::iterator it_methodList = methodList.begin();

    while(it_methodList != methodList.end()) {

      String name = getMethodName(*it_methodList);
      result += name;
      result += "; ";
      it_methodList++;
    }
    return result;
      
  }


};

#include "pass_manager/Plugin_pass.h"
#include "AST_visitor.h"


#include "process_ir/General.h"
#include <list>
#include <vector>
#include <map>

#define TDEBUG 0

using namespace AST;
using namespace std;

/**
 * Analyses the forward data flow in a sequence of statements.
 * Author: Andrei Furda, 2015
 *
 */
template <class Visitor, class PHP_script, class Statement>
class DataFlowAnalyserVisitor : public Visitor {

private:

  Statement_list *statements;


  String sourceVar; // LHS variable in first statement e.g. tid = getTID();
  list<String> *dest_varnames; // dest. variables used in the last statement
  
  
  list< pair<String, String> > assignments;
  
  bool dataFlowExists_flag;

  bool stop_error; //flag indicating some error

public:

  /**
   * @return true if dataflow exists from the first to all variables
   * used in the last statement.
   */
  bool dataFlowExists() {
    return dataFlowExists_flag;
  }
  
  void pre_php_script(PHP_script* in)
  {
    if(TDEBUG) cout << " --- DataFlowAnalyser --- \n" << endl;
    this->statements = in->statements;
    
    
    //extract the lhs variable out of the first statement
    Statement_list::iterator it_s = this->statements->begin();    
    if (it_s != this->statements->end()) { 
      String* lhs_varname = getSourceVar(*it_s);
      if (!lhs_varname){
	cout << " error (PHC): cannot get lhs variable.\n" << endl;
	stop_error = true;
	return;
      }
      this->sourceVar = *lhs_varname;
      
      if (TDEBUG) outputAssignmentMap();
      if(TDEBUG) cout << "* Source variable (1st LHS VAR): " << *lhs_varname << endl;
    }
    
    //get the variables used in the last statement
    if(TDEBUG) cout <<"* Destination variables: ";
    Statement *last_s = this->statements->back();
    if (last_s != NULL) {//!= this->statements->end()) { 
      this->dest_varnames = getDestinationVars(last_s);
      
      list<String>::iterator it_dest_varname = dest_varnames->begin();
      while(it_dest_varname!= dest_varnames->end()) {
	
	//	cout << *((String*)(*it_dest_varname)) << "; ";
	if(TDEBUG) cout << *it_dest_varname << "; ";
	it_dest_varname++;
      }
      if(TDEBUG) cout << "\n" << endl;
    }
    
  }

  void post_php_script(PHP_script* in) {

    if (!stop_error) {
      outputAssignmentMap();

      determineFlowFromFirstToLastStatement();
    }

    if(TDEBUG) cout << " --- END DataFlowAnalyser --- \n" << endl;
  }

  void pre_assignment(Assignment* in) {
    //cout << "Assignment: ";
    
    Variable *var = in->variable;
    VARIABLE_NAME* variable_name = dynamic_cast<VARIABLE_NAME*>(var->variable_name);
    String* lhs_var_name_value = variable_name->value;
    
    //cout << *lhs_var_name_value << " <- ";

    Expr* expr = in->expr;

    Variable* rhs_var = dynamic_cast<Variable*> (expr);
    if (rhs_var) {
      VARIABLE_NAME* rhs_var_name = dynamic_cast<VARIABLE_NAME*>(rhs_var->variable_name);
      String *rhs_var_name_value = rhs_var_name->value;
      //cout << *rhs_var_name_value << "\n" << endl;
      //cout << "Assignment  ("<< *lhs_var_name_value << " <- " << *rhs_var_name_value << ")" <<endl;

      storeAssignment(*lhs_var_name_value, *rhs_var_name_value);
    } else {
      String ex = "EXPR";
      storeAssignment(*lhs_var_name_value, ex);
      //cout << " expression (not evaluated)\n" << endl;
    }
    
  }


  /**
   * @returns true if the first variable assignment reaches the last 
   * statement.
   * example for true: var1 = getTID();
   *                   BO(var1); 
   */
  void determineFlowFromFirstToLastStatement() {
    list<String> reachedVars; //variables reached from source variable
    bool flowExists = false;
    
    //add the first lhs (source to the list)
    list< pair<String, String> >::iterator it_assignments = assignments.begin();
    pair<String, String> lhs_rhs = (pair<String, String>) *it_assignments;
    reachedVars.push_back( lhs_rhs.first );
    it_assignments++;
    
    //continue with the second assignment
    while (it_assignments != assignments.end()) {
      lhs_rhs = (pair<String, String>) *it_assignments;
      String lhs = lhs_rhs.first;
      String rhs = lhs_rhs.second;
      
      
      if ( find(reachedVars.begin(), 
		reachedVars.end(), 
		rhs) != reachedVars.end() ) {
	//rhs found in reachedVars
	reachedVars.push_back(lhs);

      } else {
	//rhs not found -> remove lhs from list
	
	if ( find(reachedVars.begin(),
		  reachedVars.end(),
		  lhs) != reachedVars.end() ) {
	  reachedVars.remove(lhs);
	} else {
	  if(TDEBUG) cout << "   oh... a new lhs variable: "<< lhs << endl;
	}
      }
      outputList(reachedVars);
      
      it_assignments++;
    }

    //check if AT LEAST ONE destination variable is in reachedVars
    list<String>::iterator it_dest = dest_varnames->begin(); 
    while (it_dest != dest_varnames->end()) {
      String destVar = (String)(*it_dest);
      
      //try to find the destVar in reachedVars
      if (find(reachedVars.begin(),
	       reachedVars.end(),
	       destVar) != reachedVars.end() ) {
	if(TDEBUG) cout << "Destination variable reached: " << destVar << endl;
	flowExists = true;
      }
      it_dest++;
    }
      
    this->dataFlowExists_flag = flowExists;
  }

  void outputList(list<String> l) {
    if(TDEBUG) cout << "currently reached vars: ";

    list<String>::iterator it_l = l.begin();
    while( it_l != l.end()) {
      if(TDEBUG) cout << *it_l << "; ";
      it_l++;
    }
    if(TDEBUG) cout << endl;

  }


  /**
    casts the statement to and assignment and 
    extract the LHS variable from an assignment: a=b+c -> a
    @return variable name string or NULL
  */
  String* getSourceVar(Statement *s) {
    
    Eval_expr* eval = dynamic_cast<Eval_expr*> (s);
    if (eval) {
      
      Expr* expr = eval->expr;
      if (expr) {
	Assignment *assignment = dynamic_cast<Assignment*> (expr);

	if (assignment) {
	  Variable *lhs_var = assignment->variable;
	  VARIABLE_NAME* var_name = dynamic_cast<VARIABLE_NAME*>(lhs_var->variable_name);
	  if (var_name) {
	    String* lhs_var_name_value = var_name->value;
	    return lhs_var_name_value;
	  }
      
	}
      }
    }
    return NULL;
  }

  
  /**
   * @return a list of all variables used in the statement
   * If the statement is an assignment, it will contain all RHS variables.
   * If it is a procedure call or constructor, it will contain 
   * all actual parameters.
   */
  list<String>* getDestinationVars(Statement *s) {
    //cout << "getDestinationVars called\n" << endl;
    list<String> *results = new list<String>();

    Eval_expr* eval = dynamic_cast<Eval_expr*> (s);
    if (eval) {

      Expr* expr = eval->expr;
      if (expr) {
	
	Assignment *assignment = dynamic_cast<Assignment*> (expr);
	if (assignment) {
	  //cout << "assignment" << endl;

	  New *new_op = dynamic_cast<New*> (assignment->expr);
	
	  if (new_op) {
	    //cout << "New operator found.\n" << endl;
	    Actual_parameter_list* actual_parameters = new_op->actual_parameters;
	    list<String> *parNames = getParameterNames(actual_parameters);
	    
	    results->merge(*parNames);
	  }
	}
      }
    }

    return results;
  }

  list<String>* getParameterNames(Actual_parameter_list* actual_parameters) {
    //    cout << "getParamterNames\n";
    list<String> *parNames = new list<String>();

    

    list<Actual_parameter*>::iterator it_par = actual_parameters->begin();

    while (it_par != actual_parameters->end()) {
      Actual_parameter* param = (Actual_parameter*)*it_par;
      Expr* expr = param->expr;

      Variable* var = dynamic_cast<Variable*>(expr);
      if (var) {

	VARIABLE_NAME* var_name = dynamic_cast<VARIABLE_NAME*>(var->variable_name);
	if (var_name) {
	  String* var_name_value = var_name->value;
	  //cout << "   Act.ParName: " << *var_name_value << "\n" << endl;
	  
	  parNames->push_back(*var_name_value);
	}
      }
	
      it_par++;
    }
    
    return parNames;
    
  }


  /*
   * store an assignemnt of the form lhs=rhs
   */
  void storeAssignment(String lhs, String rhs) {
    
    assignments.push_back( pair<String, String > (lhs, rhs));

  }
    

  void outputAssignmentMap() {
    cout << "   Assignments (LHS <- RHS):" << endl;

    list< pair<String, String> >::iterator it_assignments = assignments.begin();

    while (it_assignments != assignments.end()) {
      String lhs = (*it_assignments).first;
      String rhs = (*it_assignments).second;
      
      cout << "    (" << lhs << " <- " << rhs << ")"  << endl;
      
      it_assignments++;
    }

    
    cout <<"     Source variable: " << sourceVar << endl;
    
    
    cout <<"     Destination variables: ";
    if (dest_varnames) {
      list<String>::iterator it_dest_varname = dest_varnames->begin();
      while(it_dest_varname!= dest_varnames->end()) {
	cout << *it_dest_varname << "; ";
	it_dest_varname++;
      }
      cout << "\n" << endl;
    } else {
      cout << "dest_varnames == NULL\n" << endl;
    }

  }



};

#include "pass_manager/Plugin_pass.h"
#include "AST_visitor.h"


#include "process_ir/General.h"
#include <list>
#include <vector>
#include <map>

using namespace AST;
using namespace std;


//GLOBAL


/**
 * Extracts names of public variables and methods of a given class.
 * Author: Andrei Furda
 * 
 */

template <class Visitor, class PHP_script, class Statement>
class FindClassPropertiesVisitor : public Visitor
{
public:


private:
  PHP_script *script;
  bool classFound; //flag marking the given class has been found


  String* currentClass; //currently processed class 

  list<String> publicAttributes; //output
  list<String> publicMethods; //output
  Member_list* members;

public:
  
  FindClassPropertiesVisitor (String* className) {
    this->currentClass = className;
  }



  void pre_php_script(PHP_script* in)
  {
    classFound = false;
    

    script = in;
  }


  void post_php_script(PHP_script* in)
  {

  }

  list<String> getPublicAttributes () {
    return this->publicAttributes;
  }
  
  list<String> getPublicMethods() {
    return this->publicMethods;
  }


  void pre_class_def(Class_def* in) {
    
    CLASS_NAME* class_name = in->class_name;
    String *value = class_name->value;

    if (value->compare(*currentClass) == 0) {
      this->classFound = true;
      cout << endl << endl << "ClassPropVisitor Analysing class: " 
	   << *(this->currentClass) << endl;
      cout << "Class found." << endl;

      this->members = in->members;
    }

  }

  void post_class_def(Class_def* in) {
    classFound = false;
  }

  void post_method(Method* in) {
    if (classFound) {
      // cout << " Method: " << *(in->signature->method_name->value) << endl;
      
      Signature* signature = in->signature;
      Method_mod* method_mod = signature->method_mod;
      if(method_mod->is_public) {
	
	METHOD_NAME* method_name = signature->method_name;
	String* value = method_name->value;
	cout << " Public method found: " << *value << endl;
	this->publicMethods.push_back(*value);
      }
    }
  }


  void pre_attribute(Attribute* in) {
    if (classFound) {
      Attr_mod* attr_mod = in->attr_mod;
      Name_with_default_list* vars = in->vars;

      Name_with_default_list::iterator vars_it = vars->begin();
      
      while(vars_it != vars->end()) {

	Name_with_default* var = *vars_it;
      
	if (attr_mod->is_public) {
	  VARIABLE_NAME* variable_name = var->variable_name;
	
	  String* value = variable_name->value;
	  //cout << " Public Attribute: " << *(value) << endl;
	  this->publicAttributes.push_back(*value);
	}

	vars_it++;
      }
      
    }
  }


};

#include "AST_transform.h"

#include <pass_manager/Plugin_pass.h>

#include "RenameVariablesVisitor.cpp"
#include "ReturnTransform.cpp"


#define TDEBUG 1

using namespace AST;

class InlineMethod : public Transform {

private:
  List<Formal_parameter*> *formal_parameters;
  Method* method;
  Statement_list* statements;
  string method_name;
  bool within_scope;
public:

  InlineMethod() {
    within_scope = false;
  }

  void setMethodName(string name) {
    this->method_name = name;
  }

  Statement_list* getStatements() {
    return statements;
  }

  /**
   * Takes a list of statements and renames the variables using a Visitor
   * @param  statements the list of statements containing the variables to rename
   * @return            a new Statement_list with variables renamed
   */
  Statement_list* renameVariables(Formal_parameter_list* formal_parameters, Statement_list* statements) {
    AST::PHP_script* ast = new AST::PHP_script(statements);
    RenameVariablesVisitor<AST::Visitor, AST::PHP_script, AST::Statement> *visitor;
    visitor = new RenameVariablesVisitor<AST::Visitor, AST::PHP_script, AST::Statement>(formal_parameters);
    ast->visit(visitor);
    Statement_list* new_statements = visitor->getStatements();
    return new_statements;
  }

  /**
   * Takes a list of statements which contains a return statements, and also inline the return statement as an Assignment
   * @param  statements the list of statements
   * @param  var        the variable to assign the value of the return statement
   * @return            a new Statement_list with variables renamed and return statement transformed
   */
  Statement_list* renameVariables(Formal_parameter_list* formal_parameters, Statement_list* statements, Variable* var) {
    Statement_list* new_statements = renameVariables(formal_parameters, statements);
    AST::PHP_script* ast = new AST::PHP_script(new_statements);
    ReturnTransform transform;
    transform.setVar(var);
    ast->transform_children(&transform);
    return ast->statements;
  }

  void pre_method(Method* in, Method_list* out) {
    if(in->signature->method_name->match(new METHOD_NAME(new String(this->method_name)))){
      // add the comments into the method statements
      this->statements = in->statements->clone();
      this->statements->front()->get_comments()->push_back(new String("// Start of inline code"));
      this->formal_parameters = in->signature->formal_parameters;
      within_scope = true;
    }
    out->push_back(in);
  }

  void pre_eval_expr(Eval_expr* in, List<Statement*>* out) {
    //if (TDEBUG) cout << "pre_eval_expr";
    
    // state to see if return statements need to be refactored
    bool is_assignment = false;
    // define the method invocation to inline
    Method_invocation* mi;
    // define the associated variable if it's in an assignment
    Variable* var;
    // check to see if the method invocation is on the RHS of an assignment
    Assignment* as = dynamic_cast<Assignment*>(in->expr);

    // get the method invocation either from the assignment or from the node
    if (as) {
      mi = dynamic_cast<Method_invocation*>(as->expr);
      var = as->variable;
      is_assignment = true;
    } else {
      mi = dynamic_cast<Method_invocation*>(in->expr);
    }

    if (mi) {
      // we found a method invocation!
      METHOD_NAME* m_name = dynamic_cast<METHOD_NAME*>(mi->method_name);
      if((m_name != NULL) && (m_name->value->compare(this->method_name) == 0)) {
        // we matched our method!

        if (!within_scope) {
	  if (TDEBUG) {
	    cout << "   Cannot inline " << *m_name->value;
	    cout << " (Invocation before def. or def. not found.)" << endl;
	  }
	  out->push_back(in);//AF
	  return; //AF: don't exit, continue anyway
        }

        // create iterators for the parameter lists
        List<Formal_parameter*>::iterator itf = this->formal_parameters->begin();
        List<Actual_parameter*>::iterator ita = mi->actual_parameters->begin();
        Statement_list* parameter_list = new Statement_list();

	//AF (TODO): this code needs to be rewritten!

        // iterate through the parameter lists
	//int tmp_s = this->formal_parameters->size(); //AF
	//if (TDEBUG) cout << "Formal paramsize: " << tmp_s << endl;
	
        for(int i = 0; i < this->formal_parameters->size(); i++) {
          // extract the actual parameters
          Formal_parameter *formal_param = (Formal_parameter*)(*itf);
          Actual_parameter *actual_param = (Actual_parameter*)(*ita);

	  //AF: quick fix to prevent crash
	  if(!formal_param || !actual_param){
	    ++itf;
	    ++ita;
	    cout << "x";
	    out->push_back(in);//AF
	    return; //AF: 
	  }

          // create an assignment statement using the parameter values
          Expr* e = actual_param->expr;

          // Variable_name* v_name = new VARIABLE_NAME(variable_name_value);
	  if (!formal_param || 
	      !formal_param->var) {
	    cout << "formal_param or formal_param->var == NULL";
	    cout << "x";
	    ++itf;
	    ++ita;
	    out->push_back(in);//AF
	    return; //AF: 
	  }
	  
          Variable* v = new Variable(formal_param->var->variable_name);
	  if (!v) {
	    ++itf;
	    ++ita;
	    cout << "x";
	    out->push_back(in);//AF
	    return; //AF: 
	  }
	  
          Assignment* a = new Assignment(v, false, e);
	  if (!a) {
	    ++itf;
	    ++ita;
	    cout << "x";
	    out->push_back(in);//AF
	    return; //AF: 
	  }
	  
          Eval_expr* ev = new Eval_expr(a);
	  if (!ev) { 
	    ++itf;
	    ++ita;
	    cout << "x";
	    out->push_back(in);//AF
	    return; //AF: 
	  }

          // push the parameter onto the tree
          parameter_list->push_back(ev);
          ++itf;
          ++ita;
	  // cout << ".";
        }
        // push all the statements from the method into the tree
        Statement_list* renamed_variables = this->statements->clone();
        renamed_variables->push_front_all(parameter_list);
        string start_comment = "// Start inline method " + *m_name->value;
        string end_comment   = "// End inline method " + *m_name->value;
        renamed_variables->front()->get_comments()->push_back(new String(start_comment));
        if (is_assignment == true) {
          renamed_variables = renameVariables(this->formal_parameters, renamed_variables, var);
        } else {
          renamed_variables = renameVariables(this->formal_parameters, renamed_variables);
        }
        // a 'Nop' is just an empty statement (I think)
        renamed_variables->push_back(new Nop());
        renamed_variables->back()->get_comments()->push_back(new String(end_comment));
        out->push_back_all(renamed_variables);
      } else {
        out->push_back(in);
      }
    } else {
      out->push_back(in);
    }
  }

};

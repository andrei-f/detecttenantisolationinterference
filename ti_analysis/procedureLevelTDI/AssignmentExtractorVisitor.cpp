#include "pass_manager/Plugin_pass.h"
#include "AST_visitor.h"


#include "process_ir/General.h"
#include <list>
#include <vector>
#include <map>

using namespace AST;
using namespace std;

String const_tenantIdentifier = "getTenantID"; //method name to identify the tenant (getTenantID)


/**
 * Finds assignment statements.
 *
 * Author: Andrei Furda, 2015
 */

template <class Visitor, class PHP_script, class Statement>
class AssignmentExtractorVisitor : public Visitor
{

private:
  PHP_script *script;
  bool methodFound;
  Method* targetMethod;
  bool methodInvocationFound;

  list<Assignment*> assignmentTDEP; //tenant-dependent variable assignments
  list<Assignment*> assignmentTUNDECIDED; //unknown state, temp. only
 
public:
  
  
  AssignmentExtractorVisitor(Method* method) {
    this->targetMethod = method;

  }

  void pre_php_script(PHP_script* in)
  {
    methodFound = false;
    //cout << endl << endl << "AssignmentExtractor, search for: " << *(currentMethod->signature->method_name->value) << endl;
    script = in;
    
  }

  void post_php_script(PHP_script* in)
  {
    
  }


  void post_method(Method* in) {

    methodFound = false; //??
    methodInvocationFound = false;
  }

  void pre_method(Method* in) {
    
    String targetMethodName = *(targetMethod->signature->method_name->value);
    if(in->signature->method_name->match(new METHOD_NAME(&targetMethodName))) {
      //cout << "method :" << targetMethodName << endl;
      
      methodFound = true;

    }
  }

  void post_assignment(Assignment* in) {

    //    cout << "pre_assignment" << endl;
    
    if (methodFound && methodInvocationFound) {
      //  cout << "pre_assignment found" << endl;
      Variable* var = in->variable;
      VARIABLE_NAME* var_name = dynamic_cast<VARIABLE_NAME*> (var->variable_name);
      
      //VARIABLE_NAME* tenantIDVar = new VARIABLE_NAME("tenantID");
      //  cout << "tenantid: " << *(tenantIDVar->value) << endl;
      

      if (var_name) 
	{
	  String *var_name_string = var_name->value;
	  //cout << "  +TDEP var: " << *(var_name_string) << endl;
	  assignmentTDEP.push_back(in);
	}
      
      //opassignmentList.push_back(in);
    } else if (methodFound)    //all other assignments in the method become TUNDECIDED
      {
	Variable* var_tindep = in->variable;
	VARIABLE_NAME* var_name_tindep = dynamic_cast<VARIABLE_NAME*> (var_tindep->variable_name);
	String *var_name_string_tindep = var_name_tindep->value;
	//cout << "  t-undecided. var: " << *(var_name_string_tindep) << endl;
	assignmentTUNDECIDED.push_back(in);
      }
    
    methodInvocationFound = false;
  }

  void post_method_invocation(Method_invocation* in)
  {
    if(in->method_name->match(new METHOD_NAME(&const_tenantIdentifier))) 
      {
	methodInvocationFound = true;
	//	cout << "getTenantID invocation found" << endl;
      }
  }
  
    
  /**
   * Returns the assignments which generate tenant-dependent variables (i.e. those variables whic depend directly on the tenant ID).
   */
  list<Assignment*> getTDEPAssignments() {
    return assignmentTDEP;
  }
  
  /**
   * Returns the list of assignments which are not depending on the tenant ID directly. Further computation is needed to assess whether they depend on the tenant ID indirecly, by modifying the tenant ID in some way.
   */
  list<Assignment*> getTUNDECIDEDAssignments() {
    return assignmentTUNDECIDED;
  }

};

#include "pass_manager/Plugin_pass.h"
#include "AST_visitor.h"

#include "process_ir/General.h"

using namespace AST;
using namespace std;

template <class Visitor, class PHP_script, class Statement>
class RenameVariablesVisitor : public Visitor
{
private:
  PHP_script* ast;
  string append;
  Formal_parameter_list* formal_parameters;
public:

  // two constructors because one might not care if the default value
  // appended to variables is `_inline_variable`
  RenameVariablesVisitor(Formal_parameter_list* formal_parameters, string append) {
    this->formal_parameters = formal_parameters;
    this->append = append;
  }

  RenameVariablesVisitor(Formal_parameter_list* formal_parameters) {
    this->formal_parameters = formal_parameters;
    this->append = "_inline_variable";
  }

  Statement_list* getStatements() {
    return ast->statements;
  }

  void setVariableNameAppend(string append) {
    this->append = append;
  }

  /**
   * Renames a variable by appending a value to the end of it
   * @param  var the variable pointer to rename
   * @return     a new variable with it's name renamed
   */
  Variable_name* renameVariable(Variable* var) {
    List<Formal_parameter*>::iterator itf = this->formal_parameters->begin();
    bool change_name = true;
    VARIABLE_NAME* variable_name = dynamic_cast<VARIABLE_NAME*>(var->variable_name);

    // determine if the variable name is contained in the list of formal parameters
    for(int i = 0; i < this->formal_parameters->size(); i++) {
      // extract the actual parameters
      Formal_parameter *formal_param = (Formal_parameter*)(*itf);

      VARIABLE_NAME* formal_variable_name = dynamic_cast<VARIABLE_NAME*>(formal_param->var->variable_name);

      if (formal_variable_name->equals(variable_name) && formal_param->is_ref) {
        change_name = false;
      }

      ++itf;
    }

    // if the variable isn't a formal parameter, append whatever the value this->append is
    if (change_name && variable_name) { //AF: added check variable_name

      String* variable_name_value = variable_name->value;
      variable_name_value->append(this->append);
    }

    // return the modified (or unmodified) variable
    return (Variable_name*) variable_name; //AF: this is wrong! Can't statically cast to a different type.
  }

  void pre_variable(Variable* in) {
    in->variable_name = renameVariable(in);
  }

  void post_php_script(PHP_script* in) {
    ast = in;
  }

};

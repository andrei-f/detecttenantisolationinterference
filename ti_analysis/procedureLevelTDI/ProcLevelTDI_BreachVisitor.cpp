#include "pass_manager/Plugin_pass.h"
#include "AST_visitor.h"
#include "process_ir/General.h"
#include <list>
#include <vector>
#include <map>

#include "DataFlowAnalyserVisitor.cpp"

using namespace AST;
using namespace std;

/*activates inlining before analysis 
  (only of analysed procedures */
#define INLINE_PROCLEVEL 0

#define ACTIVATE_DFANALYSIS 1 //data-flow analysis

#define TDEBUG 1

/**
 * Finds tenant data isolation breaches.
 * Author: Andrei Furda, 2015
 * 
 */

template <class Visitor, class PHP_script, class Statement>
class ProcLevelTDI_BreachVisitor : public Visitor
{
public:
  int numberOfExpressions;

private:
  PHP_script *script;
  bool methodFound;
  bool targetClassNameFound; //DomainObject
  int numberOfProblems;
  int numberOfWarnings;

  String* currentMethod; //currently processed method 

  list<Statement* > statements; //RESULT OUTPUT
  
  list<String> targetClassNames; //domain classes
  list<String> targetVarNames; //variables for tenant isolation
  list<String> tenantIDGetterMethods;
  bool tenantIDGetterInvoked; //MTp statement
  bool inConditionalExpr; //indicates processing within a conditional expression

  list<Statement* > *statementsToAnalyseDF; //analyse data-flow
  bool addForDataFlowAnalysis; //flag to add statements to the analysis list
  bool stopAddingForDataFlowAnalysis;

  bool addComment; //annotate code
  String *codeAnnotation; //added to source code as comment

public:

  /**
   * @param methodName - the method to analyze
   * @param domainClassNames - the business objects (class names)
   * @param domainPropertyNames - attributes (properties) of business obj.
   */
  ProcLevelTDI_BreachVisitor(String* methodName, 
			     list<String> domainClassNames,
			     list<String> domainPropertyNames,
			     list<String> tenantIDGetterMethods) {
    this->currentMethod = methodName;
    this->targetClassNames = domainClassNames;
    this->targetVarNames = domainPropertyNames;
    this->tenantIDGetterMethods = tenantIDGetterMethods;

    //cout << "Constructor" << endl;
    this->statementsToAnalyseDF = new list<Statement*>();
  }


  void pre_php_script(PHP_script* in)
  {
    statements.clear();
    methodFound = false;
    
    //    cout << endl << "Isolation Breach Level (" << *(this->currentMethod) << ")" << endl;

    script = in;
    numberOfProblems = 0;
  }

  list<Statement*> getStatements() {
    return statements;
  }

  void post_php_script(PHP_script* in)
  {

  }



  void post_method(Method* in) {
    if (methodFound && numberOfProblems > 0) {    
      cout << ">>Extend code warnings: found "<< numberOfProblems << " potential problem(s). \n"<< endl;
    }
    methodFound = false;
    tenantIDGetterInvoked = false;
    
    addForDataFlowAnalysis=false;
    statementsToAnalyseDF->clear();
  }

  void post_assignment(Assignment* in) {
    if (methodFound) {
      //cout << endl << "+Assignment found:";

      //bool is_ref = in->is_ref;
      //cout << " is_ref = " << is_ref << endl;

      Variable* var = in->variable;
      Variable_name* varname = var->variable_name;
      VARIABLE_NAME* varnameC = dynamic_cast<VARIABLE_NAME*>(varname);

      if (varnameC) {
	String* value = varnameC->value;
	//cout << " Varname: " << *(value) << endl;

	std::list<String>::iterator iter = std::find (targetVarNames.begin(), targetVarNames.end(), *value);
	if (iter != targetVarNames.end()) {
	  //	  cout << " *Suspicious assignment to multi-tenant ID variable: ";
	  //	  cout << *value;
	  //	  cout << " (~l:" << in->get_line_number() << ")" << endl;
	  //	  numberOfProblems++;
	}
      }

    }
      
  }


  void pre_return(Return* in){
    if(methodFound) {
      
      Expr* expr = in->expr;
      
      Variable* var = dynamic_cast<Variable*>(expr);
      if (var) {
	//cout << " Return variable:";
	Variable_name* varname = var->variable_name;
	VARIABLE_NAME* varnameC = dynamic_cast<VARIABLE_NAME*>(varname);
	
	if (varnameC) {
	  String* varnameString = varnameC->value;
	  //cout << " Varname: " << *(varnameString);
	  //	  cout << " (~l:" << in->get_line_number() << ")" << endl;
	}
      }
    }
  }

  void pre_target(Target* in) {
    if (methodFound) {

      CLASS_NAME* classname = dynamic_cast<CLASS_NAME*>(in);

      if (classname) {
	String *value = classname->get_value_as_string();
	
	std::list<String>::iterator iter = std::find (targetClassNames.begin(), targetClassNames.end(), *value);
	if (iter != targetClassNames.end()) {
	  //  cout << " *Multi-Tenant object used. Class Name:";
	  //	  cout << *value << endl;
	}
      }

    }
  }

//   void pre_class_name(CLASS_NAME* in) {
//     if (methodFound) {
//       String *value = in->get_value_as_string();
      
//       //	if (value->compare(*mtTargetClassName) == 0) {
//       std::list<String>::iterator iter = std::find (targetClassNames.begin(), targetClassNames.end(), *value);
//       if (iter != targetClassNames.end()) {

// 	cout << " *Multi-Tenant classname used. Class Name ";
// 	cout << *value;
// 	cout << " (~l:" << in->get_line_number() << ")" << endl;
// 	numberOfProblems++;

//       }
//     }
//   }

  void pre_new(New* in) {
    if (methodFound) {
      Class_name* class_name = in->class_name;
      CLASS_NAME* classname = dynamic_cast<CLASS_NAME*>(class_name);

      if (classname) {
	String *value = classname->get_value_as_string();

	
	std::list<String>::iterator iter = std::find (targetClassNames.begin(), targetClassNames.end(), *value);
	if (iter != targetClassNames.end()) {

	  if (!tenantIDGetterInvoked) {
	    	    
	    cout << "(EXTEND LEVEL) Procedure: " << *(this->currentMethod);
	    cout << " (~l:" << in->get_line_number() << ")" << endl;
	    
	    cout << " *Multi-Tenant object instantiated WITHOUT prior invocation of tenantID getter. Class Name: ";
	    cout << *value;
	    cout << " (~l:" << in->get_line_number() << ")" << endl;
	    numberOfProblems++;

	    codeAnnotation = new String("//TODO (MT EXTEND Code): retrieve and use the TENANT_ID");
	    addComment = true;

	  

	  }
	}
      }
    }
  }

  void post_new(New* in) {
    if (addForDataFlowAnalysis) {
      //      addForDataFlowAnalysis = false; //stop saving statements
      stopAddingForDataFlowAnalysis = true; //stop AFTER this statement
      // cout << "POST_NEW" << endl;
      //analyseDFForward();
    }
  }

  void pre_method(Method* in) {

    if(in->signature->method_name->match(new METHOD_NAME(currentMethod))) {

      methodFound = true;
      
      //      cout << "(BL) Procedure found. Name: " << *(this->currentMethod);
      //      cout << " (~l:" << in->get_line_number() << ")" << endl;

    }
  }

  //ignore tenantIDGetterMethods inside conditional statements
  void pre_if_chain(If* in) { 
    inConditionalExpr = true; 
  }
  
  void post_if_chain(If* in) {
    inConditionalExpr = false;
  }

  void pre_while_chain(While* in) {     
    inConditionalExpr = true;
  }

  void post_while_chain(While* in){
    inConditionalExpr = true;
  }

  void pre_for_chain(For* in) {
    inConditionalExpr = true;
  }

  void post_for_chain(For* in) {
    inConditionalExpr = false;
  }

  void pre_conditional_expr(Conditional_expr* in){
    inConditionalExpr = true;
  }
  void post_conditional_expr(Conditional_expr* in) {
    inConditionalExpr = false;
  }

  void pre_switch_case_chain(Switch_case* in){
    inConditionalExpr = true;
  }
  void post_switch_case_chain(Switch_case* in){
    inConditionalExpr = false;
  }




  void pre_method_invocation(Method_invocation* in) {
    if (methodFound) {


      //------------ Inlining of procedure invocations -------
//       cout << "Procedure inlining: ";
//       if (INLINE_PROCLEVEL) cout << "enabled" << endl;
//       else {cout << "disabled" << endl;}
      
      if (INLINE_PROCLEVEL) {
	METHOD_NAME* method_name = dynamic_cast<METHOD_NAME*>(in->method_name);
	if (TDEBUG) cout << " Inlining invocations of " << method_name->value->c_str() << endl;
	InlineMethod inline_method;
	inline_method.setMethodName(method_name->value->c_str());
	this->script->transform_children(&inline_method);
      }
      //---------------- End inlining
      

      list<String>::iterator tenantIDGetterMethods_it = tenantIDGetterMethods.begin();
      while (tenantIDGetterMethods_it != tenantIDGetterMethods.end()) {
	String tidGetter = *tenantIDGetterMethods_it;
	
	//not in conditional expr, method name matches
	if((!inConditionalExpr ) && 
	   (in->method_name->match(new METHOD_NAME(&tidGetter))))
	  {
	    tenantIDGetterInvoked = true;
	    // cout << " method invocation:" << tidGetter;
	    //cout << " ~line:" << in->get_line_number() << endl;
	    addForDataFlowAnalysis = true; //start saving statements
	    
	  } else if ( in->method_name->match(new METHOD_NAME(&tidGetter))) {
	  //in conditional scope

	  addForDataFlowAnalysis = true; //start saving statements
	  cout << " TID getter invocation in conditional statement:" << tidGetter;
	  cout << " ~line:" << in->get_line_number() << endl;
	}
	tenantIDGetterMethods_it++;
      }
	
    }

  }
  
  void pre_expr(Expr* in) {
    numberOfExpressions++;
  }
 
  //code annotation
  void pre_statement(Statement* in)
  {
    if (addComment) {
      in->get_comments()->push_back(this->codeAnnotation);
    } 
    addComment = false;
  }

  void post_statement(Statement* in) {
  //for data-flow
    if(addForDataFlowAnalysis) {
      //cout << "adding statement DF" << endl;
      this->statementsToAnalyseDF->push_back(in);
    }
    if (stopAddingForDataFlowAnalysis) {
      addForDataFlowAnalysis = false;
      stopAddingForDataFlowAnalysis = false;

      cout << "DATA-FLOW analysis: ";
      if (ACTIVATE_DFANALYSIS) cout << "enabled" << endl;
      else {cout << "disabled" << endl;}

      if(ACTIVATE_DFANALYSIS) {
	analyseDFForward();
      }
    }
  }


  void analyseDFForward() {

    cout << " ** Both MT_p and BO_p statements found.\n" << endl;
    //cout << " Data-Flow analysis required. Statements to analyse:" << this->statementsToAnalyseDF->size()<<" \n" << endl;
    cout << " *Data-Flow Analysis: " << endl;
    Statement_list::iterator it_s = statementsToAnalyseDF->begin();    
    Statement_list slist;
    


    while(it_s != statementsToAnalyseDF->end()) { 
      Statement* s = (Statement*)(*it_s);
      slist.push_back(s);
      //cout << " ~l:" << s->get_line_number() << endl;
      it_s++;
    }


    PHP_script *statement_script = new PHP_script(&slist);

    DataFlowAnalyserVisitor<AST::Visitor, AST::PHP_script, AST::Statement> *dfa = new DataFlowAnalyserVisitor<AST::Visitor, AST::PHP_script, AST::Statement>();

    statement_script->visit(dfa);

    bool dataFlowExists = dfa->dataFlowExists();

    if (!dataFlowExists) {
      cout << "   *Data-Flow WARNING: variable assignment from preceding MT-procedure invocation does not reach the BO constructor! " << endl;

      codeAnnotation = new String("//TODO (MT EXTEND Code): Data-Flow does not occur between preceding MT-procedure invocation and BO instantiation!");
	    addComment = true;
    } else {
      cout << "  *Data-Flow analysis OK. " << endl;
    }
    

  }

  

  
	
};

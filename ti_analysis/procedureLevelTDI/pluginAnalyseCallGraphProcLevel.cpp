/*
 * phc -- the open source PHP compiler
 * See doc/license/README.license for licensing information
 *
 */

/**
 * Plugin calculates the Call Graph,
 * performs procedure level tenant isolation analysis.
 * Author: Andrei Furda, 2015
 */


#include "pass_manager/Plugin_pass.h"
#include "AST_visitor.h"

#include "process_ir/General.h"

#include "InlineMethodsVisitor.cpp"

#include "FindInputsOutputsClassVisitor.cpp"
#include "BuildCallGraphVisitor.cpp"
#include "DirectCallersVisitor.cpp"
#include "CallGraphCalculator.cpp"

#include "FindClassPropertiesVisitor.cpp"
#include "ProcLevelTDI_WarningVisitor.cpp"
#include "ProcLevelTDI_SuspiciousVisitor.cpp"
#include "ProcLevelTDI_BreachVisitor.cpp"

#include "ProcLevelTDI.cpp"


#define INLINEALL 0 //activate inlining of procedure invocations IN ENTIRE PROGRAM before the analysis
#define INLINEALL_DEPTH 0 //defines how deep the inlining goes



using namespace AST;
using namespace std;

void outputResultsFindInputsOutputs() {
  
  cout << endl << "------ STAGE 1: Input/Output Methods -------------" << endl;
  list<Method*>::iterator itM = directOutputMethodCallers.begin();
  cout << "Direct Output Method Callers:" << endl;
  while (itM != directOutputMethodCallers.end()) {
    cout << *((*itM)->signature->method_name->value) << endl;;
    itM++;
  }
  cout << endl;

  itM = directInputMethodCallers.begin();
  cout << "Direct Input Method Callers:" << endl;
  while (itM != directInputMethodCallers.end()) {
    cout << *((*itM)->signature->method_name->value) << endl;;
    itM++;
  }
  cout << endl;

}



void outputResultsBuildCallGraph() {

  //print cgPath map
  cout << endl << "------ STAGE 3: Call Graph  -------------" << endl;

  list< list<Method* > >::iterator itPath = cgPath.begin();

  while (itPath != cgPath.end()) {
    
    cout << endl << "Path: " << endl;
    list<Method*> callerList = *itPath;
    list<Method*>::iterator itCallerList = callerList.begin();

    while (itCallerList != callerList.end() ) {
      Method* method = *itCallerList;
      
      cout << *(method->signature->method_name->value) <<  " <- "; //method
//       cout << ": {";
//       list<Method_invocation*>::iterator it_invocations = invocations.begin();
//       while (it_invocations != invocations.end()) {
// 	Method_invocation* tmpInvoc = *it_invocations;
// 	cout << tmpInvoc->get_line_number() << ";";
// 	it_invocations++;
//       }
//       cout <<"}" << endl;
      itCallerList++;
    }
    
    itPath++;
 
  }
  
}

void outputcallGraph( list< map<Method*, list<Method* > > > callGraph ) {
  cout << endl << "---- CALL GRAPH ----"<< endl;
  cout << "(called procedure: <callers>*;)" << endl;
  list< map<Method*, list<Method* > > >::iterator it_callGraph = callGraph.begin();
    
  while (it_callGraph != callGraph.end()) {

    //      cout << endl << "Call Graph:" << endl;
    map<Method*, list<Method*> > methodCallers = *it_callGraph;

    map<Method*, list<Method*> >::iterator it = methodCallers.begin();
    Method* m = (*it).first;
    String methodName = *(m->signature->method_name->value);
    cout << methodName << " : ";
      
    list<Method*> callers = (*it).second;
    list<Method*>::iterator itCallers = callers.begin();
    while(itCallers!= callers.end()){
      Method *c = (*itCallers);
      String name = *(c->signature->method_name->value);
      cout << name << ";";
      itCallers++;
    }
    it_callGraph++;
    cout << endl;
      
  }
  //  cout << endl << "----------------------" << endl;
}




extern "C" void load (Pass_manager* pm, Plugin_pass* pass)
{
 
  // pm->add_ast_pass (pass);
  pm->add_after_named_pass (pass, new String("ast"));

}

extern "C" void run_ast (AST::PHP_script* in, Pass_manager* pm, String* option)
{

  //----- begin inline procedures
  if(INLINEALL) {
    int depth = INLINEALL_DEPTH;
    InlineMethodsVisitor<AST::Visitor, AST::PHP_script, AST::Statement> *visitor;
    visitor = new InlineMethodsVisitor<AST::Visitor, AST::PHP_script, AST::Statement>(in);
    while(depth > 0) {
      in->visit(visitor);
      in = visitor->getAST();
      depth--;
    }
  }

  //----- end inline procedures

  in->visit (new FindInputsOutputsClassVisitor<AST::Visitor, AST::PHP_script, AST::Statement> ());

  outputResultsFindInputsOutputs();

  CallGraphCalculator* cgc = new CallGraphCalculator(in);
  list< map<Method*, list<Method* > > > callGraph = cgc->calculateCallGraph(directOutputMethodCallers);
  //dfc->outputcallGraph();
  
  outputcallGraph(callGraph);




  
  cout << endl << "---- Tenant Isolation Analysis (ProcLevel) ----" << endl;

  cout << endl << "----- Find Domain Object properties  ----" << endl;
  
  list<String> domainClasses; //business objects
  domainClasses.push_back("DomainObject");
  domainClasses.push_back("Album");

  //---  SugarCRM ------------------
  //testCode_paper
  domainClasses.push_back("BO");
  
  //Accounts
  domainClasses.push_back("Account");
  domainClasses.push_back("Campaign");
  domainClasses.push_back("Company");

  //ACL modules
  domainClasses.push_back("ACLRole");
  domainClasses.push_back("ACLAction");
  domainClasses.push_back("ACLRolesViewList");
  domainClasses.push_back("Popup_Picker");

  //Campaign_modules_combined
  domainClasses.push_back("CampaignLog");
  domainClasses.push_back("Popup_Picker");
  domainClasses.push_back("Campaign");
  domainClasses.push_back("campaign_charts");
  domainClasses.push_back("charts");
  domainClasses.push_back("DeleteTestCampaigns");
  domainClasses.push_back("ProspectLink");
  domainClasses.push_back("CampaignsViewClassic");
  domainClasses.push_back("CampaignsViewDetail");
  domainClasses.push_back("CampaignsViewModulelistmenu");
  domainClasses.push_back("ViewNewsLetterList");
  domainClasses.push_back("CampaignTracker");
  domainClasses.push_back("CampaignLog");
  domainClasses.push_back("Campaign");

  //Email_modules_combined_e1
  domainClasses.push_back("EmailAddress");
  domainClasses.push_back("EmailManController");
  domainClasses.push_back("EmailMan");
  domainClasses.push_back("ViewCampaignconfig");
  domainClasses.push_back("ViewConfig");
  domainClasses.push_back("EmailManViewList");
  domainClasses.push_back("Email");
  domainClasses.push_back("EmailUI");
  domainClasses.push_back("Popup_Picker");
  domainClasses.push_back("EmailsViewModulelistmenu");
  domainClasses.push_back("EmailsViewQuickcreate");
  domainClasses.push_back("EmailTemplate");
  domainClasses.push_back("EmailTemplateFormBase");
  domainClasses.push_back("EmailText");
  domainClasses.push_back("OutboundEmail");

  //Employee_module_e1
  domainClasses.push_back("EmployeesController");
  domainClasses.push_back("Employee");
  domainClasses.push_back("Person");
  domainClasses.push_back("EmployeesViewDetail");
  domainClasses.push_back("EmployeesViewEdit");
  domainClasses.push_back("EmployeesViewList");


  list<String> tenantIDGetterMethods; //safe methods for retrieving the proper tenant ID
  tenantIDGetterMethods.push_back("getTenantID");

  //ACL_modules_combined_e1
  tenantIDGetterMethods.push_back("userHasAccess");
  tenantIDGetterMethods.push_back("getUserAccessLevel");
  tenantIDGetterMethods.push_back("userNeedsOwnership");
  
  //Employee_module_e1
  tenantIDGetterMethods.push_back("isAdminForModule");

  list<String> targetVarNames; //attribues of domain objects (automatically determined)
    
  list<String>::iterator domClass_it = domainClasses.begin();
  while(domClass_it != domainClasses.end()) {
    String className = *domClass_it;
    FindClassPropertiesVisitor<AST::Visitor, 
      AST::PHP_script, AST::Statement> *dcc = 
      new FindClassPropertiesVisitor<AST::Visitor, AST::PHP_script, AST::Statement> (&className);
    in->visit (dcc);  
    
    list<String> publicAttributes = dcc->getPublicAttributes();
    list<String>::iterator publicAttr_it = publicAttributes.begin();
    while(publicAttr_it != publicAttributes.end()) {
      String s = *publicAttr_it;
      targetVarNames.push_back(s);
      //cout << " Added public Attr: " << s << endl;
      publicAttr_it++;
    }
    
    delete dcc;
    domClass_it++;
  }


  cout << endl << "----- Analysing Procedures:  ----" << endl;

  ProcLevelTDI* ptdi = new ProcLevelTDI(in);

  list< map<Method*, list<Method* > > >::iterator it_callGraph = callGraph.begin();
  while (it_callGraph != callGraph.end()) {
    map<Method*, list<Method*> > methodCallers = *it_callGraph;
    map<Method*, list<Method*> >::iterator it = methodCallers.begin();
    Method* m = (*it).first;
    String *methodName = (m->signature->method_name->value);

    cout << endl << "--- Procedure: " << *methodName << " ---" <<endl;

    ptdi->findWarnings(methodName, domainClasses);

    ptdi->findSuspiciousStatements(methodName, domainClasses, targetVarNames);

    ptdi->findBreachStatements(methodName, 
			       domainClasses, 
			       targetVarNames,
			       tenantIDGetterMethods);
    
    it_callGraph++;
  }
  cout << endl << "---- Analysed expressions: "<< ptdi->numberOfExpressions << endl;


  
}

extern "C" void run_hir (HIR::PHP_script* in, Pass_manager* pm, String* option)
{
  // in->visit (new FindInputsOutputsClassVisitor<HIR::Visitor, HIR::PHP_script, HIR::Statement> ());
}

extern "C" void run_mir (MIR::PHP_script* in, Pass_manager* pm, String* option)
{
  // in->visit (new FindInputsOutputsClassVisitor<MIR::Visitor, MIR::PHP_script, MIR::Statement> ());



}




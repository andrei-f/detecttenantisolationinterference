#include "pass_manager/Plugin_pass.h"
#include "AST_visitor.h"


#include "process_ir/General.h"
#include <list>
#include <vector>
#include <map>

using namespace AST;
using namespace std;


//GLOBAL
//Method* currentMethodCG; //currently processed method in the call 

list< list<Method*> > cgPath; //Global Call Graph Path


/**
 * Computes the call graph backwards.
 *
 * Author: Andrei Furda, 2015
 */

template <class Visitor, class PHP_script, class Statement>
class BuildCallGraphVisitor : public Visitor
{

private:
  PHP_script *script;
  bool methodInvocationFound;

  list<Method_invocation*> currentMethodInvocCG; //currently processed method invocations

  Method* currentMethod; //currently processed method in the call 
  

public:
  
  list<Method* > currentCallers; //currentMethod, called methods

  BuildCallGraphVisitor(Method* currentMethod) {
    this->currentMethod = currentMethod;

  }

  void pre_php_script(PHP_script* in)
  {
    methodInvocationFound = false;
    cout << endl << endl << "Build call graph, search for: " << *(currentMethod->signature->method_name->value) << endl;
    script = in;
    
  }

  void post_php_script(PHP_script* in)
  {
    //    cout << "AST root reached" << endl;
    cgPath.push_back(currentCallers);
    
    list<Method* >::iterator it = currentCallers.begin();
    while (it != currentCallers.end()) {
      script->visit (new BuildCallGraphVisitor<AST::Visitor, AST::PHP_script, AST::Statement> (*it));
      it++;
    }
  }


  void post_method(Method* in) {

    if (methodInvocationFound) {
      cout << "Caller found: " << *(in->signature->method_name->value) << endl;
      methodInvocationFound = false;
      currentCallers.push_back(in);
      
      currentMethod = in;

      //add this to the callGraph
//       map<Method*, list<Method_invocation*> >::iterator it = currentCallerMap.find(in);
//       //element not there
//       if (it == currentCallerMap.end()) {
// 		currentCallerMap.insert( std::pair<Method*, list<Method_invocation*> > (in, currentMethodInvocCG));
// 	cout << "debug: adding new pair"<< endl;
//       } else {
// 	//insert 'in' into the matching list
// 	list<Method_invocation*> tmpList = it->second;
// 	tmpList.merge(currentMethodInvocCG);
// 	cout << "debug: adding into existing list"<< endl;
// 	it->second = tmpList;
//       }
      
//      script->visit (new BuildCallGraphVisitor<AST::Visitor, AST::PHP_script, AST::Statement> (in));
      
//       //check if it's an input method, if yes -> return, no -> continue
//       list<Method*>::iterator itInput = directInputMethodCallers.begin();
//       while (itInput != directInputMethodCallers.end()) {
	
// 	//String currentName = *(itInput->signature->method_name->value);
// 	if(in->match(*itInput)) {
// 	  cout << "INPUT METHOD FOUND ON PATH." << endl;
// 	  break;
// 	} else {//continue searching
// 	  script->visit (new BuildCallGraphVisitor<AST::Visitor, AST::PHP_script, AST::Statement> ());
// 	}
// 	itInput++;
//       }
      
      
    }

  }

  void pre_method(Method* in) {
    
//     //add the first method to the CG path
//     String firstMethodName = *(currentMethodCG->signature->method_name->value);
//     if(in->signature->method_name->match(new METHOD_NAME(&firstMethodName))) {
//       cout << "First method found:" << firstMethodName << endl;
//       currentCallerMap.insert( std::pair<Method*, list<Method_invocation*> > (in, currentMethodInvocCG));
//     }
    
  }


  void post_method_invocation(Method_invocation* in)
  {
    String currentName = *(currentMethod->signature->method_name->value);
    //    cout << "Current name: " << currentName << endl;

    if(in->method_name->match(new METHOD_NAME(&currentName))) {
      
      methodInvocationFound = true;
      //      currentMethodInvocCG.push_back(in);
      
      //      cout << "Method invocation found:" << currentName;
      //      cout << " ~line:" << in->get_line_number() << endl;
      
    }
    


  }
  

};

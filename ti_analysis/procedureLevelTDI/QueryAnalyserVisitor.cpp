
/**
 *  Analyses the assignemnts of a variable in a method.
 * Determines whether the variable assignment is a SQL statement 
 * and what type (SELECT, INSERT, UPDATE, DELETE)
 * 
 * Author: Andrei Furda
 */

#include "pass_manager/Plugin_pass.h"
#include "AST_visitor.h"


#include "process_ir/General.h"
#include <list>
#include <vector>
#include <map>

using namespace AST;
using namespace std;

//GLOBALS
enum query_type { UNKNOWN, INPUT, OUTPUT  }; //type of ananlysed query

//Analyses the assignemnts of a variable in a method.
//Determines whether the variable assignment is a SQL statement 
//and what type (SELECT, INSERT, UPDATE, DELETE)
template <class Visitor, class PHP_script, class Statement>
class QueryAnalyserVisitor : public Visitor
{

private:
  PHP_script *script;
  bool methodFound;
  Method* targetMethod;
  Variable* targetVariable;
  bool assignmentFound;
  bool methodInvocationFound;
  bool queryTypeDetermined; // query type has been determined
  query_type query_type_result;

  list<String> sqlInputs;
  list<String> sqlOutputs;
  
public:
  
  
  QueryAnalyserVisitor(Method* method, Variable* queryActualParameter) {
    this->targetMethod = method;
    this->targetVariable = queryActualParameter;
    this->defineIOStatements();
    queryTypeDetermined = false;
  }

  void defineIOStatements() {
    
    sqlInputs.push_back("SELECT");
    
    sqlOutputs.push_back("INSERT");
    sqlOutputs.push_back("UPDATE");    
    sqlOutputs.push_back("DELETE");


  }
  query_type getQueryType() {
    return query_type_result;
  }

  void pre_php_script(PHP_script* in)
  {
    cout << "Query Analyser: " << endl;
    methodFound = false;
    script = in;
    
  }

  void post_php_script(PHP_script* in)
  {
    
  }

  void pre_method(Method* in) {
    String targetMethodName = *(targetMethod->signature->method_name->value);
    if(in->signature->method_name->match(new METHOD_NAME(&targetMethodName))) {
      cout << "  method :" << targetMethodName << endl;
      
      methodFound = true;

    }

  }


  void post_method(Method* in) {

    methodFound = false; 
    assignmentFound = false;

  }


  void pre_assignment(Assignment* in) {

    if (methodFound) {
      Variable* var = in->variable;
     
      VARIABLE_NAME* var_name = dynamic_cast<VARIABLE_NAME*> (var->variable_name);
      //if (var->match(targetVariable) && var_name) { 
      //does not work because of intermediate variables passed to the query
      //cout << "  targetVariable found:" << *(var_name->value) << endl;
      
      Expr* expr = in->expr;
      assignmentFound = true;
	// }
    }
  }

  
//   void analyseExpression(Expr* expr) {
//     cout << "  analyseExpr:";
    
//     Literal *literal = dynamic_cast<Literal*> (expr);
//     if (literal) {
//       cout << "(literal)";

//       STRING* string_cast = dynamic_cast<STRING*>(literal);
//       if (string_cast) {
// 	cout << "(string_cast)";
// 	String* sqlstatement = string_cast->value;
// 	cout << " SQL:" << *sqlstatement << endl;
//       }
//     }
//   }


  void pre_string(STRING* in) {
    if (!queryTypeDetermined && methodFound && assignmentFound) {
      String* statement = (in->value);
      //cout << "  pre_string: ";
      //cout << *(in->value) << endl;
      
      
      query_type qtype = analyseStatement(statement);
      this->query_type_result = qtype;//set the query type result of this query
      assignmentFound = false;
    }
  }


  query_type analyseStatement(String* statement) {
    if (!queryTypeDetermined) {
      query_type result = UNKNOWN;
    
      //search for input keywords in statement
      list<String>::iterator inputIt = sqlInputs.begin();
      while (inputIt != sqlInputs.end()) {
	String keyword = *inputIt;
      
	if ((*statement).find(keyword) != string::npos) {
	  cout << " INPUT FOUND in: " << *statement << endl;
	  result = INPUT;
	  queryTypeDetermined = true;
	  //	  cout << "returning "<< result << endl;
	  return result;
	  break;
	}
      
	inputIt++;
      }

      //search for output keywords in statement
      list<String>::iterator outputIt = sqlOutputs.begin();
      while (outputIt != sqlOutputs.end()) {
	String keyword = *outputIt;
      
	if ((*statement).find(keyword) != string::npos) {
	  cout << " OUTPUT FOUND in: " << *statement << endl;
	  result = OUTPUT;
	  queryTypeDetermined = true;
	  //cout << "returning "<< result << endl;
	  return result;
	  break;
	}
      
	outputIt++;
      }

      //cout << "returning "<< result << endl;
      return result;
    }

  }



};

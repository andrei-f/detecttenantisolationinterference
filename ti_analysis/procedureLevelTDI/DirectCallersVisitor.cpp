#include "pass_manager/Plugin_pass.h"
#include "AST_visitor.h"


#include "process_ir/General.h"
#include <list>
#include <vector>
#include <map>

using namespace AST;
using namespace std;


//GLOBAL

/**
 * Finds direct callers of methods.
 * Author: Andrei Furda, 2015
 */

template <class Visitor, class PHP_script, class Statement>
class DirectCallersVisitor : public Visitor
{

private:
  PHP_script *script;
  bool methodInvocationFound;

  list<Method_invocation*> currentMethodInvocCG; //currently processed method invocations

  Method* currentMethod; //currently processed method in the call 

  list<Method* > directCallers; //RESULT OUTPUT
  

public:
  
  DirectCallersVisitor(Method* currentMethod) {
    this->currentMethod = currentMethod;
  }

  void pre_php_script(PHP_script* in)
  {
    directCallers.clear();
    methodInvocationFound = false;
    //cout << endl << endl << "DirectCallers, search for: " << *(currentMethod->signature->method_name->value) << endl;
    script = in;
  }

  list<Method*> getDirectCallers() {
    return directCallers;
  }

  void post_php_script(PHP_script* in)
  {

  }


  void post_method(Method* in) {

    if (methodInvocationFound) {
      String currentName = *(currentMethod->signature->method_name->value);
      // cout << "Caller found: " << *(in->signature->method_name->value);
      //cout << "->" << currentName << endl;;
      methodInvocationFound = false;
      directCallers.push_back(in);
    }

  }

  void pre_method(Method* in) {
    
  }


  void post_method_invocation(Method_invocation* in)
  {
    String currentName = *(currentMethod->signature->method_name->value);
  

    if(in->method_name->match(new METHOD_NAME(&currentName))) {

      methodInvocationFound = true;
      //      currentMethodInvocCG.push_back(in);
      
      //      cout << "Method invocation found:" << currentName;
      //      cout << " ~line:" << in->get_line_number() << endl;
      
    }
    


  }
  

};

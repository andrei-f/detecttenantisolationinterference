#include "pass_manager/Plugin_pass.h"
#include "AST_visitor.h"

#include "process_ir/General.h"

using namespace AST;

template <class Visitor, class PHP_script, class Statement>
class InlineMethodsListVisitor : public Visitor
{
private:
  List<string> methods;

public:

  List<string> getMethods() {
    return methods;
  }

  void pre_method_invocation(Method_invocation* in) {
    METHOD_NAME* method_name = dynamic_cast<METHOD_NAME*>(in->method_name);
    if (find(methods.begin(), methods.end(), method_name->value->c_str()) == methods.end()) {
      this->methods.push_back(method_name->value->c_str());
    }
  }

};

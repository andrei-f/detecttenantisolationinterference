Input method invocation:get_linked_beans ~line:163
Direct Input Method Caller: get_contacts->get_linked_beans
***************************Query found!
** varname= query
Query Analyser: 
  method :clear_account_case_relationship
 OUTPUT FOUND in: UPDATE cases SET account_name = '', account_id = '' WHERE account_id = '
Procedure 'clear_account_case_relationship' allows data OUTPUT
Input method invocation:fill_in_additional_list_fields ~line:192
Direct Input Method Caller: fill_in_additional_list_fields->fill_in_additional_list_fields
Input method invocation:fill_in_additional_detail_fields ~line:200
***************************Query found!
** varname= query
Query Analyser: 
  method :fill_in_additional_detail_fields
 INPUT FOUND in: SELECT a1.name from accounts a1, accounts a2 where a1.id = a2.parent_id and a2.id = '
Procedure 'fill_in_additional_detail_fields' allows data INPUT
Input method invocation:fetchByAssoc ~line:209
Direct Input Method Caller: fill_in_additional_detail_fields->fetchByAssoc
Input method invocation:get_list_view_data ~line:231
Direct Input Method Caller: get_list_view_data->get_list_view_data
***************************Query found!
** varname= query
Query Analyser: 
  method :checkForDuplicates
Procedure checkForDuplicates is of UNKNOWN type!
Input method invocation:fetchByAssoc ~line:419
Direct Input Method Caller: checkForDuplicates->fetchByAssoc
Output method invocation:save ~line:818
Output method invocation:debug ~line:820
Direct Output Method Caller: handleSave->debug
Output method invocation:handleSave ~line:3054
***************************Query found!
** varname= query
Query Analyser: 
  method :additionalDetailsAccount
Procedure additionalDetailsAccount is of UNKNOWN type!
Input method invocation:fetchByAssoc ~line:3153
Direct Output Method Caller: AccountsViewDetail->fetchByAssoc
Direct Input Method Caller: AccountsViewDetail->
Input method invocation:display ~line:3783
Output method invocation:display ~line:3783
Direct Output Method Caller: display->display
Direct Input Method Caller: display->
Output method invocation:preDisplay ~line:3882
Direct Output Method Caller: preDisplay->preDisplay

------ STAGE 1: Input/Output Methods -------------
Direct Output Method Callers:
clear_account_case_relationship
handleSave
AccountsViewDetail
display
preDisplay

Direct Input Method Callers:
get_contacts
fill_in_additional_list_fields
fill_in_additional_detail_fields
fill_in_additional_detail_fields
get_list_view_data
checkForDuplicates
AccountsViewDetail
display


---- CALL GRAPH (PLUGIN): ----
clear_account_case_relationship : 
handleSave : AccountsViewDetail;
AccountsViewDetail : 
display : display;
preDisplay : preDisplay;

---- Tenant Isolation Analysis (ProcLevel) ----

----- Find Domain Object properties  ----


ClassPropVisitor Analysing class: Account
Class found.

----- Analysing Procedures:  ----

--- Procedure: clear_account_case_relationship ---
(WL) Procedure: clear_account_case_relationship (~l:168)
 *WARNING: Parameter without type declaration: account_id
(WL) Procedure: clear_account_case_relationship (~l:168)
 *WARNING: Parameter without type declaration: case_id
>>Warnings: 2


--- Procedure: handleSave ---
(WL) Procedure: handleSave (~l:718)
 *WARNING: Parameter without type declaration: prefix
(WL) Procedure: handleSave (~l:718)
 *WARNING: Parameter without type declaration: redirect
(WL) Procedure: handleSave (~l:718)
 *WARNING: Parameter without type declaration: useRequired
>>Warnings: 3

(SL) Procedure: handleSave (~l:721)
 *Multi-Tenant object instantiated. Class Name: Account (~l:721)
(SL) Procedure: handleSave (~l:721)
 *Multi-Tenant classname used: Account (~l:721)
>>Suspicious Statements: Found 2 potential problem(s). 

(BL) Procedure: handleSave (~l:721)
 *Multi-Tenant object instantiated WITHOUT prior invocation of tenantID getter. Class Name: Account (~l:721)
>>Breaches: Found 1 potential problem(s). 


--- Procedure: AccountsViewDetail ---

--- Procedure: display ---

--- Procedure: preDisplay ---

---- Analysed expressions: 5802


real	0m1.051s
user	0m1.036s
sys	0m0.016s

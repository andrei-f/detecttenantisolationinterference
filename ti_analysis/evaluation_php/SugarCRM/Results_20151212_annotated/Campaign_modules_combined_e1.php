<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/


class CampaignLog extends SugarBean {

    var $table_name = 'campaign_log';
    var $object_name = 'CampaignLog';
    var $module_dir = 'CampaignLog';

    var $new_schema = true;

    var $campaign_id;
    var $target_tracker_key;
    var $target_id;
    var $target_type;
    var $activity_type;
    var $activity_date;
    var $related_id;
    var $related_type;
    var $deleted;
    var $list_id;
    var $hits;
    var $more_information;
    var $marketing_id;
    function CampaignLog() {
        global $sugar_config;
        parent::SugarBean();

    }

    function get_list_view_data(){
        global $locale;
        $temp_array = $this->get_list_view_array();
        //make sure that both items in array are set to some value, else return null
        if(!(isset($temp_array['TARGET_TYPE']) && $temp_array['TARGET_TYPE']!= '') || !(isset($temp_array['TARGET_ID']) && $temp_array['TARGET_ID']!= ''))
        {   //needed values to construct query are empty/null, so return null
            $GLOBALS['log']->debug("CampaignLog.php:get_list_view_data: temp_array['TARGET_TYPE'] and/or temp_array['TARGET_ID'] are empty, return null");
            $emptyArr = array();
            return $emptyArr;
        }

        $table = strtolower($temp_array['TARGET_TYPE']);

        if($temp_array['TARGET_TYPE']=='Accounts'){
            $query = "select name from $table where id = ".$this->db->quoted($temp_array['TARGET_ID']);
        }else{
            $query = "select first_name, last_name, ".$this->db->concat($table, array('first_name', 'last_name'))." name from $table" .
                " where id = ".$this->db->quoted($temp_array['TARGET_ID']);
        }
        $result=$this->db->query($query);
        $row=$this->db->fetchByAssoc($result);

        if ($row) {
            if($temp_array['TARGET_TYPE']=='Accounts'){
                $temp_array['RECIPIENT_NAME']=$row['name'];
            }else{
                $full_name = $locale->getLocaleFormattedName($row['first_name'], $row['last_name'], '');
                $temp_array['RECIPIENT_NAME']=$full_name;
            }
        }
        $temp_array['RECIPIENT_EMAIL']=$this->retrieve_email_address($temp_array['TARGET_ID']);

        $query = 'select name from email_marketing where id = \'' . $temp_array['MARKETING_ID'] . '\'';
        $result=$this->db->query($query);
        $row=$this->db->fetchByAssoc($result);

        if ($row)
        {
        	$temp_array['MARKETING_NAME'] = $row['name'];
        }

        return $temp_array;
    }

    function retrieve_email_address($trgt_id = ''){
        $return_str = '';
        if(!empty($trgt_id)){
            $qry  = " select eabr.primary_address, ea.email_address";
            $qry .= " from email_addresses ea ";
            $qry .= " Left Join email_addr_bean_rel eabr on eabr.email_address_id = ea.id ";
            $qry .= " where eabr.bean_id = '{$trgt_id}' ";
            $qry .= " and ea.deleted = 0 ";
            $qry .= " and eabr.deleted = 0" ;
            $qry .= " order by primary_address desc ";

            $result=$this->db->query($qry);
            $row=$this->db->fetchByAssoc($result);

            if (!empty($row['email_address'])){
                $return_str = $row['email_address'];
            }
        }
        return $return_str;
    }




    //this function is called statically by the campaign_log subpanel.
    function get_related_name($related_id, $related_type) {
        global $locale;
        $db= DBManagerFactory::getInstance();
        if ($related_type == 'Emails') {
            $query="SELECT name from emails where id='$related_id'";
            $result=$db->query($query);
            $row=$db->fetchByAssoc($result);
            if ($row != null) {
                return $row['name'];
            }
        }
        if ($related_type == 'Contacts') {
            $query="SELECT first_name, last_name from contacts where id='$related_id'";
            $result=$db->query($query);
            $row=$db->fetchByAssoc($result);
            if ($row != null) {
                return $full_name = $locale->getLocaleFormattedName($row['first_name'], $row['last_name']);
            }
        }
        if ($related_type == 'Leads') {
            $query="SELECT first_name, last_name from leads where id='$related_id'";
            $result=$db->query($query);
            $row=$db->fetchByAssoc($result);
            if ($row != null) {
                return $full_name = $locale->getLocaleFormattedName($row['first_name'], $row['last_name']);
            }
        }
        if ($related_type == 'Prospects') {
            $query="SELECT first_name, last_name from prospects where id='$related_id'";
            $result=$db->query($query);
            $row=$db->fetchByAssoc($result);
            if ($row != null) {
                return $full_name = $locale->getLocaleFormattedName($row['first_name'], $row['last_name']);
            }
        }
        if ($related_type == 'CampaignTrackers') {
            $query="SELECT tracker_url from campaign_trkrs where id='$related_id'";
            $result=$db->query($query);
            $row=$db->fetchByAssoc($result);
            if ($row != null) {
                return $row['tracker_url'] ;
            }
        }
        if ($related_type == 'Accounts') {
            $query="SELECT name from accounts where id='$related_id'";
            $result=$db->query($query);
            $row=$db->fetchByAssoc($result);
            if ($row != null) {
                return $row['name'];
            }
        }
		return $related_id.$related_type;
	}
}

?>


//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  TODO To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

global $mod_strings, $app_strings;
$module_menu = Array(
	);
?>

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/




global $theme;









class Popup_Picker
{
	
	
	/*
	 * 
	 */
	function Popup_Picker()
	{
		;
	}
	
	/*
	 * 
	 */
	function _get_where_clause()
	{
		$where = '';
		if(isset($_REQUEST['query']))
		{
			$where_clauses = array();
			append_where_clause($where_clauses, "target_id", "campaign_log.target_id");
			//append_where_clause($where_clauses, "last_name", "prospects.last_name");
		
			$where = generate_where_statement($where_clauses);
			if (!empty($where)) 
				$where.=" AND ";
			$where .=" activity_type='targeted'";
			
		}

		return $where;
	}
	
	/**
	 *
	 */
	function process_page()
	{
		global $theme;
		global $mod_strings;
		global $app_strings;
		global $app_list_strings;
		global $currentModule;
		global $sugar_version, $sugar_config;
		
		$output_html = '';
		$where = '';
		
		$where = $this->_get_where_clause();
		
		
		
		$first_name = empty($_REQUEST['first_name']) ? '' : $_REQUEST['first_name'];
		$last_name = empty($_REQUEST['last_name']) ? '' : $_REQUEST['last_name'];
		
		$request_data = empty($_REQUEST['request_data']) ? '' : $_REQUEST['request_data'];
		$hide_clear_button = empty($_REQUEST['hide_clear_button']) ? false : true;
		
		$button  = "<form action='index.php' method='post' name='form' id='form'>\n";
		//START:FOR MULTI-SELECT
		$multi_select=false;
		if (!empty($_REQUEST['mode']) && strtoupper($_REQUEST['mode']) == 'MULTISELECT') {
			$multi_select=true;
			$button .= "<input type='button' name='button' class='button' onclick=\"send_back_selected('Prospects',document.MassUpdate,'mass[]','" .$app_strings['ERR_NOTHING_SELECTED']."');\" title='"
				.$app_strings['LBL_SELECT_BUTTON_TITLE']."' value='  "
				.$app_strings['LBL_SELECT_BUTTON_LABEL']."  ' />\n";
		}
		//END:FOR MULTI-SELECT
		if(!$hide_clear_button)
		{
			$button .= "<input type='button' name='button' class='button' onclick=\"send_back('','');\" title='"
				.$app_strings['LBL_CLEAR_BUTTON_TITLE']."' value='  "
				.$app_strings['LBL_CLEAR_BUTTON_LABEL']."  ' />\n";
		}
		$button .= "<input type='submit' name='button' class='button' onclick=\"window.close();\" title='"
			.$app_strings['LBL_CANCEL_BUTTON_TITLE']."' accesskey='"
			.$app_strings['LBL_CANCEL_BUTTON_KEY']."' value='  "
			.$app_strings['LBL_CANCEL_BUTTON_LABEL']."  ' />\n";
		$button .= "</form>\n";

		$form = new XTemplate('modules/CampaignLog/Popup_picker.html');
		$form->assign('MOD', $mod_strings);
		$form->assign('APP', $app_strings);
		$form->assign('THEME', $theme);
		$form->assign('MODULE_NAME', $currentModule);
		$form->assign('request_data', $request_data);


		ob_start();
		insert_popup_header($theme);
		$output_html .= ob_get_contents();
		ob_end_clean();
		
		//$output_html .= get_form_header($mod_strings['LBL_SEARCH_FORM_TITLE'], '', false);
		
		$form->parse('main.SearchHeader');
		$output_html .= $form->text('main.SearchHeader');
		
		// Reset the sections that are already in the page so that they do not print again later.
		$form->reset('main.SearchHeader');

		// create the listview
		$seed_bean = new CampaignLog();
		$ListView = new ListView();
		$ListView->show_export_button = false;
		$ListView->process_for_popups = true;
		$ListView->setXTemplate($form);
		$ListView->multi_select_popup=$multi_select;  //FOR MULTI-SELECT	
		if ($multi_select) $ListView->xTemplate->assign("TAG_TYPE","SPAN"); else  $ListView->xTemplate->assign("TAG_TYPE","A");//FOR MULTI-SELECT
		//$ListView->setHeaderTitle($mod_strings['LBL_LIST_FORM_TITLE']); //FOR MULTI-SELECT
		//$ListView->setHeaderText($button); //FOR MULTI-SELECT
		$ListView->setQuery($where, '', 'campaign_name1', 'CAMPAIGNLOG');
		$ListView->setModStrings($mod_strings);

		ob_start();
		$output_html .= get_form_header($mod_strings['LBL_LIST_FORM_TITLE'], $button, false); //FOR MULTI-SELECT		
		$ListView->processListView($seed_bean, 'main', 'CAMPAIGNLOG');
		$output_html .= ob_get_contents();
		ob_end_clean();
				
		$output_html .= insert_popup_footer();
		return $output_html;
	}
} // end of class Popup_Picker
?>

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

$dictionary['CampaignLog'] = array ('audited'=>false,
	'comment' => 'Tracks items of interest that occurred after you send an email campaign',
	'table' => 'campaign_log',

	'fields' => array (
		'id' => array (
			'name' => 'id',
			'vname' => 'LBL_ID',
			'type' => 'id',
			'required' => true,
			'reportable'=>true,
			'comment' => 'Unique identifier'
			),
		'campaign_id' => array (
			'name' => 'campaign_id',
			'vname' => 'LBL_CAMPAIGN_ID',
			'type' => 'id',
			'comment' => 'Campaign identifier',
            'reportable' => false,
			),
		'target_tracker_key' => array (
			'name' => 'target_tracker_key',
			'vname' => 'LBL_TARGET_TRACKER_KEY',
			'type' => 'varchar',
			'len' => '36',
			'comment' => 'Identifier of Tracker URL',
            'reportable' => false,
			),
		'target_id' => array (
			'name' => 'target_id',
			'vname' => 'LBL_TARGET_ID',
			'type' => 'varchar',
			'len' => '36',
			'comment' => 'Identifier of target record',
            'reportable' => false,
			),
		'target_type' => array (
			'name' => 'target_type',
			'vname' => 'LBL_TARGET_TYPE',
			'type' => 'varchar',
			'len' => 100,
			'comment' => 'Descriptor of the target record type (e.g., Contact, Lead)'
			),
		'activity_type' => array (
			'name' => 'activity_type',
			'vname' => 'LBL_ACTIVITY_TYPE',
			'type' => 'enum',
			'options'=>'campainglog_activity_type_dom',
			'len' => 100,
			'comment' => 'The activity that occurred (e.g., Viewed Message, Bounced, Opted out)'
			),
		'activity_date' => array (
			'name' => 'activity_date',
			'vname' => 'LBL_ACTIVITY_DATE',
			'type' => 'datetime',
			'comment' => 'The date the activity occurred'
			),
		'related_id' => array (
			'name' => 'related_id',
			'vname' => 'LBL_RELATED_ID',
			'type' => 'varchar',
			'len' => '36',
            'reportable' => false,
			),
		'related_type' => array (
			'name' => 'related_type',
			'vname' => 'LBL_RELATED_TYPE',
			'type' => 'varchar',
			'len' => 100,
			),
		'archived' => array (
			'name' => 'archived',
			'vname' => 'LBL_ARCHIVED',
			'type' => 'bool',
			'reportable'=>false,
			'default'=>'0',
			'comment' => 'Indicates if item has been archived'
 		),
		'hits' => array (
			'name' => 'hits',
			'vname' => 'LBL_HITS',
			'type' => 'int',
			'default'=>'0',
			'reportable'=>true,
			'comment' => 'Number of times the item has been invoked (e.g., multiple click-thrus)'
		),
		'list_id' => array(
			'name' => 'list_id',
			'vname' => 'LBL_LIST_ID',
			'type' => 'id',
			'reportable' =>false,
			'len' => '36',
			'comment' => 'The target list from which item originated'
		),
		'deleted' => array (
			'name' => 'deleted',
			'vname' => 'LBL_DELETED',
			'type' => 'bool',
			'reportable'=>false,
			'comment' => 'Record deletion indicator'
		),
		'recipient_name' => array(
			'name' => 'recipient_name',
			'type' => 'varchar',
			'len' => '255',
			'source'=>'non-db',
		),
		'recipient_email' => array(
			'name' => 'recipient_email',
			'type' => 'varchar',
			'len' => '255',
			'source'=>'non-db',
		),
		'marketing_name' => array(
			'name' => 'marketing_name',
			'type' => 'varchar',
			'len' => '255',
			'source'=>'non-db',		
		),
      	'campaign_name1' => array (
    		'name' => 'campaign_name1',
    		'rname' => 'name',
    		'id_name' => 'campaign_id',
    		'vname' => 'LBL_CAMPAIGN_NAME',
    		'type' => 'relate',
    		'table' => 'campaigns',
    		'isnull' => 'true',
    		'module' => 'Campaigns',
    		'dbType' => 'varchar',
    		'link'=>'campaign',
    		'len' => '255',
   	 		'source'=>'non-db',
  		),
		'campaign_name' => array(
			'name' => 'campaign_name',
			'type' => 'varchar',
			'len' => '255',
			'source'=>'non-db',
		),
		'campaign_objective' => array(
			'name' => 'campaign_objective',
			'type' => 'varchar',
			'len' => '255',
			'source'=>'non-db',
		),
		'campaign_content' => array(
			'name' => 'campaign_content',
			'type' => 'varchar',
			'len' => '255',
			'source'=>'non-db',
		),
		'campaign'=> array (
  			'name' => 'campaign',
    		'type' => 'link',
    		'relationship' => 'campaign_campaignlog',
    		'source'=>'non-db',
		    'vname'=> 'LBL_CAMPAIGNS',
  		),
  		'related_name'=>array (
  			'source'=>'function',
		  	'function_name'=>'get_related_name',
		  	'function_class'=>'CampaignLog',
  			'function_params'=> array('related_id', 'related_type'),
  			'function_params_source'=>'this',  //valid values are 'parent' or 'this' default is parent.
  			'type'=>'function',
  			'name'=>'related_name',
  		    'reportable'=>false,
  		),
		'date_modified' => array (
	    	'name' => 'date_modified',
    		'vname' => 'LBL_DATE_MODIFIED',
    		'type' => 'datetime',
    	),
    	'more_information'=> array(
			'name'=>'more_information',
			'vname'=>'LBL_MORE_INFO',
			'type'=>'varchar',
			'len'=>'100',
		),
        'marketing_id' => array(
	        'name' => 'marketing_id',
	        'vname' => 'LBL_MARKETING_ID',
	        'type' => 'id',
	        'reportable' =>false,
	        'comment' => 'ID of marketing email this entry is associated with',
        ),
        'created_contact'=> array (
            'name' => 'created_contact',
            'vname' => 'LBL_CREATED_CONTACT',
            'type' => 'link',
            'relationship' => 'campaignlog_contact',
            'source'=>'non-db',
        ),
        'created_lead'=> array (
            'name' => 'created_lead',
            'vname' => 'LBL_CREATED_LEAD',
            'type' => 'link',
            'relationship' => 'campaignlog_lead',
            'source'=>'non-db',
        ),
        'created_opportunities'=> array (
            'name'         => 'created_opportunities',
            'vname'        => 'LBL_CREATED_OPPORTUNITY',
            'type'         => 'link',
            'relationship' => 'campaignlog_created_opportunities',
            'source'       => 'non-db',
        ),
        'targeted_user' => array(
            'name'         => 'targeted_user',
            'vname'        => 'LBL_TARGETED_USER',
            'type'         => 'link',
            'relationship' => 'campaignlog_targeted_users',
            'source'       => 'non-db',
        ),
        'sent_email'    => array(
            'name'         => 'sent_email',
            'vname'        => 'LBL_SENT_EMAIL',
            'type'         => 'link',
            'relationship' => 'campaignlog_sent_emails',
            'source'       => 'non-db',
        ),
	),
	'indices' => array (
		array (
			'name' =>'campaign_log_pk',

			'type' =>'primary',
			'fields'=>array('id')
		),
		array (
			'name' =>'idx_camp_tracker',

			'type' =>'index',
			'fields'=>array('target_tracker_key')
		),

		array (
			'name' =>'idx_camp_campaign_id',

			'type' =>'index',
			'fields'=>array('campaign_id')
		),

		array (
			'name' =>'idx_camp_more_info',

			'type' =>'index',
			'fields'=>array('more_information')
		),
		array (
			'name' =>'idx_target_id',

			'type' =>'index',
			'fields'=>array('target_id')
		),
        array (
			'name' =>'idx_target_id_deleted',

			'type' =>'index',
			'fields'=>array('target_id','deleted')
		),


	),
	'relationships' => array (
        'campaignlog_contact' => array( 'lhs_module'=> 'CampaignLog',
								        'lhs_table'=> 'campaign_log',
								        'lhs_key' => 'related_id',
								        'rhs_module'=> 'Contacts',
								        'rhs_table'=> 'contacts',
								        'rhs_key' => 'id',
								        'relationship_type'=>'one-to-many'),
        'campaignlog_lead' => array('lhs_module'=> 'CampaignLog',
							        'lhs_table'=> 'campaign_log',
							        'lhs_key' => 'related_id',
							        'rhs_module'=> 'Leads',
							        'rhs_table'=> 'leads',
							        'rhs_key' => 'id',
							        'relationship_type'=>'one-to-many'),
        'campaignlog_created_opportunities' => array(
            'lhs_module'=> 'CampaignLog',
            'lhs_table'=> 'campaign_log',
            'lhs_key' => 'related_id',
            'rhs_module'=> 'Opportunities',
            'rhs_table'=> 'opportunities',
            'rhs_key' => 'id',
            'relationship_type'=>'one-to-many'
        ),
        'campaignlog_targeted_users' => array(
            'lhs_module'=> 'CampaignLog',
            'lhs_table'=> 'campaign_log',
            'lhs_key' => 'target_id',
            'rhs_module'=> 'Users',
            'rhs_table'=> 'users',
            'rhs_key' => 'id',
            'relationship_type'=>'one-to-many'
        ),
        'campaignlog_sent_emails' => array(
            'lhs_module'=> 'CampaignLog',
            'lhs_table'=> 'campaign_log',
            'lhs_key' => 'related_id',
            'rhs_module'=> 'Emails',
            'rhs_table'=> 'emails',
            'rhs_key' => 'id',
            'relationship_type'=>'one-to-many'
        ),
    )
);
?>

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:
 ********************************************************************************/

$action_file_map['subpanelviewer'] = 'modules/Campaigns/SubPanelViewer.php';

?>

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:
 ********************************************************************************/

class Campaign extends SugarBean {
	var $field_name_map;

	// Stored fields
	var $id;
	var $date_entered;
	var $date_modified;
	var $modified_user_id;
	var $assigned_user_id;
	var $created_by;
	var $created_by_name;
    var $currency_id;
	var $modified_by_name;
	var $name;
	var $start_date;
	var $end_date;
	var $status;
	var $expected_cost;
	var $budget;
	var $actual_cost;
	var $expected_revenue;
	var $campaign_type;
	var $objective;
	var $content;
	var $tracker_key;
	var $tracker_text;
	var $tracker_count;
	var $refer_url;
    var $impressions;

	// These are related
	var $assigned_user_name;

	// module name definitions and table relations
	var $table_name = "campaigns";
	var $rel_prospect_list_table = "prospect_list_campaigns";
	var $object_name = "Campaign";
	var $module_dir = 'Campaigns';
	var $importable = true;

  	// This is used to retrieve related fields from form posts.
	var $additional_column_fields = array(
				'assigned_user_name', 'assigned_user_id',
	);

	var $relationship_fields = Array('prospect_list_id'=>'prospect_lists');

	var $new_schema = true;

	function list_view_parse_additional_sections(&$listTmpl) {
		global $locale;

		// take $assigned_user_id and get the Username value to assign
		$assId = $this->getFieldValue('assigned_user_id');

		$query = "SELECT first_name, last_name FROM users WHERE id = '".$assId."'";
		$result = $this->db->query($query);
		$user = $this->db->fetchByAssoc($result);

		//_ppd($user);
		if(!empty($user)) {
			$fullName = $locale->getLocaleFormattedName($user->first_name, $user->last_name);
			$listTmpl->assign('ASSIGNED_USER_NAME', $fullName);
		}
	}


	function get_summary_text()
	{
		return $this->name;
	}

        function create_export_query(&$order_by, &$where, $relate_link_join='')
        {
            $custom_join = $this->getCustomJoin(true, true, $where);
            $custom_join['join'] .= $relate_link_join;
            $query = "SELECT
            campaigns.*,
            users.user_name as assigned_user_name ";
            $query .=  $custom_join['select'];
	        $query .= " FROM campaigns ";
			$query .= "LEFT JOIN users
                      ON campaigns.assigned_user_id=users.id";
            $query .=  $custom_join['join'];

		$where_auto = " campaigns.deleted=0";

        if($where != "")
                $query .= " where $where AND ".$where_auto;
        else
                $query .= " where ".$where_auto;

        if($order_by != "")
                $query .= " ORDER BY $order_by";
        else
                $query .= " ORDER BY campaigns.name";
        return $query;
    }



	function clear_campaign_prospect_list_relationship($campaign_id, $prospect_list_id='')
	{
		if(!empty($prospect_list_id))
			$prospect_clause = " and prospect_list_id = '$prospect_list_id' ";
		else
			$prospect_clause = '';

		$query = "DELETE FROM $this->rel_prospect_list_table WHERE campaign_id='$campaign_id' AND deleted = '0' " . $prospect_clause;
	 	$this->db->query($query, true, "Error clearing campaign to prospect_list relationship: ");
	}



	function mark_relationships_deleted($id)
	{
		$this->clear_campaign_prospect_list_relationship($id);
	}

	function fill_in_additional_list_fields()
	{
		parent::fill_in_additional_list_fields();
	}

	function fill_in_additional_detail_fields()
	{
        parent::fill_in_additional_detail_fields();
		//format numbers.

		//don't need additional formatting here.
		//$this->budget=format_number($this->budget);
		//$this->expected_cost=format_number($this->expected_cost);
		//$this->actual_cost=format_number($this->actual_cost);
		//$this->expected_revenue=format_number($this->expected_revenue);
	}


	function update_currency_id($fromid, $toid){
	}


	function get_list_view_data(){

		$temp_array = $this->get_list_view_array();
		if ($this->campaign_type != 'Email') {
			$temp_array['OPTIONAL_LINK']="display:none";
		}
		$temp_array['TRACK_CAMPAIGN_TITLE'] = translate("LBL_TRACK_BUTTON_TITLE",'Campaigns');
		$temp_array['TRACK_CAMPAIGN_IMAGE'] = SugarThemeRegistry::current()->getImageURL('view_status.gif');
		$temp_array['LAUNCH_WIZARD_TITLE'] = translate("LBL_TO_WIZARD_TITLE",'Campaigns');
		$temp_array['LAUNCH_WIZARD_IMAGE'] = SugarThemeRegistry::current()->getImageURL('edit_wizard.gif');
        $temp_array['TRACK_VIEW_ALT_TEXT'] = translate("LBL_TRACK_BUTTON_TITLE",'Campaigns');
        $temp_array['LAUNCH_WIZ_ALT_TEXT'] = translate("LBL_TO_WIZARD_TITLE",'Campaigns');

		return $temp_array;
	}
	/**
		builds a generic search based on the query string using or
		do not include any $this-> because this is called on without having the class instantiated
	*/
	function build_generic_where_clause ($the_query_string)
	{
		$where_clauses = Array();
		$the_query_string = $this->db->quote($the_query_string);
		array_push($where_clauses, "campaigns.name like '$the_query_string%'");

		$the_where = "";
		foreach($where_clauses as $clause)
		{
			if($the_where != "") $the_where .= " or ";
			$the_where .= $clause;
		}


		return $the_where;
	}

	function save($check_notify = FALSE) {

			//US DOLLAR
			if(isset($this->amount) && !empty($this->amount)){

				$currency = new Currency();
				$currency->retrieve($this->currency_id);
				$this->amount_usdollar = $currency->convertToDollar($this->amount);

			}

		$this->unformat_all_fields();

		// Bug53301
		if($this->campaign_type != 'NewsLetter') {
		    $this->frequency = '';
		}
		
		return parent::save($check_notify);

	}


	function mark_deleted($id){
        $query = "update contacts set campaign_id = null where campaign_id = '{$id}' ";
        $this->db->query($query);
        $query = "update accounts set campaign_id = null where campaign_id = '{$id}' ";
        $this->db->query($query);
        // bug49632 - delete campaign logs for the campaign as well
        $query = "update campaign_log set deleted = 1 where campaign_id = '{$id}' ";
        $this->db->query($query);
		return parent::mark_deleted($id);
	}

	function set_notification_body($xtpl, $camp)
	{
		$xtpl->assign("CAMPAIGN_NAME", $camp->name);
		$xtpl->assign("CAMPAIGN_AMOUNT", $camp->budget);
		$xtpl->assign("CAMPAIGN_CLOSEDATE", $camp->end_date);
		$xtpl->assign("CAMPAIGN_STATUS", $camp->status);
		$xtpl->assign("CAMPAIGN_DESCRIPTION", $camp->content);

		return $xtpl;
	}

    function track_log_leads()
    {
        $this->load_relationship('log_entries');
        $query_array = $this->log_entries->getQuery(true);

        $query_array['select'] = 'SELECT campaign_log.* ';
        $query_array['where']  = $query_array['where']. " AND activity_type = 'lead' AND archived = 0 AND target_id IS NOT NULL";

        return implode(' ', $query_array);
    }

	function track_log_entries($type=array()) {
        //get arguments being passed in
        $args = func_get_args();
        $mkt_id ='';

		$this->load_relationship('log_entries');
		$query_array = $this->log_entries->getQuery(true);

        //if one of the arguments is marketing ID, then we need to filter by it
        foreach($args as $arg){
            if(isset($arg['EMAIL_MARKETING_ID_VALUE'])){
                $mkt_id = $arg['EMAIL_MARKETING_ID_VALUE'];
            }

            if(isset($arg['group_by'])) {
            	$query_array['group_by'] = $arg['group_by'];
            }
        }



		if (empty($type))
			$type[0]='targeted';

		$query_array['select'] ="SELECT campaign_log.* ";
		$query_array['where'] = $query_array['where']. " AND activity_type='{$type[0]}' AND archived=0";
        //add filtering by marketing id, if it exists
        if (!empty($mkt_id)) $query_array['where'] = $query_array['where']. " AND marketing_id ='$mkt_id' ";

        //B.F. #37943
        if( isset($query_array['group_by']))
        {
			//perform the inner join with the group by if a marketing id is defined, which means we need to filter out duplicates.
			//if no marketing id is specified then we are displaying results from multiple marketing emails and it is understood there might be duplicate target entries
			if (!empty($mkt_id)){
				$group_by = str_replace("campaign_log", "cl", $query_array['group_by']);
				$join_where = str_replace("campaign_log", "cl", $query_array['where']);
				$query_array['from'] .= " INNER JOIN (select min(id) as id from campaign_log cl $join_where GROUP BY $group_by  ) secondary
					on campaign_log.id = secondary.id	";
			}
            unset($query_array['group_by']);
        } else if(isset($query_array['group_by'])) {
           $query_array['where'] = $query_array['where'] . ' GROUP BY ' . $query_array['group_by'];
           unset($query_array['group_by']);
        }

        $query = (implode(" ",$query_array));
        return $query;
	}


	function get_queue_items() {
        //get arguments being passed in
        $args = func_get_args();
        $mkt_id ='';

        $this->load_relationship('queueitems');
		$query_array = $this->queueitems->getQuery(true);

        //if one of the arguments is marketing ID, then we need to filter by it
        foreach($args as $arg){
            if(isset($arg['EMAIL_MARKETING_ID_VALUE'])){
                $mkt_id = $arg['EMAIL_MARKETING_ID_VALUE'];
            }

            if(isset($arg['group_by'])) {
            	$query_array['group_by'] = $arg['group_by'];
            }
        }

        //add filtering by marketing id, if it exists, and if where key is not empty
        if (!empty($mkt_id) && !empty($query_array['where'])){
             $query_array['where'] = $query_array['where']. " AND marketing_id ='$mkt_id' ";
        }

		//get select query from email man
		$man = new EmailMan();
		$listquery= $man->create_queue_items_query('',str_replace(array("WHERE","where"),"",$query_array['where']),null,$query_array);
		return $listquery;

	}
//	function get_prospect_list_entries() {
//		$this->load_relationship('prospectlists');
//		$query_array = $this->prospectlists->getQuery(true);
//
//		$query=<<<EOQ
//			SELECT distinct prospect_lists.*,
//			(case  when (email_marketing.id is null) then default_message.id else email_marketing.id end) marketing_id,
//			(case  when  (email_marketing.id is null) then default_message.name else email_marketing.name end) marketing_name
//
//			FROM prospect_lists
//
//			INNER JOIN prospect_list_campaigns ON (prospect_lists.id=prospect_list_campaigns.prospect_list_id AND prospect_list_campaigns.campaign_id='{$this->id}')
//
//			LEFT JOIN email_marketing on email_marketing.message_for = prospect_lists.id and email_marketing.campaign_id = '{$this->id}'
//			and email_marketing.deleted =0 and email_marketing.status='active'
//
//			LEFT JOIN email_marketing default_message on default_message.message_for = prospect_list_campaigns.campaign_id and
//			default_message.campaign_id = '{$this->id}' and default_message.deleted =0
//			and default_message.status='active'
//
//			WHERE prospect_list_campaigns.deleted=0 AND prospect_lists.deleted=0
//
//EOQ;
//		return $query;
//	}

	 function bean_implements($interface){
		switch($interface){
			case 'ACL':return true;
		}
		return false;
	}


	/**
	 * create_list_count_query
	 * Overrode this method from SugarBean to handle the distinct parameter used to filter out
	 * duplicate entries for some of the subpanel listivews.  Without the distinct filter, the
	 * list count would be inaccurate because one-to-many email_marketing entries may be associated
	 * with a campaign.
     *
     * @param string $query Select query string
     * @param array $param array of arguments
     * @return string count query
     *
	 */
    function create_list_count_query($query, $params=array())
    {
		//include the distinct filter if a marketing id is defined, which means we need to filter out duplicates by the passed in group by.
		//if no marketing id is specified, it is understood there might be duplicate target entries so no need to filter out
		if((strpos($query,'marketing_id') !== false )&& isset($params['distinct'])) {
		   $pattern = '/SELECT(.*?)(\s){1}FROM(\s){1}/is';  // ignores the case
    	   $replacement = 'SELECT COUNT(DISTINCT ' . $params['distinct'] . ') c FROM ';
    	   $query = preg_replace($pattern, $replacement, $query, 1);
    	   return $query;
		}

		//If distinct parameter not found, default to SugarBean's function
    	return parent::create_list_count_query($query);
    }

    /**
     * Returns count of deleted leads,
     * which were created through generated lead form
     *
     * @return integer
     */
    function getDeletedCampaignLogLeadsCount()
    {
        $query = "SELECT COUNT(*) AS count FROM campaign_log WHERE campaign_id = '" . $this->getFieldValue('id') . "' AND target_id IS NULL AND activity_type = 'lead'";
        $result = $this->db->fetchOne($query);

        return (int)$result['count'];
    }
}
?>

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

/************** general UI Stuff *************/




global $mod_strings;
global $app_list_strings;
global $app_strings;
global $current_user;

//if (!is_admin($current_user)) sugar_die("Unauthorized access to administration.");
//account for use within wizards
if(!isset($_REQUEST['inline']) || $_REQUEST['inline'] != 'inline'){
	$params = array();
	$params[] = "<a href='index.php?module=Campaigns&action=index'>{$mod_strings['LBL_MODULE_NAME']}</a>";
	$params[] = $mod_strings['LBL_CAMPAIGN_DIAGNOSTICS'];
	
	echo getClassicModuleTitle('Campaigns', $params, true);
}

global $theme;
global $currentModule;




if(isset($_REQUEST['inline']) && $_REQUEST['inline'] == 'inline'){
    {

}
}else{
    //use html if not inline
    $ss = new Sugar_Smarty();
    $ss->assign("MOD", $mod_strings);
    $ss->assign("APP", $app_strings);
    if (isset($_REQUEST['return_module'])) $ss->assign("RETURN_MODULE", $_REQUEST['return_module']);
    if (isset($_REQUEST['return_action'])) $ss->assign("RETURN_ACTION", $_REQUEST['return_action']);
    if (isset($_REQUEST['return_id'])) $ss->assign("RETURN_ID", $_REQUEST['return_id']);
    // handle Create $module then Cancel
    if (empty($_REQUEST['return_id'])) {
        $ss->assign("RETURN_ACTION", 'index');
    }
}

/************  EMAIL COMPONENTS *************/
//monitored mailbox section
$focus = new Administration();
$focus->retrieveSettings(); //retrieve all admin settings.


//run query for mail boxes of type 'bounce' 
$email_health = 0;
$email_components = 2;
$mbox_qry = "select * from inbound_email where deleted ='0' and mailbox_type = 'bounce'";
$mbox_res = $focus->db->query($mbox_qry);
$mboxTable = "<table border ='0' width='100%'  class='detail view' cellpadding='0' cellspacing='0'>";
//put all rows returned into an array
$mbox = array();
while ($mbox_row = $focus->db->fetchByAssoc($mbox_res)){$mbox[] = $mbox_row;}
    $mbox_msg = ' ';
//if the array is not empty, then set "good" message
if(isset($mbox) && count($mbox)>0){
    $mboxTable .= "<tr><td colspan='5' style='text-align: left;'><b>" .count($mbox) ." ". $mod_strings['LBL_MAILBOX_CHECK1_GOOD']." </b>.</td></tr>";
        $mboxTable .= "<tr><th scope='col' width='20%'><b>".$mod_strings['LBL_MAILBOX_NAME']."</b></th>"
                   .  " <th scope='col' width='20%'><b>".$mod_strings['LBL_LOGIN']."</b></th>"
                   .  " <th scope='col' width='20%'><b>".$mod_strings['LBL_MAILBOX']."</b></th>"
                   .  " <th scope='col' width='20%'><b>".$mod_strings['LBL_SERVER_URL']."</b></th>"
                   .  " <th scope='col' width='20%'><b>".$mod_strings['LBL_LIST_STATUS']."</b></th></tr>";

    foreach($mbox as $details){
        $mboxTable .= "<tr><td>".$details['name']."</td>";
        $mboxTable .= "<td>".$details['email_user']."</td>";
        $mboxTable .= "<td>".$details['mailbox']."</td>";
        $mboxTable .= "<td>".$details['server_url']."</td>";
        $mboxTable .= "<td>".$details['status']."</td></tr>";
    }

}else{
    //if array is empty, then set "bad" message and increment health counter
    $mboxTable .=  "<tr><td colspan='5'><b class='error'>". $mod_strings['LBL_MAILBOX_CHECK1_BAD']."</b></td></tr>";
    $email_health =$email_health +1;
}

$mboxTable.= '</table>' ;


    
$ss->assign("MAILBOXES_DETECTED_MESSAGE", $mboxTable);

//email settings configured 
$conf_msg="<table border='0' width='100%' class='detail view' cellpadding='0' cellspacing='0'>";
if (strstr($focus->settings['notify_fromaddress'], 'example.com')){
    //if from address is the default, then set "bad" message and increment health counter
    $conf_msg .= "<tr><td colspan = '5'><b class='error'> ".$mod_strings['LBL_MAILBOX_CHECK2_BAD']." </b></td></td>";
    $email_health =$email_health +1;
}else{
    $conf_msg .= "<tr><td colspan = '5'><b> ".$mod_strings['LBL_MAILBOX_CHECK2_GOOD']."</b></td></tr>";
    $conf_msg .= "<tr><th scope='col' width='20%'><b>".$mod_strings['LBL_WIZ_FROM_NAME']."</b></th>"
               .  " <th scope='col' width='20%'><b>".$mod_strings['LBL_WIZ_FROM_ADDRESS']."</b></th>"
               .  " <th scope='col' width='20%'><b>".$mod_strings['LBL_MAIL_SENDTYPE']."</b></th>";
    if($focus->settings['mail_sendtype']=='SMTP'){
     $conf_msg .= " <th scope='col' width='20%'><b>".$mod_strings['LBL_MAIL_SMTPSERVER']."</b></th>"
               .  " <th scope='col' width='20%'><b>".$mod_strings['LBL_MAIL_SMTPUSER']."</b></th></tr>";

    }else{$conf_msg .= "</tr>";}
                   
    

        $conf_msg .= "<tr><td>".$focus->settings['notify_fromname']."</td>";
        $conf_msg .= "<td>".$focus->settings['notify_fromaddress']."</td>";
        $conf_msg .= "<td>".$focus->settings['mail_sendtype']."</td>";
     if($focus->settings['mail_sendtype']=='SMTP'){
        $conf_msg .= "<td>".$focus->settings['mail_smtpserver']."</td>";
        $conf_msg .= "<td>".$focus->settings['mail_smtpuser']."</td></tr>";

    }else{$conf_msg .= "</tr>";}       

}
          
$conf_msg .= '</table>'; 
$ss->assign("EMAIL_SETTINGS_CONFIGURED_MESSAGE", $conf_msg);
$email_setup_wiz_link='';
if ($email_health>0){
    if (is_admin($current_user)){
        $email_setup_wiz_link="<a href='index.php?module=Campaigns&action=WizardEmailSetup'>".$mod_strings['LBL_EMAIL_SETUP_WIZ']."</a>";
    }else{
        $email_setup_wiz_link=$mod_strings['LBL_NON_ADMIN_ERROR_MSG'];
    }    
}

$ss->assign("EMAIL_SETUP_WIZ_LINK", $email_setup_wiz_link);
$ss->assign( 'EMAIL_IMAGE', define_image($email_health, 2));
$ss->assign( 'EMAIL_COMPONENTS', $mod_strings['LBL_EMAIL_COMPONENTS']);
$ss->assign( 'SCHEDULER_COMPONENTS', $mod_strings['LBL_SCHEDULER_COMPONENTS']);
$ss->assign( 'RECHECK_BTN', $mod_strings['LBL_RECHECK_BTN']);

/************* SCHEDULER COMPONENTS ************/

//create and run the scheduler queries 
$sched_qry = "select job, name, status from schedulers where deleted = 0 and status = 'Active'";
$sched_res = $focus->db->query($sched_qry);
$sched_health = 0;
$sched = array();
$check_sched1 = 'function::runMassEmailCampaign';
$check_sched2 = 'function::pollMonitoredInboxesForBouncedCampaignEmails';
$sched_mes = '';
$sched_mes_body = '';

$scheds = array();
//build the table rows for scheduler display
while ($sched_row = $focus->db->fetchByAssoc($sched_res)){$scheds[] = $sched_row;}
foreach ($scheds as $funct){
  if( ($funct['job']==$check_sched1)  ||   ($funct['job']==$check_sched2)){
        $sched_mes = 'use';
        $sched_mes_body .= "<tr><td style='text-align: left;'>".$funct['name']."</td>";
        $sched_mes_body .= "<td style='text-align: left;'>".$funct['status']."</td></tr>";
        if($funct['job']==$check_sched1){
            $check_sched1 ="found";
        }else{
            $check_sched2 ="found";
        }  
        
  }
}

//determine which table header to use, based on whether or not schedulers were found
$show_admin_link = false;
if($sched_mes == 'use'){
    $sched_mes = "<h5>".$mod_strings['LBL_SCHEDULER_CHECK_GOOD']."</h5><br><table class='other view' cellspacing='1'>";
    $sched_mes .= "<tr><th scope='col' width='40%'><b>".$mod_strings['LBL_SCHEDULER_NAME']."</b></tH>"
               .  " <th scope='col' width='60%'><b>".$mod_strings['LBL_SCHEDULER_STATUS']."</b></tH></tr>";
            
}else{
    $sched_mes = "<table class='other view' cellspacing='1'>";
    $sched_mes  .= "<tr><td colspan ='3'><font color='red'><b> ".$mod_strings['LBL_SCHEDULER_CHECK_BAD']."</b></font></td></tr>";
    $show_admin_link = true;
}

//determine if error messages need to be displayed for schedulers
if($check_sched2 != 'found'){
    $sched_health =$sched_health +1;
    $sched_mes_body  .= "<tr><td colspan ='3'><font color='red'> ".$mod_strings['LBL_SCHEDULER_CHECK1_BAD']."</font></td></tr>";
}
if($check_sched1 != 'found'){
    $sched_health =$sched_health +1;
    $sched_mes_body  .= "<tr><td colspan ='3' scope='row'><font color='red'>".$mod_strings['LBL_SCHEDULER_CHECK2_BAD']."</font></td></tr>";
}
$admin_sched_link='';
if ($sched_health>0){
    if (is_admin($current_user)){
        $admin_sched_link="<a href='index.php?module=Schedulers&action=index'>".$mod_strings['LBL_SCHEDULER_LINK']."</a>";
    }else{
     $admin_sched_link=$mod_strings['LBL_NON_ADMIN_ERROR_MSG'];   
    }    
}    

//put table html together and display
    $final_sched_msg = $sched_mes . $sched_mes_body . '</table>' . $admin_sched_link;        
    $ss->assign("SCHEDULER_EMAILS_MESSAGE", $final_sched_msg);
    $ss->assign( 'SCHEDULE_IMAGE', define_image($sched_health, 2));


/********** FINAL END OF PAGE UI Stuff ********/
if(!isset($_REQUEST['inline']) || $_REQUEST['inline'] != 'inline'){

      $ss->display('modules/Campaigns/CampaignDiagnostic.html');
}

/**
 * This function takes in 3 parameters and determines the appropriate image source.  
 * 
 * @param  int $num parameter is the "health" parameter being tracked whenever there is something wrong.  (higher number =bad)
 * @param  int $total Parameter is the total number things being checked.
 * @return string HTML img tag
 */
function define_image($num, $total)
{ global $mod_strings;
    //if health number is equal to total number then all checks failed, set red image
    if($num == $total){
        //red
        return SugarThemeRegistry::current()->getImage('red_camp', "align='absmiddle'", null, null, ".gif", $mod_strings['LBL_INVALID']);


    }elseif($num == 0){
        //if health number is zero, then all checks passed, set green image
        //green
       return SugarThemeRegistry::current()->getImage('green_camp', "align='absmiddle'", null, null, ".gif", $mod_strings['LBL_VALID']);


    }else{
        //if health number is between total and num params, then some checks failed but not all, set yellow image
        //yellow
        return SugarThemeRegistry::current()->getImage('yellow_camp', "align='absmiddle'", null, null, ".gif", $mod_strings['LBL_ALERT']);


    }
}
    
?>

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

 /**Check captcha validation here.
 *
 */
require_once('include/recaptcha/recaptchalib.php');

$admin=new Administration();
$admin->retrieveSettings('captcha');
if($admin->settings['captcha_on']=='1' && !empty($admin->settings['captcha_private_key'])){
	$privatekey = $admin->settings['captcha_private_key'];
}else
	die("Captcha settings not found");
$response = recaptcha_check_answer($privatekey,
									$_SERVER["REMOTE_ADDR"],
									$_REQUEST["recaptcha_challenge_field"],
									$_REQUEST["recaptcha_response_field"]);
if(!$response->is_valid){
	die("Invalid captcha entry, go back and fix. ". $response->error. " ");
}
else echo("Success");

?>


//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

* Description:  Includes the functions for Customer module specific charts.
********************************************************************************/




require_once('include/SugarCharts/SugarChartFactory.php');


class campaign_charts {
	/**
	* Creates opportunity pipeline image as a VERTICAL accumlated bar graph for multiple users.
	* param $datax- the month data to display in the x-axis
	* Portions created by SugarCRM are Copyright (C) SugarCRM, Inc..
	* All Rights Reserved..
	* Contributor(s): ______________________________________..
	*/

	function campaign_response_by_activity_type($datay= array(),$targets=array(),$campaign_id, $cache_file_name='a_file', $refresh=false, $marketing_id='') {
		global $app_strings, $mod_strings, $charset, $lang, $barChartColors,$app_list_strings;
		$sugarChart = SugarChartFactory::getInstance('','Reports');
		$xmlFile = $sugarChart->getXMLFileName($campaign_id);

		if (!file_exists($xmlFile) || $refresh == true) {
			$GLOBALS['log']->debug("datay is:");
			$GLOBALS['log']->debug($datay);
			$GLOBALS['log']->debug("user_id is: ");
			$GLOBALS['log']->debug("cache_file_name is: $xmlFile");

			$focus = new Campaign();

			$query = "SELECT activity_type,target_type, count(*) hits ";
			$query.= " FROM campaign_log ";
			$query.= " WHERE campaign_id = '$campaign_id' AND archived=0 AND deleted=0";

            //if $marketing id is specified, then lets filter the chart by the value
            if (!empty($marketing_id)){
                $query.= " AND marketing_id ='$marketing_id'";
            }

			$query.= " GROUP BY  activity_type, target_type";
			$query.= " ORDER BY  activity_type, target_type";
			$result = $focus->db->query($query);
			//$camp_data=$focus->db->fetchByAssoc($result);
			$camp_data = array();
			$leadSourceArr = array();
			$total=0;
			$total_targeted=0;
			$rowTotalArr = array();
			$rowTotalArr[] = 0;
			while($row = $focus->db->fetchByAssoc($result))
			{
				if(!isset($leadSourceArr[$row['activity_type']]['row_total'])) {
					$leadSourceArr[$row['activity_type']]['row_total']=0;
				}

				$leadSourceArr[$row['activity_type']][$row['target_type']]['hits'][] = $row['hits'];
				$leadSourceArr[$row['activity_type']][$row['target_type']]['total'][] = $row['hits'];
				$leadSourceArr[$row['activity_type']]['outcome'][$row['target_type']]=$row['target_type'];
				$leadSourceArr[$row['activity_type']]['row_total'] += $row['hits'];

				if (!isset($leadSourceArr['all_activities'][$row['target_type']])) {
					$leadSourceArr['all_activities'][$row['target_type']]=array('total'=>0);
				}

				$leadSourceArr['all_activities'][$row['target_type']]['total'] += $row['hits'];

				$total += $row['hits'];
				if ($row['activity_type'] =='targeted') {
					$targeted[$row['target_type']]=$row['hits'];
					$total_targeted+=$row['hits'];
				}
			}

			foreach ($datay as $key=>$translation) {
				if ($key == '') {
					//$key = $mod_strings['NTC_NO_LEGENDS'];
					$key = 'None';
					$translation = $mod_strings['NTC_NO_LEGENDS'];
				}
				if(!isset($leadSourceArr[$key])){
					$leadSourceArr[$key] = $key;
				}


				if(is_array($leadSourceArr[$key]) && isset($leadSourceArr[$key]['row_total'])){$rowTotalArr[]=$leadSourceArr[$key]['row_total'];}
				if(is_array($leadSourceArr[$key]) && isset($leadSourceArr[$key]['row_total']) && $leadSourceArr[$key]['row_total']>100){
					$leadSourceArr[$key]['row_total'] = round($leadSourceArr[$key]['row_total']);
				}
				$camp_data[$translation] = array();
					foreach ($targets as $outcome=>$outcome_translation){
						//create alternate text.
                        $alttext = ' ';
                        if(isset($targeted) && isset($targeted[$outcome])&& !empty($targeted[$outcome])){
						$alttext=$targets[$outcome].': '.$mod_strings['LBL_TARGETED'].' '.$targeted[$outcome]. ', '.$mod_strings['LBL_TOTAL_TARGETED'].' '. $total_targeted. ".";
                        }
						if ($key != 'targeted'){
							$hits =  (isset($leadSourceArr[$key][$outcome]) && is_array($leadSourceArr[$key][$outcome]) && is_array($leadSourceArr[$key][$outcome]['hits'])) ? array_sum($leadSourceArr[$key][$outcome]['hits']) : 0;
							$alttext.=" $translation ".$hits;
						}
						$count = (isset($leadSourceArr[$key][$outcome]) && is_array($leadSourceArr[$key][$outcome]) && is_array($leadSourceArr[$key][$outcome]['total'])) ? array_sum($leadSourceArr[$key][$outcome]['total']) : 0;
						$camp_data[$translation][$outcome] =
							array(
							"numerical_value" => $count,
							"group_text" => $translation,
							"group_key" => "",
							"count" => "{$count}",
							"group_label" => $alttext,
							"numerical_label" => "Hits",
							"numerical_key" => "hits",
							"module" => 'Campaigns',
     						"group_base_text" => $outcome,
     						"link" => $key
							);
					}

			}

            // Since this isn't a standard report chart (with report defs), set the group_by manually so the chart bars show properly
            $sugarChart->group_by = array('activity_type', 'target_type');

			if($camp_data)
			$sugarChart->setData($camp_data);
			else
			$sugarChart->setData(array());

			$sugarChart->setProperties($mod_strings['LBL_CAMPAIGN_RESPONSE_BY_RECIPIENT_ACTIVITY'], "", 'horizontal group by chart');
			$sugarChart->saveXMLFile($xmlFile, $sugarChart->generateXML());
		}

		$width = '100%';
		$return = '';
		$return .= $sugarChart->display($campaign_id, $xmlFile, $width, '480',"");

		return $return;
	}

	//campaign roi computations.
	function campaign_response_roi($datay= array(),$targets=array(),$campaign_id, $cache_file_name='a_file', $refresh=false,$marketing_id='',$is_dashlet=false,$dashlet_id='') {
		global $app_strings,$mod_strings, $current_module_strings, $charset, $lang, $app_list_strings, $current_language,$sugar_config;

		$not_empty = false;

		if ($is_dashlet){
			$mod_strings = return_module_language($current_language, 'Campaigns');
		}

		if (!file_exists($cache_file_name) || $refresh == true) {
			$GLOBALS['log']->debug("datay is:");
			$GLOBALS['log']->debug($datay);
			$GLOBALS['log']->debug("user_id is: ");
			$GLOBALS['log']->debug("cache_file_name is: $cache_file_name");

			$focus = new Campaign();
            $focus->retrieve($campaign_id);
			$opp_count=0;
			$opp_query  = "select count(*) opp_count,sum(" . db_convert("amount_usdollar","IFNULL",array(0)).")  total_value";
            $opp_query .= " from opportunities";
            $opp_query .= " where campaign_id='$campaign_id'";
            $opp_query .= " and sales_stage='Prospecting'";
            $opp_query .= " and deleted=0";

            $opp_result=$focus->db->query($opp_query);
            $opp_data=$focus->db->fetchByAssoc($opp_result);
//            if (empty($opp_data['opp_count'])) $opp_data['opp_count']=0;
            if (empty($opp_data['total_value'])) $opp_data['total_value']=0;

            //report query
			$opp_query1  = "select SUM(opp.amount) as revenue";
            $opp_query1 .= " from opportunities opp";
            $opp_query1 .= " right join campaigns camp on camp.id = opp.campaign_id";
            $opp_query1 .= " where opp.sales_stage = 'Closed Won'and camp.id='$campaign_id' and opp.deleted=0";
            $opp_query1 .= " group by camp.name";

            $opp_result1=$focus->db->query($opp_query1);
            $opp_data1=$focus->db->fetchByAssoc($opp_result1);

			//if (empty($opp_data1[]))
            if (empty($opp_data1['revenue'])){
				$opp_data1[$mod_strings['LBL_ROI_CHART_REVENUE']] = 0;
                unset($opp_data1['revenue']);
            }else{
                $opp_data1[$mod_strings['LBL_ROI_CHART_REVENUE']] = $opp_data1['revenue'];
                unset($opp_data1['revenue']);
				$not_empty = true;
            }

			$camp_query1  = "select camp.name, SUM(camp.actual_cost) as investment,SUM(camp.budget) as budget,SUM(camp.expected_revenue) as expected_revenue";
            $camp_query1 .= " from campaigns camp";
            $camp_query1 .= " where camp.id='$campaign_id'";
            $camp_query1 .= " group by camp.name";

            $camp_result1=$focus->db->query($camp_query1);
            $camp_data1=$focus->db->fetchByAssoc($camp_result1);
            //query needs to be lowercase, but array keys need to be propercased, as these are used in
            //chart to display legend

			if (empty($camp_data1['investment']))
				$camp_data1['investment'] = 0;
			else
				$not_empty = true;
			if (empty($camp_data1['budget']))
				$camp_data1['budget'] = 0;
			else
				$not_empty = true;
            if (empty($camp_data1['expected_revenue']))
            	$camp_data1['expected_revenue'] = 0;
			else
				$not_empty = true;

            $opp_data1[$mod_strings['LBL_ROI_CHART_INVESTMENT']]=$camp_data1['investment'];
	        $opp_data1[$mod_strings['LBL_ROI_CHART_BUDGET']]=$camp_data1['budget'];
	        $opp_data1[$mod_strings['LBL_ROI_CHART_EXPECTED_REVENUE']]=$camp_data1['expected_revenue'];


			$query = "SELECT activity_type,target_type, count(*) hits ";
			$query.= " FROM campaign_log ";
			$query.= " WHERE campaign_id = '$campaign_id' AND archived=0 AND deleted=0";
            //if $marketing id is specified, then lets filter the chart by the value
            if (!empty($marketing_id)){
                $query.= " AND marketing_id ='$marketing_id'";
            }
			$query.= " GROUP BY  activity_type, target_type";
			$query.= " ORDER BY  activity_type, target_type";
			$result = $focus->db->query($query);

			$leadSourceArr = array();
			$total=0;
			$total_targeted=0;

		}

		global $current_user;
		$user_id = $current_user->id;


		$width = '100%';

		$return = '';
		if (!$is_dashlet){
			$return .= '<br />';
		}


        $currency_id = $focus->currency_id;
        $currency_symbol = $sugar_config['default_currency_symbol'];
        if(!empty($currency_id)){

            $cur = new Currency();
            $cur->retrieve($currency_id);
            $currency_symbol = $cur->symbol;
        }


		$sugarChart = SugarChartFactory::getInstance();
		$sugarChart->is_currency = true;
        $sugarChart->currency_symbol = $currency_symbol;

		if ($not_empty)
	 		$sugarChart->setData($opp_data1);
		else
			$sugarChart->setData(array());
		$sugarChart->setProperties($mod_strings['LBL_CAMPAIGN_RETURN_ON_INVESTMENT'], $mod_strings['LBL_AMOUNT_IN'].$currency_symbol, 'bar chart');

    	if (!$is_dashlet){
			$xmlFile = $sugarChart->getXMLFileName('roi_details_chart');
			$sugarChart->saveXMLFile($xmlFile, $sugarChart->generateXML());
			$return .= $sugarChart->display('roi_details_chart', $xmlFile, $width, '480');
		}
		else{
			$xmlFile = $sugarChart->getXMLFileName($dashlet_id);
			$sugarChart->saveXMLFile($xmlFile, $sugarChart->generateXML());
			$return .= $sugarChart->display($dashlet_id, $xmlFile, $width, '480');
		}

		return $return;
	}
}// end charts class
?>


//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

* Description:  Includes the functions for Customer module specific charts.
********************************************************************************/
//todo: experimental class for chart data handling..not used in the application at this time.



require_once('include/charts/Charts.php');




class charts {

    /* @function:
     *
     * @param array targets: translated list of all activity types, targeted, bounced etc..
     * @param string campaign_id: chart for this campaign.
     */
    function campaign_response_chart($targets,$campaign_id) {

        $focus = new Campaign();
        $leadSourceArr = array();

        $query = "SELECT activity_type,target_type, count(*) hits ";
        $query.= " FROM campaign_log ";
        $query.= " WHERE campaign_id = '$campaign_id' AND archived=0 AND deleted=0";
        $query.= " GROUP BY  activity_type, target_type";
        $query.= " ORDER BY  activity_type, target_type";

        $result = $focus->db->query($query);
        while($row = $focus->db->fetchByAssoc($result, false)) {

            if (isset($leadSourceArr[$row['activity_type']]['value'])) {
                $leadSourceArr[$row['activity_type']]['value']=0;
            }

            $leadSourceArr[$row['activity_type']]['value']=  $leadSourceArr[$row['activity_type']]['value'] + $row['hits'];

            if (!empty($row['target_type'])) {
                $leadSourceArr[$row['activity_type']]['bars'][$row['target_type']]['value']=$row['hits'];
            }
        }

        foreach ($targets as $key=>$value) {
            if (!isset($leadSourceArr[$key])) {
                $leadSourceArr[$key]['value']=0;
            }
        }

        //use the new template.
        $xtpl=new XTemplate ('modules/Campaigns/chart.tpl');
        $xtpl->assign("GRAPHTITLE",'Campaign Response by Recipient Activity');
        $xtpl->assign("Y_DEFAULT_ALT_TEXT",'Rollover a bar to view details.');

        //process rows
        foreach ($leadSourceArr as $key=>$values) {
            if (isset($values['bars'])) {
                foreach ($values['bars'] as $bar_id=>$bar_value) {
                    $xtpl->assign("Y_BAR_ID",$bar_id);
                }
            }

        }
    }
    }// end charts class
?>


//<?php
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/


require_once('include/MVC/Controller/SugarController.php');
class CampaignsController extends SugarController{

    function action_newsletterlist(){
        $this->view = 'newsletterlist';
    }
}
?>

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

if(!isset($_REQUEST['record']))
{
	sugar_die("A record number must be specified to delete the campaign.");
}

$focus = new Campaign();
$focus->retrieve($_REQUEST['record']);

if (isset($_REQUEST['mode']) and $_REQUEST['mode']=='Test') {
	//deletes all data associated with the test run.
    require_once('modules/Campaigns/DeleteTestCampaigns.php');
    $deleteTest = new DeleteTestCampaigns();
    $deleteTest->deleteTestRecords($focus);
} else {
	if(!$focus->ACLAccess('Delete')){
		ACLController::displayNoAccess(true);
		sugar_cleanup(true);
	}
	$focus->mark_deleted($_REQUEST['record']);
}

$return_id=!empty($_REQUEST['return_id'])?$_REQUEST['return_id']:$focus->id;
require_once ('include/formbase.php');
handleRedirect($return_id, $_REQUEST['return_module']);

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/


/**
 * DeleteTestCampaigns.php
 *
 * This is a class to encapsulate deleting test campaigns
 * @author Collin Lee
 */
class DeleteTestCampaigns {

/**
 * deleteTestRecords
 *
 * This method deletes the test records for a given Campaign instance
 * @param Campaign $focus The Campaign instance
 */
function deleteTestRecords($focus)
{
    if(empty($focus) || empty($focus->id))
    {
        return;
    }

    $res = $focus->db->query("SELECT DISTINCT campaign_log.related_id emailid, prospect_lists.id as listid FROM campaign_log
            JOIN prospect_lists on campaign_log.list_id = prospect_lists.id
            WHERE campaign_log.campaign_id = '{$focus->id}' AND prospect_lists.list_type='test'");
    $test_ids = array();
    $test_list_ids = array();
    while($row = $focus->db->fetchByAssoc($res)) {
       $test_ids[] = $row['emailid'];
       $test_list_ids[$row['listid']] = true;
    }
    $test_list_ids = array_keys($test_list_ids);
    unset($res);
    if(!empty($test_ids)) {
        $focus->db->query("UPDATE emails SET deleted=1 WHERE id IN ('".join("','", $test_ids)."')");
    }

    if(!empty($test_list_ids)) {
        $query = "DELETE FROM emailman WHERE campaign_id = '{$focus->id}' AND list_id IN ('".join("','", $test_list_ids)."')";
        $focus->db->query($query);

        $query = "UPDATE campaign_log SET deleted=1 WHERE campaign_id = '{$focus->id}' AND list_id IN ('".join("','", $test_list_ids)."')";

        $focus->db->query($query);
    }
}

}

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/







global $timedate;
global $current_user;


$campaign = new Campaign();
$campaign->retrieve($_REQUEST['record']);

$query = "SELECT prospect_list_id as id FROM prospect_list_campaigns WHERE campaign_id='$campaign->id' AND deleted=0";

$fromName = $_REQUEST['from_name'];
$fromEmail = $_REQUEST['from_address'];
$date_start = $_REQUEST['date_start'];
$time_start = $_REQUEST['time_start'];
$template_id = $_REQUEST['email_template'];

$dateval = $timedate->merge_date_time($date_start, $time_start);


$listresult = $campaign->db->query($query);

while($list = $campaign->db->fetchByAssoc($listresult))
{
	$prospect_list = $list['id'];
	$focus = new ProspectList();
	
	$focus->retrieve($prospect_list);

	$query = "SELECT prospect_id,contact_id,lead_id FROM prospect_lists_prospects WHERE prospect_list_id='$focus->id' AND deleted=0";
	$result = $focus->db->query($query);

	while($row = $focus->db->fetchByAssoc($result))
	{
		$prospect_id = $row['prospect_id'];
		$contact_id = $row['contact_id'];
		$lead_id = $row['lead_id'];
		
		if($prospect_id <> '')
		{
			$moduleName = "Prospects";
			$moduleID = $row['prospect_id'];
		}
		if($contact_id <> '')
		{
			$moduleName = "Contacts";
			$moduleID = $row['contact_id'];
		}
		if($lead_id <> '')
		{
			$moduleName = "Leads";
			$moduleID = $row['lead_id'];
		}
		
		$mailer = new EmailMan();
		$mailer->module = $moduleName;
		$mailer->module_id = $moduleID;
		$mailer->user_id = $current_user->id;
		$mailer->list_id = $prospect_list;
		$mailer->template_id = $template_id;
		$mailer->from_name = $fromName;
		$mailer->from_email = $fromEmail;
		$mailer->send_date_time = $dateval;
		$mailer->save();
	}
	
	
}


$header_URL = "Location: index.php?action=DetailView&module=Campaigns&record={$_REQUEST['record']}";
$GLOBALS['log']->debug("about to post header URL of: $header_URL");

header($header_URL);
?>

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  Contains field arrays that are used for caching
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
$fields_array['Campaign'] = array ('column_fields' => array(
				"id", "date_entered",
				"date_modified", "modified_user_id",
				"assigned_user_id", "created_by",
				"name", "start_date",
				"end_date", "status",
				"budget", "expected_cost",
				"actual_cost", "expected_revenue",
				"campaign_type", "objective",
				"content", "tracker_key","refer_url","tracker_text",
				"tracker_count","currency_id","impressions",
                "frequency",
	),
        'list_fields' => array(
				'id', 'name', 'status',
				'campaign_type','assigned_user_id','assigned_user_name','end_date',
				'refer_url',"currency_id",


	),
        'required_fields' => array(
				'name'=>1, 'end_date'=>2,
				'status'=>3, 'campaign_type'=>4
	),
);
?>

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
require_once('include/formbase.php');




require_once('include/utils/db_utils.php');




global $app_list_strings, $app_strings,$mod_strings;

$site_url = $sugar_config['site_url'];
$web_form_header = $mod_strings['LBL_LEAD_DEFAULT_HEADER'];
$web_form_description = $mod_strings['LBL_DESCRIPTION_TEXT_LEAD_FORM'];
$web_form_submit_label = $mod_strings['LBL_DEFAULT_LEAD_SUBMIT'];
$web_form_required_fileds_msg = $mod_strings['LBL_PROVIDE_WEB_TO_LEAD_FORM_FIELDS'];
$web_required_symbol = $app_strings['LBL_REQUIRED_SYMBOL'];
$web_not_valid_email_address = $mod_strings['LBL_NOT_VALID_EMAIL_ADDRESS'];
$web_post_url = $site_url.'/index.php?entryPoint=WebToLeadCapture';
$web_redirect_url = '';
$web_notify_campaign = '';
$web_assigned_user = '';
$web_team_user = '';
$web_form_footer = '';
$regex = "/^\w+(['\.\-\+]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,})+\$/";
//_ppd($web_required_symbol);
if(!empty($_REQUEST['web_header'])){
    $web_form_header= $_REQUEST['web_header'];
}
if(!empty($_REQUEST['web_description'])){
    $web_form_description= $_REQUEST['web_description'];
}
if(!empty($_REQUEST['web_submit'])){
    $web_form_submit_label=to_html($_REQUEST['web_submit']);
}
if(!empty($_REQUEST['post_url'])){
    $web_post_url= $_REQUEST['post_url'];
}
if(!empty($_REQUEST['redirect_url']) && $_REQUEST['redirect_url'] !="http://"){
    $web_redirect_url= $_REQUEST['redirect_url'];
}
if(!empty($_REQUEST['notify_campaign'])){
    $web_notify_campaign = $_REQUEST['notify_campaign'];
}
if(!empty($_REQUEST['web_footer'])){
    $web_form_footer= $_REQUEST['web_footer'];
}
if(!empty($_REQUEST['campaign_id'])){
    $web_form_campaign= $_REQUEST['campaign_id'];
}
if(!empty($_REQUEST['assigned_user_id'])){
    $web_assigned_user = $_REQUEST['assigned_user_id'];
}


 $lead = new Lead();
 $fieldsMetaData = new FieldsMetaData();
 $xtpl=new XTemplate ('modules/Campaigns/WebToLeadForm.html');
 $xtpl->assign("MOD", $mod_strings);
 $xtpl->assign("APP", $app_strings);
 $Web_To_Lead_Form_html = '';
 $Web_To_Lead_Form_html .='<link rel="stylesheet" type="text/css" media="all" href="' . getJSPath(SugarThemeRegistry::current()->getCSSURL('calendar-win2k-cold-1.css')) . '">';

 $Web_To_Lead_Form_html .= "<script type=\"text/javascript\" src='" . getJSPath($site_url.'/cache/include/javascript/sugar_grp1.js') . "'></script>";
 $Web_To_Lead_Form_html .= '<script type="text/javascript" src="' . getJSPath($site_url.'/cache/include/javascript/calendar.js') . '"></script>';

 $Web_To_Lead_Form_html .="<form action='$web_post_url' name='WebToLeadForm' method='POST' id='WebToLeadForm'>";
 $Web_To_Lead_Form_html .= "<table width='100%' style='border-top: 1px solid;
border-bottom: 1px solid;
padding: 10px 6px 12px 10px;
background-color: rgb(233, 243, 255);
font-size: 12px;
background-repeat: repeat-x;
background-position: center top;'>";

$Web_To_Lead_Form_html .= "<tr align='center' style='color: rgb(0, 105, 225); font-family: Arial,Verdana,Helvetica,sans-serif; font-size: 18px; font-weight: bold; margin-bottom: 0px; margin-top: 0px;'><TD COLSPAN='4'><b><h2>$web_form_header</h2></b></TD></tr>";
$Web_To_Lead_Form_html .= "<tr align='center' style='color: rgb(0, 105, 225); font-family: Arial,Verdana,Helvetica,sans-serif; font-size: 2px; font-weight: normal; margin-bottom: 0px; margin-top: 0px;'><TD COLSPAN='4'>&nbsp</TD></tr>";
$Web_To_Lead_Form_html .= "<tr align='left' style='color: rgb(0, 105, 225); font-family: Arial,Verdana,Helvetica,sans-serif; font-size: 12px; font-weight: normal; margin-bottom: 0px; margin-top: 0px;'><TD COLSPAN='4'>$web_form_description</TD></tr>";
$Web_To_Lead_Form_html .= "<tr align='center' style='color: rgb(0, 105, 225); font-family: Arial,Verdana,Helvetica,sans-serif; font-size: 8px; font-weight: normal; margin-bottom: 0px; margin-top: 0px;'><TD COLSPAN='4'>&nbsp</TD></tr>";

 //$Web_To_Lead_Form_html .= "\n<p>\n";

if(!empty($_REQUEST['colsFirst']) && !empty($_REQUEST['colsSecond'])){
 if(count($_REQUEST['colsFirst']) < count($_REQUEST['colsSecond'])){
   $columns= count($_REQUEST['colsSecond']);
 }
 if(count($_REQUEST['colsFirst']) > count($_REQUEST['colsSecond']) || count($_REQUEST['colsFirst']) == count($_REQUEST['colsSecond'])){
   $columns= count($_REQUEST['colsFirst']);
 }
}
else if(!empty($_REQUEST['colsFirst'])){
 $columns= count($_REQUEST['colsFirst']);
}
else if(!empty($_REQUEST['colsSecond'])){
 $columns= count($_REQUEST['colsSecond']);
}


$required_fields = array();
$bool_fields = array();
for($i= 0; $i<$columns;$i++){
    $colsFirstField = '';
    $colsSecondField = '';

    if(!empty($_REQUEST['colsFirst'][$i])){
        $colsFirstField = $_REQUEST['colsFirst'][$i];
        //_pp($_REQUEST['colsFirst']);
     }
    if(!empty($_REQUEST['colsSecond'][$i])){
        $colsSecondField = $_REQUEST['colsSecond'][$i];
        //_pp($_REQUEST['colsSecond']);
     }

    if(isset($lead->field_defs[$colsFirstField]) && $lead->field_defs[$colsFirstField] != null)
    {
         $field_vname = preg_replace('/:$/','',translate($lead->field_defs[$colsFirstField]['vname'],'Leads'));
         $field_name  = $colsFirstField;
         $field_label = $field_vname .": ";
         if(isset($lead->field_defs[$colsFirstField]['custom_type']) && $lead->field_defs[$colsFirstField]['custom_type'] != null){
            $field_type= $lead->field_defs[$colsFirstField]['custom_type'];
         }
         else{
            $field_type= $lead->field_defs[$colsFirstField]['type'];
         }
         
         //bug: 47574 - make sure, that webtolead_email1 field has same required attribute as email1 field
         if($colsFirstField == 'webtolead_email1' && isset($lead->field_defs['email1']) && isset($lead->field_defs['email1']['required'])){
             $lead->field_defs['webtolead_email1']['required'] = $lead->field_defs['email1']['required'];
         }
         
         $field_required = '';
         if(isset($lead->field_defs[$colsFirstField]['required']) && $lead->field_defs[$colsFirstField]['required'] != null
             && $lead->field_defs[$colsFirstField]['required'] != 0)
          {
            $field_required = $lead->field_defs[$colsFirstField]['required'];
            if (! in_array($lead->field_defs[$colsFirstField]['name'], $required_fields)){
              array_push($required_fields,$lead->field_defs[$colsFirstField]['name']);
             }
          }
          if($lead->field_defs[$colsFirstField]['name']=='last_name'){
            if (! in_array($lead->field_defs[$colsFirstField]['name'], $required_fields)){
              array_push($required_fields,$lead->field_defs[$colsFirstField]['name']);
            }
          }
         if($field_type=='multienum' || $field_type=='enum' || $field_type=='radioenum')  $field_options= $lead->field_defs[$colsFirstField]['options'];
    }
    //preg_replace('/:$/','',translate($field_def['vname'],'Leads')
    if(isset($lead->field_defs[$colsSecondField]) && $lead->field_defs[$colsSecondField] != null)
    {
         $field1_vname= preg_replace('/:$/','',translate($lead->field_defs[$colsSecondField]['vname'],'Leads'));
         $field1_name= $colsSecondField;
         $field1_label = $field1_vname .": ";
         if(isset($lead->field_defs[$colsSecondField]['custom_type']) && $lead->field_defs[$colsSecondField]['custom_type'] != null){
            $field1_type= $lead->field_defs[$colsSecondField]['custom_type'];
         }
         else{
            $field1_type= $lead->field_defs[$colsSecondField]['type'];
         }
         
         //bug: 47574 - make sure, that webtolead_email1 field has same required attribute as email1 field
         if($colsSecondField == 'webtolead_email1' && isset($lead->field_defs['email1']) && isset($lead->field_defs['email1']['required'])){
             $lead->field_defs['webtolead_email1']['required'] = $lead->field_defs['email1']['required'];
         }
         
         $field1_required = '';
         if(isset($lead->field_defs[$colsSecondField]['required']) && $lead->field_defs[$colsSecondField]['required'] != null
             && $lead->field_defs[$colsSecondField]['required'] != 0){
          $field1_required = $lead->field_defs[$colsSecondField]['required'];
           if (! in_array($lead->field_defs[$colsSecondField]['name'], $required_fields)){
              array_push($required_fields,$lead->field_defs[$colsSecondField]['name']);
            }
         }
         if($lead->field_defs[$colsSecondField]['name']=='last_name'){
            if (! in_array($lead->field_defs[$colsSecondField]['name'], $required_fields)){
              array_push($required_fields,$lead->field_defs[$colsSecondField]['name']);
            }
         }
         if($field1_type=='multienum' || $field1_type=='enum' || $field1_type=='radioenum')  $field1_options= $lead->field_defs[$colsSecondField]['options'];
    }

     $Web_To_Lead_Form_html .= "<tr>";

    if(isset($lead->field_defs[$colsFirstField]) && $lead->field_defs[$colsFirstField] != null){
        if($field_type=='multienum' || $field_type=='enum' || $field_type=='radioenum'){
          $lead_options = '';
          if(!empty($lead->$field_name)){
            $lead_options= get_select_options_with_id($app_list_strings[$field_options], unencodeMultienum($lead->$field_name));
          }
          else{
            $lead_options= get_select_options_with_id($app_list_strings[$field_options], '');
          }
          if($field_required){
                $Web_To_Lead_Form_html .= "<td width='15%' style='text-align: left; font-size: 12px; font-weight: normal;'><span sugar='slot'>$field_label</span sugar='slot'><span class='required' style='color: rgb(255, 0, 0);'>$web_required_symbol</span></td>";
            }
         else{
                $Web_To_Lead_Form_html .= "<td width='15%' style='text-align: left; font-size: 12px; font-weight: normal;'><span sugar='slot'>$field_label</span sugar='slot'></td>";
             }
          if(isset($lead->field_defs[$colsFirstField]['isMultiSelect']) && $lead->field_defs[$colsFirstField]['isMultiSelect'] ==1){
            $Web_To_Lead_Form_html .= "<td width='35%' style='font-size: 12px; font-weight: normal;'><span sugar='slot'><select id='{$field_name}' multiple='true' name='{$field_name}[]' tabindex='1'>$lead_options</select></span sugar='slot'></td>";
          }elseif(ifRadioButton($lead->field_defs[$colsFirstField]['name'])){
            $Web_To_Lead_Form_html .="<td width='35%' style='font-size: 12px; font-weight: normal;'><span sugar='slot'>";
            foreach($app_list_strings[$field_options] as $field_option_key => $field_option){
                if($field_option != null){
                	if(!empty($lead->$field_name) && in_array($field_option_key,unencodeMultienum($lead->$field_name))){
                		$Web_To_Lead_Form_html .="<input id='$colsFirstField"."_$field_option_key' checked name='$colsFirstField' value='$field_option_key' type='radio'>";
                	} else{
                		$Web_To_Lead_Form_html .="<input id='$colsFirstField"."_$field_option_key' name='$colsFirstField' value='$field_option_key' type='radio'>";
                	}
	                $Web_To_Lead_Form_html .="<span ='document.getElementById('".$lead->field_defs[$colsFirstField]."_$field_option_key').checked =true style='cursor:default'; onmousedown='return false;'>$field_option</span><br>";
                }
            }
            $Web_To_Lead_Form_html .="</span sugar='slot'></td>";
          }else{
            $Web_To_Lead_Form_html .= "<td width='35%' style='font-size: 12px; font-weight: normal;'><span sugar='slot'><select id=$field_name name=$field_name tabindex='1'>$lead_options</select></span sugar='slot'></td>";
          }
         }
         if($field_type=='bool'){
          if($field_required){
                $Web_To_Lead_Form_html .= "<td width='15%' style='text-align: left; font-size: 12px; font-weight: normal;'><span sugar='slot'>$field_label</span sugar='slot'><span class='required' style='color: rgb(255, 0, 0);'>$web_required_symbol</span></td>";
          }
          else{
                $Web_To_Lead_Form_html .= "<td width='15%' style='text-align: left; font-size: 12px; font-weight: normal;'><span sugar='slot'>$field_label</span sugar='slot'></td>";
          }
          $Web_To_Lead_Form_html .= "<td width='35%' style='font-size: 12px; font-weight: normal;'><span sugar='slot'><input type='checkbox' id=$field_name name=$field_name></span sugar='slot'></td>";
          if (! in_array($lead->field_defs[$colsFirstField]['name'], $bool_fields)){
              array_push($bool_fields,$lead->field_defs[$colsFirstField]['name']);
             }
         }
         if($field_type=='date') {

          global $timedate;
          $cal_dateformat = $timedate->get_cal_date_format();
	      $LBL_ENTER_DATE = translate('LBL_ENTER_DATE', 'Charts');
          if($field_required){
                $Web_To_Lead_Form_html .= "<td width='15%' style='text-align: left; font-size: 12px; font-weight: normal;'><span sugar='slot'>$field_label</span sugar='slot'><span class='required' style='color: rgb(255, 0, 0);'>$web_required_symbol</span></td>";
          }
          else{
                $Web_To_Lead_Form_html .= "<td width='15%' style='text-align: left; font-size: 12px; font-weight: normal;'><span sugar='slot'>$field_label</span sugar='slot'></td>";
          }

			$Web_To_Lead_Form_html .= "
				<td width='35%' style='font-size: 12px; font-weight: normal;'>
				<script type='text/javascript'>
					update{$field_name}Value = function() {
						var format = '{$cal_dateformat}';
						var month = document.getElementById('{$field_name}_month').value;
						var day = document.getElementById('{$field_name}_day').value;
						var year = document.getElementById('{$field_name}_year').value;
						var val = format.replace('%m', month).replace('%d', day).replace('%Y', year);
						if (!parseInt(month) > 0 || !parseInt(year) > 0 || !parseInt(year) > 0)
							val = '';
						document.getElementById('{$field_name}').value = val;
					}
				</script>
				<span sugar='slot'><input type='hidden' id='{$field_name}' name='{$field_name}'/>";
          	$order = explode("%", $cal_dateformat);
          	foreach($order as $part)
          	{
          		if (!isset($part[0]))
          			continue;
          		if (strToUpper($part[0]) == "M" )
          			$Web_To_Lead_Form_html .= translate("LBL_MONTH") . ":<input class=\"text\"
					name=\"{$field_name}_month\" size='2' maxlength='2' id='{$field_name}_month' value=''
					onblur=\"update{$field_name}Value()\">";
				else if (strToUpper($part[0]) == "D" )
					$Web_To_Lead_Form_html .=  translate("LBL_DAY") . ":<input class=\"text\"
					name=\"{$field_name}_day\" size='2' maxlength='2' id='{$field_name}_day' value=''
					onblur=\"update{$field_name}Value()\">";
				else if (strToUpper($part[0]) == "Y" )
					$Web_To_Lead_Form_html .= translate("LBL_YEAR") . ":<input class=\"text\"
					name=\"{$field_name}_year\" size='4' maxlength='4' id='{$field_name}_year' value=''
					onblur=\"update{$field_name}Value()\">";
          	}
          	$Web_To_Lead_Form_html .= "</span></td>";
	     } // if

         if( $field_type=='varchar' ||  $field_type=='name'
          ||  $field_type=='phone' || $field_type=='currency' || $field_type=='url' || $field_type=='int'){
           if($field_name=='last_name' ||   $field_required){
                $Web_To_Lead_Form_html .= "<td width='15%' style='text-align: left; font-size: 12px; font-weight: normal;'><span sugar='slot'>$field_label</span sugar='slot'><span class='required' style='color: rgb(255, 0, 0);'>$web_required_symbol</span></td>";
              }
            else{
                $Web_To_Lead_Form_html .= "<td width='15%' style='text-align: left; font-size: 12px; font-weight: normal;'><span sugar='slot'>$field_label</span sugar='slot'></td>";
             }
             if ( $field_name=='email1'||$field_name=='email2' ){
                 $Web_To_Lead_Form_html .= "<td width='35%' style='font-size: 12px; font-weight: normal;'><span sugar='slot'><input id=$field_name name=$field_name type='text' onchange='validateEmailAdd();'></span sugar='slot'></td>";
             } else {
                $Web_To_Lead_Form_html .= "<td width='35%' style='font-size: 12px; font-weight: normal;'><span sugar='slot'><input id=$field_name name=$field_name type='text'></span sugar='slot'></td>";
             }
            }
          if ( $field_type == 'text' ) {
               $Web_To_Lead_Form_html .= "<td width='15%' style='text-align: left; font-size: 12px; font-weight: normal;'><span sugar='slot'>$field_label</span sugar='slot'></td>";
			   $Web_To_Lead_Form_html .= "<td width='35%' style='font-size: 12px; font-weight: normal;'><span id='ta_replace' sugar='slot'><input id=$field_name name=$field_name type='text'></span sugar='slot'></td>";
           }
           if($field_type=='relate' &&  $field_name=='account_name'){
	            $Web_To_Lead_Form_html .= "<td width='15%' style='text-align: left; font-size: 12px; font-weight: normal;'><span sugar='slot'>$field_label</span sugar='slot'></td>";
	            $Web_To_Lead_Form_html .= "<td width='35%' style='font-size: 12px; font-weight: normal;'><span sugar='slot'><input id=$field_name name=$field_name type='text'></span sugar='slot'></td>";
           }
          if($field_type=='email'){
            if($field_required){
                $Web_To_Lead_Form_html .= "<td width='15%' style='text-align: left; font-size: 12px; font-weight: normal;'><span sugar='slot'>$field_label</span sugar='slot'><span class='required' style='color: rgb(255, 0, 0);'>$web_required_symbol</span></td>";
              }
           else{
                $Web_To_Lead_Form_html .= "<td width='15%' style='text-align: left; font-size: 12px; font-weight: normal;'><span sugar='slot'>$field_label</span sugar='slot'></td>";
             }
            $Web_To_Lead_Form_html .= "<td width='35%' style='font-size: 12px; font-weight: normal;'><span sugar='slot'><input id=$field_name name=$field_name type='text' onchange='validateEmailAdd();'></span sugar='slot'></td>";
           }
       }
      else{
            $Web_To_Lead_Form_html .= "<td width='15%' style='text-align: left; font-size: 12px; font-weight: normal;'><span sugar='slot'>&nbsp</span sugar='slot'></td>";
            $Web_To_Lead_Form_html .= "<td width='35%' style='font-size: 12px; font-weight: normal;'><span sugar='slot'>&nbsp</span sugar='slot'></td>";
        }

     if(isset($lead->field_defs[$colsSecondField]) && $lead->field_defs[$colsSecondField] != null){
         if($field1_type=='multienum' || $field1_type=='enum' || $field1_type=='radioenum'){
          $lead1_options = '';
          if(!empty($lead->$field1_name)){
            $lead1_options= get_select_options_with_id($app_list_strings[$field1_options], unencodeMultienum($lead->$field1_name));
          }
          else{
            $lead1_options= get_select_options_with_id($app_list_strings[$field1_options], '');
          }
            if($field1_required){
                $Web_To_Lead_Form_html .= "<td width='15%' style='text-align: left; font-size: 12px; font-weight: normal;'><span sugar='slot'>$field1_label</span sugar='slot'><span class='required' style='color: rgb(255, 0, 0);'>$web_required_symbol</span></td>";
            }
            else{
                $Web_To_Lead_Form_html .= "<td width='15%' style='text-align: left; font-size: 12px; font-weight: normal;'><span sugar='slot'>$field1_label</span sugar='slot'></td>";
             }
            if(isset($lead->field_defs[$colsSecondField]['isMultiSelect']) && $lead->field_defs[$colsSecondField]['isMultiSelect'] ==1){
                $Web_To_Lead_Form_html .= "<td width='35%' style='font-size: 12px; font-weight: normal;'><span sugar='slot'><select id='{$field1_name}' name='{$field1_name}[]' multiple='true' tabindex='1'>$lead1_options</select></span sugar='slot'></td>";
            }elseif(ifRadioButton($lead->field_defs[$colsSecondField]['name'])){
                $Web_To_Lead_Form_html .="<td width='35%' style='font-size: 12px; font-weight: normal;'><span sugar='slot'>";
                foreach($app_list_strings[$field1_options] as $field_option_key => $field_option){
                    if($field_option != null){
                    	if(!empty($lead->$field1_name) && in_array($field_option_key,unencodeMultienum($lead->$field1_name))){
                    		$Web_To_Lead_Form_html .="<input id='$colsSecondField"."_$field_option_key' checked name='$colsSecondField' value='$field_option_key' type='radio'>";
                    	}else{
	                    	$Web_To_Lead_Form_html .="<input id='$colsSecondField"."_$field_option_key' name='$colsSecondField' value='$field_option_key' type='radio'>";
	                    }
	                    $Web_To_Lead_Form_html .="<span ='document.getElementById('".$lead->field_defs[$colsSecondField]."_$field_option_key').checked =true style='cursor:default'; onmousedown='return false;'>$field_option</span><br>";
                    }
                }
                $Web_To_Lead_Form_html .="</span sugar='slot'></td>";
            }else{
                $Web_To_Lead_Form_html .= "<td width='35%' style='font-size: 12px; font-weight: normal;'><span sugar='slot'><select id=$field1_name name=$field1_name tabindex='1'>$lead1_options</select></span sugar='slot'></td>";
            }
         }
         if($field1_type=='bool'){
          if($field1_required){
            $Web_To_Lead_Form_html .= "<td width='15%' style='text-align: left; font-size: 12px; font-weight: normal;'><span sugar='slot'>$field1_label</span sugar='slot'><span class='required' style='color: rgb(255, 0, 0);'>$web_required_symbol</span></td>";
          }
          else{
            $Web_To_Lead_Form_html .= "<td width='15%' style='text-align: left; font-size: 12px; font-weight: normal;'><span sugar='slot'>$field1_label</span sugar='slot'></td>";
          }
            $Web_To_Lead_Form_html .= "<td width='35%' style='font-size: 12px; font-weight: normal;'><span sugar='slot'><input id=$field1_name name=$field1_name type='checkbox'></span sugar='slot'></td>";
            if (! in_array($lead->field_defs[$colsSecondField]['name'], $bool_fields)){
              array_push($bool_fields,$lead->field_defs[$colsSecondField]['name']);
             }
         }
         if($field1_type=='date') {
	        global $timedate;
			$cal_dateformat = $timedate->get_cal_date_format();
	        $LBL_ENTER_DATE = translate('LBL_ENTER_DATE', 'Charts');
          if($field1_required){
                $Web_To_Lead_Form_html .= "<td width='15%' style='text-align: left; font-size: 12px; font-weight: normal;'><span sugar='slot'>$field1_label</span sugar='slot'><span class='required' style='color: rgb(255, 0, 0);'>$web_required_symbol</span></td>";
          }
          else{
                $Web_To_Lead_Form_html .= "<td width='15%' style='text-align: left; font-size: 12px; font-weight: normal;'><span sugar='slot'>$field1_label</span sugar='slot'></td>";
          }
			$Web_To_Lead_Form_html .= " 
				<td width='35%' style='font-size: 12px; font-weight: normal;'>
				<script type='text/javascript'>
					update{$field1_name}Value = function() {
						var format = '{$cal_dateformat}';
						var month = document.getElementById('{$field1_name}_month').value;
						var day = document.getElementById('{$field1_name}_day').value;
						var year = document.getElementById('{$field1_name}_year').value;
						var val = format.replace('%m', month).replace('%d', day).replace('%Y', year);
						if (!parseInt(month) > 0 || !parseInt(year) > 0 || !parseInt(year) > 0)
							val = '';
						document.getElementById('{$field1_name}').value = val;
					}
				</script>
				<span sugar='slot'><input type='hidden' id='{$field1_name}' name='{$field1_name}'/>";
          	$order = explode("%", $cal_dateformat);
          	foreach($order as $part)
          	{
          		if (!isset($part[0]))
          			continue;
          		if (strToUpper($part[0]) == "M" )
          			$Web_To_Lead_Form_html .= translate("LBL_MONTH") . ":<input class=\"text\"
					name=\"{$field1_name}_month\" size='2' maxlength='2' id='{$field1_name}_month' value='' 
					onblur=\"update{$field1_name}Value()\">";
				else if (strToUpper($part[0]) == "D" ) 
					$Web_To_Lead_Form_html .=  translate("LBL_DAY") . ":<input class=\"text\"
					name=\"{$field1_name}_day\" size='2' maxlength='2' id='{$field1_name}_day' value='' 
					onblur=\"update{$field1_name}Value()\">";
				else if (strToUpper($part[0]) == "Y" ) 
					$Web_To_Lead_Form_html .= translate("LBL_YEAR") . ":<input class=\"text\"
					name=\"{$field1_name}_year\" size='4' maxlength='4' id='{$field1_name}_year' value='' 
					onblur=\"update{$field1_name}Value()\">";
          	}
          	$Web_To_Lead_Form_html .= "</span></td>";
         } // if
         if( $field1_type=='varchar' ||  $field1_type=='name'
          ||  $field1_type=='phone' || $field1_type=='currency' || $field1_type=='url' || $field1_type=='int'){
            if($field1_name=='last_name' ||  $field1_required){
                $Web_To_Lead_Form_html .= "<td width='15%' style='text-align: left; font-size: 12px; font-weight: normal;'><span sugar='slot'>$field1_label</span sugar='slot'><span class='required' style='color: rgb(255, 0, 0);'>$web_required_symbol</span></td>";
              }
            else{
                $Web_To_Lead_Form_html .= "<td width='15%' style='text-align: left; font-size: 12px; font-weight: normal;'><span sugar='slot'>$field1_label</span sugar='slot'></td>";
             }
             if ( $field1_name=='email1'||$field1_name=='email2' ){
                 $Web_To_Lead_Form_html .= "<td width='35%' style='font-size: 12px; font-weight: normal;'><span sugar='slot'><input id=$field1_name name=$field1_name type='text' onchange='validateEmailAdd();'></span sugar='slot'></td>";
             } else {
                $Web_To_Lead_Form_html .= "<td width='35%' style='font-size: 12px; font-weight: normal;'><span sugar='slot'><input id=$field1_name name=$field1_name type='text'></span sugar='slot'></td>";
             }

           }
           if ( $field1_type == 'text' ) {
               $Web_To_Lead_Form_html .= "<td width='15%' style='text-align: left; font-size: 12px; font-weight: normal;'><span sugar='slot'>$field1_label</span sugar='slot'></td>";
				$Web_To_Lead_Form_html .= "<td width='35%' style='font-size: 12px; font-weight: normal;'><span id='ta_replace' sugar='slot'><input id=$field1_name name=$field1_name type='text'></span sugar='slot'></td>";
           }
           if($field1_type=='relate' &&  $field1_name=='account_name'){
	            $Web_To_Lead_Form_html .= "<td width='15%' style='text-align: left; font-size: 12px; font-weight: normal;'><span sugar='slot'>$field1_label</span sugar='slot'></td>";
	            $Web_To_Lead_Form_html .= "<td width='35%' style='font-size: 12px; font-weight: normal;'><span sugar='slot'><input id=$field1_name name=$field1_name type='text'></span sugar='slot'></td>";
           }
           if($field1_type=='email'){
           	if($field1_required){
                $Web_To_Lead_Form_html .= "<td width='15%' style='text-align: left; font-size: 12px; font-weight: normal;'><span sugar='slot'>$field1_label</span sugar='slot'><span class='required' style='color: rgb(255, 0, 0);'>$web_required_symbol</span></td>";
              }
            else{
                $Web_To_Lead_Form_html .= "<td width='15%' style='text-align: left; font-size: 12px; font-weight: normal;'><span sugar='slot'>$field1_label</span sugar='slot'></td>";
             }
            $Web_To_Lead_Form_html .= "<td width='35%' style='font-size: 12px; font-weight: normal;'><span sugar='slot'><input id=$field1_name name=$field1_name type='text' onchange='validateEmailAdd();'></span sugar='slot'></td>";
           }
      }
      else{
            $Web_To_Lead_Form_html .= "<td width='15%' style='text-align: left; font-size: 12px; font-weight: normal;'><span sugar='slot'>&nbsp</span sugar='slot'></td>";
            $Web_To_Lead_Form_html .= "<td width='35%' style='font-size: 12px; font-weight: normal;'><span sugar='slot'>&nbsp</span sugar='slot'></td>";
       }
       $Web_To_Lead_Form_html .= "</tr>";
}

$Web_To_Lead_Form_html .= "<tr align='center' style='color: rgb(0, 105, 225); font-family: Arial,Verdana,Helvetica,sans-serif; font-size: 18px; font-weight: bold; margin-bottom: 0px; margin-top: 0px;'><TD COLSPAN='4'>&nbsp</TD></tr>";

if(!empty($web_form_footer)){
    $Web_To_Lead_Form_html .= "<tr align='center' style='color: rgb(0, 105, 225); font-family: Arial,Verdana,Helvetica,sans-serif; font-size: 18px; font-weight: bold; margin-bottom: 0px; margin-top: 0px;'><TD COLSPAN='4'>&nbsp</TD></tr>";
    $Web_To_Lead_Form_html .= "<tr align='left' style='color: rgb(0, 105, 225); font-family: Arial,Verdana,Helvetica,sans-serif; font-size: 12px; font-weight: normal; margin-bottom: 0px; margin-top: 0px;'><TD COLSPAN='4'>$web_form_footer</TD></tr>";
}

$Web_To_Lead_Form_html .= "<tr align='center'><td colspan='10'><input type='button' onclick='submit_form();' class='button' name='Submit' value='$web_form_submit_label'/></td></tr>";

if(!empty($web_form_campaign)){
   $Web_To_Lead_Form_html .= "<tr><td style='display: none'><input type='hidden' id='campaign_id' name='campaign_id' value='$web_form_campaign'></td></tr>";
}
if(!empty($web_redirect_url)){
    $Web_To_Lead_Form_html .= "<tr><td style='display: none'><input type='hidden' id='redirect_url' name='redirect_url' value='$web_redirect_url'></td></tr>";
}
if(!empty($web_assigned_user)){
    $Web_To_Lead_Form_html .= "<tr><td style='display: none'><input type='hidden' id='assigned_user_id' name='assigned_user_id' value='$web_assigned_user'></td></tr>";
}
$req_fields='';
if(isset($required_fields) && $required_fields != null ){
    foreach($required_fields as $req){
        $req_fields=$req_fields.$req.';';
    }
}
$boolean_fields='';
if(isset($bool_fields) && $bool_fields != null ){
    foreach($bool_fields as $boo){
        $boolean_fields=$boolean_fields.$boo.';';
    }
}
if(!empty($req_fields)){
    $Web_To_Lead_Form_html .= "<tr><td style='display: none'><input type='hidden' id='req_id' name='req_id' value='$req_fields'></td></tr>";
}
if(!empty($boolean_fields)){
    $Web_To_Lead_Form_html .= "<tr><td style='display: none'><input type='hidden' id='bool_id' name='bool_id' value='$boolean_fields'></td></tr>";
}


$Web_To_Lead_Form_html .= "</table >";
$Web_To_Lead_Form_html .="</form>";

$Web_To_Lead_Form_html .="<script type='text/javascript'>
 function submit_form(){
 	if(typeof(validateCaptchaAndSubmit)!='undefined'){
 		validateCaptchaAndSubmit();
 	}else{
 		check_webtolead_fields();
 	}
 }
 function check_webtolead_fields(){
     if(document.getElementById('bool_id') != null){
        var reqs=document.getElementById('bool_id').value;
        bools = reqs.substring(0,reqs.lastIndexOf(';'));
        var bool_fields = new Array();
        var bool_fields = bools.split(';');
        nbr_fields = bool_fields.length;
        for(var i=0;i<nbr_fields;i++){
          if(document.getElementById(bool_fields[i]).value == 'on'){
             document.getElementById(bool_fields[i]).value = 1;
          }
          else{
             document.getElementById(bool_fields[i]).value = 0;
          }
        }
      }
    if(document.getElementById('req_id') != null){
        var reqs=document.getElementById('req_id').value;
        reqs = reqs.substring(0,reqs.lastIndexOf(';'));
        var req_fields = new Array();
        var req_fields = reqs.split(';');
        nbr_fields = req_fields.length;
        var req = true;
        for(var i=0;i<nbr_fields;i++){
          if(document.getElementById(req_fields[i]).value.length <=0 || document.getElementById(req_fields[i]).value==0){
           req = false;
           break;
          }
        }
        if(req){
            document.WebToLeadForm.submit();
            return true;
        }
        else{
          alert('$web_form_required_fileds_msg');
          return false;
         }
        return false
   }
   else{
    document.WebToLeadForm.submit();
   }
}
function validateEmailAdd(){
	if(document.getElementById('email1') && document.getElementById('email1').value.length >0) {
		if(document.getElementById('email1').value.match($regex) == null){
		  alert('$web_not_valid_email_address');
		}
	}
	if(document.getElementById('email2') && document.getElementById('email2').value.length >0) {
		if(document.getElementById('email2').value.match($regex) == null){
		  alert('$web_not_valid_email_address');
		}
	}
}
</script>";

if(isset($Web_To_Lead_Form_html)) $xtpl->assign("BODY", $Web_To_Lead_Form_html); else $xtpl->assign("BODY", "");
if(isset($Web_To_Lead_Form_html)) $xtpl->assign("BODY_HTML", $Web_To_Lead_Form_html); else $xtpl->assign("BODY_HTML", "");


require_once('include/SugarTinyMCE.php');
$tiny = new SugarTinyMCE();
$tiny->defaultConfig['height']=400;
$tiny->defaultConfig['apply_source_formatting']=true;
$tiny->defaultConfig['cleanup']=false;
$ed = $tiny->getInstance('body_html');
$xtpl->assign("tiny", $ed);

$xtpl->parse("main.textarea");

$xtpl->assign("INSERT_VARIABLE_ONCLICK", "insert_variable_html(document.EditView.variable_text.value)");
$xtpl->parse("main.variable_button");




$xtpl->parse("main");
$xtpl->out("main");

function ifRadioButton($customFieldName){
    $custRow = null;
    $query="select id,type from fields_meta_data where deleted = 0 and name = '$customFieldName'";
    $result=$GLOBALS['db']->query($query);
    $row = $GLOBALS['db']->fetchByAssoc($result);
    if($row != null && $row['type'] == 'radioenum'){
        return $custRow = $row;
    }
    return $custRow;
}

?>


//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

require_once('modules/Campaigns/utils.php');

$GLOBALS['log']->debug('identifier from the image request is'.$_REQUEST['identifier']);
if(!empty($_REQUEST['identifier'])) {
	$keys=log_campaign_activity($_REQUEST['identifier'],'viewed');
}
sugar_cleanup();
Header("Content-Type: image/gif");
$fn=sugar_fopen(SugarThemeRegistry::current()->getImageURL("blank.gif",false),"r");
fpassthru($fn);
?>


//<?php
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

 
$_SESSION['MAILMERGE_MODULE_FROM_LISTVIEW'] = 'Campaigns';
$_SESSION['MAILMERGE_MODULE'] = 'Campaigns'; 
$_SESSION['MAILMERGE_RECORDS'] = array($_REQUEST['record']);
header('Location: index.php?module=MailMerge&action=index'); 
?>

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  TODO To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

global $mod_strings, $app_strings;
if(ACLController::checkAccess('Campaigns', 'edit', true))
	$module_menu[] = array(
		"index.php?module=Campaigns&action=WizardHome&return_module=Campaigns&return_action=index", 
		$mod_strings['LNL_NEW_CAMPAIGN_WIZARD'],"CampaignsWizard"
	);
if(ACLController::checkAccess('Campaigns', 'edit', true))
	$module_menu[]=	array(
		"index.php?module=Campaigns&action=EditView&return_module=Campaigns&return_action=index", 
		$mod_strings['LNK_NEW_CAMPAIGN'],"CreateCampaigns"
	);
if(ACLController::checkAccess('Campaigns', 'list', true))
	$module_menu[]=	array(
		"index.php?module=Campaigns&action=index&return_module=Campaigns&return_action=index", 
		$mod_strings['LNK_CAMPAIGN_LIST'],"Campaigns"
	);
if(ACLController::checkAccess('Campaigns', 'list', true))
	$module_menu[]= array(
		"index.php?module=Campaigns&action=newsletterlist&return_module=Campaigns&return_action=index", 
		$mod_strings['LBL_NEWSLETTERS'], "Newsletters"
	);
if(ACLController::checkAccess('EmailTemplates', 'edit', true))
	$module_menu[] = array(
		"index.php?module=EmailTemplates&action=EditView&return_module=EmailTemplates&return_action=DetailView",
		$mod_strings['LNK_NEW_EMAIL_TEMPLATE'],"CreateEmails","Emails"
	);
if(ACLController::checkAccess('EmailTemplates', 'list', true))
	$module_menu[] = array(
		"index.php?module=EmailTemplates&action=index",
		$mod_strings['LNK_EMAIL_TEMPLATE_LIST'],"EmailFolder", 'Emails'
	);
if (is_admin($GLOBALS['current_user']) || is_admin_for_module($GLOBALS['current_user'],'Campaigns'))
	$module_menu[] = array(
		"index.php?module=Campaigns&action=WizardEmailSetup&return_module=Campaigns&return_action=index",
		$mod_strings['LBL_EMAIL_SETUP_WIZARD'],"EmailSetupWizard"
	);
if(ACLController::checkAccess('Campaigns', 'edit', true))
	$module_menu[] = array(
		"index.php?module=Campaigns&action=CampaignDiagnostic&return_module=Campaigns&return_action=index",
		$mod_strings['LBL_DIAGNOSTIC_WIZARD'],"EmailDiagnostic"
	);
if(ACLController::checkAccess('Campaigns', 'edit', true))
	$module_menu[] = array(
		"index.php?module=Campaigns&action=WebToLeadCreation&return_module=Campaigns&return_action=index",
		$mod_strings['LBL_WEB_TO_LEAD'],"CreateWebToLeadForm"
	);

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/




global $theme;









class Popup_Picker
{
	
	
	/*
	 * 
	 */
	function Popup_Picker()
	{
		;
	}
	
	/*
	 * 
	 */
		function _get_where_clause()
	{
		$where = '';
		if(isset($_REQUEST['query']))
		{
			$where_clauses = array();
                  
			append_where_clause($where_clauses, "name", "campaigns.name");
			append_where_clause($where_clauses, "campaign_type", "campaign_type");			
			$where = generate_where_statement($where_clauses);
		}
		
		return $where;
	}
	
	/**
	 *
	 */
	function process_page()
	{
		global $theme;
		global $mod_strings;
		global $app_strings;
		global $app_list_strings;
		global $currentModule;
		global $sugar_version, $sugar_config;
		
		$output_html = '';
		$where = '';
		
		$where = $this->_get_where_clause();
		
		
		
		$name = empty($_REQUEST['name']) ? '' : $_REQUEST['name'];
		$status = empty($_REQUEST['status']) ? '' : $_REQUEST['status'];
		$campaign_type = empty($_REQUEST['campaign_type']) ? '' : $_REQUEST['campaign_type'];
		
		$request_data = empty($_REQUEST['request_data']) ? '' : $_REQUEST['request_data'];
		$hide_clear_button = empty($_REQUEST['hide_clear_button']) ? false : true;
		
		$button  = "<form action='index.php' method='post' name='form' id='form'>\n";
		//START:FOR MULTI-SELECT
		$multi_select=false;
		if (!empty($_REQUEST['mode']) && strtoupper($_REQUEST['mode']) == 'MULTISELECT') {
			$multi_select=true;
			$button .= "<input type='button' name='button' class='button' onclick=\"send_back_selected('Prospects',document.MassUpdate,'mass[]','" .$app_strings['ERR_NOTHING_SELECTED']."');\" title='"
				.$app_strings['LBL_SELECT_BUTTON_TITLE']."' value='  "
				.$app_strings['LBL_SELECT_BUTTON_LABEL']."  ' />\n";
		}
		//END:FOR MULTI-SELECT
		if(!$hide_clear_button)
		{
			$button .= "<input type='button' name='button' class='button' onclick=\"send_back('','');\" title='"
				.$app_strings['LBL_CLEAR_BUTTON_TITLE']."' value='  "
				.$app_strings['LBL_CLEAR_BUTTON_LABEL']."  ' />\n";
		}
		$button .= "<input type='submit' name='button' class='button' onclick=\"window.close();\" title='"
			.$app_strings['LBL_CANCEL_BUTTON_TITLE']."' accesskey='"
			.$app_strings['LBL_CANCEL_BUTTON_KEY']."' value='  "
			.$app_strings['LBL_CANCEL_BUTTON_LABEL']."  ' />\n";
		$button .= "</form>\n";

		$form = new XTemplate('modules/Campaigns/Popup_picker.html');
		$form->assign('MOD', $mod_strings);
		$form->assign('APP', $app_strings);
		$form->assign('THEME', $theme);
		$form->assign('MODULE_NAME', $currentModule);
		
		$form->assign('request_data', $request_data);

        $form->assign("TYPE_OPTIONS", get_select_options_with_id($app_list_strings['campaign_type_dom'],""));
		ob_start();
		insert_popup_header($theme);
		$output_html .= ob_get_contents();
		ob_end_clean();
		
		$output_html .= get_form_header($mod_strings['LBL_SEARCH_FORM_TITLE'], '', false);
		
		$form->parse('main.SearchHeader');
		$output_html .= $form->text('main.SearchHeader');
		
		// Reset the sections that are already in the page so that they do not print again later.
		$form->reset('main.SearchHeader');

		// create the listview
		$seed_bean = new Campaign();
		$ListView = new ListView();
		$ListView->show_export_button = false;
		$ListView->process_for_popups = true;
		$ListView->setXTemplate($form);
		$ListView->multi_select_popup=$multi_select;  //FOR MULTI-SELECT	
		$ListView->xTemplate->assign("TAG_TYPE","A"); //FOR MULTI-SELECT
		$ListView->setHeaderTitle($mod_strings['LBL_LIST_FORM_TITLE']); //FOR MULTI-SELECT
		$ListView->setHeaderText($button); //FOR MULTI-SELECT
		$ListView->setQuery($where, '', 'name', 'CAMPAIGN');
		$ListView->setModStrings($mod_strings);

		ob_start();
		//$output_html .= get_form_header($mod_strings['LBL_LIST_FORM_TITLE'], $button, false); //FOR MULTI-SELECT		
		$ListView->processListView($seed_bean, 'main', 'CAMPAIGN');
		$output_html .= ob_get_contents();
		ob_end_clean();
				
		$output_html .= insert_popup_footer();
		return $output_html;
	}
} // end of class Popup_Picker
?>

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/




require_once('include/DetailView/DetailView.php');
require_once('modules/Campaigns/Charts.php');


global $mod_strings;
global $app_strings;
global $app_list_strings;
global $sugar_version, $sugar_config;

global $theme;




$GLOBALS['log']->info("Campaign detail view");

$xtpl=new XTemplate ('modules/Campaigns/PopupCampaignRoi.html');

//_pp($_REQUEST['id']);
$campaign_id=$_REQUEST['id'];
$campaign = new Campaign();
$opp_query1  = "select camp.name, camp.actual_cost,camp.budget,camp.expected_revenue,count(*) opp_count,SUM(opp.amount) as Revenue, SUM(camp.actual_cost) as Investment,
                            ROUND((SUM(opp.amount) - SUM(camp.actual_cost))/(SUM(camp.actual_cost)), 2)*100 as ROI";
            $opp_query1 .= " from opportunities opp";
            $opp_query1 .= " right join campaigns camp on camp.id = opp.campaign_id";
            $opp_query1 .= " where opp.sales_stage = 'Closed Won' and camp.id='$campaign_id'";
            $opp_query1 .= " group by camp.name";
            //$opp_query1 .= " and deleted=0";
            $opp_result1=$campaign->db->query($opp_query1);
            $opp_data1=$campaign->db->fetchByAssoc($opp_result1);
 //get the click-throughs
 $query_click = "SELECT count(*) hits ";
			$query_click.= " FROM campaign_log ";
			$query_click.= " WHERE campaign_id = '$campaign_id' AND activity_type='link' AND related_type='CampaignTrackers' AND archived=0 AND deleted=0";

            //if $marketing id is specified, then lets filter the chart by the value
            if (!empty($marketing_id)){
                $query_click.= " AND marketing_id ='$marketing_id'";
            }

			$query_click.= " GROUP BY  activity_type, target_type";
			$query_click.= " ORDER BY  activity_type, target_type";
			$result = $campaign->db->query($query_click);


  $xtpl->assign("OPP_COUNT", $opp_data1['opp_count']);
  $xtpl->assign("ACTUAL_COST",$opp_data1['actual_cost']);
  $xtpl->assign("PLANNED_BUDGET",$opp_data1['budget']);
  $xtpl->assign("EXPECTED_REVENUE",$opp_data1['expected_revenue']);




	$currency  = new Currency();
if(isset($focus->currency_id) && !empty($focus->currency_id))
{
	$currency->retrieve($focus->currency_id);
	if( $currency->deleted != 1){
		$xtpl->assign("CURRENCY", $currency->iso4217 .' '.$currency->symbol );
	}else $xtpl->assign("CURRENCY", $currency->getDefaultISO4217() .' '.$currency->getDefaultCurrencySymbol() );
}else{

	$xtpl->assign("CURRENCY", $currency->getDefaultISO4217() .' '.$currency->getDefaultCurrencySymbol() );

}

global $current_user;
if(is_admin($current_user) && $_REQUEST['module'] != 'DynamicLayout' && !empty($_SESSION['editinplace'])){

	$xtpl->assign("ADMIN_EDIT","<a href='index.php?action=index&module=DynamicLayout&from_action=".$_REQUEST['action'] ."&from_module=".$_REQUEST['module'] ."&record=".$_REQUEST['record']. "'>".SugarThemeRegistry::current()->getImage("EditLayout","border='0' align='bottom'",null,null,'.gif',$mod_strings['LBL_EDIT_LAYOUT'])."</a>");

}

//$detailView->processListNavigation($xtpl, "CAMPAIGN", $offset, $focus->is_AuditEnabled());
// adding custom fields:
//require_once('modules/DynamicFields/templates/Files/DetailView.php');

/* we need to build the dropdown of related marketing values
    $latest_marketing_id = '';
    $selected_marketing_id = '';
    if(isset($_REQUEST['mkt_id'])) $selected_marketing_id = $_REQUEST['mkt_id'];
    $options_str = '<option value="all">--None--</option>';
    //query for all email marketing records related to this campaign
    $latest_marketing_query = "select id, name, date_modified from email_marketing where campaign_id = '$focus->id' order by date_modified desc";

    //build string with value(s) retrieved
    $result =$campaign->db->query($latest_marketing_query);
    if ($row = $campaign->db->fetchByAssoc($result)){
        //first, populated the latest marketing id variable, as this
        // variable will be used to build chart and subpanels
        $latest_marketing_id = $row['id'];
        //fill in first option value
        $options_str .= '<option value="'. $row['id'] .'"';
        // if the marketing id is same as selected marketing id, set this option to render as "selected"
        if (!empty($selected_marketing_id) && $selected_marketing_id == $row['id']) {
            $options_str .=' selected>'. $row['name'] .'</option>';
        // if the marketing id is empty then set this first option to render as "selected"
        }elseif(empty($selected_marketing_id)){
            $options_str .=' selected>'. $row['name'] .'</option>';
        // if the marketing is not empty, but not same as selected marketing id, then..
        //.. do not set this option to render as "selected"
        }else{
            $options_str .='>'. $row['name'] .'</option>';
        }
    }
    //process rest of records, if they exist
    while ($row = $campaign->db->fetchByAssoc($result)){
        //add to list of option values
        $options_str .= '<option value="'. $row['id'] .'"';
        //if the marketing id is same as selected marketing id, then set this option to render as "selected"
        if (!empty($selected_marketing_id) && $selected_marketing_id == $row['id']) {
            $options_str .=' selected>'. $row['name'] .'</option>';
        }else{
            $options_str .=' >'. $row['name'] .'</option>';
        }
     }
    //populate the dropdown
    $xtpl->assign("MKT_DROP_DOWN",$options_str);

  */

//add chart
$seps				= array("-", "/");
$dates				= array(date($GLOBALS['timedate']->dbDayFormat), $GLOBALS['timedate']->dbDayFormat);
$dateFileNameSafe	= str_replace($seps, "_", $dates);
$cache_file_name_roi	= $current_user->getUserPrivGuid()."_campaign_response_by_roi_".$dateFileNameSafe[0]."_".$dateFileNameSafe[1].".xml";
$chart= new campaign_charts();

//ob_start();

    //if marketing id has been selected, then set "latest_marketing_id" to the selected value
    //latest marketing id will be passed in to filter the charts and subpanels

    if(!empty($selected_marketing_id)){$latest_marketing_id = $selected_marketing_id;}
    if(empty($latest_marketing_id) ||  $latest_marketing_id === 'all'){
        $xtpl->assign("MY_CHART_ROI", $chart->campaign_response_roi_popup($app_list_strings['roi_type_dom'],$app_list_strings['roi_type_dom'],$campaign_id,sugar_cached("xml/") . $cache_file_name_roi,true));
    }else{

    $xtpl->assign("MY_CHART_ROI", $chart->campaign_response_roi_popup($app_list_strings['roi_type_dom'],$app_list_strings['roi_type_dom'],$campaign_id,sugar_cached("xml/") .$cache_file_name_roi,true));
    }

//$output_html .= ob_get_contents();
//ob_end_clean();


//_ppd($xtpl);
//end chart

$xtpl->parse("main");
$xtpl->out("main");

?>

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:
 ********************************************************************************/
//find all mailboxes of type bounce.

/**
 * Retrieve the attached error report for a bounced email if it exists.
 *
 * @param Email $email
 * @return string
 */
function retrieveErrorReportAttachment($email)
{
    $contents = "";
    $query = "SELECT description FROM notes WHERE file_mime_type = 'messsage/rfc822' AND parent_type='Emails' AND parent_id = '".$email->id."' AND deleted=0";
    $rs = $GLOBALS['db']->query($query);
    while ($row = $GLOBALS['db']->fetchByAssoc($rs)) 
		$contents .= $row['description'];

    return $contents;
}

/**
 * Create a bounced log campaign entry
 *
 * @param array $row
 * @param Email $email
 * @param string $email_description
 * @return string
 */
function createBouncedCampaignLogEntry($row,$email, $email_description)
{
    $GLOBALS['log']->debug("Creating bounced email campaign log");
    $bounce = new CampaignLog();
    $bounce->campaign_id=$row['campaign_id'];
    $bounce->target_tracker_key=$row['target_tracker_key'];
    $bounce->target_id= $row['target_id'];
    $bounce->target_type=$row['target_type'];
    $bounce->list_id=$row['list_id'];
    $bounce->marketing_id=$row['marketing_id'];

    $bounce->activity_date=$email->date_created;
    $bounce->related_type='Emails';
    $bounce->related_id= $email->id;

    //do we have the phrase permanent error in the email body.
    if (preg_match('/permanent[ ]*error/',$email_description))
    {
        $bounce->activity_type='invalid email';
        markEmailAddressInvalid($email);
    }
    else 
        $bounce->activity_type='send error';
        
    $return_id=$bounce->save();
    return $return_id;
}

/**
 * Given an email address, mark it as invalid.
 *
 * @param $email_address
 */
function markEmailAddressInvalid($email_address)
{
    if(empty($email_address))
        return;
    $sea = new SugarEmailAddress();
    $rs = $sea->retrieve_by_string_fields( array('email_address_caps' => trim(strtoupper($email_address))) );
    if($rs != null)
    {
        $sea->AddUpdateEmailAddress($email_address, 1, 0, $rs->id);
    }
}

/**
 * Get the existing campaign log entry by tracker key.
 * 
 * @param string Target Key
 * @return array Campaign Log Row
 */
function getExistingCampaignLogEntry($identifier)
{
    $row = FALSE;
    $targeted = new CampaignLog();
    $where="campaign_log.activity_type='targeted' and campaign_log.target_tracker_key='{$identifier}'";
    $query=$targeted->create_new_list_query('',$where);
    $result=$targeted->db->query($query);
    $row=$targeted->db->fetchByAssoc($result);
    
    return $row;
}

/**
 * Scan the bounced email searching for a valid target identifier.
 * 
 * @param string Email Description
 * @return array Results including matches and identifier
 */
function checkBouncedEmailForIdentifier($email_description)
{
    $matches = array();
    $identifiers = array();
    $found = FALSE;
    //Check if the identifier is present in the header.
    if(preg_match('/X-CampTrackID: [a-z0-9\-]*/',$email_description,$matches)) 
    {
        $identifiers = preg_split('/X-CampTrackID: /',$matches[0],-1,PREG_SPLIT_NO_EMPTY);
        $found = TRUE;
        $GLOBALS['log']->debug("Found campaign identifier in header of email");  
    }
    else if( preg_match('/index.php\?entryPoint=removeme&identifier=[a-z0-9\-]*/',$email_description, $matches) )
    {
        $identifiers = preg_split('/index.php\?entryPoint=removeme&identifier=/',$matches[0],-1,PREG_SPLIT_NO_EMPTY);
        $found = TRUE;
        $GLOBALS['log']->debug("Found campaign identifier in body of email");
    }
    
    return array('found' => $found, 'matches' => $matches, 'identifiers' => $identifiers);
}

function campaign_process_bounced_emails(&$email, &$email_header) 
{
	global $sugar_config;
	$emailFromAddress = $email_header->fromaddress;
	$email_description = $email->raw_source;
    	
	//if raw_source is empty, try using the description instead
    	if (empty($email_description)){
        	$email_description = $email->description;
	}

    $email_description .= retrieveErrorReportAttachment($email);

	if (preg_match('/MAILER-DAEMON|POSTMASTER/i',$emailFromAddress)) 
	{
	    $email_description=quoted_printable_decode($email_description);
		$matches=array();
		
		//do we have the identifier tag in the email?
		$identifierScanResults = checkBouncedEmailForIdentifier($email_description);
		
		if ( $identifierScanResults['found'] ) 
		{
			$matches = $identifierScanResults['matches'];
			$identifiers = $identifierScanResults['identifiers'];

			if (!empty($identifiers)) 
			{
				//array should have only one element in it.
				$identifier = trim($identifiers[0]);
				$row = getExistingCampaignLogEntry($identifier);
				
				//Found entry
				if (!empty($row)) 
				{
					//do not create another campaign_log record is we already have an
					//invalid email or send error entry for this tracker key.
					$query_log = "select * from campaign_log where target_tracker_key='{$row['target_tracker_key']}'"; 
					$query_log .=" and (activity_type='invalid email' or activity_type='send error')";
                    $targeted = new CampaignLog();
					$result_log=$targeted->db->query($query_log);
					$row_log=$targeted->db->fetchByAssoc($result_log);

					if (empty($row_log)) 
					{
						$return_id = createBouncedCampaignLogEntry($row, $email, $email_description);	
						return TRUE;
					}				
					else 
					{
					    $GLOBALS['log']->debug("Warning: campaign log entry already exists for identifier $identifier");
					    return FALSE;
					}
				} 
				else 
				{
				    $GLOBALS['log']->info("Warning: skipping bounced email with this tracker_key(identifier) in the message body: ".$identifier);
					return FALSE;
				}			
    		} 
    		else 
    		{
    			$GLOBALS['log']->info("Warning: Empty identifier for campaign log.");
    			return FALSE;
    		}
    	}  
    	else 
    	{
    	    $GLOBALS['log']->info("Warning: skipping bounced email because it does not have the removeme link.");	
    		return FALSE;	
      	}
  } 
  else 
  {
	$GLOBALS['log']->info("Warning: skipping bounced email because the sender is not MAILER-DAEMON.");
	return FALSE;
  }
}
?>


//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/


/*********************************************************************************

* Description: Bug 40166. Need for return right join for campaign's target list relations.
* All Rights Reserved.
* Contributor(s): ______________________________________..
********************************************************************************/

require_once('data/Link2.php');

/**
 * @brief Bug #40166. Campaign Log Report will not display Contact/Account Names
 */
class ProspectLink extends Link2
{

    /**
     * This method changes join of any item to campaign through target list
     * if you want to use this join method you should add code below to your vardef.php
     * 'link_class' => 'ProspectLink',
     * 'link_file' => 'modules/Campaigns/ProspectLink.php'
     *
     * @see Link::getJoin method
     */
    public function getJoin($params, $return_array = false)
    {
        $join_type= ' INNER JOIN ';
        if (isset($params['join_type']))
        {
            $join_type = $params['join_type'];
        }
        $join = '';
        $bean_is_lhs=$this->_get_bean_position();

        if (
            $this->_relationship->relationship_type == 'one-to-many'
            && $bean_is_lhs
        )
        {
            $table_with_alias = $table = $this->_relationship->rhs_table;
            $key = $this->_relationship->rhs_key;
            $module = $this->_relationship->rhs_module;
            $other_table = (empty($params['left_join_table_alias']) ? $this->_relationship->lhs_table : $params['left_join_table_alias']);
            $other_key = $this->_relationship->lhs_key;
            $alias_prefix = $table;
            if (!empty($params['join_table_alias']))
            {
                $table_with_alias = $table. " ".$params['join_table_alias'];
                $table = $params['join_table_alias'];
                $alias_prefix = $params['join_table_alias'];
            }

            $join .= ' '.$join_type.' prospect_list_campaigns '.$alias_prefix.'_plc ON';
            $join .= ' '.$alias_prefix.'_plc.'.$key.' = '.$other_table.'.'.$other_key."\n";

            // join list targets
            $join .= ' '.$join_type.' prospect_lists_prospects '.$alias_prefix.'_plp ON';
            $join .= ' '.$alias_prefix.'_plp.prospect_list_id = '.$alias_prefix.'_plc.prospect_list_id AND';
            $join .= ' '.$alias_prefix.'_plp.related_type = '.$GLOBALS['db']->quoted($module)."\n";

            // join target
            $join .= ' '.$join_type.' '.$table_with_alias.' ON';
            $join .= ' '.$table.'.id = '.$alias_prefix.'_plp.related_id AND';
            $join .= ' '.$table.'.deleted=0'."\n";

            if ($return_array)
            {
                $ret_arr = array();
                $ret_arr['join'] = $join;
                $ret_arr['type'] = $this->_relationship->relationship_type;
                if ($bean_is_lhs)
                {
                    $ret_arr['rel_key'] = $this->_relationship->join_key_rhs;
                }
                else
                {
                    $ret_arr['rel_key'] = $this->_relationship->join_key_lhs;
                }
                return $ret_arr;
            }
            return $join;
        } else {
            return parent::getJoin($params, $return_array);
        }
    }
}

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description: Schedules email for delivery. emailman table holds emails for delivery.
 * A cron job polls the emailman table and delivers emails when intended send date time is reached.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/





global $timedate;
global $current_user;
global $mod_strings;

$campaign = new Campaign();
$campaign->retrieve($_REQUEST['record']);
$err_messages=array();

$test=false;
if (isset($_REQUEST['mode']) && $_REQUEST['mode'] =='test') {
	$test=true;
}

//this is to account for the case of sending directly from summary page in wizards
$from_wiz =false;
if (isset($_REQUEST['wiz_mass'])) {
    $mass[] = $_REQUEST['wiz_mass'];
    $_POST['mass'] = $mass;
    $from_wiz =true;
}
if (isset($_REQUEST['from_wiz'])) {
    $from_wiz =true;
}

//if campaign status is 'sending' disallow this step.
if (!empty($campaign->status) && $campaign->status == 'sending') {
	$err_messages[]=$mod_strings['ERR_SENDING_NOW'];
}
$current_date = $campaign->db->now();

//start scheduling now.....
foreach ($_POST['mass'] as $message_id) {

	//fetch email marketing definition.
	if (!class_exists('EmailMarketing')) require_once('modules/EmailMarketing/EmailMarketing.php');


	$marketing = new EmailMarketing();
	$marketing->retrieve($message_id);

	//make sure that the marketing message has a mailbox.
	//
	if (empty($marketing->inbound_email_id)) {

		echo "<p>";
		echo "<h4>{$mod_strings['ERR_NO_MAILBOX']}</h4>";
		echo "<BR><a href='index.php?module=EmailMarketing&action=EditView&record={$marketing->id}'>$marketing->name</a>";
		echo "</p>";
		sugar_die('');
	}


	global $timedate;
	$mergedvalue=$timedate->merge_date_time($marketing->date_start,$marketing->time_start);
	if($test) {
	    $send_date_time = $timedate->getNow()->get("-60 seconds")->asDb();
	} else {
	    $send_date_time = $timedate->to_db($mergedvalue);
	}
    $send_date_time = $campaign->db->convert($campaign->db->quoted($send_date_time), "datetime");

	//find all prospect lists associated with this email marketing message.
	if ($marketing->all_prospect_lists == 1) {
		$query="SELECT prospect_lists.id prospect_list_id from prospect_lists ";
		$query.=" INNER JOIN prospect_list_campaigns plc ON plc.prospect_list_id = prospect_lists.id";
		$query.=" WHERE plc.campaign_id='{$campaign->id}'";
		$query.=" AND prospect_lists.deleted=0";
		$query.=" AND plc.deleted=0";
		if ($test) {
			$query.=" AND prospect_lists.list_type='test'";
		} else {
			$query.=" AND prospect_lists.list_type!='test' AND prospect_lists.list_type not like 'exempt%'";
		}
	} else {
		$query="select email_marketing_prospect_lists.* FROM email_marketing_prospect_lists ";
		$query.=" inner join prospect_lists on prospect_lists.id = email_marketing_prospect_lists.prospect_list_id";
		$query.=" WHERE prospect_lists.deleted=0 and email_marketing_id = '$message_id' and email_marketing_prospect_lists.deleted=0";

		if ($test) {
			$query.=" AND prospect_lists.list_type='test'";
		} else {
			$query.=" AND prospect_lists.list_type!='test' AND prospect_lists.list_type not like 'exempt%'";
		}
	}
	$result=$campaign->db->query($query);
	while (($row=$campaign->db->fetchByAssoc($result))!=null ) {
		$prospect_list_id=$row['prospect_list_id'];

		//delete all messages for the current campaign and current email marketing message.
		$delete_emailman_query="delete from emailman where campaign_id='{$campaign->id}' and marketing_id='{$message_id}' and list_id='{$prospect_list_id}'";
		$campaign->db->query($delete_emailman_query);
        $auto = $campaign->db->getAutoIncrementSQL("emailman", "id");

		$insert_query= "INSERT INTO emailman (date_entered, user_id, campaign_id, marketing_id,list_id, related_id, related_type, send_date_time";
		$insert_query.= empty($auto)?"":",id";
		$insert_query.=')';
		$insert_query.= " SELECT $current_date,'{$current_user->id}',plc.campaign_id,'{$message_id}',plp.prospect_list_id, plp.related_id, plp.related_type,{$send_date_time}";
		$insert_query.= empty($auto)?"":",$auto";
		$insert_query.= " FROM prospect_lists_prospects plp ";
		$insert_query.= "INNER JOIN prospect_list_campaigns plc ON plc.prospect_list_id = plp.prospect_list_id ";
		$insert_query.= "WHERE plp.prospect_list_id = '{$prospect_list_id}' ";
		$insert_query.= "AND plp.deleted=0 ";
		$insert_query.= "AND plc.deleted=0 ";
		$insert_query.= "AND plc.campaign_id='{$campaign->id}'";

		$campaign->db->query($insert_query);
	}
}

//delete all entries from the emailman table that belong to the exempt list.
//TODO:SM: may want to move this to query clause above instead
if (!$test) {
    $delete_query =  "
    DELETE FROM emailman WHERE id IN (
        SELECT em.id FROM (
            SELECT emailman.id id
            FROM emailman 
            INNER JOIN prospect_lists_prospects plp
            ON emailman.related_id =  plp.related_id AND emailman.related_type =  plp.related_type
            INNER JOIN prospect_lists pl 
            ON pl.id = plp.prospect_list_id 
            INNER JOIN prospect_list_campaigns plc 
            ON plp.prospect_list_id = plc.prospect_list_id 
            WHERE plp.deleted = 0 AND plc.deleted = 0
            AND pl.deleted = 0 AND pl.list_type = 'exempt'
            AND plc.campaign_id = '{$campaign->id}') em
    )";
    $campaign->db->query($delete_query);
}

$return_module=isset($_REQUEST['return_module'])?$_REQUEST['return_module']:'Campaigns';
$return_action=isset($_REQUEST['return_action'])?$_REQUEST['return_action']:'DetailView';
$return_id=$_REQUEST['record'];

if ($test) {
	//navigate to EmailManDelivery..
	$header_URL = "Location: index.php?action=EmailManDelivery&module=EmailMan&campaign_id={$_REQUEST['record']}&return_module={$return_module}&return_action={$return_action}&return_id={$return_id}&mode=test";
    if($from_wiz){$header_URL .= "&from_wiz=true";}
} else {
	//navigate back to campaign detail view...
	$header_URL = "Location: index.php?action={$return_action}&module={$return_module}&record={$return_id}";
    if($from_wiz){$header_URL .= "&from=send";}
}
$GLOBALS['log']->debug("about to post header URL of: $header_URL");
header($header_URL);
?>

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/




require_once('modules/Campaigns/utils.php');

if (!empty($_REQUEST['remove'])) clean_string($_REQUEST['remove'], "STANDARD");
if (!empty($_REQUEST['from'])) clean_string($_REQUEST['from'], "STANDARD");

if(!empty($_REQUEST['identifier'])) {
    global $beanFiles, $beanList, $current_user;

    //user is most likely not defined, retrieve admin user so that team queries are bypassed
    if(empty($current_user) || empty($current_user->id)){
            $current_user = new User();
            $current_user->retrieve('1');
    }
    
    $keys=log_campaign_activity($_REQUEST['identifier'],'removed');
    global $current_language;
    $mod_strings = return_module_language($current_language, 'Campaigns');

    
    if (!empty($keys) && $keys['target_type'] == 'Users'){
        //Users cannot opt out of receiving emails, print out warning message.
        echo $mod_strings['LBL_USERS_CANNOT_OPTOUT'];       
     }elseif(!empty($keys) && isset($keys['campaign_id']) && !empty($keys['campaign_id'])){
        //we need to unsubscribe the user from this particular campaign
        $beantype = $beanList[$keys['target_type']];
        require_once($beanFiles[$beantype]);
        $focus = new $beantype();
        $focus->retrieve($keys['target_id']);
        unsubscribe($keys['campaign_id'], $focus); 
    
    }elseif(!empty($keys)){
		$id = $keys['target_id'];
		$module = trim($keys['target_type']);
		$class = $beanList[$module];
		require_once($beanFiles[$class]);
		$mod = new $class();
		$db = DBManagerFactory::getInstance();

		$id = $db->quote($id);

		//no opt out for users.
		if(preg_match('/^[0-9A-Za-z\-]*$/', $id) && $module != 'Users'){
            //record this activity in the campaing log table..
			$query = "UPDATE email_addresses SET email_addresses.opt_out = 1 WHERE EXISTS(SELECT 1 FROM email_addr_bean_rel ear WHERE ear.bean_id = '$id' AND ear.deleted=0 AND email_addresses.id = ear.email_address_id)";
			$status=$db->query($query);
			if($status){
				echo "*";
			}
		}
    }
		//Print Confirmation Message.
		echo $mod_strings['LBL_ELECTED_TO_OPTOUT'];
	
}
sugar_cleanup();
?>


//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/




require_once('include/DetailView/DetailView.php');
require_once('modules/Campaigns/Charts.php');


global $mod_strings;
global $app_strings;
global $app_list_strings;
global $sugar_version, $sugar_config;

$focus = new Campaign();

$detailView = new DetailView();
$offset = 0;
$offset=0;
if (isset($_REQUEST['offset']) or isset($_REQUEST['record'])) {
	$result = $detailView->processSugarBean("CAMPAIGN", $focus, $offset);
	if($result == null) {
	    sugar_die($app_strings['ERROR_NO_RECORD']);
	}
	$focus=$result;
} else {
	header("Location: index.php?module=Accounts&action=index");
}

// For all campaigns show the same ROI interface
// ..else default to legacy detail view
/*
if(!$focus->campaign_type == "NewsLetter"){
    include ('modules/Campaigns/NewsLetterTrackDetailView.php');
} else{
	
*/
    echo getClassicModuleTitle($mod_strings['LBL_MODULE_NAME'], array($mod_strings['LBL_MODULE_NAME'],$focus->name), true);
    
    $GLOBALS['log']->info("Campaign detail view");
    
	$smarty = new Sugar_Smarty();
    $smarty->assign("MOD", $mod_strings);
    $smarty->assign("APP", $app_strings);
    
    $smarty->assign("THEME", $theme);
    $smarty->assign("GRIDLINE", $gridline);
    $smarty->assign("PRINT_URL", "index.php?".$GLOBALS['request_string']);
    $smarty->assign("ID", $focus->id);
    $smarty->assign("ASSIGNED_TO", $focus->assigned_user_name);
    $smarty->assign("STATUS", $app_list_strings['campaign_status_dom'][$focus->status]);
    $smarty->assign("NAME", $focus->name);
    $smarty->assign("TYPE", $app_list_strings['campaign_type_dom'][$focus->campaign_type]);
    $smarty->assign("START_DATE", $focus->start_date);
    $smarty->assign("END_DATE", $focus->end_date);
    
    $smarty->assign("BUDGET", $focus->budget);
    $smarty->assign("ACTUAL_COST", $focus->actual_cost);
    $smarty->assign("EXPECTED_COST", $focus->expected_cost);
    $smarty->assign("EXPECTED_REVENUE", $focus->expected_revenue);
    
    
    $smarty->assign("OBJECTIVE", nl2br($focus->objective));
    $smarty->assign("CONTENT", nl2br($focus->content));
    $smarty->assign("DATE_MODIFIED", $focus->date_modified);
    $smarty->assign("DATE_ENTERED", $focus->date_entered);
    
    $smarty->assign("CREATED_BY", $focus->created_by_name);
    $smarty->assign("MODIFIED_BY", $focus->modified_by_name);
    $smarty->assign("TRACKER_URL", $sugar_config['site_url'] . '/campaign_tracker.php?track=' . $focus->tracker_key);
    $smarty->assign("TRACKER_COUNT", intval($focus->tracker_count));
    $smarty->assign("TRACKER_TEXT", $focus->tracker_text);
    $smarty->assign("REFER_URL", $focus->refer_url);
    $smarty->assign("IMPRESSIONS", $focus->impressions);
   $roi_vals = array();
   $roi_vals['budget']= $focus->budget;
   $roi_vals['actual_cost']= $focus->actual_cost;
   $roi_vals['Expected_Revenue']= $focus->expected_revenue;
   $roi_vals['Expected_Cost']= $focus->expected_cost;
   
//Query for opportunities won, clickthroughs
$campaign_id = $focus->id;
            $opp_query1  = "select camp.name, count(*) opp_count,SUM(opp.amount) as Revenue, SUM(camp.actual_cost) as Investment, 
                            ROUND((SUM(opp.amount) - SUM(camp.actual_cost))/(SUM(camp.actual_cost)), 2)*100 as ROI";	           
            $opp_query1 .= " from opportunities opp";
            $opp_query1 .= " right join campaigns camp on camp.id = opp.campaign_id";
            $opp_query1 .= " where opp.sales_stage = 'Closed Won' and camp.id='$campaign_id'";
            $opp_query1 .= " and opp.deleted=0";                                  
            $opp_query1 .= " group by camp.name";
            $opp_result1=$focus->db->query($opp_query1);              
            $opp_data1=$focus->db->fetchByAssoc($opp_result1);
      if(empty($opp_data1['opp_count'])) $opp_data1['opp_count']=0; 
      //_ppd($opp_data1);     
     $smarty->assign("OPPORTUNITIES_WON",$opp_data1['opp_count']);
          
            $camp_query1  = "select camp.name, count(*) click_thru_link";	           
            $camp_query1 .= " from campaign_log camp_log";
            $camp_query1 .= " right join campaigns camp on camp.id = camp_log.campaign_id";
            $camp_query1 .= " where camp_log.activity_type = 'link' and camp.id='$campaign_id'";
            $camp_query1 .= " group by camp.name";
            $opp_query1 .= " and deleted=0";                                  
            $camp_result1=$focus->db->query($camp_query1);              
            $camp_data1=$focus->db->fetchByAssoc($camp_result1);
            
   if(unformat_number($focus->impressions) > 0){         
    $cost_per_impression= unformat_number($focus->actual_cost)/unformat_number($focus->impressions);
   }
   else{
   	$cost_per_impression = format_number(0);
   }       
   $smarty->assign("COST_PER_IMPRESSION",currency_format_number($cost_per_impression));
   if(empty($camp_data1['click_thru_link'])) $camp_data1['click_thru_link']=0;      
   $click_thru_links = $camp_data1['click_thru_link'];
   
   if($click_thru_links >0){
    $cost_per_click_thru= unformat_number($focus->actual_cost)/unformat_number($click_thru_links);   	
   }
   else{
   	$cost_per_click_thru = format_number(0);
   } 
   $smarty->assign("COST_PER_CLICK_THROUGH",currency_format_number($cost_per_click_thru));
    
    
    	$currency  = new Currency();
    if(isset($focus->currency_id) && !empty($focus->currency_id))
    {
    	$currency->retrieve($focus->currency_id);
    	if( $currency->deleted != 1){
    		$smarty->assign("CURRENCY", $currency->iso4217 .' '.$currency->symbol );
    	}else $smarty->assign("CURRENCY", $currency->getDefaultISO4217() .' '.$currency->getDefaultCurrencySymbol() );
    }else{
    
    	$smarty->assign("CURRENCY", $currency->getDefaultISO4217() .' '.$currency->getDefaultCurrencySymbol() );
    
    }
    global $current_user;
    if(is_admin($current_user) && $_REQUEST['module'] != 'DynamicLayout' && !empty($_SESSION['editinplace'])){
    
    	$smarty->assign("ADMIN_EDIT","<a href='index.php?action=index&module=DynamicLayout&from_action=".$_REQUEST['action'] ."&from_module=".$_REQUEST['module'] ."&record=".$_REQUEST['record']. "'>".SugarThemeRegistry::current()->getImage("EditLayout","border='0' align='bottom'", null,null,'.gif',$mod_strings['LBL_EDIT_LAYOUT'])."</a>");

    }
    
    $detailView->processListNavigation($xtpl, "CAMPAIGN", $offset, $focus->is_AuditEnabled());
    // adding custom fields:
    global $xtpl;
    $xtpl = $smarty;
    require_once('modules/DynamicFields/templates/Files/DetailView.php');
    

    
    
    
    //add chart
    $seps				= array("-", "/");
    $dates				= array(date($GLOBALS['timedate']->dbDayFormat), $GLOBALS['timedate']->dbDayFormat);
    $dateFileNameSafe	= str_replace($seps, "_", $dates);
    //$cache_file_name	= $current_user->getUserPrivGuid()."_campaign_response_by_activity_type_".$dateFileNameSafe[0]."_".$dateFileNameSafe[1].".xml";
    $cache_file_name_roi	= $current_user->getUserPrivGuid()."_campaign_response_by_roi_".$dateFileNameSafe[0]."_".$dateFileNameSafe[1].".xml";
    $chart= new campaign_charts();
    //_ppd($roi_vals);
    $smarty->assign("MY_CHART_ROI", $chart->campaign_response_roi($app_list_strings['roi_type_dom'],$app_list_strings['roi_type_dom'],$focus->id,true,true));    
    //end chart
    //custom chart code
    require_once('include/SugarCharts/SugarChartFactory.php');
    $sugarChart = SugarChartFactory::getInstance();
	$resources = $sugarChart->getChartResources();
	$smarty->assign('chartResources', $resources);

echo $smarty->fetch('modules/Campaigns/RoiDetailView.tpl');
?>

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/


$focus = new Campaign();

$focus->retrieve($_POST['record']);
if(!$focus->ACLAccess('Save')){
	ACLController::displayNoAccess(true);
	sugar_cleanup(true);
}
if (!empty($_POST['assigned_user_id']) && ($focus->assigned_user_id != $_POST['assigned_user_id']) && ($_POST['assigned_user_id'] != $current_user->id)) {
	$check_notify = TRUE;
}
else {
	$check_notify = FALSE;
}

require_once('include/formbase.php');
$focus = populateFromPost('', $focus);

//store preformatted dates for 2nd save
$preformat_start_date = $focus->start_date;
$preformat_end_date = $focus->end_date;
//_ppd($preformat_end_date);

$focus->save($check_notify);
$return_id = $focus->id;

$GLOBALS['log']->debug("Saved record with id of ".$return_id);


//copy compaign targets on duplicate
if( !empty($_REQUEST['duplicateSave']) &&  !empty($_REQUEST['duplicateId']) ){
	$copyFromCompaign = new Campaign();
	$copyFromCompaign->retrieve($_REQUEST['duplicateId']);
	$copyFromCompaign->load_relationship('prospectlists');

	$focus->load_relationship('prospectlists');
	$target_lists = $copyFromCompaign->prospectlists->get();
	if(count($target_lists)>0){
		foreach ($target_lists as $prospect_list_id){
			$focus->prospectlists->add($prospect_list_id);
		}
	}

	$focus->save();
}


//if type is set to newsletter then make sure there are prospect lists attached
if($focus->campaign_type =='NewsLetter'){
		//if this is a duplicate, and the "relate_to" and "relate_id" elements are not cleared out,
		//then prospect lists will get related to the original campaign on save of the prospect list, and then
		//will get related to the new newsletter campaign, meaning the same (un)subscription list will belong to
		//two campaigns, which is wrong
		if((isset($_REQUEST['duplicateSave']) && $_REQUEST['duplicateSave']) || (isset($_REQUEST['isDuplicate']) && $_REQUEST['isDuplicate']) ){
			$_REQUEST['relate_to'] = '';
			$_REQUEST['relate_id'] = '';

		}

        //add preformatted dates for 2nd save, to avoid formatting conversion errors
        $focus->start_date = $preformat_start_date ;
        $focus->end_date = $preformat_end_date ;

        $focus->load_relationship('prospectlists');
        $target_lists = $focus->prospectlists->get();
        if(count($target_lists)<1){
            global $current_user;
            global $mod_strings;
            //if no prospect lists are attached, then lets create a subscription and unsubscription
            //default prospect lists as these are required for newsletters.

             //create subscription list
             $subs = new ProspectList();
             $subs->name = $focus->name.' '.$mod_strings['LBL_SUBSCRIPTION_LIST'];
             $subs->assigned_user_id= $current_user->id;
             $subs->list_type = "default";
             $subs->save();
             $focus->prospectlists->add($subs->id);

             //create unsubscription list
             $unsubs = new ProspectList();
             $unsubs->name = $focus->name.' '.$mod_strings['LBL_UNSUBSCRIPTION_LIST'];
             $unsubs->assigned_user_id= $current_user->id;
             $unsubs->list_type = "exempt";
             $unsubs->save();
             $focus->prospectlists->add($unsubs->id);

             //create unsubscription list
             $test_subs = new ProspectList();
             $test_subs->name = $focus->name.' '.$mod_strings['LBL_TEST_LIST'];
             $test_subs->assigned_user_id= $current_user->id;
             $test_subs->list_type = "test";
             $test_subs->save();
             $focus->prospectlists->add($test_subs->id);
        }
        //save new relationships
        $focus->save();

}//finish newsletter processing

handleRedirect($focus->id, 'Campaigns');


?>


//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description: 
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
 






    $test=false;
//account for case when called from marketing wizard
if(isset($_REQUEST['return_action']) && $_REQUEST['return_action'] == 'WizardMarketing'){
    $_POST['return_module'] = $_REQUEST['return_module'];
    $_POST['return_action'] = 'TrackDetailView';
    $_POST['record'] = $_REQUEST['record'];    
}


if (isset($_REQUEST['mode']) && $_REQUEST['mode'] == 'test') {
    $test=true;
    $_POST['mode'] = 'test';
}


global $app_strings;
global $app_list_strings;
global $current_language;
global $current_user;
global $urlPrefix;
global $currentModule;

$current_module_strings = return_module_language($current_language, 'EmailMarketing');
if ($test)  {
	echo getClassicModuleTitle('Campaigns', array($current_module_strings['LBL_MODULE_SEND_TEST']), false);
} else {
	echo getClassicModuleTitle('Campaigns', array($current_module_strings['LBL_MODULE_SEND_EMAILS']), false);
}

$campaign_id = isset($_REQUEST['record']) ? $_REQUEST['record'] : false;

if (!empty($campaign_id)) {
	$campaign = new Campaign();
	$campaign->retrieve($campaign_id);
}

if ($campaign_id && isset($campaign) && $campaign->status == 'Inactive') {
	$ss = new Sugar_Smarty();

    $data = array($campaign->name);
    $ss->assign('campaignInactive', string_format(translate('LBL_CAMPAIGN_INACTIVE_SCHEDULE', 'Campaigns'), $data));

	$ss->display('modules/Campaigns/tpls/campaign-inactive.tpl');
} else {
	$focus = new EmailMarketing();
	if($campaign_id)
	{
		$where_clauses = Array();

		if(!empty($campaign_id)) array_push($where_clauses, "campaign_id = '".$GLOBALS['db']->quote($campaign_id)."'");

		$where = "";
		foreach($where_clauses as $clause)
		{
			if($where != "")
			$where .= " and ";
			$where .= $clause;
		}

		$GLOBALS['log']->info("Here is the where clause for the list view: $where");
	}

	$ListView = new ListView();
	$ListView->initNewXTemplate('modules/Campaigns/Schedule.html',$current_module_strings);

	if ($test)  {
		$ListView->xTemplateAssign("SCHEDULE_MESSAGE_HEADER",$current_module_strings['LBL_SCHEDULE_MESSAGE_TEST']);
	} else {
		$ListView->xTemplateAssign("SCHEDULE_MESSAGE_HEADER",$current_module_strings['LBL_SCHEDULE_MESSAGE_EMAILS']);
	}

	//force multi-select popup
	$ListView->process_for_popups=true;
	$ListView->multi_select_popup=true;
	//end
	$ListView->mergeduplicates = false;
	$ListView->show_export_button = false;
	$ListView->show_select_menu = false;
	$ListView->show_delete_button = false;
	$ListView->setDisplayHeaderAndFooter(false);
	$ListView->xTemplateAssign("RETURN_MODULE",$_POST['return_module']);
	$ListView->xTemplateAssign("RETURN_ACTION",$_POST['return_action']);
	$ListView->xTemplateAssign("RETURN_ID",$_POST['record']);
	$ListView->setHeaderTitle($current_module_strings['LBL_LIST_FORM_TITLE']);
	$ListView->setQuery($where, "", "date_modified desc", "EMAILMARKETING", false);

	if ($test) {
			$ListView->xTemplateAssign("MODE",$_POST['mode']);
			//finds all marketing messages that have an association with prospect list of the test.
			//this query can be siplified using sub-selects.
			$query="select distinct email_marketing.id email_marketing_id from email_marketing ";
			$query.=" inner join email_marketing_prospect_lists empl on empl.email_marketing_id = email_marketing.id ";
			$query.=" inner join prospect_lists on prospect_lists.id = empl.prospect_list_id ";
			$query.=" inner join prospect_list_campaigns plc on plc.prospect_list_id = empl.prospect_list_id ";
			$query.=" where empl.deleted=0  ";
			$query.=" and prospect_lists.deleted=0 ";
			$query.=" and prospect_lists.list_type='test' ";
			$query.=" and plc.deleted=0 ";
			$query.=" and plc.campaign_id='$campaign_id'";
			$query.=" and email_marketing.campaign_id='$campaign_id'";
			$query.=" and email_marketing.deleted=0 ";
			$query.=" and email_marketing.all_prospect_lists=0 ";

			$seed=array();

			$result=$focus->db->query($query);
			while(($row=$focus->db->fetchByAssoc($result)) != null) {

				$bean = new EmailMarketing();
				$bean->retrieve($row['email_marketing_id']);
				$bean->mode='test';	
				$seed[]=$bean;
			}
			$query=" select email_marketing.id email_marketing_id from email_marketing ";
			$query.=" WHERE email_marketing.campaign_id='$campaign_id'";
			$query.=" and email_marketing.deleted=0 ";
			$query.=" and email_marketing.all_prospect_lists=1 ";

			$result=$focus->db->query($query);
			while(($row=$focus->db->fetchByAssoc($result)) != null) {

				$bean = new EmailMarketing();
				$bean->retrieve($row['email_marketing_id']);
				$bean->mode='test';	
				$seed[]=$bean;
			}

			$ListView->processListView($seed, "main", "EMAILMARKETING");
	} else {
		$ListView->processListView($focus, "main", "EMAILMARKETING");
	}
}
?>

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/




global $gridline;
global $theme;
global $beanList;
global $beanFiles;


if(empty($_REQUEST['module']))
{
	die("'module' was not defined");
}

if(empty($_REQUEST['record']))
{
	die("'record' was not defined");
}

if(!isset($beanList[$_REQUEST['module']]))
{
	die("'".$_REQUEST['module']."' is not defined in \$beanList");
}

$subpanel = $_REQUEST['subpanel'];
$record = $_REQUEST['record'];
$module = $_REQUEST['module'];


$image_path = 'themes/'.$theme.'/images/';

if(empty($_REQUEST['inline']))
{
	insert_popup_header($theme);
}

//require_once('include/SubPanel/SubPanelDefinitions.php');
//require_once($beanFiles[$beanList[$_REQUEST['module']]]);
//$focus=new $beanList[$_REQUEST['module']];
//$focus->retrieve($record);

include('include/SubPanel/SubPanel.php');
$layout_def_key = '';
if(!empty($_REQUEST['layout_def_key'])){
	$layout_def_key = $_REQUEST['layout_def_key'];
}

$subpanel_object = new SubPanel($module, $record, $subpanel,null, $layout_def_key);

$subpanel_object->setTemplateFile('include/SubPanel/SubPanelDynamic.html');

if(!empty($_REQUEST['mkt_id']) && $_REQUEST['mkt_id'] != 'all') {// bug 32910
    $mkt_id = $_REQUEST['mkt_id'];
}

if(!empty($mkt_id)) {
    $subpanel_object->subpanel_defs->_instance_properties['function_parameters']['EMAIL_MARKETING_ID_VALUE'] = $mkt_id;
}
echo (empty($_REQUEST['inline']))?$subpanel_object->get_buttons():'' ;  

$subpanel_object->display();

if(empty($_REQUEST['inline']))
{
	insert_popup_footer($theme);

}

?>

 //<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

require_once('modules/Campaigns/utils.php');

global $mod_strings, $app_list_strings, $app_strings, $current_user, $import_bean_map;
global $import_file_name, $theme;

$focus = 0;
if(isset($_REQUEST['return_module'])){
    if($_REQUEST['return_module'] == 'Contacts'){

        $focus = new Contact();
    }
    if($_REQUEST['return_module'] == 'Leads'){

        $focus = new Lead();
    }
    if($_REQUEST['return_module'] == 'Prospects'){

        $focus = new Prospect();
    }
}

if(isset($_REQUEST['record'])) {
    $GLOBALS['log']->debug("In Subscriptions, about to retrieve record: ".$_REQUEST['record']);
    $result = $focus->retrieve($_REQUEST['record']);
    if($result == null)
    {
        sugar_die($app_strings['ERROR_NO_RECORD']);
    }
}


$this->ss->assign("MOD", $mod_strings);
$this->ss->assign("APP", $app_strings);

if(isset($_REQUEST['return_module'])) {
    $this->ss->assign("RETURN_MODULE", $_REQUEST['return_module']);
} else {
    $this->ss->assign("RETURN_MODULE", '');
}
if(isset($_REQUEST['return_id'])){
    $this->ss->assign("RETURN_ID", $_REQUEST['return_id']);
} else {
    $this->ss->assign("RETURN_ID", '');
}
if(isset($_REQUEST['return_action'])){
    $this->ss->assign("RETURN_ACTION", $_REQUEST['return_action']);
} else {
    $this->ss->assign("RETURN_ACTION", '');
}
if(isset($_REQUEST['record'])){
    $this->ss->assign("RECORD", $_REQUEST['record']);
} else {
    $this->ss->assign("RECORD", '');
}

//if subsaction has been set, then process subscriptions
if(isset($_REQUEST['subs_action'])){manageSubscriptions($focus);}

//$title = $GLOBALS['app_strings']['LBL_MANAGE_SUBSCRIPTIONS_FOR'].$focus->name;
$params = array();
$params[]  = "<a href='index.php?module={$focus->module_dir}&action=index'>{$focus->module_dir}</a>";
$params[] = "<a href='index.php?module={$focus->module_dir}&action=DetailView&record={$focus->id}'>{$focus->name}</a>";
$params[] = $mod_strings['LBL_MANAGE_SUBSCRIPTIONS_TITLE'];
$title = getClassicModuleTitle($focus->module_dir, $params, true);
$orig_vals_str = printOriginalValues($focus);
$orig_vals_array = constructDDSubscriptionList($focus);

$this->ss->assign('APP', $app_strings);
$this->ss->assign('MOD', $mod_strings);
$this->ss->assign('title',  $title);

$this->ss->assign('enabled_subs', $orig_vals_array[0]);
$this->ss->assign('disabled_subs', $orig_vals_array[1]);
$this->ss->assign('enabled_subs_string', $orig_vals_str[0]);
$this->ss->assign('disabled_subs_string', $orig_vals_str[1]);

$buttons = array(
    '<input id="save_button" title="'.$app_strings['LBL_SAVE_BUTTON_TITLE'].'" accessKey="'.$app_strings['LBL_SAVE_BUTTON_KEY'].'" class="button" onclick="save();this.form.action.value=\'Subscriptions\'; " type="submit" name="button" value="'.$app_strings['LBL_SAVE_BUTTON_LABEL'].'">',
    '<input id="cancel_button" title="'.$app_strings['LBL_CANCEL_BUTTON_TITLE'].'" accessKey="'.$app_strings['LBL_CANCEL_BUTTON_KEY'].'" class="button" onclick="this.form.action.value=\''.$this->ss->get_template_vars('RETURN_ACTION').'\'; this.form.module.value=\''.$this->ss->get_template_vars('RETURN_MODULE').'\';" type="submit" name="button" value="'.$app_strings['LBL_CANCEL_BUTTON_LABEL'].'">'
);
$this->ss->assign('BUTTONS', $buttons);
$this->ss->display('modules/Campaigns/Subscriptions.tpl');

/*
 *This function constructs Drag and Drop multiselect box of subscriptions for display in manage subscription form
*/
function constructDDSubscriptionList($focus,$classname=''){
    require_once("include/templates/TemplateDragDropChooser.php");
    global $mod_strings;
    $unsubs_arr = '';
    $subs_arr =  '';

    // Lets start by creating the subscription and unsubscription arrays
    $subscription_arrays = get_subscription_lists($focus);
    $unsubs_arr = $subscription_arrays['unsubscribed'];
    $subs_arr =  $subscription_arrays['subscribed'];

    $comb_array = array();
	$comb_array [0] = array();
	$comb_array [1] = array();

    foreach ($subs_arr as $key=>$val){
        $comb_array [0][$val] = $key;
    }


	foreach ($unsubs_arr as $key=>$val){
        $comb_array [1][$val] = $key;
    }

    return $comb_array ;

}



/*
 *This function constructs multiselect box of subscriptions for display in manage subscription form
*/
function printOriginalValues($focus){
    global $app_strings;
    $unsubs_arr = '';
    $subs_arr =  '';
    $return_arr =  '';

     // Lets start by creating the subscription and unsubscription arrays
        $subscription_arrays = get_subscription_lists($focus);
        $unsubs_arr = $subscription_arrays['unsubscribed'];
        $subs_arr =  $subscription_arrays['subscribed'];

//    ORIG_UNSUBS_VALUES
        $unsubs_vals = ' ';
        $subs_vals = ' ';
        foreach($subs_arr as $name => $id){
            $subs_vals .= ", $id";
        }
        $return_arr[]=$subs_vals;

        foreach($unsubs_arr as $name => $id){
            $unsubs_vals .= ", $id";
        }

        $return_arr[]=$unsubs_vals;

        return $return_arr;
    }


/*
 * Perform Subscription management work.  This function processes selected subscriptions and calls the
 * right methods to subscribe or unsubscribe the user
 * */

function manageSubscriptions($focus){


    //Process Subscription Lists first
    //compare current list of subscriptions to original list and see if there are any additions
    $orig_subscription_arr = array();
    $curr_subscription_arr = array();
    //build array of original subscriptions
    if(isset($_REQUEST['orig_enabled_values'])  && ! empty($_REQUEST['orig_enabled_values'])){
     $orig_subscription_arr = explode(",", $_REQUEST['orig_enabled_values']);
     $orig_subscription_arr = process_subscriptions($orig_subscription_arr);
    }

    //build array of current subscriptions
    if(isset($_REQUEST['enabled_subs'])  && ! empty($_REQUEST['enabled_subs'])){
     $curr_subscription_arr = explode(",", $_REQUEST['enabled_subs']);
     $curr_subscription_arr = process_subscriptions($curr_subscription_arr);
    }

    //compare both arrays and find differences
    $i=0;
    while($i<(count($curr_subscription_arr)/2)){
        //if current subscription existed in original subscription list, do nothing
        if(in_array($curr_subscription_arr['campaign'.$i], $orig_subscription_arr)){
            //nothing to process
        }else{
         //current subscription is new, so subscribe
            subscribe($curr_subscription_arr['campaign'.$i], $curr_subscription_arr['prospect_list'.$i], $focus);
        }
        $i = $i +1;
    }

    //Now process UnSubscription Lists first
    //compare current list of subscriptions to original list and see if there are any additions
    $orig_unsubscription_arr = array();
    $curr_unsubscription_arr = array();

    //build array of original subscriptions
    if(isset($_REQUEST['orig_disabled_values'])  && ! empty($_REQUEST['orig_disabled_values'])){
     $orig_unsubscription_arr = explode(",", $_REQUEST['orig_disabled_values']);
     $orig_unsubscription_arr = process_subscriptions($orig_unsubscription_arr);
    }

    //build array of current subscriptions
    if(isset($_REQUEST['disabled_subs'])  && ! empty($_REQUEST['disabled_subs'])){
     $curr_unsubscription_arr = explode(",", $_REQUEST['disabled_subs']);
     $curr_unsubscription_arr = process_subscriptions($curr_unsubscription_arr);
    }
    //compare both arrays and find differences
    $i=0;
    while($i<(count($curr_unsubscription_arr)/2)){
        //if current subscription existed in original subscription list, do nothing
        if(in_array($curr_unsubscription_arr['campaign'.$i], $orig_unsubscription_arr)){
            //nothing to process
        }else{
         //current subscription is new, so subscribe
            unsubscribe($curr_unsubscription_arr['campaign'.$i], $focus);
        }
        $i = $i +1;
    }

}

?>

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/





require_once('include/DetailView/DetailView.php');
require_once('modules/Campaigns/Charts.php');


global $mod_strings;
global $app_strings;
global $app_list_strings;
global $sugar_version, $sugar_config;

$focus = new Campaign();

$detailView = new DetailView();
$offset = 0;
$offset=0;
if (isset($_REQUEST['offset']) or isset($_REQUEST['record'])) {
	$result = $detailView->processSugarBean("CAMPAIGN", $focus, $offset);
	if($result == null) {
	    sugar_die($app_strings['ERROR_NO_RECORD']);
	}
	$focus=$result;
} else {
	header("Location: index.php?module=Accounts&action=index");
}

// if campaign type is set to newsletter, then include newsletter detail view..
// ..else default to legacy detail view

//    include ('modules/Campaigns/NewsLetterTrackDetailView.php');

if(isset($focus->campaign_type) && $focus->campaign_type == "NewsLetter"){
    echo getClassicModuleTitle($mod_strings['LBL_MODULE_NAME'], array($mod_strings['LBL_NEWSLETTER'],$focus->name), true);
} else{
    echo getClassicModuleTitle($mod_strings['LBL_MODULE_NAME'], array($mod_strings['LBL_MODULE_NAME'],$focus->name), true);
}

    $GLOBALS['log']->info("Campaign detail view");
    $smarty = new Sugar_Smarty();
    $smarty->assign("MOD", $mod_strings);
    $smarty->assign("APP", $app_strings);

    $smarty->assign("GRIDLINE", $gridline);
    $smarty->assign("PRINT_URL", "index.php?".$GLOBALS['request_string']);
    $smarty->assign("ID", $focus->id);
    $smarty->assign("ASSIGNED_TO", $focus->assigned_user_name);
    $smarty->assign("STATUS", $app_list_strings['campaign_status_dom'][$focus->status]);
    $smarty->assign("NAME", $focus->name);
    $smarty->assign("TYPE", $app_list_strings['campaign_type_dom'][$focus->campaign_type]);
    $smarty->assign("START_DATE", $focus->start_date);
    $smarty->assign("END_DATE", $focus->end_date);

    $smarty->assign("BUDGET", $focus->budget);
    $smarty->assign("ACTUAL_COST", $focus->actual_cost);
    $smarty->assign("EXPECTED_COST", $focus->expected_cost);
    $smarty->assign("EXPECTED_REVENUE", $focus->expected_revenue);


    $smarty->assign("OBJECTIVE", nl2br($focus->objective));
    $smarty->assign("CONTENT", nl2br($focus->content));
    $smarty->assign("DATE_MODIFIED", $focus->date_modified);
    $smarty->assign("DATE_ENTERED", $focus->date_entered);

    $smarty->assign("CREATED_BY", $focus->created_by_name);
    $smarty->assign("MODIFIED_BY", $focus->modified_by_name);
    $smarty->assign("TRACKER_URL", $sugar_config['site_url'] . '/campaign_tracker.php?track=' . $focus->tracker_key);
    $smarty->assign("TRACKER_COUNT", intval($focus->tracker_count));
    $smarty->assign("TRACKER_TEXT", $focus->tracker_text);
    $smarty->assign("REFER_URL", $focus->refer_url);

    if(isset($focus->campaign_type) && $focus->campaign_type == "Email" || $focus->campaign_type == "NewsLetter") {
        $smarty->assign("TRACK_DELETE_BUTTON","<input title=\"{$mod_strings['LBL_TRACK_DELETE_BUTTON_TITLE']}\" class=\"button\" onclick=\"this.form.module.value='Campaigns'; this.form.action.value='Delete';this.form.return_module.value='Campaigns'; this.form.return_action.value='TrackDetailView';this.form.mode.value='Test';return confirm('{$mod_strings['LBL_TRACK_DELETE_CONFIRM']}');\" type=\"submit\" name=\"button\" value=\"  {$mod_strings['LBL_TRACK_DELETE_BUTTON_LABEL']}  \">");
    }

    	$currency  = new Currency();
    if(isset($focus->currency_id) && !empty($focus->currency_id))
    {
    	$currency->retrieve($focus->currency_id);
    	if( $currency->deleted != 1){
    		$smarty->assign("CURRENCY", $currency->iso4217 .' '.$currency->symbol );
    	}else $smarty->assign("CURRENCY", $currency->getDefaultISO4217() .' '.$currency->getDefaultCurrencySymbol() );
    }else{

    	$smarty->assign("CURRENCY", $currency->getDefaultISO4217() .' '.$currency->getDefaultCurrencySymbol() );

    }
    global $current_user;
    if(is_admin($current_user) && $_REQUEST['module'] != 'DynamicLayout' && !empty($_SESSION['editinplace'])){
    	$smarty->assign("ADMIN_EDIT","<a href='index.php?action=index&module=DynamicLayout&from_action=".$_REQUEST['action'] ."&from_module=".$_REQUEST['module'] ."&record=".$_REQUEST['record']. "'>".SugarThemeRegistry::current()->getImage("EditLayout","border='0' align='bottom'",null,null,'.gif',$mod_strings['LBL_EDIT_LAYOUT'])."</a>");
    }

    global $xtpl;
    $xtpl = $smarty;

    $detailView->processListNavigation($xtpl, "CAMPAIGN", $offset, $focus->is_AuditEnabled());
    // adding custom fields:
    require_once('modules/DynamicFields/templates/Files/DetailView.php');


    //if this is a newsletter, we need to build dropdown
    $selected_marketing_id = '';
    if(isset($focus->campaign_type)){
        //we need to build the dropdown of related marketing values
        $options_str = "<select onchange= \"this.form.module.value='Campaigns';this.form.action.value='TrackDetailView'; submit()\" name='mkt_id'>";
        $latest_marketing_id = '';
        if(isset($_REQUEST['mkt_id'])) $selected_marketing_id = $_REQUEST['mkt_id'];

        $options_str .= '<option value="all">--None--</option>';
        //query for all email marketing records related to this campaign
        $latest_marketing_query = "select id, name, date_modified from email_marketing where campaign_id = '$focus->id' order by date_modified desc";

        //build string with value(s) retrieved
        $result =$focus->db->query($latest_marketing_query);
        if ($row = $focus->db->fetchByAssoc($result)){
            //first, populated the latest marketing id variable, as this
            // variable will be used to build chart and subpanels
            if($focus->campaign_type == 'NewsLetter') {
            	$latest_marketing_id = $row['id'];
            }

            //fill in first option value
            $options_str .= '<option value="'. $row['id'] .'"';
            // if the marketing id is same as selected marketing id, set this option to render as "selected"
            if (!empty($selected_marketing_id) && $selected_marketing_id == $row['id']) {
                $options_str .=' selected>'. $row['name'] .'</option>';
            // if the marketing id is empty then set this first option to render as "selected"
            }elseif(empty($selected_marketing_id) && $focus->campaign_type == 'NewsLetter'){
                $options_str .=' selected>'. $row['name'] .'</option>';
            // if the marketing is not empty, but not same as selected marketing id, then..
            //.. do not set this option to render as "selected"
            }else{
                $options_str .='>'. $row['name'] .'</option>';
            }
        }
        //process rest of records, if they exist
        while ($row = $focus->db->fetchByAssoc($result)){
            //add to list of option values
            $options_str .= '<option value="'. $row['id'] .'"';
            //if the marketing id is same as selected marketing id, then set this option to render as "selected"
            if (!empty($selected_marketing_id) && $selected_marketing_id == $row['id']) {
                $options_str .=' selected>'. $row['name'] .'</option>';
            }else{
                $options_str .=' >'. $row['name'] .'</option>';
            }
         }
         $options_str .="</select>";
        //populate the dropdown
        $smarty->assign("FILTER_LABEL", $mod_strings['LBL_FILTER_CHART_BY']);
        $smarty->assign("MKT_DROP_DOWN",$options_str);
    }
//add chart
$seps               = array("-", "/");
$dates              = array(date($GLOBALS['timedate']->dbDayFormat), $GLOBALS['timedate']->dbDayFormat);
$dateFileNameSafe   = str_replace($seps, "_", $dates);
$cache_file_name    = $current_user->getUserPrivGuid()."_campaign_response_by_activity_type_".$dateFileNameSafe[0]."_".$dateFileNameSafe[1].".xml";
$cache_file_name_roi    = $current_user->getUserPrivGuid()."_campaign_response_by_roi_".$dateFileNameSafe[0]."_".$dateFileNameSafe[1].".xml";
$chart= new campaign_charts();

    //if marketing id has been selected, then set "latest_marketing_id" to the selected value
    //latest marketing id will be passed in to filter the charts and subpanels

    if(!empty($selected_marketing_id)){$latest_marketing_id = $selected_marketing_id;}
    if(empty($latest_marketing_id) ||  $latest_marketing_id === 'all'){
        $smarty->assign("MY_CHART", $chart->campaign_response_by_activity_type($app_list_strings['campainglog_activity_type_dom'],$app_list_strings['campainglog_target_type_dom'],$focus->id,sugar_cached("xml/$cache_file_name"),true));
    }else{
        $smarty->assign("MY_CHART", $chart->campaign_response_by_activity_type($app_list_strings['campainglog_activity_type_dom'],$app_list_strings['campainglog_target_type_dom'],$focus->id,sugar_cached("xml/$cache_file_name"),true,$latest_marketing_id));
    }

//end chart
//custom chart code
    require_once('include/SugarCharts/SugarChartFactory.php');
    $sugarChart = SugarChartFactory::getInstance();
	$resources = $sugarChart->getChartResources();
	$smarty->assign('chartResources', $resources);

echo $smarty->fetch('modules/Campaigns/TrackDetailView.tpl');

require_once('include/SubPanel/SubPanelTiles.php');
$subpanel = new SubPanelTiles($focus, 'Campaigns');
    //if latest marketing id is empty, or if it is set to 'all'', then do no filtering, otherwise filter..
    //.. out the chart and subpanels by marketing id
    if(empty($latest_marketing_id) || $latest_marketing_id === 'all'){
        //do nothing, no filtering is needed
    }else{

        // assign selected marketing ID back to request in order to let ListView use it as a part of subpanel base URL
        $_GET['mkt_id'] = $latest_marketing_id;

        //get array of layout defs
        $layoutDefsArr= $subpanel->subpanel_definitions->layout_defs;

        //iterate through layout defs for processing of subpanels.  If a marketing Id is specified, then we need to...
        //.. filter the subpanels by it so they match the chart rendered in code above.
        foreach($layoutDefsArr as $subpanels_name => $subpanels){

            //process each subpanel definition
             foreach($subpanels as $subpane_key => $subpane){

                    //see if "function_parameters" key exists in subpanel properties array
                      if (isset($subpane['function_parameters'])){
                          //if a function_parameters property key exists, then process further
                          $functionParamsArr = $subpane['function_parameters'];//$panelProperty;

                            //Check the array of function parameters and see if
                            //one exists for market value id.
                            if (isset($functionParamsArr['EMAIL_MARKETING_ID_VALUE'])){
                                //We found the property, lets fill in the marketing id value...
                                //.. into the subpanel object, using the keys of the array that..
                                //.. we used to get to thi property
                                $subpanel->subpanel_definitions->layout_defs[$subpanels_name][$subpane_key]['function_parameters']['EMAIL_MARKETING_ID_VALUE'] = $latest_marketing_id;
                            }
                        }//end if (isset($subpane['function_parameters'])){
            }//end foreach($subpanels as $subpane_key => $subpane){

        }//_pp($subpanel->subpanel_definitions->layout_defs);
    }//end else

$deletedCampaignLogLeadsCount = $focus->getDeletedCampaignLogLeadsCount();
if ($deletedCampaignLogLeadsCount > 0)
{
    $subpanel->subpanel_definitions->layout_defs['subpanel_setup']['lead']['top_buttons'][] = array(
        'widget_class' => 'SubPanelTopMessage',
        'message' => string_format($mod_strings['LBL_LEADS_DELETED_SINCE_CREATED'], array($deletedCampaignLogLeadsCount)),
    );
}

$alltabs=$subpanel->subpanel_definitions->get_available_tabs();
if (!empty($alltabs)) {

    foreach ($alltabs as $name) {
        if ($name == 'prospectlists' || $name=='emailmarketing' || $name == 'tracked_urls') {
            $subpanel->subpanel_definitions->exclude_tab($name);
        }
    }
}
echo $subpanel->display();
?>

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  Defines the English language pack for the base application.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

// logic will be added here at a later date to track campaigns
// this script; currently forwards to site_URL variable of $sugar_config
// redirect URL will also be added so specified redirect URL can be used

// additionally, another script using fopen will be used to call this
// script externally

require_once('modules/Campaigns/utils.php');

$GLOBALS['log'] = LoggerManager::getLogger('Campaign Tracker v2');

$db = DBManagerFactory::getInstance();

if(empty($_REQUEST['track'])) {
	$track = "";
} else {
	$track = $_REQUEST['track'];
}
if(!empty($_REQUEST['identifier'])) {
	$keys=log_campaign_activity($_REQUEST['identifier'],'link',true,$track);
    
}else{
    //if this has no identifier, then this is a web/banner campaign
    //pass in with id set to string 'BANNER'
    $keys=log_campaign_activity('BANNER','link',true,$track);

}

$track = $db->quote($track);

if(preg_match('/^[0-9A-Za-z\-]*$/', $track))
{
	$query = "SELECT tracker_url FROM campaign_trkrs WHERE id='$track'";
	$res = $db->query($query);

	$row = $db->fetchByAssoc($res);

	$redirect_URL = $row['tracker_url'];
	sugar_cleanup();
	header("Location: $redirect_URL");
}
else
{
	sugar_cleanup();
}
exit;
?>


//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  Defines the English language pack for the base application.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

/*
 *returns a list of objects a message can be scoped by, the list contacts the current campaign
 *name and list of all prospects associated with this campaign..
 *
 */
function get_message_scope_dom($campaign_id, $campaign_name,$db=null, $mod_strings=array()) {

    //find prospect list attached to this campaign..
    $query =  "SELECT prospect_list_id, prospect_lists.name ";
    $query .= "FROM prospect_list_campaigns ";
    $query .= "INNER join prospect_lists on prospect_lists.id = prospect_list_campaigns.prospect_list_id ";
    $query .= "WHERE prospect_lists.deleted = 0 ";
    $query .= "AND prospect_list_campaigns.deleted=0 ";
    $query .= "AND campaign_id='".$campaign_id."'";
    $query.=" and prospect_lists.list_type not like 'exempt%'";

    if (empty($db)) {
        $db = DBManagerFactory::getInstance();
    }
    if (empty($mod_strings) or !isset($mod_strings['LBL_DEFAULT'])) {
        global $current_language;
        $mod_strings = return_module_language($current_language, 'Campaigns');
    }

    //add campaign to the result array.
    //$return_array[$campaign_id]= $campaign_name . ' (' . $mod_strings['LBL_DEFAULT'] . ')';

    $result=$db->query($query);
    while(($row=$db->fetchByAssoc($result))!= null) {
        $return_array[$row['prospect_list_id']]=$row['name'];
    }
    if (empty($return_array)) $return_array=array();
    else return $return_array;
}
/**
 * Return bounce handling mailboxes for campaign.
 *
 * @param unknown_type $emails
 * @param unknown_type $get_box_name, Set it to false if want to get "From Name" other than the InboundEmail Name.
 * @return $get_name=true, bounce handling mailboxes' name; $get_name=false, bounce handling mailboxes' from name.
 */
function get_campaign_mailboxes(&$emails, $get_name=true) {
    if (!class_exists('InboundEmail')) {
        require('modules/InboundEmail/InboundEmail.php');
    }
    $query =  "select id,name,stored_options from inbound_email where mailbox_type='bounce' and status='Active' and deleted='0'";
    $db = DBManagerFactory::getInstance();
    $result=$db->query($query);
    while(($row=$db->fetchByAssoc($result))!= null) {
    	if($get_name) {
    		$return_array[$row['id']] = $row['name'];
    	} else {
        	$return_array[$row['id']]= InboundEmail::get_stored_options('from_name',$row['name'],$row['stored_options']);
    	}
        $emails[$row['id']]=InboundEmail::get_stored_options('from_addr','nobody@example.com',$row['stored_options']);
    }

    if (empty($return_array)) $return_array=array(''=>'');
    return $return_array;

}

function get_campaign_mailboxes_with_stored_options() {
	$ret = array();

    if(!class_exists('InboundEmail')) {
        require('modules/InboundEmail/InboundEmail.php');
    }

    $q = "SELECT id, name, stored_options FROM inbound_email WHERE mailbox_type='bounce' AND status='Active' AND deleted='0'";

    $db = DBManagerFactory::getInstance();

    $r = $db->query($q);

    while($a = $db->fetchByAssoc($r)) {
        $ret[$a['id']] = unserialize(base64_decode($a['stored_options']));
    }
	return $ret;
}

function log_campaign_activity($identifier, $activity, $update=true, $clicked_url_key=null) {

    $return_array = array();

    $db = DBManagerFactory::getInstance();



     //check to see if the identifier has been replaced with Banner string
    if($identifier == 'BANNER' && isset($clicked_url_key)  && !empty($clicked_url_key))
    {
        // create md5 encrypted string using the client ip, this will be used for tracker id purposes
        $enc_id = 'BNR'.md5($_SERVER['REMOTE_ADDR']);

        //default the identifier to ip address
        $identifier = $enc_id;

        //if user has chosen to not use this mode of id generation, then replace identifier with plain guid.
        //difference is that guid will generate a new campaign log for EACH CLICK!!
        //encrypted generation will generate 1 campaign log and update the hit counter for each click
        if(isset($sugar_config['campaign_banner_id_generation'])  && $sugar_config['campaign_banner_id_generation'] != 'md5'){
            $identifier = create_guid();
        }

        //retrieve campaign log.
        $trkr_query = "select * from campaign_log where target_tracker_key='$identifier' and related_id = '$clicked_url_key'";
        $current_trkr=$db->query($trkr_query);
        $row=$db->fetchByAssoc($current_trkr);

        //if campaign log is not retrieved (this is a new ip address or we have chosen to create
        //unique entries for each click
        if($row==null  || empty($row)){


                //retrieve campaign id
                $trkr_query = "select ct.campaign_id from campaign_trkrs ct, campaigns c where c.id = ct.campaign_id and ct.id = '$clicked_url_key'";
                $current_trkr=$db->query($trkr_query);
                $row=$db->fetchByAssoc($current_trkr);


                //create new campaign log with minimal info.  Note that we are creating new unique id
                //as target id, since we do not link banner/web campaigns to any users

                $data['target_id']="'" . create_guid() . "'";
                $data['target_type']= "'Prospects'";
                $data['id']="'" . create_guid() . "'";
                $data['campaign_id']="'" . $row['campaign_id'] . "'";
                $data['target_tracker_key']="'" . $identifier . "'";
                $data['activity_type']="'" .  $activity . "'";
                $data['activity_date']="'" . TimeDate::getInstance()->nowDb() . "'";
                $data['hits']=1;
                $data['deleted']=0;
                if (!empty($clicked_url_key)) {
                    $data['related_id']="'".$clicked_url_key."'";
                    $data['related_type']="'".'CampaignTrackers'."'";
                }

                //values for return array..
                $return_array['target_id']=$data['target_id'];
                $return_array['target_type']=$data['target_type'];

                //create insert query for new campaign log
                $insert_query="INSERT into campaign_log (" . implode(",",array_keys($data)) . ")";
                $insert_query.=" VALUES  (" . implode(",",array_values($data)) . ")";
                $db->query($insert_query);
            }else{

                //campaign log already exists, so just set the return array and update hits column
                $return_array['target_id']= $row['target_id'];
                $return_array['target_type']= $row['target_type'];
                $query1="update campaign_log set hits=hits+1 where id='{$row['id']}'";
                $current=$db->query($query1);


           }

        //return array and exit
        return $return_array;

    }



    $query1="select * from campaign_log where target_tracker_key='$identifier' and activity_type='$activity'";
    if (!empty($clicked_url_key)) {
        $query1.=" AND related_id='$clicked_url_key'";
    }
    $current=$db->query($query1);
    $row=$db->fetchByAssoc($current);

        if ($row==null) {
            $query="select * from campaign_log where target_tracker_key='$identifier' and activity_type='targeted'";
            $targeted=$db->query($query);
            $row=$db->fetchByAssoc($targeted);

            //if activity is removed and target type is users, then a user is trying to opt out
            //of emails.  This is not possible as Users Table does not have opt out column.
            if ($row  && (strtolower($row['target_type']) == 'users' &&  $activity == 'removed' )) {
                $return_array['target_id']= $row['target_id'];
                $return_array['target_type']= $row['target_type'];
                return $return_array;
            }
            elseif ($row){
                $data['id']="'" . create_guid() . "'";
                $data['campaign_id']="'" . $row['campaign_id'] . "'";
                $data['target_tracker_key']="'" . $identifier . "'";
                $data['target_id']="'" .  $row['target_id'] . "'";
                $data['target_type']="'" .  $row['target_type'] . "'";
                $data['activity_type']="'" .  $activity . "'";
                $data['activity_date']="'" . TimeDate::getInstance()->nowDb() . "'";
                $data['list_id']="'" .  $row['list_id'] . "'";
                $data['marketing_id']="'" .  $row['marketing_id'] . "'";
                $data['hits']=1;
                $data['deleted']=0;
                if (!empty($clicked_url_key)) {
                    $data['related_id']="'".$clicked_url_key."'";
                    $data['related_type']="'".'CampaignTrackers'."'";
                }
                //values for return array..
                $return_array['target_id']=$row['target_id'];
                $return_array['target_type']=$row['target_type'];
                $insert_query="INSERT into campaign_log (" . implode(",",array_keys($data)) . ")";
                $insert_query.=" VALUES  (" . implode(",",array_values($data)) . ")";
                $db->query($insert_query);
            }
        } else {

            $return_array['target_id']= $row['target_id'];
            $return_array['target_type']= $row['target_type'];

            $query1="update campaign_log set hits=hits+1 where id='{$row['id']}'";
            $current=$db->query($query1);

        }
        //check to see if this is a removal action
        if ($row  && $activity == 'removed' ) {
            //retrieve campaign and check it's type, we are looking for newsletter Campaigns
            $query = "SELECT campaigns.* FROM campaigns WHERE campaigns.id = '".$row['campaign_id']."' ";
            $result = $db->query($query);
            if(!empty($result))
            {
                $c_row = $db->fetchByAssoc($result);

                //if type is newsletter, then add campaign id to return_array for further processing.
                if(isset($c_row['campaign_type']) && $c_row['campaign_type'] == 'NewsLetter'){
                    $return_array['campaign_id']=$c_row['id'];
                }
            }
        }
        return $return_array;
    }


 /**
     *
     * This method is deprecated
     * @deprecated 62_Joneses - June 24, 2011
     * @see campaign_log_lead_or_contact_entry()
     */
function campaign_log_lead_entry($campaign_id, $parent_bean,$child_bean,$activity_type){
    campaign_log_lead_or_contact_entry($campaign_id, $parent_bean,$child_bean,$activity_type);
}


function campaign_log_lead_or_contact_entry($campaign_id, $parent_bean,$child_bean,$activity_type){
    global $timedate;

    //create campaign tracker id and retrieve related bio bean
     $tracker_id = create_guid();
    //create new campaign log record.
    $campaign_log = new CampaignLog();
    $campaign_log->campaign_id = $campaign_id;
    $campaign_log->target_tracker_key = $tracker_id;
    $campaign_log->related_id = $parent_bean->id;
    $campaign_log->related_type = $parent_bean->module_dir;
    $campaign_log->target_id = $child_bean->id;
    $campaign_log->target_type = $child_bean->module_dir;
    $campaign_log->activity_date = $timedate->now();
    $campaign_log->activity_type = $activity_type;
    //save the campaign log entry
    $campaign_log->save();
}


function get_campaign_urls($campaign_id) {
    $return_array=array();

    if (!empty($campaign_id)) {

        $db = DBManagerFactory::getInstance();

        $query1="select * from campaign_trkrs where campaign_id='$campaign_id' and deleted=0";
        $current=$db->query($query1);
        while (($row=$db->fetchByAssoc($current)) != null) {
            $return_array['{'.$row['tracker_name'].'}']=$row['tracker_name'] . ' : ' . $row['tracker_url'];
        }
    }
    return $return_array;
}

/**
 * Queries for the list
 */
function get_subscription_lists_query($focus, $additional_fields = null) {
    //get all prospect lists belonging to Campaigns of type newsletter
    $all_news_type_pl_query = "select c.name, pl.list_type, plc.campaign_id, plc.prospect_list_id";
    if(is_array($additional_fields) && !empty($additional_fields)) $all_news_type_pl_query .= ', ' . implode(', ', $additional_fields);
    $all_news_type_pl_query .= " from prospect_list_campaigns plc , prospect_lists pl, campaigns c ";


	$all_news_type_pl_query .= "where plc.campaign_id = c.id ";
    $all_news_type_pl_query .= "and plc.prospect_list_id = pl.id ";
    $all_news_type_pl_query .= "and c.campaign_type = 'NewsLetter'  and pl.deleted = 0 and c.deleted=0 and plc.deleted=0 ";
    $all_news_type_pl_query .= "and (pl.list_type like 'exempt%' or pl.list_type ='default') ";

    $all_news_type_list =$focus->db->query($all_news_type_pl_query);

    //build array of all newsletter campaigns
    $news_type_list_arr = array();
    while ($row = $focus->db->fetchByAssoc($all_news_type_list)){$news_type_list_arr[] = $row;}

    //now get all the campaigns that the current user is assigned to
    $all_plp_current = "select prospect_list_id from prospect_lists_prospects where related_id = '$focus->id' and deleted = 0 ";

    //build array of prospect lists that this user belongs to
    $current_plp =$focus->db->query($all_plp_current );
    $current_plp_arr = array();
    while ($row = $focus->db->fetchByAssoc($current_plp)){$current_plp_arr[] = $row;}

    return array('current_plp_arr' => $current_plp_arr, 'news_type_list_arr' => $news_type_list_arr);
}
/*
 * This function takes in a bean from a lead, prospect, or contact and returns an array containing
 * all subscription lists that the bean is a part of, and all the subscriptions that the bean is not
 * a part of.  The array elements have the key names of "subscribed" and "unsusbscribed".  These elements contain an array
 * of the corresponding list.  In other words, the "subscribed" element holds another array that holds the subscription information.
 *
 * The subscription information is a concatenated string that holds the prospect list id and the campaign id, separated by at "@" character.
 * To parse these information string into something more usable, use the "process subscriptions()" function
 *
 * */
function get_subscription_lists($focus, $descriptions = false) {
    $subs_arr = array();
    $unsubs_arr = array();

    $results = get_subscription_lists_query($focus, $descriptions);

    $news_type_list_arr = $results['news_type_list_arr'];
    $current_plp_arr = $results['current_plp_arr'];

   //For each  prospect list of type 'NewsLetter', check to see if current user is already in list,
    foreach($news_type_list_arr as $news_list){
        $match = 'false';

        //perform this check against each prospect list this user belongs to
        foreach($current_plp_arr as $current_list_key => $current_list){//echo " new entry from current lists user is subscribed to-------------";
            //compare current user list id against newsletter id
            if ($news_list['prospect_list_id'] == $current_list['prospect_list_id']){
                //if id's match, user is subscribed to this list, check to see if this is an exempt list,
                if(strpos($news_list['list_type'],  'exempt')!== false){
                    //this is an exempt list, so process
                    if(array_key_exists($news_list['name'],$subs_arr)){
                        //first, add to unsubscribed array
                        $unsubs_arr[$news_list['name']] = $subs_arr[$news_list['name']];
                        //now remove from exempt subscription list
                        unset($subs_arr[$news_list['name']]);
                    }else{
                        //we know this is an exempt list the user belongs to, but the
                        //non exempt list has not been processed yet, so just add to exempt array
                        $unsubs_arr[$news_list['name']] = "prospect_list@".$news_list['prospect_list_id']."@campaign@".$news_list['campaign_id'];
                    }
                    $match = 'false';//although match is false, this is an exempt array, so
                    //it will not be added a second time down below
                }else{
                    //this list is not exempt, and user is subscribed, so add to subscribed array, and unset from the unsubs_arr
                    //as long as this list is not in exempt array
                	$temp = "prospect_list@".$news_list['prospect_list_id']."@campaign@".$news_list['campaign_id'];
                	if(!array_search($temp,$unsubs_arr)){
                        $subs_arr[$news_list['name']] = "prospect_list@".$news_list['prospect_list_id']."@campaign@".$news_list['campaign_id'];
                        $match = 'true';
                        unset($unsubs_arr[$news_list['name']]);
                    }
                }
            }else{
                //do nothing, there is no match
                }
        }
         //if this newsletter id never matched a user subscription..
         //..then add to available(unsubscribed) NewsLetters if list is not of type exempt
        if (($match == 'false') && (strpos($news_list['list_type'], 'exempt') === false) && (!array_key_exists($news_list['name'], $subs_arr)))
        {
            $unsubs_arr[$news_list['name']] = "prospect_list@".$news_list['prospect_list_id']."@campaign@".$news_list['campaign_id'];
        }

    }
    $return_array['unsubscribed'] = $unsubs_arr;
    $return_array['subscribed'] = $subs_arr;
    return $return_array;
}

/**
 * same function as get_subscription_lists, but with the data separated in an associated array
 */
function get_subscription_lists_keyed($focus) {
    $subs_arr = array();
    $unsubs_arr = array();

    $results = get_subscription_lists_query($focus, array('c.content', 'c.frequency'));

    $news_type_list_arr = $results['news_type_list_arr'];
    $current_plp_arr = $results['current_plp_arr'];

   //For each  prospect list of type 'NewsLetter', check to see if current user is already in list,
    foreach($news_type_list_arr as $news_list){
        $match = false;

        $news_list_data = array('prospect_list_id' => $news_list['prospect_list_id'],
                                'campaign_id'      => $news_list['campaign_id'],
                                'description'      => $news_list['content'],
                                'frequency'        => $news_list['frequency']);

        //perform this check against each prospect list this user belongs to
        foreach($current_plp_arr as $current_list_key => $current_list){//echo " new entry from current lists user is subscribed to-------------";
            //compare current user list id against newsletter id
            if ($news_list['prospect_list_id'] == $current_list['prospect_list_id']){
                //if id's match, user is subscribed to this list, check to see if this is an exempt list,

                if($news_list['list_type'] == 'exempt'){
                    //this is an exempt list, so process
                    if(array_key_exists($news_list['name'],$subs_arr)){
                        //first, add to unsubscribed array
                        $unsubs_arr[$news_list['name']] = $subs_arr[$news_list['name']];
                        //now remove from exempt subscription list
                        unset($subs_arr[$news_list['name']]);
                    }else{
                        //we know this is an exempt list the user belongs to, but the
                        //non exempt list has not been processed yet, so just add to exempt array
                        $unsubs_arr[$news_list['name']] = $news_list_data;
                    }
                    $match = false;//although match is false, this is an exempt array, so
                    //it will not be added a second time down below
                }else{
                    //this list is not exempt, and user is subscribed, so add to subscribed array
                    //as long as this list is not in exempt array
                    if(!array_key_exists($news_list['name'],$unsubs_arr)){
                        $subs_arr[$news_list['name']] = $news_list_data;
                        $match = 'true';
                    }
                }
            }else{
                //do nothing, there is no match
                }
        }
         //if this newsletter id never matched a user subscription..
         //..then add to available(unsubscribed) NewsLetters if list is not of type exempt
         if(($match == false) && ($news_list['list_type'] != 'exempt')){
            $unsubs_arr[$news_list['name']] = $news_list_data;
        }

    }

    $return_array['unsubscribed'] = $unsubs_arr;
    $return_array['subscribed'] = $subs_arr;
    return $return_array;
}



/*
 * This function will take an array of strings that have been created by the "get_subscription_lists()" method
 * and parses it into an array.  The returned array has it's key's labeled in a specific fashion.
 *
 * Each string produces a campaign and a prospect id.  The keys are appended with a number specifying the order
 * it was process in.  So an input array containing 3 strings will have the following key values:
 * "prospect_list0", "campaign0"
 * "prospect_list1", "campaign1"
 * "prospect_list2", "campaign2"
 *
 * */
function process_subscriptions($subscription_string_to_parse) {
    $subs_change = array();

    //parse through and build list of id's'.  We are retrieving the campaign_id and
    //the prospect_list id from the selected subscriptions
    $i = 0;
    foreach($subscription_string_to_parse as $subs_changes){
        $subs_changes = trim($subs_changes);
        if(!empty($subs_changes)){
            $ids_arr = explode("@", $subs_changes);
            $subs_change[$ids_arr[0].$i] = $ids_arr[1];
            $subs_change[$ids_arr[2].$i] = $ids_arr[3];
            $i = $i+1;
        }
    }
    return $subs_change;
}


    /*This function is used by the Manage Subscriptions page in order to add the user
     * to the default prospect lists of the passed in campaign
     * Takes in campaign and prospect list id's we are subscribing to.
     * It also takes in a bean of the user (lead,target,prospect) we are subscribing
     * */
    function subscribe($campaign, $prospect_list, $focus, $default_list = false) {
            $relationship = strtolower($focus->getObjectName()).'s';

            //--grab all the lists for the passed in campaign id
            $pl_qry ="select id, list_type from prospect_lists where id in (select prospect_list_id from prospect_list_campaigns ";
            $pl_qry .= "where campaign_id = '$campaign') and deleted = 0 ";
            $GLOBALS['log']->debug("In Campaigns Util: subscribe function, about to run query: ".$pl_qry );
            $pl_qry_result = $focus->db->query($pl_qry);

            //build the array of all prospect_lists
            $pl_arr = array();
            while ($row = $focus->db->fetchByAssoc($pl_qry_result)){$pl_arr[] = $row;}

            //--grab all the prospect_lists this user belongs to
            $curr_pl_qry ="select prospect_list_id, related_id  from prospect_lists_prospects ";
            $curr_pl_qry .="where related_id = '$focus->id'  and deleted = 0 ";
            $GLOBALS['log']->debug("In Campaigns Util: subscribe function, about to run query: ".$curr_pl_qry );
            $curr_pl_qry_result = $focus->db->query($curr_pl_qry);

            //build the array of all prospect lists that this current user belongs to
            $curr_pl_arr = array();
            while ($row = $focus->db->fetchByAssoc($curr_pl_qry_result)){$curr_pl_arr[] = $row;}

            //search through prospect lists for this campaign and identifiy the "unsubscription list"
            $exempt_id = '';
           foreach($pl_arr as $subscription_list){
                if(strpos($subscription_list['list_type'],  'exempt')!== false){
                    $exempt_id = $subscription_list['id'];
                }

                if($subscription_list['list_type'] == 'default' && $default_list) {
                    $prospect_list = $subscription_list['id'];
                }
           }

           //now that we have exempt (unsubscription) list id, compare against user list id's
           if(!empty($exempt_id)){
            $exempt_array['exempt_id'] = $exempt_id;

               foreach($curr_pl_arr as $curr_subscription_list){
                    if($curr_subscription_list['prospect_list_id'] == $exempt_id){
                        //--if we are in here then user is subscribing to a list in which they are exempt.
                        // we need to remove the user from this unsubscription list.
                        //Begin by retrieving unsubscription prospect list
                        $exempt_subscription_list = new ProspectList();


                        $exempt_result = $exempt_subscription_list->retrieve($exempt_id);
                        if($exempt_result == null)
                        {//error happened while retrieving this list
                            return;
                        }
                        //load realationships and delete user from unsubscription list
                        $exempt_subscription_list->load_relationship($relationship);
                        $exempt_subscription_list->$relationship->delete($exempt_id,$focus->id);

                    }
               }
           }

            //Now we need to check if user is already in subscription list
            $already_here = 'false';
            //for each list user is subscribed to, compare id's with current list id'
            foreach($curr_pl_arr as $user_list){
                if(in_array($prospect_list, $user_list)){
                    //if user already exists, then set flag to true
                    $already_here = 'true';
                }
            }
            if($already_here ==='true'){
                //do nothing, user is already subscribed
            }else{
                //user is not subscribed already, so add to subscription list
                $subscription_list = new ProspectList();
                $subs_result = $subscription_list->retrieve($prospect_list);
                if($subs_result == null)
                {//error happened while retrieving this list, iterate and continue
                    return;
                }
                //load subscription list and add this user
                $GLOBALS['log']->debug("In Campaigns Util, loading relationship: ".$relationship);
                $subscription_list->load_relationship($relationship);
                $subscription_list->$relationship->add($focus->id);
            }
}


    /*This function is used by the Manage Subscriptions page in order to add the user
     * to the exempt prospect lists of the passed in campaign
     * Takes in campaign and focus parameters.
     * */
    function unsubscribe($campaign, $focus) {
        $relationship = strtolower($focus->getObjectName()).'s';
        //--grab all the list for this campaign id
        $pl_qry ="select id, list_type from prospect_lists where id in (select prospect_list_id from prospect_list_campaigns ";
        $pl_qry .= "where campaign_id = '$campaign') and deleted = 0 ";
        $pl_qry_result = $focus->db->query($pl_qry);
        //build the array with list information
        $pl_arr = array();
        $GLOBALS['log']->debug("In Campaigns Util, about to run query: ".$pl_qry);
    	while ($row = $focus->db->fetchByAssoc($pl_qry_result)){$pl_arr[] = $row;}

        //retrieve lists that this user belongs to
        $curr_pl_qry ="select prospect_list_id, related_id  from prospect_lists_prospects ";
        $curr_pl_qry .="where related_id = '$focus->id'  and deleted = 0 ";
        $GLOBALS['log']->debug("In Campaigns Util, unsubscribe function about to run query: ".$curr_pl_qry );
        $curr_pl_qry_result = $focus->db->query($curr_pl_qry);

        //build the array with current user list information
        $curr_pl_arr = array();
        while ($row = $focus->db->fetchByAssoc($curr_pl_qry_result)){$curr_pl_arr[] = $row;}
         //check to see if user is already there in prospect list
        $already_here = 'false';
        $exempt_id = '';

        foreach($curr_pl_arr as $user_list){
        	foreach($pl_arr as $v){
        		//if list is exempt list
            	if($v['list_type'] == 'exempt'){
            		//save the exempt list id for later use
            		$exempt_id = $v['id'];
					//check to see if user is already in this exempt list
            		if(in_array($v['id'], $user_list)){
                		$already_here = 'true';
            		}

                	break 2;
            	}
        	}
        }

        //unsubscribe subscripted newsletter
        foreach($pl_arr as $subscription_list){
			//create a new instance of the prospect list
        	$exempt_list = new ProspectList();
        	$exempt_list->retrieve($subscription_list['id']);
        	$exempt_list->load_relationship($relationship);
			//if list type is default, then delete the relationship
            //if list type is exempt, then add the relationship to unsubscription list
            if($subscription_list['list_type'] == 'exempt') {
		        $exempt_list->$relationship->add($focus->id);
            }elseif($subscription_list['list_type'] == 'default' || $subscription_list['list_type'] == 'test'){
            	//if list type is default or test, then delete the relationship
            	//$exempt_list->$relationship->delete($subscription_list['id'],$focus->id);
            }

        }

        if($already_here =='true'){
            //do nothing, user is already exempted

        }else{
            //user is not exempted yet , so add to unsubscription list


            $exempt_result = $exempt_list->retrieve($exempt_id);
            if($exempt_result == null)
            {//error happened while retrieving this list
                return;
            }
            $GLOBALS['log']->debug("In Campaigns Util, loading relationship: ".$relationship);
            $exempt_list->load_relationship($relationship);
            $exempt_list->$relationship->add($focus->id);
        }

    }


    /*
     *This function will return a string to the newsletter wizard if campaign check
     *does not return 100% healthy.
     */
    function diagnose()
    {
        global $mod_strings;
        global $current_user;
        $msg = " <table class='detail view small' width='100%'><tr><td> ".$mod_strings['LNK_CAMPAIGN_DIGNOSTIC_LINK']."</td></tr>";
        //Start with email components
        //monitored mailbox section
        $focus = new Administration();
        $focus->retrieveSettings(); //retrieve all admin settings.


        //run query for mail boxes of type 'bounce'
        $email_health = 0;
        $email_components = 2;
        $mbox_qry = "select * from inbound_email where deleted ='0' and mailbox_type = 'bounce'";
        $mbox_res = $focus->db->query($mbox_qry);

        $mbox = array();
        while ($mbox_row = $focus->db->fetchByAssoc($mbox_res)){$mbox[] = $mbox_row;}
        //if the array is not empty, then set "good" message
        if(isset($mbox) && count($mbox)>0){
            //everything is ok, do nothing

        }else{
            //if array is empty, then increment health counter
            $email_health =$email_health +1;
            $msg  .=  "<tr><td ><font color='red'><b>". $mod_strings['LBL_MAILBOX_CHECK1_BAD']."</b></font></td></tr>";
        }


        if (strstr($focus->settings['notify_fromaddress'], 'example.com')){
            //if "from_address" is the default, then set "bad" message and increment health counter
            $email_health =$email_health +1;
            $msg .= "<tr><td ><font color='red'><b> ".$mod_strings['LBL_MAILBOX_CHECK2_BAD']." </b></font></td></tr>";
        }else{
            //do nothing, address has been changed
        }
        //if health counter is above 1, then show admin link
        if($email_health>0){
            if (is_admin($current_user)){
                $msg.="<tr><td ><a href='index.php?module=Campaigns&action=WizardEmailSetup";
                if(isset($_REQUEST['return_module'])){
                    $msg.="&return_module=".$_REQUEST['return_module'];
                }
                if(isset($_REQUEST['return_action'])){
                    $msg.="&return_action=".$_REQUEST['return_action'];
                }
                $msg.="'>".$mod_strings['LBL_EMAIL_SETUP_WIZ']."</a></td></tr>";
            }else{
                $msg.="<tr><td >".$mod_strings['LBL_NON_ADMIN_ERROR_MSG']."</td></tr>";

            }

        }


        // proceed with scheduler components

        //create and run the scheduler queries
        $sched_qry = "select job, name, status from schedulers where deleted = 0 and status = 'Active'";
        $sched_res = $focus->db->query($sched_qry);
        $sched_health = 0;
        $sched = array();
        $check_sched1 = 'function::runMassEmailCampaign';
        $check_sched2 = 'function::pollMonitoredInboxesForBouncedCampaignEmails';
        $sched_mes = '';
        $sched_mes_body = '';
        $scheds = array();

        while ($sched_row = $focus->db->fetchByAssoc($sched_res)){$scheds[] = $sched_row;}
        //iterate through and see which jobs were found
        foreach ($scheds as $funct){
          if( ($funct['job']==$check_sched1)  ||   ($funct['job']==$check_sched2)){
                if($funct['job']==$check_sched1){
                    $check_sched1 ="found";
                }else{
                    $check_sched2 ="found";
                }

          }
        }
        //determine if error messages need to be displayed for schedulers
        if($check_sched2 != 'found'){
            $sched_health =$sched_health +1;
            $msg.= "<tr><td><font color='red'><b>".$mod_strings['LBL_SCHEDULER_CHECK1_BAD']."</b></font></td></tr>";
        }
        if($check_sched1 != 'found'){
            $sched_health =$sched_health +1;
            $msg.= "<tr><td><font color='red'><b>".$mod_strings['LBL_SCHEDULER_CHECK2_BAD']."</b></font></td></tr>";
        }
        //if health counter is above 1, then show admin link
        if($sched_health>0){
            global $current_user;
            if (is_admin($current_user)){
                $msg.="<tr><td ><a href='index.php?module=Schedulers&action=index'>".$mod_strings['LBL_SCHEDULER_LINK']."</a></td></tr>";
            }else{
                $msg.="<tr><td >".$mod_strings['LBL_NON_ADMIN_ERROR_MSG']."</td></tr>";
            }

        }

        //determine whether message should be returned
        if(($sched_health + $email_health)>0){
            $msg  .= "</table> ";
        }else{
            $msg = '';
        }
        return $msg;
    }


/**
 * Handle campaign log entry creation for mail-merge activity. The function will be called by the soap component.
 *
 * @param String campaign_id Primary key of the campaign
 * @param array targets List of keys for entries from prospect_lists_prosects table
 */
 function campaign_log_mail_merge($campaign_id, $targets) {

    $campaign= new Campaign();
    $campaign->retrieve($campaign_id);

    if (empty($campaign->id)) {
        $GLOBALS['log']->debug('set_campaign_merge: Invalid campaign id'. $campaign_id);
    } else {
        foreach ($targets as $target_list_id) {
            $pl_query = "select * from prospect_lists_prospects where id='".$GLOBALS['db']->quote($target_list_id)."'";
            $result=$GLOBALS['db']->query($pl_query);
            $row=$GLOBALS['db']->fetchByAssoc($result);
            if (!empty($row)) {
                write_mail_merge_log_entry($campaign_id,$row);
            }
        }
    }

 }
/**
 * Function creates a campaign_log entry for campaigns processesed using the mail-merge feature. If any entry
 * exist the hit counter is updated. target_tracker_key is used to locate duplicate entries.
 * @param string campaign_id Primary key of the campaign
 * @param array $pl_row A row of data from prospect_lists_prospects table.
 */
function write_mail_merge_log_entry($campaign_id,$pl_row) {

    //Update the log entry if it exists.
    $update="update campaign_log set hits=hits+1 where campaign_id='".$GLOBALS['db']->quote($campaign_id)."' and target_tracker_key='" . $GLOBALS['db']->quote($pl_row['id']) . "'";
    $result=$GLOBALS['db']->query($update);

    //get affected row count...
    $count=$GLOBALS['db']->getAffectedRowCount();
    if ($count==0) {
        $data=array();

        $data['id']="'" . create_guid() . "'";
        $data['campaign_id']="'" . $GLOBALS['db']->quote($campaign_id) . "'";
        $data['target_tracker_key']="'" . $GLOBALS['db']->quote($pl_row['id']) . "'";
        $data['target_id']="'" .  $GLOBALS['db']->quote($pl_row['related_id']) . "'";
        $data['target_type']="'" .  $GLOBALS['db']->quote($pl_row['related_type']) . "'";
        $data['activity_type']="'targeted'";
        $data['activity_date']="'" . TimeDate::getInstance()->nowDb() . "'";
        $data['list_id']="'" .  $GLOBALS['db']->quote($pl_row['prospect_list_id']) . "'";
        $data['hits']=1;
        $data['deleted']=0;
        $insert_query="INSERT into campaign_log (" . implode(",",array_keys($data)) . ")";
        $insert_query.=" VALUES  (" . implode(",",array_values($data)) . ")";
        $GLOBALS['db']->query($insert_query);
    }
}

    function track_campaign_prospects($focus){
        $campaign_id = $GLOBALS['db']->quote($focus->id);
        $delete_query="delete from campaign_log where campaign_id='".$campaign_id."' and activity_type='targeted'";
        $focus->db->query($delete_query);

        $current_date = $focus->db->now();
        $guidSQL = $focus->db->getGuidSQL();

        $insert_query= "INSERT INTO campaign_log (id,activity_date, campaign_id, target_tracker_key,list_id, target_id, target_type, activity_type, deleted";
        $insert_query.=')';
        $insert_query.="SELECT {$guidSQL}, $current_date, plc.campaign_id,{$guidSQL},plp.prospect_list_id, plp.related_id, plp.related_type,'targeted',0 ";
        $insert_query.="FROM prospect_lists INNER JOIN prospect_lists_prospects plp ON plp.prospect_list_id = prospect_lists.id";
        $insert_query.=" INNER JOIN prospect_list_campaigns plc ON plc.prospect_list_id = prospect_lists.id";
        $insert_query.=" WHERE plc.campaign_id='".$GLOBALS['db']->quote($focus->id)."'";
        $insert_query.=" AND prospect_lists.deleted=0";
        $insert_query.=" AND plc.deleted=0";
        $insert_query.=" AND plp.deleted=0";
        $insert_query.=" AND prospect_lists.list_type!='test' AND prospect_lists.list_type not like 'exempt%'";
        $focus->db->query($insert_query);

        global $mod_strings;
        //return success message
        return $mod_strings['LBL_DEFAULT_LIST_ENTRIES_WERE_PROCESSED'];
    }

    function create_campaign_log_entry($campaign_id, $focus, $rel_name, $rel_bean, $target_id = ''){
        global $timedate;

        $target_ids = array();
        //check if this is specified for one target/contact/prospect/lead (from contact/lead detail subpanel)
        if(!empty($target_id)){
            $target_ids[] = $target_id;
        }else{
            //this is specified for all, so load target/prospect relationships (mark as sent button)
            $focus->load_relationship($rel_name);
            $target_ids = $focus->$rel_name->get();

        }
        if(count($target_ids)>0){


            //retrieve the target beans and create campaign log entry
            foreach($target_ids as $id){
                 //perform duplicate check
                 $dup_query = "select id from campaign_log where campaign_id = '$campaign_id' and target_id = '$id'";
                 $dup_result = $focus->db->query($dup_query);
                 $row = $focus->db->fetchByAssoc($dup_result);

                //process if this is not a duplicate campaign log entry
                if(empty($row)){
                    //create campaign tracker id and retrieve related bio bean
                     $tracker_id = create_guid();
                     $rel_bean->retrieve($id);

                    //create new campaign log record.
                    $campaign_log = new CampaignLog();
                    $campaign_log->campaign_id = $campaign_id;
                    $campaign_log->target_tracker_key = $tracker_id;
                    $campaign_log->target_id = $rel_bean->id;
                    $campaign_log->target_type = $rel_bean->module_dir;
                    $campaign_log->activity_type = 'targeted';
                    $campaign_log->activity_date=$timedate->now();
                    //save the campaign log entry
                    $campaign_log->save();
                }
            }
        }

    }

    /*
     * This function will return an array that has been formatted to work as a Quick Search Object for prospect lists
     */
    function getProspectListQSObjects($source = '', $return_field_name='name', $return_field_id='id' ) {
        global $app_strings;
        //if source has not been specified, then search across all prospect lists
        if(empty($source)){
            $qsProspectList = array('method' => 'query',
                                'modules'=> array('ProspectLists'),
                                'group' => 'and',
                                'field_list' => array('name', 'id'),
                                'populate_list' => array('prospect_list_name', 'prospect_list_id'),
                                'conditions' => array( array('name'=>'name','op'=>'like_custom','end'=>'%','value'=>'') ),
                                'order' => 'name',
                                'limit' => '30',
                                'no_match_text' => $app_strings['ERR_SQS_NO_MATCH']);
        }else{
             //source has been specified use it to tell quicksearch.js which html input to use to get filter value
            $qsProspectList = array('method' => 'query',
                                'modules'=> array('ProspectLists'),
                                'group' => 'and',
                                'field_list' => array('name', 'id'),
                                'populate_list' => array($return_field_name, $return_field_id),
                                'conditions' => array(
                                                    array('name'=>'name','op'=>'like_custom','end'=>'%','value'=>''),
                                                    //this condition has the source parameter defined, meaning the query will take the value specified below
                                                    array('name'=>'list_type', 'op'=>'like_custom', 'end'=>'%','value'=>'', 'source' => $source)
                                ),
                                'order' => 'name',
                                'limit' => '30',
                                'no_match_text' => $app_strings['ERR_SQS_NO_MATCH']);

        }

        return $qsProspectList;
    }


?>

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

$dictionary['Campaign'] = array ('audited'=>true,
	'comment' => 'Campaigns are a series of operations undertaken to accomplish a purpose, usually acquiring leads',
	'table' => 'campaigns',
	'unified_search' => true,
	'full_text_search' => true,
	'fields' => array (
		'tracker_key' => array (
			'name' => 'tracker_key',
			'vname' => 'LBL_TRACKER_KEY',
			'type' => 'int',
			'required' => true,
			'studio' => array('editview' => false),
			'len' => '11',
			'auto_increment' => true,
			'comment' => 'The internal ID of the tracker used in a campaign; no longer used as of 4.2 (see campaign_trkrs)'
			),
		'tracker_count' => array (
			'name' => 'tracker_count',
			'vname' => 'LBL_TRACKER_COUNT',
			'type' => 'int',
			'len' => '11',
			'default' => '0',
			'comment' => 'The number of accesses made to the tracker URL; no longer used as of 4.2 (see campaign_trkrs)'
		),
		'name' => array (
			'name' => 'name',
			'vname' => 'LBL_CAMPAIGN_NAME',
			'dbType' => 'varchar',
			'type' => 'name',
			'len' => '50',
			'comment' => 'The name of the campaign',
			'importable' => 'required',
            'required' => true,
			'unified_search' => true,
			'full_text_search' => array('boost' => 3),
			),
		'refer_url' => array (
			'name' => 'refer_url',
			'vname' => 'LBL_REFER_URL',
			'type' => 'varchar',
			'len' => '255',
			'default' => 'http://',
			'comment' => 'The URL referenced in the tracker URL; no longer used as of 4.2 (see campaign_trkrs)'
		),
		'description'=>array('name'=>'description','type'=>'none', 'comment'=>'inhertied but not used', 'source'=>'non-db'),
		'tracker_text' => array (
			'name' => 'tracker_text',
			'vname' => 'LBL_TRACKER_TEXT',
			'type' => 'varchar',
			'len' => '255',
			'comment' => 'The text that appears in the tracker URL; no longer used as of 4.2 (see campaign_trkrs)'
		),

		'start_date' => array (
			'name' => 'start_date',
			'vname' => 'LBL_CAMPAIGN_START_DATE',
			'type' => 'date',
			'audited'=>true,
			'comment' => 'Starting date of the campaign',
		    'validation' => array ('type' => 'isbefore', 'compareto' => 'end_date'),
		    'enable_range_search' => true,
		    'options' => 'date_range_search_dom',
		),
		'end_date' => array (
			'name' => 'end_date',
			'vname' => 'LBL_CAMPAIGN_END_DATE',
			'type' => 'date',
			'audited'=>true,
			'comment' => 'Ending date of the campaign',
			'importable' => 'required',
            'required' => true,
		    'enable_range_search' => true,
		    'options' => 'date_range_search_dom',
		),
		'status' => array (
			'name' => 'status',
			'vname' => 'LBL_CAMPAIGN_STATUS',
			'type' => 'enum',
			'options' => 'campaign_status_dom',
			'len' => 100,
			'audited'=>true,
			'comment' => 'Status of the campaign',
			'importable' => 'required',
            'required' => true,
		),
		  'impressions' => array (
			'name' => 'impressions',
			'vname' => 'LBL_CAMPAIGN_IMPRESSIONS',
			'type' => 'int',
			'default'=>0,
            'reportable'=>true,
			'comment' => 'Expected Click throughs manually entered by Campaign Manager'
		),
  		'currency_id' =>
  		array (
    		'name' => 'currency_id',
    		'vname' => 'LBL_CURRENCY',
    		'type' => 'id',
    		'group'=>'currency_id',
    		'function'=>array('name'=>'getCurrencyDropDown', 'returns'=>'html'),
    		'required'=>false,
    		'do_report'=>false,
    		'reportable'=>false,
    		'comment' => 'Currency in use for the campaign'
  		),
		'budget' => array (
			'name' => 'budget',
			'vname' => 'LBL_CAMPAIGN_BUDGET',
			'type' => 'currency',
			'dbType' => 'double',
			'comment' => 'Budgeted amount for the campaign'
		),
		'expected_cost' => array (
			'name' => 'expected_cost',
			'vname' => 'LBL_CAMPAIGN_EXPECTED_COST',
			'type' => 'currency',
			'dbType' => 'double',
			'comment' => 'Expected cost of the campaign'
		),
		'actual_cost' => array (
			'name' => 'actual_cost',
			'vname' => 'LBL_CAMPAIGN_ACTUAL_COST',
			'type' => 'currency',
			'dbType' => 'double',
			'comment' => 'Actual cost of the campaign'
		),
		'expected_revenue' => array (
			'name' => 'expected_revenue',
			'vname' => 'LBL_CAMPAIGN_EXPECTED_REVENUE',
			'type' => 'currency',
			'dbType' => 'double',
			'comment' => 'Expected revenue stemming from the campaign'
		),
		'campaign_type' => array (
			'name' => 'campaign_type',
			'vname' => 'LBL_CAMPAIGN_TYPE',
			'type' => 'enum',
			'options' => 'campaign_type_dom',
			'len' => 100,
			'audited'=>true,
			'comment' => 'The type of campaign',
			'importable' => 'required',
            'required' => true,
		),
		'objective' => array (
			'name' => 'objective',
			'vname' => 'LBL_CAMPAIGN_OBJECTIVE',
			'type' => 'text',
			'comment' => 'The objective of the campaign'
		),
		'content' => array (
			'name' => 'content',
			'vname' => 'LBL_CAMPAIGN_CONTENT',
			'type' => 'text',
			'comment' => 'The campaign description'
		),
		'prospectlists'=> array (
  			'name' => 'prospectlists',
    		'type' => 'link',
    		'relationship' => 'prospect_list_campaigns',
    		'source'=>'non-db',
  		),
		'emailmarketing'=> array (
  			'name' => 'emailmarketing',
    		'type' => 'link',
    		'relationship' => 'campaign_email_marketing',
    		'source'=>'non-db',
  		),
		'queueitems'=> array (
  			'name' => 'queueitems',
    		'type' => 'link',
    		'relationship' => 'campaign_emailman',
    		'source'=>'non-db',
  		),
		'log_entries'=> array (
  			'name' => 'log_entries',
    		'type' => 'link',
    		'relationship' => 'campaign_campaignlog',
    		'source'=>'non-db',
            'vname' => 'LBL_LOG_ENTRIES',
  		),
  		'tracked_urls' => array (
  			'name' => 'tracked_urls',
    		'type' => 'link',
    		'relationship' => 'campaign_campaigntrakers',
    		'source'=>'non-db',
			'vname'=>'LBL_TRACKED_URLS',
  		),
        'frequency' => array (
            'name' => 'frequency',
            'vname' => 'LBL_CAMPAIGN_FREQUENCY',
            'type' => 'enum',
            //'options' => 'campaign_status_dom',
            'len' => 100,
            'comment' => 'Frequency of the campaign',
            'options' => 'newsletter_frequency_dom',
            'len' => 100,
        ),
        'leads'=> array (
            'name' => 'leads',
            'type' => 'link',
            'relationship' => 'campaign_leads',
            'source'=>'non-db',
		    'vname' => 'LBL_LEADS',
            'link_class' => 'ProspectLink',
            'link_file' => 'modules/Campaigns/ProspectLink.php'
        ),

        'opportunities'=> array (
            'name' => 'opportunities',
            'type' => 'link',
            'relationship' => 'campaign_opportunities',
            'source'=>'non-db',
		    'vname' => 'LBL_OPPORTUNITIES',
        ),
        'contacts'=> array (
            'name' => 'contacts',
            'type' => 'link',
            'relationship' => 'campaign_contacts',
            'source'=>'non-db',
		    'vname' => 'LBL_CONTACTS',
            'link_class' => 'ProspectLink',
            'link_file' => 'modules/Campaigns/ProspectLink.php'
        ),
        'accounts'=> array (
            'name' => 'accounts',
            'type' => 'link',
            'relationship' => 'campaign_accounts',
            'source'=>'non-db',
		    'vname' => 'LBL_ACCOUNTS',
            'link_class' => 'ProspectLink',
            'link_file' => 'modules/Campaigns/ProspectLink.php'
        ),


	),
	'indices' => array (
		array (
			'name' => 'camp_auto_tracker_key' ,
			'type'=>'index' ,
			'fields'=>array('tracker_key')
		),
		array (
			'name' =>'idx_campaign_name',
			'type' =>'index',
			'fields'=>array('name')
		),
	),

	'relationships' => array (
        'campaign_accounts' => array('lhs_module'=> 'Campaigns', 'lhs_table'=> 'campaigns', 'lhs_key' => 'id',
                 'rhs_module'=> 'Accounts', 'rhs_table'=> 'accounts', 'rhs_key' => 'campaign_id',
                 'relationship_type'=>'one-to-many'),

        'campaign_contacts' => array('lhs_module'=> 'Campaigns', 'lhs_table'=> 'campaigns', 'lhs_key' => 'id',
                 'rhs_module'=> 'Contacts', 'rhs_table'=> 'contacts', 'rhs_key' => 'campaign_id',
                 'relationship_type'=>'one-to-many'),

        'campaign_leads' => array('lhs_module'=> 'Campaigns', 'lhs_table'=> 'campaigns', 'lhs_key' => 'id',
                 'rhs_module'=> 'Leads', 'rhs_table'=> 'leads', 'rhs_key' => 'campaign_id',
                 'relationship_type'=>'one-to-many'),

        'campaign_prospects' => array('lhs_module'=> 'Campaigns', 'lhs_table'=> 'campaigns', 'lhs_key' => 'id',
                 'rhs_module'=> 'Prospects', 'rhs_table'=> 'prospects', 'rhs_key' => 'campaign_id',
                 'relationship_type'=>'one-to-many'),

        'campaign_opportunities' => array('lhs_module'=> 'Campaigns', 'lhs_table'=> 'campaigns', 'lhs_key' => 'id',
                 'rhs_module'=> 'Opportunities', 'rhs_table'=> 'opportunities', 'rhs_key' => 'campaign_id',
                 'relationship_type'=>'one-to-many'),

		'campaign_email_marketing' => array('lhs_module'=> 'Campaigns', 'lhs_table'=> 'campaigns', 'lhs_key' => 'id',
				 'rhs_module'=> 'EmailMarketing', 'rhs_table'=> 'email_marketing', 'rhs_key' => 'campaign_id',
				 'relationship_type'=>'one-to-many'),

		'campaign_emailman' => array('lhs_module'=> 'Campaigns', 'lhs_table'=> 'campaigns', 'lhs_key' => 'id',
				 'rhs_module'=> 'EmailMan', 'rhs_table'=> 'emailman', 'rhs_key' => 'campaign_id',
				 'relationship_type'=>'one-to-many'),

		'campaign_campaignlog' => array('lhs_module'=> 'Campaigns', 'lhs_table'=> 'campaigns', 'lhs_key' => 'id',
				 'rhs_module'=> 'CampaignLog', 'rhs_table'=> 'campaign_log', 'rhs_key' => 'campaign_id',
				 'relationship_type'=>'one-to-many'),

        'campaign_assigned_user' => array('lhs_module'=> 'Users', 'lhs_table'=> 'users', 'lhs_key' => 'id',
                'rhs_module'=> 'Campaigns', 'rhs_table'=> 'campaigns', 'rhs_key' => 'assigned_user_id',
                'relationship_type'=>'one-to-many'),

        'campaign_modified_user' => array('lhs_module'=> 'Users', 'lhs_table'=> 'users', 'lhs_key' => 'id',
                'rhs_module'=> 'Campaigns', 'rhs_table'=> 'campaigns', 'rhs_key' => 'modified_user_id',
                'relationship_type'=>'one-to-many'),
		)
);
VardefManager::createVardef('Campaigns','Campaign', array('default', 'assignable',
));
?>

//<?php
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

require_once('include/MVC/View/SugarView.php');
require_once('include/MVC/Controller/SugarController.php');

class CampaignsViewClassic extends SugarView
{	
 	function CampaignsViewClassic()
 	{
 		parent::SugarView();
 		$this->type = $this->action;
 	}	
 	
 	/**
	 * @see SugarView::display()
	 */
	public function display()
	{
 		// Call SugarController::getActionFilename to handle case sensitive file names
 		$file = SugarController::getActionFilename($this->action);
 		if(file_exists('custom/modules/' . $this->module . '/'. $file . '.php')){
			$this->includeClassicFile('custom/modules/'. $this->module . '/'. $file . '.php');
			return true;
		}elseif(file_exists('modules/' . $this->module . '/'. $file . '.php')){
			$this->includeClassicFile('modules/'. $this->module . '/'. $file . '.php');
			return true;
		}
		return false;
 	} 	
	
    /**
	 * @see SugarView::_getModuleTitleParams()
	 */
	protected function _getModuleTitleParams($browserTitle = false)
	{
    	$params = array();
    	$params[] = $this->_getModuleTitleListParam($browserTitle);
    	if (isset($this->action)){
    		switch($_REQUEST['action']){
    				case 'WizardHome':
				    	if(!empty($this->bean->id))
				    	{
				    		$params[] = "<a href='index.php?module={$this->module}&action=DetailView&record={$this->bean->id}'>".$this->bean->name."</a>";
				    	}
				    	$params[] = $GLOBALS['mod_strings']['LBL_CAMPAIGN_WIZARD'];
				    	break;
				    case 'WebToLeadCreation':
    					$params[] = $GLOBALS['mod_strings']['LBL_LEAD_FORM_WIZARD'];
    					break;
    				case 'WizardNewsletter':
				    	if(!empty($this->bean->id))
				    	{
				    		$params[] = "<a href='index.php?module={$this->module}&action=DetailView&record={$this->bean->id}'>".$GLOBALS['mod_strings']['LBL_NEWSLETTER_TITLE']."</a>";
				    	}
				    	$params[] = $GLOBALS['mod_strings']['LBL_CREATE_NEWSLETTER'];
				    	break;
    				case 'CampaignDiagnostic':
    					$params[] = $GLOBALS['mod_strings']['LBL_CAMPAIGN_DIAGNOSTICS'];
    					break;  
    				case 'WizardEmailSetup':
    					$params[] = $GLOBALS['mod_strings']['LBL_EMAIL_SETUP_WIZARD_TITLE'];
    					break;    
    				case 'TrackDetailView':
    					if(!empty($this->bean->id))
    					{
	    					$params[] = "<a href='index.php?module={$this->module}&action=DetailView&record={$this->bean->id}'>".$this->bean->name."</a>";
    					}
	    				$params[] = $GLOBALS['mod_strings']['LBL_LIST_TO_ACTIVITY'];
    					break;			  					    					
    		}//switch
    	}//fi
 		
    	return $params;
    }
}

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description: This file is used to override the default Meta-data DetailView behavior
 * to provide customization specific to the Campaigns module.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

require_once('include/json_config.php');

require_once('include/MVC/View/views/view.detail.php');

class CampaignsViewDetail extends ViewDetail {

 	function CampaignsViewDetail(){

        parent::ViewDetail();
        //turn off normal display of subpanels
        $this->options['show_subpanels'] = false;
        
 	}
 	

    function preDisplay(){
        global $mod_strings;
        if (isset($this->bean->campaign_type) && strtolower($this->bean->campaign_type) == 'newsletter'){
            $mod_strings['LBL_MODULE_NAME'] = $mod_strings['LBL_NEWSLETTERS'];
        }
        parent::preDisplay();
        $this->options['show_subpanels'] = false;

    }        

 	function display() {
 	    global $app_list_strings; 
 	    $this->ss->assign('APP_LIST', $app_list_strings);
 	    
        if (isset($_REQUEST['mode']) && $_REQUEST['mode']=='set_target'){
            require_once('modules/Campaigns/utils.php');
            //call function to create campaign logs
            $mess = track_campaign_prospects($this->bean);
            
            $confirm_msg = "var ajax_C_LOG_Status = new SUGAR.ajaxStatusClass(); 
            window.setTimeout(\"ajax_C_LOG_Status.showStatus('".$mess."')\",1000); 
            window.setTimeout('ajax_C_LOG_Status.hideStatus()', 1500); 
            window.setTimeout(\"ajax_C_LOG_Status.showStatus('".$mess."')\",2000); 
            window.setTimeout('ajax_C_LOG_Status.hideStatus()', 5000); ";
            $this->ss->assign("MSG_SCRIPT",$confirm_msg);
            
        }         
        
	    if (($this->bean->campaign_type == 'Email') || ($this->bean->campaign_type == 'NewsLetter' )) {
	    	$this->ss->assign("ADD_BUTTON_STATE", "submit");
	        $this->ss->assign("TARGET_BUTTON_STATE", "hidden");
	    } else {
	    	$this->ss->assign("ADD_BUTTON_STATE", "hidden");
	    	$this->ss->assign("DISABLE_LINK", "display:none");
	        $this->ss->assign("TARGET_BUTTON_STATE", "submit");
	    }
	    
	    $currency = new Currency();
	    if(isset($this->bean->currency_id) && !empty($this->bean->currency_id))
	    {
	    	$currency->retrieve($this->bean->currency_id);
	    	if( $currency->deleted != 1){
	    		$this->ss->assign('CURRENCY', $currency->iso4217 .' '.$currency->symbol);
	    	}else {
	    	    $this->ss->assign('CURRENCY', $currency->getDefaultISO4217() .' '.$currency->getDefaultCurrencySymbol());	
	    	}
	    }else{
	    	$this->ss->assign('CURRENCY', $currency->getDefaultISO4217() .' '.$currency->getDefaultCurrencySymbol());
	    }

        parent::display();

        //We want to display subset of available, panels, so we will call subpanel
        //object directly instead of using sugarview.  
        $GLOBALS['focus'] = $this->bean;
        require_once('include/SubPanel/SubPanelTiles.php');
        $subpanel = new SubPanelTiles($this->bean, $this->module);
        //get available list of subpanels
        $alltabs=$subpanel->subpanel_definitions->get_available_tabs();
        if (!empty($alltabs)) {
            //iterate through list, and filter out all but 3 subpanels
            foreach ($alltabs as $key=>$name) {
                if ($name != 'prospectlists' && $name!='emailmarketing' && $name != 'tracked_urls') {
                    //exclude subpanels that are not prospectlists, emailmarketing, or tracked urls
                    $subpanel->subpanel_definitions->exclude_tab($name);
                }   
            }
            //only show email marketing subpanel for email/newsletter campaigns
            if ($this->bean->campaign_type != 'Email' && $this->bean->campaign_type != 'NewsLetter' ) {
                //exclude emailmarketing subpanel if not on an email or newsletter campaign
                $subpanel->subpanel_definitions->exclude_tab('emailmarketing');
                // Bug #49893  - 20120120 - Captivea (ybi) - Remove trackers subpanels if not on an email/newsletter campaign (useless subpannl)
                $subpanel->subpanel_definitions->exclude_tab('tracked_urls');
            }                       
        }
        //show filtered subpanel list
        echo $subpanel->display();    

    }
}
?>

//<?php
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

require_once('include/MVC/View/views/view.modulelistmenu.php');

class CampaignsViewModulelistmenu extends ViewModulelistmenu
{
 	public function display()
 	{
 	    $tracker = new Tracker();
        $history = $tracker->get_recently_viewed($GLOBALS['current_user']->id, array('Campaigns','ProspectLists','Prospects'));
        foreach ( $history as $key => $row ) {
            $history[$key]['item_summary_short'] = getTrackerSubstring($row['item_summary']);
            $history[$key]['image'] = SugarThemeRegistry::current()
                ->getImage($row['module_name'],'border="0" align="absmiddle"', null,null,'.gif',$row['item_summary']);

        }
        $this->ss->assign('LAST_VIEWED',$history);
 	    
 		$this->ss->display('include/MVC/View/tpls/modulelistmenu.tpl');
 	}
}
?>

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');

/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************
 ********************************************************************************/

require_once('include/MVC/View/views/view.list.php');

class ViewNewsLetterList extends ViewList 
{    
    function processSearchForm()
    {
        // we have a query
        if(!empty($_SERVER['HTTP_REFERER']) && preg_match('/action=EditView/', $_SERVER['HTTP_REFERER'])) { // from EditView cancel
            $this->searchForm->populateFromArray($this->storeQuery->query);
        }
        else {
            $this->searchForm->populateFromRequest();
        }   
        $where_clauses = $this->searchForm->generateSearchWhere(true, $this->seed->module_dir);
        $where_clauses[] = "campaigns.campaign_type in ('NewsLetter')";
        if (count($where_clauses) > 0 )$this->where = '('. implode(' ) AND ( ', $where_clauses) . ')';
        $GLOBALS['log']->info("List View Where Clause: $this->where");


        echo $this->searchForm->display($this->headers);
    }
    
    /**
	 * @see SugarView::preDisplay()
	 */
	public function preDisplay() 
	{
        global $mod_strings;
        $mod_strings['LBL_MODULE_TITLE'] = $mod_strings['LBL_NEWSLETTER_TITLE'];
        $mod_strings['LBL_LIST_FORM_TITLE'] = $mod_strings['LBL_NEWSLETTER_LIST_FORM_TITLE'];
        parent::preDisplay();

    }        
}

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

require_once('include/formbase.php');
require_once('modules/Leads/LeadFormBase.php');



global $app_strings, $app_list_strings, $sugar_config, $timedate, $current_user;

$mod_strings = return_module_language($sugar_config['default_language'], 'Leads');

$app_list_strings['record_type_module'] = array('Contact'=>'Contacts', 'Account'=>'Accounts', 'Opportunity'=>'Opportunities', 'Case'=>'Cases', 'Note'=>'Notes', 'Call'=>'Calls', 'Email'=>'Emails', 'Meeting'=>'Meetings', 'Task'=>'Tasks', 'Lead'=>'Leads','Bug'=>'Bugs',

);

/**
 * To make your changes upgrade safe create a file called leadCapture_override.php and place the changes there
 */
$users = array(
	'PUT A RANDOM KEY FROM THE WEBSITE HERE' => array('name'=>'PUT THE USER_NAME HERE', 'pass'=>'PUT THE USER_HASH FOR THE RESPECTIVE USER HERE'),
);

if (isset($_POST['campaign_id']) && !empty($_POST['campaign_id'])) {
	    //adding the client ip address
	    $_POST['client_id_address'] = query_client_ip();
		$campaign_id=$_POST['campaign_id'];
		$campaign = new Campaign();
		$camp_query  = "select name,id from campaigns where id='$campaign_id'";
		$camp_query .= " and deleted=0";
        $camp_result=$campaign->db->query($camp_query);
        $camp_data = $campaign->db->fetchByAssoc($camp_result);
        // Bug 41292 - have to select marketing_id for new lead
        $db = DBManagerFactory::getInstance();
        $marketing = new EmailMarketing();
        $marketing_query = $marketing->create_new_list_query(
                'date_start desc, date_modified desc',
                "campaign_id = '{$campaign_id}' and status = 'active' and date_start < " . $db->convert('', 'today'),
                array('id')
        );
        $marketing_result = $db->limitQuery($marketing_query, 0, 1, true);
        $marketing_data = $db->fetchByAssoc($marketing_result);
        // .Bug 41292
		if (isset($_REQUEST['assigned_user_id']) && !empty($_REQUEST['assigned_user_id'])) {
			$current_user = new User();
			$current_user->retrieve($_REQUEST['assigned_user_id']);
		} 

	    if(isset($camp_data) && $camp_data != null ){
			$leadForm = new LeadFormBase();
            $lead = new Lead();
			$prefix = '';
			if(!empty($_POST['prefix'])){
				$prefix = $_POST['prefix'];
			}

			if(empty($lead->id)) {
                $lead->id = create_guid();
                $lead->new_with_id = true;
            }
            $GLOBALS['check_notify'] = true;

            //bug: 47574 - make sure, that webtolead_email1 field has same required attribute as email1 field
            if(isset($lead->required_fields['email1'])){
                $lead->required_fields['webtolead_email1'] = $lead->required_fields['email1'];
            }
            
            //bug: 42398 - have to unset the id from the required_fields since it is not populated in the $_POST
            unset($lead->required_fields['id']);
            unset($lead->required_fields['team_name']);
            unset($lead->required_fields['team_count']);

            // Bug #52563 : Web to Lead form redirects to Sugar when duplicate detected
            // prevent duplicates check
            $_POST['dup_checked'] = true;

            // checkRequired needs a major overhaul before it works for web to lead forms.
            $lead = $leadForm->handleSave($prefix, false, false, false, $lead);
            
			if(!empty($lead)){
				
	            //create campaign log
	            $camplog = new CampaignLog();
	            $camplog->campaign_id  = $_POST['campaign_id'];
	            $camplog->related_id   = $lead->id;
	            $camplog->related_type = $lead->module_dir;
	            $camplog->activity_type = "lead";
	            $camplog->target_type = $lead->module_dir;
	            $campaign_log->activity_date=$timedate->now();
	            $camplog->target_id    = $lead->id;
                if(isset($marketing_data['id']))
                {
                    $camplog->marketing_id = $marketing_data['id'];
                }
	            $camplog->save();

		        //link campaignlog and lead

		        if (isset($_POST['email1']) && $_POST['email1'] != null)
                {
                    $lead->email1 = $_POST['email1'];
		        } 
                //in case there are old forms used webtolead_email1
                elseif (isset($_POST['webtolead_email1']) && $_POST['webtolead_email1'] != null)
                {
                    $lead->email1 = $_POST['webtolead_email1'];
                }
                
		        if (isset($_POST['email2']) && $_POST['email2'] != null)
                {
                    $lead->email2 = $_POST['email2'];
		        } 
                //in case there are old forms used webtolead_email2
                elseif (isset($_POST['webtolead_email2']) && $_POST['webtolead_email2'] != null)
                {
                    $lead->email2 = $_POST['webtolead_email2'];
                }
                
		        $lead->load_relationship('campaigns');
		        $lead->campaigns->add($camplog->id);
                if(!empty($GLOBALS['check_notify'])) {
                    $lead->save($GLOBALS['check_notify']);
                }
                else {
                    $lead->save(FALSE);
                }
            }

            //in case there are forms out there still using email_opt_out
            if(isset($_POST['webtolead_email_opt_out']) || isset($_POST['email_opt_out'])){
                    
                if(isset ($lead->email1) && !empty($lead->email1)){
                    $sea = new SugarEmailAddress();
                    $sea->AddUpdateEmailAddress($lead->email1,0,1);
                }   
                if(isset ($lead->email2) && !empty($lead->email2)){
                    $sea = new SugarEmailAddress();
                    $sea->AddUpdateEmailAddress($lead->email2,0,1);
                    
                }
            }              
			if(isset($_POST['redirect_url']) && !empty($_POST['redirect_url'])){
			    // Get the redirect url, and make sure the query string is not too long
		        $redirect_url = $_POST['redirect_url'];
		        $query_string = '';
				$first_char = '&';
				if(strpos($redirect_url, '?') === FALSE){
					$first_char = '?';
				}
				$first_iteration = true;
				$get_and_post = array_merge($_GET, $_POST);
				foreach($get_and_post as $param => $value) {

					if($param == 'redirect_url' && $param == 'submit')
						continue;
					
					if($first_iteration){
						$first_iteration = false;
						$query_string .= $first_char;
					}
					else{
						$query_string .= "&";
					}
					$query_string .= "{$param}=".urlencode($value);
				}
				if(empty($lead)) {
					if($first_iteration){
						$query_string .= $first_char;
					}
					else{
						$query_string .= "&";
					}
					$query_string .= "error=1";
				}
				
				$redirect_url = $redirect_url.$query_string;


				// Check if the headers have been sent, or if the redirect url is greater than 2083 characters (IE max URL length)
				//   and use a javascript form submission if that is the case.
			    if(headers_sent() || strlen($redirect_url) > 2083){
    				echo '<html ' . get_language_header() . '><head><title>SugarCRM</title></head><body>';
    				echo '<form name="redirect" action="' .$_POST['redirect_url']. '" method="GET">';
    
    				foreach($_POST as $param => $value) {
    					if($param != 'redirect_url' ||$param != 'submit') {
    						echo '<input type="hidden" name="'.$param.'" value="'.$value.'">';
    					}
    				}
    				if(empty($lead)) {
    					echo '<input type="hidden" name="error" value="1">';
    				}
    				echo '</form><script language="javascript" type="text/javascript">document.redirect.submit();</script>';
    				echo '</body></html>';
    			}
				else{
    				header("Location: {$redirect_url}");
    				die();
			    }
			}
			else{
				echo $mod_strings['LBL_THANKS_FOR_SUBMITTING_LEAD'];
			}
			sugar_cleanup();
			// die to keep code from running into redirect case below
			die();
	    }
	   else{
	  	  echo $mod_strings['LBL_SERVER_IS_CURRENTLY_UNAVAILABLE'];
	  }
}

if (!empty($_POST['redirect'])) {
    if(headers_sent()){
    	echo '<html ' . get_language_header() . '><head><title>SugarCRM</title></head><body>';
    	echo '<form name="redirect" action="' .$_POST['redirect']. '" method="GET">';
    	echo '</form><script language="javascript" type="text/javascript">document.redirect.submit();</script>';
    	echo '</body></html>';
    }
    else{
    	header("Location: {$_POST['redirect']}");
    	die();
    }
}

echo $mod_strings['LBL_SERVER_IS_CURRENTLY_UNAVAILABLE'];

?>


//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/


require_once('include/EditView/EditView2.php');

require_once('modules/Campaigns/utils.php');


require_once('modules/Campaigns/utils.php');

global $mod_strings, $app_list_strings, $app_strings, $current_user, $import_bean_map;
global $import_file_name, $theme;$app_list_strings;
$lead = new Lead();
$fields = array();

$xtpl=new XTemplate ('modules/Campaigns/WebToLeadCreation.html');
$xtpl->assign("MOD", $mod_strings);
$xtpl->assign("APP", $app_strings);
if(isset($_REQUEST['module']))
{
    $xtpl->assign("MODULE", $_REQUEST['module']);
}
if(isset($_REQUEST['return_module']))
{
    $xtpl->assign("RETURN_MODULE", $_REQUEST['return_module']);
}
if(isset($_REQUEST['return_id']))
{
    $xtpl->assign("RETURN_ID", $_REQUEST['return_id']);
}
if(isset($_REQUEST['return_id']))
{
    $xtpl->assign("RETURN_ACTION", $_REQUEST['return_action']);
}
if(isset($_REQUEST['record']))
{
    $xtpl->assign("RECORD", $_REQUEST['record']);
}

global $theme;
global $currentModule;

$ev = new EditView;

$xtpl->assign("TITLE1",
    $this->getModuleTitle(
        true,
        array(
            $this->_getModuleTitleListParam(),
            $mod_strings['LBL_WEB_TO_LEAD_FORM_TITLE1']
            )
        )
    );

$xtpl->assign("TITLE2",
    $this->getModuleTitle(
        true,
        array(
            $this->_getModuleTitleListParam(),
            $mod_strings['LBL_WEB_TO_LEAD_FORM_TITLE2']
            )
        )
    );

$site_url = $sugar_config['site_url'];
$web_post_url = $site_url.'/index.php?entryPoint=WebToLeadCapture';
$json = getJSONobj();
// Users Popup
$popup_request_data = array(
	'call_back_function' => 'set_return',
	'form_name' => 'WebToLeadCreation',
	'field_to_name_array' => array(
		'id' => 'assigned_user_id',
		'user_name' => 'assigned_user_name',
		),
	);
$xtpl->assign('encoded_users_popup_request_data', $json->encode($popup_request_data));

//Campaigns popup
$popup_request_data = array(
		'call_back_function' => 'set_return',
		'form_name' => 'WebToLeadCreation',
		'field_to_name_array' => array(
			'id' => 'campaign_id',
			'name' => 'campaign_name',
		),
);
$encoded_users_popup_request_data = $json->encode($popup_request_data);
$xtpl->assign('encoded_campaigns_popup_request_data' , $json->encode($popup_request_data));

//create the cancel button
$cancel_buttons_html = "<input class='button' onclick=\"this.form.action.value='".$_REQUEST['return_action']."'; this.form.module.value='".$_REQUEST['return_module']."';\" type='submit' name='cancel' value='".$app_strings['LBL_BACK']."'/>";
$xtpl->assign("CANCEL_BUTTON", $cancel_buttons_html );

$field_defs_js = "var field_defs = {'Contacts':[";

//bug: 47574 - make sure, that webtolead_email1 field has same required attribute as email1 field
if(isset($lead->field_defs['webtolead_email1']) && isset($lead->field_defs['email1']) && isset($lead->field_defs['email1']['required'])){
    $lead->field_defs['webtolead_email1']['required'] = $lead->field_defs['email1']['required'];
}

$count= 0;
foreach($lead->field_defs as $field_def)
{
	$email_fields = false;
    if($field_def['name']== 'email1' || $field_def['name']== 'email2')
    {
    	$email_fields = true;
    }
	  if($field_def['name']!= 'account_name'){
	    if( ( $field_def['type'] == 'relate' && empty($field_def['custom_type']) )
	    	|| $field_def['type'] == 'assigned_user_name' || $field_def['type'] =='link'
	    	|| (isset($field_def['source'])  && $field_def['source']=='non-db' && !$email_fields) || $field_def['type'] == 'id')
	    {
	        continue;
	    }
	   }
	    if($field_def['name']== 'deleted' || $field_def['name']=='converted' || $field_def['name']=='date_entered'
	        || $field_def['name']== 'date_modified' || $field_def['name']=='modified_user_id'
	        || $field_def['name']=='assigned_user_id' || $field_def['name']=='created_by'
	        || $field_def['name']=='team_id')
	    {
	    	continue;
	    }


    $field_def['vname'] = preg_replace('/:$/','',translate($field_def['vname'],'Leads'));

     //$cols_name = "{'".$field_def['vname']."'}";
     $col_arr = array();
     if((isset($field_def['required']) && $field_def['required'] != null && $field_def['required'] != 0)
     	|| $field_def['name']=='last_name'
     	){
        $cols_name=$field_def['vname'].' '.$app_strings['LBL_REQUIRED_SYMBOL'];
        $col_arr[0]=$cols_name;
        $col_arr[1]=$field_def['name'];
        $col_arr[2]=true;
     }
     else{
	     $cols_name=$field_def['vname'];
	     $col_arr[0]=$cols_name;
	     $col_arr[1]=$field_def['name'];
     }
     if (! in_array($cols_name, $fields))
     {
         array_push($fields,$col_arr);
     }
     $count++;
}

$xtpl->assign("WEB_POST_URL",$web_post_url);
//$xtpl->assign("LEAD_SELECT_FIELDS",'MOD.LBL_SELECT_LEAD_FIELDS');

require_once('include/QuickSearchDefaults.php');
$qsd = QuickSearchDefaults::getQuickSearchDefaults();
$sqs_objects = array('account_name' => $qsd->getQSParent(),
					'assigned_user_name' => $qsd->getQSUser(),
					'campaign_name' => $qsd->getQSCampaigns(),

					);
$quicksearch_js = '<script type="text/javascript" language="javascript">sqs_objects = ' . $json->encode($sqs_objects) . '</script>';
$xtpl->assign("JAVASCRIPT", $quicksearch_js);



if (empty($focus->assigned_user_id) && empty($focus->id))  $focus->assigned_user_id = $current_user->id;
if (empty($focus->assigned_name) && empty($focus->id))  $focus->assigned_user_name = $current_user->user_name;
$xtpl->assign("ASSIGNED_USER_OPTIONS", get_select_options_with_id(get_user_array(TRUE, "Active", $focus->assigned_user_id), $focus->assigned_user_id));
$xtpl->assign("ASSIGNED_USER_NAME", $focus->assigned_user_name);
$xtpl->assign("ASSIGNED_USER_ID", $focus->assigned_user_id );

$xtpl->assign("REDIRECT_URL_DEFAULT",'http://');

//required fields on Webtolead form
$campaign= new Campaign();

$javascript = new javascript();
$javascript->setFormName('WebToLeadCreation');
$javascript->setSugarBean($lead);
$javascript->addAllFields('');
//$javascript->addFieldGeneric('redirect_url', '', 'LBL_REDIRECT_URL' ,'true');
$javascript->addFieldGeneric('campaign_name', '', 'LBL_RELATED_CAMPAIGN' ,'true');
$javascript->addFieldGeneric('assigned_user_name', '', 'LBL_ASSIGNED_TO' ,'true');
$javascript->addToValidateBinaryDependency('campaign_name', 'alpha', $app_strings['ERR_SQS_NO_MATCH_FIELD'] . $mod_strings['LBL_LEAD_NOTIFY_CAMPAIGN'], 'false', '', 'campaign_id');
$javascript->addToValidateBinaryDependency('assigned_user_name', 'alpha', $app_strings['ERR_SQS_NO_MATCH_FIELD'] . $app_strings['LBL_ASSIGNED_TO'], 'false', '', 'assigned_user_id');
echo $javascript->getScript();

$json = getJSONobj();
$lead_fields = $json->encode($fields);
$xtpl->assign("LEAD_FIELDS",$lead_fields);
$classname = "SUGAR_GRID";
$xtpl->assign("CLASSNAME",$classname);
$xtpl->assign("DRAG_DROP_CHOOSER_WEB_TO_LEAD",constructDDWebToLeadFields($fields,$classname));

$xtpl->parse("main");
$xtpl->out("main");
/*
$str = "<script>
WebToLeadForm.lead_fields = {$lead_fields};
</script>";
echo $str;
*/
/*
 *This function constructs Drag and Drop multiselect box of subscriptions for display in manage subscription form
*/
function constructDDWebToLeadFields($fields,$classname){
require_once("include/templates/TemplateDragDropChooser.php");
global $mod_strings;
$d2 = array();
    //now call function that creates javascript for invoking DDChooser object
    $dd_chooser = new TemplateDragDropChooser();
    $dd_chooser->args['classname']  = $classname;
    $dd_chooser->args['left_header']  = $mod_strings['LBL_AVALAIBLE_FIELDS_HEADER'];
    $dd_chooser->args['mid_header']   = $mod_strings['LBL_LEAD_FORM_FIRST_HEADER'];
    $dd_chooser->args['right_header'] = $mod_strings['LBL_LEAD_FORM_SECOND_HEADER'];
    $dd_chooser->args['left_data']    = $fields;
    $dd_chooser->args['mid_data']     = $d2;
    $dd_chooser->args['right_data']   = $d2;
    $dd_chooser->args['title']        =  ' ';
    $dd_chooser->args['left_div_name']      = 'ddgrid2';
    $dd_chooser->args['mid_div_name']       = 'ddgrid3';
    $dd_chooser->args['right_div_name']     = 'ddgrid4';
    $dd_chooser->args['gridcount'] = 'three';
    $str = $dd_chooser->displayScriptTags();
    $str .= $dd_chooser->displayDefinitionScript();
    $str .= $dd_chooser->display();
    $str .= "<script>
	           //function post rows
	           function postMoveRows(){
			    	//Call other function when this is called
	           }
	        </script>";
	$str .= "<script>
		       function displayAddRemoveDragButtons(Add_All_Fields,Remove_All_Fields){
				    var addRemove = document.getElementById('lead_add_remove_button');
				    if(" . $dd_chooser->args['classname'] . "_grid0.getDataModel().getTotalRowCount() ==0) {
				     addRemove.setAttribute('value',Remove_All_Fields);
				     addRemove.setAttribute('title',Remove_All_Fields);
				    }
				    else if(" . $dd_chooser->args['classname'] . "_grid1.getDataModel().getTotalRowCount() ==0 && " . $dd_chooser->args['classname'] . "_grid2.getDataModel().getTotalRowCount() ==0){
				     addRemove.setAttribute('value',Add_All_Fields);
				     addRemove.setAttribute('title',Add_All_Fields);
				    }
              }
            </script>";

    return $str;
}

/**
 * function to retrieve webtolead image and title. path to help file
 * refactored to use SugarView::getModuleTitle()
 *
 * @deprecated use SugarView::getModuleTitle() instead
 *
 * @param  $module       string  not used, only for backward compatibility
 * @param  $image_name   string  image name
 * @param  $module_title string  to display as the module title
 * @param  $show_help    boolean which determines if the print and help links are shown.
 * @return string HTML
 */
function get_webtolead_title(
    $module,
    $image_name, 
    $module_title, 
    $show_help
    )
{
    return $GLOBALS['current_view']->getModuleTitle($show_help);
}


//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
require_once('include/formbase.php');
require_once('include/SugarTinyMCE.php');



global $mod_strings;
global $app_strings;


//-----------begin replacing text input tags that have been marked with text area tags
//get array of text areas strings to process
$bodyHTML = html_entity_decode($_REQUEST['body_html'],ENT_QUOTES);
//Bug53791
$bodyHTML = str_replace(chr(160), " ", $bodyHTML);

while (strpos($bodyHTML, "ta_replace") !== false){

	//define the marker edges of the sub string to process (opening and closing tag brackets)
	$marker = strpos($bodyHTML, "ta_replace");
	$start_border = strpos($bodyHTML, "input", $marker) - 1;// to account for opening '<' char;
	$end_border = strpos($bodyHTML, '>', $start_border); //get the closing tag after marker ">";

	//extract the input tag string
	$working_str = substr($bodyHTML, $marker-3, $end_border-($marker-3) );

	//replace input markup with text areas markups
	$new_str = str_replace('input','textarea',$working_str);
	$new_str = str_replace("type='text'", ' ', $new_str);
	$new_str = $new_str . '> </textarea';

	//replace the marker with generic term
	$new_str = str_replace('ta_replace', 'sugarslot', $new_str);

	//merge the processed string back into bodyhtml string
	$bodyHTML = str_replace($working_str , $new_str, $bodyHTML);
}
//<<<----------end replacing marked text inputs with text area tags

$guid = create_guid();
$form_file = "upload://$guid";

$SugarTiny =  new SugarTinyMCE();
$html = $SugarTiny->cleanEncodedMCEHtml($bodyHTML);

//Check to ensure we have <html> tags in the form. Without them, IE8 will attempt to display the page as XML.
if (stripos($html, "<html") === false) {
    $langHeader = get_language_header();
    $html = "<html {$langHeader}><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></head><body>" . $html . "</body></html>";
}
file_put_contents($form_file, $html);

$xtpl=new XTemplate ('modules/Campaigns/WebToLeadDownloadForm.html');
$xtpl->assign("MOD", $mod_strings);
$xtpl->assign("APP", $app_strings);
$webformlink = "<b>$mod_strings[LBL_DOWNLOAD_TEXT_WEB_TO_LEAD_FORM]</b><br/>";
$webformlink .= "<a href=\"index.php?entryPoint=download&id={$guid}&isTempFile=1&tempName=WebToLeadForm.html&type=temp\">$mod_strings[LBL_DOWNLOAD_WEB_TO_LEAD_FORM]</a>";
$xtpl->assign("LINK_TO_WEB_FORM",$webformlink);
$xtpl->assign("RAW_SOURCE", htmlspecialchars($html)); 
$xtpl->parse("main.copy_source");
$xtpl->parse("main");
$xtpl->out("main");

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

/*************** general UI Stuff ****************/





global $mod_strings,$app_list_strings,$app_strings,$current_user;


if (!is_admin($current_user)&& !is_admin_for_module($GLOBALS['current_user'],'Campaigns')) sugar_die("Unauthorized access to administration.");

$params = array();
$params[] = "<a href='index.php?module=Campaigns&action=index'>{$mod_strings['LBL_MODULE_NAME']}</a>";
$params[] = $mod_strings['LBL_EMAIL_SETUP_WIZARD_TITLE'];

echo getClassicModuleTitle('Campaigns', $params, true);


global $theme, $currentModule, $sugar_config;





//get administration bean for email setup
$focus = new Administration();
$focus->retrieveSettings(); //retrieve all admin settings.
$GLOBALS['log']->info("Mass Emailer(EmailMan) ConfigureSettings view");
$email = new Email();
$ss = new Sugar_Smarty();
$ss->assign("MOD", $mod_strings);
$ss->assign("APP", $app_strings);
if (isset($_REQUEST['return_module'])) $ss->assign("RETURN_MODULE", $_REQUEST['return_module']);
if (isset($_REQUEST['return_action'])) $ss->assign("RETURN_ACTION", $_REQUEST['return_action']);
if (isset($_REQUEST['return_id'])) $ss->assign("RETURN_ID", $_REQUEST['return_id']);



/******** Email Setup UI DIV Stuff **********/
//get Settings if they exist
$ss->assign("notify_fromaddress", $focus->settings['notify_fromaddress']);
$ss->assign("notify_send_from_assigning_user", ($focus->settings['notify_send_from_assigning_user']) ? "checked='checked'" : "");
$ss->assign("notify_on", ($focus->settings['notify_on']) ? "checked='checked'" : "");
$ss->assign("notify_fromname", $focus->settings['notify_fromname']);
$ss->assign("mail_smtpserver", $focus->settings['mail_smtpserver']);
$ss->assign("mail_smtpport", $focus->settings['mail_smtpport']);
$ss->assign("mail_sendtype_options", get_select_options_with_id($app_list_strings['notifymail_sendtype'], $focus->settings['mail_sendtype']));
$ss->assign("mail_smtpuser", $focus->settings['mail_smtpuser']);
$ss->assign("mail_smtppass", $focus->settings['mail_smtppass']);
$ss->assign("mail_smtpauth_req", ($focus->settings['mail_smtpauth_req']) ? "checked='checked'" : "");

$protocol = filterInboundEmailPopSelection($app_list_strings['dom_email_server_type']);
$ss->assign('PROTOCOL', get_select_options_with_id($protocol, ''));
if (isset($focus->settings['massemailer_campaign_emails_per_run']) && !empty($focus->settings['massemailer_campaign_emails_per_run'])) {
    $ss->assign("EMAILS_PER_RUN", $focus->settings['massemailer_campaign_emails_per_run']);
} else  {
    $ss->assign("EMAILS_PER_RUN", 500);
}

if (!isset($focus->settings['massemailer_tracking_entities_location_type']) or empty($focus->settings['massemailer_tracking_entities_location_type']) or $focus->settings['massemailer_tracking_entities_location_type']=='1') {
    $ss->assign("DEFAULT_CHECKED", "checked");
    $ss->assign("TRACKING_ENTRIES_LOCATION_STATE", "disabled");
    $ss->assign("TRACKING_ENTRIES_LOCATION",$mod_strings['TRACKING_ENTRIES_LOCATION_DEFAULT_VALUE']);
} else  {
    $ss->assign("USERDEFINED_CHECKED", "checked");
    $ss->assign("TRACKING_ENTRIES_LOCATION",$focus->settings["massemailer_tracking_entities_location"]);
}

$ss->assign("SITEURL",$sugar_config['site_url']);

// Change the default campaign to not store a copy of each message.
if (!empty($focus->settings['massemailer_email_copy']) and $focus->settings['massemailer_email_copy']=='1') {
    $ss->assign("YES_CHECKED", "checked='checked'");
} else  {
    $ss->assign("NO_CHECKED", "checked='checked'");
}

$ss->assign("MAIL_SSL_OPTIONS", get_select_options_with_id($app_list_strings['email_settings_for_ssl'], $focus->settings['mail_smtpssl']));


/*********** New Mail Box UI DIV Stuff ****************/
$mbox_qry = "select * from inbound_email where deleted ='0' and mailbox_type = 'bounce'";
$mbox_res = $focus->db->query($mbox_qry);
while ($mbox_row = $focus->db->fetchByAssoc($mbox_res)){$mbox[] = $mbox_row;}
$mbox_msg = ' ';
$need_mbox = '';  

$mboxTable = "<table class='list view' width='100%' border='0' cellspacing='1' cellpadding='1'>";
if(isset($mbox) && count($mbox)>0){
    $mboxTable .= "<tr><td colspan='5'><b>" .count($mbox) ." ". $mod_strings['LBL_MAILBOX_CHECK_WIZ_GOOD']." </b>.</td></tr>";
        $mboxTable .= "<tr class='listViewHRS1'><td width='20%'><b>".$mod_strings['LBL_MAILBOX_NAME']."</b></td>"
                   .  " <td width='20%'><b>".$mod_strings['LBL_LOGIN']."</b></td>"
                   .  " <td width='20%'><b>".$mod_strings['LBL_MAILBOX']."</b></td>" 
                   .  " <td width='20%'><b>".$mod_strings['LBL_SERVER_URL']."</b></td>"
                   .  " <td width='20%'><b>".$mod_strings['LBL_LIST_STATUS']."</b></td></tr>";
    $colorclass=' ';
    foreach($mbox as $details){
                
     if( $colorclass == "class='evenListRowS1'"){
            $colorclass= "class='oddListRowS1'";
        }else{ 
            $colorclass= "class='evenListRowS1'";
        }           
        
        $mboxTable .= "<tr $colorclass>";
        $mboxTable .= "<td>".$details['name']."</td>";
        $mboxTable .= "<td>".$details['email_user']."</td>";
        $mboxTable .= "<td>".$details['mailbox']."</td>";
        $mboxTable .= "<td>".$details['server_url']."</td>";
        $mboxTable .= "<td>".$details['status']."</td></tr>";
    }


}else{
$need_mbox = 'checked';
$mboxTable .= "<tr><td colspan='5'><b>".$mod_strings['LBL_MAILBOX_CHECK_WIZ_BAD']." </b>.</td></tr>";
}        
$mboxTable .= "</table>";
$ss->assign("MAILBOXES_DETECTED_MESSAGE", $mboxTable);
$ss->assign("MBOX_NEEDED", $need_mbox);          
$ss->assign('ROLLOVER', $email->rolloverStyle);
if(!function_exists('imap_open')) {
    $ss->assign('IE_DISABLED', 'DISABLED');   
}
/**************************** SUMMARY UI DIV Stuff *******************/

/**************************** WIZARD UI DIV Stuff *******************/
  
//  this is the wizard control script that resides in page    
 $divScript = <<<EOQ
 <script type="text/javascript" language="javascript">  

    //this function toggles visibility of fields based on selected options
    function notify_setrequired() {
        f = document.getElementById("wizform");
        document.getElementById("smtp_settings").style.display = (f.mail_sendtype.value == "SMTP") ? "inline" : "none";
        document.getElementById("smtp_settings").style.visibility = (f.mail_sendtype.value == "SMTP") ? "visible" : "hidden";
        document.getElementById("smtp_auth").style.display = (document.getElementById('mail_smtpauth_req').checked) ? "inline" : "none";
        document.getElementById("smtp_auth").style.visibility = (document.getElementById('mail_smtpauth_req').checked) ? "visible" : "hidden";
        document.getElementById("new_mbox").style.display = (document.getElementById('create_mbox').checked) ? "inline" : "none";
        document.getElementById("new_mbox").style.visibility = (document.getElementById('create_mbox').checked) ? "visible" : "hidden";
        document.getElementById("wiz_new_mbox").value = (document.getElementById('create_mbox').checked) ? "1" : "0";
        return true;
    }
    
    //this function will copy as much information as possible from the first step in wizard
    //onto the the second step in wizard
    function copy_down() {
        document.getElementById("name").value = document.getElementById("notify_fromname").value;
        document.getElementById("email_user").value = document.getElementById("notify_fromaddress").value;
        document.getElementById("from_addr").value = document.getElementById("notify_fromaddress").value;
        if(document.getElementById("mail_sendtype").value=='SMTP'){
            document.getElementById("protocol").value = "SMTP";
            document.getElementById("server_url").value = document.getElementById("mail_smtpserver").value;
            if(document.getElementById('mail_smtpauth_req').checked){    
                document.getElementById("email_user").value = document.getElementById("mail_smtpuser").value;
                document.getElementById("email_password").value = document.getElementById("mail_smtppass").value;
            }
        
        }
        return true;
    }

    //this calls the validation functions for each step that needs validation 
    function validate_wiz_form(step){
        switch (step){
            case 'step1':
            if(!validate_step1()){return false;}
              copy_down();
            break;
            case 'step2':
           if(!validate_step2()){return false;} 
            break;                  
            default://no additional validation needed      
        }
        return true;
    
    }
    
    //this function will add validation to step1
    function validate_step1(){
        requiredTxt = SUGAR.language.get('app_strings', 'ERR_MISSING_REQUIRED_FIELDS');
        var haserrors = 0;
        var fields = new Array();

        //create list of fields that need validation, based on selected options        
        fields[0] = 'notify_fromname';
        fields[1] = 'notify_fromaddress';
        fields[2] = 'massemailer_campaign_emails_per_run';
        fields[3] = 'massemailer_tracking_entities_location';
        if(document.getElementById("mail_sendtype").value=='SMTP'){
            fields[4] = 'mail_smtpserver';
            fields[5] = 'mail_smtpport';
                if(document.getElementById('mail_smtpauth_req').checked){    
                    fields[6] = 'mail_smtpuser';
                    fields[7] = 'mail_smtppass';
                }
        }
        
        var field_value = '';
        //iterate through required fields and set empty string values ('  '') to null, this will cause failure later on 
        for (i=0; i < fields.length; i++){
            elem = document.getElementById(fields[i]);
            field_value = trim(elem.value);
            if(field_value.length<1){
                elem.value = '';
            }
        }
        //add to generic validation and call function to calidate
        if(validate['wizform']!='undefined'){delete validate['wizform']};        
        addToValidate('wizform', 'notify_fromaddress', 'email', true,  document.getElementById('notify_fromaddress').title);
        addToValidate('wizform', 'notify_fromname', 'alphanumeric', true,  document.getElementById('notify_fromname').title);
        addToValidate('wizform', 'massemailer_campaign_emails_per_run', 'int', true,  document.getElementById('massemailer_campaign_emails_per_run').title);
        addToValidate('wizform', 'massemailer_tracking_entities_location', 'alphanumeric', true,  document.getElementById('massemailer_tracking_entities_location').title);
        if(document.getElementById("mail_sendtype").value=='SMTP'){
            addToValidate('wizform', 'mail_smtpserver', 'alphanumeric', true,  document.getElementById('mail_smtpserver').title);
            addToValidate('wizform', 'mail_smtpport', 'int', true,  document.getElementById('mail_smtpport').title);        
                if(document.getElementById('mail_smtpauth_req').checked){    
                    addToValidate('wizform', 'mail_smtpuser', 'alphanumeric', true,  document.getElementById('mail_smtpuser').title);
                    addToValidate('wizform', 'mail_smtppass', 'alphanumeric', true,  document.getElementById('mail_smtppass').title);
                }
        }
    
      
        return check_form('wizform');    
    }    
    
    
    
    function validate_step2(){
        requiredTxt = SUGAR.language.get('app_strings', 'ERR_MISSING_REQUIRED_FIELDS');
        //validate only if the create mailbox form input has been selected
        if(document.getElementById("wiz_new_mbox").value == "0"){
         //this form is not checked, do not validate
         return true;   
        }
        var haserrors = 0;
        var wiz_message = document.getElementById('wiz_message');

        //create list of fields that need validation, based on selected options        
        var fields = new Array();
        fields[0] = 'name';
        fields[1] = 'server_url';
        fields[2] = 'email_user';
        fields[3] = 'protocol';
        fields[4] = 'email_password';
        fields[5] = 'mailbox';
        fields[6] = 'port';
    
        //iterate through required fields and set empty string values ('  '') to null, this will cause failure later on 
        var field_value = ''; 
        for (i=0; i < fields.length; i++){
            field_value = trim(document.getElementById(fields[i]).value);
            if(field_value.length<1){
                add_error_style('wizform', fields[i], requiredTxt +' ' +document.getElementById(fields[i]).title );
                haserrors = 1;
            }
        }

        //add to generic validation and call function to calidate
        if(validate['wizform']!='undefined'){delete validate['wizform']};        
        addToValidate('wizform', 'name', 'alphanumeric', true,  document.getElementById('name').title);
        addToValidate('wizform', 'server_url', 'alphanumeric', true,  document.getElementById('server_url').title);
        addToValidate('wizform', 'email_user', 'alphanumeric', true,  document.getElementById('email_user').title);
        addToValidate('wizform', 'email_password', 'alphanumeric', true,  document.getElementById('email_password').title);
        addToValidate('wizform', 'mailbox', 'alphanumeric', true,  document.getElementById('mailbox').title);
        addToValidate('wizform', 'protocol', 'alphanumeric', true,  document.getElementById('protocol').title);
        addToValidate('wizform', 'port', 'int', true,  document.getElementById('port').title);        

        if(haserrors == 1){
            return false;
        }
        return check_form('wizform');


    }    

    /*
     * The generic create summary will not work for this wizard, as we have a step that only gets
     * displayed if a check box is marked, and we also have certain inputs we do not want displayed(ie. password)
     * so this function will override the genereic version 
     */
    function create_summary(){
        var current_step = document.getElementById('wiz_current_step');
        var currentValue = parseInt(current_step.value);
        var temp_elem = '';

        //  alert(test.title);alert(test.name);alert(test.id);
        var fields = new Array();
        //create the list of fields to create summary table
        fields[0] = 'notify_fromname';
        fields[1] = 'notify_fromaddress';
        fields[2] = 'massemailer_campaign_emails_per_run';
        fields[3] = 'massemailer_tracking_entities_location';
        
         if(document.getElementById("mail_sendtype").value=='SMTP'){
              fields[4] = 'mail_smtpserver';
              fields[5] = 'mail_smtpport';
                 if(document.getElementById('mail_smtpauth_req').checked){    
                      fields[6] = 'mail_smtpuser';
                 }
              fields[7] = 'mail_smtpssl';
          }
        
          if(document.getElementById("wiz_new_mbox").value != "0"){
                fields[8] = 'name';
                fields[9] = 'server_url';
                fields[10] = 'email_user';
                fields[11] = 'from_addr';
                fields[12] = 'protocol';
                fields[13] = 'mailbox';
                fields[14] = 'port';
                fields[15] = 'ssl';
          }
    
        //iterate through list and create table
        var summhtml = "<table class='detail view' width='100%' border='0' cellspacing='1' cellpadding='1'>";
        var colorclass = 'tabDetailViewDF2';
        var elem ='';
        for (var i=0; i<fields.length; i++)
          {elem = document.getElementById(fields[i]);
            if(elem!=null){
                if(elem.type.indexOf('select') >= 0 ){
                    var selInd = elem.selectedIndex;
                    if(selInd<0){selInd =0;}
                    summhtml = summhtml+ "<tr  class='"+colorclass+"' ><td scope='row'  width='15%'><b>" +elem.title+ "</b></td><td class='tabDetailViewDF'  width='30%'>" + YAHOO.lang.escapeHTML(elem.options[selInd].text)+ "&nbsp;</td></tr>";
                }else if(elem.type == 'checked'){
                    summhtml = summhtml+ "<tr  class='"+colorclass+"' ><td scope='row'  width='15%'><b>" +elem.title+ "</b></td><td class='tabDetailViewDF'  width='30%'>" + YAHOO.lang.escapeHTML(elem.value) + "&nbsp;</td></tr>";
                }else if(elem.type == 'checkbox'){
                    if(elem.checked){
                        summhtml = summhtml+ "<tr  class='"+colorclass+"' ><td scope='row'  width='15%'><b>" +elem.title+ "</b></td><td class='tabDetailViewDF'  width='30%'><input type='checkbox' class='checkbox' disabled checked>&nbsp;</td></tr>";
                    }else{
                        summhtml = summhtml+ "<tr  class='"+colorclass+"' ><td scope='row'  width='15%'><b>" +elem.title+ "</b></td><td class='tabDetailViewDF'  width='30%'><input type='checkbox' class='checkbox' disabled >&nbsp;</td></tr>";
                    }
                }else{
                    summhtml = summhtml+ "<tr  class='"+colorclass+"' ><td scope='row'  width='15%'><b>" +elem.title+ "</b></td><td class='tabDetailViewDF'  width='30%'>" + YAHOO.lang.escapeHTML(elem.value) + "&nbsp;</td></tr>";
                }
            }
            if( colorclass== 'tabDetailViewDL2'){
                colorclass= 'tabDetailViewDF2';
            }else{ 
                colorclass= 'tabDetailViewDL2'
            }            
          }

        summhtml = summhtml+ "</table>";
        temp_elem = document.getElementById('wiz_summ');
        temp_elem.innerHTML = summhtml;
        
    }

    showfirst('email');
    notify_setrequired();
    
    
    
    
</script>
EOQ;

if(isset($_REQUEST['error'])){
    //if there is an error flagged, then we are coming here after a save where there was an error detected
    //on an inbound email save.  Display error to user so they are aware.
    $errorString = "<div class='error'>".$mod_strings['ERR_NO_OPTS_SAVED']."  <a href='index.php?module=InboundEmail&action=index'>".$mod_strings['ERR_REVIEW_EMAIL_SETTINGS']."</a></div>";
    $ss->assign('ERROR', $errorString);
    //navigate to inbound email page by default
    $divScript .=" <script>navigate('next');</script>";
}

$ss->assign("DIV_JAVASCRIPT", $divScript);


/**************************** FINAL END OF PAGE UI Stuff *******************/

//$ss->assign("JAVASCRIPT", get_validate_record_js());

$ss->display('modules/Campaigns/WizardEmailSetup.html');

?>


//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
require_once('include/formbase.php');


global $mod_strings;


//get new administration bean for setup
$focus = new Administration();
$camp_steps[] = 'wiz_step_';
$camp_steps[] = 'wiz_step1_';
$camp_steps[] = 'wiz_step2_';

    //name is used as key in post, it is also used in creation of summary page for wizard,
    //so let's clean up the posting so we can reuse the save functionality for inbound emails and
    //from existing save.php's  
    foreach($camp_steps as $step){
        clean_up_post($step);
    }
/**************************** Save general Email Setup  *****************************/

//we do not need to track location if location type is not set
if(isset($_POST['tracking_entities_location_type'])) {
    if ($_POST['tracking_entities_location_type'] != '2') {
        unset($_POST['tracking_entities_location']);
        unset($_POST['tracking_entities_location_type']);
    }
}
//if the check box is empty, then set it to 0
if(!isset($_POST['mail_smtpauth_req'])) { $_POST['mail_smtpauth_req'] = 0; }
//default ssl use to false
if(!isset($_POST['mail_smtpssl'])) { $_POST['mail_smtpssl'] = 0; }
//reuse existing saveconfig functionality
$focus->saveConfig();



/**************************** Add New Monitored Box  *****************************/
//perform this if the option to create new mail box has been checked
if(isset($_REQUEST['wiz_new_mbox']) && ($_REQUEST['wiz_new_mbox']=='1')){
    
   //Populate the Request variables that inboundemail expects
    $_REQUEST['mark_read'] = 1;
    $_REQUEST['only_since'] = 1;
    $_REQUEST['mailbox_type'] = 'bounce';
    $_REQUEST['from_name'] = $_REQUEST['name'];
    $_REQUEST['group_id'] = 'new';
//    $_REQUEST['from_addr'] = $_REQUEST['wiz_step1_notify_fromaddress'];
    //reuse save functionality for inbound email
    require_once('modules/InboundEmail/Save.php');    

}
    if (!empty($_REQUEST['error'])){
            //an error was found during inbound save.  This means the save was allowed but the inbound box had problems, return user to wizard
            //and display error message
            header("Location: index.php?action=WizardEmailSetup&module=Campaigns&error=true");
    }else{
        //set navigation details
        header("Location: index.php?action=index&module=Campaigns");
    }

/*
 * This function will re-add the post variables that exist with the specified prefix.  
 * It will add them minus the specified prefix.  This is needed in order to reuse the save functionality,
 * which does not expect the prefix, and still use the generic create summary functionality in wizard, which
 * does expect the prefix.  
 */
function clean_up_post($prefix){

    foreach ($_REQUEST as $key => $val) {
              if((strstr($key, $prefix )) && (strpos($key, $prefix )== 0)){
              $newkey  =substr($key, strlen($prefix)) ;
              $_REQUEST[$newkey] = $val;
         }               
    }

    foreach ($_POST as $key => $val) {
              if((strstr($key, $prefix )) && (strpos($key, $prefix )== 0)){
              $newkey  =substr($key, strlen($prefix)) ;
              $_POST[$newkey] = $val;
              
         }               
    }
}

?>


//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

/************** general UI Stuff *************/


require_once('modules/Campaigns/utils.php');



$focus = new Campaign();
if(isset($_REQUEST['record']) &&  !empty($_REQUEST['record'])) {
    $focus->retrieve($_REQUEST['record']);

global $mod_strings;
global $app_list_strings;
global $app_strings;
global $current_user;


//if (!is_admin($current_user)) sugar_die("Unauthorized access to administration.");
//account for use within wizards
if($focus->campaign_type == 'NewsLetter'){
    echo getClassicModuleTitle($mod_strings['LBL_MODULE_NAME'], array($mod_strings['LBL_NEWSLETTER_WIZARD_START_TITLE'].$focus->name), true, false);
}else{
    echo getClassicModuleTitle($mod_strings['LBL_MODULE_NAME'], array($mod_strings['LBL_CAMPAIGN_WIZARD_START_TITLE'].$focus->name), true, false);
}

global $theme;
global $currentModule;





  
    $ss = new Sugar_Smarty();
    $ss->assign("MOD", $mod_strings);
    $ss->assign("APP", $app_strings);

    //if this page has been refreshed as a result of sending emails, then display status 
    if(isset($_REQUEST['from'])){
        $mess = $mod_strings['LBL_TEST_EMAILS_SENT'];
        if($_REQUEST['from']=='send'){ $mess = $mod_strings['LBL_EMAILS_SCHEDULED'];  }
        $confirm_msg = "var ajaxWizStatus = new SUGAR.ajaxStatusClass(); ";
        $confirm_msg .= "window.setTimeout(\"ajaxWizStatus.showStatus('".$mess."')\",1000); ";
        $confirm_msg .= "window.setTimeout('ajaxWizStatus.hideStatus()', 1500); ";
        $confirm_msg .= "window.setTimeout(\"ajaxWizStatus.showStatus('".$mess."')\",2000); ";
        $confirm_msg .= "window.setTimeout('ajaxWizStatus.hideStatus()', 5000); ";        
        $ss->assign("MSG_SCRIPT",$confirm_msg);
    }
    
    if (isset($_REQUEST['return_module'])) $ss->assign("RETURN_MODULE", $_REQUEST['return_module']);
    if (isset($_REQUEST['return_action'])) $ss->assign("RETURN_ACTION", $_REQUEST['return_action']);
    if (isset($_REQUEST['return_id'])) $ss->assign("RETURN_ID", $_REQUEST['return_id']);
    if (isset($_REQUEST['record'])) $ss->assign("ID", $_REQUEST['record']);    
    // handle Create $module then Cancel
    if (empty($_REQUEST['return_id'])) {
        $ss->assign("RETURN_ACTION", 'index');
    }
        
    
    
    $ss->assign("CAMPAIGN_TBL", create_campaign_summary ($focus));
    $ss->assign("TARGETS_TBL", create_target_summary ($focus));
    if($focus->campaign_type =='NewsLetter' || $focus->campaign_type =='Email'){
        $ss->assign("MARKETING_TBL", create_marketing_summary ($focus));
        $ss->assign("TRACKERS_TBL", create_tracker_summary ($focus));
    }
    
    $camp_url = "index.php?action=WizardNewsletter&module=Campaigns&return_module=Campaigns&return_action=WizardHome";
    $camp_url .= "&return_id=".$focus->id."&record=".$focus->id."&direct_step=";
    $ss->assign("CAMP_WIZ_URL", $camp_url);

    $mrkt_string = $mod_strings['LBL_NAVIGATION_MENU_MARKETING'];
    if(!empty($focus->id)){
        $mrkt_url = "<a  href='index.php?action=WizardMarketing&module=Campaigns&return_module=Campaigns&return_action=WizardHome";
        $mrkt_url .= "&return_id=".$focus->id."&campaign_id=".$focus->id;
        $mrkt_url .= "'>". $mrkt_string."</a>";
        $mrkt_string = $mrkt_url;
    }
        
        $mrkt_url = "<a  href='index.php?action=WizardMarketing&module=Campaigns&return_module=Campaigns&return_action=WizardHome";
        $mrkt_url .= "&return_id=".$focus->id."&campaign_id=".$focus->id;
        $mrkt_url .= "'>". $mod_strings['LBL_NAVIGATION_MENU_MARKETING']."</a>";
    $ss->assign("MRKT_WIZ_URL", $mrkt_url);

    $summ_url = "<a  href='index.php?action=WizardHome&module=Campaigns";
    $summ_url .= "&return_id=".$focus->id."&record=".$focus->id;
    $summ_url .= "'> ". $mod_strings['LBL_NAVIGATION_MENU_SUMMARY']."</a>";    


    //Create the html to fill in the wizard steps
    if($focus->campaign_type == 'NewsLetter'){
        $ss->assign('NAV_ITEMS',create_wiz_menu_items('newsletter',$mrkt_string,$camp_url,$summ_url ));
        $ss->assign("CAMPAIGN_DIAGNOSTIC_LINK", diagnose());
    }elseif($focus->campaign_type == 'Email'){
        $ss->assign('NAV_ITEMS',create_wiz_menu_items('email',$mrkt_string,$camp_url,$summ_url ));
        $ss->assign("CAMPAIGN_DIAGNOSTIC_LINK", diagnose());
     }else{
        $ss->assign('NAV_ITEMS',create_wiz_menu_items('general',$mrkt_string,$camp_url,$summ_url ));
    }    
    
            
    /********** FINAL END OF PAGE UI Stuff ********/
    $ss->display(file_exists('custom/modules/Campaigns/WizardHome.html') ? 'custom/modules/Campaigns/WizardHome.html' : 'modules/Campaigns/WizardHome.html');
    
}else{
    //there is no record to retrieve, so ask which type of campaign wizard to launch
/*    $header_URL = "Location: index.php?module=Campaigns&action=index";
    $GLOBALS['log']->debug("about to post header URL of: $header_URL");
    header($header_URL);            
*/
    global $mod_strings;
    global $app_list_strings;
    global $app_strings;
    global $current_user;
    
    //if (!is_admin($current_user)) sugar_die("Unauthorized access to administration.");
    //account for use within wizards
        echo getClassicModuleTitle($mod_strings['LBL_MODULE_NAME'], array($mod_strings['LBL_CAMPAIGN_WIZARD'].$focus->name), true, false);
             

    $ss = new Sugar_Smarty();
    $ss->assign("MOD", $mod_strings);
    $ss->assign("APP", $app_strings);
    $ss->display(file_exists('custom/modules/Campaigns/tpls/WizardHomeStart.tpl') ? 'custom/modules/Campaigns/tpls/WizardHomeStart.tpl' : 'modules/Campaigns/tpls/WizardHomeStart.tpl');
       
}




function create_campaign_summary  ($focus){
    global $mod_strings,$app_strings;
    $fields = array();
    $fields[] = 'name';
    $fields[] = 'assigned_user_name';
    $fields[] = 'status';
    $fields[] = 'team_name';
    $fields[] = 'start_date';
    $fields[] = 'end_date';
    if($focus->campaign_type=='NewsLetter'){
        $fields[] = 'frequency';
    }
    $fields[] = 'content';
    $fields[] = 'budget';
    $fields[] = 'actual_cost';
    $fields[] = 'expected_revenue';
    $fields[] = 'expected_cost';
    $fields[] = 'impressions';
    $fields[] = 'objective';  
    
    //create edit view status and input buttons
    $cmp_input = '';

    //create edit campaign button
    $cmp_input =  "<input id='wiz_next_button' name='SUBMIT'  ";  
    $cmp_input.= "onclick=\"this.form.return_module.value='Campaigns';";
    $cmp_input.= "this.form.module.value='Campaigns';";
    $cmp_input.= "this.form.action.value='WizardNewsletter';";
    $cmp_input.= "this.form.return_action.value='WizardHome';";
    $cmp_input.= "this.form.direct_step.value='1';";
    $cmp_input.= "this.form.record.value='".$focus->id."';";
    $cmp_input.= "this.form.return_id.value='".$focus->id."';\" "; 
    $cmp_input.= "class='button' value='".$mod_strings['LBL_EDIT_EXISTING']."' type='submit'> ";
          
    //create view status button      
    if(($focus->campaign_type == 'NewsLetter') || ($focus->campaign_type == 'Email')){
        $cmp_input .=  " <input id='wiz_status_button' name='SUBMIT'  ";  
        $cmp_input.= "onclick=\"this.form.return_module.value='Campaigns';";
        $cmp_input.= "this.form.module.value='Campaigns';";
        $cmp_input.= "this.form.action.value='TrackDetailView';";
        $cmp_input.= "this.form.return_action.value='WizardHome';";
        $cmp_input.= "this.form.record.value='".$focus->id."';";
        $cmp_input.= "this.form.return_id.value='".$focus->id."';\" "; 
        $cmp_input.= "class='button' value='".$mod_strings['LBL_TRACK_BUTTON_TITLE']."' type='submit'>";
    }
    //create view roi button
    $cmp_input .=  " <input id='wiz_status_button' name='SUBMIT'  ";  
    $cmp_input.= "onclick=\"this.form.return_module.value='Campaigns';";
    $cmp_input.= "this.form.module.value='Campaigns';";
    $cmp_input.= "this.form.action.value='RoiDetailView';";
    $cmp_input.= "this.form.return_action.value='WizardHome';";
    $cmp_input.= "this.form.record.value='".$focus->id."';";
    $cmp_input.= "this.form.return_id.value='".$focus->id."';\" "; 
    $cmp_input.= "class='button' value='".$mod_strings['LBL_TRACK_ROI_BUTTON_LABEL']."' type='submit'>";
          
    //Create Campaign Header    
    $cmpgn_tbl = "<p><table class='edit view' width='100%' border='0' cellspacing='0' cellpadding='0'>";
    $cmpgn_tbl .= "<tr><td class='dataField' align='left'><h4 class='dataLabel'> ".$mod_strings['LBL_LIST_CAMPAIGN_NAME'].'  '. $mod_strings['LBL_WIZ_NEWSLETTER_TITLE_SUMMARY']." </h4></td>";
    $cmpgn_tbl .= "<td align='right'>$cmp_input</td></tr>";
    $colorclass = '';
    foreach($fields as $key){

                if(!empty($focus->$key) && !empty($mod_strings[$focus->field_name_map[$key]['vname']])){
                    $cmpgn_tbl .= "<tr><td scope='row' width='15%'>".$mod_strings[$focus->field_name_map[$key]['vname']]."</td>\n";
                    if($key == 'team_name') {
					   require_once('modules/Teams/TeamSetManager.php');
					   $cmpgn_tbl .= "<td scope='row'>".TeamSetManager::getCommaDelimitedTeams($focus->team_set_id, $focus->team_id, true)."</td></tr>\n";
		            } else {
                       $cmpgn_tbl .= "<td scope='row'>".$focus->$key."</td></tr>\n";
                    }
                }            
    }
    $cmpgn_tbl .= "</table></p>";
    
    return $cmpgn_tbl ;
}

function create_marketing_summary  ($focus){
    global $mod_strings,$app_strings;
    $colorclass = '';
    
    //create new marketing button input
    $new_mrkt_input =  "<input id='wiz_new_mrkt_button' name='SUBMIT' ";  
    $new_mrkt_input .= "onclick=\"this.form.return_module.value='Campaigns';";
    $new_mrkt_input .= "this.form.module.value='Campaigns';";
    $new_mrkt_input .= "this.form.record.value='';";
    $new_mrkt_input .= "this.form.return_module.value='Campaigns';";    
    $new_mrkt_input .= "this.form.action.value='WizardMarketing';";
    $new_mrkt_input .= "this.form.return_action.value='WizardHome';";
    $new_mrkt_input .= "this.form.direct_step.value='1';";
    $new_mrkt_input .= "this.form.campaign_id.value='".$focus->id."';";
    $new_mrkt_input .= "this.form.return_id.value='".$focus->id."';\" "; 
    $new_mrkt_input .= "class='button' value='".$mod_strings['LBL_CREATE_NEW_MARKETING_EMAIL']."' type='submit'>";        
       
        //create marketing email table
    $mrkt_tbl='';
    
    $focus->load_relationship('emailmarketing');
    $mrkt_lists = $focus->emailmarketing->get();
    
    
    $mrkt_tbl = "<p><table  class='list view' width='100%' border='0' cellspacing='1' cellpadding='1'>";
    $mrkt_tbl .= "<tr class='detail view'><td colspan='3'><h4> ".$mod_strings['LBL_WIZ_MARKETING_TITLE']." </h4></td>" .
                 "<td colspan=2 align='right'>$new_mrkt_input</td></tr>";
    $mrkt_tbl .= "<tr  class='listViewHRS1'><td scope='col' width='15%'><b>".$mod_strings['LBL_MRKT_NAME']."</b></td><td width='15%' scope='col'><b>".$mod_strings['LBL_FROM_MAILBOX_NAME']."</b></td><td width='15%' scope='col'><b>".$mod_strings['LBL_STATUS_TEXT']."</b></td><td scope='col' colspan=2>&nbsp;</td></tr>";
    
    if(count($mrkt_lists)>0){
    
            
            $mrkt_focus = new EmailMarketing();
            foreach($mrkt_lists as $mrkt_id){
                $mrkt_focus->retrieve($mrkt_id);    
    
                //create send test marketing button input
                $test_mrkt_input =  "<input id='wiz_new_mrkt_button' name='SUBMIT'  ";  
                $test_mrkt_input .= "onclick=\"this.form.return_module.value='Campaigns'; ";
                $test_mrkt_input .= "this.form.module.value='Campaigns'; ";
                $test_mrkt_input .= "this.form.record.value='';";
                $test_mrkt_input .= "this.form.return_module.value='Campaigns'; ";    
                $test_mrkt_input .= "this.form.action.value='QueueCampaign'; ";
                $test_mrkt_input .= "this.form.return_action.value='WizardHome'; ";
                $test_mrkt_input .= "this.form.wiz_mass.value='".$mrkt_focus->id."'; ";
                $test_mrkt_input .= "this.form.mode.value='test'; ";    
                $test_mrkt_input .= "this.form.direct_step.value='1'; ";
                $test_mrkt_input .= "this.form.record.value='".$focus->id."'; ";
                $test_mrkt_input .= "this.form.return_id.value='".$focus->id."';\" "; 
                $test_mrkt_input .= "class='button' value='".$mod_strings['LBL_TEST_BUTTON_LABEL']."' type='submit'>";        
            
                //create send marketing button input
                $send_mrkt_input =  "<input id='wiz_new_mrkt_button' name='SUBMIT'  ";  
                $send_mrkt_input .= "onclick=\"this.form.return_module.value='Campaigns'; ";
                $send_mrkt_input .= "this.form.module.value='Campaigns'; ";
                $send_mrkt_input .= "this.form.record.value='';";
                $send_mrkt_input .= "this.form.return_module.value='Campaigns'; ";    
                $send_mrkt_input .= "this.form.action.value='QueueCampaign'; ";
                $send_mrkt_input .= "this.form.return_action.value='WizardHome'; ";
                $send_mrkt_input .= "this.form.wiz_mass.value='".$mrkt_focus->id."'; ";
                $send_mrkt_input .= "this.form.mode.value='send'; ";    
                $send_mrkt_input .= "this.form.direct_step.value='1'; ";
                $send_mrkt_input .= "this.form.record.value='".$focus->id."'; ";
                $send_mrkt_input .= "this.form.return_id.value='".$focus->id."';\" "; 
                $send_mrkt_input .= "class='button' value='".$mod_strings['LBL_SEND_EMAIL']."' type='submit'>";        
            
    
    
             if( $colorclass== "class='evenListRowS1'"){
                    $colorclass= "class='oddListRowS1'";
                }else{ 
                    $colorclass= "class='evenListRowS1'";
                }        
    
              if(isset($mrkt_focus->name) && !empty($mrkt_focus->name)){
                $mrkt_tbl  .= "<tr $colorclass>";
                $mrkt_tbl  .= "<td scope='row' width='40%'><a href='index.php?action=WizardMarketing&module=Campaigns&return_module=Campaigns&return_action=WizardHome";
                $mrkt_tbl  .= "&return_id=" .$focus->id. "&campaign_id=" .$focus->id."&record=".$mrkt_focus->id."'>".$mrkt_focus->name."</a></td>";
                $mrkt_tbl  .= "<td scope='row' width='25%'>".$mrkt_focus->from_name."</td>";
                $mrkt_tbl  .= "<td scope='row' width='15%'>".$mrkt_focus->status."</td>";
                $mrkt_tbl  .= "<td scope='row' width='10%'>$test_mrkt_input</td>";
                $mrkt_tbl  .= "<td scope='row' width='10%'>$send_mrkt_input</td>";
                $mrkt_tbl  .= "</tr>";
          
              }
            }        
    }else{
        $mrkt_tbl  .= "<tr><td colspan='3'>".$mod_strings['LBL_NONE']."</td></tr>";   
    }
    $mrkt_tbl .= "</table></p>";
    return $mrkt_tbl ;
}

function create_target_summary  ($focus){
    global $mod_strings,$app_strings,$app_list_strings;
    $colorclass = '';
    $camp_type = $focus->campaign_type;


    //create schedule table
    $pltbl='';
    //set the title based on campaign type
    $target_title = $mod_strings['LBL_TARGET_LISTS'];
    if($camp_type=='NewsLetter'){
        $target_title = $mod_strings['LBL_NAVIGATION_MENU_SUBSCRIPTIONS'];
    }
    
    
    $focus->load_relationship('prospectlists');
    $pl_lists = $focus->prospectlists->get();

    $pl_tbl = "<p><table align='center' class='list view' width='100%' border='0' cellspacing='1' cellpadding='1'>";
    $pl_tbl .= "<tr class='detail view'><td colspan='4'><h4> ".$target_title." </h4></td></tr>";
    $pl_tbl .= "<tr class='listViewHRS1'><td width='50%' scope='col'><b>".$mod_strings['LBL_LIST_NAME']."</b></td><td width='30%' scope='col'><b>".$mod_strings['LBL_LIST_TYPE']."</b></td>";
    $pl_tbl .= "<td width='15%' scope='col'><b>".$mod_strings['LBL_TOTAL_ENTRIES']."</b></td><td width='5%' scope='col'>&nbsp;</td></tr>";
   
    if(count($pl_lists)>0){

            
            $pl_focus = new ProspectList();
            foreach($pl_lists as $pl_id){
                
             if( $colorclass== "class='evenListRowS1'"){
                    $colorclass= "class='oddListRowS1'";
                }else{ 
                    $colorclass= "class='evenListRowS1'";
                }         
                                    
              $pl_focus->retrieve($pl_id);
              //set the list type if this is a newsletter
              $type=$pl_focus->list_type;    
              if($camp_type=='NewsLetter'){ 
                  if (($pl_focus->list_type == 'default') || ($pl_focus->list_type == 'seed')){$type = $mod_strings['LBL_SUBSCRIPTION_TYPE_NAME'];}
                  if($pl_focus->list_type == 'exempt'){$type = $mod_strings['LBL_UNSUBSCRIPTION_TYPE_NAME'];}
                  if($pl_focus->list_type == 'test'){$type = $mod_strings['LBL_TEST_TYPE_NAME'];}
              }else{
                $type = $app_list_strings['prospect_list_type_dom'][$pl_focus->list_type];
              }
              if(isset($pl_focus->id) && !empty($pl_focus->id)){
                $pl_tbl  .= "<tr $colorclass>";
                $pl_tbl  .= "<td scope='row' width='50%'><a href='index.php?action=DetailView&module=ProspectLists&return_module=Campaigns&return_action=WizardHome&return_id=" .$focus->id. "&record=".$pl_focus->id."'>";
                $pl_tbl  .=  $pl_focus->name."</a></td>";
                $pl_tbl  .= "<td scope='row' width='30%'>$type</td>";
                $pl_tbl  .= "<td scope='row' width='15%'>".$pl_focus->get_entry_count()."</td>";
                $pl_tbl  .= "<td scope='row' width='5%' align='right'><a href='index.php?action=EditView&module=ProspectLists&return_module=Campaigns&return_action=WizardHome&return_id=" .$focus->id. "&record=".$pl_focus->id."'>";
                $pl_tbl  .= SugarThemeRegistry::current()->getImage('edit_inline', 'border=0', null, null, ".gif", $mod_strings['LBL_EDIT_INLINE']) . "</a>&nbsp;";


                $pl_tbl  .= "<a href='index.php?action=DetailView&module=ProspectLists&return_module=Campaigns&return_action=WizardHome&return_id=" .$focus->id. "&record=".$pl_focus->id."'>";
                $pl_tbl  .= SugarThemeRegistry::current()->getImage('view_inline', 'border=0', null, null, ".gif", $mod_strings['LBL_VIEW_INLINE'])."</a></td>";


              }
            }        
    }else{
     $pl_tbl .= "<tr><td class='$colorclass' scope='row' colspan='2'>".$mod_strings['LBL_NONE']."</td></tr>";   
    }

    $pl_tbl .= "</table></p>";
    return $pl_tbl;        
    
}

function create_tracker_summary  ($focus){
    global $mod_strings,$app_strings;
    $colorclass = '';
    $trkr_tbl='';
    //create tracker table
    $focus->load_relationship('tracked_urls');
    $trkr_lists = $focus->tracked_urls->get();

    $trkr_tbl = "<p><table align='center' class='list view' width='100%' border='0' cellspacing='1' cellpadding='1'>";
    $trkr_tbl .= "<tr class='detail view'><td colspan='6'><h4> ".$mod_strings['LBL_NAVIGATION_MENU_TRACKERS']." </h4></td></tr>";
    $trkr_tbl .= "<tr class='listViewHRS1'><td width='15%' scope='col'><b>".$mod_strings['LBL_EDIT_TRACKER_NAME']."</b></td><td width='15%' scope='col'><b>".$mod_strings['LBL_EDIT_TRACKER_URL']."</b></td><td width='15%' scope='col'><b>".$mod_strings['LBL_EDIT_OPT_OUT']."</b></td></tr>";
    
    if(count($trkr_lists)>0){
            
        foreach($trkr_lists as $trkr_id){
             if( $colorclass== "class='evenListRowS1'"){
                    $colorclass= "class='oddListRowS1'";
                }else{ 
                    $colorclass= "class='evenListRowS1'";
                }        
            
            
            $ct_focus = new CampaignTracker();
            $ct_focus->retrieve($trkr_id);
          if(isset($ct_focus->tracker_name) && !empty($ct_focus->tracker_name)){
            if($ct_focus->is_optout){$opt = 'checked';}else{$opt = '';}
            $trkr_tbl  .= "<tr $colorclass>";
            $trkr_tbl  .= "<td scope='row' ><a href='index.php?action=DetailView&module=CampaignTrackers&return_module=Campaigns&return_action=WizardHome&return_id=" .$focus->id. "&record=".$ct_focus->id."'>";
            $trkr_tbl  .= $ct_focus->tracker_name."</a></td>";
            $trkr_tbl  .= "<td scope='row' width='15%'>".$ct_focus->tracker_url."</td>";
            $trkr_tbl  .= "<td scope='row' width='15%'>&nbsp;&nbsp;<input type='checkbox' class='checkbox' $opt disabled></td>";
            $trkr_tbl  .= "</tr>";
          }
        }
    }else{
        $trkr_tbl  .= "<tr ><td colspan='3'>".$mod_strings['LBL_NONE']."</td>";
    }
    $trkr_tbl .= "</table></p>";
    return $trkr_tbl ;
    
}


function create_wiz_menu_items($type,$mrkt_string,$camp_url,$summ_url){
    global $mod_strings;
    
    $steps[$mod_strings['LBL_NAVIGATION_MENU_GEN1']]          = file_exists('custom/modules/Campaigns/tpls/WizardCampaignHeader.tpl') ? 'custom/modules/Campaigns/tpls/WizardCampaignHeader.tpl' : 'modules/Campaigns/tpls/WizardCampaignHeader.tpl';
    $steps[$mod_strings['LBL_NAVIGATION_MENU_GEN2']]          = file_exists('custom/modules/Campaigns/tpls/WizardCampaignBudget.tpl') ? 'custom/modules/Campaigns/tpls/WizardCampaignBudget.tpl' : 'modules/Campaigns/tpls/WizardCampaignBudget.tpl';

    //do not show tracker step for general campaigns (only for newsletter/email)
    if($type != 'general'){
        $steps[$mod_strings['LBL_NAVIGATION_MENU_TRACKERS']]      = file_exists('custom/modules/Campaigns/tpls/WizardCampaignTracker.tpl') ? 'custom/modules/Campaigns/tpls/WizardCampaignTracker.tpl' : 'modules/Campaigns/tpls/WizardCampaignTracker.tpl';
    }

    if($type == 'newsletter'){
        $steps[$mod_strings['LBL_NAVIGATION_MENU_SUBSCRIPTIONS']] = file_exists('custom/modules/Campaigns/tpls/WizardCampaignTargetList.tpl') ? 'custom/modules/Campaigns/tpls/WizardCampaignTargetList.tpl' : 'modules/Campaigns/tpls/WizardCampaignTargetList.tpl';
    }else{
        $steps[$mod_strings['LBL_TARGET_LISTS']]                  = file_exists('custom/modules/Campaigns/tpls/WizardCampaignTargetListForNonNewsLetter.tpl') ? 'custom/modules/Campaigns/tpls/WizardCampaignTargetListForNonNewsLetter.tpl' : 'modules/Campaigns/tpls/WizardCampaignTargetListForNonNewsLetter.tpl';
    }    

    $nav_html = '<table border="0" cellspacing="0" cellpadding="0" width="100%" >';
    if(isset($steps)  && !empty($steps)){
        $i=1;
        foreach($steps as $name=>$step){
            $nav_html .= "<tr><td scope='row' nowrap><div id='nav_step$i'><a href='".$camp_url.$i."'>$name</a></div></td></tr>";
            $i=$i+1;
        }
    }

    if($type == 'newsletter'  ||  $type == 'email'){
        $nav_html .= "<td scope='row' nowrap><div id='nav_step'".($i+1).">$mrkt_string</div></td></tr>";
        $nav_html .= "<td scope='row' nowrap><div id='nav_step'".($i+2).">".$mod_strings['LBL_NAVIGATION_MENU_SEND_EMAIL']."</div></td></tr>";
        $nav_html .= "<td scope='row' nowrap><div id='nav_step'".($i+3).">".$summ_url."</div></td></tr>";
    }else{
     $nav_html .= "<td scope='row' nowrap><div id='nav_step'".($i+1).">".$summ_url."</div></td></tr>";   
    }
    
    $nav_html .= '</table>';
  
    return $nav_html;
}

?>

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

/**************************** general UI Stuff *******************/



require_once('modules/Campaigns/utils.php');


global $app_strings;
global $timedate;
global $app_list_strings;
global $mod_strings;
global $current_user;
global $sugar_version, $sugar_config;


/**************************** GENERAL SETUP WORK*******************/
$campaign_focus = new Campaign();
if (isset($_REQUEST['campaign_id']) && !empty($_REQUEST['campaign_id'])) {
    $campaign_focus->retrieve($_REQUEST['campaign_id']);
}else{
    sugar_die($app_strings['ERROR_NO_RECORD']);
}

global $theme;



$json = getJSONobj();

$GLOBALS['log']->info("Wizard Continue Create Wizard");
 if($campaign_focus->campaign_type=='NewsLetter'){
    echo getClassicModuleTitle($mod_strings['LBL_MODULE_NAME'], array($mod_strings['LBL_NEWSLETTER WIZARD_TITLE'].' '.$campaign_focus->name), true);
 }else{
    echo getClassicModuleTitle($mod_strings['LBL_MODULE_NAME'], array($mod_strings['LBL_CAMPAIGN'].' '.$campaign_focus->name), true);
 }

$ss = new Sugar_Smarty();
$ss->assign("MOD", $mod_strings);
$ss->assign("APP", $app_strings);
if (isset($_REQUEST['return_module'])) $ss->assign("RETURN_MODULE", $_REQUEST['return_module']);
if (isset($_REQUEST['return_action'])) $ss->assign("RETURN_ACTION", $_REQUEST['return_action']);
if (isset($_REQUEST['return_id'])) $ss->assign("RETURN_ID", $_REQUEST['return_id']);
// handle Create $module then Cancel
$ss->assign('CAMPAIGN_ID', $campaign_focus->id);

$seps = get_number_seperators();
$ss->assign("NUM_GRP_SEP", $seps[0]);
$ss->assign("DEC_SEP", $seps[1]);


/**************************** MARKETING UI DIV Stuff *******************/
//$campaign_focus->load_relationship('emailmarketing');
//$mrkt_ids = $campaign_focus->emailmarketing->get();

$mrkt_focus = new EmailMarketing();

//if record param exists and it is not empty, then retrieve this bean
if(isset($_REQUEST['record']) and !empty($_REQUEST['record'])){
    $mrkt_focus->retrieve($_REQUEST['record']);
}else{
        //check to see if this campaign has an email marketing already attached, and if so, create duplicate
        $campaign_focus->load_relationship('emailmarketing');
        $mrkt_lists = $campaign_focus->emailmarketing->get();
        if(!empty($mrkt_lists)){
            //reverse array so we always use the most recent one:
            $mrkt_lists = array_reverse($mrkt_lists);
            $mrkt_focus->retrieve($mrkt_lists[0]);
            $mrkt_focus->id = '';
            $mrkt_focus->name = $mod_strings['LBL_COPY_OF'] . ' '. $mrkt_focus->name;
        }

}


$ss->assign("CALENDAR_LANG", "en");
$ss->assign("USER_DATEFORMAT", '('. $timedate->get_user_date_format().')');
$ss->assign("CALENDAR_DATEFORMAT", $timedate->get_cal_date_format());
$ss->assign("TIME_MERIDIEM", $timedate->AMPMMenu('', $mrkt_focus->time_start));
$ss->assign("MRKT_ID", $mrkt_focus->id);
$ss->assign("MRKT_NAME", $mrkt_focus->name);
$ss->assign("MRKT_FROM_NAME", $mrkt_focus->from_name);
$ss->assign("MRKT_FROM_ADDR", $mrkt_focus->from_addr);
$def = $mrkt_focus->getFieldDefinition('from_name');
$ss->assign("MRKT_FROM_NAME_LEN", $def['len']);

//jc: bug 15498
// assigning the length of the reply name from the var defs to the template to be used
// as the max length for the input field
$def = $mrkt_focus->getFieldDefinition('reply_to_name');
$ss->assign("MRKT_REPLY_NAME_LEN", $def['len']);
$ss->assign("MRKT_REPLY_NAME", $mrkt_focus->reply_to_name);
$def = $mrkt_focus->getFieldDefinition('reply_to_addr');
$ss->assign("MRKT_REPLY_ADDR_LEN", $def['len']);
// end bug 15498
$ss->assign("MRKT_REPLY_ADDR", $mrkt_focus->reply_to_addr);
$ss->assign("MRKT_DATE_START", $mrkt_focus->date_start);
$ss->assign("MRKT_TIME_START", $mrkt_focus->time_start);
//$_REQUEST['mass'] = $mrkt_focus->id;
$ss->assign("MRKT_ID", $mrkt_focus->id);
$emails=array();
$mailboxes=get_campaign_mailboxes($emails);

/*
 * get full array of stored options
 */
$IEStoredOptions = get_campaign_mailboxes_with_stored_options();
$IEStoredOptionsJSON = (!empty($IEStoredOptions)) ? $json->encode($IEStoredOptions, false) : 'new Object()';
$ss->assign("IEStoredOptions", $IEStoredOptionsJSON);

//add empty options.
$emails['']='nobody@example.com';
$mailboxes['']='';

//inbound_email_id
$default_email_address='nobody@example.com';
$from_emails = '';
foreach ($mailboxes as $id=>$name) {
    if (!empty($from_emails)) {
        $from_emails.=',';
    }
    if ($id=='') {
        $from_emails.="'EMPTY','$name','$emails[$id]'";
    } else {
        $from_emails.="'$id','$name','$emails[$id]'";
    }
}
$ss->assign("FROM_EMAILS",$from_emails);
$ss->assign("DEFAULT_FROM_EMAIL",$default_email_address);
$ss->assign("STATUS_OPTIONS", get_select_options_with_id($app_list_strings['email_marketing_status_dom'],$mrkt_focus->status));
if (empty($mrkt_focus->inbound_email_id)) {
    $ss->assign("MAILBOXES", get_select_options_with_id($mailboxes, ''));
} else {
    $ss->assign("MAILBOXES", get_select_options_with_id($mailboxes, $mrkt_focus->inbound_email_id));
}

$ss->assign("TIME_MERIDIEM", $timedate->AMPMMenu('', $mrkt_focus->time_start));
$ss->assign("TIME_FORMAT", '('. $timedate->get_user_time_format().')');

$email_templates_arr = get_bean_select_array(true, 'EmailTemplate','name','','name');
if($mrkt_focus->template_id) {
    $ss->assign("TEMPLATE_ID", $mrkt_focus->template_id);
    $ss->assign("EMAIL_TEMPLATE_OPTIONS", get_select_options_with_id($email_templates_arr, $mrkt_focus->template_id));
    $ss->assign("EDIT_TEMPLATE","visibility:inline");
}
else {
    $ss->assign("EMAIL_TEMPLATE_OPTIONS", get_select_options_with_id($email_templates_arr, ""));
    $ss->assign("EDIT_TEMPLATE","visibility:hidden");
}


$scope_options=get_message_scope_dom($campaign_focus->id,$campaign_focus->name,$mrkt_focus->db);
$prospectlists=array();
if (isset($mrkt_focus->all_prospect_lists) && $mrkt_focus->all_prospect_lists==1) {
    $ss->assign("ALL_PROSPECT_LISTS_CHECKED","checked");
    $ss->assign("MESSAGE_FOR_DISABLED","disabled");
}
else {
    //get select prospect list.
    if (!empty($mrkt_focus->id)) {
        $mrkt_focus->load_relationship('prospectlists');
        $prospectlists=$mrkt_focus->prospectlists->get();
    };
}
if (empty($prospectlists)) $prospectlists=array();
if (empty($scope_options)) $scope_options=array();
$ss->assign("SCOPE_OPTIONS", get_select_options_with_id($scope_options, $prospectlists));
$ss->assign("SAVE_CONFIRM_MESSAGE", $mod_strings['LBL_CONFIRM_SEND_SAVE']);



$javascript = new javascript();
$javascript->setFormName('wizform');
$javascript->setSugarBean($mrkt_focus);
$javascript->addAllFields('');
echo $javascript->getScript();

/**************************** Final Step UI DIV *******************/

    //Grab the prospect list of type default
    $default_pl_focus = ' ';
        $campaign_focus->load_relationship('prospectlists');
        $prospectlists=$campaign_focus->prospectlists->get();

    
    $pl_count = 0;
    $pl_lists = 0;
    if(!empty($prospectlists)){
        foreach ($prospectlists as $prospect_id){
            $pl_focus = new ProspectList();
            $pl_focus->retrieve($prospect_id);

            if (($pl_focus->list_type == 'default') || ($pl_focus->list_type == 'seed')){
                $default_pl_focus= $pl_focus;
                // get count of all attached target types
                $pl_count = $default_pl_focus->get_entry_count();
             }
             $pl_lists = $pl_lists+1;
        }


    }
    //if count is 0, then hide inputs and and print warning message
    if ($pl_count==0){
        if ($pl_lists==0){
            //print no target list warning
            $ss->assign("WARNING_MESSAGE", $mod_strings['LBL_NO_TARGETS_WARNING']);
        }else{
            //print no entries warning
            if($campaign_focus->campaign_type='NewsLetter'){
                $ss->assign("WARNING_MESSAGE", $mod_strings['LBL_NO_SUBS_ENTRIES_WARNING']);
            }else{
               $ss->assign("WARNING_MESSAGE", $mod_strings['LBL_NO_TARGET_ENTRIES_WARNING']);
            }
        }
        //disable the send email options
        $ss->assign("PL_DISABLED",'disabled');

    }else{
        //show inputs and assign type to be radio
    }



/**************************** WIZARD UI DIV Stuff *******************/

$camp_url = "index.php?action=WizardNewsletter&module=Campaigns&return_module=Campaigns&return_action=WizardHome";
$camp_url .= "&return_id=".$campaign_focus->id."&record=".$campaign_focus->id."&direct_step=";
$ss->assign("CAMP_WIZ_URL", $camp_url);
    $summ_url = $mod_strings['LBL_NAVIGATION_MENU_SUMMARY'];
    if(!empty($focus->id)){
        $summ_url = "<a href='index.php?action=WizardHome&module=Campaigns";
        $summ_url .= "&return_id=".$focus->id."&record=".$focus->id;
        $summ_url .= "'> ". $mod_strings['LBL_NAVIGATION_MENU_SUMMARY']."</a>";
    }
$summ_url = $mod_strings['LBL_NAVIGATION_MENU_SUMMARY'];
if(!empty($focus->id)){
    $summ_url = "index.php?action=WizardHome&module=Campaigns&return_id=".$focus->id."&record=".$focus->id;
}
$ss->assign("SUMM_URL", $summ_url);

//  this is the wizard control script that resides in page
 $divScript = <<<EOQ

 <script type="text/javascript" language="javascript">
    /*
     * this is the custom validation script that will call the right validation for each div
     */
    function validate_wiz_form(step){
        switch (step){
            case 'step1':
            return check_form('wizform');
            break;
            default://no additional validation needed
        }
        return true;

    }

    showfirst('marketing')
</script>
EOQ;

//$ss->assign("WIZ_JAVASCRIPT", print_wizard_jscript());
$ss->assign("DIV_JAVASCRIPT", $divScript);





/**************************** FINAL END OF PAGE UI Stuff *******************/

      $ss->display('modules/Campaigns/WizardMarketing.html');
?>

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

/******** general UI Stuff ***********/



require_once('modules/Campaigns/utils.php');


global $app_strings;
global $timedate;
global $app_list_strings;
global $mod_strings;
global $current_user;
global $sugar_version, $sugar_config;


/*************** GENERAL SETUP WORK **********/

$focus = new Campaign();
if(isset($_REQUEST['record'])) {
    $focus->retrieve($_REQUEST['record']);
}
if(isset($_REQUEST['isDuplicate']) && $_REQUEST['isDuplicate'] == 'true') {
    $focus->id = "";
}
global $theme;



$json = getJSONobj();

$GLOBALS['log']->info("Campaign NewsLetter Wizard");

if( (isset($_REQUEST['wizardtype'])  && $_REQUEST['wizardtype']==1)  ||  ($focus->campaign_type=='NewsLetter')){
    echo getClassicModuleTitle($mod_strings['LBL_MODULE_NAME'], array($mod_strings['LBL_NEWSLETTER WIZARD_TITLE'].$focus->name), true, false);
}else{
    echo getClassicModuleTitle($mod_strings['LBL_MODULE_NAME'], array($mod_strings['LBL_CAMPAIGN'].$focus->name), true, false);   
}


$ss = new Sugar_Smarty();
$ss->assign("MOD", $mod_strings);
$ss->assign("APP", $app_strings);

if (isset($_REQUEST['return_module'])) $ss->assign("RETURN_MODULE", $_REQUEST['return_module']);
if (isset($_REQUEST['return_action'])) $ss->assign("RETURN_ACTION", $_REQUEST['return_action']);
if (isset($_REQUEST['return_id'])) $ss->assign("RETURN_ID", $_REQUEST['return_id']);
// handle Create $module then Cancel
if (empty($_REQUEST['return_id'])) {
    $ss->assign("RETURN_ACTION", 'index');
}
$ss->assign("PRINT_URL", "index.php?".$GLOBALS['request_string']);

require_once('include/QuickSearchDefaults.php');
$qsd = QuickSearchDefaults::getQuickSearchDefaults();
$qsd->setFormName('wizform');
$sqs_objects = array('parent_name' => $qsd->getQSParent(), 
                    'assigned_user_name' => $qsd->getQSUser(),
                    //'prospect_list_name' => getProspectListQSObjects(),
                    'test_name' => getProspectListQSObjects('prospect_list_type_test', 'test_name','wiz_step3_test_name_id'),
                    'unsubscription_name' => getProspectListQSObjects('prospect_list_type_exempt', 'unsubscription_name','wiz_step3_unsubscription_name_id'),
                    'subscription_name' => getProspectListQSObjects('prospect_list_type_default', 'subscription_name','wiz_step3_subscription_name_id'),
                    );
                    

$quicksearch_js = '<script type="text/javascript" language="javascript">sqs_objects = ' . $json->encode($sqs_objects) . '</script>';

$ss->assign("JAVASCRIPT", $quicksearch_js);


//set the campaign type based on wizardtype value from request object
$campaign_type = 'newsletter';
if( (isset($_REQUEST['wizardtype'])  && $_REQUEST['wizardtype']==1)  ||  ($focus->campaign_type=='NewsLetter')){
    $campaign_type = 'newsletter';
    $ss->assign("CAMPAIGN_DIAGNOSTIC_LINK", diagnose());    
}elseif( (isset($_REQUEST['wizardtype'])  && $_REQUEST['wizardtype']==2)  || ($focus->campaign_type=='Email') ){
    $campaign_type = 'email';
    $ss->assign("CAMPAIGN_DIAGNOSTIC_LINK", diagnose());
}else{
    $campaign_type = 'general';
}


//******** CAMPAIGN HEADER AND BUDGET UI DIV Stuff (both divs) **********/
/// Users Popup
$popup_request_data = array(
    'call_back_function' => 'set_return',
    'form_name' => 'wizform',
    'field_to_name_array' => array(
        'id' => 'assigned_user_id',
        'user_name' => 'assigned_user_name',
        ),
    );
$ss->assign('encoded_users_popup_request_data', $json->encode($popup_request_data));


//set default values
$ss->assign("CALENDAR_LANG", "en");
$ss->assign("USER_DATEFORMAT", '('. $timedate->get_user_date_format().')');
$ss->assign("CALENDAR_DATEFORMAT", $timedate->get_cal_date_format());
$ss->assign("CAMP_DATE_ENTERED", $focus->date_entered);
$ss->assign("CAMP_DATE_MODIFIED", $focus->date_modified);
$ss->assign("CAMP_CREATED_BY", $focus->created_by_name);
$ss->assign("CAMP_MODIFIED_BY", $focus->modified_by_name);
$ss->assign("ID", $focus->id);
$ss->assign("CAMP_TRACKER_TEXT", $focus->tracker_text);
$ss->assign("CAMP_START_DATE", $focus->start_date);
$ss->assign("CAMP_END_DATE", $focus->end_date);
$ss->assign("CAMP_BUDGET", $focus->budget);
$ss->assign("CAMP_ACTUAL_COST", $focus->actual_cost);
$ss->assign("CAMP_EXPECTED_REVENUE", $focus->expected_revenue);
$ss->assign("CAMP_EXPECTED_COST", $focus->expected_cost);
$ss->assign("CAMP_OBJECTIVE", $focus->objective);
$ss->assign("CAMP_CONTENT", $focus->content);
$ss->assign("CAMP_NAME", $focus->name);
$ss->assign("CAMP_RECORD", $focus->id);
$ss->assign("CAMP_IMPRESSIONS", $focus->impressions);
if (empty($focus->assigned_user_id) && empty($focus->id))  $focus->assigned_user_id = $current_user->id;
if (empty($focus->assigned_name) && empty($focus->id))  $focus->assigned_user_name = $current_user->user_name;
$ss->assign("ASSIGNED_USER_OPTIONS", get_select_options_with_id(get_user_array(TRUE, "Active", $focus->assigned_user_id), $focus->assigned_user_id));
$ss->assign("ASSIGNED_USER_NAME", $focus->assigned_user_name);
$ss->assign("ASSIGNED_USER_ID", $focus->assigned_user_id );

if((!isset($focus->status)) && (!isset($focus->id)))
    $ss->assign("STATUS_OPTIONS", get_select_options_with_id($app_list_strings['campaign_status_dom'], 'Planning'));
else
    $ss->assign("STATUS_OPTIONS", get_select_options_with_id($app_list_strings['campaign_status_dom'], $focus->status));

//hide frequency options if this is not a newsletter
if($campaign_type == 'newsletter'){
    $ss->assign("HIDE_FREQUENCY_IF_NEWSLETTER", "Select");
    $ss->assign("FREQUENCY_LABEL", $mod_strings['LBL_CAMPAIGN_FREQUENCY']);
    if((!isset($focus->frequency)) && (!isset($focus->id))){
        $ss->assign("FREQ_OPTIONS", get_select_options_with_id($app_list_strings['newsletter_frequency_dom'], 'Monthly'));
    }else{
        $ss->assign("FREQ_OPTIONS", get_select_options_with_id($app_list_strings['newsletter_frequency_dom'], $focus->frequency));
    }
}else{
    $ss->assign("HIDE_FREQUENCY_IF_NEWSLETTER", "input type='hidden'");
    $ss->assign("FREQUENCY_LABEL", '&nbsp;');
}
global $current_user;
require_once('modules/Currencies/ListCurrency.php');
$currency = new ListCurrency();
if(isset($focus->currency_id) && !empty($focus->currency_id)){
    $selectCurrency = $currency->getSelectOptions($focus->currency_id);
    $ss->assign("CURRENCY", $selectCurrency);
}
else if($current_user->getPreference('currency') && !isset($focus->id))
{
    $selectCurrency = $currency->getSelectOptions($current_user->getPreference('currency'));
    $ss->assign("CURRENCY", $selectCurrency);
}else{

    $selectCurrency = $currency->getSelectOptions();
    $ss->assign("CURRENCY", $selectCurrency);

}
global $current_user;
if(is_admin($current_user) && $_REQUEST['module'] != 'DynamicLayout' && !empty($_SESSION['editinplace'])){  
    $record = '';
    if(!empty($_REQUEST['record'])){
        $record =   $_REQUEST['record'];
    }
    $ss->assign("ADMIN_EDIT","<a href='index.php?action=index&module=DynamicLayout&from_action=".$_REQUEST['action'] ."&from_module=".$_REQUEST['module'] ."&record=".$record. "'>".SugarThemeRegistry::current()->getImage("EditLayout","border='0' align='bottom'",null,null,'.gif',$mod_strings['LBL_EDIT_LAYOUT'])."</a>");

}

echo $currency->getJavascript();

$seps = get_number_seperators();
$ss->assign("NUM_GRP_SEP", $seps[0]);
$ss->assign("DEC_SEP", $seps[1]);


//fill out the campaign type dropdown based on type of campaign being created
if($campaign_type == 'general'){
    //get regular campaign dom object and strip out entries for email and newsletter
    $myTypeOptionsArr = array();
    $OptionsArr = $app_list_strings['campaign_type_dom'];
    foreach($OptionsArr as $key=>$val){
        if($val =='Newsletter' || $val =='Email' || $val =='' ){
            //do not add   
        }else{
            $myTypeOptionsArr[$key] = $val;
        }
    }
    
    //now create select option html without the newsletter/email, or blank ('') options
    $type_option_html =' ';
    $selected = false;
    foreach($myTypeOptionsArr as $optionKey=>$optionName){
        //if the selected flag is set to true, then just populate
        if($selected){
            $type_option_html .="<option value='$optionKey' >$optionName</option>";
        }else{//if not selected yet, check to see if this option should be selected
            //if the campaign type is not empty, then select the retrieved type
            if(!empty($focus->campaign_type)){
                //check to see if key matches campaign type
                if($optionKey == $focus->campaign_type){
                    //mark as selected
                    $type_option_html .="<option value='$optionKey' selected>$optionName</option>";
                    //mark as selected for next time
                    $selected=true;
                }else{
                    //key does not match, just populate
                    $type_option_html .="<option value='$optionKey' >$optionName</option>";
                }
            }else{
            //since the campaign type is empty, then select first one                
                $type_option_html .="<option value='$optionKey' selected>$optionName</option>";    
                //mark as selected for next time
                $selected=true;
            }
        }    
    }
    //assign the modified dropdown for general campaign creation
    $ss->assign("CAMPAIGN_TYPE_OPTIONS", $type_option_html);
    $ss->assign("SHOULD_TYPE_BE_DISABLED", "select");    
}elseif($campaign_type == 'email'){
    //Assign Email as type of campaign being created an disable the select widget
    $ss->assign("CAMPAIGN_TYPE_OPTIONS", $mod_strings['LBL_EMAIL']);
    $ss->assign("SHOULD_TYPE_BE_DISABLED", "input type='hidden' value='Email'");
}else{
    //Assign NewsLetter as type of campaign being created an disable the select widget
    $ss->assign("CAMPAIGN_TYPE_OPTIONS", $mod_strings['LBL_NEWSLETTER']);
    $ss->assign("SHOULD_TYPE_BE_DISABLED", "input type='hidden' value='NewsLetter'");

}





/***************  TRACKER UI DIV Stuff ***************/
//retrieve the trackers
$focus->load_relationship('tracked_urls');

$trkr_lists = $focus->tracked_urls->get();
$trkr_html ='';    
$ss->assign('TRACKER_COUNT',count($trkr_lists));
if(count($trkr_lists)>0){
global $odd_bg, $even_bg, $hilite_bg;
    
    $trkr_count = 0;
//create the html to create tracker table
    foreach($trkr_lists as $trkr_id){
        $ct_focus = new CampaignTracker();
        $ct_focus->retrieve($trkr_id);
      if(isset($ct_focus->tracker_name) && !empty($ct_focus->tracker_name)){
            if($ct_focus->is_optout){$opt = 'checked';}else{$opt = '';}
            $trkr_html .= "<div id='existing_trkr".$trkr_count."'> <table width='100%' border='0' cellspacing='0' cellpadding='0'>" ;
            $trkr_html .= "<tr class='evenListRowS1'><td width='15%'><input name='wiz_step3_is_optout".$trkr_count."' title='".$mod_strings['LBL_EDIT_OPT_OUT'] . $trkr_count ."' id='existing_is_optout". $trkr_count ."' class='checkbox' type='checkbox' $opt  /><input name='wiz_step3_id".$trkr_count."' value='".$ct_focus->id."' id='existing_tracker_id". $trkr_count ."'type='hidden''/></td>";
            $trkr_html .= "<td width='40%'> <input id='existing_tracker_name". $trkr_count ."' type='text' size='20' maxlength='255' name='wiz_step3_tracker_name". $trkr_count ."' title='".$mod_strings['LBL_EDIT_TRACKER_NAME']. $trkr_count ."' value='".$ct_focus->tracker_name."' ></td>";
            $trkr_html .= "<td width='40%'><input type='text' size='60' maxlength='255' name='wiz_step3_tracker_url". $trkr_count ."' title='".$mod_strings['LBL_EDIT_TRACKER_URL']. $trkr_count ."' id='existing_tracker_url". $trkr_count ."' value='".$ct_focus->tracker_url."' ></td>";
            $trkr_html .= "<td><a href='#' onclick=\"javascript:remove_existing_tracker('existing_trkr".$trkr_count."','".$ct_focus->id."'); \" >  ";
            $trkr_html .= SugarThemeRegistry::current()->getImage('delete_inline', "border='0'  align='absmiddle'", 12, 12, ".gif", $mod_strings['LBL_DELETE'])."</a></td></tr></table></div>";



      }
      $trkr_count =$trkr_count+1;
    }
    
    $trkr_html .= "<div id='no_trackers'></div>";
    }else{
        $trkr_html .= "<div id='no_trackers'><table width='100%' border='0' cellspacing='0' cellpadding='0'><tr class='evenListRowS1'><td>".$mod_strings['LBL_NONE']."</td></tr></table></div>";
    }
    $ss->assign('EXISTING_TRACKERS', $trkr_html);









/************** SUBSCRIPTION UI DIV Stuff ***************/
//fill in popups for target list options
    $popup_request_data = array(
        'call_back_function' => 'set_return', 
        'form_name' => 'wizform',
        'field_to_name_array' => array(
            'id' => 'wiz_step3_subscription_name_id',
            'name' => 'wiz_step3_subscription_name',
            
            ),
        );

$json = getJSONobj();
$encoded_newsletter_popup_request_data = $json->encode($popup_request_data);
$ss->assign('encoded_subscription_popup_request_data', $encoded_newsletter_popup_request_data);

    $popup_request_data = array(
        'call_back_function' => 'set_return', 
        'form_name' => 'wizform',
        'field_to_name_array' => array(
            'id' => 'wiz_step3_unsubscription_name_id',
            'name' => 'unsubscription_name',
            
            ),
        );

$json = getJSONobj();
$encoded_newsletter_popup_request_data = $json->encode($popup_request_data);
$ss->assign('encoded_unsubscription_popup_request_data', $encoded_newsletter_popup_request_data);

    $popup_request_data = array(
        'call_back_function' => 'set_return', //set_return_and_save_background
        'form_name' => 'wizform',
        'field_to_name_array' => array(
            'id' => 'wiz_step3_test_name_id',
            'name' => 'test_name',
            
            ),
        );

$json = getJSONobj();
$encoded_newsletter_popup_request_data = $json->encode($popup_request_data);
$ss->assign('encoded_test_popup_request_data', $encoded_newsletter_popup_request_data);


    $popup_request_data = array(
        'call_back_function' => 'set_return_prospect_list', 
        'form_name' => 'wizform',
        'field_to_name_array' => array(
            'id' => 'popup_target_list_id',
            'name' => 'popup_target_list_name',
            'list_type' => 'popup_target_list_type',
            
            ),
        );

$json = getJSONobj();
$encoded_newsletter_popup_request_data = $json->encode($popup_request_data);
$ss->assign('encoded_target_list_popup_request_data', $encoded_newsletter_popup_request_data);

$ss->assign('TARGET_OPTIONS', get_select_options_with_id($app_list_strings['prospect_list_type_dom'], 'default'));

//retrieve the subscriptions
$focus->load_relationship('prospectlists');

$prospect_lists = $focus->prospectlists->get();

if((isset($_REQUEST['wizardtype']) && $_REQUEST['wizardtype'] ==1) || ($focus->campaign_type=='NewsLetter')){
 //this is a newsletter type campaign, fill in subscription values   

//if prospect lists are returned, then iterate through and populate form values
if(count($prospect_lists)>0){
    
    foreach($prospect_lists as $pl_id){
    //retrieve prospect list
     $pl = new ProspectList();   
     $pl->retrieve($pl_id);

      if(isset($pl->list_type) && !empty($pl->list_type)){
         //assign values based on type
         if(($pl->list_type == 'default') || ($pl->list_type == 'seed')){            
            $ss->assign('SUBSCRIPTION_ID', $pl->id);
            $ss->assign('SUBSCRIPTION_NAME', $pl->name);
         };
         if($pl->list_type == 'exempt'){
            $ss->assign('UNSUBSCRIPTION_ID', $pl->id);
            $ss->assign('UNSUBSCRIPTION_NAME', $pl->name);
         
         };
         if($pl->list_type == 'test'){
            $ss->assign('TEST_ID', $pl->id);
            $ss->assign('TEST_NAME', $pl->name);
         
         };
      }
     
    }
}



}else{
 //this is not a newlsetter campaign, so fill in target list table
    //create array for javascript, this will help to display the option text, not the value
    $dom_txt =' ';
    foreach($app_list_strings['prospect_list_type_dom'] as $key=>$val){
        $dom_txt .="if(trgt_type_text =='$key'){trgt_type_text='$val';}";
    }
    $ss->assign("PL_DOM_STMT", $dom_txt); 
    $trgt_count = 0;
    $trgt_html = ' ';
    if(count($prospect_lists)>0){
        
        foreach($prospect_lists as $pl_id){
        //retrieve prospect list
             $pl = new ProspectList();   
             $pl_focus = $pl->retrieve($pl_id);
             $trgt_html .= "<div id='existing_trgt".$trgt_count."'> <table class='tabDetailViewDL2' width='100%'>" ;
             $trgt_html .= "<td width='25%'> <input id='existing_target_name". $trgt_count ."' type='hidden' type='text' size='60' maxlength='255' name='existing_target_name". $trgt_count ."'  value='". $pl_focus->name."' >". $pl_focus->name."</td>";
             $trgt_html .= "<td width='25%'><input type='hidden' size='60' maxlength='255' name='existing_tracker_list_type". $trgt_count ."'   id='existing_tracker_list_type". $trgt_count ."' value='".$pl_focus->list_type."' >".$app_list_strings['prospect_list_type_dom'][$pl_focus->list_type];
             $trgt_html .= "<input type='hidden' name='added_target_id". $trgt_count ."' id='added_target_id". $trgt_count ."' value='". $pl_focus->id ."' ></td>";
             $trgt_html .= "<td><a href='#' onclick=\"javascript:remove_existing_target('existing_trgt".$trgt_count."','".$pl_focus->id."'); \" >  ";
             $trgt_html .= SugarThemeRegistry::current()->getImage('delete_inline', "border='0' align='absmiddle'", 12, 12, ".gif", $mod_strings['LBL_DELETE'])."</a></td></tr></table></div>";


    
          
          $trgt_count =$trgt_count +1;
        }
        
        $trgt_html  .= "<div id='no_targets'></div>";
    }else{
        $trgt_html  .= "<div id='no_targets'><table width='100%' border='0' cellspacing='0' cellpadding='0'><tr class='evenListRowS1'><td>".$mod_strings['LBL_NONE']."</td></tr></table></div>";
        
    }
    $ss->assign('EXISTING_TARGETS', $trgt_html );

}

    
/**************************** WIZARD UI DIV Stuff *******************/
$mrkt_string = $mod_strings['LBL_NAVIGATION_MENU_MARKETING'];
if(!empty($focus->id)){
    $mrkt_url = "<a  href='index.php?action=WizardMarketing&module=Campaigns&return_module=Campaigns&return_action=WizardHome";
    $mrkt_url .= "&return_id=".$focus->id."&campaign_id=".$focus->id;
    $mrkt_url .= "'>". $mrkt_string."</a>";
    $mrkt_string = $mrkt_url;
}
    $summ_url = $mod_strings['LBL_NAVIGATION_MENU_SUMMARY'];
    if(!empty($focus->id)){
        $summ_url = "<a  href='index.php?action=WizardHome&module=Campaigns";
        $summ_url .= "&return_id=".$focus->id."&record=".$focus->id;
        $summ_url .= "'> ". $mod_strings['LBL_NAVIGATION_MENU_SUMMARY']."</a>";
    } 
   


$script_to_call ='';
    if (!empty($focus->id)){
        $script_to_call = "link_navs(1,4);";
        if(isset($_REQUEST['direct_step']) and !empty($_REQUEST['direct_step'])){
            $script_to_call .='   direct('.$_REQUEST['direct_step'].');';
        }
    } 
    $ss->assign("HILITE_ALL", $script_to_call);


//  this is the wizard control script that resides in page    
 $divScript = <<<EOQ

 <script type="text/javascript" language="javascript">
   
    /*
     * this is the custom validation script that will call the right validation for each div
     */
    function validate_wiz_form(step){
        switch (step){
            case 'step1':
            if(!validate_step1()){return false;}
            break;
            case 'step2':
            if(!validate_step2()){return false;} 
            break;                  
            default://no additional validation needed      
        }
        return true;
    
    }

    showfirst('newsletter');
</script>
EOQ;

$ss->assign("DIV_JAVASCRIPT", $divScript);


$sshtml = ' ';
    $i = 1;

//Create the html to fill in the wizard steps

if($campaign_type == 'general'){
    $steps = create_campaign_steps();    
    $ss->assign('NAV_ITEMS',create_wiz_menu_items($steps,'campaign',$mrkt_string,$summ_url));
    $ss->assign('HIDE_CONTINUE','hidden');

}elseif($campaign_type == 'email'){
    $steps = create_email_steps();  
    $ss->assign('NAV_ITEMS',create_wiz_menu_items($steps,'email',$mrkt_string,$summ_url));
    $ss->assign('HIDE_CONTINUE','submit');
}else{
    $steps = create_newsletter_steps();  
    $ss->assign('NAV_ITEMS',create_wiz_menu_items($steps,'newsletter',$mrkt_string,$summ_url));
    $ss->assign('HIDE_CONTINUE','submit');
}

$ss->assign('TOTAL_STEPS', count($steps));
$sshtml = create_wiz_step_divs($steps,$ss);
$ss->assign('STEPS',$sshtml);
     	   	

/**************************** FINAL END OF PAGE UI Stuff *******************/

$ss->display(file_exists('custom/modules/Campaigns/tpls/WizardNewsletter.tpl') ? 'custom/modules/Campaigns/tpls/WizardNewsletter.tpl' : 'modules/Campaigns/tpls/WizardNewsletter.tpl');


function create_newsletter_steps(){
    global $mod_strings;
    $steps[$mod_strings['LBL_NAVIGATION_MENU_GEN1']]          = file_exists('custom/modules/Campaigns/tpls/WizardCampaignHeader.tpl') ? 'custom/modules/Campaigns/tpls/WizardCampaignHeader.tpl' : 'modules/Campaigns/tpls/WizardCampaignHeader.tpl';
    $steps[$mod_strings['LBL_NAVIGATION_MENU_GEN2']]          = file_exists('custom/modules/Campaigns/tpls/WizardCampaignBudget.tpl') ? 'custom/modules/Campaigns/tpls/WizardCampaignBudget.tpl' : 'modules/Campaigns/tpls/WizardCampaignBudget.tpl';
    $steps[$mod_strings['LBL_NAVIGATION_MENU_TRACKERS']]      = file_exists('custom/modules/Campaigns/tpls/WizardCampaignTracker.tpl') ? 'custom/modules/Campaigns/tpls/WizardCampaignTracker.tpl' : 'modules/Campaigns/tpls/WizardCampaignTracker.tpl';
    $steps[$mod_strings['LBL_NAVIGATION_MENU_SUBSCRIPTIONS']] = file_exists('custom/modules/Campaigns/tpls/WizardCampaignTargetList.tpl') ? 'custom/modules/Campaigns/tpls/WizardCampaignTargetList.tpl' : 'modules/Campaigns/tpls/WizardCampaignTargetList.tpl';
    return  $steps;
}

function create_campaign_steps(){
    global $mod_strings;
    $steps[$mod_strings['LBL_NAVIGATION_MENU_GEN1']]          = file_exists('custom/modules/Campaigns/tpls/WizardCampaignHeader.tpl') ? 'custom/modules/Campaigns/tpls/WizardCampaignHeader.tpl' : 'modules/Campaigns/tpls/WizardCampaignHeader.tpl';
    $steps[$mod_strings['LBL_NAVIGATION_MENU_GEN2']]          = file_exists('custom/modules/Campaigns/tpls/WizardCampaignBudget.tpl') ? 'custom/modules/Campaigns/tpls/WizardCampaignBudget.tpl' : 'modules/Campaigns/tpls/WizardCampaignBudget.tpl';
    $steps[$mod_strings['LBL_TARGET_LISTS']]                   = file_exists('custom/modules/Campaigns/tpls/WizardCampaignTargetListForNonNewsLetter.tpl') ? 'custom/modules/Campaigns/tpls/WizardCampaignTargetListForNonNewsLetter.tpl' : 'modules/Campaigns/tpls/WizardCampaignTargetListForNonNewsLetter.tpl';
    return  $steps;
}

function create_email_steps(){
    global $mod_strings;
    $steps[$mod_strings['LBL_NAVIGATION_MENU_GEN1']]          = file_exists('custom/modules/Campaigns/tpls/WizardCampaignHeader.tpl') ? 'custom/modules/Campaigns/tpls/WizardCampaignHeader.tpl' : 'modules/Campaigns/tpls/WizardCampaignHeader.tpl';
    $steps[$mod_strings['LBL_NAVIGATION_MENU_GEN2']]          = file_exists('custom/modules/Campaigns/tpls/WizardCampaignBudget.tpl') ? 'custom/modules/Campaigns/tpls/WizardCampaignBudget.tpl' : 'modules/Campaigns/tpls/WizardCampaignBudget.tpl';
    $steps[$mod_strings['LBL_NAVIGATION_MENU_TRACKERS']]      = file_exists('custom/modules/Campaigns/tpls/WizardCampaignTracker.tpl') ? 'custom/modules/Campaigns/tpls/WizardCampaignTracker.tpl' : 'modules/Campaigns/tpls/WizardCampaignTracker.tpl';
    $steps[$mod_strings['LBL_TARGET_LISTS']]                   = file_exists('custom/modules/Campaigns/tpls/WizardCampaignTargetListForNonNewsLetter.tpl') ? 'custom/modules/Campaigns/tpls/WizardCampaignTargetListForNonNewsLetter.tpl' : 'modules/Campaigns/tpls/WizardCampaignTargetListForNonNewsLetter.tpl';
    return  $steps;
}


function create_wiz_step_divs($steps,$ss){
    $step_html = '';
    if(isset($steps)  && !empty($steps)){
        $i=1;
        foreach($steps as $name=>$step){
            $step_html .="<p><div id='step$i'>";
            $step_html .= $ss->fetch($step);
            $step_html .="</div></p>";
            $i = $i+1;
        }    
    }
    return $step_html;
}
 
function create_wiz_menu_items($steps,$type,$mrkt_string,$summ_url){
    global $mod_strings;
    $nav_html = '<table border="0" cellspacing="0" cellpadding="0" width="100%" >';
    if(isset($steps)  && !empty($steps)){
        $i=1;
        foreach($steps as $name=>$step){
            $nav_html .= "<tr><td scope='row' nowrap><div id='nav_step$i'>$name</div></td></tr>";
            $i=$i+1;
        }
    }
    if($type == 'newsletter'  ||  $type == 'email'){
        $nav_html .= "<tr><td scope='row' nowrap><div id='nav_step'".($i+1).">$mrkt_string</div></td></tr>";
        $nav_html .= "<tr><td scope='row' nowrap><div id='nav_step'".($i+2).">".$mod_strings['LBL_NAVIGATION_MENU_SEND_EMAIL']."</div></li>";
        $nav_html .= "<tr><td scope='row' nowrap><div id='nav_step'".($i+3).">".$summ_url."</div></td></tr>";
    }else{
     $nav_html .= "<tr><td scope='row' nowrap><div id='nav_step'".($i+1).">".$summ_url."</div></td></tr>";   
    }
       
    $nav_html .= '</table>';
  
    return $nav_html;
}
    


?>


//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
require_once('include/formbase.php');

global $mod_strings;

    //create new campaign bean and populate
    $campaign_focus = new Campaign();
    if(isset($_REQUEST['record'])) {
        $campaign_focus->retrieve($_REQUEST['record']);
    }

    $camp_steps[] = 'wiz_step1_';
    $camp_steps[] = 'wiz_step2_';

    $campaign_focus = populateFromPost('', $campaign_focus);

    foreach($camp_steps as $step){
       $campaign_focus =  populate_wizard_bean_from_request($campaign_focus,$step);
    }

    //save here so we can link relationships
        $campaign_focus->save();
        $GLOBALS['log']->debug("Saved record with id of ".$campaign_focus->id);

    //process prospect lists

        //process subscription lists if this is a newsletter
        if($campaign_focus->campaign_type =='NewsLetter'){
            $pl_list = process_subscriptions_from_request($campaign_focus->name);

            $campaign_focus->load_relationship('prospectlists');
            $existing_pls = $campaign_focus->prospectlists->get();
            $ui_ids = array();

            //for each list returned, add the list to the relationship
            foreach($pl_list as $pl){
                $campaign_focus->prospectlists->add($pl->id);
                //populate array with id's from UI'
                $ui_ids[] = $pl->id;
            }

            //now remove the lists that may have existed before, but were not specified in UI.
            //this will enforce that Newsletters only have 3 available target lists.
            foreach($existing_pls as $pl_del){
                if (!in_array($pl_del, $ui_ids)){
                    $campaign_focus->prospectlists->delete($campaign_focus->id, $pl_del);
                }
            }
        }else{
            //process target lists if this is not a newsletter
            //remove Target Lists if defined

            if(isset($_REQUEST['wiz_remove_target_list'])){

                $remove_target_strings = explode(",", $_REQUEST['wiz_remove_target_list']);
                foreach($remove_target_strings as $remove_trgt_string){
                        if(!empty($remove_trgt_string)){
                        //load relationship and add to the list
                            $campaign_focus->load_relationship('prospectlists');
                            $campaign_focus->prospectlists->delete($campaign_focus->id,$remove_trgt_string);
                        }
                }
            }


    //create new campaign tracker and save if defined
    if(isset($_REQUEST['wiz_list_of_targets'])){
        $target_strings = explode(",", $_REQUEST['wiz_list_of_targets']);
        foreach($target_strings as $trgt_string){
            $target_values = explode("@@", $trgt_string);
            if(count($target_values)==3){

                if(!empty($target_values[0])){
                    //this is a selected target, as the id is already populated, retrieve and link
                    $trgt_focus = new ProspectList();
                    $trgt_focus->retrieve($target_values[0]);

                    //load relationship and add to the list
                    $campaign_focus->load_relationship('prospectlists');
                    $campaign_focus->prospectlists->add($trgt_focus ->id);
                }else{

                    //this is a new target, as the id is not populated, need to create and link
                    $trgt_focus = new ProspectList();
                    $trgt_focus->name = $target_values[1];
                    $trgt_focus->list_type = $target_values[2];
                    $trgt_focus->save();

                    //load relationship and add to the list
                    $campaign_focus->load_relationship('prospectlists');
                    $campaign_focus->prospectlists->add($trgt_focus->id);
                }

            }


        }
    }


        }



    //remove campaign trackers if defined
    if(isset($_REQUEST['wiz_remove_tracker_list'])){

        $remove_tracker_strings = explode(",", $_REQUEST['wiz_remove_tracker_list']);
        foreach($remove_tracker_strings as $remove_trkr_string){
                if(!empty($remove_trkr_string)){
                //load relationship and add to the list
                    $campaign_focus->load_relationship('tracked_urls');
                    $campaign_focus->tracked_urls->delete($campaign_focus->id,$remove_trkr_string);
                }
        }
    }


    //save  campaign trackers and save if defined
    if(isset($_REQUEST['wiz_list_of_existing_trackers'])){
        $tracker_strings = explode(",", $_REQUEST['wiz_list_of_existing_trackers']);
        foreach($tracker_strings as $trkr_string){
            $tracker_values = explode("@@", $trkr_string);
            $ct_focus = new CampaignTracker();
            $ct_focus->retrieve($tracker_values[0]);
            if(!empty($ct_focus->tracker_name)){
                $ct_focus->tracker_name = $tracker_values[1];
                $ct_focus->is_optout = $tracker_values[2];
                $ct_focus->tracker_url = $tracker_values[3];
                $ct_focus->save();

                //load relationship and add to the list
                $campaign_focus->load_relationship('tracked_urls');
                $campaign_focus->tracked_urls->add($ct_focus->id);
            }
        }
    }


    //create new campaign tracker and save if defined
    if(isset($_REQUEST['wiz_list_of_trackers'])){
        $tracker_strings = explode(",", $_REQUEST['wiz_list_of_trackers']);
        foreach($tracker_strings as $trkr_string){
            $tracker_values = explode("@@", $trkr_string);
            if(count($tracker_values)==3){
                $ct_focus = new CampaignTracker();
                $ct_focus->tracker_name = $tracker_values[0];
                $ct_focus->is_optout = $tracker_values[1];
                $ct_focus->tracker_url = $tracker_values[2];
                $ct_focus->save();

                //load relationship and add to the list
                $campaign_focus->load_relationship('tracked_urls');
                $campaign_focus->tracked_urls->add($ct_focus->id);
                // save campaign_trkrs after populating campaign id
                $ct_focus->save();
            }
        }
    }

//set navigation details
$_REQUEST['return_id'] = $campaign_focus->id;
$_REQUEST['return_module'] = $campaign_focus->module_dir;
$_REQUEST['return_action'] = "WizardNewsLetter";
$_REQUEST['action'] = "WizardMarketing";
$_REQUEST['record'] = $campaign_focus->id;;

$action = '';
if(isset($_REQUEST['wiz_direction'])  &&  $_REQUEST['wiz_direction']== 'continue'){
    $action = 'WizardMarketing';
}else{
    $action = 'WizardHome&record='.$campaign_focus->id;
}
//require_once('modules/Campaigns/WizardMarketing.php');
$header_URL = "Location: index.php?return_module=Campaigns&module=Campaigns&action=".$action."&campaign_id=".$campaign_focus->id."&return_action=WizardNewsLetter&return_id=".$campaign_focus->id;
$GLOBALS['log']->debug("about to post header URL of: $header_URL");
 header($header_URL);



/*
 * This function will populate the passed in bean with the post variables
 * that contain the specified prefix
 */
function populate_wizard_bean_from_request($bean,$prefix){
    foreach($_REQUEST as $key=> $val){
     $key = trim($key);
     if((strstr($key, $prefix )) && (strpos($key, $prefix )== 0)){
          $field  =substr($key, strlen($prefix)) ;
          if(isset($_REQUEST[$key]) && !empty($_REQUEST[$key])){
              //echo "prefix is $prefix, field is $field,    key is $key,   and value is $val<br>";
              $value = $_REQUEST[$key];
              $bean->$field = $value;
          }
     }
    }

    return $bean;
}


/*
 * This function will process any specified prospect lists and attach them to current campaign
 * If no prospect lists have been specified, then it will create one for you.  A total of 3 prospect lists
 * will be created for you (Subscription, Unsubscription, and test)
 */
function process_subscriptions_from_request($campaign_name){
    global $mod_strings;
    $pl_list = array();

    //process default target list
    $create_new = true;
    $pl_subs = new ProspectList($campaign_name);
    if(!empty($_REQUEST['wiz_step3_subscription_list_id'])){
        //if subscription list is specified then attach
        $pl_subs->retrieve($_REQUEST['wiz_step3_subscription_list_id']);
        //check to see name matches the bean, if not, then the user has chosen to create new bean
        if($pl_subs->name == $_REQUEST['wiz_step3_subscription_name']){
            $pl_list[] = $pl_subs;
            $create_new = false;
       }

    }
    //create new bio if one was not retrieved successfully
    if($create_new){
        //use default name if one has not been specified
        $name = $campaign_name . " ".$mod_strings['LBL_SUBSCRIPTION_LIST'];
        if(isset($_REQUEST['wiz_step3_subscription_name']) && !empty($_REQUEST['wiz_step3_subscription_name'])){
            $name = $_REQUEST['wiz_step3_subscription_name'];
        }
        //if subscription list is not specified then create and attach default one
        $pl_subs->name = $name;
        $pl_subs->list_type = 'default';
        $pl_subs->assigned_user_id= $GLOBALS['current_user']->id;
        $pl_subs->save();
        $pl_list[] = $pl_subs;
    }

    //process exempt target list
    $create_new = true;
    $pl_un_subs = new ProspectList();
    if(!empty($_REQUEST['wiz_step3_unsubscription_list_id'])){
        //if unsubscription list is specified then attach
        $pl_un_subs->retrieve($_REQUEST['wiz_step3_unsubscription_list_id']);
        //check to see name matches the bean, if not, then the user has chosen to create new bean
        if($pl_un_subs->name == $_REQUEST['wiz_step3_unsubscription_name']){
            $pl_list[] = $pl_un_subs;
            $create_new = false;
       }

    }
    //create new bean if one was not retrieved successfully
    if($create_new){
        //use default name if one has not been specified
        $name = $campaign_name . " ".$mod_strings['LBL_UNSUBSCRIPTION_LIST'];
        if(isset($_REQUEST['wiz_step3_unsubscription_name']) && !empty($_REQUEST['wiz_step3_unsubscription_name'])){
            $name = $_REQUEST['wiz_step3_unsubscription_name'];
        }
        //if unsubscription list is not specified then create and attach default one
        $pl_un_subs->name = $name;
        $pl_un_subs->list_type = 'exempt';
        $pl_un_subs->assigned_user_id= $GLOBALS['current_user']->id;
        $pl_un_subs->save();
        $pl_list[] = $pl_un_subs;
    }

    //process test target list
    $pl_test = new ProspectList();
    $create_new = true;
    if(!empty($_REQUEST['wiz_step3_test_list_id'])){
        //if test list is specified then attach
        $pl_test->retrieve($_REQUEST['wiz_step3_test_list_id']);
        //check to see name matches the bean, if not, then the user has chosen to create new bean
        if($pl_test->name == $_REQUEST['wiz_step3_test_name']){
            $pl_list[] = $pl_test;
            $create_new = false;
        }
    }
    //create new bio if one was not retrieved successfully
    if($create_new){
        //use default name if one has not been specified
        $name = $campaign_name . " ".$mod_strings['LBL_TEST_LIST'];
        if(isset($_REQUEST['wiz_step3_test_name']) && !empty($_REQUEST['wiz_step3_test_name'])){
            $name = $_REQUEST['wiz_step3_test_name'];
        }
        //if test list is not specified then create and attach default one
        $pl_test->name = $name;
        $pl_test->list_type = 'test';
        $pl_test->assigned_user_id= $GLOBALS['current_user']->id;
        $pl_test->save();
        $pl_list[] = $pl_test;
    }

    return $pl_list;
}
?>


//<?php
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description: The primary Function of this file is to manage all the data
 * used by other files in this nodule. It should extend the SugarBean which implements
 * all the basic database operations. Any custom behaviors can be implemented here by
 * implementing functions available in the SugarBean.
 ********************************************************************************/






class CampaignTracker extends SugarBean {
    /* Foreach instance of the bean you will need to access the fields in the table.
    * So define a variable for each one of them, the variable name should be same as the field name
    * Use this module's vardef file as a reference to create these variables.
    */
    var $id;
    var $date_entered;
    var $created_by;
    var $date_modified;
    var $modified_by;
    var $deleted;
    var $tracker_key;
    var $tracker_url;
    var $tracker_name;
    var $campaign_id;
    var $campaign_name;
    var $message_url;
    var $is_optout;

    /* End field definitions*/

    /* variable $table_name is used by SugarBean and methods in this file to constructs queries
    * set this variables value to the table associated with this bean.
    */
    var $table_name = 'campaign_trkrs';

    /*This  variable overrides the object_name variable in SugarBean, wher it has a value of null.*/
    var $object_name = 'CampaignTracker';

    /**/
    var $module_dir = 'CampaignTrackers';

    /* This is a legacy variable, set its value to true for new modules*/
    var $new_schema = true;

    /* $column_fields holds a list of columns that exist in this bean's table. This list is referenced
    * when fetching or saving data for the bean. As you modify a table you need to keep this up to date.
    */
    var $column_fields = Array(
            'id'
            ,'tracker_key'
            ,'tracker_url'
            ,'tracker_name'
            ,'campaign_id'
    );

    // This is used to retrieve related fields from form posts.
    var $additional_column_fields = Array('campaign_id');
    var $relationship_fields = Array('campaing_id'=>'campaign');

    var $required_fields =  array('tracker_name'=>1,'tracker_url'=>1);
    /*This bean's constructor*/
    function CampaignTracker() {
        parent::SugarBean();
    }

    function save() {
        //make sure that the url has a scheme, if not then add http:// scheme
        if ($this->is_optout!=1 ){
            $url = strtolower(trim($this->tracker_url));
            if(!preg_match('/^(http|https|ftp):\/\//i', $url)){
                $this->tracker_url = 'http://'.$url;
            }
        }

        parent::save();
    }

    /* This method should return the summary text which is used to build the bread crumb navigation*/
    /* Generally from this method you would return value of a field that is required and is of type string*/
    function get_summary_text()
    {
        return "$this->tracker_name";
    }


    /* This method is used to generate query for the list form. The base implementation of this method
    * uses the table_name and list_field variable to generate the basic query and then  adds the custom field
    * join and team filter. If you are implementing this function do not forget to consider the additional conditions.
    */

    function fill_in_additional_detail_fields() {
        global $sugar_config;

        //setup campaign name.
        $query = "SELECT name from campaigns where id = '$this->campaign_id'";
        $result =$this->db->query($query,true," Error filling in additional detail fields: ");

        // Get the id and the name.
        $row = $this->db->fetchByAssoc($result);
        if($row != null) {
            $this->campaign_name=$row['name'];
        }

        if (!class_exists('Administration')) {

        }
        $admin=new Administration();
        $admin->retrieveSettings('massemailer'); //retrieve all admin settings.
        if (isset($admin->settings['massemailer_tracking_entities_location_type']) and $admin->settings['massemailer_tracking_entities_location_type']=='2'  and isset($admin->settings['massemailer_tracking_entities_location']) ) {
            $this->message_url=$admin->settings['massemailer_tracking_entities_location'];
        } else {
            $this->message_url=$sugar_config['site_url'];
        }
        if ($this->is_optout == 1) {
            $this->message_url .= '/index.php?entryPoint=removeme&identifier={MESSAGE_ID}';
        } else {
            $this->message_url .= '/index.php?entryPoint=campaign_trackerv2&track=' . $this->id;
        }
    }
}
?>

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/






global $app_strings;
global $mod_strings;

$focus = new CampaignTracker();
$focus->retrieve($_REQUEST['record']);

if(isset($_REQUEST['isDuplicate']) && $_REQUEST['isDuplicate'] == 'true') {
	$focus->id = "";
}

echo getClassicModuleTitle($mod_strings['LBL_MODULE_NAME'], array($mod_strings['LBL_MODULE_NAME'],$focus->tracker_name), true);



$GLOBALS['log']->info("campaign tracker detail view");

$xtpl=new XTemplate ('modules/CampaignTrackers/DetailView.html');
$xtpl->assign("MOD", $mod_strings);
$xtpl->assign("APP", $app_strings);

if (isset($_REQUEST['return_module'])) {
	$xtpl->assign("RETURN_MODULE", $_REQUEST['return_module']);
} else {
	$xtpl->assign("RETURN_MODULE", 'Campaigns');
}
if (isset($_REQUEST['return_action'])) {
	$xtpl->assign("RETURN_ACTION", $_REQUEST['return_action']);
} else {
	$xtpl->assign("RETURN_ACTION", 'DetailView');
}
if (isset($_REQUEST['return_id'])) {
	$xtpl->assign("RETURN_ID", $_REQUEST['return_id']);
} else {
	$xtpl->assign("RETURN_ID", $focus->campaign_id);
}
 
$xtpl->assign("GRIDLINE", $gridline);
$xtpl->assign("PRINT_URL", "index.php?".$GLOBALS['request_string']);
$xtpl->assign("ID", $focus->id);
if (!empty($_REQUEST['campaign_name'])) {
	$xtpl->assign("CAMPAIGN_NAME", $_REQUEST['campaign_name']);
} else  {
	$xtpl->assign("CAMPAIGN_NAME", $focus->campaign_name);
}

if (!empty($_REQUEST['campaign_id'])) {
	$xtpl->assign("CAMPAIGN_ID", $_REQUEST['campaign_id']);
} else {
	$xtpl->assign("CAMPAIGN_ID", $focus->campaign_id);
}
$xtpl->assign("TRACKER_NAME", $focus->tracker_name);
$xtpl->assign("TRACKER_URL", $focus->tracker_url);
$xtpl->assign("MESSAGE_URL", $focus->message_url);
$xtpl->assign("TRACKER_KEY", $focus->tracker_key);

if (!empty($focus->is_optout) && $focus->is_optout == 1) {
	$xtpl->assign("IS_OPTOUT_CHECKED","checked");
}


//$xtpl->assign("CREATED_BY", $focus->created_by_name);
//$xtpl->assign("MODIFIED_BY", $focus->modified_by_name);
//$xtpl->assign("DATE_MODIFIED", $focus->date_modified);
//$xtpl->assign("DATE_ENTERED", $focus->date_entered);

$xtpl->parse("main");
$xtpl->out("main");

?>

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description: TODO:  To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/




require_once('modules/CampaignTrackers/Forms.php');
global $app_strings;
global $app_list_strings;
global $mod_strings;
global $sugar_version, $sugar_config;

$focus = new CampaignTracker();

if(isset($_REQUEST['record'])) {
    $focus->retrieve($_REQUEST['record']);
}
$old_id = '';

if(isset($_REQUEST['isDuplicate']) && $_REQUEST['isDuplicate'] == 'true') {
	$focus->id = "";
}




$GLOBALS['log']->info("Campaign Tracker Edit View");

$xtpl=new XTemplate ('modules/CampaignTrackers/EditView.html');
$xtpl->assign("MOD", $mod_strings);
$xtpl->assign("APP", $app_strings);

$campaignName = '';
$campaignId = '';
if (!empty($_REQUEST['campaign_name'])) {
	$xtpl->assign("CAMPAIGN_NAME", $_REQUEST['campaign_name']);
	$campaignName = $_REQUEST['campaign_name'];
} else {
	$xtpl->assign("CAMPAIGN_NAME", $focus->campaign_name);
	$campaignName = $focus->campaign_name;
}
if (!empty($_REQUEST['campaign_id'])) {
	$xtpl->assign("CAMPAIGN_ID", $_REQUEST['campaign_id']);
	$campaignId = $_REQUEST['campaign_id'];
} else {
	$xtpl->assign("CAMPAIGN_ID", $focus->campaign_id);
	$campaignId = $focus->campaign_id;
}
$params = array();
$params[] = "<a href='index.php?module=Campaigns&action=DetailView&record={$campaignId}'>{$campaignName}</a>";
$params[] = $mod_strings['LBL_MODULE_NAME'];
echo getClassicModuleTitle($focus->module_dir, $params, true);

if (isset($_REQUEST['return_module'])) $xtpl->assign("RETURN_MODULE", $_REQUEST['return_module']);
if (isset($_REQUEST['return_action'])) $xtpl->assign("RETURN_ACTION", $_REQUEST['return_action']);
if (isset($_REQUEST['return_id'])) $xtpl->assign("RETURN_ID", $_REQUEST['return_id']);

$xtpl->assign("PRINT_URL", "index.php?".$GLOBALS['request_string']);

$xtpl->assign("JAVASCRIPT", get_set_focus_js().get_validate_record_js());
$xtpl->assign("ID", $focus->id);



$xtpl->assign("TRACKER_NAME", $focus->tracker_name);
$xtpl->assign("TRACKER_URL", $focus->tracker_url);

global $current_user;
if(is_admin($current_user) && $_REQUEST['module'] != 'DynamicLayout' && !empty($_SESSION['editinplace'])){	
	$record = '';
	if(!empty($_REQUEST['record'])){
		$record = 	$_REQUEST['record'];
	}
	$xtpl->assign("ADMIN_EDIT","<a href='index.php?action=index&module=DynamicLayout&from_action=".$_REQUEST['action'] ."&from_module=".$_REQUEST['module'] ."&record=".$record. "'>".SugarThemeRegistry::current()->getImage("EditLayout","border='0' align='bottom'",null,null,'.gif',$mod_strings['LBL_EDIT_LAYOUT'])."</a>");
}
if (!empty($focus->is_optout) && $focus->is_optout == 1) {
	$xtpl->assign("IS_OPTOUT_CHECKED","checked");
	$xtpl->assign("TRACKER_URL_DISABLED","disabled");
}

$xtpl->parse("main");

$xtpl->out("main");

$javascript = new javascript();
$javascript->setFormName('EditView');
$javascript->setSugarBean($focus);
$javascript->addAllFields('');
echo $javascript->getScript();
?>

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  Contains a variety of utility functions used to display UI
 * components such as form headers and footers.  Intended to be modified on a per
 * theme basis.
 ********************************************************************************/





/**
 * Create javascript to validate the data entered into a record.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 */
function get_validate_record_js () {
global $mod_strings;
global $app_strings;

$err_missing_required_fields = $app_strings['ERR_MISSING_REQUIRED_FIELDS'];

$the_script  = <<<EOQ

<script type="text/javascript" language="Javascript">

function verify_data(form) {
	var isError = false;
	var errorMessage = "";

	if (isError == true) {
		alert("$err_missing_required_fields" + errorMessage);
		return false;
	}
	return true;
}
</script>

EOQ;

return $the_script;



}

/**
 * Create HTML form to enter a new record with the minimum necessary fields.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 */
function get_new_record_form () {
	global $app_strings;
	global $app_list_strings;
	global $mod_strings;
	global $currentModule;
	global $current_user;
	global $timedate;
	
	$the_form = get_left_form_header($mod_strings['LBL_NEW_FORM_TITLE']);
	$form = new XTemplate ('modules/Campaigns/Forms.html');

	$module_select = empty($_REQUEST['module_select']) ? ''
		: $_REQUEST['module_select'];
	$form->assign('MOD', $mod_strings);
	$form->assign('APP', $app_strings);
	$form->assign('THEME', SugarThemeRegistry::current()->__toString());
	$form->assign("JAVASCRIPT", get_set_focus_js().get_validate_record_js());
	$form->assign("STATUS_OPTIONS", get_select_options_with_id($app_list_strings['campaign_status_dom'], "Planning"));
	$form->assign("TYPE_OPTIONS", get_select_options_with_id($app_list_strings['campaign_type_dom'], ""));

	$form->assign("USER_ID", $current_user->id);


	$form->assign("CALENDAR_LANG", "en");
	$form->assign("USER_DATEFORMAT", '('. $timedate->get_user_date_format().')');
	$form->assign("CALENDAR_DATEFORMAT", $timedate->get_cal_date_format());

	$form->parse('main');
	$the_form .= $form->text('main');

	
	$focus = new Campaign();
	
	
	$javascript = new javascript();
	$javascript->setFormName('quick_save');
	$javascript->setSugarBean($focus);
	$javascript->addRequiredFields('');
	$jscript = $javascript->getScript();

	$the_form .= $jscript . get_left_form_footer();
	return $the_form;


}

?>

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point'); 
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  TODO To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/

global $mod_strings;
$module_menu  = Array();
if(ACLController::checkAccess('Campaigns', 'list', true))$module_menu[]=	Array("index.php?module=Campaigns&action=index&return_module=Campaigns&return_action=index", $mod_strings['LNK_CAMPAIGN_LIST'],"Campaigns");
?>

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/



$subpanel_layout = array(
	'top_buttons' => array(
		array('widget_class' => 'SubPanelTopCreateButton'),
	),

	'where' => '',


	'list_fields' => array(
		'tracker_name'=>array(
	 		'vname' => 'LBL_SUBPANEL_TRACKER_NAME',
			'widget_class' => 'SubPanelDetailViewLink', 		
			'width' => '20%',
		),
		'tracker_url'=>array(
	 		'vname' => 'LBL_SUBPANEL_TRACKER_URL',
		 	'width' => '60%',
		),
		'tracker_key'=>array(
	 		'vname' => 'LBL_SUBPANEL_TRACKER_KEY',
			'width' => '10%',
		),
		'edit_button'=>array(
			'vname' => 'LBL_EDIT_BUTTON',
			'widget_class' => 'SubPanelEditButton',
		 	'module' => 'Cases',
			'width' => '5%',
		),
		'remove_button'=>array(
			'vname' => 'LBL_REMOVE',
			'widget_class' => 'SubPanelRemoveButton',
		 	'module' => 'Cases',
			'width' => '5%',
		),
	),
);
?>

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
require_once('include/formbase.php');

$focus = new CampaignTracker();

$focus->retrieve($_POST['record']);
if(!$focus->ACLAccess('Save')){
	ACLController::displayNoAccess(true);
	sugar_cleanup(true);
}

$check_notify = FALSE;
foreach($focus->column_fields as $field) {
	if(isset($_POST[$field])) {
		$value = $_POST[$field];
		$focus->$field = $value;
	}
}

foreach($focus->additional_column_fields as $field) {
	if(isset($_POST[$field])) {
		$value = $_POST[$field];
		$focus->$field = $value;

	}
}
//set check box states.
if (isset($_POST['is_optout']) && $_POST['is_optout'] =='on') {
	$focus->is_optout=1;
	$focus->tracker_url='index.php?entryPoint=removeme';
} else {
	$focus->is_optout=0;
}

$focus->save($check_notify);
$return_id = $focus->id;
$GLOBALS['log']->debug("Saved record with id of ".$return_id);
handleRedirect('', '');
?>

//<?php
if(!defined('sugarEntry') || !sugarEntry) die('Not A Valid Entry Point');
/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/

$dictionary['CampaignTracker'] = array('table' => 'campaign_trkrs',
	'comment' => 'Maintains the Tracker URLs used in campaign emails',
          
'fields' => array (
    'id' => array (
        'name' => 'id',
        'vname' => 'LBL_ID',
        'type' => 'id',
        'required'=>true,
        'reportable'=>false,
        'comment' => 'Unique identifier'
    ),
   'tracker_name' => array (
        'name' => 'tracker_name',
        'vname' => 'LBL_TRACKER_NAME',
        'type' => 'varchar',
        'len' => '30',
        'comment' => 'The name of the campaign tracker'
   ),
  	'tracker_url' => array (
        'name' => 'tracker_url',
        'vname' => 'LBL_TRACKER_URL',
        'type' => 'varchar',
        'len' => '255',
        'default' => 'http://',
        'comment' => 'The URL that represents the landing page when the tracker URL in the campaign email is clicked'
   	),
    'tracker_key' => array (
        'name' => 'tracker_key',
        'vname' => 'LBL_TRACKER_KEY',
        'type' => 'int',
        'len' => '11',
        'auto_increment' => true,
        'required'=>true,
        'studio' => array('editview' => false),
        'comment' => 'Internal key to uniquely identifier the tracker URL'
  	),  
  'campaign_id'=> array(
    	'name'=>'campaign_id',
    	'vname'=>'LBL_CAMPAIGN_ID',
    	'type'=>'id',
    	'required'=>false,
    	'reportable'=>false,
    	'comment' => 'The ID of the campaign'
  	),
    'date_entered' => array (
    	'name' => 'date_entered',
        'vname' => 'LBL_DATE_ENTERED',
        'type' => 'datetime',
		'required' => true,
		'comment' => 'Date record created'
  	),
  	'date_modified' => array (
    	'name' => 'date_modified',
    	'vname' => 'LBL_DATE_MODIFIED',
    	'type' => 'datetime',
    	'required' => true,
		'comment' => 'Date record last modified'
  	),
    'modified_user_id' => array (
    	'name' => 'modified_user_id',
    	'vname' => 'LBL_MODIFIED_USER_ID',
    	'dbType' => 'id',
    	'type'=>'id',
		'comment' => 'User who last modified record'
  	),
  	'created_by' => array (
    	'name' => 'created_by',
    	'vname' => 'LBL_CREATED_BY',
    	'type' => 'assigned_user_name',
    	'table' => 'users',
    	'isnull' => 'false',
    	'dbType' => 'id',
		'comment' => 'User ID who created record'
  	),
  	'is_optout' => array (
    	'name' => 'is_optout',
    	'vname' => 'LBL_OPTOUT',
    	'type' => 'bool',
    	'required' => true,
    	'default' => '0',
    	'reportable'=>false,
    	'comment' => 'Indicator whether tracker URL represents an opt-out link'
  	),
  	'deleted' => array (
    	'name' => 'deleted',
    	'vname' => 'LBL_DELETED',
    	'type' => 'bool',
    	'required' => false,
    	'default' => '0',
    	'reportable'=>false,
    	'comment' => 'Record deletion indicator'
  	),
  	'campaign' => array (
  		'name' => 'campaign',
    	'type' => 'link',
    	'relationship' => 'campaign_campaigntrakers',
    	'source'=>'non-db',
		'vname'=>'LBL_CAMPAIGN',
  ),
),

'relationships'=>array(

  'campaign_campaigntrakers' => array(
		'lhs_module'=> 'Campaigns', 
		'lhs_table'=> 'campaigns', 
		'lhs_key' => 'id',
   		'rhs_module'=> 'CampaignTrackers', 
		'rhs_table'=> 'campaign_trkrs', 
		'rhs_key' => 'campaign_id',
   		'relationship_type'=>'one-to-many'
  )
)
,'indices' => array (
      array('name' =>'campaign_trackepk', 'type' =>'primary', 'fields'=>array('id')),
      array('name' => 'campaign_tracker_key_idx', 'type'=>'index', 'fields'=>array('tracker_key')),
 )
);
?>


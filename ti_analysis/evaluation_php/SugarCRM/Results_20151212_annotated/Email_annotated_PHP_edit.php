<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description:
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc. All Rights
 * Reserved. Contributor(s): ______________________________________..
 *********************************************************************************/
	
	
	
	/**
 * Stub class, exists only to allow Link class easily use the SugarEmailAddress class
 */
	class EmailAddress extends SugarEmailAddress
	{
		var $disable_row_level_security = true;
		
		function EmailAddress()
		{
			parent::SugarEmailAddress();
		}
	}
	
	// 	function save($id = '', $module = '', $new_addrs=array(), $primary='', $replyTo='', $invalid='', $optOut='', $in_workflow=false) 
	// 	{
	// 		if ( func_num_args() > 1 ) {
	// 		    parent::save($id, $module, $new_addrs, $primary, $replyTo, $invalid, $optOut, $in_workflow);
	// 		}
	// 		else {
	// 		    SugarBean::save($id);
	// 		}
	// 	}
	
	//<?php
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	
	$action_view_map['config'] = 'config';
	$action_view_map['campaignconfig'] = 'campaignconfig';
	
	//<?php
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	
	class EmailManController extends SugarController
	{
		function action_Save()
		{
			
			
			require_once ('include/OutboundEmail/OutboundEmail.php');
			require_once ('modules/Configurator/Configurator.php');
			
			$configurator = new Configurator();
			global $sugar_config;
			global $current_user;
			if (!is_admin($current_user) 
				&& !is_admin_for_module($GLOBALS['current_user'], 'Emails') 
				&& !is_admin_for_module($GLOBALS['current_user'], 'Campaigns')) {
				sugar_die("Unauthorized access to administration.");
			}
			
			//Do not allow users to spoof for sendmail if the config flag is not set.
			if (!isset($sugar_config['allow_sendmail_outbound']) || !$sugar_config['allow_sendmail_outbound']) 
				$_REQUEST['mail_sendtype'] = "SMTP";
			
			// save Outbound settings  #Bug 20033 Ensure data for Outbound email exists before trying to update the system mailer.
			if (isset($_REQUEST['mail_sendtype']) && empty($_REQUEST['campaignConfig'])) {
				$oe = new OutboundEmail();

				//TODO (MT EXTEND Code): retrieve and use the TENANT_ID
				$oe->populateFromPost();
				$oe->saveSystem();
			}
			
			
			
			$focus = new Administration();
			
			if (isset($_POST['tracking_entities_location_type'])) {
				if ($_POST['tracking_entities_location_type'] != '2') {
					unset($_POST['tracking_entities_location']);
					unset($_POST['tracking_entities_location_type']);
				}
			}
			// cn: handle mail_smtpauth_req checkbox on/off (removing double reference in the form itself
			if (!isset($_POST['mail_smtpauth_req'])) {
				$_POST['mail_smtpauth_req'] = 0;
				if (empty($_POST['campaignConfig'])) {
					$_POST['notify_allow_default_outbound'] = 0; // If smtp auth is disabled ensure outbound is disabled.
				}
			}
			
			if (!empty($_POST['notify_allow_default_outbound'])) {
				$oe = new OutboundEmail();

				if (!$oe->isAllowUserAccessToSystemDefaultOutbound()) 
					//TODO (MT EXTEND Code): retrieve and use the TENANT_ID
					$oe->removeUserOverrideAccounts();
			}
			
			$focus->saveConfig();
			
			// save User defaults for emails
			$configurator->config['email_default_delete_attachments'] = (isset($_REQUEST['email_default_delete_attachments'])) ? true : false;
			
			///////////////////////////////////////////////////////////////////////////////
			////	SECURITY
			$security = array();
			if (isset($_REQUEST['applet'])) 
				$security['applet'] = 'applet';
			if (isset($_REQUEST['base'])) 
				$security['base'] = 'base';
			if (isset($_REQUEST['embed'])) 
				$security['embed'] = 'embed';
			if (isset($_REQUEST['form'])) 
				$security['form'] = 'form';
			if (isset($_REQUEST['frame'])) 
				$security['frame'] = 'frame';
			if (isset($_REQUEST['frameset'])) 
				$security['frameset'] = 'frameset';
			if (isset($_REQUEST['iframe'])) 
				$security['iframe'] = 'iframe';
			if (isset($_REQUEST['import'])) 
				$security['import'] = '\?import';
			if (isset($_REQUEST['layer'])) 
				$security['layer'] = 'layer';
			if (isset($_REQUEST['link'])) 
				$security['link'] = 'link';
			if (isset($_REQUEST['object'])) 
				$security['object'] = 'object';
			if (isset($_REQUEST['style'])) 
				$security['style'] = 'style';
			if (isset($_REQUEST['xmp'])) 
				$security['xmp'] = 'xmp';
			$security['script'] = 'script';
			
			$configurator->config['email_xss'] = base64_encode(serialize($security));
			
			////	SECURITY
			///////////////////////////////////////////////////////////////////////////////
			
			ksort($sugar_config);
			
			$configurator->handleOverride();
			
			
		}
	}
	
?>


//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	
	if (empty($_REQUEST['id']) || !preg_match("/^[\w\d\-]+$/", $_REQUEST['id'])) {
		die("Not a Valid Entry Point");
	}
	
	require_once ('modules/Notes/Note.php');
	$note = new Note();
	//check if file is an email image
	if (!$note->retrieve_by_string_fields(array('id' => $_REQUEST['id'], 'parent_type' => "Emails"))) {
		die("Not a Valid Entry Point");
	}
	
	$location = $GLOBALS['sugar_config']['upload_dir'] . "/" . $_REQUEST['id'];
	
	$mime = getimagesize($location);
	
	if (!empty($mime)) {
		header("Content-Type: {$mime['mime']}");
	} else {
		header("Content-Type: image/png");
	}
	
	
	readfile($location);
	
	
	
	//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	
	
	class EmailMan extends SugarBean
	{
		var $id;
		var $deleted;
		var $date_created;
		var $date_modified;
		var $module;
		var $module_id;
		var $marketing_id;
		var $campaign_id;
		var $user_id;
		var $list_id;
		var $invalid_email;
		var $from_name;
		var $from_email;
		var $in_queue;
		var $in_queue_date;
		var $template_id;
		var $send_date_time;
		var $table_name = "emailman";
		var $object_name = "EmailMan";
		var $module_dir = "EmailMan";
		var $send_attempts;
		var $related_id;
		var $related_type;
		var $test = false;
		var $notes_array = array();
		var $verified_email_marketing_ids = array();
		function toString()
		{
			return "EmailMan:\nid = $this->id ,user_id= $this->user_id module = $this->module , related_id = $this->related_id , related_type = $this->related_type ,list_id = $this->list_id, send_date_time= $this->send_date_time\n";
		}
		
		// This is used to retrieve related fields from form posts.
		var $additional_column_fields = array();
		
		function EmailMan()
		{
			parent::SugarBean();
			
		}
		
		var $new_schema = true;
		
		function create_new_list_query($order_by, $where, $filter = array(), $params = array(), $show_deleted = 0, $join_type = '', $return_array = false, $parentbean = null, $singleSelect = false)
		{
			$query = array('select' => '', 'from' => '', 'where' => '', 'order_by' => '');
			
			
			
			$query['select'] = "SELECT $this->table_name.* ,
					campaigns.name as campaign_name,
					email_marketing.name as message_name,
					(CASE related_type
						WHEN 'Contacts' THEN " . $this->db->concat('contacts', array('first_name', 'last_name'), '&nbsp;') . 
				"
						WHEN 'Leads' THEN " . $this->db->concat('leads', array('first_name', 'last_name'), '&nbsp;') . 
				"
						WHEN 'Accounts' THEN accounts.name
						WHEN 'Users' THEN " . $this->db->concat('users', array('first_name', 'last_name'), '&nbsp;') . 
				"
						WHEN 'Prospects' THEN " . $this->db->concat('prospects', array('first_name', 'last_name'), '&nbsp;') . 
				"
					END) recipient_name";
			$query['from'] = "	FROM $this->table_name
					LEFT JOIN users ON users.id = $this->table_name.related_id and $this->table_name.related_type ='Users'
					LEFT JOIN contacts ON contacts.id = $this->table_name.related_id and $this->table_name.related_type ='Contacts'
					LEFT JOIN leads ON leads.id = $this->table_name.related_id and $this->table_name.related_type ='Leads'
					LEFT JOIN accounts ON accounts.id = $this->table_name.related_id and $this->table_name.related_type ='Accounts'
					LEFT JOIN prospects ON prospects.id = $this->table_name.related_id and $this->table_name.related_type ='Prospects'
					LEFT JOIN prospect_lists ON prospect_lists.id = $this->table_name.list_id
                    LEFT JOIN email_addr_bean_rel ON email_addr_bean_rel.bean_id = $this->table_name.related_id and $this->table_name.related_type = email_addr_bean_rel.bean_module and email_addr_bean_rel.primary_address = 1 and email_addr_bean_rel.deleted=0
					LEFT JOIN campaigns ON campaigns.id = $this->table_name.campaign_id
					LEFT JOIN email_marketing ON email_marketing.id = $this->table_name.marketing_id ";
			
			$where_auto = " $this->table_name.deleted=0";
			
			if ($where != "") 
				$query['where'] = "WHERE $where AND " . $where_auto;
			 else 
				$query['where'] = "WHERE " . $where_auto;
			
			if (isset($params['group_by'])) {
				$query['group_by'] .= " GROUP BY {$params['group_by']}";
			}
			
			$order_by = $this->process_order_by($order_by);
			if (!empty($order_by)) {
				$query['order_by'] = ' ORDER BY ' . $order_by;
			}
			
			if ($return_array) {
				return $query;
			}
			
			return $query['select'] . $query['from'] . $query['where'] . $query['order_by'];
			
		}
		// if
		
		function create_queue_items_query($order_by, $where, $filter = array(), $params = array(), $show_deleted = 0, $join_type = '', $return_array = false, $parentbean = null, $singleSelect = false)
		{
			
			if ($return_array) {
				return parent::create_new_list_query($order_by, $where, $filter, $params, $show_deleted, $join_type, $return_array, $parentbean, $singleSelect);
			}
			
			$query = "SELECT $this->table_name.* ,
					campaigns.name as campaign_name,
					email_marketing.name as message_name,
					(CASE related_type
						WHEN 'Contacts' THEN " . $this->db->concat('contacts', array('first_name', 'last_name'), '&nbsp;') . 
				"
						WHEN 'Leads' THEN " . $this->db->concat('leads', array('first_name', 'last_name'), '&nbsp;') . 
				"
						WHEN 'Accounts' THEN accounts.name
						WHEN 'Users' THEN " . $this->db->concat('users', array('first_name', 'last_name'), '&nbsp;') . 
				"
						WHEN 'Prospects' THEN " . $this->db->concat('prospects', array('first_name', 'last_name'), '&nbsp;') . 
				"
					END) recipient_name";
			
			$query .= " FROM $this->table_name
		            LEFT JOIN users ON users.id = $this->table_name.related_id and $this->table_name.related_type ='Users'
					LEFT JOIN contacts ON contacts.id = $this->table_name.related_id and $this->table_name.related_type ='Contacts'
					LEFT JOIN leads ON leads.id = $this->table_name.related_id and $this->table_name.related_type ='Leads'
					LEFT JOIN accounts ON accounts.id = $this->table_name.related_id and $this->table_name.related_type ='Accounts'
					LEFT JOIN prospects ON prospects.id = $this->table_name.related_id and $this->table_name.related_type ='Prospects'
					LEFT JOIN prospect_lists ON prospect_lists.id = $this->table_name.list_id
                    LEFT JOIN email_addr_bean_rel ON email_addr_bean_rel.bean_id = $this->table_name.related_id and $this->table_name.related_type = email_addr_bean_rel.bean_module and email_addr_bean_rel.primary_address = 1 and email_addr_bean_rel.deleted=0
					LEFT JOIN campaigns ON campaigns.id = $this->table_name.campaign_id
					LEFT JOIN email_marketing ON email_marketing.id = $this->table_name.marketing_id ";
			
			//B.F. #37943
			if (isset($params['group_by'])) {
				$group_by = str_replace("emailman", "em", $params['group_by']);
				$query .= "INNER JOIN (select min(id) as id from emailman  em GROUP BY $group_by  ) secondary
			           on {$this->table_name}.id = secondary.id	";
			}
			
			$where_auto = " $this->table_name.deleted=0";
			
			if ($where != "") 
				$query .= "WHERE $where AND " . $where_auto;
			 else 
				$query .= "WHERE " . $where_auto;
			
			$order_by = $this->process_order_by($order_by);
			if (!empty($order_by)) {
				$query .= ' ORDER BY ' . $order_by;
			}
			
			return $query;
			
		}
		
		function create_list_query($order_by, $where, $show_deleted = 0)
		{
			
			$query = "SELECT $this->table_name.* ,
					campaigns.name as campaign_name,
					email_marketing.name as message_name,
					(CASE related_type
						WHEN 'Contacts' THEN " . $this->db->concat('contacts', array('first_name', 'last_name'), '&nbsp;') . 
				"
						WHEN 'Leads' THEN " . $this->db->concat('leads', array('first_name', 'last_name'), '&nbsp;') . 
				"
						WHEN 'Accounts' THEN accounts.name
						WHEN 'Users' THEN " . $this->db->concat('users', array('first_name', 'last_name'), '&nbsp;') . 
				"
						WHEN 'Prospects' THEN " . $this->db->concat('prospects', array('first_name', 'last_name'), '&nbsp;') . 
				"
					END) recipient_name";
			$query .= "	FROM $this->table_name
					LEFT JOIN users ON users.id = $this->table_name.related_id and $this->table_name.related_type ='Users'
					LEFT JOIN contacts ON contacts.id = $this->table_name.related_id and $this->table_name.related_type ='Contacts'
					LEFT JOIN leads ON leads.id = $this->table_name.related_id and $this->table_name.related_type ='Leads'
					LEFT JOIN accounts ON accounts.id = $this->table_name.related_id and $this->table_name.related_type ='Accounts'
					LEFT JOIN prospects ON prospects.id = $this->table_name.related_id and $this->table_name.related_type ='Prospects'
					LEFT JOIN prospect_lists ON prospect_lists.id = $this->table_name.list_id
                    LEFT JOIN email_addr_bean_rel ON email_addr_bean_rel.bean_id = $this->table_name.related_id and $this->table_name.related_type = email_addr_bean_rel.bean_module and email_addr_bean_rel.primary_address = 1 and email_addr_bean_rel.deleted=0
					LEFT JOIN campaigns ON campaigns.id = $this->table_name.campaign_id
					LEFT JOIN email_marketing ON email_marketing.id = $this->table_name.marketing_id ";
			
			$where_auto = " $this->table_name.deleted=0";
			
			if ($where != "") 
				$query .= "where $where AND " . $where_auto;
			 else 
				$query .= "where " . $where_auto;
			
			$order_by = $this->process_order_by($order_by);
			if (!empty($order_by)) {
				$query .= ' ORDER BY ' . $order_by;
			}
			
			return $query;
		}
		
		function get_list_view_data()
		{
			global $locale, $current_user;
			$temp_array = parent::get_list_view_array();
			
			$related_type = $temp_array['RELATED_TYPE'];
			$related_id = $temp_array['RELATED_ID'];
			$is_person = SugarModule::get($related_type)->moduleImplements('Person');
			
			if ($is_person) {
				$query = "SELECT first_name, last_name FROM " . strtolower($related_type) . " WHERE id ='" . $related_id . "'";
			} else {
				$query = "SELECT name FROM " . strtolower($related_type) . " WHERE id ='" . $related_id . "'";
			}
			
			$result = $this->db->query($query);
			$row = $this->db->fetchByAssoc($result);
			
			if ($row) {
				$temp_array['RECIPIENT_NAME'] = $is_person ? $locale->getLocaleFormattedName($row['first_name'], $row['last_name'], '') : $row['name'];
			}
			
			//also store the recipient_email address
			$query = "SELECT addr.email_address FROM email_addresses addr,email_addr_bean_rel eb WHERE eb.deleted=0 AND addr.id=eb.email_address_id AND bean_id ='" . $related_id . "' AND primary_address = '1'";
			
			$result = $this->db->query($query);
			$row = $this->db->fetchByAssoc($result);
			if ($row) {
				$temp_array['RECIPIENT_EMAIL'] = $row['email_address'];
			}
			
			$this->email1 = $temp_array['RECIPIENT_EMAIL'];
			$temp_array['EMAIL1_LINK'] = $current_user->getEmailLink('email1', $this, '', '', 'ListView');
			
			return $temp_array;
		}
		
		
		function set_as_sent($email_address, $delete = true, $email_id = null, $email_type = null, $activity_type = null)
		{
			
			//TODO (MT): specify type of proc. parameters
			global $timedate;
			
			$this->send_attempts++;
			$this->id = (int) $this->id;
			if ($delete || $this->send_attempts > 5) {
				
				//create new campaign log record.
				
				$campaign_log = new CampaignLog();

				//TODO (MT EXTEND Code): retrieve and use the TENANT_ID
				$campaign_log->campaign_id = $this->campaign_id;
				$campaign_log->target_tracker_key = $this->target_tracker_key;
				$campaign_log->target_id = $this->related_id;
				$campaign_log->target_type = $this->related_type;
				$campaign_log->marketing_id = $this->marketing_id;
				//if test suppress duplicate email address checking.
				if (!$this->test) {
					$campaign_log->more_information = $email_address;
				}
				$campaign_log->activity_type = $activity_type;
				$campaign_log->activity_date = $timedate->now();
				$campaign_log->list_id = $this->list_id;
				$campaign_log->related_id = $email_id;
				$campaign_log->related_type = $email_type;
				$campaign_log->save();
				
				$query = "DELETE FROM emailman WHERE id = $this->id";
				$this->db->query($query);
			} else {
				//try to send the email again a day later.
				$query = 'UPDATE ' . $this->table_name . " SET in_queue='1', send_attempts='$this->send_attempts', in_queue_date=" . $this->db->now() . " WHERE id = $this->id";
				$this->db->query($query);
			}
		}
		
		/**
     * Function finds the reference email for the campaign. Since a campaign can have multiple email templates
     * the reference email has same id as the marketing id.
     * this function will create an email if one does not exist. also the function will load these relationships leads, accounts, contacts
     * users and targets
     *
     * @param varchar marketing_id message id
     * @param string $subject email subject
     * @param string $body_text Email Body Text
     * @param string $body_html Email Body HTML
     * @param string $campaign_name Campaign Name
     * @param string from_address Email address of the sender, usually email address of the configured inbox.
     * @param string sender_id If of the user sending the campaign.
     * @param array  macro_nv array of name value pair, one row for each replacable macro in email template text.
     * @param string from_address_name The from address eg markeing <marketing@sugar.net>
     * @return
     */
		function create_ref_email($marketing_id, $subject, $body_text, $body_html, $campagin_name, $from_address, $sender_id, $notes, $macro_nv, $newmessage, $from_address_name)
		{
			
			//TODO (MT): specify type of proc. parameters
			global $mod_Strings, $timedate;
			$upd_ref_email = false;
			if ($newmessage or empty($this->ref_email->id)) {
				$this->ref_email = new Email();

				//TODO (MT EXTEND Code): retrieve and use the TENANT_ID
				$this->ref_email->retrieve($marketing_id, true, false);
				
				//the reference email should be updated when user swithces from test mode to regular mode,and, for every run in test mode, and is user
				//switches back to test mode.
				//this is to account for changes to email template.
				$upd_ref_email = (!empty($this->ref_email->id) and $this->ref_email->parent_type == 'test' and $this->ref_email->parent_id == 'test');
				//following condition is for switching back to test mode.
				if (!$upd_ref_email) 
					$upd_ref_email = ($this->test and !empty($this->ref_email->id) and empty($this->ref_email->parent_type) and empty($this->ref_email->parent_id));
				if (empty($this->ref_email->id) or $upd_ref_email) {
					//create email record.
					$this->ref_email->id = $marketing_id;
					$this->ref_email->date_sent = '';
					
					if ($upd_ref_email == false) {
						$this->ref_email->new_with_id = true;
					}
					
					$this->ref_email->to_addrs = '';
					$this->ref_email->to_addrs_ids = '';
					$this->ref_email->to_addrs_names = '';
					$this->ref_email->to_addrs_emails = '';
					$this->ref_email->type = 'campaign';
					$this->ref_email->deleted = '0';
					$this->ref_email->name = $campagin_name . ': ' . $subject;
					$this->ref_email->description_html = $body_html;
					$this->ref_email->description = $body_text;
					$this->ref_email->from_addr = $from_address;
					$this->ref_email->from_addr_name = $from_address_name;
					$this->ref_email->assigned_user_id = $sender_id;
					if ($this->test) {
						$this->ref_email->parent_type = 'test';
						$this->ref_email->parent_id = 'test';
					} else {
						$this->ref_email->parent_type = '';
						$this->ref_email->parent_id = '';
					}
					$this->ref_email->date_start = $timedate->nowDate();
					$this->ref_email->time_start = $timedate->asUserTime($timedate->getNow(true));
					
					$this->ref_email->status = 'sent';
					$retId = $this->ref_email->save();
					
					foreach ($notes as $note) {
						if ($note->object_name == 'Note') {
							if (!empty($note->file->temp_file_location) && is_file($note->file->temp_file_location)) {
								$file_location = $note->file->temp_file_location;
								$filename = $note->file->original_file_name;
								$mime_type = $note->file->mime_type;
							} else {
								$file_location = "upload://{$note->id}";
								$filename = $note->id . $note->filename;
								$mime_type = $note->file_mime_type;
							}
						} elseif ($note->object_name == 'DocumentRevision') {
							// from Documents
							$filename = $note->id . $note->filename;
							$file_location = "upload://$filename";
							$mime_type = $note->file_mime_type;
						}
						
						$noteAudit = new Note();
						$noteAudit->parent_id = $retId;
						$noteAudit->parent_type = $this->ref_email->module_dir;
						$noteAudit->description = "[" . $note->filename . "] " . $mod_strings['LBL_ATTACHMENT_AUDIT'];
						$noteAudit->filename = $filename;
						$noteAudit->file_mime_type = $mime_type;
						$noteAudit_id = $noteAudit->save();
						
						UploadFile::duplicate_file($note->id, $noteAudit_id, $filename);
					}
				}
				
				//load relationships.
				$this->ref_email->load_relationship('users');
				$this->ref_email->load_relationship('prospects');
				$this->ref_email->load_relationship('contacts');
				$this->ref_email->load_relationship('leads');
				$this->ref_email->load_relationship('accounts');
			}
			
			if (!empty($this->related_id) && !empty($this->related_type)) {
				
				//save relationships.
				switch ($this->related_type) {
					case 'Users':
						$rel_name = "users";
						break;
					
					case 'Prospects':
						$rel_name = "prospects";
						break;
					
					case 'Contacts':
						$rel_name = "contacts";
						break;
					
					case 'Leads':
						$rel_name = "leads";
						break;
					
					case 'Accounts':
						$rel_name = "accounts";
						break;
				}
				
				//serialize data to be passed into Link2->add() function
				$campaignData = serialize($macro_nv);
				
				//required for one email per campaign per marketing message.
				$this->ref_email->$rel_name->add($this->related_id, array('campaign_data' => $this->db->quote($campaignData)));
			}
			return $this->ref_email->id;
		}
		
		/**
    * The function creates a copy of email send to each target.
    */
		function create_indiv_email($module, $mail)
		{
			
			//TODO (MT): specify type of proc. parameters
			global $locale, $timedate;
			$email = new Email();
			
			//TODO (MT EXTEND Code): retrieve and use the TENANT_ID
			$email->to_addrs = $module->name . '&lt;' . $module->email1 . '&gt;';
			$email->to_addrs_ids = $module->id . ';';
			$email->to_addrs_names = $module->name . ';';
			$email->to_addrs_emails = $module->email1 . ';';
			$email->type = 'archived';
			$email->deleted = '0';
			$email->name = $this->current_campaign->name . ': ' . $mail->Subject;
			if ($mail->ContentType == "text/plain") {
				$email->description = $mail->Body;
				$email->description_html = null;
			} else {
				$email->description_html = $mail->Body;
				$email->description = $mail->AltBody;
			}
			$email->from_addr = $mail->From;
			$email->assigned_user_id = $this->user_id;
			$email->parent_type = $this->related_type;
			$email->parent_id = $this->related_id;
			
			$email->date_start = $timedate->nowDbDate();
			$email->time_start = $timedate->asDbTime($timedate->getNow());
			$email->status = 'sent';
			$retId = $email->save();
			
			foreach ($this->notes_array as $note) {
				if (!class_exists('Note')) {
					
				}
				// create "audit" email without duping off the file to save on disk space
				$noteAudit = new Note();
				$noteAudit->parent_id = $retId;
				$noteAudit->parent_type = $email->module_dir;
				$noteAudit->description = "[" . $note->filename . "] " . $mod_strings['LBL_ATTACHMENT_AUDIT'];
				$noteAudit->save();
			}
			
			if (!empty($this->related_id) && !empty($this->related_type)) {
				
				//save relationships.
				switch ($this->related_type) {
					case 'Users':
						$rel_name = "users";
						break;
					
					case 'Prospects':
						$rel_name = "prospects";
						break;
					
					case 'Contacts':
						$rel_name = "contacts";
						break;
					
					case 'Leads':
						$rel_name = "leads";
						break;
					
					case 'Accounts':
						$rel_name = "accounts";
						break;
				}
				
				if (!empty($rel_name)) {
					$email->load_relationship($rel_name);
					$email->$rel_name->add($this->related_id);
				}
			}
			return $email->id;
		}
		/*
     * Call this function to verify the email_marketing message and email_template configured
     * for the campaign. If issues are found a fatal error will be logged but processing will not stop.
     * @return Boolean Returns true if all campaign parameters are set correctly
     */
		function verify_campaign($marketing_id)
		{
			
			if (!isset($this->verified_email_marketing_ids[$marketing_id])) {
				if (!class_exists('EmailMarketing')) {
					
				}
				$email_marketing = new EmailMarketing();
				$ret = $email_marketing->retrieve($marketing_id);
				if (empty($ret)) {
					$GLOBALS['log']->fatal('Error retrieving marketing message for the email campaign. marketing_id = ' . $marketing_id);
					return false;
				}
				
				//verify the email template.
				if (empty($email_marketing->template_id)) {
					$GLOBALS['log']->fatal('Error retrieving template for the email campaign. marketing_id = ' . $marketing_id);
					return false;
				}
				
				if (!class_exists('EmailTemplate')) {
					
				}
				$emailtemplate = new EmailTemplate();
				
				$ret = $emailtemplate->retrieve($email_marketing->template_id);
				if (empty($ret)) {
					$GLOBALS['log']->fatal('Error retrieving template for the email campaign. template_id = ' . $email_marketing->template_id);
					return false;
				}
				
				if (empty($emailtemplate->subject) and empty($emailtemplate->body) and empty($emailtemplate->body_html)) {
					$GLOBALS['log']->fatal('Email template is empty. email_template_id=' . $email_marketing->template_id);
					return false;
				}
				
			}
			$this->verified_email_marketing_ids[$marketing_id] = 1;
			
			return true;
		}
		function sendEmail($mail, $save_emails = 1, $testmode = false)
		{
			//TODO (MT): specify type of proc. parameters
			$this->test = $testmode;
			
			global $beanList, $beanFiles, $sugar_config;
			global $mod_strings;
			global $locale;
			$OBCharset = $locale->getPrecedentPreference('default_email_charset');
			$mod_strings = return_module_language($sugar_config['default_language'], 'EmailMan');
			
			//get tracking entities locations.
			if (!isset($this->tracking_url)) {
				if (!class_exists('Administration')) {
					
				}
				$admin = new Administration();
				$admin->retrieveSettings('massemailer'); //retrieve all admin settings.
				if (isset($admin->settings['massemailer_tracking_entities_location_type']) and $admin->settings['massemailer_tracking_entities_location_type'] == '2' and isset($admin->settings['massemailer_tracking_entities_location'])) {
					$this->tracking_url = $admin->settings['massemailer_tracking_entities_location'];
				} else {
					$this->tracking_url = $sugar_config['site_url'];
				}
			}
			
			//make sure tracking url ends with '/' character
			$strLen = strlen($this->tracking_url);
			if ($this->tracking_url{$strLen - 1} != '/') {
				$this->tracking_url .= '/';
			}
			
			if (!isset($beanList[$this->related_type])) {
				return false;
			}
			$class = $beanList[$this->related_type];
			if (!class_exists($class)) {
				require_once ($beanFiles[$class]);
			}
			
			if (!class_exists('Email')) {
				
			}
			
			//prepare variables for 'set_as_sent' function
			$this->target_tracker_key = create_guid();
			
			$module = new $class();
			$module->retrieve($this->related_id);
			$module->emailAddress->handleLegacyRetrieve($module);
			
			//check to see if bean has a primary email address
			if (!$this->is_primary_email_address($module)) {
				//no primary email address designated, do not send out email, create campaign log
				//of type send error to denote that this user was not emailed
				$this->set_as_sent($module->email1, true, null, null, 'send error');
				//create fatal logging for easy review of cause.
				$GLOBALS['log']->fatal('Email Address provided is not Primary Address for email with id ' . $module->email1 . "' Emailman id=$this->id");
				return true;
			}
			
			if (!$this->valid_email_address($module->email1)) {
				$this->set_as_sent($module->email1, true, null, null, 'invalid email');
				$GLOBALS['log']->fatal('Encountered invalid email address' . $module->email1 . " Emailman id=$this->id");
				return true;
			}
			
			if ((!isset($module->email_opt_out) 
				|| ($module->email_opt_out !== 'on' 
				&& $module->email_opt_out !== 1 
				&& $module->email_opt_out !== '1')) 
				&& (!isset($module->invalid_email) 
				|| ($module->invalid_email !== 'on' 
				&& $module->invalid_email !== 1 
				&& $module->invalid_email !== '1'))) {
				$lower_email_address = strtolower($module->email1);
				//test against indivdual address.
				if (isset($this->restricted_addresses) and isset($this->restricted_addresses[$lower_email_address])) {
					$this->set_as_sent($lower_email_address, true, null, null, 'blocked');
					return true;
				}
				//test against restricted domains
				$at_pos = strrpos($lower_email_address, '@');
				if ($at_pos !== false) {
					foreach ($this->restricted_domains as $domain => $value) {
						$pos = strrpos($lower_email_address, $domain);
						if ($pos !== false && $pos > $at_pos) {
							//found
							$this->set_as_sent($lower_email_address, true, null, null, 'blocked');
							return true;
						}
					}
				}
				
				//test for duplicate email address by marketing id.
				$dup_query = "select id from campaign_log where more_information='" . $this->db->quote($module->email1) . "' and marketing_id='" . $this->marketing_id . "'";
				$dup = $this->db->query($dup_query);
				$dup_row = $this->db->fetchByAssoc($dup);
				if (!empty($dup_row)) {
					//we have seen this email address before
					$this->set_as_sent($module->email1, true, null, null, 'blocked');
					return true;
				}
				
				
				//fetch email marketing.
				if (empty($this->current_emailmarketing) or !isset($this->current_emailmarketing)) {
					if (!class_exists('EmailMarketing')) {
						
					}
					
					$this->current_emailmarketing = new EmailMarketing();
					
				}
				if (empty($this->current_emailmarketing->id) or $this->current_emailmarketing->id !== $this->marketing_id) {
					$this->current_emailmarketing->retrieve($this->marketing_id);
					
					$this->newmessage = true;
				}
				//fetch email template associate with the marketing message.
				if (empty($this->current_emailtemplate) or $this->current_emailtemplate->id !== $this->current_emailmarketing->template_id) {
					if (!class_exists('EmailTemplate')) {
						
					}
					$this->current_emailtemplate = new EmailTemplate();
					

					//TODO (MT EXTEND Code): retrieve and use the TENANT_ID
					$this->current_emailtemplate->retrieve($this->current_emailmarketing->template_id);
					
					//escape email template contents.
					$this->current_emailtemplate->subject = from_html($this->current_emailtemplate->subject);
					$this->current_emailtemplate->body_html = from_html($this->current_emailtemplate->body_html);
					$this->current_emailtemplate->body = from_html($this->current_emailtemplate->body);
					
					$q = "SELECT * FROM notes WHERE parent_id = '" . $this->current_emailtemplate->id . "' AND deleted = 0";
					$r = $this->db->query($q);
					
					// cn: bug 4684 - initialize the notes array, else old data is still around for the next round
					$this->notes_array = array();
					if (!class_exists('Note')) {
						require_once ('modules/Notes/Note.php');
					}
					while ($a = $this->db->fetchByAssoc($r)) {
						$noteTemplate = new Note();
						$noteTemplate->retrieve($a['id']);
						$this->notes_array[] = $noteTemplate;
					}
				}
				
				// fetch mailbox details..
				if (empty($this->current_mailbox)) {
					if (!class_exists('InboundEmail')) {
						
					}
					$this->current_mailbox = new InboundEmail();
				}
				if (empty($this->current_mailbox->id) or $this->current_mailbox->id !== $this->current_emailmarketing->inbound_email_id) {
					$this->current_mailbox->retrieve($this->current_emailmarketing->inbound_email_id);
					//extract the email address.
					$this->mailbox_from_addr = $this->current_mailbox->get_stored_options('from_addr', 'nobody@example.com', null);
				}
				
				// fetch campaign details..
				if (empty($this->current_campaign)) {
					if (!class_exists('Campaign')) {
						
					}
					$this->current_campaign = new Campaign();
				}

				if (empty($this->current_campaign->id) or $this->current_campaign->id !== $this->current_emailmarketing->campaign_id) {
					//TODO (MT EXTEND Code): retrieve and use the TENANT_ID
					$this->current_campaign->retrieve($this->current_emailmarketing->campaign_id);
					
					//load defined tracked_urls
					$this->current_campaign->load_relationship('tracked_urls');
					$query_array = $this->current_campaign->tracked_urls->getQuery(true);
					$query_array['select'] = "SELECT tracker_name, tracker_key, id, is_optout ";
					$result = $this->current_campaign->db->query(implode(' ', $query_array));
					
					$this->has_optout_links = false;
					$this->tracker_urls = array();
					while (($row = $this->current_campaign->db->fetchByAssoc($result)) != null) {
						$this->tracker_urls['{' . $row['tracker_name'] . '}'] = $row;
						//has the user defined opt-out links for the campaign.
						if ($row['is_optout'] == 1) {
							$this->has_optout_links = true;
						}
					}
				}
				
				$mail->ClearAllRecipients();
				$mail->ClearReplyTos();
				$mail->Sender = $this->mailbox_from_addr;
				$mail->From = $this->mailbox_from_addr;
				$mail->FromName = $locale->translateCharsetMIME(trim($this->current_emailmarketing->from_name), 'UTF-8', $OBCharset);
				$mail->ClearCustomHeaders();
				$mail->AddCustomHeader('X-CampTrackID:' . $this->target_tracker_key);
				//CL - Bug 25256 Check if we have a reply_to_name/reply_to_addr value from the email marketing table.  If so use email marketing entry; otherwise current mailbox (inbound email) entry
				$replyToName = empty($this->current_emailmarketing->reply_to_name) ? $this->current_mailbox->get_stored_options('reply_to_name', $mail->FromName, null) : $this->current_emailmarketing->reply_to_name;
				$replyToAddr = empty($this->current_emailmarketing->reply_to_addr) ? $this->current_mailbox->get_stored_options('reply_to_addr', $mail->From, null) : $this->current_emailmarketing->reply_to_addr;
				
				if (!empty($replyToAddr)) {
					$mail->AddReplyTo($replyToAddr, $locale->translateCharsetMIME(trim($replyToName), 'UTF-8', $OBCharset));
				}
				
				//parse and replace bean variables.
				$macro_nv = array();
				$focus_name = 'Contacts';
				if ($module->module_dir == 'Accounts') {
					$focus_name = 'Accounts';
				}
				
				
				$template_data = $this->current_emailtemplate->parse_email_template(array('subject' => $this->current_emailtemplate->subject, 
					'body_html' => $this->current_emailtemplate->body_html, 
					'body' => $this->current_emailtemplate->body), $focus_name, $module, $macro_nv);
				
				//add email address to this list.
				$macro_nv['sugar_to_email_address'] = $module->email1;
				$macro_nv['email_template_id'] = $this->current_emailmarketing->template_id;
				
				//parse and replace urls.
				//this is new style of adding tracked urls to a campaign.
				$tracker_url_template = $this->tracking_url . 'index.php?entryPoint=campaign_trackerv2&track=%s' . '&identifier=' . $this->target_tracker_key;
				$removeme_url_template = $this->tracking_url . 'index.php?entryPoint=removeme&identifier=' . $this->target_tracker_key;
				$template_data = $this->current_emailtemplate->parse_tracker_urls($template_data, $tracker_url_template, $this->tracker_urls, $removeme_url_template);
				$mail->AddAddress($module->email1, $locale->translateCharsetMIME(trim($module->name), 'UTF-8', $OBCharset));
				
				//refetch strings in case they have been changed by creation of email templates or other beans.
				$mod_strings = return_module_language($sugar_config['default_language'], 'EmailMan');
				
				if ($this->test) {
					$mail->Subject = $mod_strings['LBL_PREPEND_TEST'] . $template_data['subject'];
				} else {
					$mail->Subject = $template_data['subject'];
				}
				
				//check if this template is meant to be used as "text only"
				$text_only = false;
				if (isset($this->current_emailtemplate->text_only) && $this->current_emailtemplate->text_only) {
					$text_only = true;
				}
				//if this template is textonly, then just send text body.  Do not add tracker, opt out,
				//or perform other processing as it will not show up in text only email
				if ($text_only) {
					$this->description_html = '';
					$mail->IsHTML(false);
					$mail->Body = $template_data['body'];
					
				} else {
					$mail->Body = wordwrap($template_data['body_html'], 900);
					//BEGIN:this code will trigger for only campaigns pending before upgrade to 4.2.0.
					//will be removed for the next release.
					if (!isset($btracker)) 
						$btracker = false;
					if ($btracker) {
						$mail->Body .= "<br /><br /><a href='" . $tracker_url . "'>" . $tracker_text . "</a><br /><br />";
					} else {
						if (!empty($tracker_url)) {
							$mail->Body = str_replace('TRACKER_URL_START', "<a href='" . $tracker_url . "'>", $mail->Body);
							$mail->Body = str_replace('TRACKER_URL_END', "</a>", $mail->Body);
							$mail->AltBody = str_replace('TRACKER_URL_START', "<a href='" . $tracker_url . "'>", $mail->AltBody);
							$mail->AltBody = str_replace('TRACKER_URL_END', "</a>", $mail->AltBody);
						}
					}
					//END
					
					//do not add the default remove me link if the campaign has a trackerurl of the opotout link
					if ($this->has_optout_links == false) {
						$mail->Body .= "<br /><span style='font-size:0.8em'>{$mod_strings['TXT_REMOVE_ME']} <a href='" . $this->tracking_url . "index.php?entryPoint=removeme&identifier={$this->target_tracker_key}'>{$mod_strings['TXT_REMOVE_ME_CLICK']}</a></span>";
					}
					// cn: bug 11979 - adding single quote to comform with HTML email RFC
					$mail->Body .= "<br /><img alt='' height='1' width='1' src='{$this->tracking_url}index.php?entryPoint=image&identifier={$this->target_tracker_key}' />";
					
					$mail->AltBody = $template_data['body'];
					if ($btracker) {
						$mail->AltBody .= "\n" . $tracker_url;
					}
					if ($this->has_optout_links == false) {
						$mail->AltBody .= "\n\n\n{$mod_strings['TXT_REMOVE_ME_ALT']} " . $this->tracking_url . "index.php?entryPoint=removeme&identifier=$this->target_tracker_key";
					}
					
				}
				
				// cn: bug 4684, handle attachments in email templates.
				$mail->handleAttachments($this->notes_array);
				$tmp_Subject = $mail->Subject;
				$mail->prepForOutbound();
				
				$success = $mail->Send();
				//Do not save the encoded subject.
				$mail->Subject = $tmp_Subject;
				if ($success) {
					$email_id = null;
					if ($save_emails == 1) {
						$email_id = $this->create_indiv_email($module, $mail);
					} else {
						//find/create reference email record. all campaign targets reveiving this message will be linked with this message.
						$decodedFromName = mb_decode_mimeheader($this->current_emailmarketing->from_name);
						$fromAddressName = "{$decodedFromName} <{$this->mailbox_from_addr}>";
						
						$email_id = $this->create_ref_email($this->marketing_id, $this->current_emailtemplate->subject, $this->current_emailtemplate->body, $this->current_emailtemplate->body_html, $this->current_campaign->name, $this->mailbox_from_addr, $this->user_id, $this->notes_array, $macro_nv, $this->newmessage, $fromAddressName);
						$this->newmessage = false;
					}
				}
				
				if ($success) {
					$this->set_as_sent($module->email1, true, $email_id, 'Emails', 'targeted');
				} else {
					
					if (!empty($layout_def['parent_id'])) {
						if (isset($layout_def['fields'][strtoupper($layout_def['parent_id'])])) {
							$parent .= "&parent_id=" . $layout_def['fields'][strtoupper($layout_def['parent_id'])];
						}
					}
					if (!empty($layout_def['parent_module'])) {
						if (isset($layout_def['fields'][strtoupper($layout_def['parent_module'])])) {
							$parent .= "&parent_module=" . $layout_def['fields'][strtoupper($layout_def['parent_module'])];
						}
					}
					//log send error. save for next attempt after 24hrs. no campaign log entry will be created.
					$this->set_as_sent($module->email1, false, null, null, 'send error');
				}
			} else {
				$success = false;
				$this->target_tracker_key = create_guid();
				
				if (isset($module->email_opt_out) && ($module->email_opt_out === 'on' || $module->email_opt_out == '1' || $module->email_opt_out == 1)) {
					$this->set_as_sent($module->email1, true, null, null, 'removed');
				} else {
					if (isset($module->invalid_email) && ($module->invalid_email == 1 || $module->invalid_email == '1')) {
						$this->set_as_sent($module->email1, true, null, null, 'invalid email');
					} else {
						$this->set_as_sent($module->email1, true, null, null, 'send error');
					}
				}
			}
			
			return $success;
		}
		
		/*
	 * Validates the passed email address.
	 * Limitations of this algorithm: does not validate email addressess that end with .meuseum
	 *
	 */
		function valid_email_address($email_address)
		{
			
			$email_address = trim($email_address);
			if (empty($email_address)) {
				return false;
			}
			
			$pattern = '/[A-Z0-9\._%-]+@[A-Z0-9\.-]+\.[A-Za-z]{2,}$/i';
			$ret = preg_match($pattern, $email_address);
			echo $ret;
			if ($ret === false or $ret == 0) {
				return false;
			}
			return true;
		}
		
		/*
     * This function takes in the given bean and searches for a related email address
     * that has been designated as primary.  If one is found, true is returned
     * If no primary email address is found, then false is returned
     *
     */
		function is_primary_email_address($bean)
		{
			$email_address = trim($bean->email1);
			
			if (empty($email_address)) {
				return false;
			}
			//query for this email address rel and see if this is primary address
			$primary_qry = "select email_address_id from email_addr_bean_rel where bean_id = '" . $bean->id . "' and email_addr_bean_rel.primary_address=1 and deleted=0";
			$res = $bean->db->query($primary_qry);
			$prim_row = $this->db->fetchByAssoc($res);
			//return true if this is primary
			if (!empty($prim_row)) {
				return true;
			}
			return false;
			
		}
		
		function create_export_query(&$order_by, &$where)
		{
			$custom_join = $this->getCustomJoin(true, true, $where);
			$query = "SELECT emailman.*";
			$query .= $custom_join['select'];
			
			$query .= " FROM emailman ";
			
			$query .= $custom_join['join'];
			
			$where_auto = "( emailman.deleted IS NULL OR emailman.deleted=0 )";
			
			if ($where != "") 
				$query .= "where ($where) AND " . $where_auto;
			 else 
				$query .= "where " . $where_auto;
			
			$order_by = $this->process_order_by($order_by);
			if (!empty($order_by)) {
				$query .= ' ORDER BY ' . $order_by;
			}
			
			return $query;
		}
		
		/**
     * Actuall deletes the emailman record
     * @param int $id
     */
		public function mark_deleted($id)
		{
			$this->db->query("DELETE FROM {$this->table_name} WHERE id=" . intval($id));
		}
	}
	
	
	//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	
	require_once ('include/SugarPHPMailer.php');
	
	$test = false;
	if (isset($_REQUEST['mode']) && $_REQUEST['mode'] == 'test') {
		$test = true;
	}
	if (isset($_REQUEST['send_all']) && $_REQUEST['send_all'] == true) {
		$send_all = true;
	} else {
		$send_all = false; //if set to true email delivery will continue..to run until all email have been delivered.
	}
	
	if (!isset($GLOBALS['log'])) {
		$GLOBALS['log'] = LoggerManager::getLogger('SugarCRM');
	}
	
	$mail = new SugarPHPMailer();
	$admin = new Administration();
	$admin->retrieveSettings();
	if (isset($admin->settings['massemailer_campaign_emails_per_run'])) {
		$max_emails_per_run = $admin->settings['massemailer_campaign_emails_per_run'];
	}
	if (empty($max_emails_per_run)) {
		$max_emails_per_run = 500; //default
	}
	//save email copies?
	$massemailer_email_copy = 0; //default: save copies of the email.
	if (isset($admin->settings['massemailer_email_copy'])) {
		$massemailer_email_copy = $admin->settings['massemailer_email_copy'];
	}
	
	$emailsPerSecond = 10;
	
	$mail->setMailerForSystem();
	$mail->From = "no-reply@example.com";
	$mail->FromName = "no-reply";
	$mail->ContentType = "text/html";
	
	$campaign_id = null;
	if (isset($_REQUEST['campaign_id']) && !empty($_REQUEST['campaign_id'])) {
		$campaign_id = $_REQUEST['campaign_id'];
	}
	
	$db = DBManagerFactory::getInstance();
	$timedate = TimeDate::getInstance();
	$emailman = new EmailMan();
	
	if ($test) {
		//if this is in test mode, then
		//find all the message that meet the following criteria.
		//1. scheduled send date time is now
		//2. campaign matches the current campaign
		//3. recipient belongs to a prospect list of type test, attached to this campaign
		
		$select_query = " SELECT em.* FROM emailman em";
		$select_query .= " join prospect_list_campaigns plc on em.campaign_id = plc.campaign_id";
		$select_query .= " join prospect_lists pl on pl.id = plc.prospect_list_id ";
		$select_query .= " WHERE em.list_id = pl.id and pl.list_type = 'test'";
		$select_query .= " AND em.send_date_time <= " . $db->now();
		$select_query .= " AND (em.in_queue ='0' OR em.in_queue IS NULL OR (em.in_queue ='1' AND em.in_queue_date <= " . $db->convert($db->quoted($timedate->fromString("-1 day")->asDb()), "datetime") . "))";
		$select_query .= " AND em.campaign_id='{$campaign_id}'";
		$select_query .= " ORDER BY em.send_date_time ASC, em.user_id, em.list_id";
	} else {
		//this is not a test..
		//find all the message that meet the following criteria.
		//1. scheduled send date time is now
		//2. were never processed or last attempt was 24 hours ago
		$select_query = " SELECT *";
		$select_query .= " FROM $emailman->table_name";
		$select_query .= " WHERE send_date_time <= " . $db->now();
		$select_query .= " AND (in_queue ='0' OR in_queue IS NULL OR ( in_queue ='1' AND in_queue_date <= " . $db->convert($db->quoted($timedate->fromString("-1 day")->asDb()), "datetime") . "))";
		
		if (!empty($campaign_id)) {
			$select_query .= " AND campaign_id='{$campaign_id}'";
		}
		$select_query .= " ORDER BY send_date_time ASC,user_id, list_id";
		
	}
	
	//bug 26926 fix start
	DBManager::setQueryLimit(0);
	//end bug fix
	
	do {
		
		$no_items_in_queue = true;
		
		$result = $db->limitQuery($select_query, 0, $max_emails_per_run);
		global $current_user;
		if (isset($current_user)) {
			$temp_user = $current_user;
		}
		$current_user = new User();
		$startTime = microtime(true);
		
		while (($row = $db->fetchByAssoc($result)) != null) {
			
			//verify the queue item before further processing.
			//we have found cases where users have taken away access to email templates while them message is in queue.
			if (empty($row['campaign_id'])) {
				$GLOBALS['log']->fatal('Skipping emailman entry with empty campaign id' . print_r($row, true));
				continue;
			}
			if (empty($row['marketing_id'])) {
				$GLOBALS['log']->fatal('Skipping emailman entry with empty marketing id' . print_r($row, true));
				continue; //do not process this row .
			}
			
			//fetch user that scheduled the campaign.
			if (empty($current_user) or $row['user_id'] != $current_user->id) {
				$current_user->retrieve($row['user_id']);
			}
			
			if (!$emailman->verify_campaign($row['marketing_id'])) {
				$GLOBALS['log']->fatal('Error verifying templates for the campaign, exiting');
				continue;
			}
			
			
			//verify the email template too..
			//find the template associated with marketing message. make sure that template has a subject and
			//a non-empty body
			if (!isset($template_status[$row['marketing_id']])) {
				if (!class_exists('EmailMarketing')) {
					
				}
				$current_emailmarketing = new EmailMarketing();
				$current_emailmarketing->retrieve($row['marketing_id']);
				
				if (!class_exists('EmailTemplate')) {
					
				}
				$current_emailtemplate = new EmailTemplate();
				$current_emailtemplate->retrieve($current_emailmarketing->template_id);
				
			}
			
			//acquire a lock.
			//if the database does not support repeatable read isolation by default, we might get data that does not meet
			//the criteria in the original query, and we care most about the in_queue_date and process_date_time,
			//if they are null or in past(older than 24 horus) then we are okay.
			
			$lock_query = "UPDATE emailman SET in_queue=1, in_queue_date=" . $db->now() . " WHERE id = " . intval($row['id']);
			$lock_query .= " AND (in_queue ='0' OR in_queue IS NULL OR ( in_queue ='1' AND in_queue_date <= " . $db->convert($db->quoted($timedate->fromString("-1 day")->asDb()), "datetime") . "))";
			
			//if the query fails to execute.. terminate campaign email process.
			$lock_result = $db->query($lock_query, true, 'Error acquiring a lock for emailman entry.');
			$lock_count = $db->getAffectedRowCount($lock_result);
			
			//do not process the message if unable to acquire lock.
			if ($lock_count != 1) {
				$GLOBALS['log']->fatal("Error acquiring lock for the emailman entry, skipping email delivery. lock status=$lock_count " . print_r($row, true));
				continue; //do not process this row we will examine it after 24 hrs. the email address based dupe check is in place too.
			}
			
			$no_items_in_queue = false;
			
			
			foreach ($row as $name => $value) {
				$emailman->$name = $value;
			}
			
			//for the campaign process the suppression lists.
			if (!isset($current_campaign_id) or empty($current_campaign_id) or $current_campaign_id != $row['campaign_id']) {
				$current_campaign_id = $row['campaign_id'];
				
				//is this email address suppressed?
				$plc_query = " SELECT prospect_list_id, prospect_lists.list_type,prospect_lists.domain_name FROM prospect_list_campaigns ";
				$plc_query .= " LEFT JOIN prospect_lists on prospect_lists.id = prospect_list_campaigns.prospect_list_id";
				$plc_query .= " WHERE ";
				$plc_query .= " campaign_id='{$current_campaign_id}' ";
				$plc_query .= " AND prospect_lists.list_type in ('exempt_address','exempt_domain')";
				$plc_query .= " AND prospect_list_campaigns.deleted=0";
				$plc_query .= " AND prospect_lists.deleted=0";
				
				$emailman->restricted_domains = array();
				$emailman->restricted_addresses = array();
				
				$result1 = $db->query($plc_query);
				while ($row1 = $db->fetchByAssoc($result1)) {
					if ($row1['list_type'] == 'exempt_domain') {
						$emailman->restricted_domains[strtolower($row1['domain_name'])] = 1;
					} else {
						//find email address of targets in this prospect list.
						$email_query = "SELECT email_address FROM email_addresses ea JOIN email_addr_bean_rel eabr ON ea.id = eabr.email_address_id JOIN prospect_lists_prospects plp ON eabr.bean_id = plp.related_id AND eabr.bean_module = plp.related_type AND plp.prospect_list_id = '{$row1['prospect_list_id']}' and plp.deleted = 0";
						$email_query_result = $db->query($email_query);
						
						while ($email_address = $db->fetchByAssoc($email_query_result)) {
							//ignore empty email addresses;
							if (!empty($email_address['email_address'])) {
								$emailman->restricted_addresses[strtolower($email_address['email_address'])] = 1;
							}
						}
					}
				}
			}
			
			if (!$emailman->sendEmail($mail, $massemailer_email_copy, $test)) {
				$GLOBALS['log']->fatal("Email delivery FAILURE:" . print_r($row, true));
			} else {
				$GLOBALS['log']->debug("Email delivery SUCCESS:" . print_r($row, true));
			}
			if ($mail->isError()) {
				$GLOBALS['log']->fatal("Email delivery error:" . print_r($row, true) . $mail->ErrorInfo);
			}
		}
		
		$send_all = $send_all ? !$no_items_in_queue : $send_all;
		
	} while ($send_all == true);
	
	if ($admin->settings['mail_sendtype'] == "SMTP") {
		$mail->SMTPClose();
	}
	if (isset($temp_user)) {
		$current_user = $temp_user;
	}
	if (isset($_REQUEST['return_module']) && isset($_REQUEST['return_action']) && isset($_REQUEST['return_id'])) {
		$from_wiz = ' ';
		if (isset($_REQUEST['from_wiz']) && $_REQUEST['from_wiz'] == true) {
			header("Location: index.php?module={$_REQUEST['return_module']}&action={$_REQUEST['return_action']}&record={$_REQUEST['return_id']}&from=test");
		} else {
			header("Location: index.php?module={$_REQUEST['return_module']}&action={$_REQUEST['return_action']}&record={$_REQUEST['return_id']}");
		}
	} else {
		/* this will be triggered when manually sending off Email campaigns from the
	 * Mass Email Queue Manager.
 	*/
		if (isset($_POST['manual'])) {
			header("Location: index.php?module=EmailMan&action=index");
		}
	}
?>



//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description:  Contains a variety of utility functions specific to this module.
 ********************************************************************************/
	
	/**
 * Create javascript to validate the data entered into a record.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 */
	function get_validate_record_js()
	{
		global $mod_strings;
		global $app_strings;
		
		$lbl_email_per_run = $mod_strings['LBL_EMAILS_PER_RUN'];
		$lbl_location = $mod_strings['LBL_LOCATION_ONLY'];
		$err_int_only = $mod_strings['ERR_INT_ONLY_EMAIL_PER_RUN'];
		$err_missing_required_fields = $app_strings['ERR_MISSING_REQUIRED_FIELDS'];
		$err_from_name = $mod_strings['LBL_LIST_FROM_NAME'];
		$err_from_addr = $app_strings['LBL_EMAIL_SETTINGS_FROM_ADDR'];
		$err_smtpport = $mod_strings['LBL_MAIL_SMTPPORT'];
		$err_mailserver = $mod_strings['LBL_MAIL_SMTPSERVER'];
		$err_smtpuser = $mod_strings['LBL_MAIL_SMTPUSER'];
		$err_smtppass = $mod_strings['LBL_MAIL_SMTPPASS'];
		
		$the_script = <<<EOQ

<script type="text/javascript" language="Javascript">
<!--  to hide script contents from old browsers

function verify_data(button) {
	var isError = false;
	var errorMessage = "";
	if (typeof button.form['campaign_emails_per_run'] != 'undefined' && trim(button.form['campaign_emails_per_run'].value) == "") {
		isError = true;
		errorMessage += "\\n$lbl_email_per_run";
	} else {
		 //make sure emails per run  is an integer.
		if (typeof button.form['campaign_emails_per_run'] != 'undefined' && isInteger(trim(button.form['campaign_emails_per_run'].value)) == false) {
			isError = true;
			errorMessage += "\\n$err_int_only";
		}
	}
	if (typeof button.form['tracking_entities_location_type'] != 'undefined' && button.form['tracking_entities_location_type'][1].checked == true) {
		if (typeof button.form['tracking_entities_location'] != 'undefined' && trim(button.form['tracking_entities_location'].value) == "") {
			isError = true;
			errorMessage += "\\n$lbl_location";
		}
	}
	if (typeof document.forms['ConfigureSettings'] != 'undefined') {
        var fromname = document.getElementById('notify_fromname').value;
        var fromAddress = document.getElementById('notify_fromaddress').value;
        var sendType = document.getElementById('mail_sendtype').value;
        var smtpPort = document.getElementById('mail_smtpport').value;
        var smtpserver = document.getElementById('mail_smtpserver').value;
        var mailsmtpauthreq = document.getElementById('mail_smtpauth_req');

        if(trim(fromname) == "") {
			isError = true;
			errorMessage += "\\n$err_from_name";
        }
        if(trim(fromAddress) == "") {
			isError = true;
			errorMessage += "\\n$err_from_addr";
        }

        if (sendType == 'SMTP') {
	        if(trim(smtpserver) == "") {
				isError = true;
				errorMessage += "\\n$err_mailserver";
	        }
	        if(trim(smtpPort) == "") {
				isError = true;
				errorMessage += "\\n$err_smtpport";
	        }
	        if (mailsmtpauthreq.checked) {
		        if(trim(document.getElementById('mail_smtpuser').value) == "") {
					isError = true;
					errorMessage += "\\n$err_smtpuser";
		        }
	        }

        } // if
	} // if

	// Here we decide whether to submit the form.
	if (isError == true) {
		alert("$err_missing_required_fields" + errorMessage);
		return false;
	}
	return true;
}

function add_checks(f) {
	removeFromValidate('ConfigureSettings', 'mail_smtpserver');
	removeFromValidate('ConfigureSettings', 'mail_smtpport');
	removeFromValidate('ConfigureSettings', 'mail_smtpuser');
	removeFromValidate('ConfigureSettings', 'mail_smtppass');

	if (f.mail_sendtype.value == "SMTP") {
		addToValidate('ConfigureSettings', 'mail_smtpserver', 'varchar', 'true', '{$mod_strings['LBL_MAIL_SMTPSERVER']}');
		addToValidate('ConfigureSettings', 'mail_smtpport', 'int', 'true', '{$mod_strings['LBL_MAIL_SMTPPORT']}');
		if (f.mail_smtpauth_req.checked) {
			addToValidate('ConfigureSettings', 'mail_smtpuser', 'varchar', 'true', '{$mod_strings['LBL_MAIL_SMTPUSER']}');
			addToValidate('ConfigureSettings', 'mail_smtppass', 'varchar', 'true', '{$mod_strings['LBL_MAIL_SMTPPASS']}');
		}
	}

	return true;
}

// end hiding contents from old browsers  -->
</script>
EOQ;
		return $the_script;
	}
?>


//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description:  TODO To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
	
	
	$module_menu = array();
	
	//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
	
	
	
	
	require_once ('modules/Emails/Email.php');
	
	global $mod_strings;
	global $app_list_strings;
	global $app_strings;
	global $current_user;
	
	$json = getJSONobj();
	$pass = '';
	if (!empty($_REQUEST['mail_smtppass'])) {
		$pass = $_REQUEST['mail_smtppass'];
	} else 
		if (!empty($_REQUEST['mail_type']) && $_REQUEST['mail_type'] == 'system') {
			$oe = new OutboundEmail();
			$oe = $oe->getSystemMailerSettings();
			if (!empty($oe)) {
				$pass = $oe->mail_smtppass;
			}
		} elseif (isset($_REQUEST['mail_name'])) {
			$oe = new OutboundEmail();
			$oe = $oe->getMailerByName($current_user, $_REQUEST['mail_name']);
			if (!empty($oe)) {
				$pass = $oe->mail_smtppass;
			}
		}
	$out = Email::sendEmailTest($_REQUEST['mail_smtpserver'], $_REQUEST['mail_smtpport'], $_REQUEST['mail_smtpssl'], ($_REQUEST['mail_smtpauth_req'] == 'true' ? 1 : 0), $_REQUEST['mail_smtpuser'], $pass, $_REQUEST['outboundtest_from_address'], $_REQUEST['outboundtest_to_address'], $_REQUEST['mail_sendtype'], $_REQUEST['mail_from_name']);
	
	$out = $json->encode($out);
	echo $out;
?>



//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
	
	require_once ('include/MVC/View/SugarView.php');
	require_once ('modules/EmailMan/Forms.php');
	
	class ViewCampaignconfig extends SugarView
	{
		/**
	 * @see SugarView::_getModuleTitleParams()
	 */
		protected function _getModuleTitleParams($browserTitle = false)
		{
			//TODO (MT): specify type of proc. parameters
			global $mod_strings;
			
			return array(
				"<a href='index.php?module=Administration&action=index'>" . translate('LBL_MODULE_NAME', 'Administration') . "</a>", translate('LBL_CAMPAIGN_CONFIG_TITLE', 'Administration'));
		}
		
		/**
	 * @see SugarView::preDisplay()
	 */
		public function preDisplay()
		{
			global $current_user;
			
			if (!is_admin($current_user) 
				&& !is_admin_for_module($GLOBALS['current_user'], 'Campaigns')) 
				sugar_die("Unauthorized access to administration.");
		}
		
		/**
	 * @see SugarView::display()
	 */
		public function display()
		{
			global $mod_strings;
			global $app_list_strings;
			global $app_strings;
			global $current_user;
			
			echo $this->getModuleTitle(false);
			global $currentModule, $sugar_config;
			
			$focus = new Administration();
			$focus->retrieveSettings(); //retrieve all admin settings.
			$GLOBALS['log']->info("Mass Emailer(EmailMan) ConfigureSettings view");
			
			$this->ss->assign("MOD", $mod_strings);
			$this->ss->assign("APP", $app_strings);
			$this->ss->assign("THEME", SugarThemeRegistry::current()->__toString());
			$this->ss->assign("RETURN_MODULE", "Administration");
			$this->ss->assign("RETURN_ACTION", "index");
			
			$this->ss->assign("MODULE", $currentModule);
			$this->ss->assign("PRINT_URL", "index.php?" . $GLOBALS['request_string']);
			
			if (isset($focus->settings['massemailer_campaign_emails_per_run']) && !empty($focus->settings['massemailer_campaign_emails_per_run'])) {
				$this->ss->assign("EMAILS_PER_RUN", $focus->settings['massemailer_campaign_emails_per_run']);
			} else {
				$this->ss->assign("EMAILS_PER_RUN", 500);
			}
			
			if (!isset($focus->settings['massemailer_tracking_entities_location_type']) or empty($focus->settings['massemailer_tracking_entities_location_type']) or $focus->settings['massemailer_tracking_entities_location_type'] == '1') {
				$this->ss->assign("default_checked", "checked");
				$this->ss->assign("TRACKING_ENTRIES_LOCATION_STATE", "disabled");
				$this->ss->assign("TRACKING_ENTRIES_LOCATION", $mod_strings['TRACKING_ENTRIES_LOCATION_DEFAULT_VALUE']);
			} else {
				$this->ss->assign("userdefined_checked", "checked");
				$this->ss->assign("TRACKING_ENTRIES_LOCATION", $focus->settings["massemailer_tracking_entities_location"]);
			}
			$this->ss->assign("SITEURL", $sugar_config['site_url']);
			
			
			// Change the default campaign to not store a copy of each message.
			if (!empty($focus->settings['massemailer_email_copy']) and $focus->settings['massemailer_email_copy'] == '1') {
				$this->ss->assign("yes_checked", "checked='checked'");
			} else {
				$this->ss->assign("no_checked", "checked='checked'");
			}
			
			$email = new Email();
			
			//TODO (MT EXTEND Code): retrieve and use the TENANT_ID
			$this->ss->assign('ROLLOVER', $email->rolloverStyle);
			
			$this->ss->assign("JAVASCRIPT", get_validate_record_js());
			$this->ss->display("modules/EmailMan/tpls/campaignconfig.tpl");
		}
	}
	
	
	//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
	
	require_once ('include/MVC/View/SugarView.php');
	require_once ('modules/EmailMan/Forms.php');
	
	class ViewConfig extends SugarView
	{
		/**
	 * @see SugarView::_getModuleTitleParams()
	 */
		protected function _getModuleTitleParams($browserTitle = false)
		{
			//TODO (MT): specify type of proc. parameters
			global $mod_strings;
			
			return array(
				"<a href='index.php?module=Administration&action=index'>" . translate('LBL_MODULE_NAME', 'Administration') . "</a>", translate('LBL_MASS_EMAIL_CONFIG_TITLE', 'Administration'));
		}
		
		/**
	 * @see SugarView::preDisplay()
	 */
		public function preDisplay()
		{
			global $current_user;
			
			if (!is_admin($current_user) 
				&& !is_admin_for_module($GLOBALS['current_user'], 'Emails') 
				&& !is_admin_for_module($GLOBALS['current_user'], 'Campaigns')) 
				sugar_die("Unauthorized access to administration.");
		}
		
		/**
	 * @see SugarView::display()
	 */
		public function display()
		{
			global $mod_strings;
			global $app_list_strings;
			global $app_strings;
			global $current_user;
			global $sugar_config;
			
			echo $this->getModuleTitle(false);
			global $currentModule;
			
			$focus = new Administration();
			$focus->retrieveSettings(); //retrieve all admin settings.
			$GLOBALS['log']->info("Mass Emailer(EmailMan) ConfigureSettings view");
			
			$this->ss->assign("MOD", $mod_strings);
			$this->ss->assign("APP", $app_strings);
			
			$this->ss->assign("RETURN_MODULE", "Administration");
			$this->ss->assign("RETURN_ACTION", "index");
			
			$this->ss->assign("MODULE", $currentModule);
			$this->ss->assign("PRINT_URL", "index.php?" . $GLOBALS['request_string']);
			$this->ss->assign("HEADER", get_module_title("EmailMan", "{MOD.LBL_CONFIGURE_SETTINGS}", true));
			$this->ss->assign("notify_fromaddress", $focus->settings['notify_fromaddress']);
			$this->ss->assign("notify_send_from_assigning_user", (isset($focus->settings['notify_send_from_assigning_user']) && !empty($focus->settings['notify_send_from_assigning_user'])) ? "checked='checked'" : "");
			$this->ss->assign("notify_on", ($focus->settings['notify_on']) ? "checked='checked'" : "");
			$this->ss->assign("notify_fromname", $focus->settings['notify_fromname']);
			$this->ss->assign("notify_allow_default_outbound_on", (!empty($focus->settings['notify_allow_default_outbound']) && $focus->settings['notify_allow_default_outbound']) ? "checked='checked'" : "");
			
			$this->ss->assign("mail_smtptype", $focus->settings['mail_smtptype']);
			$this->ss->assign("mail_smtpserver", $focus->settings['mail_smtpserver']);
			$this->ss->assign("mail_smtpport", $focus->settings['mail_smtpport']);
			$this->ss->assign("mail_smtpuser", $focus->settings['mail_smtpuser']);
			$this->ss->assign("mail_smtpauth_req", ($focus->settings['mail_smtpauth_req']) ? "checked='checked'" : "");
			$this->ss->assign("mail_haspass", empty($focus->settings['mail_smtppass']) ? 0 : 1);
			$this->ss->assign("MAIL_SSL_OPTIONS", get_select_options_with_id($app_list_strings['email_settings_for_ssl'], $focus->settings['mail_smtpssl']));
			
			//Assign the current users email for the test send dialogue.
			$this->ss->assign("CURRENT_USER_EMAIL", $current_user->email1);
			
			$showSendMail = FALSE;
			$outboundSendTypeCSSClass = "yui-hidden";
			if (isset($sugar_config['allow_sendmail_outbound']) && $sugar_config['allow_sendmail_outbound']) {
				$showSendMail = TRUE;
				$app_list_strings['notifymail_sendtype']['sendmail'] = 'sendmail';
				$outboundSendTypeCSSClass = "";
			}
			
			$this->ss->assign("OUTBOUND_TYPE_CLASS", $outboundSendTypeCSSClass);
			$this->ss->assign("mail_sendtype_options", get_select_options_with_id($app_list_strings['notifymail_sendtype'], $focus->settings['mail_sendtype']));
			
			///////////////////////////////////////////////////////////////////////////////
			////	USER EMAIL DEFAULTS
			// editors
			$editors = $app_list_strings['dom_email_editor_option'];
			$newEditors = array();
			foreach ($editors as $k => $v) {
				if ($k != "") {
					$newEditors[$k] = $v;
				}
			}
			
			// preserve attachments
			$preserveAttachments = '';
			if (isset($sugar_config['email_default_delete_attachments']) && $sugar_config['email_default_delete_attachments'] == true) {
				$preserveAttachments = 'CHECKED';
			}
			$this->ss->assign('DEFAULT_EMAIL_DELETE_ATTACHMENTS', $preserveAttachments);
			////	END USER EMAIL DEFAULTS
			///////////////////////////////////////////////////////////////////////////////
			
			
			//setting to manage.
			//emails_per_run
			//tracking_entities_location_type default or custom
			//tracking_entities_location http://www.sugarcrm.com/track/
			
			//////////////////////////////////////////////////////////////////////////////
			////	EMAIL SECURITY
			if (!isset($sugar_config['email_xss']) || empty($sugar_config['email_xss'])) {
				$sugar_config['email_xss'] = getDefaultXssTags();
			}
			
			foreach (unserialize(base64_decode($sugar_config['email_xss'])) as $k => $v) {
				$this->ss->assign($k . "Checked", 'CHECKED');
			}
			
			////	END EMAIL SECURITY
			///////////////////////////////////////////////////////////////////////////////
			
			require_once ('modules/Emails/Email.php');
			$email = new Email();
			
			//TODO (MT EXTEND Code): retrieve and use the TENANT_ID
			$this->ss->assign('ROLLOVER', $email->rolloverStyle);
			$this->ss->assign('THEME', $GLOBALS['theme']);
			
			$this->ss->assign("JAVASCRIPT", get_validate_record_js());
			$this->ss->display('modules/EmailMan/tpls/config.tpl');
		}
	}
	
	
	//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	
	require_once ('include/MVC/View/views/view.list.php');
	
	class EmailManViewList extends ViewList
	{
		/**
	 * @see SugarView::preDisplay()
	 */
		public function preDisplay()
		{
			global $current_user;
			
			if (!is_admin($current_user) && !is_admin_for_module($current_user, 'Campaigns')) 
				sugar_die($GLOBALS['app_strings']['ERR_NOT_ADMIN']);
			
			$this->lv = new ListViewSmarty();
			$this->lv->export = false;
			$this->lv->quickViewLinks = false;
		}
		
		/**
	 * @see SugarView::_getModuleTitleParams()
	 */
		protected function _getModuleTitleParams($browserTitle = false)
		{
			//TODO (MT): specify type of proc. parameters
			global $mod_strings;
			
			return array(
				"<a href='index.php?module=Administration&action=index'>" . translate('LBL_MODULE_NAME', 'Administration') . "</a>", translate('LBL_MASS_EMAIL_MANAGER_TITLE', 'Administration'));
		}
		
		
		function listViewPrepare()
		{
			$this->options['show_title'] = false;
			parent::listViewPrepare();
			echo $this->getModuleTitle(false);
		}
		/**
	 * @see ViewList::listViewProcess()
	 */
		function listViewProcess()
		{
			parent::listViewProcess();
			
			global $app_strings;
			
			echo 
				"<form action=\"index.php\" method=\"post\" name=\"EmailManDelivery\" id=\"form\">
			<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class='actionsContainer'>
				<tr><td style=\"padding-bottom: 2px;\">
                        <input type=\"hidden\" name=\"module\" value=\"EmailMan\">
                        <input type=\"hidden\" name=\"action\">
                        <input type=\"hidden\" name=\"return_module\">
                        <input type=\"hidden\" name=\"return_action\">
                        <input type=\"hidden\" name=\"manual\" value=\"true\">
                        <input	title=\"" . $app_strings['LBL_CAMPAIGNS_SEND_QUEUED'] . 
				"\" 
                                accessKey=\"" . $app_strings['LBL_SAVE_BUTTON_KEY'] . 
				"\" class=\"button\" 
                                onclick=\"this.form.return_module.value='EmailMan'; this.form.return_action.value='index'; this.form.action.value='EmailManDelivery'\" 
                                type=\"submit\" name=\"Send\" value=\"" . $app_strings['LBL_CAMPAIGNS_SEND_QUEUED'] . 
				"\">
            </td></tr></table></form>";
		}
	}
	
	
	//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
	
	
	
	
	
	$focus = new EmailMarketing();
	
	if (!isset($_REQUEST['record'])) {
		sugar_die("A record number must be specified to delete the marketing campaign.");
	}
	$focus->retrieve($_REQUEST['record']);
	if (!$focus->ACLAccess('Delete')) {
		ACLController::displayNoAccess(true);
		sugar_cleanup(true);
	}
	$focus->mark_deleted($_REQUEST['record']);
	
	if (isset($_REQUEST['record'])) {
		$query = "DELETE FROM emailman WHERE marketing_id ='" . $_REQUEST['record'] . "'";
		$focus->db->query($query);
	}
	
	header("Location: index.php?module=" . $_REQUEST['return_module'] . "&action=" . $_REQUEST['return_action'] . "&record=" . $_REQUEST['return_id']);
?>


//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
	
	
	
	require_once ('modules/EmailMarketing/Forms.php');
	
	global $app_strings;
	global $app_list_strings;
	global $mod_strings;
	global $current_user;
	
	// Unimplemented until jscalendar language files are fixed
	// global $current_language;
	// global $default_language;
	// global $cal_codes;
	
	$focus = new EmailMarketing();
	
	if (isset($_REQUEST['record'])) {
		$focus->retrieve($_REQUEST['record']);
	}
	
	global $theme;
	
	
	
	$GLOBALS['log']->info("EmailMarketing Edit View");
	
	$xtpl = new XTemplate('modules/EmailMarketing/DetailView.html');
	
	$xtpl->assign("MOD", $mod_strings);
	$xtpl->assign("APP", $app_strings);
	
	if (isset($_REQUEST['return_module'])) {
		$xtpl->assign("RETURN_MODULE", $_REQUEST['return_module']);
	} else {
		$xtpl->assign("RETURN_MODULE", 'Campaigns');
	}
	if (isset($_REQUEST['return_action'])) {
		$xtpl->assign("RETURN_ACTION", $_REQUEST['return_action']);
	} else {
		$xtpl->assign("RETURN_ACTION", 'DetailView');
	}
	if (isset($_REQUEST['return_id'])) {
		$xtpl->assign("RETURN_ID", $_REQUEST['return_id']);
	} else {
		if (!empty($focus->campaign_id)) {
			$xtpl->assign("RETURN_ID", $focus->campaign_id);
		}
	}
	
	
	if ($focus->campaign_id) {
		$campaign_id = $focus->campaign_id;
	} else {
		$campaign_id = $_REQUEST['campaign_id'];
	}
	$xtpl->assign("CAMPAIGN_ID", $campaign_id);
	
	
	$xtpl->assign("PRINT_URL", "index.php?" . $GLOBALS['request_string']);
	$xtpl->assign("JAVASCRIPT", get_set_focus_js());
	$xtpl->assign("DATE_ENTERED", $focus->date_entered);
	$xtpl->assign("DATE_MODIFIED", $focus->date_modified);
	$xtpl->assign("ID", $focus->id);
	$xtpl->assign("NAME", $focus->name);
	$xtpl->assign("FROM_NAME", $focus->from_name);
	$xtpl->assign("FROM_ADDR", $focus->from_addr);
	$xtpl->assign("REPLY_TO_NAME", $focus->reply_to_name);
	$xtpl->assign("REPLY_TO_ADDR", $focus->reply_to_addr);
	$xtpl->assign("DATE_START", $focus->date_start);
	$xtpl->assign("TIME_START", $focus->time_start);
	
	$email_templates_arr = get_bean_select_array(true, 'EmailTemplate', 'name');
	if ($focus->template_id) {
		$xtpl->assign("EMAIL_TEMPLATE", $email_templates_arr[$focus->template_id]);
	}
	
	//include campaign utils..
	require_once ('modules/Campaigns/utils.php');
	if (empty($_REQUEST['campaign_name'])) {
		
		$campaign = new Campaign();
		$campaign->retrieve($campaign_id);
		$campaign_name = $campaign->name;
	} else {
		$campaign_name = $_REQUEST['campaign_name'];
	}
	
	$params = array();
	$params[] = "<a href='index.php?module=Campaigns&action=index'>{$mod_strings['LNK_CAMPAIGN_LIST']}</a>";
	$params[] = "<a href='index.php?module=Campaigns&action=DetailView&record={$campaign_id}'>{$campaign_name}</a>";
	$params[] = $focus->name;
	
	echo getClassicModuleTitle($focus->module_dir, $params, true);
	
	if (!empty($focus->all_prospect_lists)) {
		$xtpl->assign("MESSAGE_FOR", $mod_strings['LBL_ALL_PROSPECT_LISTS']);
	} else {
		$xtpl->assign("MESSAGE_FOR", $mod_strings['LBL_RELATED_PROSPECT_LISTS']);
	}
	
	if (!empty($focus->status)) {
		$xtpl->assign("STATUS", $app_list_strings['email_marketing_status_dom'][$focus->status]);
	}
	$emails = array();
	$mailboxes = get_campaign_mailboxes($emails);
	//_ppd($emails[$focus->inbound_email_id]);
	if (!empty($focus->inbound_email_id) && isset($mailboxes[$focus->inbound_email_id])) {
		$xtpl->assign("FROM_MAILBOX_NAME", $mailboxes[$focus->inbound_email_id] . "&nbsp;&lt;{$emails[$focus->inbound_email_id]}&gt;");
	}
	
	$xtpl->parse("main");
	$xtpl->out("main");
	
	
	$javascript = new javascript();
	$javascript->setFormName('EditView');
	$javascript->setSugarBean($focus);
	$javascript->addAllFields('');
	echo $javascript->getScript();
	
	require_once ('include/SubPanel/SubPanelTiles.php');
	$subpanel = new SubPanelTiles($focus, 'EmailMarketing');
	
	if ($focus->all_prospect_lists == 1) {
		$subpanel->subpanel_definitions->exclude_tab('prospectlists');
	} else {
		$subpanel->subpanel_definitions->exclude_tab('allprospectlists');
		
	}
	
	echo $subpanel->display();
	
?>


//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	require_once ('modules/EmailMarketing/Forms.php');
	
	global $timedate;
	global $app_strings;
	global $app_list_strings;
	global $mod_strings;
	global $current_user;
	
	// Unimplemented until jscalendar language files are fixed
	// global $current_language;
	// global $default_language;
	// global $cal_codes;
	
	$focus = new EmailMarketing();
	if (isset($_REQUEST['record'])) {
		$focus->retrieve($_REQUEST['record']);
	}
	
	if (isset($_REQUEST['isDuplicate']) && $_REQUEST['isDuplicate'] == 'true') {
		$focus->id = "";
	}
	global $theme;
	
	
	
	$GLOBALS['log']->info("EmailMarketing Edit View");
	$xtpl = new XTemplate('modules/EmailMarketing/EditView.html');
	if (!ACLController::checkAccess('EmailTemplates', 'edit', true)) {
		unset($mod_strings['LBL_CREATE_EMAIL_TEMPLATE']);
		unset($mod_strings['LBL_EDIT_EMAIL_TEMPLATE']);
	}
	$xtpl->assign("MOD", $mod_strings);
	$xtpl->assign("APP", $app_strings);
	$xtpl->assign("THEME", SugarThemeRegistry::current()->__toString());
	// Unimplemented until jscalendar language files are fixed
	// $xtpl->assign("CALENDAR_LANG", ((empty($cal_codes[$current_language])) ? $cal_codes[$default_language] : $cal_codes[$current_language]));
	$xtpl->assign("CALENDAR_LANG", "en");
	$xtpl->assign("USER_DATEFORMAT", '(' . $timedate->get_user_date_format() . ')');
	$xtpl->assign("CALENDAR_DATEFORMAT", $timedate->get_cal_date_format());
	$time_ampm = $timedate->AMPMMenu('', $focus->time_start);
	$xtpl->assign("TIME_MERIDIEM", $time_ampm);
	
	if (isset($_REQUEST['return_module'])) {
		$xtpl->assign("RETURN_MODULE", $_REQUEST['return_module']);
	} else {
		$xtpl->assign("RETURN_MODULE", 'Campaigns');
	}
	if (isset($_REQUEST['return_action'])) {
		$xtpl->assign("RETURN_ACTION", $_REQUEST['return_action']);
	} else {
		$xtpl->assign("RETURN_ACTION", 'DetailView');
	}
	if (isset($_REQUEST['return_id'])) {
		$xtpl->assign("RETURN_ID", $_REQUEST['return_id']);
	} else {
		if (!empty($focus->campaign_id)) {
			$xtpl->assign("RETURN_ID", $focus->campaign_id);
		}
	}
	
	if ($focus->campaign_id) {
		$campaign_id = $focus->campaign_id;
	} else {
		$campaign_id = $_REQUEST['campaign_id'];
	}
	$xtpl->assign("CAMPAIGN_ID", $campaign_id);
	
	if (empty($time_ampm) || empty($focus->time_start)) {
		$time_start = $focus->time_start;
	} else {
		$split = $timedate->splitTime($focus->time_start, $timedate->get_time_format());
		$time_start = $split['h'] . $timedate->timeSeparator() . $split['m'];
	}
	$xtpl->assign("PRINT_URL", "index.php?" . $GLOBALS['request_string']);
	$xtpl->assign("JAVASCRIPT", get_set_focus_js() . get_validate_record_js());
	$xtpl->assign("DATE_ENTERED", $focus->date_entered);
	$xtpl->assign("DATE_MODIFIED", $focus->date_modified);
	$xtpl->assign("ID", $focus->id);
	$xtpl->assign("NAME", $focus->name);
	$xtpl->assign("FROM_NAME", $focus->from_name);
	$xtpl->assign("FROM_ADDR", $focus->from_addr);
	$xtpl->assign("REPLY_NAME", $focus->reply_to_name);
	$xtpl->assign("REPLY_ADDR", $focus->reply_to_addr);
	$xtpl->assign("DATE_START", $focus->date_start);
	$xtpl->assign("TIME_START", $time_start);
	$xtpl->assign("TIME_FORMAT", '(' . $timedate->get_user_time_format() . ')');
	
	$email_templates_arr = get_bean_select_array(true, 'EmailTemplate', 'name', "(type IS NULL OR type='' OR type='campaign')", 'name');
	if ($focus->template_id) {
		$xtpl->assign("TEMPLATE_ID", $focus->template_id);
		$xtpl->assign("EMAIL_TEMPLATE_OPTIONS", get_select_options_with_id($email_templates_arr, $focus->template_id));
		$xtpl->assign("EDIT_TEMPLATE", "visibility:inline");
	} else {
		$xtpl->assign("EMAIL_TEMPLATE_OPTIONS", get_select_options_with_id($email_templates_arr, ""));
		$xtpl->assign("EDIT_TEMPLATE", "visibility:hidden");
	}
	
	//include campaign utils..
	require_once ('modules/Campaigns/utils.php');
	if (empty($_REQUEST['campaign_name'])) {
		
		$campaign = new Campaign();
		$campaign->retrieve($campaign_id);
		$campaign_name = $campaign->name;
	} else {
		$campaign_name = $_REQUEST['campaign_name'];
	}
	
	$params = array();
	$params[] = "<a href='index.php?module=Campaigns&action=index'>{$mod_strings['LNK_CAMPAIGN_LIST']}</a>";
	$params[] = "<a href='index.php?module=Campaigns&action=DetailView&record={$campaign_id}'>{$campaign_name}</a>";
	if (empty($focus->id)) {
		$params[] = $GLOBALS['app_strings']['LBL_CREATE_BUTTON_LABEL'] . " " . $mod_strings['LBL_MODULE_NAME'];
	} else {
		$params[] = "<a href='index.php?module={$focus->module_dir}&action=DetailView&record={$focus->id}'>{$focus->name}</a>";
		$params[] = $GLOBALS['app_strings']['LBL_EDIT_BUTTON_LABEL'];
	}
	
	echo getClassicModuleTitle($focus->module_dir, $params, true);
	$scope_options = get_message_scope_dom($campaign_id, $campaign_name, $focus->db);
	$prospectlists = array();
	if (isset($focus->all_prospect_lists) && $focus->all_prospect_lists == 1) {
		$xtpl->assign("ALL_PROSPECT_LISTS_CHECKED", "checked");
		$xtpl->assign("MESSAGE_FOR_DISABLED", "disabled");
	} else {
		//get select prospect list.
		if (!empty($focus->id)) {
			$focus->load_relationship('prospectlists');
			$prospectlists = $focus->prospectlists->get();
		}
		;
	}
	if (empty($prospectlists)) 
		$prospectlists = array();
	if (empty($scope_options)) 
		$scope_options = array();
	$xtpl->assign("SCOPE_OPTIONS", get_select_options_with_id($scope_options, $prospectlists));
	
	$emails = array();
	$mailboxes = get_campaign_mailboxes($emails);
	$mailboxes_with_from_name = get_campaign_mailboxes($emails, false);
	
	//add empty options.
	$emails[''] = 'nobody@example.com';
	$mailboxes[''] = '';
	
	//inbound_email_id
	$default_email_address = 'nobody@example.com';
	$from_emails = '';
	foreach ($mailboxes_with_from_name as $id => $name) {
		if (!empty($from_emails)) {
			$from_emails .= ',';
		}
		if ($id == '') {
			$from_emails .= "'EMPTY','$name','$emails[$id]'";
		} else {
			$from_emails .= "'$id','$name','$emails[$id]'";
		}
		if ($id == $focus->inbound_email_id) {
			$default_email_address = $emails[$id];
		}
	}
	$xtpl->assign("FROM_EMAILS", $from_emails);
	$xtpl->assign("DEFAULT_FROM_EMAIL", $default_email_address);
	
	if (empty($focus->inbound_email_id)) {
		$xtpl->assign("MAILBOXES", get_select_options_with_id($mailboxes, ''));
	} else {
		$xtpl->assign("MAILBOXES", get_select_options_with_id($mailboxes, $focus->inbound_email_id));
	}
	
	$xtpl->assign("STATUS_OPTIONS", get_select_options_with_id($app_list_strings['email_marketing_status_dom'], $focus->status));
	
	//pass in info to populate from/reply address info
	require_once ('modules/Campaigns/utils.php');
	$json = getJSONobj();
	$IEStoredOptions = get_campaign_mailboxes_with_stored_options();
	$IEStoredOptionsJSON = (!empty($IEStoredOptions)) ? $json->encode($IEStoredOptions, false) : 'new Object()';
	$xtpl->assign("IEStoredOptions", $IEStoredOptionsJSON);
	
	
	$xtpl->parse("main");
	$xtpl->out("main");
	
	
	
	$javascript = new javascript();
	$javascript->setFormName('EditView');
	$javascript->setSugarBean($focus);
	$javascript->addAllFields('');
	echo $javascript->getScript();
?>



//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description:  Contains field arrays that are used for caching
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
	$fields_array['EmailMarketing'] = array('column_fields' => array(
		'id', 'date_entered', 'date_modified', 
		'modified_user_id', 'created_by', 'name', 
		'from_addr', 'from_name', 'reply_to_name', 'reply_to_addr', 'date_start', 'time_start', 'template_id', 'campaign_id', 'status', 'inbound_email_id', 'all_prospect_lists'), 
		'list_fields' => array(
		'id', 'name', 'date_start', 'time_start', 'template_id', 'status', 'all_prospect_lists', 'campaign_id'), 
		'required_fields' => array(
		'name' => 1, 'from_name' => 1, 'from_addr' => 1, 'date_start' => 1, 'time_start' => 1, 
		'template_id' => 1, 'status' => 1));
?>



//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	function get_validate_record_js()
	{
		global $mod_strings;
		global $app_strings;
		
		$err_missing_required_fields = $app_strings['ERR_MISSING_REQUIRED_FIELDS'];
		$err_lbl_send_message = $mod_strings['LBL_MESSAGE_FOR'];
		$the_script = <<<EOQ

<script type="text/javascript" language="Javascript">
function verify_data(form,formname) {
	if (!check_form(formname))
		return false;

	var isError = false;
	var errorMessage = "";
		
	var thecheckbox=document.getElementById('all_prospect_lists');
	var theselectbox=document.getElementById('message_for');		

	if (!thecheckbox.checked && theselectbox.selectedIndex < 0)  {
		isError=true;
		errorMessage="$err_lbl_send_message";
	}
			
	if (isError == true) {
		alert("$err_missing_required_fields" + errorMessage);
		return false;
	}
	return true;
}
</script>

EOQ;
		
		return $the_script;
		
	}
	
	/**
 * Create HTML form to enter a new record with the minimum necessary fields.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 */
	
?>



//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description:  TODO To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
	
	global $mod_strings, $app_strings;
	
	
	$module_menu = array(array("index.php?module=Campaigns&action=EditView&return_module=Campaigns&return_action=index", $mod_strings['LNK_NEW_CAMPAIGN'], "CreateCampaigns"), array("index.php?module=Campaigns&action=index&return_module=Campaigns&return_action=index", $mod_strings['LNK_CAMPAIGN_LIST'], "Campaigns"), array("index.php?module=ProspectLists&action=EditView&return_module=ProspectLists&return_action=DetailView", $mod_strings['LNK_NEW_PROSPECT_LIST'], "CreateProspectLists"), array("index.php?module=ProspectLists&action=index&return_module=ProspectLists&return_action=index", $mod_strings['LNK_PROSPECT_LIST_LIST'], "ProspectLists"), array("index.php?module=Prospects&action=EditView&return_module=Prospects&return_action=DetailView", $mod_strings['LNK_NEW_PROSPECT'], "CreateProspects"), array("index.php?module=Prospects&action=index&return_module=Prospects&return_action=index", $mod_strings['LNK_PROSPECT_LIST'], "Prospects"));
?>


//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
	
	
	
	
	
	
	global $timedate;
	global $current_user;
	if (!empty($_POST['meridiem'])) {
		$_POST['time_start'] = $timedate->merge_time_meridiem($_POST['time_start'], $timedate->get_time_format(), $_POST['meridiem']);
	}
	
	if (empty($_REQUEST['time_start'])) {
		$_REQUEST['date_start'] = $_REQUEST['date_start'] . ' 00:00';
		$_POST['date_start'] = $_POST['date_start'] . ' 00:00';
	} else {
		$_REQUEST['date_start'] = $_REQUEST['date_start'] . ' ' . $_REQUEST['time_start'];
		$_POST['date_start'] = $_POST['date_start'] . ' ' . $_POST['time_start'];
	}
	
	$marketing = new EmailMarketing();
	if (isset($_POST['record']) && !empty($_POST['record'])) {
		$marketing->retrieve($_POST['record']);
	}
	if (!$marketing->ACLAccess('Save')) {
		ACLController::displayNoAccess(true);
		sugar_cleanup(true);
	}
	
	if (!empty($_POST['assigned_user_id']) && ($marketing->assigned_user_id != $_POST['assigned_user_id']) && ($_POST['assigned_user_id'] != $current_user->id)) {
		$check_notify = TRUE;
	} else {
		$check_notify = FALSE;
	}
	foreach ($marketing->column_fields as $field) {
		if ($field == 'all_prospect_lists') {
			if (isset($_POST[$field]) && $_POST[$field] = 'on') {
				$marketing->$field = 1;
			} else {
				$marketing->$field = 0;
			}
		} else {
			if (isset($_POST[$field])) {
				$value = $_POST[$field];
				$marketing->$field = $value;
			}
		}
	}
	
	foreach ($marketing->additional_column_fields as $field) {
		if (isset($_POST[$field])) {
			$value = $_POST[$field];
			$marketing->$field = $value;
			
		}
	}
	
	$marketing->campaign_id = $_REQUEST['campaign_id'];
	$marketing->save($check_notify);
	
	//add prospect lists to campaign.
	$marketing->load_relationship('prospectlists');
	$prospectlists = $marketing->prospectlists->get();
	if ($marketing->all_prospect_lists == 1) {
		//remove all related prospect lists.
		if (!empty($prospectlists)) {
			$marketing->prospectlists->delete($marketing->id);
		}
	} else {
		if (is_array($_REQUEST['message_for'])) {
			foreach ($_REQUEST['message_for'] as $prospect_list_id) {
				
				$key = array_search($prospect_list_id, $prospectlists);
				if ($key === null or $key === false) {
					$marketing->prospectlists->add($prospect_list_id);
				} else {
					unset($prospectlists[$key]);
				}
			}
			if (count($prospectlists) != 0) {
				foreach ($prospectlists as $key => $list_id) {
					$marketing->prospectlists->delete($marketing->id, $list_id);
				}
			}
		}
	}
	if ($_REQUEST['action'] != 'WizardMarketingSave') {
		$header_URL = "Location: index.php?action=DetailView&module=Campaigns&record={$_REQUEST['campaign_id']}";
		$GLOBALS['log']->debug("about to post header URL of: $header_URL");
		header($header_URL);
	}
?>


//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
	
	
	
	
	
	global $app_strings;
	//we don't want the parent module's string file, but rather the string file specifc to this subpanel
	global $current_language;
	$current_module_strings = return_module_language($current_language, 'EmailMarketing');
	
	global $currentModule;
	
	global $theme;
	global $focus;
	global $action;
	
	
	
	
	// focus_list is the means of passing data to a SubPanelView.
	global $focus_list;
	
	$button = "<form action='index.php' method='post' name='MKForm' id='MKForm'>\n";
	$button .= "<input type='hidden' name='module' value='EmailMarketing'>\n";
	$button .= "<input type='hidden' name='campaign_id' value='$focus->id'>\n";
	$button .= "<input type='hidden' name='return_module' value='" . $currentModule . "'>\n";
	$button .= "<input type='hidden' name='return_action' value='DetailView'>\n";
	$button .= "<input type='hidden' name='return_id' value='" . $focus->id . "'>\n";
	$button .= "<input type='hidden' name='action'>\n";
	
	$button .= "<input title='" . $app_strings['LBL_NEW_BUTTON_TITLE'] . "' accessyKey='" . $app_strings['LBL_NEW_BUTTON_KEY'] . "' class='button' onclick=\"this.form.action.value='EditView'\" type='submit' name='New' value='  " . $app_strings['LBL_NEW_BUTTON_LABEL'] . "  '>\n";
	
	$button .= "</form>\n";
	
	$ListView = new ListView();
	$ListView->initNewXTemplate('modules/EmailMarketing/SubPanelView.html', $current_module_strings);
	
	$ListView->xTemplateAssign("EDIT_INLINE_PNG", SugarThemeRegistry::current()->getImage('edit_inline', 'align="absmiddle" border="0"', null, null, '.gif', $app_strings['LNK_EDIT']));
	$ListView->xTemplateAssign("REMOVE_INLINE_PNG", SugarThemeRegistry::current()->getImage('delete_inline', 'align="absmiddle" border="0"', null, null, '.gif', $app_strings['LNK_REMOVE']));
	
	$ListView->xTemplateAssign("RETURN_URL", "&return_module=" . $currentModule . "&return_action=DetailView&return_id=" . $focus->id);
	$ListView->setHeaderTitle($current_module_strings['LBL_MODULE_NAME']);
	$ListView->setHeaderText($button);
	$ListView->processListView($focus_list, "main", "EMAILMARKETING");
	
?>


//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	
	global $current_user;
	
	if (isset($_REQUEST['type']) && $_REQUEST['type'] == 'personal') {
		if ($current_user->hasPersonalEmail()) {
			
			$ie = new InboundEmail();
			$beans = $ie->retrieveByGroupId($current_user->id);
			if (!empty($beans)) {
				/** @var InboundEmail $bean */
				foreach ($beans as $bean) {
					$bean->importMessages();
				}
			}
		}
		header('Location: index.php?module=Emails&action=ListView&type=inbound&assigned_user_id=' . $current_user->id);
	} elseif (isset($_REQUEST['type']) && $_REQUEST['type'] == 'group') {
		$ie = new InboundEmail();
		// this query only polls Group Inboxes
		$r = $ie->db->query('SELECT inbound_email.id FROM inbound_email JOIN users ON inbound_email.group_id = users.id WHERE inbound_email.deleted=0 AND inbound_email.status = \'Active\' AND mailbox_type != \'bounce\' AND users.deleted = 0 AND users.is_group = 1');
		
		while ($a = $ie->db->fetchByAssoc($r)) {
			$ieX = new InboundEmail();
			$ieX->retrieve($a['id']);
			$ieX->importMessages();
		}
		
		header('Location: index.php?module=Emails&action=ListViewGroup');
	} else {
		// fail gracefully
		header('Location: index.php?module=Emails&action=index');
	}
	
	
	//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description: TODO:  To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
	
	//Shorten name.
	$data = $_REQUEST;
	
	if (!empty($data['listViewExternalClient'])) {
		$email = new Email();
		echo $email->getNamePlusEmailAddressesForCompose($_REQUEST['action_module'], (explode(",", $_REQUEST['uid'])));
	} else 
		//For the full compose/email screen, the compose package is generated and script execution
		//continues to the Emails/index.php page.
		if (!isset($data['forQuickCreate'])) {
			$ret = generateComposeDataPackage($data);
		}
	
	/**
 * Initialize the full compose window by creating the compose package
 * and then including Emails index.php file.
 *
 * @param Array $ret
 */
	function initFullCompose($ret)
	{
		//TODO (MT): specify type of proc. parameters
		global $current_user;
		$json = getJSONobj();
		$composeOut = $json->encode($ret);
		
		//For listview 'Email' call initiated by subpanels, just returned the composePackage data, do not
		//include the entire Emails page
		if (isset($_REQUEST['ajaxCall']) && $_REQUEST['ajaxCall']) {
			echo $composeOut;
		} else {
			//For normal full compose screen
			include ('modules/Emails/index.php');
			echo "<script type='text/javascript' language='javascript'>\ncomposePackage = {$composeOut};\n</script>";
		}
	}
	
	/**
 * Generate the compose data package consumed by the full and quick compose screens.
 *
 * @param Array $data
 * @param Bool $forFullCompose If full compose is set to TRUE, then continue execution and include the full Emails UI.  Otherwise
 *             the data generated is returned.
 * @param SugarBean $bean Optional - parent object with data
 */
	function generateComposeDataPackage($data, $forFullCompose = TRUE, $bean = null)
	{
		// we will need the following:
		//TODO (MT): specify type of proc. parameters
		if (isset($data['parent_type']) && !empty($data['parent_type']) && isset($data['parent_id']) && !empty($data['parent_id']) && 
			!isset($data['ListView']) && !isset($data['replyForward'])) {
			if (empty($bean)) {
				global $beanList;
				global $beanFiles;
				global $mod_strings;
				
				$parentName = '';
				$class = $beanList[$data['parent_type']];
				require_once ($beanFiles[$class]);
				
				$bean = new $class();
				$bean->retrieve($data['parent_id']);
			}
			if (isset($bean->full_name)) {
				$parentName = $bean->full_name;
			} elseif (isset($bean->name)) {
				$parentName = $bean->name;
			} else {
				$parentName = '';
			}
			$parentName = from_html($parentName);
			$namePlusEmail = '';
			if (isset($data['to_email_addrs'])) {
				$namePlusEmail = $data['to_email_addrs'];
				$namePlusEmail = from_html(str_replace("&nbsp;", " ", $namePlusEmail));
			} else {
				if (isset($bean->full_name)) {
					$namePlusEmail = from_html($bean->full_name) . " <" . from_html($bean->emailAddress->getPrimaryAddress($bean)) . ">";
				} else 
					if (isset($bean->emailAddress)) {
						$namePlusEmail = "<" . from_html($bean->emailAddress->getPrimaryAddress($bean)) . ">";
					}
			}
			
			$subject = "";
			$body = "";
			$email_id = "";
			$attachments = array();
			if ($bean->module_dir == 'Cases') {
				$subject = str_replace('%1', $bean->case_number, $bean->getEmailSubjectMacro() . " " . from_html($bean->name)); //bug 41928
				$bean->load_relationship("contacts");
				$contact_ids = $bean->contacts->get();
				$contact = new Contact();
				foreach ($contact_ids as $cid) {
					$contact->retrieve($cid);
					$namePlusEmail .= empty($namePlusEmail) ? "" : ", ";
					$namePlusEmail .= from_html($contact->full_name) . " <" . from_html($contact->emailAddress->getPrimaryAddress($contact)) . ">";
				}
			}
			if ($bean->module_dir == 'KBDocuments') {
				
				require_once ("modules/Emails/EmailUI.php");
				$subject = $bean->kbdocument_name;
				$article_body = str_replace('/cache/images/', $GLOBALS['sugar_config']['site_url'] . '/cache/images/', KBDocument::get_kbdoc_body_without_incrementing_count($bean->id));
				$body = from_html($article_body);
				$attachments = KBDocument::get_kbdoc_attachments_for_newemail($bean->id);
				$attachments = $attachments['attachments'];
			}
			// if
			if ($bean->module_dir == 'Quotes' && isset($data['recordId'])) {
				$quotesData = getQuotesRelatedData($bean, $data);
				global $current_language;
				$namePlusEmail = $quotesData['toAddress'];
				$subject = $quotesData['subject'];
				$body = $quotesData['body'];
				$attachments = $quotesData['attachments'];
				$email_id = $quotesData['email_id'];
			}
			// if
			
			$ret = array(
				'to_email_addrs' => $namePlusEmail, 
				'parent_type' => $data['parent_type'], 
				'parent_id' => $data['parent_id'], 
				'parent_name' => $parentName, 
				'subject' => $subject, 
				'body' => $body, 
				'attachments' => $attachments, 
				'email_id' => $email_id);
		} else 
			if (isset($_REQUEST['ListView'])) {
				
				$email = new Email();

				//TODO (MT EXTEND Code): retrieve and use the TENANT_ID
				$namePlusEmail = $email->getNamePlusEmailAddressesForCompose($_REQUEST['action_module'], (explode(",", $_REQUEST['uid'])));
				$ret = array(
					'to_email_addrs' => $namePlusEmail);
			} else 
				if (isset($data['replyForward'])) {
					
					require_once ("modules/Emails/EmailUI.php");
					
					$ret = array();
					$ie = new InboundEmail();
					$ie->email = new Email();
					
					//TODO (MT EXTEND Code): retrieve and use the TENANT_ID
					$ie->email->email2init();
					$replyType = $data['reply'];
					$email_id = $data['record'];
					$ie->email->retrieve($email_id);
					$emailType = "";
					if ($ie->email->type == 'draft') {
						$emailType = $ie->email->type;
					}
					$ie->email->from_addr = $ie->email->from_addr_name;
					$ie->email->to_addrs = to_html($ie->email->to_addrs_names);
					$ie->email->cc_addrs = to_html($ie->email->cc_addrs_names);
					$ie->email->bcc_addrs = $ie->email->bcc_addrs_names;
					$ie->email->from_name = $ie->email->from_addr;
					$preBodyHTML = "&nbsp;<div><hr></div>";
					if ($ie->email->type != 'draft') {
						$email = $ie->email->et->handleReplyType($ie->email, $replyType);
					} else {
						$email = $ie->email;
						$preBodyHTML = "";
					}
					// else
					if ($ie->email->type != 'draft') {
						$emailHeader = $email->description;
					}
					$ret = $ie->email->et->displayComposeEmail($email);
					if ($ie->email->type != 'draft') {
						$ret['description'] = $emailHeader;
					}
					if ($replyType == 'forward' || $emailType == 'draft') {
						$ret = $ie->email->et->getDraftAttachments($ret);
					}
					$return = $ie->email->et->getFromAllAccountsArray($ie, $ret);
					
					if ($replyType == "forward") {
						$return['to'] = '';
					} else {
						if ($email->type != 'draft') {
							$return['to'] = from_html($ie->email->from_addr);
						}
					}
					// else
					$ret = array(
						'to_email_addrs' => $return['to'], 
						'parent_type' => $return['parent_type'], 
						'parent_id' => $return['parent_id'], 
						'parent_name' => $return['parent_name'], 
						'subject' => $return['name'], 
						'body' => $preBodyHTML . $return['description'], 
						'attachments' => (isset($return['attachments']) ? $return['attachments'] : array()), 
						'email_id' => $email_id, 
						'fromAccounts' => $return['fromAccounts']);
					
					// If it's a 'Reply All' action, append the CC addresses
					if ($data['reply'] == 'replyAll') {
						global $current_user;
						
						$ccEmails = $ie->email->to_addrs;
						
						if (!empty($ie->email->cc_addrs)) {
							$ccEmails .= ", " . $ie->email->cc_addrs;
						}
						
						$myEmailAddresses = array();
						foreach ($current_user->emailAddress->addresses as $p) {
							array_push($myEmailAddresses, $p['email_address']);
						}
						
						//remove current user's email address (if contained in To/CC)
						$ccEmailsArr = explode(", ", $ccEmails);
						
						foreach ($ccEmailsArr as $p => $q) {
							preg_match('/<(.*?)>/', $q, $email);
							if (isset($email[1])) {
								$checkemail = $email[1];
							} else {
								$checkemail = $q;
							}
							if (in_array($checkemail, $myEmailAddresses)) {
								unset($ccEmailsArr[$p]);
							}
						}
						
						$ccEmails = implode(", ", $ccEmailsArr);
						
						$ret['cc_addrs'] = from_html($ccEmails);
					}
					
				} else {
					$ret = array(
						'to_email_addrs' => '');
				}
		
		if ($forFullCompose) 
			initFullCompose($ret);
		 else 
			return $ret;
	}
	
	function getQuotesRelatedData($bean, $data)
	{
		$return = array();
		$emailId = $data['recordId'];
		
		require_once ("modules/Emails/EmailUI.php");
		$email = new Email();
		$email->retrieve($emailId);
		$return['subject'] = $email->name;
		$return['body'] = from_html($email->description_html);
		$return['toAddress'] = $email->to_addrs;
		$ret = array();
		$ret['uid'] = $emailId;
		$ret = EmailUI::getDraftAttachments($ret);
		$return['attachments'] = $ret['attachments'];
		$return['email_id'] = $emailId;
		return $return;
	}
	// fn
	
	
	//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
	
	
	$focus = new Email();
	
	if (!isset($_REQUEST['record'])) 
		sugar_die("A record number must be specified to delete the email.");
	$focus->retrieve($_REQUEST['record']);
	$email_type = $focus->type;
	if (!$focus->ACLAccess('Delete')) {
		ACLController::displayNoAccess(true);
		sugar_cleanup(true);
	}
	$focus->mark_deleted($_REQUEST['record']);
	
	// make sure assigned_user_id is set - during testing this isn't always set
	if (!isset($_REQUEST['assigned_user_id'])) {
		$_REQUEST['assigned_user_id'] = '';
	}
	
	if ($email_type == 'archived') {
		global $current_user;
		$loc = 'Location: index.php?module=Emails';
	} else {
		$loc = 'Location: index.php?module=' . $_REQUEST['return_module'] . '&action=' . $_REQUEST['return_action'] . '&record=' . $_REQUEST['return_id'] . '&type=' . $_REQUEST['type'] . '&assigned_user_id=' . $_REQUEST['assigned_user_id'];
	}
	
	header($loc);
?>



//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
	
	///////////////////////////////////////////////////////////////////////////////
	////	CANCEL HANDLING
	if (!isset($_REQUEST['record']) || empty($_REQUEST['record'])) {
		header("Location: index.php?module=Emails&action=index");
	}
	////	CANCEL HANDLING
	///////////////////////////////////////////////////////////////////////////////
	
	
	require_once ('include/DetailView/DetailView.php');
	global $gridline;
	global $app_strings;
	global $focus;
	
	// SETTING DEFAULTS
	$focus = new Email();
	$detailView = new DetailView();
	$offset = 0;
	$email_type = 'archived';
	
	///////////////////////////////////////////////////////////////////////////////
	////	TO HANDLE 'NEXT FREE'
	if (!empty($_REQUEST['next_free']) && $_REQUEST['next_free'] == true) {
		$_REQUEST['record'] = $focus->getNextFree();
	}
	////	END 'NEXT FREE'
	///////////////////////////////////////////////////////////////////////////////
	
	if (isset($_REQUEST['offset']) or isset($_REQUEST['record'])) {
		$result = $detailView->processSugarBean("EMAIL", $focus, $offset);
		if ($result == null) {
			sugar_die($app_strings['ERROR_NO_RECORD']);
		}
		$focus = $result;
	} else {
		header("Location: index.php?module=Emails&action=index");
		die();
	}
	
	/* if the Email status is draft, say as a saved draft to a Lead/Case/etc.,
 * don't show detail view. go directly to EditView */
	if ($focus->status == 'draft') {
		//header('Location: index.php?module=Emails&action=EditView&record='.$_REQUEST['record']);
		//die();
	}
	
	// ACL Access Check
	if (!$focus->ACLAccess('DetailView')) {
		ACLController::displayNoAccess(true);
		sugar_cleanup(true);
	}
	
	//needed when creating a new email with default values passed in
	if (isset($_REQUEST['contact_name']) && is_null($focus->contact_name)) {
		$focus->contact_name = $_REQUEST['contact_name'];
	}
	if (isset($_REQUEST['contact_id']) && is_null($focus->contact_id)) {
		$focus->contact_id = $_REQUEST['contact_id'];
	}
	if (isset($_REQUEST['opportunity_name']) && is_null($focus->parent_name)) {
		$focus->parent_name = $_REQUEST['opportunity_name'];
	}
	if (isset($_REQUEST['opportunity_id']) && is_null($focus->parent_id)) {
		$focus->parent_id = $_REQUEST['opportunity_id'];
	}
	if (isset($_REQUEST['account_name']) && is_null($focus->parent_name)) {
		$focus->parent_name = $_REQUEST['account_name'];
	}
	if (isset($_REQUEST['account_id']) && is_null($focus->parent_id)) {
		$focus->parent_id = $_REQUEST['account_id'];
	}
	
	// un/READ flags
	if (!empty($focus->status)) {
		// "Read" flag for InboundEmail
		if ($focus->status == 'unread') {
			// creating a new instance here to avoid data corruption below
			$e = new Email();
			$e->retrieve($focus->id);
			$e->status = 'read';
			$e->save();
			$email_type = $e->status;
		} else {
			$email_type = $focus->status;
		}
		
	} elseif (!empty($_REQUEST['type'])) {
		$email_type = $_REQUEST['type'];
	}
	
	
	///////////////////////////////////////////////////////////////////////////////
	////	OUTPUT
	///////////////////////////////////////////////////////////////////////////////
	echo "\n<p>\n";
	$GLOBALS['log']->info("Email detail view");
	$show_forward = true;
	if ($email_type == 'archived') {
		echo getClassicModuleTitle('Emails', array($mod_strings['LBL_ARCHIVED_EMAIL'], $focus->name), true);
		$xtpl = new XTemplate('modules/Emails/DetailView.html');
	} else {
		$xtpl = new XTemplate('modules/Emails/DetailViewSent.html');
		if ($focus->type == 'out') {
			echo getClassicModuleTitle('Emails', array($mod_strings['LBL_SENT_MODULE_NAME'], $focus->name), true);
			//$xtpl->assign('DISABLE_REPLY_BUTTON', 'NONE');
		} elseif ($focus->type == 'draft') {
			$xtpl->assign('DISABLE_FORWARD_BUTTON', 'NONE');
			$show_forward = false;
			echo getClassicModuleTitle('Emails', array($mod_strings['LBL_LIST_FORM_DRAFTS_TITLE'], $focus->name), true);
		} elseif ($focus->type == 'inbound') {
			echo getClassicModuleTitle('Emails', array($mod_strings['LBL_INBOUND_TITLE'], $focus->name), true);
		}
	}
	echo "\n</p>\n";
	
	
	
	///////////////////////////////////////////////////////////////////////////////
	////	RETURN NAVIGATION
	$uri = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : 'index.php';
	$start = $focus->getStartPage($uri);
	$ret_mod = '';
	$ret_action = '';
	if (isset($_REQUEST['return_id'])) {
		// coming from a subpanel, return_module|action is not set
		$xtpl->assign('RETURN_ID', $_REQUEST['return_id']);
		if (isset($_REQUEST['return_module'])) {
			$xtpl->assign('RETURN_MODULE', $_REQUEST['return_module']);
			$ret_mod = $_REQUEST['return_module'];
		} else {
			$xtpl->assign('RETURN_MODULE', 'Emails');
			$ret_mod = 'Emails';
		}
		if (isset($_REQUEST['return_action'])) {
			$xtpl->assign('RETURN_ACTION', $_REQUEST['return_action']);
			$ret_action = $_REQUEST['return_action'];
		} else {
			$xtpl->assign('RETURN_ACTION', 'DetailView');
			$ret_action = 'DetailView';
		}
	}
	
	if (isset($start['action']) && !empty($start['action'])) {
		$xtpl->assign('DELETE_RETURN_ACTION', $start['action']);
	} else {
		$start['action'] = '';
	}
	if (isset($start['module']) && !empty($start['module'])) {
		$xtpl->assign('DELETE_RETURN_MODULE', $start['module']);
	} else {
		$start['module'] = '';
	}
	if (isset($start['record']) && !empty($start['record'])) {
		$xtpl->assign('DELETE_RETURN_ID', $start['record']);
	} else {
		$start['record'] = '';
	}
	// this is to support returning to My Inbox
	if (isset($start['type']) && !empty($start['type'])) {
		$xtpl->assign('DELETE_RETURN_TYPE', $start['type']);
	} else {
		$start['type'] = '';
	}
	if (isset($start['assigned_user_id']) && !empty($start['assigned_user_id'])) {
		$xtpl->assign('DELETE_RETURN_ASSIGNED_USER_ID', $start['assigned_user_id']);
	} else {
		$start['assigned_user_id'] = '';
	}
	
	
	
	////	END RETURN NAVIGATION
	///////////////////////////////////////////////////////////////////////////////
	
	
	// DEFAULT TO TEXT IF NO HTML CONTENT:
	$html = trim(from_html($focus->description_html));
	if (empty($html)) {
		$xtpl->assign('SHOW_PLAINTEXT', 'true');
	} else {
		$xtpl->assign('SHOW_PLAINTEXT', 'false');
	}
	$show_subpanels = true;
	//if the email is of type campaign, process the macros...using the values stored in the relationship table.
	//this is is part of the feature that adds support for one email per campaign.
	if ($focus->type == 'campaign' and !empty($_REQUEST['parent_id']) and !empty($_REQUEST['parent_module'])) {
		$show_subpanels = false;
		$parent_id = $_REQUEST['parent_id'];
		
		// cn: bug 14300 - emails_beans schema refactor - fixing query
		$query = "SELECT * FROM emails_beans WHERE email_id='{$focus->id}' AND bean_id='{$parent_id}' AND bean_module = '{$_REQUEST['parent_module']}' ";
		
		$res = $focus->db->query($query);
		$row = $focus->db->fetchByAssoc($res);
		if (!empty($row)) {
			$campaign_data = $row['campaign_data'];
			$macro_values = array();
			if (!empty($campaign_data)) {
				$macro_values = unserialize(from_html($campaign_data));
			}
			
			if (count($macro_values) > 0) {
				$m_keys = array_keys($macro_values);
				$m_values = array_values($macro_values);
				
				$focus->name = str_replace($m_keys, $m_values, $focus->name);
				$focus->description = str_replace($m_keys, $m_values, $focus->description);
				$focus->description_html = str_replace($m_keys, $m_values, $focus->description_html);
				if (!empty($macro_values['sugar_to_email_address'])) {
					$focus->to_addrs = $macro_values['sugar_to_email_address'];
				}
			}
		}
	}
	//if not empty or set to test (from test campaigns)
	if (!empty($focus->parent_type) && $focus->parent_type != 'test') {
		$xtpl->assign('PARENT_MODULE', $focus->parent_type);
		$xtpl->assign('PARENT_TYPE_UNTRANSLATE', $focus->parent_type);
		$xtpl->assign('PARENT_TYPE', $app_list_strings['record_type_display'][$focus->parent_type] . ':');
	}
	
	$to_addr = !empty($focus->to_addrs_names) ? htmlspecialchars($focus->to_addrs_names, ENT_COMPAT, 'UTF-8') : nl2br($focus->to_addrs);
	$from_addr = !empty($focus->from_addr_name) ? htmlspecialchars($focus->from_addr_name, ENT_COMPAT, 'UTF-8') : nl2br($focus->from_addr);
	$cc_addr = !empty($focus->cc_addrs_names) ? htmlspecialchars($focus->cc_addrs_names, ENT_COMPAT, 'UTF-8') : nl2br($focus->cc_addrs);
	$bcc_addr = !empty($focus->bcc_addrs_names) ? htmlspecialchars($focus->bcc_addrs_names, ENT_COMPAT, 'UTF-8') : nl2br($focus->bcc_addrs);
	
	$xtpl->assign('MOD', $mod_strings);
	$xtpl->assign('APP', $app_strings);
	$xtpl->assign('GRIDLINE', $gridline);
	$xtpl->assign('PRINT_URL', 'index.php?' . $GLOBALS['request_string']);
	$xtpl->assign('ID', $focus->id);
	$xtpl->assign('TYPE', $email_type);
	$xtpl->assign('PARENT_NAME', $focus->parent_name);
	$xtpl->assign('PARENT_ID', $focus->parent_id);
	$xtpl->assign('NAME', $focus->name);
	$xtpl->assign('ASSIGNED_TO', $focus->assigned_user_name);
	$xtpl->assign('DATE_MODIFIED', $focus->date_modified);
	$xtpl->assign('DATE_ENTERED', $focus->date_entered);
	$xtpl->assign('DATE_START', $focus->date_start);
	$xtpl->assign('TIME_START', $focus->time_start);
	$xtpl->assign('FROM', $from_addr);
	$xtpl->assign('TO', $to_addr);
	$xtpl->assign('CC', $cc_addr);
	$xtpl->assign('BCC', $bcc_addr);
	$xtpl->assign('CREATED_BY', $focus->created_by_name);
	$xtpl->assign('MODIFIED_BY', $focus->modified_by_name);
	$xtpl->assign('DATE_SENT', $focus->date_entered);
	$xtpl->assign('EMAIL_NAME', 'RE: ' . $focus->name);
	$xtpl->assign("TAG", $focus->listviewACLHelper());
	
	$show_raw = FALSE;
	if (!empty($focus->raw_source)) {
		$xtpl->assign("RAW_METADATA", $focus->id);
		$show_raw = TRUE;
	}
	
	if (!empty($focus->reply_to_email)) {
		$replyTo = 
			"
		<tr>
        <td class=\"tabDetailViewDL\"><slot>" . $mod_strings['LBL_REPLY_TO_NAME'] . 
			"</slot></td>
        <td colspan=3 class=\"tabDetailViewDF\"><slot>" . $focus->reply_to_email . 
			"</slot></td>
        </tr>";
		$xtpl->assign("REPLY_TO", $replyTo);
	}
	
	
	
	// Using action menu (new UI) instead of buttons for Archived Email DetailView.
	$buttons = array(<<<EOD
            <input	title="{$app_strings['LBL_EDIT_BUTTON_TITLE']}" accessKey="{$app_strings['LBL_EDIT_BUTTON_KEY']}" class="button"
                    id="edit_button"
					onclick="	this.form.return_module.value='Emails';
								this.form.return_action.value='DetailView';
								this.form.return_id.value='{$focus->id}';
								this.form.action.value='EditView'"
					type="submit" name="Edit" value=" {$app_strings['LBL_EDIT_BUTTON_LABEL']}">
EOD
	, <<<EOD
            <input title="{$app_strings['LBL_DELETE_BUTTON_TITLE']}"
					accessKey="{$app_strings['LBL_DELETE_BUTTON_KEY']}"
					class="button"
					id="delete_button"
					onclick="this.form.return_module.value='{$start['module']}';
											this.form.return_action.value='{$start['action']}';
											this.form.return_id.value='{$start['record']}';
											this.form.type.value='{$start['type']}';
											this.form.assigned_user_id.value='{$start['assigned_user_id']}';
											this.form.action.value='Delete';
											return confirm('{$app_strings['NTC_DELETE_CONFIRMATION']}')"
					type="submit" name="button"
					value="{$app_strings['LBL_DELETE_BUTTON_LABEL']}"
			>
EOD
	);
	
	// Bug #52046: Disable the 'Show Raw' link where it does not need to be shown.
	if ($show_raw) {
		$buttons[] = <<<EOD
        <input type="button" name="button" class="button"
            id="rawButton"
            title="{$mod_strings['LBL_BUTTON_RAW_TITLE']}"
            value="{$mod_strings['LBL_BUTTON_RAW_LABEL']}"
            onclick="open_popup('Emails', 800, 600, '', true, true, '', 'show_raw', '', '{$focus->id}');"
        />
EOD;
	}
	
	require_once ('include/Smarty/plugins/function.sugar_action_menu.php');
	$action_button = smarty_function_sugar_action_menu(array(
		'id' => 'detail_header_action_menu', 
		'buttons' => $buttons, 
		'class' => 'clickMenu fancymenu'), $xtpl);
	
	$xtpl->assign("ACTION_BUTTON", $action_button);
	
	/////////
	///Using action menu (new UI) instead of buttons for Sent Email DetailView.
	$buttons_sent_email = array();
	if ($show_forward) {
		$buttons_sent_email[] = <<<EOD
            <input title="{$mod_strings['LBL_BUTTON_FORWARD']}"
					class="button" onclick="this.form.return_module.value='{$ret_mod}';
											this.form.return_action.value='{$ret_action}';
											this.form.return_id.value='{$focus->id}';
											this.form.action.value='EditView';
											this.form.type.value='forward'"
					type="submit" name="button"
					value="  {$mod_strings['LBL_BUTTON_FORWARD']}  "
					style="display:{DISABLE_FORWARD_BUTTON};"
			>
EOD;
	}
	$buttons_sent_email[] = <<<EOD
            <input title="{$mod_strings['LBL_BUTTON_REPLY_TITLE']}"
					class="button" onclick="this.form.return_module.value='{$ret_mod}';
											this.form.return_action.value='{$ret_action}';
											this.form.return_id.value='{$focus->id}';
											this.form.action.value='EditView';
											this.form.type.value='reply'"
					type="submit" name="button"
					value="  {$mod_strings['LBL_BUTTON_REPLY']}  "
			>
EOD;
	$buttons_sent_email[] = <<<EOD
            <input title="{$mod_strings['LBL_BUTTON_REPLY_ALL']}"
					class="button" onclick="this.form.return_module.value='{$ret_mod}';
											this.form.return_action.value='{$ret_action}';
											this.form.return_id.value='{$focus->id}';
											this.form.action.value='EditView';
											this.form.type.value='replyAll'"
					type="submit" name="button"
					value="  {$mod_strings['LBL_BUTTON_REPLY_ALL']}  "
			>
EOD;
	$buttons_sent_email[] = <<<EOD
            <input title="{$app_strings['LBL_DELETE_BUTTON_TITLE']}"
					accessKey="{$app_strings['LBL_DELETE_BUTTON_KEY']}"
					class="button" onclick="this.form.return_module.value='{$start['module']}';
											this.form.return_action.value='{$start['action']}';
											this.form.return_id.value='{$start['record']}';
											this.form.type.value='{$start['type']}';
											this.form.assigned_user_id.value='{$start['assigned_user_id']}';
											this.form.action.value='Delete';
											return confirm('{$app_strings['NTC_DELETE_CONFIRMATION']}')"
					type="submit" name="button"
					value="    {$app_strings['LBL_DELETE_BUTTON']}    "
			>
EOD;
	
	if ($show_raw) {
		$buttons_sent_email[] = <<<EOD
            <input type="button" name="button" class="button"
				id="rawButton"
				title="{$mod_strings['LBL_BUTTON_RAW_TITLE']}"
				value=" {$mod_strings['LBL_BUTTON_RAW_LABEL']} "
				onclick="open_popup('Emails', 800, 600, '', true, true, '', 'show_raw', '', '{$focus->id}');"
			/>
EOD;
	}
	
	require_once ('include/Smarty/plugins/function.sugar_action_menu.php');
	$action_button_sent_email = smarty_function_sugar_action_menu(array(
		'id' => 'detail_header_action_menu', 
		'buttons' => $buttons_sent_email, 
		'class' => 'clickMenu fancymenu'), $xtpl);
	
	$xtpl->assign("ACTION_BUTTON_SENT_EMAIL", $action_button_sent_email);
	
	///////////////////////////////////////////////////////////////////////////////
	////	JAVASCRIPT VARS
	$jsVars = '';
	$jsVars .= "var showRaw = '{$mod_strings['LBL_BUTTON_RAW_LABEL']}';";
	$jsVars .= "var hideRaw = '{$mod_strings['LBL_BUTTON_RAW_LABEL_HIDE']}';";
	$xtpl->assign("JS_VARS", $jsVars);
	
	
	// ADMIN EDIT
	if (is_admin($GLOBALS['current_user']) && $_REQUEST['module'] != 'DynamicLayout' && !empty($_SESSION['editinplace'])) {
		$xtpl->assign("ADMIN_EDIT", "<a href='index.php?action=index&module=DynamicLayout&from_action=" . $_REQUEST['action'] . "&from_module=" . $_REQUEST['module'] . "&record=" . $_REQUEST['record'] . "'>" . SugarThemeRegistry::current()->getImage("EditLayout", "border='0' align='bottom'", null, null, '.gif', $mod_strings['LBL_EDIT_LAYOUT']) . "</a>");
	}
	
	if (isset($_REQUEST['offset']) && !empty($_REQUEST['offset'])) {
		$offset = $_REQUEST['offset'];
	} else 
		$offset = 1;
	$detailView->processListNavigation($xtpl, "EMAIL", $offset, false);
	
	
	
	// adding custom fields:
	require_once ('modules/DynamicFields/templates/Files/DetailView.php');
	$do_open = true;
	if ($do_open) {
		$xtpl->parse("main.open_source");
	}
	
	///////////////////////////////////////////////////////////////////////////////
	////	NOTES (attachements, etc.)
	///////////////////////////////////////////////////////////////////////////////
	
	$note = new Note();
	$where = "notes.parent_id='{$focus->id}'";
	//take in account if this is from campaign and the template id is stored in the macros.
	
	if (isset($macro_values) && isset($macro_values['email_template_id'])) {
		$where = "notes.parent_id='{$macro_values['email_template_id']}'";
	}
	$notes_list = $note->get_full_list("notes.name", $where, true);
	
	if (!isset($notes_list)) {
		$notes_list = array();
	}
	
	$attachments = '';
	for($i = 0; $i < count($notes_list); $i++) {
		$the_note = $notes_list[$i];
		if (!empty($the_note->filename)) 
			$attachments .= "<a href=\"index.php?entryPoint=download&id=" . $the_note->id . "&type=Notes\">" . $the_note->name . "</a><br />";
		$focus->cid2Link($the_note->id, $the_note->file_mime_type);
	}
	
	$xtpl->assign('DESCRIPTION', nl2br($focus->description));
	$xtpl->assign('DESCRIPTION_HTML', from_html($focus->description_html));
	$xtpl->assign("ATTACHMENTS", $attachments);
	$xtpl->parse("main");
	$xtpl->out("main");
	
	$sub_xtpl = $xtpl;
	$old_contents = ob_get_contents();
	ob_end_clean();
	ob_start();
	echo $old_contents;
	
	///////////////////////////////////////////////////////////////////////////////
	////    SUBPANELS
	///////////////////////////////////////////////////////////////////////////////
	if ($show_subpanels) {
		require_once ('include/SubPanel/SubPanelTiles.php');
		$subpanel = new SubPanelTiles($focus, 'Emails');
		echo $subpanel->display();
	}
?>


//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	
	$GLOBALS['log']->info("Email edit view");
	
	require_once ('include/SugarTinyMCE.php');
	
	
	
	
	global $theme;
	global $app_strings;
	global $app_list_strings;
	global $mod_strings;
	global $current_user;
	global $sugar_version, $sugar_config;
	global $timedate;
	
	///////////////////////////////////////////////////////////////////////////////
	////	PREPROCESS BEAN DATA FOR DISPLAY
	$focus = new Email();
	$email_type = 'archived';
	
	if (isset($_REQUEST['record'])) {
		$focus->retrieve($_REQUEST['record']);
	}
	if (!empty($_REQUEST['type'])) {
		$email_type = $_REQUEST['type'];
	} elseif (!empty($focus->id)) {
		$email_type = $focus->type;
	}
	
	$focus->type = $email_type;
	
	//needed when creating a new email with default values passed in
	if (isset($_REQUEST['contact_name']) && is_null($focus->contact_name)) {
		$focus->contact_name = $_REQUEST['contact_name'];
	}
	
	if (!empty($_REQUEST['load_id']) && !empty($beanList[$_REQUEST['load_module']])) {
		$class_name = $beanList[$_REQUEST['load_module']];
		require_once ($beanFiles[$class_name]);
		$contact = new $class_name();
		if ($contact->retrieve($_REQUEST['load_id'])) {
			$link_id = $class_name . '_id';
			$focus->$link_id = $_REQUEST['load_id'];
			$focus->contact_name = (isset($contact->full_name)) ? $contact->full_name : $contact->name;
			$focus->to_addrs_names = $focus->contact_name;
			$focus->to_addrs_ids = $_REQUEST['load_id'];
			//Retrieve the email address.
			//If Opportunity or Case then Oppurtinity/Case->Accounts->(email_addr_bean_rel->email_addresses)
			//If Contacts, Leads etc.. then Contact->(email_addr_bean_rel->email_addresses)
			$sugarEmailAddress = new SugarEmailAddress();
			if ($class_name == 'Opportunity' || $class_name == 'aCase') {
				$account = new Account();
				if ($contact->account_id != null && $account->retrieve($contact->account_id)) {
					$sugarEmailAddress->handleLegacyRetrieve($account);
					if (isset($account->email1)) {
						$focus->to_addrs_emails = $account->email1;
						$focus->to_addrs = "$focus->contact_name <$account->email1>";
					}
				}
			} else {
				$sugarEmailAddress->handleLegacyRetrieve($contact);
				if (isset($contact->email1)) {
					$focus->to_addrs_emails = $contact->email1;
					$focus->to_addrs = "$focus->contact_name <$contact->email1>";
				}
			}
			if (!empty($_REQUEST['parent_type']) && empty($app_list_strings['record_type_display'][$_REQUEST['parent_type']])) {
				if (!empty($app_list_strings['record_type_display'][$_REQUEST['load_module']])) {
					$_REQUEST['parent_type'] = $_REQUEST['load_module'];
					$_REQUEST['parent_id'] = $focus->contact_id;
					$_REQUEST['parent_name'] = $focus->to_addrs_names;
				} else {
					unset($_REQUEST['parent_type']);
					unset($_REQUEST['parent_id']);
					unset($_REQUEST['parent_name']);
				}
			}
		}
	}
	if (isset($_REQUEST['contact_id']) && is_null($focus->contact_id)) {
		$focus->contact_id = $_REQUEST['contact_id'];
	}
	if (isset($_REQUEST['parent_name'])) {
		$focus->parent_name = $_REQUEST['parent_name'];
	}
	if (isset($_REQUEST['parent_id'])) {
		$focus->parent_id = $_REQUEST['parent_id'];
	}
	if (isset($_REQUEST['parent_type'])) {
		$focus->parent_type = $_REQUEST['parent_type'];
	} elseif (is_null($focus->parent_type)) {
		$focus->parent_type = $app_list_strings['record_type_default_key'];
	}
	if (isset($_REQUEST['to_email_addrs'])) {
		$focus->to_addrs = $_REQUEST['to_email_addrs'];
	}
	// needed when clicking through a Contacts detail view:
	if (isset($_REQUEST['to_addrs_ids'])) {
		$focus->to_addrs_ids = $_REQUEST['to_addrs_ids'];
	}
	if (isset($_REQUEST['to_addrs_emails'])) {
		$focus->to_addrs_emails = $_REQUEST['to_addrs_emails'];
	}
	if (isset($_REQUEST['to_addrs_names'])) {
		$focus->to_addrs_names = $_REQUEST['to_addrs_names'];
	}
	// user's email, go through 3 levels of precedence:
	$from = $current_user->getEmailInfo();
	////	END PREPROCESSING
	///////////////////////////////////////////////////////////////////////////////
	
	
	///////////////////////////////////////////////////////////////////////////////
	////	XTEMPLATE ASSIGNMENT
	if ($email_type == 'archived') {
		if (isset($focus->name)) 
			$params_module_title = array($mod_strings['LBL_ARCHIVED_EMAILS_CREATE'], $focus->name);
		 else 
			$params_module_title = array($mod_strings['LBL_ARCHIVED_EMAILS_CREATE']);
		echo getClassicModuleTitle('Emails', $params_module_title, true);
		$xtpl = new XTemplate('modules/Emails/EditViewArchive.html');
	} else {
		
		if (empty($_REQUEST['parent_module'])) {
			$parent_module = "Emails";
		} else {
			$parent_module = $_REQUEST['parent_module'];
		}
		if (empty($_REQUEST['parent_id'])) {
			$parent_id = "";
		} else {
			$parent_id = $_REQUEST['parent_id'];
		}
		if (empty($_REQUEST['return_module'])) {
			$return_module = "Emails";
		} else {
			$return_module = $_REQUEST['return_module'];
		}
		if (empty($_REQUEST['return_module'])) {
			$return_id = "";
		} else {
			$return_id = $_REQUEST['return_module'];
		}
		$replyType = "reply";
		if ($_REQUEST['type'] == 'forward' || $_REQUEST['type'] == 'replyAll') {
			$replyType = $_REQUEST['type'];
		}
		
		header("Location: index.php?module=Emails&action=Compose&record=$focus->id&replyForward=true&reply=$replyType");
		return;
	}
	echo "\n</p>\n";
	
	// CHECK USER'S EMAIL SETTINGS TO ENABLE/DISABLE 'SEND' BUTTON
	if (!$focus->check_email_settings() && ($email_type == 'out' || $email_type == 'draft')) {
		print "<font color='red'>" . $mod_strings['WARNING_SETTINGS_NOT_CONF'] . " <a href='index.php?module=Users&action=EditView&record=" . $current_user->id . "&return_module=Emails&type=out&return_action=EditView'>" . $mod_strings['LBL_EDIT_MY_SETTINGS'] . "</a></font>";
		$xtpl->assign("DISABLE_SEND", 'DISABLED');
	}
	
	// CHECK THAT SERVER HAS A PLACE TO PUT UPLOADED TEMP FILES SO THAT ATTACHMENTS WILL WORK
	// cn: Bug 5995
	$tmpUploadDir = ini_get('upload_tmp_dir');
	if (!empty($tmpUploadDir)) {
		if (!is_writable($tmpUploadDir)) {
			echo "<font color='red'>{$mod_strings['WARNING_UPLOAD_DIR_NOT_WRITABLE']}</font>";
		}
	} else {
		//echo "<font color='red'>{$mod_strings['WARNING_NO_UPLOAD_DIR']}</font>";
	}
	
	
	///////////////////////////////////////////////////////////////////////////////
	////	INBOUND EMAIL HANDLING
	if (isset($_REQUEST['email_name'])) {
		$name = str_replace('_', ' ', $_REQUEST['email_name']);
	}
	if (isset($_REQUEST['inbound_email_id'])) {
		$ieMail = new Email();
		$ieMail->retrieve($_REQUEST['inbound_email_id']);
		
		$quoted = '';
		// cn: bug 9725: replies/forwards lose real content
		$quotedHtml = $ieMail->quoteHtmlEmail($ieMail->description_html);
		
		// plain-text
		$desc = nl2br(trim($ieMail->description));
		
		$exDesc = explode('<br />', $desc);
		foreach ($exDesc as $k => $line) {
			$quoted .= '> ' . trim($line) . "\r";
		}
		
		// prefill empties with the other's contents
		if (empty($quotedHtml) && !empty($quoted)) {
			$quotedHtml = nl2br($quoted);
		}
		if (empty($quoted) && !empty($quotedHtml)) {
			$quoted = strip_tags(br2nl($quotedHtml));
		}
		
		// forwards have special text
		if ($_REQUEST['type'] == 'forward') {
			$header = $ieMail->getForwardHeader();
			// subject is handled in Subject line handling below
		} else {
			// we have a reply in focus
			$header = $ieMail->getReplyHeader();
		}
		
		$quoted = br2nl($header . $quoted);
		$quotedHtml = $header . $quotedHtml;
		
		
		// if not a forward: it's a reply
		if ($_REQUEST['type'] != 'forward') {
			$ieMailName = 'RE: ' . $ieMail->name;
		} else {
			$ieMailName = $ieMail->name;
		}
		
		$focus->id = null; // nulling this to prevent overwriting a replied email(we're basically doing a "Duplicate" function)
		$focus->to_addrs = $ieMail->from_addr;
		$focus->description = $quoted; // don't know what i was thinking: ''; // this will be filled on save/send
		$focus->description_html = $quotedHtml; // cn: bug 7357 - htmlentities() breaks FCKEditor
		$focus->parent_type = $ieMail->parent_type;
		$focus->parent_id = $ieMail->parent_id;
		$focus->parent_name = $ieMail->parent_name;
		$focus->name = $ieMailName;
		$xtpl->assign('INBOUND_EMAIL_ID', $_REQUEST['inbound_email_id']);
		// un/READ flags
		if (!empty($ieMail->status)) {
			// "Read" flag for InboundEmail
			if ($ieMail->status == 'unread') {
				// creating a new instance here to avoid data corruption below
				$e = new Email();
				$e->retrieve($ieMail->id);
				$e->status = 'read';
				$e->save();
				$email_type = $e->status;
			}
		}
		
		///////////////////////////////////////////////////////////////////////////
		////	PRIMARY PARENT LINKING
		if (empty($focus->parent_type) && empty($focus->parent_id)) {
			$focus->fillPrimaryParentFields();
		}
		////	END PRIMARY PARENT LINKING
		///////////////////////////////////////////////////////////////////////////
		
		// setup for my/mailbox email switcher
		$mbox = $ieMail->getMailboxDefaultEmail();
		$user = $current_user->getPreferredEmail();
		$useGroup = 
			'&nbsp;<input id="use_mbox" name="use_mbox" type="checkbox" CHECKED onClick="switchEmail()" >
				<script type="text/javascript">
				function switchEmail() {
					var mboxName = "' . $mbox['name'] . 
			'";
					var mboxAddr = "' . $mbox['email'] . 
			'";
					var userName = "' . $user['name'] . 
			'";
					var userAddr = "' . $user['email'] . 
			'";

					if(document.getElementById("use_mbox").checked) {
						document.getElementById("from_addr_field").value = mboxName + " <" + mboxAddr + ">";
						document.getElementById("from_addr_name").value = mboxName;
						document.getElementById("from_addr_email").value = mboxAddr;
					} else {
						document.getElementById("from_addr_field").value = userName + " <" + userAddr + ">";
						document.getElementById("from_addr_name").value = userName;
						document.getElementById("from_addr_email").value = userAddr;
					}

				}

				</script>';
		$useGroup .= $mod_strings['LBL_USE_MAILBOX_INFO'];
		
		$xtpl->assign('FROM_ADDR_GROUP', $useGroup);
	}
	////	END INBOUND EMAIL HANDLING
	///////////////////////////////////////////////////////////////////////////////
	
	///////////////////////////////////////////////////////////////////////////////
	////	SUBJECT FIELD MANIPULATION
	$name = '';
	if (!empty($_REQUEST['parent_id']) && !empty($_REQUEST['parent_type'])) {
		$focus->parent_id = $_REQUEST['parent_id'];
		$focus->parent_type = $_REQUEST['parent_type'];
	}
	if (!empty($focus->parent_id) && !empty($focus->parent_type)) {
		if ($focus->parent_type == 'Cases') {
			
			$myCase = new aCase();
			$myCase->retrieve($focus->parent_id);
			$myCaseMacro = $myCase->getEmailSubjectMacro();
			if (isset($ieMail->name) && !empty($ieMail->name)) {
				// if replying directly to an InboundEmail
				$oldEmailSubj = $ieMail->name;
			} elseif (isset($_REQUEST['parent_name']) && !empty($_REQUEST['parent_name'])) {
				$oldEmailSubj = $_REQUEST['parent_name'];
			} else {
				$oldEmailSubj = $focus->name; // replying to an email using old subject
			}
			
			if (!preg_match('/^re:/i', $oldEmailSubj)) {
				$oldEmailSubj = 'RE: ' . $oldEmailSubj;
			}
			$focus->name = $oldEmailSubj;
			
			if (strpos($focus->name, str_replace('%1', $myCase->case_number, $myCaseMacro))) {
				$name = $focus->name;
			} else {
				$name = $focus->name . ' ' . str_replace('%1', $myCase->case_number, $myCaseMacro);
			}
		} else {
			$name = $focus->name;
		}
	} else {
		if (empty($focus->name)) {
			$name = '';
		} else {
			$name = $focus->name;
		}
	}
	if ($email_type == 'forward') {
		$name = 'FW: ' . $name;
	}
	////	END SUBJECT FIELD MANIPULATION
	///////////////////////////////////////////////////////////////////////////////
	
	///////////////////////////////////////////////////////////////////////////////
	////	GENERAL TEMPLATE ASSIGNMENTS
	$xtpl->assign('MOD', $mod_strings);
	$xtpl->assign('APP', $app_strings);
	$xtpl->assign('THEME', SugarThemeRegistry::current()->__toString());
	if (!isset($focus->id)) 
		$xtpl->assign('USER_ID', $current_user->id);
	if (!isset($focus->id) && isset($_REQUEST['contact_id'])) 
		$xtpl->assign('CONTACT_ID', $_REQUEST['contact_id']);
	
	if (isset($_REQUEST['return_module']) && !empty($_REQUEST['return_module'])) {
		$xtpl->assign('RETURN_MODULE', $_REQUEST['return_module']);
	} else {
		$xtpl->assign('RETURN_MODULE', 'Emails');
	}
	if (isset($_REQUEST['return_action']) && !empty($_REQUEST['return_action']) && ($_REQUEST['return_action'] != 'SubPanelViewer')) {
		$xtpl->assign('RETURN_ACTION', $_REQUEST['return_action']);
	} else {
		$xtpl->assign('RETURN_ACTION', 'DetailView');
	}
	if (isset($_REQUEST['return_id']) && !empty($_REQUEST['return_id'])) {
		$xtpl->assign('RETURN_ID', $_REQUEST['return_id']);
	}
	// handle Create $module then Cancel
	if (empty($_REQUEST['return_id']) && !isset($_REQUEST['type'])) {
		$xtpl->assign('RETURN_ACTION', 'index');
	}
	
	$xtpl->assign('PRINT_URL', 'index.php?' . $GLOBALS['request_string']);
	
	
	///////////////////////////////////////////////////////////////////////////////
	////	QUICKSEARCH CODE
	require_once ('include/QuickSearchDefaults.php');
	$qsd = QuickSearchDefaults::getQuickSearchDefaults();
	$sqs_objects = array('EditView_parent_name' => $qsd->getQSParent(), 
		'EditView_assigned_user_name' => $qsd->getQSUser());
	
	
	$json = getJSONobj();
	
	$sqs_objects_encoded = $json->encode($sqs_objects);
	$quicksearch_js = <<<EOQ
		<script type="text/javascript" language="javascript">sqs_objects = $sqs_objects_encoded; var dialog_loaded;
			function parent_typechangeQS() {
				var new_module = document.EditView.parent_type.value;
				var sqsId = 'EditView_parent_name';
				if(new_module == 'Contacts' || new_module == 'Leads' || typeof(disabledModules[new_module]) != 'undefined') {
					sqs_objects[sqsId]['disable'] = true;
					document.getElementById('parent_name').readOnly = true;
				}
				else {
					sqs_objects[sqsId]['disable'] = false;
					document.getElementById('parent_name').readOnly = false;
				}

				sqs_objects[sqsId]['modules'] = new Array(new_module);
				if (typeof(QSFieldsArray[sqsId]) != 'undefined')
                {
                    QSFieldsArray[sqsId].sqs.modules = new Array(new_module);
                }
				enableQS(false);
			}
			parent_typechangeQS();
		</script>
EOQ;
	$xtpl->assign('JAVASCRIPT', get_set_focus_js() . $quicksearch_js);
	////	END QUICKSEARCH CODE
	///////////////////////////////////////////////////////////////////////////////
	
	// cn: bug 14191 - duping archive emails overwrites the original
	if (!isset($_REQUEST['isDuplicate']) || $_REQUEST['isDuplicate'] != 'true') {
		$xtpl->assign('ID', $focus->id);
	}
	
	if (isset($_REQUEST['parent_type']) && !empty($_REQUEST['parent_type']) && isset($_REQUEST['parent_id']) && !empty($_REQUEST['parent_id'])) {
		$xtpl->assign('OBJECT_ID', $_REQUEST['parent_id']);
		$xtpl->assign('OBJECT_TYPE', $_REQUEST['parent_type']);
	}
	$xtpl->assign('FROM_ADDR', $focus->from_addr);
	//// prevent TO: prefill when type is 'forward'
	if ($email_type != 'forward') {
		$xtpl->assign('TO_ADDRS', $focus->to_addrs);
		$xtpl->assign('TO_ADDRS_IDS', $focus->to_addrs_ids);
		$xtpl->assign('TO_ADDRS_NAMES', $focus->to_addrs_names);
		$xtpl->assign('TO_ADDRS_EMAILS', $focus->to_addrs_emails);
		$xtpl->assign('CC_ADDRS', $focus->cc_addrs);
		$xtpl->assign('CC_ADDRS_IDS', $focus->cc_addrs_ids);
		$xtpl->assign('CC_ADDRS_NAMES', $focus->cc_addrs_names);
		$xtpl->assign('CC_ADDRS_EMAILS', $focus->cc_addrs_emails);
		$xtpl->assign('BCC_ADDRS', $focus->bcc_addrs);
		$xtpl->assign('BCC_ADDRS_IDS', $focus->bcc_addrs_ids);
		$xtpl->assign('BCC_ADDRS_NAMES', $focus->bcc_addrs_names);
		$xtpl->assign('BCC_ADDRS_EMAILS', $focus->bcc_addrs_emails);
	}
	
	//$xtpl->assign('FROM_ADDR', $from['name'].' <'.$from['email'].'>');
	$xtpl->assign('FROM_ADDR_NAME', $from['name']);
	$xtpl->assign('FROM_ADDR_EMAIL', $from['email']);
	
	$xtpl->assign('NAME', from_html($name));
	//$xtpl->assign('DESCRIPTION_HTML', from_html($focus->description_html));
	$xtpl->assign('DESCRIPTION', $focus->description);
	$xtpl->assign('TYPE', $email_type);
	
	// Unimplemented until jscalendar language files are fixed
	// $xtpl->assign('CALENDAR_LANG',((empty($cal_codes[$current_language])) ? $cal_codes[$default_language] : $cal_codes[$current_language]));
	$xtpl->assign('CALENDAR_LANG', 'en');
	$xtpl->assign('CALENDAR_DATEFORMAT', $timedate->get_cal_date_format());
	$xtpl->assign('DATE_START', $focus->date_start);
	$xtpl->assign('TIME_FORMAT', '(' . $timedate->get_user_time_format() . ')');
	$xtpl->assign('TIME_START', substr($focus->time_start, 0, 5));
	$xtpl->assign('TIME_MERIDIEM', $timedate->AMPMMenu('', $focus->time_start));
	
	$parent_types = $app_list_strings['record_type_display'];
	$disabled_parent_types = ACLController::disabledModuleList($parent_types, false, 'list');
	
	foreach ($disabled_parent_types as $disabled_parent_type) {
		if ($disabled_parent_type != $focus->parent_type) {
			unset($parent_types[$disabled_parent_type]);
		}
	}
	
	$xtpl->assign('TYPE_OPTIONS', get_select_options_with_id($parent_types, $focus->parent_type));
	$xtpl->assign('USER_DATEFORMAT', '(' . $timedate->get_user_date_format() . ')');
	$xtpl->assign('PARENT_NAME', $focus->parent_name);
	$xtpl->assign('PARENT_ID', $focus->parent_id);
	if (empty($focus->parent_type)) {
		$xtpl->assign('PARENT_RECORD_TYPE', '');
	} else {
		$xtpl->assign('PARENT_RECORD_TYPE', $focus->parent_type);
	}
	
	if (is_admin($current_user) && $_REQUEST['module'] != 'DynamicLayout' && !empty($_SESSION['editinplace'])) {
		$record = '';
		if (!empty($_REQUEST['record'])) {
			$record = $_REQUEST['record'];
		}
		$xtpl->assign('ADMIN_EDIT', "<a href='index.php?action=index&module=DynamicLayout&from_action=" . $_REQUEST['action'] . "&from_module=" . $_REQUEST['module'] . "&record=" . $record . "'>" . SugarThemeRegistry::current()->getImage("EditLayout", "border='0' align='bottom'", null, null, '.gif', $mod_strings['LBL_EDIT_LAYOUT']) . "</a>");
	}
	
	////	END GENERAL TEMPLATE ASSIGNMENTS
	///////////////////////////////////////////////////////////////////////////////
	
	
	///////////////////////////////////////
	///
	/// SETUP PARENT POPUP
	
	$popup_request_data = array(
		'call_back_function' => 'set_return', 
		'form_name' => 'EditView', 
		'field_to_name_array' => array(
		'id' => 'parent_id', 
		'name' => 'parent_name'));
	
	$encoded_popup_request_data = $json->encode($popup_request_data);
	
	/// Users Popup
	$popup_request_data = array(
		'call_back_function' => 'set_return', 
		'form_name' => 'EditView', 
		'field_to_name_array' => array(
		'id' => 'assigned_user_id', 
		'user_name' => 'assigned_user_name'));
	$xtpl->assign('encoded_users_popup_request_data', $json->encode($popup_request_data));
	
	
	//
	///////////////////////////////////////
	
	$change_parent_button = '<input type="button" name="button" tabindex="2" class="button" ' . 'title="' . $app_strings['LBL_SELECT_BUTTON_TITLE'] . '" ' . 'value="' . $app_strings['LBL_SELECT_BUTTON_LABEL'] . '" ' . "onclick='ValidateParentType();' />\n" . 
		'<script>function ValidateParentType() {
        	var parentTypeValue = document.getElementById("parent_type").value;
    		if (trim(parentTypeValue) == ""){
    			alert("' . $mod_strings['LBL_ERROR_SELECT_MODULE'] . 
		'");
    			return false;
    		}
    		open_popup(document.EditView.parent_type.value,600,400,"&tree=ProductsProd",true,false,' . $encoded_popup_request_data . 
		');
		}</script>';
	
	$xtpl->assign("CHANGE_PARENT_BUTTON", $change_parent_button);
	
	$button_attr = '';
	if (!ACLController::checkAccess('Contacts', 'list', true)) {
		$button_attr = 'disabled="disabled"';
	}
	
	$change_to_addrs_button = '<input type="button" name="to_button" tabindex="3" class="button" ' . 'title="' . $app_strings['LBL_SELECT_BUTTON_TITLE'] . '" ' . 'value="' . $mod_strings['LBL_EMAIL_SELECTOR_SELECT'] . '" ' . "onclick='button_change_onclick(this);' $button_attr />\n";
	$xtpl->assign("CHANGE_TO_ADDRS_BUTTON", $change_to_addrs_button);
	
	$change_cc_addrs_button = '<input type="button" name="cc_button" tabindex="3" class="button" ' . 'title="' . $app_strings['LBL_SELECT_BUTTON_TITLE'] . '" ' . 'value="' . $mod_strings['LBL_EMAIL_SELECTOR_SELECT'] . '" ' . "onclick='button_change_onclick(this);' $button_attr />\n";
	$xtpl->assign("CHANGE_CC_ADDRS_BUTTON", $change_cc_addrs_button);
	
	$change_bcc_addrs_button = '<input type="button" name="bcc_button" tabindex="3" class="button" ' . 'title="' . $app_strings['LBL_SELECT_BUTTON_TITLE'] . '" ' . 'value="' . $mod_strings['LBL_EMAIL_SELECTOR_SELECT'] . '" ' . "onclick='button_change_onclick(this);' $button_attr />\n";
	$xtpl->assign("CHANGE_BCC_ADDRS_BUTTON", $change_bcc_addrs_button);
	
	
	///////////////////////////////////////
	////	USER ASSIGNMENT
	global $current_user;
	if (is_admin($current_user) && $_REQUEST['module'] != 'DynamicLayout' && !empty($_SESSION['editinplace'])) {
		$record = '';
		if (!empty($_REQUEST['record'])) {
			$record = $_REQUEST['record'];
		}
		$xtpl->assign('ADMIN_EDIT', "<a href='index.php?action=index&module=DynamicLayout&from_action=" . $_REQUEST['action'] . "&from_module=" . $_REQUEST['module'] . "&record=" . $record . "'>" . SugarThemeRegistry::current()->getImage("EditLayout", "border='0' align='bottom'", null, null, '.gif', $mod_strings['LBL_EDIT_LAYOUT']) . "</a>");
	}
	
	if (empty($focus->assigned_user_id) && empty($focus->id)) 
		$focus->assigned_user_id = $current_user->id;
	if (empty($focus->assigned_name) && empty($focus->id)) 
		$focus->assigned_user_name = $current_user->user_name;
	$xtpl->assign('ASSIGNED_USER_OPTIONS', get_select_options_with_id(get_user_array(TRUE, 'Active', $focus->assigned_user_id), $focus->assigned_user_id));
	$xtpl->assign('ASSIGNED_USER_NAME', $focus->assigned_user_name);
	$xtpl->assign('ASSIGNED_USER_ID', $focus->assigned_user_id);
	$xtpl->assign('DURATION_HOURS', $focus->duration_hours);
	$xtpl->assign('TYPE_OPTIONS', get_select_options_with_id($parent_types, $focus->parent_type));
	
	if (isset($focus->duration_minutes)) {
		$xtpl->assign('DURATION_MINUTES_OPTIONS', get_select_options_with_id($focus->minutes_values, $focus->duration_minutes));
	}
	////	END USER ASSIGNMENT
	///////////////////////////////////////
	
	
	
	//Add Custom Fields
	require_once ('modules/DynamicFields/templates/Files/EditView.php');
	
	
	///////////////////////////////////////
	////	ATTACHMENTS
	$attachments = '';
	if (!empty($focus->id) || (!empty($_REQUEST['record']) && $_REQUEST['type'] == 'forward')) {
		
		$attachments = "<input type='hidden' name='removeAttachment' id='removeAttachment' value=''>\n";
		$ids = '';
		
		$focusId = empty($focus->id) ? $_REQUEST['record'] : $focus->id;
		$note = new Note();
		$where = "notes.parent_id='{$focusId}' AND notes.filename IS NOT NULL";
		$notes_list = $note->get_full_list("", $where, true);
		
		if (!isset($notes_list)) {
			$notes_list = array();
		}
		for($i = 0; $i < count($notes_list); $i++) {
			$the_note = $notes_list[$i];
			if (empty($the_note->filename)) {
				continue;
			}
			
			// cn: bug 8034 - attachments from forwards/replies lost when saving drafts
			if (!empty($ids)) {
				$ids .= ",";
			}
			$ids .= $the_note->id;
			
			$attachments .= 
				"
			<div id='noteDiv{$the_note->id}'>
				" . SugarThemeRegistry::current()->getImage('delete_inline', "onclick='deletePriorAttachment(\"{$the_note->id}\");' value='{$the_note->id}'", null, null, ".gif", $mod_strings['LBL_DELETE_INLINE']) . "&nbsp;";
			$attachments .= "<a href=\"index.php?entryPoint=download&id=" . $the_note->id . "&type=Notes\">" . $the_note->name . "</a><div />";
			//$attachments .= '<a href="'.UploadFile::get_url($the_note->filename,$the_note->id).'&entryPoint=download&type=Notes' . '" target="_blank">'. $the_note->filename .'</a></div>';
			
		}
		// cn: bug 8034 - attachments from forwards/replies lost when saving drafts
		$attachments .= "<input type='hidden' name='prior_attachments' value='{$ids}'>";
		
		// workaround $mod_strings being overriden by Note object instantiation above.
		global $current_language, $mod_strings;
		$mod_strings = return_module_language($current_language, 'Emails');
	}
	
	$attJs = '<script type="text/javascript">';
	$attJs .= 'var lnk_remove = "' . $app_strings['LNK_REMOVE'] . '";';
	$attJs .= '</script>';
	$xtpl->assign('ATTACHMENTS', $attachments);
	$xtpl->assign('ATTACHMENTS_JAVASCRIPT', $attJs);
	////	END ATTACHMENTS
	///////////////////////////////////////
	
	///////////////////////////////////////////////////////////////////////////////
	////	DOCUMENTS
	$popup_request_data = array(
		'call_back_function' => 'document_set_return', 
		'form_name' => 'EditView', 
		'field_to_name_array' => array(
		'id' => 'related_doc_id', 
		'document_name' => 'related_document_name'));
	$json = getJSONobj();
	$xtpl->assign('encoded_document_popup_request_data', $json->encode($popup_request_data));
	////	END DOCUMENTS
	///////////////////////////////////////////////////////////////////////////////
	
	$parse_open = true;
	
	if ($parse_open) {
		$xtpl->parse('main.open_source_1');
	}
	///////////////////////////////////////////////////////////////////////////////
	////	EMAIL TEMPLATES
	if (ACLController::checkAccess('EmailTemplates', 'list', true) && ACLController::checkAccess('EmailTemplates', 'view', true)) {
		$et = new EmailTemplate();
		$etResult = $focus->db->query($et->create_new_list_query('', '', array(), array(), ''));
		$email_templates_arr[] = '';
		while ($etA = $focus->db->fetchByAssoc($etResult)) {
			$email_templates_arr[$etA['id']] = $etA['name'];
		}
	} else {
		$email_templates_arr = array('' => $app_strings['LBL_NONE']);
	}
	
	$xtpl->assign('EMAIL_TEMPLATE_OPTIONS', get_select_options_with_id($email_templates_arr, ''));
	////	END EMAIL TEMPLATES
	///////////////////////////////////////////////////////////////////////////////
	
	///////////////////////////////////////
	////	TEXT EDITOR
	// cascade from User to Sys Default
	$editor = $focus->getUserEditorPreference();
	
	if ($editor != 'plain') {
		// this box is checked by Javascript on-load.
		$xtpl->assign('EMAIL_EDITOR_OPTION', 'CHECKED');
	}
	$description_html = from_html($focus->description_html);
	$description = $focus->description;
	
	/////////////////////////////////////////////////
	// signatures
	if ($sig = $current_user->getDefaultSignature()) {
		if (!$focus->hasSignatureInBody($sig) && $focus->type != 'draft') {
			if ($current_user->getPreference('signature_prepend')) {
				$description_html = '<br />' . from_html($sig['signature_html']) . '<br /><br />' . $description_html;
				$description = "\n" . $sig['signature'] . "\n\n" . $description;
			} else {
				$description_html .= '<br /><br />' . from_html($sig['signature_html']);
				$description = $description . "\n\n" . $sig['signature'];
			}
		}
	}
	$xtpl->assign('DESCRIPTION', $description);
	// sigs
	/////////////////////////////////////////////////
	$tiny = new SugarTinyMCE();
	$ed = $tiny->getInstance("description_html");
	$xtpl->assign("TINY", $ed);
	$xtpl->assign("DESCRIPTION_HTML", $description_html);
	
	$xtpl->parse('main.htmlarea');
	////	END TEXT EDITOR
	///////////////////////////////////////
	
	///////////////////////////////////////
	////	SPECIAL INBOUND LANDING SCREEN ASSIGNS
	if (!empty($_REQUEST['inbound_email_id'])) {
		if (!empty($_REQUEST['start'])) {
			$parts = $focus->getStartPage(base64_decode($_REQUEST['start']));
			$xtpl->assign('RETURN_ACTION', $parts['action']);
			$xtpl->assign('RETURN_MODULE', $parts['module']);
			$xtpl->assign('GROUP', $parts['group']);
		}
		$xtpl->assign('ASSIGNED_USER_ID', $current_user->id);
		$xtpl->assign('MYINBOX', 'this.form.type.value=\'inbound\';');
	}
	////	END SPECIAL INBOUND LANDING SCREEN ASSIGNS
	///////////////////////////////////////
	
	echo '<script>var disabledModules=' . $json->encode($disabled_parent_types) . ';</script>';
	$jsVars = 'var lbl_send_anyways = "' . $mod_strings['LBL_SEND_ANYWAYS'] . '";';
	$xtpl->assign('JS_VARS', $jsVars);
	$xtpl->parse("main");
	$xtpl->out("main");
	echo '<script>checkParentType(document.EditView.parent_type.value, document.EditView.change_parent);</script>';
	////	END XTEMPLATE ASSIGNMENT
	///////////////////////////////////////////////////////////////////////////////
	
	
	$javascript = new javascript();
	$javascript->setFormName('EditView');
	$javascript->setSugarBean($focus);
	$skip_fields = array();
	if ($email_type == 'out') {
		$skip_fields['name'] = 1;
		$skip_fields['date_start'] = 1;
	}
	$javascript->addAllFields('', $skip_fields);
	$javascript->addToValidateBinaryDependency('parent_name', 'alpha', $app_strings['ERR_SQS_NO_MATCH_FIELD'] . $mod_strings['LBL_MEMBER_OF'], 'false', '', 'parent_id');
	$javascript->addToValidateBinaryDependency('parent_type', 'alpha', $app_strings['ERR_SQS_NO_MATCH_FIELD'] . $mod_strings['LBL_MEMBER_OF'], 'false', '', 'parent_id');
	$javascript->addToValidateBinaryDependency('user_name', 'alpha', $app_strings['ERR_SQS_NO_MATCH_FIELD'] . $app_strings['LBL_ASSIGNED_TO'], 'false', '', 'assigned_user_id');
	if ($email_type == 'archived') {
		$javascript->addFieldIsValidDate('date_start', 'date', $mod_strings['LBL_DATE'], $mod_strings['ERR_DATE_START'], true);
		$javascript->addFieldIsValidTime('time_start', 'time', $mod_strings['LBL_TIME'], $mod_strings['ERR_TIME_SENT'], true);
	}
	echo $javascript->getScript();
	
	
	//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	
	require_once ('include/SugarPHPMailer.php');
	require_once 'include/upload_file.php';
	
	class Email extends SugarBean
	{
		/* SugarBean schema */
		var $id;
		var $date_entered;
		var $date_modified;
		var $assigned_user_id;
		var $assigned_user_name;
		var $modified_user_id;
		var $created_by;
		var $deleted;
		var $from_addr;
		var $reply_to_addr;
		var $to_addrs;
		var $cc_addrs;
		var $bcc_addrs;
		var $message_id;
		
		/* Bean Attributes */
		var $name;
		var $type = 'archived';
		var $date_sent;
		var $status;
		var $intent;
		var $mailbox_id;
		var $from_name;
		
		var $reply_to_status;
		var $reply_to_name;
		var $reply_to_email;
		var $description;
		var $description_html;
		var $raw_source;
		var $parent_id;
		var $parent_type;
		
		/* link attributes */
		var $parent_name;
		
		
		/* legacy */
		var $date_start; // legacy
		var $time_start; // legacy
		var $from_addr_name;
		var $to_addrs_arr;
		var $cc_addrs_arr;
		var $bcc_addrs_arr;
		var $to_addrs_ids;
		var $to_addrs_names;
		var $to_addrs_emails;
		var $cc_addrs_ids;
		var $cc_addrs_names;
		var $cc_addrs_emails;
		var $bcc_addrs_ids;
		var $bcc_addrs_names;
		var $bcc_addrs_emails;
		var $contact_id;
		var $contact_name;
		
		/* Archive Email attrs */
		var $duration_hours;
		
		
		
		var $new_schema = true;
		var $table_name = 'emails';
		var $module_dir = 'Emails';
		var $module_name = 'Emails';
		var $object_name = 'Email';
		var $db;
		
		/* private attributes */
		var $rolloverStyle = "<style>div#rollover {position: relative;float: left;margin: none;text-decoration: none;}div#rollover a:hover {padding: 0;text-decoration: none;}div#rollover a span {display: none;}div#rollover a:hover span {text-decoration: none;display: block;width: 250px;margin-top: 5px;margin-left: 5px;position: absolute;padding: 10px;color: #333;	border: 1px solid #ccc;	background-color: #fff;	font-size: 12px;z-index: 1000;}</style>\n";
		var $cachePath;
		var $cacheFile = 'robin.cache.php';
		var $replyDelimiter = "> ";
		var $emailDescription;
		var $emailDescriptionHTML;
		var $emailRawSource;
		var $link_action;
		var $emailAddress;
		var $attachments = array();
		
		/* to support Email 2.0 */
		var $isDuplicate;
		var $uid;
		var $to;
		var $flagged;
		var $answered;
		var $seen;
		var $draft;
		var $relationshipMap = array(
			'Contacts' => 'emails_contacts_rel', 
			'Accounts' => 'emails_accounts_rel', 
			'Leads' => 'emails_leads_rel', 
			'Users' => 'emails_users_rel', 
			'Prospects' => 'emails_prospects_rel');
		
		/* public */
		var $et; // EmailUI object
		// prefix to use when importing inlinge images in emails
		public $imagePrefix;
		
		/**
     * Used for keeping track of field defs that have been modified
     *
     * @var array
     */
		public $modifiedFieldDefs = array();
		
		public $attachment_image;
		
		/**
	 * sole constructor
	 */
		function Email()
		{
			global $current_user;
			$this->cachePath = sugar_cached('modules/Emails');
			parent::SugarBean();
			
			$this->emailAddress = new SugarEmailAddress();
			
			$this->imagePrefix = rtrim($GLOBALS['sugar_config']['site_url'], "/") . "/cache/images/";
		}
		
		//TODO (MT CHECK): Assignment to property of potential multi-tenant object!
		function email2init()
		{
			require_once ('modules/Emails/EmailUI.php');
			$this->et = new EmailUI();
		}
		
		function bean_implements($interface)
		{
			switch ($interface) {
				case 'ACL':
					return true;
				default:
					return false;
			}
			
		}
		
		/**
	 * Presaves one attachment for new email 2.0 spec
	 * DOES NOT CREATE A NOTE
	 * @return string ID of note associated with the attachment
	 */
		public function email2saveAttachment()
		{
			$email_uploads = "modules/Emails/{$GLOBALS['current_user']->id}";
			$upload = new UploadFile('email_attachment');
			if (!$upload->confirm_upload()) {
				$err = $upload->get_upload_error();
				if ($err) {
					$GLOBALS['log']->error("Email Attachment could not be attached due to error: $err");
				}
				return array();
			}
			
			$guid = create_guid();
			$fileName = $upload->create_stored_filename();
			$GLOBALS['log']->debug("Email Attachment [$fileName]");
			if ($upload->final_move($guid)) {
				copy("upload://$guid", sugar_cached("$email_uploads/$guid"));
				return array(
					'guid' => $guid, 
					'name' => $GLOBALS['db']->quote($fileName), 
					'nameForDisplay' => $fileName);
			} else {
				$GLOBALS['log']->debug("Email Attachment [$fileName] could not be moved to upload dir");
				return array();
			}
		}
		
		function safeAttachmentName($filename)
		{
			global $sugar_config;
			$badExtension = false;
			//get position of last "." in file name
			$file_ext_beg = strrpos($filename, ".");
			$file_ext = "";
			
			//get file extension
			if ($file_ext_beg !== false) {
				$file_ext = substr($filename, $file_ext_beg + 1);
			}
			
			//check to see if this is a file with extension located in "badext"
			foreach ($sugar_config['upload_badext'] as $badExt) {
				if (strtolower($file_ext) == strtolower($badExt)) {
					//if found, then append with .txt and break out of lookup
					$filename = $filename . ".txt";
					$badExtension = true;
					break; // no need to look for more
				}
				// if
			}
			// foreach
			
			return $badExtension;
		}
		// fn
		
		/**
	 * takes output from email 2.0 to/cc/bcc fields and returns appropriate arrays for usage by PHPMailer
	 * @param string addresses
	 * @return array
	 */
		function email2ParseAddresses($addresses)
		{
			$addresses = from_html($addresses);
			$addresses = $this->et->unifyEmailString($addresses);
			
			$pattern = '/@.*,/U';
			preg_match_all($pattern, $addresses, $matchs);
			if (!empty($matchs[0])) {
				$total = $matchs[0];
				foreach ($total as $match) {
					$convertedPattern = str_replace(',', '::;::', $match);
					$addresses = str_replace($match, $convertedPattern, $addresses);
				}
				//foreach
			}
			
			$exAddr = explode("::;::", $addresses);
			
			$ret = array();
			$clean = array("<", ">");
			$dirty = array("&lt;", "&gt;");
			
			foreach ($exAddr as $addr) {
				$name = '';
				
				$addr = str_replace($dirty, $clean, $addr);
				
				if ((strpos($addr, "<") === false) && (strpos($addr, ">") === false)) {
					$address = $addr;
				} else {
					$address = substr($addr, strpos($addr, "<") + 1, strpos($addr, ">") - 1 - strpos($addr, "<"));
					$name = substr($addr, 0, strpos($addr, "<"));
				}
				
				$addrTemp = array();
				$addrTemp['email'] = trim($address);
				$addrTemp['display'] = trim($name);
				$ret[] = $addrTemp;
			}
			
			return $ret;
		}
		
		/**
	 * takes output from email 2.0 to/cc/bcc fields and returns appropriate arrays for usage by PHPMailer
	 * @param string addresses
	 * @return array
	 */
		function email2ParseAddressesForAddressesOnly($addresses)
		{
			$addresses = from_html($addresses);
			$pattern = '/@.*,/U';
			preg_match_all($pattern, $addresses, $matchs);
			if (!empty($matchs[0])) {
				$total = $matchs[0];
				foreach ($total as $match) {
					$convertedPattern = str_replace(',', '::;::', $match);
					$addresses = str_replace($match, $convertedPattern, $addresses);
				}
				//foreach
			}
			
			$exAddr = explode("::;::", $addresses);
			
			$ret = array();
			$clean = array("<", ">");
			$dirty = array("&lt;", "&gt;");
			
			foreach ($exAddr as $addr) {
				$name = '';
				
				$addr = str_replace($dirty, $clean, $addr);
				
				if (strpos($addr, "<") && strpos($addr, ">")) {
					$address = substr($addr, strpos($addr, "<") + 1, strpos($addr, ">") - 1 - strpos($addr, "<"));
				} else {
					$address = $addr;
				}
				
				$ret[] = trim($address);
			}
			
			return $ret;
		}
		
		/**
	 * Determines MIME-type encoding as possible.
	 * @param string $fileLocation relative path to file
	 * @return string MIME-type
	 */
		function email2GetMime($fileLocation)
		{
			if (!is_readable($fileLocation)) {
				return 'application/octet-stream';
			}
			if (function_exists('mime_content_type')) {
				$mime = mime_content_type($fileLocation);
			} elseif (function_exists('ext2mime')) {
				$mime = ext2mime($fileLocation);
			} else {
				$mime = 'application/octet-stream';
			}
			return $mime;
		}
		
		
		function sendEmailTest($mailserver_url, $port, $ssltls, $smtp_auth_req, $smtp_username, $smtppassword, $fromaddress, $toaddress, $mail_sendtype = 'smtp', $fromname = '')
		{
			global $current_user, $app_strings;
			$mod_strings = return_module_language($GLOBALS['current_language'], 'Emails'); //Called from EmailMan as well.
			$mail = new SugarPHPMailer();
			$mail->Mailer = strtolower($mail_sendtype);
			if ($mail->Mailer == 'smtp') {
				$mail->Host = $mailserver_url;
				$mail->Port = $port;
				if (isset($ssltls) && !empty($ssltls)) {
					$mail->protocol = "ssl://";
					if ($ssltls == 1) {
						$mail->SMTPSecure = 'ssl';
					}
					// if
					if ($ssltls == 2) {
						$mail->SMTPSecure = 'tls';
					}
					// if
				} else {
					$mail->protocol = "tcp://";
				}
				if ($smtp_auth_req) {
					$mail->SMTPAuth = TRUE;
					$mail->Username = $smtp_username;
					$mail->Password = $smtppassword;
				}
			} else 
				$mail->Mailer = 'sendmail';
			
			$mail->Subject = from_html($mod_strings['LBL_TEST_EMAIL_SUBJECT']);
			$mail->From = $fromaddress;
			
			if ($fromname != '') {
				$mail->FromName = html_entity_decode($fromname, ENT_QUOTES);
			} else {
				$mail->FromName = $current_user->name;
			}
			
			$mail->Sender = $mail->From;
			$mail->AddAddress($toaddress);
			$mail->Body = $mod_strings['LBL_TEST_EMAIL_BODY'];
			
			$return = array();
			
			if (!$mail->Send()) {
				ob_clean();
				$return['status'] = false;
				$return['errorMessage'] = $app_strings['LBL_EMAIL_ERROR_PREPEND'] . $mail->ErrorInfo;
				return $return;
			}
			// if
			$return['status'] = true;
			return $return;
		}
		// fn
		
		function decodeDuringSend($htmlData)
		{
			$htmlData = str_replace("sugarLessThan", "&lt;", $htmlData);
			$htmlData = str_replace("sugarGreaterThan", "&gt;", $htmlData);
			return $htmlData;
		}
		
		/**
	 * Returns true or false if this email is a draft.
	 *
	 * @param array $request
	 * @return bool True indicates this email is a draft.
	 */
		function isDraftEmail($request)
		{
			return (isset($request['saveDraft']) || ($this->type == 'draft' && $this->status == 'draft'));
		}
		
		/**
	 * Sends Email for Email 2.0
	 */
		function email2Send($request)
		{
			//TODO (MT): specify type of proc. parameters
			global $mod_strings;
			global $app_strings;
			global $current_user;
			global $sugar_config;
			global $locale;
			global $timedate;
			global $beanList;
			global $beanFiles;
			$OBCharset = $locale->getPrecedentPreference('default_email_charset');
			
			/**********************************************************************
		 * Sugar Email PREP
		 */
			/* preset GUID */
			
			$orignialId = "";
			if (!empty($this->id)) {
				$orignialId = $this->id;
			}
			// if
			
			if (empty($this->id)) {
				$this->id = create_guid();
				$this->new_with_id = true;
			}
			
			/* satisfy basic HTML email requirements */
			$this->name = $request['sendSubject'];
			$this->description_html = '&lt;html&gt;&lt;body&gt;' . $request['sendDescription'] . '&lt;/body&gt;&lt;/html&gt;';
			
			/**********************************************************************
		 * PHPMAILER PREP
		 */
			$mail = new SugarPHPMailer();
			$mail = $this->setMailer($mail, '', $_REQUEST['fromAccount']);
			if (empty($mail->Host) && !$this->isDraftEmail($request)) {
				$this->status = 'send_error';
				
				if ($mail->oe->type == 'system') 
					echo ($app_strings['LBL_EMAIL_ERROR_PREPEND'] . $app_strings['LBL_EMAIL_INVALID_SYSTEM_OUTBOUND']);
				 else 
					echo ($app_strings['LBL_EMAIL_ERROR_PREPEND'] . $app_strings['LBL_EMAIL_INVALID_PERSONAL_OUTBOUND']);
				
				return false;
			}
			
			$subject = $this->name;
			$mail->Subject = from_html($this->name);
			
			// work-around legacy code in SugarPHPMailer
			if ($_REQUEST['setEditor'] == 1) {
				$_REQUEST['description_html'] = $_REQUEST['sendDescription'];
				$this->description_html = $_REQUEST['description_html'];
			} else {
				$this->description_html = '';
				$this->description = $_REQUEST['sendDescription'];
			}
			// end work-around
			
			if ($this->isDraftEmail($request)) {
				if ($this->type != 'draft' && $this->status != 'draft') {
					$this->id = create_guid();
					$this->new_with_id = true;
					$this->date_entered = "";
				}
				// if
				$q1 = "update emails_email_addr_rel set deleted = 1 WHERE email_id = '{$this->id}'";
				$r1 = $this->db->query($q1);
			}
			// if
			
			if (isset($request['saveDraft'])) {
				$this->type = 'draft';
				$this->status = 'draft';
				$forceSave = true;
			} else {
				/* Apply Email Templates */
				// do not parse email templates if the email is being saved as draft....
				$toAddresses = $this->email2ParseAddresses($_REQUEST['sendTo']);
				$sea = new SugarEmailAddress();
				$object_arr = array();
				
				if (isset($_REQUEST['parent_type']) && !empty($_REQUEST['parent_type']) && isset($_REQUEST['parent_id']) && !empty($_REQUEST['parent_id']) && ($_REQUEST['parent_type'] == 'Accounts' || $_REQUEST['parent_type'] == 'Contacts' || $_REQUEST['parent_type'] == 'Leads' || $_REQUEST['parent_type'] == 'Users' || $_REQUEST['parent_type'] == 'Prospects')) {
					if (isset($beanList[$_REQUEST['parent_type']]) && !empty($beanList[$_REQUEST['parent_type']])) {
						$className = $beanList[$_REQUEST['parent_type']];
						if (isset($beanFiles[$className]) && !empty($beanFiles[$className])) {
							if (!class_exists($className)) {
								require_once ($beanFiles[$className]);
							}
							$bean = new $className();
							$bean->retrieve($_REQUEST['parent_id']);
							$object_arr[$bean->module_dir] = $bean->id;
						}
						// if
					}
					// if
				}
				foreach ($toAddresses as $addrMeta) {
					$addr = $addrMeta['email'];
					$beans = $sea->getBeansByEmailAddress($addr);
					foreach ($beans as $bean) {
						if (!isset($object_arr[$bean->module_dir])) {
							$object_arr[$bean->module_dir] = $bean->id;
						}
					}
				}
				
				/* template parsing */
				if (empty($object_arr)) {
					$object_arr = array('Contacts' => '123');
				}
				$object_arr['Users'] = $current_user->id;
				$this->description_html = EmailTemplate::parse_template($this->description_html, $object_arr);
				//TODO (MT CHECK Code): ensure usage of tenantID
				$this->name = EmailTemplate::parse_template($this->name, $object_arr);
				//TODO (MT CHECK Code): ensure usage of tenantID
				$this->description = EmailTemplate::parse_template($this->description, $object_arr);
				//TODO (MT CHECK Code): ensure usage of tenantID
				$this->description = html_entity_decode($this->description, ENT_COMPAT, 'UTF-8');
				if ($this->type != 'draft' && $this->status != 'draft') {
					$this->id = create_guid();
					$this->date_entered = "";
					$this->new_with_id = true;
					$this->type = 'out';
					$this->status = 'sent';
				}
			}
			
			if (isset($_REQUEST['parent_type']) && empty($_REQUEST['parent_type']) && isset($_REQUEST['parent_id']) && empty($_REQUEST['parent_id'])) {
				$this->parent_id = "";
				$this->parent_type = "";
			}
			// if
			
			
			$mail->Subject = $this->name;
			$mail = $this->handleBody($mail);
			$mail->Subject = $this->name;
			$this->description_html = from_html($this->description_html);
			$this->description_html = $this->decodeDuringSend($this->description_html);
			$this->description = $this->decodeDuringSend($this->description);
			
			/* from account */
			$replyToAddress = $current_user->emailAddress->getReplyToAddress($current_user, true);
			$replyToName = "";
			if (empty($request['fromAccount'])) {
				$defaults = $current_user->getPreferredEmail();
				$mail->From = $defaults['email'];
				$mail->FromName = $defaults['name'];
				$replyToName = $mail->FromName;
				//$replyToAddress = $current_user->emailAddress->getReplyToAddress($current_user);
			} else {
				// passed -> user -> system default
				$ie = new InboundEmail();
				$ie->retrieve($request['fromAccount']);
				$storedOptions = unserialize(base64_decode($ie->stored_options));
				$fromName = "";
				$fromAddress = "";
				$replyToName = "";
				//$replyToAddress = "";
				if (!empty($storedOptions)) {
					$fromAddress = $storedOptions['from_addr'];
					$fromName = from_html($storedOptions['from_name']);
					$replyToAddress = (isset($storedOptions['reply_to_addr']) ? $storedOptions['reply_to_addr'] : "");
					$replyToName = (isset($storedOptions['reply_to_name']) ? from_html($storedOptions['reply_to_name']) : "");
				}
				// if
				$defaults = $current_user->getPreferredEmail();
				// Personal Account doesn't have reply To Name and Reply To Address. So add those columns on UI
				// After adding remove below code
				
				// code to remove
				if ($ie->is_personal) {
					if (empty($replyToAddress)) {
						$replyToAddress = $current_user->emailAddress->getReplyToAddress($current_user, true);
					}
					// if
					if (empty($replyToName)) {
						$replyToName = $defaults['name'];
					}
					// if
					//Personal accounts can have a reply_address, which should
					//overwrite the users set default.
					if (!empty($storedOptions['reply_to_addr'])) 
						$replyToAddress = $storedOptions['reply_to_addr'];
					
				}
				// end of code to remove
				$mail->From = (!empty($fromAddress)) ? $fromAddress : $defaults['email'];
				$mail->FromName = (!empty($fromName)) ? $fromName : $defaults['name'];
				$replyToName = (!empty($replyToName)) ? $replyToName : $mail->FromName;
			}
			
			$mail->Sender = $mail->From; /* set Return-Path field in header to reduce spam score in emails sent via Sugar's Email module */
			
			if (!empty($replyToAddress)) {
				$mail->AddReplyTo($replyToAddress, $locale->translateCharsetMIME(trim($replyToName), 'UTF-8', $OBCharset));
			} else {
				$mail->AddReplyTo($mail->From, $locale->translateCharsetMIME(trim($mail->FromName), 'UTF-8', $OBCharset));
			}
			// else
			$emailAddressCollection = array(); // used in linking to beans below
			// handle to/cc/bcc
			foreach ($this->email2ParseAddresses($request['sendTo']) as $addr_arr) {
				if (empty($addr_arr['email'])) 
					continue;
				
				if (empty($addr_arr['display'])) {
					$mail->AddAddress($addr_arr['email'], "");
				} else {
					$mail->AddAddress($addr_arr['email'], $locale->translateCharsetMIME(trim($addr_arr['display']), 'UTF-8', $OBCharset));
				}
				$emailAddressCollection[] = $addr_arr['email'];
			}
			foreach ($this->email2ParseAddresses($request['sendCc']) as $addr_arr) {
				if (empty($addr_arr['email'])) 
					continue;
				
				if (empty($addr_arr['display'])) {
					$mail->AddCC($addr_arr['email'], "");
				} else {
					$mail->AddCC($addr_arr['email'], $locale->translateCharsetMIME(trim($addr_arr['display']), 'UTF-8', $OBCharset));
				}
				$emailAddressCollection[] = $addr_arr['email'];
			}
			
			foreach ($this->email2ParseAddresses($request['sendBcc']) as $addr_arr) {
				if (empty($addr_arr['email'])) 
					continue;
				
				if (empty($addr_arr['display'])) {
					$mail->AddBCC($addr_arr['email'], "");
				} else {
					$mail->AddBCC($addr_arr['email'], $locale->translateCharsetMIME(trim($addr_arr['display']), 'UTF-8', $OBCharset));
				}
				$emailAddressCollection[] = $addr_arr['email'];
			}
			
			
			/* parse remove attachments array */
			$removeAttachments = array();
			if (!empty($request['templateAttachmentsRemove'])) {
				$exRemove = explode("::", $request['templateAttachmentsRemove']);
				
				foreach ($exRemove as $file) {
					$removeAttachments = substr($file, 0, 36);
				}
			}
			
			/* handle attachments */
			if (!empty($request['attachments'])) {
				$exAttachments = explode("::", $request['attachments']);
				
				foreach ($exAttachments as $file) {
					$file = trim(from_html($file));
					$file = str_replace("\\", "", $file);
					if (!empty($file)) {
						//$fileLocation = $this->et->userCacheDir."/{$file}";
						$fileGUID = preg_replace('/[^a-z0-9\-]/', "", substr($file, 0, 36));
						$fileLocation = $this->et->userCacheDir . "/{$fileGUID}";
						$filename = substr($file, 36, strlen($file)); // strip GUID	for PHPMailer class to name outbound file
						
						$mail->AddAttachment($fileLocation, $filename, 'base64', $this->email2GetMime($fileLocation));
						//$mail->AddAttachment($fileLocation, $filename, 'base64');
						
						// only save attachments if we're archiving or drafting
						if ((($this->type == 'draft') && !empty($this->id)) || (isset($request['saveToSugar']) && $request['saveToSugar'] == 1)) {
							$note = new Note();
							$note->id = create_guid();
							$note->new_with_id = true; // duplicating the note with files
							$note->parent_id = $this->id;
							$note->parent_type = $this->module_dir;
							$note->name = $filename;
							$note->filename = $filename;
							$note->file_mime_type = $this->email2GetMime($fileLocation);
							$dest = "upload://{$note->id}";
							if (!copy($fileLocation, $dest)) {
								$GLOBALS['log']->debug("EMAIL 2.0: could not copy attachment file to $fileLocation => $dest");
							}
							
							$note->save();
						}
					}
				}
			}
			
			/* handle sugar documents */
			if (!empty($request['documents'])) {
				$exDocs = explode("::", $request['documents']);
				
				foreach ($exDocs as $docId) {
					$docId = trim($docId);
					if (!empty($docId)) {
						$doc = new Document();
						$docRev = new DocumentRevision();
						$doc->retrieve($docId);
						$docRev->retrieve($doc->document_revision_id);
						
						$filename = $docRev->filename;
						$docGUID = preg_replace('/[^a-z0-9\-]/', "", $docRev->id);
						$fileLocation = "upload://{$docGUID}";
						$mime_type = $docRev->file_mime_type;
						$mail->AddAttachment($fileLocation, $locale->translateCharsetMIME(trim($filename), 'UTF-8', $OBCharset), 'base64', $mime_type);
						
						// only save attachments if we're archiving or drafting
						if ((($this->type == 'draft') && !empty($this->id)) || (isset($request['saveToSugar']) && $request['saveToSugar'] == 1)) {
							$note = new Note();
							$note->id = create_guid();
							$note->new_with_id = true; // duplicating the note with files
							$note->parent_id = $this->id;
							$note->parent_type = $this->module_dir;
							$note->name = $filename;
							$note->filename = $filename;
							$note->file_mime_type = $mime_type;
							$dest = "upload://{$note->id}";
							if (!copy($fileLocation, $dest)) {
								$GLOBALS['log']->debug("EMAIL 2.0: could not copy SugarDocument revision file $fileLocation => $dest");
							}
							
							$note->save();
						}
					}
				}
			}
			
			/* handle template attachments */
			if (!empty($request['templateAttachments'])) {
				
				$exNotes = explode("::", $request['templateAttachments']);
				foreach ($exNotes as $noteId) {
					$noteId = trim($noteId);
					if (!empty($noteId)) {
						$note = new Note();
						$note->retrieve($noteId);
						if (!empty($note->id)) {
							$filename = $note->filename;
							$noteGUID = preg_replace('/[^a-z0-9\-]/', "", $note->id);
							$fileLocation = "upload://{$noteGUID}";
							$mime_type = $note->file_mime_type;
							if (!$note->embed_flag) {
								$mail->AddAttachment($fileLocation, $filename, 'base64', $mime_type);
								// only save attachments if we're archiving or drafting
								if ((($this->type == 'draft') && !empty($this->id)) || (isset($request['saveToSugar']) && $request['saveToSugar'] == 1)) {
									
									if ($note->parent_id != $this->id) 
										$this->saveTempNoteAttachments($filename, $fileLocation, $mime_type);
								}
								// if
								
							}
							// if
						} else {
							//$fileLocation = $this->et->userCacheDir."/{$file}";
							$fileGUID = preg_replace('/[^a-z0-9\-]/', "", substr($noteId, 0, 36));
							$fileLocation = $this->et->userCacheDir . "/{$fileGUID}";
							//$fileLocation = $this->et->userCacheDir."/{$noteId}";
							$filename = substr($noteId, 36, strlen($noteId)); // strip GUID	for PHPMailer class to name outbound file
							
							$mail->AddAttachment($fileLocation, $locale->translateCharsetMIME(trim($filename), 'UTF-8', $OBCharset), 'base64', $this->email2GetMime($fileLocation));
							
							//If we are saving an email we were going to forward we need to save the attachments as well.
							if ((($this->type == 'draft') && !empty($this->id)) 
								|| (isset($request['saveToSugar']) && $request['saveToSugar'] == 1)) {
								$mimeType = $this->email2GetMime($fileLocation);
								$this->saveTempNoteAttachments($filename, $fileLocation, $mimeType);
							}
							// if
						}
					}
				}
			}
			
			
			
			/**********************************************************************
		 * Final Touches
		 */
			/* save email to sugar? */
			$forceSave = false;
			
			if ($this->type == 'draft' && !isset($request['saveDraft'])) {
				// sending a draft email
				$this->type = 'out';
				$this->status = 'sent';
				$forceSave = true;
			} elseif (isset($request['saveDraft'])) {
				$this->type = 'draft';
				$this->status = 'draft';
				$forceSave = true;
			}
			
			/**********************************************************************
         * SEND EMAIL (finally!)
         */
			$mailSent = false;
			if ($this->type != 'draft') {
				$mail->prepForOutbound();
				$mail->Body = $this->decodeDuringSend($mail->Body);
				$mail->AltBody = $this->decodeDuringSend($mail->AltBody);
				if (!$mail->Send()) {
					$this->status = 'send_error';
					ob_clean();
					echo ($app_strings['LBL_EMAIL_ERROR_PREPEND'] . $mail->ErrorInfo);
					return false;
				}
			}
			
			if ((!(empty($orignialId) || isset($request['saveDraft']) || ($this->type == 'draft' && $this->status == 'draft'))) && (($_REQUEST['composeType'] == 'reply') || ($_REQUEST['composeType'] == 'replyAll') || ($_REQUEST['composeType'] == 'replyCase')) && ($orignialId != $this->id)) {
				$originalEmail = new Email();

				//TODO (MT EXTEND Code): retrieve and use the TENANT_ID
				$originalEmail->retrieve($orignialId);
				$originalEmail->reply_to_status = 1;
				$originalEmail->save();
				$this->reply_to_status = 0;
			}
			// if
			
			if ($_REQUEST['composeType'] == 'reply' || $_REQUEST['composeType'] == 'replyCase') {
				if (isset($_REQUEST['ieId']) && isset($_REQUEST['mbox'])) {
					$emailFromIe = new InboundEmail();
					$emailFromIe->retrieve($_REQUEST['ieId']);
					$emailFromIe->mailbox = $_REQUEST['mbox'];
					if (isset($emailFromIe->id) && $emailFromIe->is_personal) {
						if ($emailFromIe->isPop3Protocol()) {
							$emailFromIe->mark_answered($this->uid, 'pop3');
						} elseif ($emailFromIe->connectMailserver() == 'true') {
							$emailFromIe->markEmails($this->uid, 'answered');
							$emailFromIe->mark_answered($this->uid);
						}
					}
				}
			}
			
			
			if ($forceSave || $this->type == 'draft' || (isset($request['saveToSugar']) && $request['saveToSugar'] == 1)) {
				
				// saving a draft OR saving a sent email
				$decodedFromName = mb_decode_mimeheader($mail->FromName);
				$this->from_addr = "{$decodedFromName} <{$mail->From}>";
				$this->from_addr_name = $this->from_addr;
				$this->to_addrs = $_REQUEST['sendTo'];
				$this->to_addrs_names = $_REQUEST['sendTo'];
				$this->cc_addrs = $_REQUEST['sendCc'];
				$this->cc_addrs_names = $_REQUEST['sendCc'];
				$this->bcc_addrs = $_REQUEST['sendBcc'];
				$this->bcc_addrs_names = $_REQUEST['sendBcc'];
				$this->assigned_user_id = $current_user->id;
				
				$this->date_sent = $timedate->now();
				///////////////////////////////////////////////////////////////////
				////	LINK EMAIL TO SUGARBEANS BASED ON EMAIL ADDY
				
				if (isset($_REQUEST['parent_type']) && !empty($_REQUEST['parent_type']) && isset($_REQUEST['parent_id']) && !empty($_REQUEST['parent_id'])) {
					$this->parent_id = $_REQUEST['parent_id'];
					$this->parent_type = $_REQUEST['parent_type'];
					$q = "SELECT count(*) c FROM emails_beans WHERE  email_id = '{$this->id}' AND bean_id = '{$_REQUEST['parent_id']}' AND bean_module = '{$_REQUEST['parent_type']}'";
					$r = $this->db->query($q);
					$a = $this->db->fetchByAssoc($r);
					if ($a['c'] <= 0) {
						if (isset($beanList[$_REQUEST['parent_type']]) && !empty($beanList[$_REQUEST['parent_type']])) {
							$className = $beanList[$_REQUEST['parent_type']];
							if (isset($beanFiles[$className]) && !empty($beanFiles[$className])) {
								if (!class_exists($className)) {
									require_once ($beanFiles[$className]);
								}
								$bean = new $className();
								$bean->retrieve($_REQUEST['parent_id']);
								if ($bean->load_relationship('emails')) {
									$bean->emails->add($this->id);
								}
								// if
								
							}
							// if
							
						}
						// if
						
					}
					// if
					
				} else {
					if (!class_exists('aCase')) {
						
					} else {
						$c = new aCase();
						if ($caseId = InboundEmail::getCaseIdFromCaseNumber($mail->Subject, $c)) {
							$c->retrieve($caseId);
							$c->load_relationship('emails');
							$c->emails->add($this->id);
							$this->parent_type = "Cases";
							$this->parent_id = $caseId;
						}
						// if
					}
					
				}
				// else
				
				////	LINK EMAIL TO SUGARBEANS BASED ON EMAIL ADDY
				///////////////////////////////////////////////////////////////////
				$this->save();
			}
			
			if (!empty($request['fromAccount'])) {
				if (isset($ie->id) && !$ie->isPop3Protocol() && $mail->oe->mail_smtptype != 'gmail') {
					$sentFolder = $ie->get_stored_options("sentFolder");
					if (!empty($sentFolder)) {
						$data = $mail->CreateHeader() . "\r\n" . $mail->CreateBody() . "\r\n";
						$ie->mailbox = $sentFolder;
						if ($ie->connectMailserver() == 'true') {
							$connectString = $ie->getConnectString($ie->getServiceString(), $ie->mailbox);
							$returnData = imap_append($ie->conn, $connectString, $data, "\\Seen");
							if (!$returnData) {
								$GLOBALS['log']->debug("could not copy email to {$ie->mailbox} for {$ie->name}");
							}
							// if
						} else {
							$GLOBALS['log']->debug("could not connect to mail serve for folder {$ie->mailbox} for {$ie->name}");
						}
						// else
					} else {
						$GLOBALS['log']->debug("could not copy email to {$ie->mailbox} sent folder as its empty");
					}
					// else
				}
				// if
			}
			// if
			return true;
		}
		// end email2send
		
		/**
     * Generates a config-specified separated name and addresses to be used in compose email screen for
     * contacts or leads from listview
     * By default, use comma, but allow for non-standard delimiters as specified in email_address_separator
     *
     * @param $module string module name
     * @param $idsArray array of record ids to get the email address for
     * @return string (config-specified) delimited list of email addresses
     */
		public function getNamePlusEmailAddressesForCompose($module, $idsArray)
		{
			global $locale;
			$result = array();
			
			foreach ($idsArray as $id) {
				// Load bean
				$bean = BeanFactory::getBean($module, $id);
				
				// Got a bean
				if (!empty($bean)) {
					// For CE, just get primary e-mail address
					$emailAddress = $bean->email1;
					
					
					// If we have an e-mail address loaded
					if (!empty($emailAddress)) {
						// Use bean name by default
						$fullName = $bean->name;
						
						// Depending on module, format the name
						if (in_array($module, array('Users', 'Employees'))) {
							$fullName = from_html($locale->getLocaleFormattedName($bean->first_name, $bean->last_name, 
								'', $bean->title));
						} else 
							if (SugarModule::get($module)->moduleImplements('Person')) {
								$fullName = from_html($locale->getLocaleFormattedName($bean->first_name, $bean->last_name, $bean->salutation, $bean->title));
							}
						
						// Make e-mail address in format "Name <@email>"
						$result[$bean->id] = $fullName . " <" . from_html($emailAddress) . ">";
					}
				}
			}
			
			// Broken out of method to facilitate unit testing
			return $this->_arrayToDelimitedString($result);
		}
		
		/**
     * @param Array $arr - list of strings
     * @return string the list of strings delimited by email_address_separator
     */
		function _arrayToDelimitedString($arr)
		{
			// bug 51804: outlook does not respect the correct email address separator (',') , so let
			// clients override the default.
			$separator = (isset($GLOBALS['sugar_config']['email_address_separator']) && 
				!empty($GLOBALS['sugar_config']['email_address_separator'])) ? $GLOBALS['sugar_config']['email_address_separator'] : 
				',';
			
			return join($separator, array_values($arr));
		}
		
		/**
	 * Overrides
	 */
		///////////////////////////////////////////////////////////////////////////
		////	SAVERS
		function save($check_notify = false)
		{
			//TODO (MT): specify type of proc. parameters
			global $current_user;
			
			if ($this->isDuplicate) {
				$GLOBALS['log']->debug("EMAIL - tried to save a duplicate Email record");
			} else {
				
				if (empty($this->id)) {
					$this->id = create_guid();
					$this->new_with_id = true;
				}
				$this->from_addr_name = $this->cleanEmails($this->from_addr_name);
				$this->to_addrs_names = $this->cleanEmails($this->to_addrs_names);
				$this->cc_addrs_names = $this->cleanEmails($this->cc_addrs_names);
				$this->bcc_addrs_names = $this->cleanEmails($this->bcc_addrs_names);
				$this->reply_to_addr = $this->cleanEmails($this->reply_to_addr);
				$this->description = SugarCleaner::cleanHtml($this->description);
				$this->description_html = SugarCleaner::cleanHtml($this->description_html, true);
				$this->raw_source = SugarCleaner::cleanHtml($this->raw_source, true);
				$this->saveEmailText();
				$this->saveEmailAddresses();
				
				$GLOBALS['log']->debug('-------------------------------> Email called save()');
				
				// handle legacy concatenation of date and time fields
				//Bug 39503 - SugarBean is not setting date_sent when seconds missing
				if (empty($this->date_sent)) {
					global $timedate;
					$date_sent_obj = $timedate->fromUser($timedate->merge_date_time($this->date_start, $this->time_start), $current_user);
					if (!empty($date_sent_obj) && ($date_sent_obj instanceof SugarDateTime)) {
						$this->date_sent = $date_sent_obj->asDb();
					}
				}
				
				parent::save($check_notify);
				
				if (!empty($this->parent_type) && !empty($this->parent_id)) {
					if (!empty($this->fetched_row) && !empty($this->fetched_row['parent_id']) && !empty($this->fetched_row['parent_type'])) {
						if ($this->fetched_row['parent_id'] != $this->parent_id || $this->fetched_row['parent_type'] != $this->parent_type) {
							$mod = strtolower($this->fetched_row['parent_type']);
							$rel = array_key_exists($mod, $this->field_defs) ? $mod : $mod . "_activities_emails"; //Custom modules rel name
							if ($this->load_relationship($rel)) {
								$this->$rel->delete($this->id, $this->fetched_row['parent_id']);
							}
						}
					}
					$mod = strtolower($this->parent_type);
					$rel = array_key_exists($mod, $this->field_defs) ? $mod : $mod . "_activities_emails"; //Custom modules rel name
					if ($this->load_relationship($rel)) {
						$this->$rel->add($this->parent_id);
					}
				}
			}
			$GLOBALS['log']->debug('-------------------------------> Email save() done');
		}
		
		/**
	 * Helper function to save temporary attachments assocaited to an email as note.
	 *
	 * @param string $filename
	 * @param string $fileLocation
	 * @param string $mimeType
	 */
		function saveTempNoteAttachments($filename, $fileLocation, $mimeType)
		{
			//TODO (MT): specify type of proc. parameters
			$tmpNote = new Note();
			$tmpNote->id = create_guid();
			$tmpNote->new_with_id = true;
			$tmpNote->parent_id = $this->id;
			$tmpNote->parent_type = $this->module_dir;
			$tmpNote->name = $filename;
			$tmpNote->filename = $filename;
			$tmpNote->file_mime_type = $mimeType;
			$noteFile = "upload://{$tmpNote->id}";
			if (!copy($fileLocation, $noteFile)) {
				$GLOBALS['log']->fatal("EMAIL 2.0: could not copy SugarDocument revision file $fileLocation => $noteFile");
			}
			$tmpNote->save();
		}
		/**
	 * Handles normalization of Email Addressess
	 */
		function saveEmailAddresses()
		{
			// from, single address
			$fromId = $this->emailAddress->getEmailGUID(from_html($this->from_addr));
			if (!empty($fromId)) {
				$this->linkEmailToAddress($fromId, 'from');
			}
			
			// to, multiple
			$replace = array(",", ";");
			$toaddrs = str_replace($replace, "::", from_html($this->to_addrs));
			$exToAddrs = explode("::", $toaddrs);
			
			if (!empty($exToAddrs)) {
				foreach ($exToAddrs as $toaddr) {
					$toaddr = trim($toaddr);
					if (!empty($toaddr)) {
						$toId = $this->emailAddress->getEmailGUID($toaddr);
						$this->linkEmailToAddress($toId, 'to');
					}
				}
			}
			
			// cc, multiple
			$ccAddrs = str_replace($replace, "::", from_html($this->cc_addrs));
			$exccAddrs = explode("::", $ccAddrs);
			
			if (!empty($exccAddrs)) {
				foreach ($exccAddrs as $ccAddr) {
					$ccAddr = trim($ccAddr);
					if (!empty($ccAddr)) {
						$ccId = $this->emailAddress->getEmailGUID($ccAddr);
						$this->linkEmailToAddress($ccId, 'cc');
					}
				}
			}
			
			// bcc, multiple
			$bccAddrs = str_replace($replace, "::", from_html($this->bcc_addrs));
			$exbccAddrs = explode("::", $bccAddrs);
			if (!empty($exbccAddrs)) {
				foreach ($exbccAddrs as $bccAddr) {
					$bccAddr = trim($bccAddr);
					if (!empty($bccAddr)) {
						$bccId = $this->emailAddress->getEmailGUID($bccAddr);
						$this->linkEmailToAddress($bccId, 'bcc');
					}
				}
			}
		}
		
		function linkEmailToAddress($id, $type)
		{
			// TODO: make this update?
			$q1 = "SELECT * FROM emails_email_addr_rel WHERE email_id = '{$this->id}' AND email_address_id = '{$id}' AND address_type = '{$type}' AND deleted = 0";
			$r1 = $this->db->query($q1);
			$a1 = $this->db->fetchByAssoc($r1);
			
			if (!empty($a1) && !empty($a1['id'])) {
				return $a1['id'];
			} else {
				$guid = create_guid();
				$q2 = "INSERT INTO emails_email_addr_rel VALUES('{$guid}', '{$this->id}', '{$type}', '{$id}', 0)";
				$r2 = $this->db->query($q2);
			}
			
			return $guid;
		}
		
		protected $email_to_text = array(
			"email_id" => "id", 
			"description" => "description", 
			"description_html" => "description_html", 
			"raw_source" => "raw_source", 
			"from_addr" => "from_addr_name", 
			"reply_to_addr" => "reply_to_addr", 
			"to_addrs" => "to_addrs_names", 
			"cc_addrs" => "cc_addrs_names", 
			"bcc_addrs" => "bcc_addrs_names");
		
		function cleanEmails($emails)
		{
			if (empty($emails)) 
				return '';
			$emails = str_replace(array(",", ";"), "::", from_html($emails));
			$addrs = explode("::", $emails);
			$res = array();
			foreach ($addrs as $addr) {
				$parts = $this->emailAddress->splitEmailAddress($addr);
				if (empty($parts["email"])) {
					continue;
				}
				if (!empty($parts["name"])) {
					$res[] = "{$parts['name']} <{$parts['email']}>";
				} else {
					$res[] .= $parts["email"];
				}
			}
			return join(", ", $res);
		}
		
		protected function saveEmailText()
		{
			$text = SugarModule::get("EmailText")->loadBean();
			foreach ($this->email_to_text as $textfield => $mailfield) {
				$text->$textfield = $this->$mailfield;
			}
			$text->email_id = $this->id;
			if (!$this->new_with_id) {
				$this->db->update($text);
			} else {
				$this->db->insert($text);
			}
		}
		
		///////////////////////////////////////////////////////////////////////////
		////	RETRIEVERS
		function retrieve($id, $encoded = true, $deleted = true)
		{
			// cn: bug 11915, return SugarBean's retrieve() call bean instead of $this
			$ret = parent::retrieve($id, $encoded, $deleted);
			
			if ($ret) {
				$ret->retrieveEmailText();
				//$ret->raw_source = SugarCleaner::cleanHtml($ret->raw_source);
				$ret->description = to_html($ret->description);
				//$ret->description_html = SugarCleaner::cleanHtml($ret->description_html);
				$ret->retrieveEmailAddresses();
				
				$ret->date_start = '';
				$ret->time_start = '';
				$dateSent = explode(' ', $ret->date_sent);
				if (!empty($dateSent)) {
					$ret->date_start = $dateSent[0];
					if (isset($dateSent[1])) 
						$ret->time_start = $dateSent[1];
				}
				// for Email 2.0
				foreach ($ret as $k => $v) {
					$this->$k = $v;
				}
			}
			return $ret;
		}
		
		
		/**
	 * Retrieves email addresses from GUIDs
	 */
		function retrieveEmailAddresses()
		{
			$return = array();
			
			$q = 
				"SELECT email_address, address_type
				FROM emails_email_addr_rel eam
				JOIN email_addresses ea ON ea.id = eam.email_address_id
				WHERE eam.email_id = '{$this->id}' AND eam.deleted=0";
			$r = $this->db->query($q);
			
			while ($a = $this->db->fetchByAssoc($r)) {
				if (!isset($return[$a['address_type']])) {
					$return[$a['address_type']] = array();
				}
				$return[$a['address_type']][] = $a['email_address'];
			}
			
			if (count($return) > 0) {
				if (isset($return['from'])) {
					$this->from_addr = implode(", ", $return['from']);
				}
				if (isset($return['to'])) {
					$this->to_addrs = implode(", ", $return['to']);
				}
				if (isset($return['cc'])) {
					$this->cc_addrs = implode(", ", $return['cc']);
				}
				if (isset($return['bcc'])) {
					$this->bcc_addrs = implode(", ", $return['bcc']);
				}
			}
		}
		
		/**
	 * Handles longtext fields
	 */
		function retrieveEmailText()
		{
			$q = "SELECT from_addr, reply_to_addr, to_addrs, cc_addrs, bcc_addrs, description, description_html, raw_source FROM emails_text WHERE email_id = '{$this->id}'";
			$r = $this->db->query($q);
			$a = $this->db->fetchByAssoc($r, false);
			
			$this->description = $a['description'];
			$this->description_html = $a['description_html'];
			$this->raw_source = $a['raw_source'];
			$this->from_addr_name = $a['from_addr'];
			$this->reply_to_addr = $a['reply_to_addr'];
			$this->to_addrs_names = $a['to_addrs'];
			$this->cc_addrs_names = $a['cc_addrs'];
			$this->bcc_addrs_names = $a['bcc_addrs'];
		}
		
		function delete($id = '')
		{
			//TODO (MT): specify type of proc. parameters
			if (empty($id)) 
				$id = $this->id;
			
			$id = $this->db->quote($id);
			
			$q = "UPDATE emails SET deleted = 1 WHERE id = '{$id}'";
			$qt = "UPDATE emails_text SET deleted = 1 WHERE email_id = '{$id}'";
			$qf = "UPDATE folders_rel SET deleted = 1 WHERE polymorphic_id = '{$id}' AND polymorphic_module = 'Emails'";
			$qn = "UPDATE notes SET deleted = 1 WHERE parent_id = '{$id}' AND parent_type = 'Emails'";
			$this->db->query($q);
			$this->db->query($qt);
			$this->db->query($qf);
			$this->db->query($qn);
		}
		
		/**
	 * creates the standard "Forward" info at the top of the forwarded message
	 * @return string
	 */
		function getForwardHeader()
		{
			global $mod_strings;
			global $current_user;
			
			//$from = str_replace(array("&gt;","&lt;"), array(")","("), $this->from_name);
			$from = to_html($this->from_name);
			$subject = to_html($this->name);
			$ret = "<br /><br />";
			$ret .= $this->replyDelimiter . "{$mod_strings['LBL_FROM']} {$from}<br />";
			$ret .= $this->replyDelimiter . "{$mod_strings['LBL_DATE_SENT']} {$this->date_sent}<br />";
			$ret .= $this->replyDelimiter . "{$mod_strings['LBL_TO']} {$this->to_addrs}<br />";
			$ret .= $this->replyDelimiter . "{$mod_strings['LBL_CC']} {$this->cc_addrs}<br />";
			$ret .= $this->replyDelimiter . "{$mod_strings['LBL_SUBJECT']} {$subject}<br />";
			$ret .= $this->replyDelimiter . "<br />";
			
			return $ret;
			//return from_html($ret);
		}
		
		/**
     * retrieves Notes that belong to this Email and stuffs them into the "attachments" attribute
     */
		function getNotes($id, $duplicate = false)
		{
			//TODO (MT): specify type of proc. parameters
			if (!class_exists('Note')) {
				
			}
			
			$exRemoved = array();
			if (isset($_REQUEST['removeAttachment'])) {
				$exRemoved = explode('::', $_REQUEST['removeAttachment']);
			}
			
			$noteArray = array();
			$q = "SELECT id FROM notes WHERE parent_id = '" . $id . "'";
			$r = $this->db->query($q);
			
			while ($a = $this->db->fetchByAssoc($r)) {
				if (!in_array($a['id'], $exRemoved)) {
					$note = new Note();
					$note->retrieve($a['id']);
					
					// duplicate actual file when creating forwards
					if ($duplicate) {
						if (!class_exists('UploadFile')) {
							require_once ('include/upload_file.php');
						}
						// save a brand new Note
						$noteDupe->id = create_guid();
						$noteDupe->new_with_id = true;
						$noteDupe->parent_id = $this->id;
						$noteDupe->parent_type = $this->module_dir;
						
						$noteFile = new UploadFile();
						$noteFile->duplicate_file($a['id'], $note->id, $note->filename);
						
						$note->save();
					}
					// add Note to attachments array
					$this->attachments[] = $note;
				}
			}
		}
		
		/**
	 * creates the standard "Reply" info at the top of the forwarded message
	 * @return string
	 */
		function getReplyHeader()
		{
			global $mod_strings;
			global $current_user;
			
			$from = str_replace(array("&gt;", "&lt;", ">", "<"), array(")", "(", ")", "("), $this->from_name);
			$ret = "<br>{$mod_strings['LBL_REPLY_HEADER_1']} {$this->date_start}, {$this->time_start}, {$from} {$mod_strings['LBL_REPLY_HEADER_2']}";
			
			return from_html($ret);
		}
		
		/**
	 * Quotes plain-text email text
	 * @param string $text
	 * @return string
	 */
		function quotePlainTextEmail($text)
		{
			$quoted = "\n";
			
			// plain-text
			$desc = nl2br(trim($text));
			$exDesc = explode('<br />', $desc);
			
			foreach ($exDesc as $k => $line) {
				$quoted .= '> ' . trim($line) . "\r";
			}
			
			return $quoted;
		}
		
		/**
	 * "quotes" (i.e., "> my text yadda" the HTML part of an email
	 * @param string $text HTML text to quote
	 * @return string
	 */
		function quoteHtmlEmail($text)
		{
			$text = trim(from_html($text));
			
			if (empty($text)) {
				return '';
			}
			$out = "<div style='border-left:1px solid #00c; padding:5px; margin-left:10px;'>{$text}</div>";
			
			return $out;
		}
		
		/**
	 * "quotes" (i.e., "> my text yadda" the HTML part of an email
	 * @param string $text HTML text to quote
	 * @return string
	 */
		function quoteHtmlEmailForNewEmailUI($text)
		{
			$text = trim($text);
			
			if (empty($text)) {
				return '';
			}
			$text = str_replace("\n", "<BR/>", $text);
			$out = "<div style='border-left:1px solid #00c; padding:5px; margin-left:10px;'>{$text}</div>";
			
			return $out;
		}
		
		/**
	 * Ensures that the user is able to send outbound emails
	 */
		function check_email_settings()
		{
			global $current_user;
			
			$mail_fromaddress = $current_user->emailAddress->getPrimaryAddress($current_user);
			$replyToName = $current_user->getPreference('mail_fromname');
			$mail_fromname = (!empty($replyToName)) ? $current_user->getPreference('mail_fromname') : $current_user->full_name;
			
			if (empty($mail_fromaddress)) {
				return false;
			}
			if (empty($mail_fromname)) {
				return false;
			}
			
			$send_type = $current_user->getPreference('mail_sendtype');
			if (!empty($send_type) && $send_type == "SMTP") {
				$mail_smtpserver = $current_user->getPreference('mail_smtpserver');
				$mail_smtpport = $current_user->getPreference('mail_smtpport');
				$mail_smtpauth_req = $current_user->getPreference('mail_smtpauth_req');
				$mail_smtpuser = $current_user->getPreference('mail_smtpuser');
				$mail_smtppass = $current_user->getPreference('mail_smtppass');
				if (empty($mail_smtpserver) || empty($mail_smtpport) || (!empty($mail_smtpauth_req) && (empty($mail_smtpuser) || empty($mail_smtppass)))) {
					return false;
				}
			}
			return true;
		}
		
		/**
	 * outputs JS to set fields in the MassUpdate form in the "My Inbox" view
	 */
		function js_set_archived()
		{
			global $mod_strings;
			$script = 
				'
		<script type="text/javascript" language="JavaScript"><!-- Begin
			function setArchived() {
				var form = document.getElementById("MassUpdate");
				var status = document.getElementById("mass_status");
				var ok = false;

				for(var i=0; i < form.elements.length; i++) {
					if(form.elements[i].name == "mass[]") {
						if(form.elements[i].checked == true) {
							ok = true;
						}
					}
				}

				if(ok == true) {
					var user = document.getElementById("mass_assigned_user_name");
					var team = document.getElementById("team");

					user.value = "";
					for(var j=0; j<status.length; j++) {
						if(status.options[j].value == "archived") {
							status.options[j].selected = true;
							status.selectedIndex = j; // for IE
						}
					}

					form.submit();
				} else {
					alert("' . $mod_strings['ERR_ARCHIVE_EMAIL'] . 
				'");
				}

			}
		//  End --></script>';
			return $script;
		}
		
		/**
	 * replaces the javascript in utils.php - more specialized
	 */
		function u_get_clear_form_js($type = '', $group = '', $assigned_user_id = '')
		{
			$uType = '';
			$uGroup = '';
			$uAssigned_user_id = '';
			
			if (!empty($type)) {
				$uType = '&type=' . $type;
			}
			if (!empty($group)) {
				$uGroup = '&group=' . $group;
			}
			if (!empty($assigned_user_id)) {
				$uAssigned_user_id = '&assigned_user_id=' . $assigned_user_id;
			}
			
			$the_script = 
				'
		<script type="text/javascript" language="JavaScript"><!-- Begin
			function clear_form(form) {
				var newLoc = "index.php?action=" + form.action.value + "&module=" + form.module.value + "&query=true&clear_query=true' . $uType . $uGroup . $uAssigned_user_id . 
				'";
				if(typeof(form.advanced) != "undefined"){
					newLoc += "&advanced=" + form.advanced.value;
				}
				document.location.href= newLoc;
			}
		//  End --></script>';
			return $the_script;
		}
		
		function pickOneButton()
		{
			global $theme;
			global $mod_strings;
			$out = '<div><input	title="' . $mod_strings['LBL_BUTTON_GRAB_TITLE'] . 
				'"
						class="button"
						type="button" name="button"
						onClick="window.location=\'index.php?module=Emails&action=Grab\';"
						style="margin-bottom:2px"
						value="  ' . $mod_strings['LBL_BUTTON_GRAB'] . '  "></div>';
			return $out;
		}
		
		/**
	 * Determines what Editor (HTML or Plain-text) the current_user uses;
	 * @return string Editor type
	 */
		function getUserEditorPreference()
		{
			global $sugar_config;
			global $current_user;
			
			$editor = '';
			
			if (!isset($sugar_config['email_default_editor'])) {
				$sugar_config = $current_user->setDefaultsInConfig();
			}
			
			$userEditor = $current_user->getPreference('email_editor_option');
			$systemEditor = $sugar_config['email_default_editor'];
			
			if ($userEditor != '') {
				$editor = $userEditor;
			} else {
				$editor = $systemEditor;
			}
			
			return $editor;
		}
		
		/**
	 * takes the mess we pass from EditView and tries to create some kind of order
	 * @param array addrs
	 * @param array addrs_ids (from contacts)
	 * @param array addrs_names (from contacts);
	 * @param array addrs_emails (from contacts);
	 * @return array Parsed assoc array to feed to PHPMailer
	 */
		function parse_addrs($addrs, $addrs_ids, $addrs_names, $addrs_emails)
		{
			// cn: bug 9406 - enable commas to separate email addresses
			$addrs = str_replace(",", ";", $addrs);
			
			$ltgt = array('&lt;', '&gt;');
			$gtlt = array('<', '>');
			
			$return = array();
			$addrs = str_replace($ltgt, '', $addrs);
			$addrs_arr = explode(";", $addrs);
			$addrs_arr = $this->remove_empty_fields($addrs_arr);
			$addrs_ids_arr = explode(";", $addrs_ids);
			$addrs_ids_arr = $this->remove_empty_fields($addrs_ids_arr);
			$addrs_emails_arr = explode(";", $addrs_emails);
			$addrs_emails_arr = $this->remove_empty_fields($addrs_emails_arr);
			$addrs_names_arr = explode(";", $addrs_names);
			$addrs_names_arr = $this->remove_empty_fields($addrs_names_arr);
			
			///////////////////////////////////////////////////////////////////////
			////	HANDLE EMAILS HAND-WRITTEN
			$contactRecipients = array();
			$knownEmails = array();
			
			foreach ($addrs_arr as $i => $v) {
				if (trim($v) == "") 
					continue; // skip any "blanks" - will always have 1
				
				$recipient = array();
				
				//// get the email to see if we're dealing with a dupe
				//// what crappy coding
				preg_match("/[A-Z0-9._%-\']+@[A-Z0-9.-]+\.[A-Z]{2,}/i", $v, $match);
				
				
				if (!empty($match[0]) && !in_array(trim($match[0]), $knownEmails)) {
					$knownEmails[] = $match[0];
					$recipient['email'] = $match[0];
					
					//// handle the Display name
					$display = trim(str_replace($match[0], '', $v));
					
					//// only trigger a "displayName" <email@address> when necessary
					if (isset($addrs_names_arr[$i])) {
						$recipient['display'] = $addrs_names_arr[$i];
					} else 
						if (!empty($display)) {
							$recipient['display'] = $display;
						}
					if (isset($addrs_ids_arr[$i]) && $addrs_emails_arr[$i] == $match[0]) {
						$recipient['contact_id'] = $addrs_ids_arr[$i];
					}
					$return[] = $recipient;
				}
			}
			
			return $return;
		}
		
		function remove_empty_fields(&$arr)
		{
			$newarr = array();
			
			foreach ($arr as $field) {
				$field = trim($field);
				if (empty($field)) {
					continue;
				}
				array_push($newarr, $field);
			}
			return $newarr;
		}
		
		/**
	 * handles attachments of various kinds when sending email
	 */
		function handleAttachments()
		{
			
			
			
			
			global $mod_strings;
			
			///////////////////////////////////////////////////////////////////////////
			////    ATTACHMENTS FROM DRAFTS
			if (($this->type == 'out' || $this->type == 'draft') && $this->status == 'draft' && isset($_REQUEST['record'])) {
				$this->getNotes($_REQUEST['record']); // cn: get notes from OLD email for use in new email
			}
			////    END ATTACHMENTS FROM DRAFTS
			///////////////////////////////////////////////////////////////////////////
			
			///////////////////////////////////////////////////////////////////////////
			////    ATTACHMENTS FROM FORWARDS
			// Bug 8034 Jenny - Need the check for type 'draft' here to handle cases where we want to save
			// forwarded messages as drafts.  We still need to save the original message's attachments.
			if (($this->type == 'out' || $this->type == 'draft') && isset($_REQUEST['origType']) && $_REQUEST['origType'] == 'forward' && isset($_REQUEST['return_id']) && !empty($_REQUEST['return_id'])) {
				$this->getNotes($_REQUEST['return_id'], true);
			}
			
			// cn: bug 8034 - attachments from forward/replies lost when saving in draft
			if (isset($_REQUEST['prior_attachments']) && !empty($_REQUEST['prior_attachments']) && $this->new_with_id == true) {
				$exIds = explode(",", $_REQUEST['prior_attachments']);
				if (!isset($_REQUEST['template_attachment'])) {
					$_REQUEST['template_attachment'] = array();
				}
				$_REQUEST['template_attachment'] = array_merge($_REQUEST['template_attachment'], $exIds);
			}
			////    END ATTACHMENTS FROM FORWARDS
			///////////////////////////////////////////////////////////////////////////
			
			///////////////////////////////////////////////////////////////////////////
			////	ATTACHMENTS FROM TEMPLATES
			// to preserve individual email integrity, we must dupe Notes and associated files
			// for each outbound email - good for integrity, bad for filespace
			if (isset($_REQUEST['template_attachment']) && !empty($_REQUEST['template_attachment'])) {
				$removeArr = array();
				$noteArray = array();
				
				if (isset($_REQUEST['temp_remove_attachment']) && !empty($_REQUEST['temp_remove_attachment'])) {
					$removeArr = $_REQUEST['temp_remove_attachment'];
				}
				
				
				foreach ($_REQUEST['template_attachment'] as $noteId) {
					if (in_array($noteId, $removeArr)) {
						continue;
					}
					$noteTemplate = new Note();
					$noteTemplate->retrieve($noteId);
					$noteTemplate->id = create_guid();
					$noteTemplate->new_with_id = true; // duplicating the note with files
					$noteTemplate->parent_id = $this->id;
					$noteTemplate->parent_type = $this->module_dir;
					$noteTemplate->date_entered = '';
					$noteTemplate->save();
					
					$noteFile = new UploadFile();
					$noteFile->duplicate_file($noteId, $noteTemplate->id, $noteTemplate->filename);
					$noteArray[] = $noteTemplate;
				}
				$this->attachments = array_merge($this->attachments, $noteArray);
			}
			////	END ATTACHMENTS FROM TEMPLATES
			///////////////////////////////////////////////////////////////////////////
			
			///////////////////////////////////////////////////////////////////////////
			////	ADDING NEW ATTACHMENTS
			$max_files_upload = 10;
			// Jenny - Bug 8211 Since attachments for drafts have already been processed,
			// we don't need to re-process them.
			if ($this->status != "draft") {
				$notes_list = array();
				if (!empty($this->id) && !$this->new_with_id) {
					$note = new Note();
					$where = "notes.parent_id='{$this->id}'";
					$notes_list = $note->get_full_list("", $where, true);
				}
				$this->attachments = array_merge($this->attachments, $notes_list);
			}
			// cn: Bug 5995 - rudimentary error checking
			$filesError = array(
				0 => 'UPLOAD_ERR_OK - There is no error, the file uploaded with success.', 
				1 => 'UPLOAD_ERR_INI_SIZE - The uploaded file exceeds the upload_max_filesize directive in php.ini.', 
				2 => 'UPLOAD_ERR_FORM_SIZE - The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.', 
				3 => 'UPLOAD_ERR_PARTIAL - The uploaded file was only partially uploaded.', 
				4 => 'UPLOAD_ERR_NO_FILE - No file was uploaded.', 
				5 => 'UNKNOWN ERROR', 
				6 => 'UPLOAD_ERR_NO_TMP_DIR - Missing a temporary folder. Introduced in PHP 4.3.10 and PHP 5.0.3.', 
				7 => 'UPLOAD_ERR_CANT_WRITE - Failed to write file to disk. Introduced in PHP 5.1.0.');
			
			for($i = 0; $i < $max_files_upload; $i++) {
				// cn: Bug 5995 - rudimentary error checking
				if (!isset($_FILES["email_attachment{$i}"])) {
					$GLOBALS['log']->debug("Email Attachment {$i} does not exist.");
					continue;
				}
				if ($_FILES['email_attachment' . $i]['error'] != 0 && $_FILES['email_attachment' . $i]['error'] != 4) {
					$GLOBALS['log']->debug('Email Attachment could not be attach due to error: ' . $filesError[$_FILES['email_attachment' . $i]['error']]);
					continue;
				}
				
				$note = new Note();
				$note->parent_id = $this->id;
				$note->parent_type = $this->module_dir;
				$upload_file = new UploadFile('email_attachment' . $i);
				
				if (empty($upload_file)) {
					continue;
				}
				
				if (isset($_FILES['email_attachment' . $i]) && $upload_file->confirm_upload()) {
					$note->filename = $upload_file->get_stored_file_name();
					$note->file = $upload_file;
					$note->name = $mod_strings['LBL_EMAIL_ATTACHMENT'] . ': ' . $note->file->original_file_name;
					
					$this->attachments[] = $note;
				}
			}
			
			$this->saved_attachments = array();
			foreach ($this->attachments as $note) {
				if (!empty($note->id)) {
					array_push($this->saved_attachments, $note);
					continue;
				}
				$note->parent_id = $this->id;
				$note->parent_type = 'Emails';
				$note->file_mime_type = $note->file->mime_type;
				$note_id = $note->save();
				
				$this->saved_attachments[] = $note;
				
				$note->id = $note_id;
				$note->file->final_move($note->id);
			}
			////	END NEW ATTACHMENTS
			///////////////////////////////////////////////////////////////////////////
			
			///////////////////////////////////////////////////////////////////////////
			////	ATTACHMENTS FROM DOCUMENTS
			for($i = 0; $i < 10; $i++) {
				if (isset($_REQUEST['documentId' . $i]) && !empty($_REQUEST['documentId' . $i])) {
					$doc = new Document();
					$docRev = new DocumentRevision();
					$docNote = new Note();
					$noteFile = new UploadFile();
					
					$doc->retrieve($_REQUEST['documentId' . $i]);
					$docRev->retrieve($doc->document_revision_id);
					
					$this->saved_attachments[] = $docRev;
					
					// cn: bug 9723 - Emails with documents send GUID instead of Doc name
					$docNote->name = $docRev->getDocumentRevisionNameForDisplay();
					$docNote->filename = $docRev->filename;
					$docNote->description = $doc->description;
					$docNote->parent_id = $this->id;
					$docNote->parent_type = 'Emails';
					$docNote->file_mime_type = $docRev->file_mime_type;
					$docId = $docNote = $docNote->save();
					
					$noteFile->duplicate_file($docRev->id, $docId, $docRev->filename);
				}
			}
			
			////	END ATTACHMENTS FROM DOCUMENTS
			///////////////////////////////////////////////////////////////////////////
			
			///////////////////////////////////////////////////////////////////////////
			////	REMOVE ATTACHMENTS
			if (isset($_REQUEST['remove_attachment']) && !empty($_REQUEST['remove_attachment'])) {
				foreach ($_REQUEST['remove_attachment'] as $noteId) {
					$q = 'UPDATE notes SET deleted = 1 WHERE id = \'' . $noteId . '\'';
					$this->db->query($q);
				}
			}
			
			//this will remove attachments that have been selected to be removed from drafts.
			if (isset($_REQUEST['removeAttachment']) && !empty($_REQUEST['removeAttachment'])) {
				$exRemoved = explode('::', $_REQUEST['removeAttachment']);
				foreach ($exRemoved as $noteId) {
					$q = 'UPDATE notes SET deleted = 1 WHERE id = \'' . $noteId . '\'';
					$this->db->query($q);
				}
			}
			////	END REMOVE ATTACHMENTS
			///////////////////////////////////////////////////////////////////////////
		}
		
		
		/**
	 * Determines if an email body (HTML or Plain) has a User signature already in the content
	 * @param array Array of signatures
	 * @return bool
	 */
		function hasSignatureInBody($sig)
		{
			// strpos can't handle line breaks - normalize
			$html = $this->removeAllNewlines($this->description_html);
			$htmlSig = $this->removeAllNewlines($sig['signature_html']);
			$plain = $this->removeAllNewlines($this->description);
			$plainSig = $this->removeAllNewlines($sig['signature']);
			
			// cn: bug 11621 - empty sig triggers notice error
			if (!empty($htmlSig) && false !== strpos($html, $htmlSig)) {
				return true;
			} elseif (!empty($plainSig) && false !== strpos($plain, $plainSig)) {
				return true;
			} else {
				return false;
			}
		}
		
		/**
	 * internal helper
	 * @param string String to be normalized
	 * @return string
	 */
		function removeAllNewlines($str)
		{
			$bad = array("\r\n", "\n\r", "\n", "\r");
			$good = array('', '', '', '');
			
			return str_replace($bad, $good, strip_tags(br2nl(from_html($str))));
		}
		
		
		
		/**
	 * Set navigation anchors to aid DetailView record navigation (VCR buttons)
	 * @param string uri The URI from the referring page (always ListView)
	 * @return array start Array of the URI broken down with a special "current_view" for My Inbox Navs
	 */
		function getStartPage($uri)
		{
			if (strpos($uri, '&')) {
				// "&" to ensure that we can explode the GET vars - else we're gonna trigger a Notice error
				$serial = substr($uri, (strpos($uri, '?') + 1), strlen($uri));
				$exUri = explode('&', $serial);
				$start = array('module' => '', 'action' => '', 'group' => '', 'record' => '', 'type' => '');
				
				foreach ($exUri as $k => $pair) {
					$exPair = explode('=', $pair);
					$start[$exPair[0]] = $exPair[1];
				}
				
				// specific views for current_user
				if (isset($start['assigned_user_id'])) {
					$start['current_view'] = "{$start['action']}&module={$start['module']}&assigned_user_id={$start['assigned_user_id']}&type={$start['type']}";
				}
				
				return $start;
			} else {
				return array();
			}
		}
		
		/**
	 * preps SMTP info for email transmission
	 * @param object mail SugarPHPMailer object
	 * @param string mailer_id
	 * @param string ieId
	 * @return object mail SugarPHPMailer object
	 */
		function setMailer($mail, $mailer_id = '', $ieId = '')
		{
			global $current_user;
			
			require_once ("include/OutboundEmail/OutboundEmail.php");
			$oe = new OutboundEmail();
			$oe = $oe->getInboundMailerSettings($current_user, $mailer_id, $ieId);
			
			// ssl or tcp - keeping outside isSMTP b/c a default may inadvertantly set ssl://
			$mail->protocol = ($oe->mail_smtpssl) ? "ssl://" : "tcp://";
			if ($oe->mail_sendtype == "SMTP") {
				//Set mail send type information
				$mail->Mailer = "smtp";
				$mail->Host = $oe->mail_smtpserver;
				$mail->Port = $oe->mail_smtpport;
				if ($oe->mail_smtpssl == 1) {
					$mail->SMTPSecure = 'ssl';
				}
				// if
				if ($oe->mail_smtpssl == 2) {
					$mail->SMTPSecure = 'tls';
				}
				// if
				
				if ($oe->mail_smtpauth_req) {
					$mail->SMTPAuth = TRUE;
					$mail->Username = $oe->mail_smtpuser;
					$mail->Password = $oe->mail_smtppass;
				}
			} else 
				$mail->Mailer = "sendmail";
			
			$mail->oe = $oe;
			return $mail;
		}
		
		/**
	 * preps SugarPHPMailer object for HTML or Plain text sends
	 * @param SugarPHPMailer $mail SugarPHPMailer instance
	 */
		function handleBody($mail)
		{
			global $current_user;
			global $sugar_config;
			///////////////////////////////////////////////////////////////////////
			////	HANDLE EMAIL FORMAT PREFERENCE
			// the if() below is HIGHLY dependent on the Javascript unchecking the Send HTML Email box
			// HTML email
			if ((isset($_REQUEST['setEditor']) 
				&& $_REQUEST['setEditor'] == 1 
				&& trim($_REQUEST['description_html']) != '') 
				|| trim($this->description_html) != '' 
				&& $current_user->getPreference('email_editor_option', 'global') !== 'plain') {
				/* from Email EditView navigation */
				/* from email templates */
				//user preference is not set to plain text
				$this->handleBodyInHTMLformat($mail);
			} else {
				// plain text only
				$this->description_html = '';
				$mail->IsHTML(false);
				$plainText = from_html($this->description);
				$plainText = str_replace("&nbsp;", " ", $plainText);
				$plainText = str_replace("</p>", "</p><br />", $plainText);
				$plainText = strip_tags(br2nl($plainText));
				$plainText = str_replace("&amp;", "&", $plainText);
				$plainText = str_replace("&#39;", "'", $plainText);
				$mail->Body = wordwrap($plainText, 996);
				$mail->Body = $this->decodeDuringSend($mail->Body);
				$this->description = $mail->Body;
			}
			
			// wp: if plain text version has lines greater than 998, use base64 encoding
			foreach (explode("\n", ($mail->ContentType == "text/html") ? $mail->AltBody : $mail->Body) as $line) {
				if (strlen($line) > 998) {
					$mail->Encoding = 'base64';
					break;
				}
			}
			////	HANDLE EMAIL FORMAT PREFERENCE
			///////////////////////////////////////////////////////////////////////
			
			return $mail;
		}
		
		/**
	 * Retrieve function from handlebody() to unit test easily
	 * @param SugarPHPMailer $mail SugarPHPMailer instance
	 * @return formatted $mail body
	 */
		function handleBodyInHTMLformat($mail)
		{
			global $sugar_config;
			// wp: if body is html, then insert new lines at 996 characters. no effect on client side
			// due to RFC 2822 which limits email lines to 998
			$mail->IsHTML(true);
			$body = from_html(wordwrap($this->description_html, 996));
			$mail->Body = $body;
			
			// cn: bug 9725
			// new plan is to use the selected type (html or plain) to fill the other
			$plainText = from_html($this->description_html);
			$plainText = strip_tags(br2nl($plainText));
			$mail->AltBody = $plainText;
			$this->description = $plainText;
			
			$mail->replaceImageByRegex("(?:{$sugar_config['site_url']})?/?cache/images/", sugar_cached("images/"));
			
			//Replace any embeded images using the secure entryPoint for src url.
			$mail->replaceImageByRegex("(?:{$sugar_config['site_url']})?/?index.php[?]entryPoint=download&(?:amp;)?[^\"]+?id=", "upload://", true);
			
			$mail->Body = from_html($mail->Body);
		}
		
		/**
	 * Sends Email
	 * @return bool True on success
	 */
		function send()
		{
			global $mod_strings, $app_strings;
			global $current_user;
			global $sugar_config;
			global $locale;
			$OBCharset = $locale->getPrecedentPreference('default_email_charset');
			$mail = new SugarPHPMailer();
			
			foreach ($this->to_addrs_arr as $addr_arr) {
				if (empty($addr_arr['display'])) {
					$mail->AddAddress($addr_arr['email'], "");
				} else {
					$mail->AddAddress($addr_arr['email'], $locale->translateCharsetMIME(trim($addr_arr['display']), 'UTF-8', $OBCharset));
				}
			}
			foreach ($this->cc_addrs_arr as $addr_arr) {
				if (empty($addr_arr['display'])) {
					$mail->AddCC($addr_arr['email'], "");
				} else {
					$mail->AddCC($addr_arr['email'], $locale->translateCharsetMIME(trim($addr_arr['display']), 'UTF-8', $OBCharset));
				}
			}
			
			foreach ($this->bcc_addrs_arr as $addr_arr) {
				if (empty($addr_arr['display'])) {
					$mail->AddBCC($addr_arr['email'], "");
				} else {
					$mail->AddBCC($addr_arr['email'], $locale->translateCharsetMIME(trim($addr_arr['display']), 'UTF-8', $OBCharset));
				}
			}
			
			$mail = $this->setMailer($mail);
			
			// FROM ADDRESS
			if (!empty($this->from_addr)) {
				$mail->From = $this->from_addr;
			} else {
				$mail->From = $current_user->getPreference('mail_fromaddress');
				$this->from_addr = $mail->From;
			}
			// FROM NAME
			if (!empty($this->from_name)) {
				$mail->FromName = $this->from_name;
			} else {
				$mail->FromName = $current_user->getPreference('mail_fromname');
				$this->from_name = $mail->FromName;
			}
			
			//Reply to information for case create and autoreply.
			if (!empty($this->reply_to_name)) {
				$ReplyToName = $this->reply_to_name;
			} else {
				$ReplyToName = $mail->FromName;
			}
			if (!empty($this->reply_to_addr)) {
				$ReplyToAddr = $this->reply_to_addr;
			} else {
				$ReplyToAddr = $mail->From;
			}
			$mail->Sender = $mail->From; /* set Return-Path field in header to reduce spam score in emails sent via Sugar's Email module */
			$mail->AddReplyTo($ReplyToAddr, $locale->translateCharsetMIME(trim($ReplyToName), 'UTF-8', $OBCharset));
			
			//$mail->Subject = html_entity_decode($this->name, ENT_QUOTES, 'UTF-8');
			$mail->Subject = $this->name;
			
			///////////////////////////////////////////////////////////////////////
			////	ATTACHMENTS
			foreach ($this->saved_attachments as $note) {
				$mime_type = 'text/plain';
				if ($note->object_name == 'Note') {
					if (!empty($note->file->temp_file_location) && is_file($note->file->temp_file_location)) {
						// brandy-new file upload/attachment
						$file_location = "upload://$note->id";
						$filename = $note->file->original_file_name;
						$mime_type = $note->file->mime_type;
					} else {
						// attachment coming from template/forward
						$file_location = "upload://{$note->id}";
						// cn: bug 9723 - documents from EmailTemplates sent with Doc Name, not file name.
						$filename = !empty($note->filename) ? $note->filename : $note->name;
						$mime_type = $note->file_mime_type;
					}
				} elseif ($note->object_name == 'DocumentRevision') {
					// from Documents
					$filePathName = $note->id;
					// cn: bug 9723 - Emails with documents send GUID instead of Doc name
					$filename = $note->getDocumentRevisionNameForDisplay();
					$file_location = "upload://$note->id";
					$mime_type = $note->file_mime_type;
				}
				
				// strip out the "Email attachment label if exists
				$filename = str_replace($mod_strings['LBL_EMAIL_ATTACHMENT'] . ': ', '', $filename);
				$file_ext = pathinfo($filename, PATHINFO_EXTENSION);
				//is attachment in our list of bad files extensions?  If so, append .txt to file location
				//check to see if this is a file with extension located in "badext"
				foreach ($sugar_config['upload_badext'] as $badExt) {
					if (strtolower($file_ext) == strtolower($badExt)) {
						//if found, then append with .txt to filename and break out of lookup
						//this will make sure that the file goes out with right extension, but is stored
						//as a text in db.
						$file_location = $file_location . ".txt";
						break; // no need to look for more
					}
				}
				$mail->AddAttachment($file_location, $locale->translateCharsetMIME(trim($filename), 'UTF-8', $OBCharset), 'base64', $mime_type);
				
				// embedded Images
				if ($note->embed_flag == true) {
					$cid = $filename;
					$mail->AddEmbeddedImage($file_location, $cid, $filename, 'base64', $mime_type);
				}
			}
			////	END ATTACHMENTS
			///////////////////////////////////////////////////////////////////////
			
			$mail = $this->handleBody($mail);
			
			$GLOBALS['log']->debug('Email sending --------------------- ');
			
			///////////////////////////////////////////////////////////////////////
			////	I18N TRANSLATION
			$mail->prepForOutbound();
			////	END I18N TRANSLATION
			///////////////////////////////////////////////////////////////////////
			
			if ($mail->Send()) {
				///////////////////////////////////////////////////////////////////
				////	INBOUND EMAIL HANDLING
				// mark replied
				if (!empty($_REQUEST['inbound_email_id'])) {
					$ieMail = new Email();

					//TODO (MT EXTEND Code): retrieve and use the TENANT_ID
					$ieMail->retrieve($_REQUEST['inbound_email_id']);
					$ieMail->status = 'replied';
					$ieMail->save();
				}
				$GLOBALS['log']->debug(' --------------------- buh bye -- sent successful');
				////	END INBOUND EMAIL HANDLING
				///////////////////////////////////////////////////////////////////
				return true;
			}
			$GLOBALS['log']->debug($app_strings['LBL_EMAIL_ERROR_PREPEND'] . $mail->ErrorInfo);
			return false;
		}
		
		
		function listviewACLHelper()
		{
			$array_assign = parent::listviewACLHelper();
			$is_owner = false;
			if (!empty($this->parent_name)) {
				
				if (!empty($this->parent_name_owner)) {
					global $current_user;
					$is_owner = $current_user->id == $this->parent_name_owner;
				}
			}
			if (!ACLController::moduleSupportsACL($this->parent_type) || ACLController::checkAccess($this->parent_type, 'view', $is_owner)) {
				$array_assign['PARENT'] = 'a';
			} else {
				$array_assign['PARENT'] = 'span';
			}
			$is_owner = false;
			if (!empty($this->contact_name)) {
				if (!empty($this->contact_name_owner)) {
					global $current_user;
					$is_owner = $current_user->id == $this->contact_name_owner;
				}
			}
			if (ACLController::checkAccess('Contacts', 'view', $is_owner)) {
				$array_assign['CONTACT'] = 'a';
			} else {
				$array_assign['CONTACT'] = 'span';
			}
			
			return $array_assign;
		}
		
		function getSystemDefaultEmail()
		{
			$email = array();
			
			$r1 = $this->db->query('SELECT config.value FROM config WHERE name=\'fromaddress\'');
			$r2 = $this->db->query('SELECT config.value FROM config WHERE name=\'fromname\'');
			$a1 = $this->db->fetchByAssoc($r1);
			$a2 = $this->db->fetchByAssoc($r2);
			
			$email['email'] = $a1['value'];
			$email['name'] = $a2['value'];
			
			return $email;
		}
		
		
		function create_new_list_query($order_by, $where, $filter = array(), $params = array(), $show_deleted = 0, $join_type = '', $return_array = false, $parentbean = null, $singleSelect = false)
		{
			
			if ($return_array) {
				return parent::create_new_list_query($order_by, $where, $filter, $params, $show_deleted, $join_type, $return_array, $parentbean, $singleSelect);
			}
			$custom_join = $this->getCustomJoin();
			
			$query = "SELECT " . $this->table_name . ".*, users.user_name as assigned_user_name\n";
			
			$query .= $custom_join['select'];
			$query .= " FROM emails\n";
			if ($where != "" && (strpos($where, "contacts.first_name") > 0)) {
				$query .= " LEFT JOIN emails_beans ON emails.id = emails_beans.email_id\n";
			}
			
			$query .= " LEFT JOIN users ON emails.assigned_user_id=users.id \n";
			if ($where != "" && (strpos($where, "contacts.first_name") > 0)) {
				
				$query .= " JOIN contacts ON contacts.id= emails_beans.bean_id AND emails_beans.bean_module='Contacts' and contacts.deleted=0 \n";
			}
			
			$query .= $custom_join['join'];
			
			if ($show_deleted == 0) {
				$where_auto = " emails.deleted=0 \n";
			} else 
				if ($show_deleted == 1) {
					$where_auto = " emails.deleted=1 \n";
				}
			
			if ($where != "") 
				$query .= "WHERE $where AND " . $where_auto;
			 else 
				$query .= "WHERE " . $where_auto;
			
			if ($order_by != "") 
				$query .= " ORDER BY $order_by";
			 else 
				$query .= " ORDER BY date_sent DESC";
			
			return $query;
		}
		// fn
		
		
		function fill_in_additional_list_fields()
		{
			global $timedate, $mod_strings;
			$this->fill_in_additional_detail_fields();
			
			$this->link_action = 'DetailView';
			///////////////////////////////////////////////////////////////////////
			//populate attachment_image, used to display attachment icon.
			$query = "select 1 from notes where notes.parent_id = '$this->id' and notes.deleted = 0";
			$result = $this->db->query($query, true, " Error filling in additional list fields: ");
			
			$row = $this->db->fetchByAssoc($result);
			
			if ($row) {
				$this->attachment_image = SugarThemeRegistry::current()->getImage(
					'attachment', 
					'', 
					null, 
					null, 
					'.gif', translate('LBL_ATTACHMENT', 'Emails'));
			} else {
				//TODO (MT CHECK): Assignment to property of potential multi-tenant object!
				$this->attachment_image = '';
			}
			
			///////////////////////////////////////////////////////////////////////
			//TODO (MT CHECK): Assignment to property of potential multi-tenant object!
			if (empty($this->contact_id) && !empty($this->parent_id) && !empty($this->parent_type) && $this->parent_type === 'Contacts' && !empty($this->parent_name)) {
				$this->contact_id = $this->parent_id;
				$this->contact_name = $this->parent_name;
			}
		}
		
		function fill_in_additional_detail_fields()
		{
			global $app_list_strings, $mod_strings;
			// Fill in the assigned_user_name
			$this->assigned_user_name = get_assigned_user_name($this->assigned_user_id, '');
			//if ($this->parent_type == 'Contacts') {
			$query = "SELECT contacts.first_name, contacts.last_name, contacts.phone_work, contacts.id, contacts.assigned_user_id contact_name_owner, 'Contacts' contact_name_mod FROM contacts, emails_beans ";
			$query .= "WHERE emails_beans.email_id='$this->id' AND emails_beans.bean_id=contacts.id AND emails_beans.bean_module = 'Contacts' AND emails_beans.deleted=0 AND contacts.deleted=0";
			if (!empty($this->parent_id)) {
				$query .= " AND contacts.id= '" . $this->parent_id . "' ";
			} else 
				if (!empty($_REQUEST['record'])) {
					$query .= " AND contacts.id= '" . $_REQUEST['record'] . "' ";
				}
			$result = $this->db->query($query, true, " Error filling in additional detail fields: ");
			
			// Get the id and the name.
			$row = $this->db->fetchByAssoc($result);
			if ($row != null) {
				
				$contact = new Contact();
				$contact->retrieve($row['id']);
				$this->contact_name = $contact->full_name;
				$this->contact_phone = $row['phone_work'];
				$this->contact_id = $row['id'];
				$this->contact_email = $contact->emailAddress->getPrimaryAddress($contact);
				$this->contact_name_owner = $row['contact_name_owner'];
				$this->contact_name_mod = $row['contact_name_mod'];
				$GLOBALS['log']->debug("Call($this->id): contact_name = $this->contact_name");
				$GLOBALS['log']->debug("Call($this->id): contact_phone = $this->contact_phone");
				$GLOBALS['log']->debug("Call($this->id): contact_id = $this->contact_id");
				$GLOBALS['log']->debug("Call($this->id): contact_email1 = $this->contact_email");
			} else {
				$this->contact_name = '';
				$this->contact_phone = '';
				$this->contact_id = '';
				$this->contact_email = '';
				$this->contact_name_owner = '';
				$this->contact_name_mod = '';
				$GLOBALS['log']->debug("Call($this->id): contact_name = $this->contact_name");
				$GLOBALS['log']->debug("Call($this->id): contact_phone = $this->contact_phone");
				$GLOBALS['log']->debug("Call($this->id): contact_id = $this->contact_id");
				$GLOBALS['log']->debug("Call($this->id): contact_email1 = $this->contact_email");
			}
			//}
			$this->created_by_name = get_assigned_user_name($this->created_by);
			$this->modified_by_name = get_assigned_user_name($this->modified_user_id);
			
			$this->link_action = 'DetailView';
			
			if (!empty($this->type)) {
				if ($this->type == 'out' && $this->status == 'send_error') {
					$this->type_name = $mod_strings['LBL_NOT_SENT'];
				} else {
					$this->type_name = $app_list_strings['dom_email_types'][$this->type];
				}
				
				if (($this->type == 'out' && $this->status == 'send_error') || $this->type == 'draft') {
					$this->link_action = 'EditView';
				}
			}
			
			//todo this  isset( $app_list_strings['dom_email_status'][$this->status]) is hack for 3261.
			if (!empty($this->status) && isset($app_list_strings['dom_email_status'][$this->status])) {
				$this->status_name = $app_list_strings['dom_email_status'][$this->status];
			}
			
			if (empty($this->name) && empty($_REQUEST['record'])) {
				$this->name = $mod_strings['LBL_NO_SUBJECT'];
			}
			
			$this->fill_in_additional_parent_fields();
		}
		
		
		
		function create_export_query(&$order_by, &$where)
		{
			$contact_required = stristr($where, "contacts");
			$custom_join = $this->getCustomJoin(true, true, $where);
			
			if ($contact_required) {
				$query = "SELECT emails.*, contacts.first_name, contacts.last_name";
				$query .= $custom_join['select'];
				
				$query .= " FROM contacts, emails, emails_contacts ";
				$where_auto = "emails_contacts.contact_id = contacts.id AND emails_contacts.email_id = emails.id AND emails.deleted=0 AND contacts.deleted=0";
			} else {
				$query = 'SELECT emails.*';
				$query .= $custom_join['select'];
				
				$query .= ' FROM emails ';
				$where_auto = "emails.deleted=0";
			}
			
			$query .= $custom_join['join'];
			
			if ($where != "") 
				$query .= "where $where AND " . $where_auto;
			 else 
				$query .= "where " . $where_auto;
			
			if ($order_by != "") 
				$query .= " ORDER BY $order_by";
			 else 
				$query .= " ORDER BY emails.name";
			return $query;
		}
		
		function get_list_view_data()
		{
			global $app_list_strings;
			global $theme;
			global $current_user;
			global $timedate;
			global $mod_strings;
			
			$email_fields = $this->get_list_view_array();
			$this->retrieveEmailText();
			$email_fields['FROM_ADDR'] = $this->from_addr_name;
			$mod_strings = return_module_language($GLOBALS['current_language'], 'Emails'); // hard-coding for Home screen ListView
			
			if ($this->status != 'replied') {
				$email_fields['QUICK_REPLY'] = '<a  href="index.php?module=Emails&action=Compose&replyForward=true&reply=reply&record=' . $this->id . '&inbound_email_id=' . $this->id . '">' . $mod_strings['LNK_QUICK_REPLY'] . '</a>';
				$email_fields['STATUS'] = ($email_fields['REPLY_TO_STATUS'] == 1 ? $mod_strings['LBL_REPLIED'] : $email_fields['STATUS']);
			} else {
				$email_fields['QUICK_REPLY'] = $mod_strings['LBL_REPLIED'];
			}
			if (!empty($this->parent_type)) {
				$email_fields['PARENT_MODULE'] = $this->parent_type;
			} else {
				switch ($this->intent) {
					case 'support':
						$email_fields['CREATE_RELATED'] = '<a href="index.php?module=Cases&action=EditView&inbound_email_id=' . $this->id . '" >' . SugarThemeRegistry::current()->getImage('CreateCases', 'border="0"', null, null, ".gif", $mod_strings['LBL_CREATE_CASES']) . $mod_strings['LBL_CREATE_CASE'] . '</a>';
						break;
					
					case 'sales':
						$email_fields['CREATE_RELATED'] = '<a href="index.php?module=Leads&action=EditView&inbound_email_id=' . $this->id . '" >' . SugarThemeRegistry::current()->getImage('CreateLeads', 'border="0"', null, null, ".gif", $mod_strings['LBL_CREATE_LEADS']) . $mod_strings['LBL_CREATE_LEAD'] . '</a>';
						break;
					
					case 'contact':
						$email_fields['CREATE_RELATED'] = '<a href="index.php?module=Contacts&action=EditView&inbound_email_id=' . $this->id . '" >' . SugarThemeRegistry::current()->getImage('CreateContacts', 'border="0"', null, null, ".gif", $mod_strings['LBL_CREATE_CONTACTS']) . $mod_strings['LBL_CREATE_CONTACT'] . '</a>';
						break;
					
					case 'bug':
						$email_fields['CREATE_RELATED'] = '<a href="index.php?module=Bugs&action=EditView&inbound_email_id=' . $this->id . '" >' . SugarThemeRegistry::current()->getImage('CreateBugs', 'border="0"', null, null, ".gif", $mod_strings['LBL_CREATE_BUGS']) . $mod_strings['LBL_CREATE_BUG'] . '</a>';
						break;
					
					case 'task':
						$email_fields['CREATE_RELATED'] = '<a href="index.php?module=Tasks&action=EditView&inbound_email_id=' . $this->id . '" >' . SugarThemeRegistry::current()->getImage('CreateTasks', 'border="0"', null, null, ".gif", $mod_strings['LBL_CREATE_TASKS']) . $mod_strings['LBL_CREATE_TASK'] . '</a>';
						break;
					
					case 'bounce':
						break;
					
					case 'pick':
					// break;
					
					case 'info':
					//break;
					
					default:
						$email_fields['CREATE_RELATED'] = $this->quickCreateForm();
						break;
				}
				
			}
			
			//BUG 17098 - MFH changed $this->from_addr to $this->to_addrs
			$email_fields['CONTACT_NAME'] = empty($this->contact_name) ? '</a>' . $this->trimLongTo($this->to_addrs) . '<a>' : $this->contact_name;
			$email_fields['CONTACT_ID'] = empty($this->contact_id) ? '' : $this->contact_id;
			$email_fields['ATTACHMENT_IMAGE'] = $this->attachment_image;
			$email_fields['LINK_ACTION'] = $this->link_action;
			
			if (isset($this->type_name)) 
				$email_fields['TYPE_NAME'] = $this->type_name;
			
			return $email_fields;
		}
		
		function quickCreateForm()
		{
			global $mod_strings, $app_strings, $currentModule, $current_language;
			
			// Coming from the home page via Dashlets
			if ($currentModule != 'Email') 
				$mod_strings = return_module_language($current_language, 'Emails');
			return $mod_strings['LBL_QUICK_CREATE'] . "&nbsp;<a id='$this->id' onclick='return quick_create_overlib(\"{$this->id}\", \"" . SugarThemeRegistry::current()->__toString() . "\", this);' href=\"#\" >" . SugarThemeRegistry::current()->getImage("advanced_search", "border='0' align='absmiddle'", null, null, '.gif', $mod_strings['LBL_QUICK_CREATE']) . "</a>";
		}
		
		/**
     * Searches all imported emails and returns the result set as an array.
     *
     */
		function searchImportedEmails($sort = '', $direction = '')
		{
			require_once ('include/TimeDate.php');
			global $timedate;
			global $current_user;
			global $beanList;
			global $sugar_config;
			global $app_strings;
			
			$emailSettings = $current_user->getPreference('emailSettings', 'Emails');
			// cn: default to a low number until user specifies otherwise
			if (empty($emailSettings['showNumInList'])) 
				$pageSize = 20;
			 else 
				$pageSize = $emailSettings['showNumInList'];
			
			if (isset($_REQUEST['start']) && isset($_REQUEST['limit'])) 
				$page = ceil($_REQUEST['start'] / $_REQUEST['limit']) + 1;
			 else 
				$page = 1;
			
			//Determine sort ordering
			
			//Sort ordering parameters in the request do not coincide with actual column names
			//so we need to remap them.
			$hrSortLocal = array(
				'flagged' => 'type', 
				'status' => 'reply_to_status', 
				'from' => 'emails_text.from_addr', 
				'subject' => 'name', 
				'date' => 'date_sent', 
				'AssignedTo' => 'assigned_user_id', 
				'flagged' => 'flagged');
			
			$sort = !empty($_REQUEST['sort']) ? $this->db->getValidDBName($_REQUEST['sort']) : "";
			$direction = !empty($_REQUEST['dir']) && in_array(strtolower($_REQUEST['dir']), array("asc", "desc")) ? $_REQUEST['dir'] : "";
			
			$order = (!empty($sort) && !empty($direction)) ? " ORDER BY {$hrSortLocal[$sort]} {$direction}" : "";
			
			//Get our main query.
			$fullQuery = $this->_genereateSearchImportedEmailsQuery();
			
			//Perform a count query needed for pagination.
			$countQuery = $this->create_list_count_query($fullQuery);
			
			$count_rs = $this->db->query($countQuery, false, 'Error executing count query for imported emails search');
			$count_row = $this->db->fetchByAssoc($count_rs);
			$total_count = ($count_row != null) ? $count_row['c'] : 0;
			
			$start = ($page - 1) * $pageSize;
			
			//Execute the query
			$rs = $this->db->limitQuery($fullQuery . $order, $start, $pageSize);
			
			$return = array();
			
			while ($a = $this->db->fetchByAssoc($rs)) {
				$temp = array();
				$temp['flagged'] = (is_null($a['flagged']) || $a['flagged'] == '0') ? '' : 1;
				$temp['status'] = (is_null($a['reply_to_status']) || $a['reply_to_status'] == '0') ? '' : 1;
				$temp['subject'] = $a['name'];
				$temp['date'] = $timedate->to_display_date_time($a['date_sent']);
				$temp['uid'] = $a['id'];
				$temp['ieId'] = $a['mailbox_id'];
				$temp['site_url'] = $sugar_config['site_url'];
				$temp['seen'] = ($a['status'] == 'unread') ? 0 : 1;
				$temp['type'] = $a['type'];
				$temp['mbox'] = 'sugar::Emails';
				$temp['hasAttach'] = $this->doesImportedEmailHaveAttachment($a['id']);
				//To and from addresses may be stored in emails_text, if nothing is found, revert to
				//regular email addresses.
				$temp['to_addrs'] = preg_replace('/[\x00-\x08\x0B-\x1F]/', '', $a['to_addrs']);
				$temp['from'] = preg_replace('/[\x00-\x08\x0B-\x1F]/', '', $a['from_addr']);
				if (empty($temp['from']) || empty($temp['to_addrs'])) {
					//Retrieve email addresses seperatly.
					$tmpEmail = new Email();
					$tmpEmail->id = $a['id'];
					$tmpEmail->retrieveEmailAddresses();
					$temp['from'] = $tmpEmail->from_addr;
					$temp['to_addrs'] = $tmpEmail->to_addrs;
				}
				
				$return[] = $temp;
			}
			
			$metadata = array();
			$metadata['totalCount'] = $total_count;
			$metadata['out'] = $return;
			
			return $metadata;
		}
		
		/**
     * Determine if an imported email has an attachment by examining the relationship to notes.
     *
     * @param string $id
     * @return boolean
     */
		function doesImportedEmailHaveAttachment($id)
		{
			$hasAttachment = FALSE;
			$query = "SELECT id FROM notes where parent_id='$id' AND parent_type='Emails' AND file_mime_type is not null AND deleted=0";
			$rs = $this->db->limitQuery($query, 0, 1);
			$row = $this->db->fetchByAssoc($rs);
			if (!empty($row['id'])) 
				$hasAttachment = TRUE;
			
			return (int) $hasAttachment;
		}
		
		/**
     * Generate the query used for searching imported emails.
     *
     * @return String Query to be executed.
     */
		function _genereateSearchImportedEmailsQuery()
		{
			global $timedate;
			
			$additionalWhereClause = $this->_generateSearchImportWhereClause();
			
			$query = array();
			$fullQuery = "";
			$query['select'] = 
				"emails.id , emails.mailbox_id, emails.name, emails.date_sent, emails.status, emails.type, emails.flagged, emails.reply_to_status,
		                      emails_text.from_addr, emails_text.to_addrs  FROM emails ";
			
			$query['joins'] = " JOIN emails_text on emails.id = emails_text.email_id ";
			
			//Handle from and to addr joins
			if (!empty($_REQUEST['from_addr'])) {
				$from_addr = $this->db->quote(strtolower($_REQUEST['from_addr']));
				$query['joins'] .= 
					"INNER JOIN emails_email_addr_rel er_from ON er_from.email_id = emails.id AND er_from.deleted = 0 INNER JOIN email_addresses ea_from ON ea_from.id = er_from.email_address_id
                                AND er_from.address_type='from' AND emails_text.from_addr LIKE '%" . $from_addr . "%'";
			}
			
			if (!empty($_REQUEST['to_addrs'])) {
				$to_addrs = $this->db->quote(strtolower($_REQUEST['to_addrs']));
				$query['joins'] .= 
					"INNER JOIN emails_email_addr_rel er_to ON er_to.email_id = emails.id AND er_to.deleted = 0 INNER JOIN email_addresses ea_to ON ea_to.id = er_to.email_address_id
                                    AND er_to.address_type='to' AND ea_to.email_address LIKE '%" . $to_addrs . "%'";
			}
			
			$query['where'] = " WHERE (emails.type= 'inbound' OR emails.type='archived' OR emails.type='out') AND emails.deleted = 0 ";
			if (!empty($additionalWhereClause)) 
				$query['where'] .= "AND $additionalWhereClause";
			
			//If we are explicitly looking for attachments.  Do not use a distinct query as the to_addr is defined
			//as a text which equals clob in oracle and the distinct query can not be executed correctly.
			$addDistinctKeyword = "";
			if (!empty($_REQUEST['attachmentsSearch']) && $_REQUEST['attachmentsSearch'] == 1) 
				//1 indicates yes
				$query['where'] .= " AND EXISTS ( SELECT id FROM notes n WHERE n.parent_id = emails.id AND n.deleted = 0 AND n.filename is not null )";
			 else 
				if (!empty($_REQUEST['attachmentsSearch']) && $_REQUEST['attachmentsSearch'] == 2) 
					$query['where'] .= " AND NOT EXISTS ( SELECT id FROM notes n WHERE n.parent_id = emails.id AND n.deleted = 0 AND n.filename is not null )";
			
			$fullQuery = "SELECT " . $query['select'] . " " . $query['joins'] . " " . $query['where'];
			
			return $fullQuery;
		}
		/**
     * Generate the where clause for searching imported emails.
     *
     */
		function _generateSearchImportWhereClause()
		{
			global $timedate;
			
			//The clear button was removed so if a user removes the asisgned user name, do not process the id.
			if (empty($_REQUEST['assigned_user_name']) && !empty($_REQUEST['assigned_user_id'])) 
				unset($_REQUEST['assigned_user_id']);
			
			$availableSearchParam = array('name' => array('table_name' => 'emails'), 
				'data_parent_id_search' => array('table_name' => 'emails', 'db_key' => 'parent_id', 'opp' => '='), 
				'assigned_user_id' => array('table_name' => 'emails', 'opp' => '='));
			
			$additionalWhereClause = array();
			foreach ($availableSearchParam as $key => $properties) {
				if (!empty($_REQUEST[$key])) {
					$db_key = isset($properties['db_key']) ? $properties['db_key'] : $key;
					$searchValue = $this->db->quote($_REQUEST[$key]);
					
					$opp = isset($properties['opp']) ? $properties['opp'] : 'like';
					if ($opp == 'like') 
						$searchValue = "%" . $searchValue . "%";
					
					$additionalWhereClause[] = "{$properties['table_name']}.$db_key $opp '$searchValue' ";
				}
			}
			
			
			
			$isDateFromSearchSet = !empty($_REQUEST['searchDateFrom']);
			$isdateToSearchSet = !empty($_REQUEST['searchDateTo']);
			$bothDateRangesSet = $isDateFromSearchSet & $isdateToSearchSet;
			
			//Hanlde date from and to separately
			if ($bothDateRangesSet) {
				$dbFormatDateFrom = $timedate->to_db_date($_REQUEST['searchDateFrom'], false);
				$dbFormatDateFrom = db_convert("'" . $dbFormatDateFrom . "'", 'datetime');
				
				$dbFormatDateTo = $timedate->to_db_date($_REQUEST['searchDateTo'], false);
				$dbFormatDateTo = db_convert("'" . $dbFormatDateTo . "'", 'datetime');
				
				$additionalWhereClause[] = "( emails.date_sent >= $dbFormatDateFrom AND
                                          emails.date_sent <= $dbFormatDateTo )";
			} elseif ($isdateToSearchSet) {
				$dbFormatDateTo = $timedate->to_db_date($_REQUEST['searchDateTo'], false);
				$dbFormatDateTo = db_convert("'" . $dbFormatDateTo . "'", 'datetime');
				$additionalWhereClause[] = "emails.date_sent <= $dbFormatDateTo ";
			} elseif ($isDateFromSearchSet) {
				$dbFormatDateFrom = $timedate->to_db_date($_REQUEST['searchDateFrom'], false);
				$dbFormatDateFrom = db_convert("'" . $dbFormatDateFrom . "'", 'datetime');
				$additionalWhereClause[] = "emails.date_sent >= $dbFormatDateFrom ";
			}
			
			$additionalWhereClause = implode(" AND ", $additionalWhereClause);
			
			return $additionalWhereClause;
		}
		
		
		
		/**
	 * takes a long TO: string of emails and returns the first appended by an
	 * elipse
	 */
		function trimLongTo($str)
		{
			if (strpos($str, ',')) {
				$exStr = explode(',', $str);
				return $exStr[0] . '...';
			} elseif (strpos($str, ';')) {
				$exStr = explode(';', $str);
				return $exStr[0] . '...';
			} else {
				return $str;
			}
		}
		
		function get_summary_text()
		{
			return $this->name;
		}
		
		
		
		function distributionForm($where)
		{
			global $app_list_strings;
			global $app_strings;
			global $mod_strings;
			global $theme;
			global $current_user;
			
			$distribution = get_select_options_with_id($app_list_strings['dom_email_distribution'], '');
			$_SESSION['distribute_where'] = $where;
			
			
			$out = '<form name="Distribute" id="Distribute">';
			$out .= get_form_header($mod_strings['LBL_DIST_TITLE'], '', false);
			$out .= <<<eoq
		<script>
			enableQS(true);
		</script>
eoq;
			$out .= 
				'
		<table cellpadding="0" cellspacing="0" width="100%" border="0">
			<tr>
				<td>
					<script type="text/javascript">


						function checkDeps(form) {
							return;
						}

						function mySubmit() {
							var assform = document.getElementById("Distribute");
							var select = document.getElementById("userSelect");
							var assign1 = assform.r1.checked;
							var assign2 = assform.r2.checked;
							var dist = assform.dm.value;
							var assign = false;
							var users = false;
							var rules = false;
							var warn1 = "' . $mod_strings['LBL_WARN_NO_USERS'] . 
				'";
							var warn2 = "";

							if(assign1 || assign2) {
								assign = true;

							}

							for(i=0; i<select.options.length; i++) {
								if(select.options[i].selected == true) {
									users = true;
									warn1 = "";
								}
							}

							if(dist != "") {
								rules = true;
							} else {
								warn2 = "' . $mod_strings['LBL_WARN_NO_DIST'] . 
				'";
							}

							if(assign && users && rules) {

								if(document.getElementById("r1").checked) {
									var mu = document.getElementById("MassUpdate");
									var grabbed = "";

									for(i=0; i<mu.elements.length; i++) {
										if(mu.elements[i].type == "checkbox" && mu.elements[i].checked && mu.elements[i].name.value != "massall") {
											if(grabbed != "") { grabbed += "::"; }
											grabbed += mu.elements[i].value;
										}
									}
									var formgrab = document.getElementById("grabbed");
									formgrab.value = grabbed;
								}
								assform.submit();
							} else {
								alert("' . $mod_strings['LBL_ASSIGN_WARN'] . 
				'" + "\n" + warn1 + "\n" + warn2);
							}
						}

						function submitDelete() {
							if(document.getElementById("r1").checked) {
								var mu = document.getElementById("MassUpdate");
								var grabbed = "";

								for(i=0; i<mu.elements.length; i++) {
									if(mu.elements[i].type == "checkbox" && mu.elements[i].checked && mu.elements[i].name != "massall") {
										if(grabbed != "") { grabbed += "::"; }
										grabbed += mu.elements[i].value;
									}
								}
								var formgrab = document.getElementById("grabbed");
								formgrab.value = grabbed;
							}
							if(grabbed == "") {
								alert("' . $mod_strings['LBL_MASS_DELETE_ERROR'] . 
				'");
							} else {
								document.getElementById("Distribute").submit();
							}
						}

					</script>
						<input type="hidden" name="module" value="Emails">
						<input type="hidden" name="action" id="action">
						<input type="hidden" name="grabbed" id="grabbed">

					<table cellpadding="1" cellspacing="0" width="100%" border="0" class="edit view">
						<tr height="20">
							<td scope="col" scope="row" NOWRAP align="center">
								&nbsp;' . $mod_strings['LBL_ASSIGN_SELECTED_RESULTS_TO'] . '&nbsp;';
			$out .= $this->userSelectTable();
			$out .= 
				'</td>
							<td scope="col" scope="row" NOWRAP align="left">
								&nbsp;' . $mod_strings['LBL_USING_RULES'] . 
				'&nbsp;
								<select name="distribute_method" id="dm" onChange="checkDeps(this.form);">' . $distribution . 
				'</select>
							</td>';
			
			
			$out .= 
				'</td>
							</tr>';
			
			
			$out .= 
				'<tr>
								<td scope="col" width="50%" scope="row" NOWRAP align="right" colspan="2">
								<input title="' . $mod_strings['LBL_BUTTON_DISTRIBUTE_TITLE'] . 
				'"
									id="dist_button"
									class="button" onClick="AjaxObject.detailView.handleAssignmentDialogAssignAction();"
									type="button" name="button"
									value="  ' . $mod_strings['LBL_BUTTON_DISTRIBUTE'] . '  ">';
			$out .= 
				'</tr>
					</table>
				</td>
			</tr>
		</table>
		</form>';
			return $out;
		}
		
		function userSelectTable()
		{
			global $theme;
			global $mod_strings;
			
			$colspan = 1;
			$setTeamUserFunction = '';
			
			
			// get users
			$r = $this->db->query("SELECT users.id, users.user_name, users.first_name, users.last_name FROM users WHERE deleted=0 AND status = 'Active' AND is_group=0 ORDER BY users.last_name, users.first_name");
			
			$userTable = '<table cellpadding="0" cellspacing="0" border="0">';
			$userTable .= '<tr><td colspan="2"><b>' . $mod_strings['LBL_USER_SELECT'] . '</b></td></tr>';
			$userTable .= '<tr><td><input type="checkbox" style="border:0px solid #000000" onClick="toggleAll(this); setCheckMark(); checkDeps(this.form);"></td> <td>' . $mod_strings['LBL_TOGGLE_ALL'] . '</td></tr>';
			$userTable .= '<tr><td colspan="2"><select style="visibility:hidden;" name="users[]" id="userSelect" multiple size="12">';
			
			while ($a = $this->db->fetchByAssoc($r)) {
				$userTable .= '<option value="' . $a['id'] . '" id="' . $a['id'] . '">' . $a['first_name'] . ' ' . $a['last_name'] . '</option>';
			}
			$userTable .= '</select></td></tr>';
			$userTable .= '</table>';
			
			$out = '<script type="text/javascript">';
			$out .= $setTeamUserFunction;
			$out .= 
				'
					function setCheckMark() {
						var select = document.getElementById("userSelect");

						for(i=0 ; i<select.options.length; i++) {
							if(select.options[i].selected == true) {
								document.getElementById("checkMark").style.display="";
								return;
							}
						}

						document.getElementById("checkMark").style.display="none";
						return;
					}

					function showUserSelect() {
						var targetTable = document.getElementById("user_select");
						targetTable.style.visibility="visible";
						var userSelectTable = document.getElementById("userSelect");
						userSelectTable.style.visibility="visible";
						return;
					}
					function hideUserSelect() {
						var targetTable = document.getElementById("user_select");
						targetTable.style.visibility="hidden";
						var userSelectTable = document.getElementById("userSelect");
						userSelectTable.style.visibility="hidden";
						return;
					}
					function toggleAll(toggle) {
						if(toggle.checked) {
							var stat = true;
						} else {
							var stat = false;
						}
						var form = document.getElementById("userSelect");
						for(i=0; i<form.options.length; i++) {
							form.options[i].selected = stat;
						}
					}


				</script>
			<span id="showUsersDiv" style="position:relative;">
				<a href="#" id="showUsers" onClick="javascript:showUserSelect();">
					' . SugarThemeRegistry::current()->getImage('Users', '', null, null, ".gif", $mod_strings['LBL_USERS']) . 
				'</a>&nbsp;
				<a href="#" id="showUsers" onClick="javascript:showUserSelect();">
					<span style="display:none;" id="checkMark">' . SugarThemeRegistry::current()->getImage('check_inline', 'border="0"', null, null, ".gif", $mod_strings['LBL_CHECK_INLINE']) . 
				'</span>
				</a>


				<div id="user_select" style="width:200px;position:absolute;left:2;top:2;visibility:hidden;z-index:1000;">
				<table cellpadding="0" cellspacing="0" border="0" class="list view">
					<tr height="20">
						<td  colspan="' . $colspan . 
				'" id="hiddenhead" onClick="hideUserSelect();" onMouseOver="this.style.border = \'outset red 1px\';" onMouseOut="this.style.border = \'inset white 0px\';this.style.borderBottom = \'inset red 1px\';">
							<a href="#" onClick="javascript:hideUserSelect();">' . SugarThemeRegistry::current()->getImage('close', 'border="0"', null, null, ".gif", $mod_strings['LBL_CLOSE']) . 
				'</a>
							' . $mod_strings['LBL_USER_SELECT'] . 
				'
						</td>
					</tr>
					<tr>';
			//<td valign="middle" height="30"  colspan="'.$colspan.'" id="hiddenhead" onClick="hideUserSelect();" onMouseOver="this.style.border = \'outset red 1px\';" onMouseOut="this.style.border = \'inset white 0px\';this.style.borderBottom = \'inset red 1px\';">
			$out .= 
				'		<td style="padding:5px" class="oddListRowS1" bgcolor="#fdfdfd" valign="top" align="left" style="left:0;top:0;">
							' . $userTable . 
				'
						</td>
					</tr>
				</table></div>
			</span>';
			return $out;
		}
		
		function checkInbox($type)
		{
			global $theme;
			global $mod_strings;
			$out = '<div><input	title="' . $mod_strings['LBL_BUTTON_CHECK_TITLE'] . 
				'"
						class="button"
						type="button" name="button"
						onClick="window.location=\'index.php?module=Emails&action=Check&type=' . $type . 
				'\';"
						style="margin-bottom:2px"
						value="  ' . $mod_strings['LBL_BUTTON_CHECK'] . '  "></div>';
			return $out;
		}
		
		/**
         * Guesses Primary Parent id from From: email address.  Cascades guesses from Accounts to Contacts to Leads to
         * Users.  This will not affect the many-to-many relationships already constructed as this is, at best,
         * informational linking.
         */
		function fillPrimaryParentFields()
		{
			if (empty($this->from_addr)) 
				return;
			
			$GLOBALS['log']->debug("*** Email trying to guess Primary Parent from address [ {$this->from_addr} ]");
			
			$tables = array('accounts');
			$ret = array();
			// loop through types to get hits
			foreach ($tables as $table) {
				$q = "SELECT name, id FROM {$table} WHERE email1 = '{$this->from_addr}' OR email2 = '{$this->from_addr}' AND deleted = 0";
				$r = $this->db->query($q);
				while ($a = $this->db->fetchByAssoc($r)) {
					if (!empty($a['name']) && !empty($a['id'])) {
						$this->parent_type = ucwords($table);
						$this->parent_id = $a['id'];
						$this->parent_name = $a['name'];
						return;
					}
				}
			}
		}
		
		/**
         * Convert reference to inline image (stored as Note) to URL link
         * Enter description here ...
         * @param string $note ID of the note
         * @param string $ext type of the note
         */
		public function cid2Link($noteId, $noteType)
		{
			if (empty($this->description_html)) 
				return;
			list($type, $subtype) = explode('/', $noteType);
			if (strtolower($type) != 'image') {
				return;
			}
			$upload = new UploadFile();
			$this->description_html = preg_replace("#class=\"image\" src=\"cid:$noteId\.(.+?)\"#", "class=\"image\" src=\"{$this->imagePrefix}{$noteId}.\\1\"", $this->description_html);
			// ensure the image is in the cache
			$imgfilename = sugar_cached("images/") . "$noteId." . strtolower($subtype);
			$src = "upload://$noteId";
			if (!file_exists($imgfilename) && file_exists($src)) {
				copy($src, $imgfilename);
			}
		}
		
		/**
         * Convert all cid: links in this email into URLs
         */
		function cids2Links()
		{
			if (empty($this->description_html)) 
				return;
			$q = "SELECT id, file_mime_type FROM notes WHERE parent_id = '{$this->id}' AND deleted = 0";
			$r = $this->db->query($q);
			while ($a = $this->db->fetchByAssoc($r)) {
				$this->cid2Link($a['id'], $a['file_mime_type']);
			}
		}
		
		/**
     * Bugs 50972, 50973
     * Sets the field def for a field to allow null values
     *
     * @todo Consider moving to SugarBean to allow other models to set fields to NULL
     * @param string $field The field name to modify
     * @return void
     */
		public function setFieldNullable($field)
		{
			if (isset($this->field_defs[$field]) && is_array($this->field_defs[$field])) {
				if (empty($this->modifiedFieldDefs[$field])) {
					if (isset($this->field_defs[$field]['isnull']) && (strtolower($this->field_defs[$field]['isnull']) == 'false' || $this->field_defs[$field]['isnull'] === false)) {
						$this->modifiedFieldDefs[$field]['isnull'] = $this->field_defs[$field]['isnull'];
						unset($this->field_defs[$field]['isnull']);
					}
					
					if (isset($this->field_defs[$field]['dbType']) && $this->field_defs[$field]['dbType'] == 'id') {
						$this->modifiedFieldDefs[$field]['dbType'] = $this->field_defs[$field]['dbType'];
						unset($this->field_defs[$field]['dbType']);
					}
				}
			}
		}
		
		/**
     * Bugs 50972, 50973
     * Set the field def back to the way it was prior to modification
     *
     * @param $field
     * @return void
     */
		public function revertFieldNullable($field)
		{
			if (!empty($this->modifiedFieldDefs[$field]) && is_array($this->modifiedFieldDefs[$field])) {
				foreach ($this->modifiedFieldDefs[$field] as $k => $v) {
					$this->field_defs[$field][$k] = $v;
				}
				
				unset($this->modifiedFieldDefs[$field]);
			}
		}
	}
	// end class def
	
	
	//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description:
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc. All Rights
 * Reserved. Contributor(s): ______________________________________..
 *********************************************************************************/
	
	require_once ("include/ytree/Tree.php");
	require_once ("include/ytree/ExtNode.php");
	require_once ("include/SugarFolders/SugarFolders.php");
	
	
	
	class EmailUI
	{
		var $db;
		var $folder; // place holder for SugarFolder object
		var $folderStates = array(); // array of folderPath names and their states (1/0)
		var $smarty;
		var $addressSeparators = array(";", ",");
		var $rolloverStyle = "<style>div#rollover {position: relative;float: left;margin: none;text-decoration: none;}div#rollover a:hover {padding: 0;}div#rollover a span {display: none;}div#rollover a:hover span {text-decoration: none;display: block;width: 250px;margin-top: 5px;margin-left: 5px;position: absolute;padding: 10px;color: #333;	border: 1px solid #ccc;	background-color: #fff;	font-size: 12px;z-index: 1000;}</style>\n";
		var $groupCss = "<span class='groupInbox'>";
		// 24 hours
		// 5 mins
		// 24 hours
		var $cacheTimeouts = array(
			'messages' => 86400, 
			'folders' => 300, 
			'attachments' => 86400);
		var $userCacheDir = '';
		var $coreDynamicFolderQuery = 
			"SELECT emails.id polymorphic_id, 'Emails' polymorphic_module FROM emails
								   JOIN emails_text on emails.id = emails_text.email_id
                                   WHERE (type = '::TYPE::' OR status = '::STATUS::') AND assigned_user_id = '::USER_ID::' AND emails.deleted = '0'";
		
		/**
	 * Sole constructor
	 */
		function EmailUI()
		{
			global $sugar_config;
			global $current_user;
			
			$folderStateSerial = $current_user->getPreference('folderOpenState', 'Emails');
			
			if (!empty($folderStateSerial)) {
				$this->folderStates = unserialize($folderStateSerial);
			}
			
			$this->smarty = new Sugar_Smarty();
			$this->folder = new SugarFolder();
			$this->userCacheDir = sugar_cached("modules/Emails/{$current_user->id}");
			$this->db = DBManagerFactory::getInstance();
		}
		
		
		///////////////////////////////////////////////////////////////////////////
		////	CORE
		/**
	 * Renders the frame for emails
	 */
		function displayEmailFrame()
		{
			
			require_once ("include/OutboundEmail/OutboundEmail.php");
			
			global $app_strings, $app_list_strings;
			global $mod_strings;
			global $sugar_config;
			global $current_user;
			global $locale;
			global $timedate;
			global $theme;
			global $sugar_version;
			global $sugar_flavor;
			global $current_language;
			global $server_unique_key;
			
			$this->preflightUserCache();
			$ie = new InboundEmail();
			
			// focus listView
			$list = array(
				'mbox' => 'Home', 
				'ieId' => '', 
				'name' => 'Home', 
				'unreadChecked' => 0, 
				'out' => array());
			
			$this->_generateComposeConfigData('email_compose');
			
			
			//Check quick create module access
			$QCAvailableModules = $this->_loadQuickCreateModules();
			
			//Get the quickSearch js needed for assigned user id on Search Tab
			require_once ('include/QuickSearchDefaults.php');
			$qsd = QuickSearchDefaults::getQuickSearchDefaults();
			$qsd->setFormName('advancedSearchForm');
			$quicksearchAssignedUser = "if(typeof sqs_objects == 'undefined'){var sqs_objects = new Array;}";
			$quicksearchAssignedUser .= "sqs_objects['advancedSearchForm_assigned_user_name']=" . json_encode($qsd->getQSUser()) . ";";
			$qsd->setFormName('Distribute');
			$quicksearchAssignedUser .= "sqs_objects['Distribute_assigned_user_name']=" . json_encode($qsd->getQSUser()) . ";";
			$this->smarty->assign('quickSearchForAssignedUser', $quicksearchAssignedUser);
			
			
			///////////////////////////////////////////////////////////////////////
			////	BASIC ASSIGNS
			$this->smarty->assign("currentUserId", $current_user->id);
			$this->smarty->assign("CURRENT_USER_EMAIL", $current_user->email1);
			$this->smarty->assign("currentUserName", $current_user->name);
			$this->smarty->assign('yuiPath', 'modules/Emails/javascript/yui-ext/');
			$this->smarty->assign('app_strings', $app_strings);
			$this->smarty->assign('mod_strings', $mod_strings);
			$this->smarty->assign('theme', $theme);
			$this->smarty->assign('sugar_config', $sugar_config);
			$this->smarty->assign('is_admin', $current_user->is_admin);
			$this->smarty->assign('sugar_version', $sugar_version);
			$this->smarty->assign('sugar_flavor', $sugar_flavor);
			$this->smarty->assign('current_language', $current_language);
			$this->smarty->assign('server_unique_key', $server_unique_key);
			$this->smarty->assign('qcModules', json_encode($QCAvailableModules));
			$extAllDebugValue = "ext-all.js";
			$this->smarty->assign('extFileName', $extAllDebugValue);
			
			// settings: general
			$e2UserPreferences = $this->getUserPrefsJS();
			$emailSettings = $e2UserPreferences['emailSettings'];
			
			///////////////////////////////////////////////////////////////////////
			////	USER SETTINGS
			// settings: accounts
			
			$cuDatePref = $current_user->getUserDateTimePreferences();
			$this->smarty->assign('dateFormat', $cuDatePref['date']);
			$this->smarty->assign('dateFormatExample', str_replace(array("Y", "m", "d"), array("yyyy", "mm", "dd"), $cuDatePref['date']));
			$this->smarty->assign('calFormat', $timedate->get_cal_date_format());
			$this->smarty->assign('TIME_FORMAT', $timedate->get_user_time_format());
			
			$ieAccounts = $ie->retrieveByGroupId($current_user->id);
			$ieAccountsOptions = "<option value=''>{$app_strings['LBL_NONE']}</option>\n";
			
			foreach ($ieAccounts as $k => $v) {
				$disabled = (!$v->is_personal) ? "DISABLED" : "";
				$group = (!$v->is_personal) ? $app_strings['LBL_EMAIL_GROUP'] . "." : "";
				$ieAccountsOptions .= "<option value='{$v->id}' $disabled>{$group}{$v->name}</option>\n";
			}
			
			$this->smarty->assign('ieAccounts', $ieAccountsOptions);
			$this->smarty->assign('rollover', $this->rolloverStyle);
			
			$protocol = filterInboundEmailPopSelection($app_list_strings['dom_email_server_type']);
			$this->smarty->assign('PROTOCOL', get_select_options_with_id($protocol, ''));
			$this->smarty->assign('MAIL_SSL_OPTIONS', get_select_options_with_id($app_list_strings['email_settings_for_ssl'], ''));
			$this->smarty->assign('ie_mod_strings', return_module_language($current_language, 'InboundEmail'));
			
			$charsetSelectedValue = isset($emailSettings['defaultOutboundCharset']) ? $emailSettings['defaultOutboundCharset'] : false;
			if (!$charsetSelectedValue) {
				$charsetSelectedValue = $current_user->getPreference('default_export_charset', 'global');
				if (!$charsetSelectedValue) {
					$charsetSelectedValue = $locale->getPrecedentPreference('default_email_charset');
				}
			}
			$charset = array(
				'options' => $locale->getCharsetSelect(), 
				'selected' => $charsetSelectedValue);
			$this->smarty->assign('charset', $charset);
			
			$emailCheckInterval = array('options' => $app_strings['LBL_EMAIL_CHECK_INTERVAL_DOM'], 'selected' => $emailSettings['emailCheckInterval']);
			$this->smarty->assign('emailCheckInterval', $emailCheckInterval);
			$this->smarty->assign('attachmentsSearchOptions', $app_list_strings['checkbox_dom']);
			$this->smarty->assign('sendPlainTextChecked', ($emailSettings['sendPlainText'] == 1) ? 'CHECKED' : '');
			$this->smarty->assign('showNumInList', get_select_options_with_id($app_list_strings['email_settings_num_dom'], $emailSettings['showNumInList']));
			
			////	END USER SETTINGS
			///////////////////////////////////////////////////////////////////////
			
			///////////////////////////////////////////////////////////////////////
			////	SIGNATURES
			$prependSignature = ($current_user->getPreference('signature_prepend')) ? 'true' : 'false';
			$defsigID = $current_user->getPreference('signature_default');
			$this->smarty->assign('signatures', $current_user->getSignatures(false, $defsigID));
			$this->smarty->assign('signaturesSettings', $current_user->getSignatures(false, $defsigID, false));
			$signatureButtons = $current_user->getSignatureButtons('SUGAR.email2.settings.createSignature', !empty($defsigID));
			if (!empty($defsigID)) {
				$signatureButtons = $signatureButtons . '<span name="delete_sig" id="delete_sig" style="visibility:inherit;"><input class="button" onclick="javascript:SUGAR.email2.settings.deleteSignature();" value="' . $app_strings['LBL_EMAIL_DELETE'] . 
					'" type="button" tabindex="392">&nbsp;
					</span>';
			} else {
				$signatureButtons = $signatureButtons . '<span name="delete_sig" id="delete_sig" style="visibility:hidden;"><input class="button" onclick="javascript:SUGAR.email2.settings.deleteSignature();" value="' . $app_strings['LBL_EMAIL_DELETE'] . 
					'" type="button" tabindex="392">&nbsp;
					</span>';
			}
			$this->smarty->assign('signatureButtons', $signatureButtons);
			$this->smarty->assign('signaturePrepend', $prependSignature == 'true' ? 'CHECKED' : '');
			////	END SIGNATURES
			///////////////////////////////////////////////////////////////////////
			
			///////////////////////////////////////////////////////////////////////
			////	EMAIL TEMPLATES
			$email_templates_arr = $this->getEmailTemplatesArray();
			natcasesort($email_templates_arr);
			$this->smarty->assign('EMAIL_TEMPLATE_OPTIONS', get_select_options_with_id($email_templates_arr, ''));
			////	END EMAIL TEMPLATES
			///////////////////////////////////////////////////////////////////////
			
			///////////////////////////////////////////////////////////////////////
			////	FOLDERS & TreeView
			$this->smarty->assign('groupUserOptions', $ie->getGroupsWithSelectOptions(array('' => $app_strings['LBL_EMAIL_CREATE_NEW'])));
			
			$tree = $this->getMailboxNodes();
			
			// preloaded folder
			$preloadFolder = 'lazyLoadFolder = ';
			$focusFolderSerial = $current_user->getPreference('focusFolder', 'Emails');
			if (!empty($focusFolderSerial)) {
				$focusFolder = unserialize($focusFolderSerial);
				//$focusFolder['ieId'], $focusFolder['folder']
				$preloadFolder .= json_encode($focusFolder) . ";";
			} else {
				$preloadFolder .= "new Object();";
			}
			////	END FOLDERS
			///////////////////////////////////////////////////////////////////////
			
			$out = "";
			$out .= $this->smarty->fetch("modules/Emails/templates/_baseEmail.tpl");
			$out .= $tree->generate_header();
			$out .= $tree->generateNodesNoInit(true, 'email2treeinit');
			$out .= <<<eoq
			<script type="text/javascript" language="javascript">

				var loader = new YAHOO.util.YUILoader({
				    require : [
				    	"layout", "element", "tabview", "menu",
				    	"cookie", "sugarwidgets"
				    ],
				    loadOptional: true,
				    skin: { base: 'blank', defaultSkin: '' },
				    onSuccess: email2init,
				    allowRollup: true,
				    base: "include/javascript/yui/build/"
				});
				loader.addModule({
				    name :"sugarwidgets",
				    type : "js",
				    fullpath: "include/javascript/sugarwidgets/SugarYUIWidgets.js",
				    varName: "YAHOO.SUGAR",
				    requires: ["datatable", "dragdrop", "treeview", "tabview", "calendar"]
				});
				loader.insert();

				{$preloadFolder};

			</script>
eoq;
			
			
			return $out;
		}
		
		/**
	 * Generate the frame needed for the quick compose email UI.  This frame is loaded dynamically
	 * by an ajax call.
	 *
	 * @return JSON An object containing html markup and js script variables.
	 */
		function displayQuickComposeEmailFrame()
		{
			$this->preflightUserCache();
			
			$this->_generateComposeConfigData('email_compose_light');
			$javascriptOut = $this->smarty->fetch("modules/Emails/templates/_baseConfigData.tpl");
			
			$divOut = $this->smarty->fetch("modules/Emails/templates/overlay.tpl");
			$divOut .= $this->smarty->fetch("modules/Emails/templates/addressSearchContent.tpl");
			
			$outData = array('jsData' => $javascriptOut, 'divData' => $divOut);
			$out = json_encode($outData);
			return $out;
		}
		
		/**
     * Load the modules from the metadata file and include in a custom one if it exists
     *
     * @return array
     */
		protected function _loadQuickCreateModules()
		{
			$QCAvailableModules = array();
			$QCModules = array();
			
			include ('modules/Emails/metadata/qcmodulesdefs.php');
			if (file_exists('custom/modules/Emails/metadata/qcmodulesdefs.php')) {
				include ('custom/modules/Emails/metadata/qcmodulesdefs.php');
			}
			
			foreach ($QCModules as $module) {
				$seed = SugarModule::get($module)->loadBean();
				if (($seed instanceof SugarBean) && $seed->ACLAccess('edit')) {
					$QCAvailableModules[] = $module;
				}
			}
			
			return $QCAvailableModules;
		}
		
		/**
     * Given an email link url (eg. index.php?action=Compose&parent_type=Contacts...) break up the
     * request components and create a compose package that can be used by the quick compose UI. The
     * result is typically passed into the js call SUGAR.quickCompose.init which initalizes the quick compose
     * UI.
     *
     * @param String $emailLinkUrl
     * @return JSON Object containing the composePackage and full link url
     */
		function generateComposePackageForQuickCreateFromComposeUrl($emailLinkUrl, $lazyLoad = false)
		{
			//TODO (MT): specify type of proc. parameters
			$composeData = explode("&", $emailLinkUrl);
			$a_composeData = array();
			foreach ($composeData as $singleRequest) {
				$tmp = explode("=", $singleRequest);
				$a_composeData[$tmp[0]] = urldecode($tmp[1]);
			}
			
			return $this->generateComposePackageForQuickCreate($a_composeData, $emailLinkUrl, $lazyLoad);
		}
		
		
		/**
     * Generate the composePackage for the quick compose email UI.  The package contains
     * key/value pairs generated by the Compose.php file which are then set into the
     * quick compose email UI (eg. to addr, parent id, parent type, etc)
     *
     * @param Array $composeData Associative array read and processed by generateComposeDataPackage.
     * @param String $fullLinkUrl A link that contains all pertinant information so the user can be
     *                              directed to the full compose screen if needed
     * @param SugarBean $bean Optional - the parent object bean with data
     * @return JSON Object containg composePackage and fullLinkUrl
     */
		function generateComposePackageForQuickCreate($composeData, $fullLinkUrl, $lazyLoad = false, $bean = null)
		{
			//TODO (MT): specify type of proc. parameters
			$_REQUEST['forQuickCreate'] = true;
			
			if (!$lazyLoad) {
				require_once ('modules/Emails/Compose.php');
				$composePackage = generateComposeDataPackage($composeData, FALSE, $bean);
			} else {
				$composePackage = $composeData;
			}
			
			// JSON object is passed into the function defined within the a href onclick event
			// which is delimeted by '.  Need to escape all single quotes and &, <, >
			// but not double quotes since json would escape them
			foreach ($composePackage as $key => $singleCompose) {
				if (is_string($singleCompose)) 
					$composePackage[$key] = str_replace("&nbsp;", " ", from_html($singleCompose));
			}
			
			$quickComposeOptions = array('fullComposeUrl' => $fullLinkUrl, 'composePackage' => $composePackage);
			
			$j_quickComposeOptions = JSON::encode($quickComposeOptions, false, true);
			
			
			return $j_quickComposeOptions;
		}
		
		/**
     * Generate the config data needed for the Full Compose UI and the Quick Compose UI.  The set of config data
     * returned is the minimum set needed by the quick compose UI.
     *
     * @param String $type Drives which tinyMCE options will be included.
     */
		function _generateComposeConfigData($type = "email_compose_light")
		{
			global $app_list_strings, $current_user, $app_strings, $mod_strings, $current_language, $locale;
			
			//Link drop-downs
			$parent_types = $app_list_strings['record_type_display'];
			$disabled_parent_types = ACLController::disabledModuleList($parent_types, false, 'list');
			
			foreach ($disabled_parent_types as $disabled_parent_type) {
				unset($parent_types[$disabled_parent_type]);
			}
			asort($parent_types);
			$linkBeans = json_encode(get_select_options_with_id($parent_types, ''));
			
			//TinyMCE Config
			require_once ("include/SugarTinyMCE.php");
			$tiny = new SugarTinyMCE();
			$tinyConf = $tiny->getConfig($type);
			
			//Generate Language Packs
			$lang = "var app_strings = new Object();\n";
			foreach ($app_strings as $k => $v) {
				if (strpos($k, 'LBL_EMAIL_') !== false) {
					$lang .= "app_strings.{$k} = '{$v}';\n";
				}
			}
			//Get the email mod strings but don't use the global variable as this may be overridden by
			//other modules when the quick create is rendered.
			$email_mod_strings = return_module_language($current_language, 'Emails');
			$modStrings = "var mod_strings = new Object();\n";
			foreach ($email_mod_strings as $k => $v) {
				$v = str_replace("'", "\'", $v);
				$modStrings .= "mod_strings.{$k} = '{$v}';\n";
			}
			$lang .= "\n\n{$modStrings}\n";
			
			//Grab the Inboundemail language pack
			$ieModStrings = "var ie_mod_strings = new Object();\n";
			$ie_mod_strings = return_module_language($current_language, 'InboundEmail');
			foreach ($ie_mod_strings as $k => $v) {
				$v = str_replace("'", "\'", $v);
				$ieModStrings .= "ie_mod_strings.{$k} = '{$v}';\n";
			}
			$lang .= "\n\n{$ieModStrings}\n";
			
			$this->smarty->assign('linkBeans', $linkBeans);
			$this->smarty->assign('linkBeansOptions', $parent_types);
			$this->smarty->assign('tinyMCE', $tinyConf);
			$this->smarty->assign('lang', $lang);
			$this->smarty->assign('app_strings', $app_strings);
			$this->smarty->assign('mod_strings', $email_mod_strings);
			$ie1 = new InboundEmail();
			
			//Signatures
			$defsigID = $current_user->getPreference('signature_default');
			$defaultSignature = $current_user->getDefaultSignature();
			$sigJson = !empty($defaultSignature) ? json_encode(array($defaultSignature['id'] => from_html($defaultSignature['signature_html']))) : "new Object()";
			$this->smarty->assign('defaultSignature', $sigJson);
			$this->smarty->assign('signatureDefaultId', (isset($defaultSignature['id'])) ? $defaultSignature['id'] : "");
			//User Preferences
			$this->smarty->assign('userPrefs', json_encode($this->getUserPrefsJS()));
			
			//Get the users default outbound id
			$defaultOutID = $ie1->getUsersDefaultOutboundServerId($current_user);
			$this->smarty->assign('defaultOutID', $defaultOutID);
			
			//Character Set
			$charsets = json_encode($locale->getCharsetSelect());
			$this->smarty->assign('emailCharsets', $charsets);
			
			//Relateable List of People for address book search
			//#20776 jchi
			$peopleTables = array("users", 
				"contacts", 
				"leads", 
				"prospects", 
				"accounts");
			$filterPeopleTables = array();
			global $app_list_strings, $app_strings;
			$filterPeopleTables['LBL_DROPDOWN_LIST_ALL'] = $app_strings['LBL_DROPDOWN_LIST_ALL'];
			foreach ($peopleTables as $table) {
				$module = ucfirst($table);
				$class = substr($module, 0, strlen($module) - 1);
				require_once ("modules/{$module}/{$class}.php");
				$person = new $class();
				
				if (!$person->ACLAccess('list')) 
					continue;
				$filterPeopleTables[$person->table_name] = $app_list_strings['moduleList'][$person->module_dir];
			}
			$this->smarty->assign('listOfPersons', get_select_options_with_id($filterPeopleTables, ''));
			
		}
		
		
		
		////	END CORE
		///////////////////////////////////////////////////////////////////////////
		
		///////////////////////////////////////////////////////////////////////////
		////	ADDRESS BOOK
		/**
	 * Retrieves all relationship metadata for a user's address book
	 * @return array
	 */
		function getContacts()
		{
			global $current_user;
			
			$q = "SELECT * FROM address_book WHERE assigned_user_id = '{$current_user->id}' ORDER BY bean DESC";
			$r = $this->db->query($q);
			
			$ret = array();
			
			while ($a = $this->db->fetchByAssoc($r)) {
				$ret[$a['bean_id']] = array(
					'id' => $a['bean_id'], 
					'module' => $a['bean']);
			}
			
			return $ret;
		}
		
		/**
	 * Saves changes to a user's address book
	 * @param array contacts
	 */
		function setContacts($contacts)
		{
			//TODO (MT): specify type of proc. parameters
			global $current_user;
			
			$oldContacts = $this->getContacts();
			
			foreach ($contacts as $cid => $contact) {
				if (!in_array($contact['id'], $oldContacts)) {
					$q = "INSERT INTO address_book (assigned_user_id, bean, bean_id) VALUES ('{$current_user->id}', '{$contact['module']}', '{$contact['id']}')";
					$r = $this->db->query($q, true);
				}
			}
		}
		
		/**
	 * Removes contacts from the user's address book
	 * @param array ids
	 */
		function removeContacts($ids)
		{
			//TODO (MT): specify type of proc. parameters
			global $current_user;
			
			$concat = "";
			
			foreach ($ids as $id) {
				if (!empty($concat)) 
					$concat .= ", ";
				
				$concat .= "'{$id}'";
			}
			
			$q = "DELETE FROM address_book WHERE assigned_user_id = '{$current_user->id}' AND bean_id IN ({$concat})";
			$r = $this->db->query($q);
		}
		
		/**
	 * saves editted Contact info
	 * @param string $str JSON serialized object
	 */
		function saveContactEdit($str)
		{
			
			//TODO (MT): specify type of proc. parameters
			$json = getJSONobj();
			
			$str = from_html($str);
			$obj = $json->decode($str);
			
			$contact = new Contact();
			$contact->retrieve($obj['contact_id']);
			$contact->first_name = $obj['contact_first_name'];
			$contact->last_name = $obj['contact_last_name'];
			$contact->save();
			
			// handle email address changes
			$addresses = array();
			
			foreach ($obj as $k => $req) {
				if (strpos($k, 'emailAddress') !== false) {
					$addresses[$k] = $req;
				}
			}
			
			// prefill some REQUEST vars for emailAddress save
			$_REQUEST['emailAddressOptOutFlag'] = $obj['optOut'];
			$_REQUEST['emailAddressInvalidFlag'] = $obj['invalid'];
			$contact->emailAddress->save($obj['contact_id'], 'Contacts', $addresses, $obj['primary'], '');
		}
		
		/**
	 * Prepares the Edit Contact mini-form via template assignment
	 * @param string id ID of contact in question
	 * @param string module Module in focus
	 * @return array
	 */
		function getEditContact($id, $module)
		{
			global $app_strings;
			
			
			if (!class_exists("Contact")) {
				
			}
			
			$contact = new Contact();
			$contact->retrieve($_REQUEST['id']);
			$ret = array();
			
			if ($contact->ACLAccess('edit')) {
				$contactMeta = array();
				$contactMeta['id'] = $contact->id;
				$contactMeta['module'] = $contact->module_dir;
				$contactMeta['first_name'] = $contact->first_name;
				$contactMeta['last_name'] = $contact->last_name;
				
				$this->smarty->assign("app_strings", $app_strings);
				$this->smarty->assign("contact_strings", return_module_language($_SESSION['authenticated_user_language'], 'Contacts'));
				$this->smarty->assign("contact", $contactMeta);
				
				$ea = new SugarEmailAddress();
				$newEmail = $ea->getEmailAddressWidgetEditView($id, $module, true);
				$this->smarty->assign("emailWidget", $newEmail['html']);
				
				$ret['form'] = $this->smarty->fetch("modules/Emails/templates/editContact.tpl");
				$ret['prefillData'] = $newEmail['prefillData'];
			} else {
				$id = "";
				$ret['form'] = $app_strings['LBL_EMAIL_ERROR_NO_ACCESS'];
				$ret['prefillData'] = '{}';
			}
			
			$ret['id'] = $id;
			$ret['contactName'] = $contact->full_name;
			
			return $ret;
		}
		
		
		/**
	 * Retrieves a concatenated list of contacts, those with assigned_user_id = user's id and those in the address_book
	 * table
	 * @param array $contacts Array of contact types -> IDs
	 * @param object $user User in focus
	 * @return array
	 */
		function getUserContacts($contacts, $user = null)
		{
			
			global $current_user;
			global $locale;
			
			if (empty($user)) {
				$user = $current_user;
			}
			
			$emailAddress = new SugarEmailAddress();
			$ret = array();
			
			$union = '';
			
			$modules = array();
			foreach ($contacts as $contact) {
				if (!isset($modules[$contact['module']])) {
					$modules[$contact['module']] = array();
				}
				$modules[$contact['module']][] = $contact;
			}
			
			foreach ($modules as $module => $contacts) {
				if (!empty($union)) {
					$union .= " UNION ALL ";
				}
				
				$table = strtolower($module);
				$idsSerial = '';
				
				foreach ($contacts as $contact) {
					if (!empty($idsSerial)) {
						$idsSerial .= ",";
					}
					$idsSerial .= "'{$contact['id']}'";
				}
				
				$union .= "(SELECT id, first_name, last_name, title, '{$module}' module FROM {$table} WHERE id IN({$idsSerial}) AND deleted = 0 )";
			}
			if (!empty($union)) {
				$union .= " ORDER BY last_name";
			}
			
			$r = $user->db->query($union);
			
			//_pp($union);
			
			while ($a = $user->db->fetchByAssoc($r)) {
				$c = array();
				
				$c['name'] = $locale->getLocaleFormattedName($a['first_name'], "<b>{$a['last_name']}</b>", '', $a['title'], '', $user);
				$c['id'] = $a['id'];
				$c['module'] = $a['module'];
				$c['email'] = $emailAddress->getAddressesByGUID($a['id'], $a['module']);
				$ret[$a['id']] = $c;
			}
			
			return $ret;
		}
		////	END ADDRESS BOOK
		///////////////////////////////////////////////////////////////////////////
		
		
		///////////////////////////////////////////////////////////////////////////
		////	EMAIL 2.0 Preferences
		function getUserPrefsJS()
		{
			global $current_user;
			global $locale;
			
			// sort order per mailbox view
			$sortSerial = $current_user->getPreference('folderSortOrder', 'Emails');
			$sortArray = array();
			if (!empty($sortSerial)) {
				$sortArray = unserialize($sortSerial);
			}
			
			// treeview collapsed/open states
			$folderStateSerial = $current_user->getPreference('folderOpenState', 'Emails');
			$folderStates = array();
			if (!empty($folderStateSerial)) {
				$folderStates = unserialize($folderStateSerial);
			}
			
			// subscribed accounts
			$showFolders = unserialize(base64_decode($current_user->getPreference('showFolders', 'Emails')));
			
			// general settings
			$emailSettings = $current_user->getPreference('emailSettings', 'Emails');
			
			if (empty($emailSettings)) {
				$emailSettings = array();
				$emailSettings['emailCheckInterval'] = -1;
				$emailSettings['autoImport'] = '';
				$emailSettings['alwaysSaveOutbound'] = '1';
				$emailSettings['sendPlainText'] = '';
				$emailSettings['defaultOutboundCharset'] = $GLOBALS['sugar_config']['default_email_charset'];
				$emailSettings['showNumInList'] = 20;
			}
			
			// focus folder
			$focusFolder = $current_user->getPreference('focusFolder', 'Emails');
			$focusFolder = !empty($focusFolder) ? unserialize($focusFolder) : array();
			
			// unread only flag
			$showUnreadOnly = $current_user->getPreference('showUnreadOnly', 'Emails');
			
			$listViewSort = array(
				"sortBy" => 'date', 
				"sortDirection" => 'DESC');
			
			// signature prefs
			$signaturePrepend = $current_user->getPreference('signature_prepend') ? 'true' : 'false';
			$signatureDefault = $current_user->getPreference('signature_default');
			$signatures = array(
				'signature_prepend' => $signaturePrepend, 
				'signature_default' => $signatureDefault);
			
			
			// current_user
			$user = array(
				'emailAddresses' => $current_user->emailAddress->getAddressesByGUID($current_user->id, 'Users'), 
				'full_name' => from_html($current_user->full_name));
			
			$userPreferences = array();
			$userPreferences['sort'] = $sortArray;
			$userPreferences['folderStates'] = $folderStates;
			$userPreferences['showFolders'] = $showFolders;
			$userPreferences['emailSettings'] = $emailSettings;
			$userPreferences['focusFolder'] = $focusFolder;
			$userPreferences['showUnreadOnly'] = $showUnreadOnly;
			$userPreferences['listViewSort'] = $listViewSort;
			$userPreferences['signatures'] = $signatures;
			$userPreferences['current_user'] = $user;
			return $userPreferences;
		}
		
		
		
		///////////////////////////////////////////////////////////////////////////
		////	FOLDER FUNCTIONS
		
		/**
	 * Creates a new Sugar folder
	 * @param string $nodeLabel New sugar folder name
	 * @param string $parentLabel Parent folder name
	 */
		function saveNewFolder($nodeLabel, $parentId, $isGroup = 0)
		{
			//TODO (MT): specify type of proc. parameters
			global $current_user;
			
			$this->folder->name = $nodeLabel;
			$this->folder->is_group = $isGroup;
			$this->folder->parent_folder = ($parentId == 'Home') ? "" : $parentId;
			$this->folder->has_child = 0;
			$this->folder->created_by = $current_user->id;
			$this->folder->modified_by = $current_user->id;
			$this->folder->date_modified = $this->folder->date_created = TimeDate::getInstance()->nowDb();
			
			$this->folder->save();
			return array(
				'action' => 'newFolderSave', 
				'id' => $this->folder->id, 
				'name' => $this->folder->name, 
				'is_group' => $this->folder->is_group, 
				'is_dynamic' => $this->folder->is_dynamic);
		}
		
		/**
	 * Saves user sort prefernces
	 */
		function saveListViewSortOrder($ieId, $focusFolder, $sortBy, $sortDir)
		{
			//TODO (MT): specify type of proc. parameters
			global $current_user;
			
			$sortArray = array();
			
			$sortSerial = $current_user->getPreference('folderSortOrder', 'Emails');
			if (!empty($sortSerial)) {
				$sortArray = unserialize($sortSerial);
			}
			
			$sortArray[$ieId][$focusFolder]['current']['sort'] = $sortBy;
			$sortArray[$ieId][$focusFolder]['current']['direction'] = $sortDir;
			$sortSerial = serialize($sortArray);
			$current_user->setPreference('folderSortOrder', $sortSerial, '', 'Emails');
		}
		
		/**
	 * Stickies folder collapse/open state
	 */
		function saveFolderOpenState($focusFolder, $focusFolderOpen)
		{
			//TODO (MT): specify type of proc. parameters
			global $current_user;
			
			$folderStateSerial = $current_user->getPreference('folderOpenState', 'Emails');
			$folderStates = array();
			
			if (!empty($folderStateSerial)) {
				$folderStates = unserialize($folderStateSerial);
			}
			
			$folderStates[$focusFolder] = $focusFolderOpen;
			$newFolderStateSerial = serialize($folderStates);
			$current_user->setPreference('folderOpenState', $newFolderStateSerial, '', 'Emails');
		}
		
		/**
	 * saves a folder's view state
	 */
		function saveListView($ieId, $folder)
		{
			//TODO (MT): specify type of proc. parameters
			global $current_user;
			
			$saveState = array();
			$saveState['ieId'] = $ieId;
			$saveState['folder'] = $folder;
			$saveStateSerial = serialize($saveState);
			$current_user->setPreference('focusFolder', $saveStateSerial, '', 'Emails');
		}
		
		/**
	 * Generates cache folder structure
	 */
		function preflightEmailCache($cacheRoot)
		{
			// base
			if (!file_exists($cacheRoot)) 
				mkdir_recursive(clean_path($cacheRoot));
			
			// folders
			if (!file_exists($cacheRoot . "/folders")) 
				mkdir_recursive(clean_path("{$cacheRoot}/folders"));
			
			// messages
			if (!file_exists($cacheRoot . "/messages")) 
				mkdir_recursive(clean_path("{$cacheRoot}/messages"));
			
			// attachments
			if (!file_exists($cacheRoot . "/attachments")) 
				mkdir_recursive(clean_path("{$cacheRoot}/attachments"));
		}
		
		function deleteEmailCacheForFolders($cacheRoot)
		{
			$filePath = $cacheRoot . "/folders/folders.php";
			if (file_exists($filePath)) {
				unlink($filePath);
			}
		}
		///////////////////////////////////////////////////////////////////////////
		////	IMAP FUNCTIONS
		/**
	 * Identifies subscribed mailboxes and empties the trash
	 * @param object $ie InboundEmail
	 */
		function emptyTrash(&$ie)
		{
			//TODO (MT): specify type of proc. parameters
			global $current_user;
			
			$showFolders = unserialize(base64_decode($current_user->getPreference('showFolders', 'Emails')));
			
			if (is_array($showFolders)) {
				foreach ($showFolders as $ieId) {
					if (!empty($ieId)) {
						$ie->retrieve($ieId);
						$ie->emptyTrash();
					}
				}
			}
		}
		
		/**
	 * returns an array of nodes that correspond to IMAP mailboxes.
	 * @param bool $forceRefresh
	 * @return object TreeView object
	 */
		function getMailboxNodes()
		{
			global $sugar_config;
			global $current_user;
			global $app_strings;
			
			$tree = new Tree("frameFolders");
			$tree->tree_style = 'include/ytree/TreeView/css/check/tree.css';
			
			$nodes = array();
			$ie = new InboundEmail();
			$refreshOffset = $this->cacheTimeouts['folders']; // 5 mins.  this will be set via user prefs
			
			$rootNode = new ExtNode($app_strings['LBL_EMAIL_HOME_FOLDER'], $app_strings['LBL_EMAIL_HOME_FOLDER']);
			$rootNode->dynamicloadfunction = '';
			$rootNode->expanded = true;
			$rootNode->dynamic_load = true;
			$showFolders = unserialize(base64_decode($current_user->getPreference('showFolders', 'Emails')));
			
			if (empty($showFolders)) {
				$showFolders = array();
			}
			
			// INBOX NODES
			if ($current_user->hasPersonalEmail()) {
				$personals = $ie->retrieveByGroupId($current_user->id);
				
				foreach ($personals as $k => $personalAccount) {
					if (in_array($personalAccount->id, $showFolders)) {
						// check for cache value
						$cacheRoot = sugar_cached("modules/Emails/{$personalAccount->id}");
						$this->preflightEmailCache($cacheRoot);
						
						if ($this->validCacheFileExists($personalAccount->id, 'folders', "folders.php")) {
							$mailboxes = $this->getMailBoxesFromCacheValue($personalAccount);
						} else {
							$mailboxes = $personalAccount->getMailboxes();
						}
						
						$acctNode = new ExtNode('Home::' . $personalAccount->name, $personalAccount->name);
						$acctNode->dynamicloadfunction = '';
						$acctNode->expanded = false;
						$acctNode->set_property('cls', 'ieFolder');
						$acctNode->set_property('ieId', $personalAccount->id);
						$acctNode->set_property('protocol', $personalAccount->protocol);
						
						if (array_key_exists('Home::' . $personalAccount->name, $this->folderStates)) {
							if ($this->folderStates['Home::' . $personalAccount->name] == 'open') {
								$acctNode->expanded = true;
							}
						}
						$acctNode->dynamic_load = true;
						
						$nodePath = $acctNode->_properties['id'];
						
						foreach ($mailboxes as $k => $mbox) {
							$acctNode->add_node($this->buildTreeNode($k, $k, $mbox, $personalAccount->id, $nodePath, false, $personalAccount));
						}
						
						$rootNode->add_node($acctNode);
					}
				}
			}
			
			// GROUP INBOX NODES
			$beans = $ie->retrieveAllByGroupId($current_user->id, false);
			foreach ($beans as $k => $groupAccount) {
				if (in_array($groupAccount->id, $showFolders)) {
					// check for cache value
					$cacheRoot = sugar_cached("modules/Emails/{$groupAccount->id}");
					$this->preflightEmailCache($cacheRoot);
					//$groupAccount->connectMailserver();
					
					if ($this->validCacheFileExists($groupAccount->id, 'folders', "folders.php")) {
						$mailboxes = $this->getMailBoxesFromCacheValue($groupAccount);
					} else {
						$mailboxes = $groupAccount->getMailBoxesForGroupAccount();
					}
					
					$acctNode = new ExtNode($groupAccount->name, "group.{$groupAccount->name}");
					$acctNode->dynamicloadfunction = '';
					$acctNode->expanded = false;
					$acctNode->set_property('isGroup', 'true');
					$acctNode->set_property('ieId', $groupAccount->id);
					$acctNode->set_property('protocol', $groupAccount->protocol);
					
					if (array_key_exists('Home::' . $groupAccount->name, $this->folderStates)) {
						if ($this->folderStates['Home::' . $groupAccount->name] == 'open') {
							$acctNode->expanded = true;
						}
					}
					$acctNode->dynamic_load = true;
					$nodePath = $rootNode->_properties['id'] . "::" . $acctNode->_properties['id'];
					
					foreach ($mailboxes as $k => $mbox) {
						$acctNode->add_node($this->buildTreeNode($k, $k, $mbox, $groupAccount->id, $nodePath, true, $groupAccount));
					}
					
					$rootNode->add_node($acctNode);
				}
			}
			
			// SugarFolder nodes
			/* SugarFolders are built at onload when the UI renders */
			
			$tree->add_node($rootNode);
			return $tree;
		}
		
		function getMailBoxesFromCacheValue($mailAccount)
		{
			//TODO (MT): specify type of proc. parameters
			$foldersCache = $this->getCacheValue($mailAccount->id, 'folders', "folders.php", 'foldersCache');
			$mailboxes = $foldersCache['mailboxes'];
			$mailboxesArray = $mailAccount->generateFlatArrayFromMultiDimArray($mailboxes, $mailAccount->retrieveDelimiter());
			$mailAccount->saveMailBoxFolders($mailboxesArray);
			$this->deleteEmailCacheForFolders($cacheRoot);
			return $mailboxes;
		}
		// fn
		
		/**
	 * Builds up a TreeView Node object
	 * @param mixed
	 * @param mixed
	 * @param string
	 * @param string ID of InboundEmail instance
	 * @param string nodePath Serialized path from root node to current node
	 * @param bool isGroup
	 * @param bool forceRefresh
	 * @return mixed
	 */
		function buildTreeNode($key, $label, $mbox, $ieId, $nodePath, $isGroup, $ie)
		{
			//TODO (MT): specify type of proc. parameters
			global $sugar_config;
			
			// get unread counts
			$exMbox = explode("::", $nodePath);
			$unseen = 0;
			$GLOBALS['log']->debug("$key --- $nodePath::$label");
			
			if (count($exMbox) >= 2) {
				$mailbox = "";
				for($i = 2; $i < count($exMbox); $i++) {
					if ($mailbox != "") {
						$mailbox .= ".";
					}
					$mailbox .= "{$exMbox[$i]}";
				}
				
				$mailbox = substr($key, strpos($key, '.'));
				
				$unseen = $this->getUnreadCount($ie, $mailbox);
				
				if ($unseen > 0) {
					//$label = " <span id='span{$ie->id}{$ie->mailbox}' style='font-weight:bold'>{$label} (<span id='span{$ie->id}{$ie->mailbox}nums'>{$unseen}</span>)</span>";
				}
			}
			
			$nodePath = $nodePath . "::" . $label;
			$node = new ExtNode($nodePath, $label);
			$node->dynamicloadfunction = '';
			$node->expanded = false;
			$node->set_property('labelStyle', "remoteFolder");
			
			
			if (array_key_exists($nodePath, $this->folderStates)) {
				if ($this->folderStates[$nodePath] == 'open') {
					$node->expanded = true;
				}
			}
			
			$group = ($isGroup) ? 'true' : 'false';
			$node->dynamic_load = true;
			//$node->set_property('href', " SUGAR.email2.listView.populateListFrame(YAHOO.namespace('frameFolders').selectednode, '{$ieId}', 'false');");
			$node->set_property('isGroup', $group);
			$node->set_property('isDynamic', 'false');
			$node->set_property('ieId', $ieId);
			$node->set_property('mbox', $key);
			$node->set_property('unseen', $unseen);
			$node->set_property('cls', 'ieFolder');
			
			if (is_array($mbox)) {
				foreach ($mbox as $k => $v) {
					$node->add_node($this->buildTreeNode("$key.$k", $k, $v, $ieId, $nodePath, $isGroup, $ie));
				}
			}
			
			return $node;
		}
		
		/**
	 * Totals the unread emails
	 */
		function getUnreadCount(&$ie, $mailbox)
		{
			global $sugar_config;
			$unseen = 0;
			
			// use cache
			return $ie->getCacheUnreadCount($mailbox);
		}
		
		///////////////////////////////////////////////////////////////////////////
		////	DISPLAY CODE
		/**
	 * Used exclusively by draft code.  Returns Notes and Documents as attachments.
	 * @param array $ret
	 * @return array
	 */
		function getDraftAttachments($ret)
		{
			global $db;
			
			// $ret['uid'] is the draft Email object's GUID
			$ret['attachments'] = array();
			
			$q = "SELECT id, filename FROM notes WHERE parent_id = '{$ret['uid']}' AND deleted = 0";
			$r = $db->query($q);
			
			while ($a = $db->fetchByAssoc($r)) {
				$ret['attachments'][$a['id']] = array(
					'id' => $a['id'], 
					'filename' => $a['filename']);
			}
			
			return $ret;
		}
		
		function createCopyOfInboundAttachment($ie, $ret, $uid)
		{
			global $sugar_config;
			if ($ie->isPop3Protocol()) {
				// get the UIDL from database;
				$cachedUIDL = md5($uid);
				$cache = sugar_cached("modules/Emails/{$ie->id}/messages/{$ie->mailbox}{$cachedUIDL}.php");
			} else {
				$cache = sugar_cached("modules/Emails/{$ie->id}/messages/{$ie->mailbox}{$uid}.php");
			}
			if (file_exists($cache)) {
				include ($cache); // profides $cacheFile
				$metaOut = unserialize($cacheFile['out']);
				$meta = $metaOut['meta']['email'];
				if (isset($meta['attachments'])) {
					$attachmentHtmlData = $meta['attachments'];
					$actualAttachmentInfo = array();
					$this->parseAttachmentInfo($actualAttachmentInfo, $attachmentHtmlData);
					if (sizeof($actualAttachmentInfo) > 0) {
						foreach ($actualAttachmentInfo as $key => $value) {
							$info_vars = array();
							parse_str($value, $info_vars);
							$fileName = $info_vars['tempName'];
							$attachmentid = $info_vars['id'];
							$guid = create_guid();
							$destination = clean_path("{$this->userCacheDir}/{$guid}");
							
							$attachmentFilePath = sugar_cached("modules/Emails/{$ie->id}/attachments/{$attachmentid}");
							copy($attachmentFilePath, $destination);
							$ret['attachments'][$guid] = array();
							$ret['attachments'][$guid]['id'] = $guid . $fileName;
							$ret['attachments'][$guid]['filename'] = $fileName;
						}
						// for
					}
					// if
				}
				// if
				
			}
			// if
			return $ret;
			
		}
		// fn
		
		function parseAttachmentInfo(&$actualAttachmentInfo, $attachmentHtmlData)
		{
			$downLoadPHP = strpos($attachmentHtmlData, "index.php?entryPoint=download&");
			while ($downLoadPHP) {
				$attachmentHtmlData = substr($attachmentHtmlData, $downLoadPHP + 30);
				$final = strpos($attachmentHtmlData, "\">");
				$actualAttachmentInfo[] = substr($attachmentHtmlData, 0, $final);
				$attachmentHtmlData = substr($attachmentHtmlData, $final);
				$downLoadPHP = strpos($attachmentHtmlData, "index.php?entryPoint=download&");
			}
			// while
		}
		/**
	 * Renders the QuickCreate form from Smarty and returns HTML
	 * @param array $vars request variable global
	 * @param object $email Fetched email object
	 * @param bool $addToAddressBook
	 * @return array
	 */
		function getQuickCreateForm($vars, $email, $addToAddressBookButton = false)
		{
			//TODO (MT): specify type of proc. parameters
			require_once ("include/EditView/EditView2.php");
			global $app_strings;
			global $mod_strings;
			global $current_user;
			global $beanList;
			global $beanFiles;
			global $current_language;
			
			//Setup the current module languge
			$mod_strings = return_module_language($current_language, $_REQUEST['qc_module']);
			
			$bean = $beanList[$_REQUEST['qc_module']];
			$class = $beanFiles[$bean];
			require_once ($class);
			
			$focus = new $bean();
			
			$people = array(
				'Contact', 'Lead');
			$emailAddress = array();
			
			// people
			if (in_array($bean, $people)) {
				// lead specific
				$focus->lead_source = 'Email';
				$focus->lead_source_description = trim($email->name);
				
				$from = (isset($email->from_name) && !empty($email->from_name)) ? $email->from_name : $email->from_addr;
				
				if (isset($_REQUEST['sugarEmail']) && !empty($_REQUEST['sugarEmail'])) {
					if ($email->status == "sent") {
						$from = (isset($email->to_addrs_names) && !empty($email->to_addrs_names)) ? $email->to_addrs_names : $email->to_addrs;
					} else {
						$from = (isset($email->from_name) && !empty($email->from_name)) ? $email->from_name : $email->from_addr_name;
					}
				}
				
				$name = explode(" ", trim($from));
				
				$address = trim(array_pop($name));
				$address = str_replace(array("<", ">", "&lt;", "&gt;"), "", $address);
				
				$emailAddress[] = array(
					'email_address' => $address, 
					'primary_address' => 1, 
					'invalid_email' => 0, 
					'opt_out' => 0, 
					'reply_to_address' => 1);
				
				$focus->email1 = $address;
				
				if (!empty($name)) {
					$focus->last_name = trim(array_pop($name));
					
					foreach ($name as $first) {
						if (!empty($focus->first_name)) {
							$focus->first_name .= " ";
						}
						$focus->first_name .= trim($first);
					}
				}
			} else {
				// bugs, cases, tasks
				$focus->name = trim($email->name);
			}
			
			$focus->description = trim(strip_tags($email->description));
			$focus->assigned_user_id = $current_user->id;
			
			
			$EditView = new EditView();
			$EditView->ss = new Sugar_Smarty();
			//MFH BUG#20283 - checks for custom quickcreate fields
			$EditView->setup($_REQUEST['qc_module'], $focus, 'custom/modules/' . $focus->module_dir . '/metadata/editviewdefs.php', 'include/EditView/EditView.tpl');
			$EditView->process();
			$EditView->render();
			
			$EditView->defs['templateMeta']['form']['buttons'] = array(
				'email2save' => array(
				'id' => 'e2AjaxSave', 
				'customCode' => '<input type="button" class="button" value="   ' . $app_strings['LBL_SAVE_BUTTON_LABEL'] . '   " onclick="SUGAR.email2.detailView.saveQuickCreate(false);" />'), 
				'email2saveandreply' => array(
				'id' => 'e2SaveAndReply', 
				'customCode' => '<input type="button" class="button" value="   ' . $app_strings['LBL_EMAIL_SAVE_AND_REPLY'] . '   " onclick="SUGAR.email2.detailView.saveQuickCreate(\'reply\');" />'), 
				'email2cancel' => array(
				'id' => 'e2cancel', 
				'customCode' => '<input type="button" class="button" value="   ' . $app_strings['LBL_EMAIL_CANCEL'] . '   " onclick="SUGAR.email2.detailView.quickCreateDialog.hide();" />'));
			
			
			if ($addToAddressBookButton) {
				$EditView->defs['templateMeta']['form']['buttons']['email2saveAddToAddressBook'] = array(
					'id' => 'e2addToAddressBook', 
					'customCode' => '<input type="button" class="button" value="   ' . $app_strings['LBL_EMAIL_ADDRESS_BOOK_SAVE_AND_ADD'] . '   " onclick="SUGAR.email2.detailView.saveQuickCreate(true);" />');
			}
			
			//Get the module language for javascript
			if (!is_file(sugar_cached('jsLanguage/') . $_REQUEST['qc_module'] . '/' . $GLOBALS['current_language'] . '.js')) {
				require_once ('include/language/jsLanguage.php');
				jsLanguage::createModuleStringsCache($_REQUEST['qc_module'], $GLOBALS['current_language']);
			}
			$jsLanguage = getVersionedScript("cache/jsLanguage/{$_REQUEST['qc_module']}/{$GLOBALS['current_language']}.js", $GLOBALS['sugar_config']['js_lang_version']);
			
			
			$EditView->view = 'EmailQCView';
			$EditView->defs['templateMeta']['form']['headerTpl'] = 'include/EditView/header.tpl';
			$EditView->defs['templateMeta']['form']['footerTpl'] = 'include/EditView/footer.tpl';
			$meta = array();
			$meta['html'] = $jsLanguage . $EditView->display(false, true);
			$meta['html'] = str_replace("src='" . getVersionedPath('include/SugarEmailAddress/SugarEmailAddress.js') . "'", '', $meta['html']);
			$meta['emailAddress'] = $emailAddress;
			
			$mod_strings = return_module_language($current_language, 'Emails');
			
			return $meta;
		}
		
		/**
     * Renders the Import form from Smarty and returns HTML
     * @param array $vars request variable global
     * @param object $email Fetched email object
     * @param bool $addToAddressBook
     * @return array
     */
		function getImportForm($vars, $email, $formName = 'ImportEditView')
		{
			require_once ("include/EditView/EditView2.php");
			require_once ("include/TemplateHandler/TemplateHandler.php");
			require_once ('include/QuickSearchDefaults.php');
			$qsd = QuickSearchDefaults::getQuickSearchDefaults();
			$qsd->setFormName($formName);
			
			global $app_strings;
			global $current_user;
			global $app_list_strings;
			$sqs_objects = array(
				"{$formName}_parent_name" => $qsd->getQSParent());
			$smarty = new Sugar_Smarty();
			$smarty->assign("APP", $app_strings);
			$smarty->assign('formName', $formName);
			$showAssignTo = false;
			if (!isset($vars['showAssignTo']) || $vars['showAssignTo'] == true) {
				$showAssignTo = true;
			}
			// if
			if ($showAssignTo) {
				if (empty($email->assigned_user_id) && empty($email->id)) 
					$email->assigned_user_id = $current_user->id;
				if (empty($email->assigned_name) && empty($email->id)) 
					$email->assigned_user_name = $current_user->user_name;
				$sqs_objects["{$formName}_assigned_user_name"] = $qsd->getQSUser();
			}
			$smarty->assign("showAssignedTo", $showAssignTo);
			
			$showDelete = false;
			if (!isset($vars['showDelete']) || $vars['showDelete'] == true) {
				$showDelete = true;
			}
			$smarty->assign("showDelete", $showDelete);
			
			$smarty->assign("userId", $email->assigned_user_id);
			$smarty->assign("userName", $email->assigned_user_name);
			$parent_types = $app_list_strings['record_type_display'];
			$smarty->assign('parentOptions', get_select_options_with_id($parent_types, $email->parent_type));
			
			$quicksearch_js = '<script type="text/javascript" language="javascript">sqs_objects = ' . json_encode($sqs_objects) . '</script>';
			$smarty->assign('SQS', $quicksearch_js);
			
			$meta = array();
			$meta['html'] = $smarty->fetch("modules/Emails/templates/importRelate.tpl");
			return $meta;
		}
		
		/**
     * This function returns the detail view for email in new 2.0 interface
     *
     */
		function getDetailViewForEmail2($emailId)
		{
			
			//TODO (MT): specify type of proc. parameters
			require_once ('include/DetailView/DetailView.php');
			global $app_strings, $app_list_strings;
			global $mod_strings;
			
			$smarty = new Sugar_Smarty();
			
			// SETTING DEFAULTS
			$focus = new Email();

			//TODO (MT EXTEND Code): retrieve and use the TENANT_ID
			$focus->retrieve($emailId);
			$detailView->ss = new Sugar_Smarty();
			$detailView = new DetailView();
			$title = "";
			$offset = 0;
			if ($focus->type == 'out') {
				$title = getClassicModuleTitle('Emails', array($mod_strings['LBL_SENT_MODULE_NAME'], $focus->name), true);
			} elseif ($focus->type == 'draft') {
				$title = getClassicModuleTitle('Emails', array($mod_strings['LBL_LIST_FORM_DRAFTS_TITLE'], $focus->name), true);
			} elseif ($focus->type == 'inbound') {
				$title = getClassicModuleTitle('Emails', array($mod_strings['LBL_INBOUND_TITLE'], $focus->name), true);
			}
			$smarty->assign("emailTitle", $title);
			
			// DEFAULT TO TEXT IF NO HTML CONTENT:
			$html = trim(from_html($focus->description_html));
			if (empty($html)) {
				$smarty->assign('SHOW_PLAINTEXT', 'true');
			} else {
				$smarty->assign('SHOW_PLAINTEXT', 'false');
			}
			
			//if not empty or set to test (from test campaigns)
			if (!empty($focus->parent_type) && $focus->parent_type != 'test') {
				$smarty->assign('PARENT_MODULE', $focus->parent_type);
				$smarty->assign('PARENT_TYPE', $app_list_strings['record_type_display'][$focus->parent_type] . ":");
			}
			
			global $gridline;
			$smarty->assign('MOD', $mod_strings);
			$smarty->assign('APP', $app_strings);
			$smarty->assign('GRIDLINE', $gridline);
			$smarty->assign('PRINT_URL', 'index.php?' . $GLOBALS['request_string']);
			$smarty->assign('ID', $focus->id);
			$smarty->assign('TYPE', $focus->type);
			$smarty->assign('PARENT_NAME', $focus->parent_name);
			$smarty->assign('PARENT_ID', $focus->parent_id);
			$smarty->assign('NAME', $focus->name);
			$smarty->assign('ASSIGNED_TO', $focus->assigned_user_name);
			$smarty->assign('DATE_MODIFIED', $focus->date_modified);
			$smarty->assign('DATE_ENTERED', $focus->date_entered);
			$smarty->assign('DATE_START', $focus->date_start);
			$smarty->assign('TIME_START', $focus->time_start);
			$smarty->assign('FROM', $focus->from_addr);
			$smarty->assign('TO', nl2br($focus->to_addrs));
			$smarty->assign('CC', nl2br($focus->cc_addrs));
			$smarty->assign('BCC', nl2br($focus->bcc_addrs));
			$smarty->assign('CREATED_BY', $focus->created_by_name);
			$smarty->assign('MODIFIED_BY', $focus->modified_by_name);
			$smarty->assign('DATE_SENT', $focus->date_entered);
			$smarty->assign('EMAIL_NAME', 'RE: ' . $focus->name);
			$smarty->assign("TAG", $focus->listviewACLHelper());
			$smarty->assign("SUGAR_VERSION", $GLOBALS['sugar_version']);
			$smarty->assign("JS_CUSTOM_VERSION", $GLOBALS['sugar_config']['js_custom_version']);
			if (!empty($focus->reply_to_email)) {
				$replyTo = 
					"
				<tr>
		        <td class=\"tabDetailViewDL\"><slot>" . $mod_strings['LBL_REPLY_TO_NAME'] . 
					"</slot></td>
		        <td colspan=3 class=\"tabDetailViewDF\"><slot>" . $focus->reply_to_addr . 
					"</slot></td>
		        </tr>";
				$smarty->assign("REPLY_TO", $replyTo);
			}
			///////////////////////////////////////////////////////////////////////////////
			////	JAVASCRIPT VARS
			$jsVars = '';
			$jsVars .= "var showRaw = '{$mod_strings['LBL_BUTTON_RAW_LABEL']}';";
			$jsVars .= "var hideRaw = '{$mod_strings['LBL_BUTTON_RAW_LABEL_HIDE']}';";
			$smarty->assign("JS_VARS", $jsVars);
			///////////////////////////////////////////////////////////////////////////////
			////	NOTES (attachements, etc.)
			///////////////////////////////////////////////////////////////////////////////
			
			$note = new Note();
			$where = "notes.parent_id='{$focus->id}'";
			//take in account if this is from campaign and the template id is stored in the macros.
			
			if (isset($macro_values) && isset($macro_values['email_template_id'])) {
				$where = "notes.parent_id='{$macro_values['email_template_id']}'";
			}
			$notes_list = $note->get_full_list("notes.name", $where, true);
			
			if (!isset($notes_list)) {
				$notes_list = array();
			}
			
			$attachments = '';
			for($i = 0; $i < count($notes_list); $i++) {
				$the_note = $notes_list[$i];
				$attachments .= "<a href=\"index.php?entryPoint=download&id={$the_note->id}&type=Notes\">" . $the_note->name . "</a><br />";
				$focus->cid2Link($the_note->id, $the_note->file_mime_type);
			}
			$smarty->assign('DESCRIPTION', nl2br($focus->description));
			$smarty->assign('DESCRIPTION_HTML', from_html($focus->description_html));
			$smarty->assign("ATTACHMENTS", $attachments);
			///////////////////////////////////////////////////////////////////////////////
			////    SUBPANELS
			///////////////////////////////////////////////////////////////////////////////
			$show_subpanels = true;
			if ($show_subpanels) {
				require_once ('include/SubPanel/SubPanelTiles.php');
				$subpanel = new SubPanelTiles($focus, 'Emails');
				$smarty->assign("SUBPANEL", $subpanel->display());
			}
			$meta['html'] = $smarty->fetch("modules/Emails/templates/emailDetailView.tpl");
			return $meta;
			
		}
		// fn
		
		/**
	 * Sets the "read" flag in the overview cache
	 */
		function setReadFlag($ieId, $mbox, $uid)
		{
			//TODO (MT): specify type of proc. parameters
			$this->markEmails('read', $ieId, $mbox, $uid);
		}
		
		/**
	 * Marks emails with the passed flag type.  This will be applied to local
	 * cache files as well as remote emails.
	 * @param string $type Flag type
	 * @param string $ieId
	 * @param string $folder IMAP folder structure or SugarFolder GUID
	 * @param string $uids Comma sep list of UIDs or GUIDs
	 */
		function markEmails($type, $ieId, $folder, $uids)
		{
			
			//TODO (MT): specify type of proc. parameters
			global $app_strings;
			$uids = $this->_cleanUIDList($uids);
			$exUids = explode($app_strings['LBL_EMAIL_DELIMITER'], $uids);
			
			if (strpos($folder, 'sugar::') !== false) {
				// dealing with a sugar email object, uids are GUIDs
				foreach ($exUids as $id) {
					$email = new Email();

					//TODO (MT EXTEND Code): retrieve and use the TENANT_ID
					$email->retrieve($id);
					
					// BUG FIX BEGIN
					// Bug 50973 - marking unread in group inbox removes message
					if (empty($email->assigned_user_id)) {
						$email->setFieldNullable('assigned_user_id');
					}
					// BUG FIX END
					
					switch ($type) {
						case "unread":
							$email->status = 'unread';
							$email->save();
							break;
						
						case "read":
							$email->status = 'read';
							$email->save();
							break;
						
						case "deleted":
							$email->delete();
							break;
						
						case "flagged":
							$email->flagged = 1;
							$email->save();
							break;
						
						case "unflagged":
							$email->flagged = 0;
							$email->save();
							break;
					}
					
					
					// BUG FIX BEGIN
					// Bug 50973 - reset assigned_user_id field defs
					if (empty($email->assigned_user_id)) {
						$email->revertFieldNullable('assigned_user_id');
					}
					// BUG FIX END
				}
			} else {
				/* dealing with IMAP email, uids are IMAP uids */
				global $ie; // provided by EmailUIAjax.php
				if (empty($ie)) {
					
					$ie = new InboundEmail();
				}
				$ie->retrieve($ieId);
				$ie->mailbox = $folder;
				$ie->connectMailserver();
				// mark cache files
				if ($type == 'deleted') {
					$ie->deleteMessageOnMailServer($uids);
					$ie->deleteMessageFromCache($uids);
				} else {
					$overviews = $ie->getCacheValueForUIDs($ie->mailbox, $exUids);
					$manipulated = array();
					
					foreach ($overviews['retArr'] as $k => $overview) {
						if (in_array($overview->uid, $exUids)) {
							switch ($type) {
								case "unread":
									$overview->seen = 0;
									break;
								
								case "read":
									$overview->seen = 1;
									break;
								
								case "flagged":
									$overview->flagged = 1;
									break;
								
								case "unflagged":
									$overview->flagged = 0;
									break;
							}
							$manipulated[] = $overview;
						}
					}
					
					if (!empty($manipulated)) {
						$ie->setCacheValue($ie->mailbox, array(), $manipulated);
						/* now mark emails on email server */
						$ie->markEmails(implode(",", explode($app_strings['LBL_EMAIL_DELIMITER'], $uids)), $type);
					}
				}
				// end not type == deleted
			}
		}
		
		function doAssignment($distributeMethod, $ieid, $folder, $uids, $users)
		{
			//TODO (MT): specify type of proc. parameters
			global $app_strings;
			$users = explode(",", $users);
			$emailIds = explode($app_strings['LBL_EMAIL_DELIMITER'], $uids);
			$out = "";
			if ($folder != 'sugar::Emails') {
				$emailIds = array();
				$uids = explode($app_strings['LBL_EMAIL_DELIMITER'], $uids);
				$ie = new InboundEmail();
				$ie->retrieve($ieid);
				$messageIndex = 1;
				// dealing with an inbound email data so we need to import an email and then
				foreach ($uids as $uid) {
					$ie->mailbox = $folder;
					$ie->connectMailserver();
					$msgNo = $uid;
					if (!$ie->isPop3Protocol()) {
						$msgNo = imap_msgno($ie->conn, $uid);
					} else {
						$msgNo = $ie->getCorrectMessageNoForPop3($uid);
					}
					
					if (!empty($msgNo)) {
						if ($ie->importOneEmail($msgNo, $uid)) {
							$emailIds[] = $ie->email->id;
							$ie->deleteMessageOnMailServer($uid);
							//$ie->retrieve($ieid);
							//$ie->connectMailserver();
							$ie->mailbox = $folder;
							$ie->deleteMessageFromCache(($uids[] = $uid));
						} else {
							$out = $out . "Message No : " . $messageIndex . " failed. Reason : Message already imported \r\n";
						}
					}
					$messageIndex++;
				}
				// for
			}
			// if
			
			if (count($emailIds) > 0) {
				$this->doDistributionWithMethod($users, $emailIds, $distributeMethod);
			}
			// if
			return $out;
		}
		// fn
		
		/**
 * get team id and team set id from request
 * @return  array
 */
		function getTeams()
		{
		}
		
		function doDistributionWithMethod($users, $emailIds, $distributionMethod)
		{
			// we have users and the items to distribute
			//TODO (MT): specify type of proc. parameters
			if ($distributionMethod == 'roundRobin') {
				$this->distRoundRobin($users, $emailIds);
			} elseif ($distributionMethod == 'leastBusy') {
				$this->distLeastBusy($users, $emailIds);
			} elseif ($distributionMethod == 'direct') {
				if (count($users) > 1) {
					// only 1 user allowed in direct assignment
					$error = 1;
				} else {
					$user = $users[0];
					$this->distDirect($user, $emailIds);
				}
				// else
			}
			// elseif
			
		}
		// fn
		
		/**
 * distributes emails to users on Round Robin basis
 * @param	$userIds	array of users to dist to
 * @param	$mailIds	array of email ids to push on those users
 * @return  boolean		true on success
 */
		function distRoundRobin($userIds, $mailIds)
		{
			// check if we have a 'lastRobin'
			//TODO (MT): specify type of proc. parameters
			$lastRobin = $userIds[0];
			foreach ($mailIds as $k => $mailId) {
				$userIdsKeys = array_flip($userIds); // now keys are values
				$thisRobinKey = $userIdsKeys[$lastRobin] + 1;
				if (!empty($userIds[$thisRobinKey])) {
					$thisRobin = $userIds[$thisRobinKey];
					$lastRobin = $userIds[$thisRobinKey];
				} else {
					$thisRobin = $userIds[0];
					$lastRobin = $userIds[0];
				}
				
				$email = new Email();

				//TODO (MT EXTEND Code): retrieve and use the TENANT_ID
				$email->retrieve($mailId);
				$email->assigned_user_id = $thisRobin;
				$email->status = 'unread';
				$email->save();
			}
			
			return true;
		}
		
		/**
 * distributes emails to users on Least Busy basis
 * @param	$userIds	array of users to dist to
 * @param	$mailIds	array of email ids to push on those users
 * @return  boolean		true on success
 */
		function distLeastBusy($userIds, $mailIds)
		{
			//TODO (MT): specify type of proc. parameters
			foreach ($mailIds as $k => $mailId) {
				$email = new Email();

				//TODO (MT EXTEND Code): retrieve and use the TENANT_ID
				$email->retrieve($mailId);
				foreach ($userIds as $k => $id) {
					$r = $this->db->query("SELECT count(*) AS c FROM emails WHERE assigned_user_id = '.$id.' AND status = 'unread'");
					$a = $this->db->fetchByAssoc($r);
					$counts[$id] = $a['c'];
				}
				asort($counts); // lowest to highest
				$countsKeys = array_flip($counts); // keys now the 'count of items'
				$leastBusy = array_shift($countsKeys); // user id of lowest item count
				$email->assigned_user_id = $leastBusy;
				$email->status = 'unread';
				$email->save();
			}
			return true;
		}
		
		/**
 * distributes emails to 1 user
 * @param	$user		users to dist to
 * @param	$mailIds	array of email ids to push
 * @return  boolean		true on success
 */
		function distDirect($user, $mailIds)
		{
			//TODO (MT): specify type of proc. parameters
			foreach ($mailIds as $k => $mailId) {
				$email = new Email();

				//TODO (MT EXTEND Code): retrieve and use the TENANT_ID
				$email->retrieve($mailId);
				$email->assigned_user_id = $user;
				$email->status = 'unread';
				
				$email->save();
			}
			return true;
		}
		
		function getAssignedEmailsCountForUsers($userIds)
		{
			$counts = array();
			foreach ($userIds as $id) {
				$r = $this->db->query("SELECT count(*) AS c FROM emails WHERE assigned_user_id = '$id' AND status = 'unread'");
				$a = $this->db->fetchByAssoc($r);
				$counts[$id] = $a['c'];
			}
			// foreach
			return $counts;
		}
		// fn
		
		function getLastRobin($ie)
		{
			//TODO (MT): specify type of proc. parameters
			$lastRobin = "";
			if ($this->validCacheFileExists($ie->id, 'folders', "robin.cache.php")) {
				$lastRobin = $this->getCacheValue($ie->id, 'folders', "robin.cache.php", 'robin');
			}
			// if
			return $lastRobin;
		}
		// fn
		
		function setLastRobin($ie, $lastRobin)
		{
			//TODO (MT): specify type of proc. parameters
			global $sugar_config;
			$cacheFolderPath = sugar_cached("modules/Emails/{$ie->id}/folders");
			if (!file_exists($cacheFolderPath)) {
				mkdir_recursive($cacheFolderPath);
			}
			$this->writeCacheFile('robin', $lastRobin, $ie->id, 'folders', "robin.cache.php");
		}
		// fn
		
		/**
	 * returns the metadata defining a single email message for display.  Uses cache file if it exists
	 * @return array
	 */
		function getSingleMessage($ie)
		{
			
			//TODO (MT): specify type of proc. parameters
			global $timedate;
			global $app_strings, $mod_strings;
			$ie->retrieve($_REQUEST['ieId']);
			$noCache = true;
			
			$ie->mailbox = $_REQUEST['mbox'];
			$filename = $_REQUEST['mbox'] . $_REQUEST['uid'] . ".php";
			$md5uidl = "";
			if ($ie->isPop3Protocol()) {
				$md5uidl = md5($_REQUEST['uid']);
				$filename = $_REQUEST['mbox'] . $md5uidl . ".php";
			}
			// if
			
			if ($this->validCacheFileExists($_REQUEST['ieId'], 'messages', $filename)) {
				$out = $this->getCacheValue($_REQUEST['ieId'], 'messages', $filename, 'out');
				$noCache = false;
				
				// something fubar'd the cache?
				if (empty($out['meta']['email']['name']) && empty($out['meta']['email']['description'])) {
					$noCache = true;
				} else {
					// When sending data from cache, convert date into users preffered format
					$dateTimeInGMTFormat = $out['meta']['email']['date_start'];
					$out['meta']['email']['date_start'] = $timedate->to_display_date_time($dateTimeInGMTFormat);
				}
				// else
			}
			
			if ($noCache) {
				$writeToCacheFile = true;
				if ($ie->isPop3Protocol()) {
					$status = $ie->setEmailForDisplay($_REQUEST['uid'], true, true, true);
				} else {
					$status = $ie->setEmailForDisplay($_REQUEST['uid'], false, true, true);
				}
				$out = $ie->displayOneEmail($_REQUEST['uid'], $_REQUEST['mbox']);
				// modify the out object to store date in GMT format on the local cache file
				$dateTimeInUserFormat = $out['meta']['email']['date_start'];
				$out['meta']['email']['date_start'] = $timedate->to_db($dateTimeInUserFormat);
				if ($status == 'error') {
					$writeToCacheFile = false;
				}
				if ($writeToCacheFile) {
					if ($ie->isPop3Protocol()) {
						$this->writeCacheFile('out', $out, $_REQUEST['ieId'], 'messages', "{$_REQUEST['mbox']}{$md5uidl}.php");
					} else {
						$this->writeCacheFile('out', $out, $_REQUEST['ieId'], 'messages', "{$_REQUEST['mbox']}{$_REQUEST['uid']}.php");
					}
					// else
					// restore date in the users preferred format to be send on to UI for diaply
					$out['meta']['email']['date_start'] = $dateTimeInUserFormat;
				}
				// if
			}
			$out['meta']['email']['toaddrs'] = $this->generateExpandableAddrs($out['meta']['email']['toaddrs']);
			if (!empty($out['meta']['email']['cc_addrs'])) {
				$ccs = $this->generateExpandableAddrs($out['meta']['email']['cc_addrs']);
				$out['meta']['cc'] = <<<eoq
				<tr>
					<td NOWRAP valign="top" class="displayEmailLabel">
						{$app_strings['LBL_EMAIL_CC']}:
					</td>
					<td class="displayEmailValue">
						{$ccs}
					</td>
				</tr>
eoq;
			}
			
			if (empty($out['meta']['email']['description'])) 
				$out['meta']['email']['description'] = $mod_strings['LBL_EMPTY_EMAIL_BODY'];
			
			if ($noCache) {
				$GLOBALS['log']->debug("EMAILUI: getSingleMessage() NOT using cache file");
			} else {
				$GLOBALS['log']->debug("EMAILUI: getSingleMessage() using cache file [ " . $_REQUEST['mbox'] . $_REQUEST['uid'] . ".php ]");
			}
			
			$this->setReadFlag($_REQUEST['ieId'], $_REQUEST['mbox'], $_REQUEST['uid']);
			return $out;
		}
		
		
		/**
	 * Returns the HTML for a list of emails in a given folder
	 * @param GUID $ieId GUID to InboundEmail instance
	 * @param string $mbox Mailbox path name in dot notation
	 * @param int $folderListCacheOffset Seconds for valid cache file
	 * @return string HTML render of list.
	 */
		function getListEmails($ieId, $mbox, $folderListCacheOffset, $forceRefresh = 'false')
		{
			//TODO (MT): specify type of proc. parameters
			global $sugar_config;
			
			
			$ie = new InboundEmail();
			$ie->retrieve($ieId);
			$list = $ie->displayFolderContents($mbox, $forceRefresh);
			
			return $list;
		}
		
		/**
	 * Returns the templatized compose screen.  Used by reply, forwards and draft status messages.
	 * @param object email Email bean in focus
	 */
		function displayComposeEmail($email)
		{
			global $locale;
			global $current_user;
			
			
			$ea = new SugarEmailAddress();
			
			if (!empty($email)) {
				$email->cids2Links();
				$description = (empty($email->description_html)) ? $email->description : $email->description_html;
			}
			
			//Get the most complete address list availible for this email
			$addresses = array('toAddresses' => 'to', 'ccAddresses' => 'cc', 'bccAddresses' => 'bcc');
			foreach ($addresses as $var => $type) {
				$$var = "";
				foreach (array("{$type}_addrs_names", "{$type}addrs", "{$type}_addrs") as $emailVar) {
					if (!empty($email->$emailVar)) {
						$$var = $email->$emailVar;
						break;
					}
				}
			}
			
			$ret = array();
			$ret['type'] = $email->type;
			$ret['name'] = $email->name;
			$ret['description'] = $description;
			$ret['from'] = (isset($_REQUEST['composeType']) && $_REQUEST['composeType'] == 'forward') ? "" : $email->from_addr;
			$ret['to'] = from_html($toAddresses);
			$ret['uid'] = $email->id;
			$ret['parent_name'] = $email->parent_name;
			$ret['parent_type'] = $email->parent_type;
			$ret['parent_id'] = $email->parent_id;
			
			if ($email->type == 'draft') {
				$ret['cc'] = from_html($ccAddresses);
				$ret['bcc'] = $bccAddresses;
			}
			// reply all
			if (isset($_REQUEST['composeType']) && $_REQUEST['composeType'] == 'replyAll') {
				$ret['cc'] = from_html($ccAddresses);
				$ret['bcc'] = $bccAddresses;
				
				$userEmails = array();
				$userEmailsMeta = $ea->getAddressesByGUID($current_user->id, 'Users');
				foreach ($userEmailsMeta as $emailMeta) {
					$userEmails[] = from_html(strtolower(trim($emailMeta['email_address'])));
				}
				$userEmails[] = from_html(strtolower(trim($email->from_addr)));
				
				$ret['cc'] = from_html($email->cc_addrs);
				$toAddresses = from_html($toAddresses);
				$to = str_replace($this->addressSeparators, "::", $toAddresses);
				$exTo = explode("::", $to);
				
				if (is_array($exTo)) {
					foreach ($exTo as $addr) {
						$addr = strtolower(trim($addr));
						if (!in_array($addr, $userEmails)) {
							if (!empty($ret['cc'])) {
								$ret['cc'] = $ret['cc'] . ", ";
							}
							$ret['cc'] = $ret['cc'] . trim($addr);
						}
					}
				} elseif (!empty($exTo)) {
					$exTo = trim($exTo);
					if (!in_array($exTo, $userEmails)) {
						$ret['cc'] = $ret['cc'] . ", " . $exTo;
					}
				}
			}
			return $ret;
		}
		/**
	 * Formats email body on reply/forward
	 * @param object email Email object in focus
	 * @param string type
	 * @return object email
	 */
		function handleReplyType($email, $type)
		{
			//TODO (MT): specify type of proc. parameters
			global $mod_strings;
			$GLOBALS['log']->debug("****At Handle Reply Type: $type");
			switch ($type) {
				case "reply":
				case "replyAll":
					$header = $email->getReplyHeader();
					if (!preg_match('/^(re:)+/i', $email->name)) {
						$email->name = "{$mod_strings['LBL_RE']} {$email->name}";
					}
					if ($type == "reply") {
						$email->cc_addrs = "";
						if (!empty($email->reply_to_addr)) {
							$email->from_addr = $email->reply_to_addr;
						}
						// if
					} else {
						if (!empty($email->reply_to_addr)) {
							$email->to_addrs = $email->to_addrs . "," . $email->reply_to_addr;
						}
						// if
					}
					// else
					break;
				
				case "forward":
					$header = $email->getForwardHeader();
					if (!preg_match('/^(fw:)+/i', $email->name)) {
						$email->name = "{$mod_strings['LBL_FW']} {$email->name}";
					}
					$email->cc_addrs = "";
					break;
				
				case "replyCase":
					$GLOBALS['log']->debug("EMAILUI: At reply case");
					$header = $email->getReplyHeader();
					
					$myCase = new aCase();
					$myCase->retrieve($email->parent_id);
					$myCaseMacro = $myCase->getEmailSubjectMacro();
					$email->parent_name = $myCase->name;
					$GLOBALS['log']->debug("****Case # : {$myCase->case_number} macro: $myCaseMacro");
					if (!strpos($email->name, str_replace('%1', $myCase->case_number, $myCaseMacro))) {
						$GLOBALS['log']->debug("Replacing");
						$email->name = str_replace('%1', $myCase->case_number, $myCaseMacro) . ' ' . $email->name;
					}
					$email->name = "{$mod_strings['LBL_RE']} {$email->name}";
					break;
			}
			
			$html = trim($email->description_html);
			$plain = trim($email->description);
			
			$desc = (!empty($html)) ? $html : $plain;
			
			$email->description = $header . $email->quoteHtmlEmailForNewEmailUI($desc);
			return $email;
			
		}
		
		///////////////////////////////////////////////////////////////////////////
		////	PRIVATE HELPERS
		/**
	 * Generates a UNION query to get one list of users, contacts, leads, and
	 * prospects; used specifically for the addressBook
	 */
		function _getPeopleUnionQuery($whereArr, $person)
		{
			global $current_user, $app_strings;
			global $db;
			if (!isset($person) || $person === 'LBL_DROPDOWN_LIST_ALL') {
				$peopleTables = array("users", 
					"contacts", 
					"leads", 
					"prospects", 
					"accounts");
			} else {
				$peopleTables = array($person);
			}
			$q = '';
			
			$whereAdd = "";
			
			foreach ($whereArr as $column => $clause) {
				if (!empty($whereAdd)) {
					$whereAdd .= " AND ";
				}
				$clause = $current_user->db->quote($clause);
				$whereAdd .= "{$column} LIKE '{$clause}%'";
			}
			
			
			foreach ($peopleTables as $table) {
				$module = ucfirst($table);
				$class = substr($module, 0, strlen($module) - 1);
				require_once ("modules/{$module}/{$class}.php");
				$person = new $class();
				if (!$person->ACLAccess('list')) {
					continue;
				}
				// if
				$where = "({$table}.deleted = 0 AND eabr.primary_address = 1 AND {$table}.id <> '{$current_user->id}')";
				
				if (ACLController::requireOwner($module, 'list')) {
					$where = $where . " AND ({$table}.assigned_user_id = '{$current_user->id}')";
				}
				// if
				if (!empty($whereAdd)) {
					$where .= " AND ({$whereAdd})";
				}
				
				if ($person === 'accounts') {
					$t = "SELECT {$table}.id, '' first_name, {$table}.name, eabr.primary_address, ea.email_address, '{$module}' module ";
				} else {
					$t = "SELECT {$table}.id, {$table}.first_name, {$table}.last_name, eabr.primary_address, ea.email_address, '{$module}' module ";
				}
				$t .= "FROM {$table} ";
				$t .= "JOIN email_addr_bean_rel eabr ON ({$table}.id = eabr.bean_id and eabr.deleted=0) ";
				$t .= "JOIN email_addresses ea ON (eabr.email_address_id = ea.id) ";
				$t .= " WHERE {$where}";
				
				if (!empty($q)) {
					$q .= "\n UNION ALL \n";
				}
				
				$q .= "({$t})";
			}
			$countq = "SELECT count(people.id) c from ($q) people";
			$q .= "ORDER BY last_name";
			
			return array('query' => $q, 'countQuery' => $countq);
		}
		
		/**
     * get emails of related bean for a given bean id
     * @param $beanType
     * @param $condition array of conditions inclued bean id
     * @return array('query' => $q, 'countQuery' => $countq);
     */
		function getRelatedEmail($beanType, $whereArr, $relatedBeanInfoArr = '')
		{
			global $beanList, $current_user, $app_strings, $db;
			$finalQuery = '';
			$searchBeans = null;
			if ($beanType === 'LBL_DROPDOWN_LIST_ALL') 
				$searchBeans = array("users", 
					"contacts", 
					"leads", 
					"prospects", 
					"accounts");
			
			if ($relatedBeanInfoArr == '' || empty($relatedBeanInfoArr['related_bean_type'])) {
				if ($searchBeans != null) {
					$q = array();
					foreach ($searchBeans as $searchBean) {
						$searchq = $this->findEmailFromBeanIds('', $searchBean, $whereArr);
						if (!empty($searchq)) {
							$q[] = "($searchq)";
						}
					}
					if (!empty($q)) 
						$finalQuery .= implode("\n UNION ALL \n", $q);
				} else 
					$finalQuery = $this->findEmailFromBeanIds('', $beanType, $whereArr);
			} else {
				$class = $beanList[$relatedBeanInfoArr['related_bean_type']];
				$focus = new $class();
				$focus->retrieve($relatedBeanInfoArr['related_bean_id']);
				if ($searchBeans != null) {
					$q = array();
					foreach ($searchBeans as $searchBean) {
						if ($focus->load_relationship($searchBean)) {
							$data = $focus->$searchBean->get();
							if (count($data) != 0) 
								$q[] = '(' . $this->findEmailFromBeanIds($data, $searchBean, $whereArr) . ')';
						}
					}
					if (!empty($q)) 
						$finalQuery .= implode("\n UNION ALL \n", $q);
				} else {
					if ($focus->load_relationship($beanType)) {
						$data = $focus->$beanType->get();
						if (count($data) != 0) 
							$finalQuery = $this->findEmailFromBeanIds($data, $beanType, $whereArr);
					}
				}
			}
			$countq = "SELECT count(people.id) c from ($finalQuery) people";
			return array('query' => $finalQuery, 'countQuery' => $countq);
		}
		
		function findEmailFromBeanIds($beanIds, $beanType, $whereArr)
		{
			global $current_user;
			$q = '';
			$whereAdd = "";
			$relatedIDs = '';
			if ($beanIds != '') {
				foreach ($beanIds as $key => $value) {
					$beanIds[$key] = '\'' . $value . '\'';
				}
				$relatedIDs = implode(',', $beanIds);
			}
			
			if ($beanType == 'accounts') {
				if (isset($whereArr['first_name'])) {
					$whereArr['name'] = $whereArr['first_name'];
				}
				unset($whereArr['last_name']);
				unset($whereArr['first_name']);
			}
			
			foreach ($whereArr as $column => $clause) {
				if (!empty($whereAdd)) {
					$whereAdd .= " OR ";
				}
				$clause = $current_user->db->quote($clause);
				$whereAdd .= "{$column} LIKE '{$clause}%'";
			}
			$table = $beanType;
			$module = ucfirst($table);
			$class = substr($module, 0, strlen($module) - 1);
			require_once ("modules/{$module}/{$class}.php");
			$person = new $class();
			if ($person->ACLAccess('list')) {
				if ($relatedIDs != '') {
					$where = "({$table}.deleted = 0 AND eabr.primary_address = 1 AND {$table}.id in ($relatedIDs))";
				} else {
					$where = "({$table}.deleted = 0 AND eabr.primary_address = 1)";
				}
				
				if (ACLController::requireOwner($module, 'list')) {
					$where = $where . " AND ({$table}.assigned_user_id = '{$current_user->id}')";
				}
				// if
				if (!empty($whereAdd)) {
					$where .= " AND ({$whereAdd})";
				}
				
				if ($beanType === 'accounts') {
					$t = "SELECT {$table}.id, '' first_name, {$table}.name last_name, eabr.primary_address, ea.email_address, '{$module}' module ";
				} else {
					$t = "SELECT {$table}.id, {$table}.first_name, {$table}.last_name, eabr.primary_address, ea.email_address, '{$module}' module ";
				}
				
				$t .= "FROM {$table} ";
				$t .= "JOIN email_addr_bean_rel eabr ON ({$table}.id = eabr.bean_id and eabr.deleted=0) ";
				$t .= "JOIN email_addresses ea ON (eabr.email_address_id = ea.id) ";
				$t .= " WHERE {$where}";
			}
			// if
			return $t;
		}
		
		/**
	 * Cleans UID lists
	 * @param mixed $uids
	 * @param bool $returnString False will return an array
	 * @return mixed
	 */
		function _cleanUIDList($uids, $returnString = false)
		{
			//TODO (MT): specify type of proc. parameters
			global $app_strings;
			$GLOBALS['log']->debug("_cleanUIDList: before - [ {$uids} ]");
			
			if (!is_array($uids)) {
				$returnString = true;
				
				$exUids = explode($app_strings['LBL_EMAIL_DELIMITER'], $uids);
				$uids = $exUids;
			}
			
			$cleanUids = array();
			foreach ($uids as $uid) {
				$cleanUids[$uid] = $uid;
			}
			
			sort($cleanUids);
			
			if ($returnString) {
				$cleanImplode = implode($app_strings['LBL_EMAIL_DELIMITER'], $cleanUids);
				$GLOBALS['log']->debug("_cleanUIDList: after - [ {$cleanImplode} ]");
				return $cleanImplode;
			}
			
			return $cleanUids;
		}
		
		/**
     * Creates Mail folder
     *
     * @param object $user User in focus
     * @param array $folder_params Array of parameters for folder creation
     */
		protected function createFolder($user, $folder_params)
		{
			//TODO (MT): specify type of proc. parameters
			$folder = new SugarFolder();
			foreach ($folder_params as $key => $val) {
				$folder->$key = $val;
			}
			
			$folder->save();
			
			return $folder;
		}
		
		/**
	 * Creates defaults for the User
	 * @param object $user User in focus
	 */
		public function preflightUser(&$user)
		{
			//TODO (MT): specify type of proc. parameters
			global $mod_strings;
			$folder_types = array();
			
			// My Emails
			// My Drafts
			// Sent Emails
			// Archived Emails
			$params = array(
				"inbound" => array(
				'name' => $mod_strings['LNK_MY_INBOX'], 
				'folder_type' => "inbound", 
				'has_child' => 1, 
				'dynamic_query' => $this->generateDynamicFolderQuery("inbound", $user->id), 
				'is_dynamic' => 1, 
				'created_by' => $user->id, 
				'modified_by' => $user->id), 
				"draft" => array(
				'name' => $mod_strings['LNK_MY_DRAFTS'], 
				'folder_type' => "draft", 
				'has_child' => 0, 
				'dynamic_query' => $this->generateDynamicFolderQuery("draft", $user->id), 
				'is_dynamic' => 1, 
				'created_by' => $user->id, 
				'modified_by' => $user->id), 
				"sent" => array(
				'name' => $mod_strings['LNK_SENT_EMAIL_LIST'], 
				'folder_type' => "sent", 
				'has_child' => 0, 
				'dynamic_query' => $this->generateDynamicFolderQuery("sent", $user->id), 
				'is_dynamic' => 1, 
				'created_by' => $user->id, 
				'modified_by' => $user->id), 
				"archived" => array(
				'name' => $mod_strings['LBL_LIST_TITLE_MY_ARCHIVES'], 
				'folder_type' => "archived", 
				'has_child' => 0, 
				'dynamic_query' => '', 
				'is_dynamic' => 1, 
				'created_by' => $user->id, 
				'modified_by' => $user->id));
			
			$q = "SELECT * FROM folders f WHERE f.created_by = '{$user->id}' AND f.deleted = 0 AND coalesce(" . $user->db->convert("f.folder_type", "length") . ",0) > 0";
			$r = $user->db->query($q);
			
			while ($row = $GLOBALS['db']->fetchByAssoc($r)) {
				if ($row['folder_type'] == 'inbound') {
					$parent_id = $row['id'];
				}
				if (!in_array($row['folder_type'], $folder_types)) {
					array_push($folder_types, $row['folder_type']);
				}
				if (isset($params[$row['folder_type']])) {
					unset($params[$row['folder_type']]);
				}
			}
			
			require_once ("include/SugarFolders/SugarFolders.php");
			
			foreach ($params as $type => $type_params) {
				if ($type == "inbound") {
					
					$folder = $this->createFolder($user, $params[$type]);
					
					$parent_id = $folder->id;
					
					// handle the case where inbound folder was deleted, but other folders exist
					if (count($folder_types) != 0) {
						// This update query will exclude inbound parent, and any custom created folders.
						// For others, it will update their parent_id for the current user.
						$q = "UPDATE folders SET parent_folder = '" . $parent_id . 
							"' WHERE folder_type IN ('draft', 'sent', 'archived') AND created_by = '" . $user->id . "'";
						$q = "UPDATE folders SET parent_folder = '" . $parent_id . 
							"' WHERE folder_type IN ('draft', 'sent', 'archived') AND created_by = '" . $user->id . "'";
						$r = $user->db->query($q);
					}
				} else {
					$params[$type]['parent_folder'] = $parent_id;
					
					$this->createFolder($user, $params[$type]);
				}
			}
		}
		
		/**
	 * Parses the core dynamic folder query
	 * @param string $type 'inbound', 'draft', etc.
	 * @param string $userId
	 * @return string
	 */
		function generateDynamicFolderQuery($type, $userId)
		{
			$q = $this->coreDynamicFolderQuery;
			
			$status = $type;
			
			if ($type == "sent") {
				$type = "out";
			}
			
			$replacee = array("::TYPE::", "::STATUS::", "::USER_ID::");
			$replacer = array($type, $status, $userId);
			
			$ret = str_replace($replacee, $replacer, $q);
			
			if ($type == 'inbound') {
				$ret .= " AND status NOT IN ('sent', 'archived', 'draft') AND type NOT IN ('out', 'archived', 'draft')";
			} else {
				$ret .= " AND status NOT IN ('archived') AND type NOT IN ('archived')";
			}
			
			return $ret;
		}
		
		/**
	 * Preps the User's cache dir
	 */
		function preflightUserCache()
		{
			$path = clean_path($this->userCacheDir);
			if (!file_exists($this->userCacheDir)) 
				mkdir_recursive($path);
			
			$files = findAllFiles($path, array());
			
			foreach ($files as $file) {
				unlink($file);
			}
		}
		
		function clearInboundAccountCache($ieId)
		{
			global $sugar_config;
			$cacheRoot = sugar_cached("modules/Emails/{$ieId}");
			$files = findAllFiles($cacheRoot . "/messages/", array());
			foreach ($files as $file) {
				unlink($file);
			}
			// fn
			$files = findAllFiles($cacheRoot . "/attachments/", array());
			foreach ($files as $file) {
				unlink($file);
			}
			// for
		}
		// fn
		
		/**
	 * returns an array of EmailTemplates that the user has access to for the compose email screen
	 * @return array
	 */
		function getEmailTemplatesArray()
		{
			
			global $app_strings;
			
			if (ACLController::checkAccess('EmailTemplates', 'list', true) && ACLController::checkAccess('EmailTemplates', 'view', true)) {
				$et = new EmailTemplate();
				$etResult = $et->db->query($et->create_new_list_query('', "(type IS NULL OR type='' OR type='email')", array(), array(), ''));
				$email_templates_arr = array('' => $app_strings['LBL_NONE']);
				while ($etA = $et->db->fetchByAssoc($etResult)) {
					$email_templates_arr[$etA['id']] = $etA['name'];
				}
			} else {
				$email_templates_arr = array('' => $app_strings['LBL_NONE']);
			}
			
			return $email_templates_arr;
		}
		
		function getFromAccountsArray($ie)
		{
			//TODO (MT): specify type of proc. parameters
			global $current_user;
			global $app_strings;
			
			$ieAccountsFull = $ie->retrieveAllByGroupIdWithGroupAccounts($current_user->id);
			$ieAccountsFrom = array();
			
			$oe = new OutboundEmail();

			//TODO (MT EXTEND Code): retrieve and use the TENANT_ID
			$system = $oe->getSystemMailerSettings();
			$ret = $current_user->getUsersNameAndEmail();
			$ret['name'] = from_html($ret['name']);
			$useMyAccountString = true;
			
			if (empty($ret['email'])) {
				$systemReturn = $current_user->getSystemDefaultNameAndEmail();
				$ret['email'] = $systemReturn['email'];
				$ret['name'] = from_html($systemReturn['name']);
				$useMyAccountString = false;
			}
			// if
			
			$myAccountString = '';
			if ($useMyAccountString) {
				$myAccountString = " - {$app_strings['LBL_MY_ACCOUNT']}";
			}
			// if
			
			//Check to make sure that the user has set the associated inbound email account -> outbound account is active.
			$showFolders = unserialize(base64_decode($current_user->getPreference('showFolders', 'Emails')));
			$sf = new SugarFolder();
			$groupSubs = $sf->getSubscriptions($current_user);
			
			foreach ($ieAccountsFull as $k => $v) {
				$personalSelected = (!empty($showFolders) && in_array($v->id, $showFolders));
				
				$allowOutboundGroupUsage = $v->get_stored_options('allow_outbound_group_usage', FALSE);
				$groupSelected = (in_array($v->groupfolder_id, $groupSubs) && $allowOutboundGroupUsage);
				$selected = ($personalSelected || $groupSelected);
				
				if (!$selected) {
					$GLOBALS['log']->debug("Inbound Email {$v->name}, not selected and will not be available for selection within compose UI.");
					continue;
				}
				
				$name = $v->get_stored_options('from_name');
				$addr = $v->get_stored_options('from_addr');
				if ($name != null && $addr != null) {
					$name = from_html($name);
					if (!$v->is_personal) {
						$ieAccountsFrom[] = array("value" => $v->id, "text" => "{$name} ({$addr}) - {$app_strings['LBL_EMAIL_UPPER_CASE_GROUP']}");
					} else {
						$ieAccountsFrom[] = array("value" => $v->id, "text" => "{$name} ({$addr})");
					}
					// else
				}
				// if
			}
			// foreach
			
			
			$userSystemOverride = $oe->getUsersMailerForSystemOverride($current_user->id);
			//Substitute in the users system override if its available.
			if ($userSystemOverride != null) 
				$system = $userSystemOverride;
			
			if (!empty($system->mail_smtpserver)) {
				$admin = new Administration();
				$admin->retrieveSettings(); //retrieve all admin settings.
				$ieAccountsFrom[] = array("value" => $system->id, "text" => 
					"{$ret['name']} ({$ret['email']}){$myAccountString}");
			}
			
			return $ieAccountsFrom;
		}
		// fn
		
		/**
     * This function will return all the accounts this user has access to based on the
     * match of the emailId passed in as a parameter
     *
     * @param unknown_type $ie
     * @return unknown
     */
		function getFromAllAccountsArray($ie, $ret)
		{
			global $current_user;
			global $app_strings;
			
			$ret['fromAccounts'] = array();
			if (!isset($ret['to']) && !empty($ret['from'])) {
				$ret['fromAccounts']['status'] = false;
				return $ret;
			}
			$ieAccountsFull = $ie->retrieveAllByGroupIdWithGroupAccounts($current_user->id);
			$foundInPersonalAccounts = false;
			$foundInGroupAccounts = false;
			$foundInSystemAccounts = false;
			
			//$toArray = array();
			if ($ret['type'] == "draft") {
				$toArray = explode(",", $ret['from']);
			} else {
				$toArray = $ie->email->email2ParseAddressesForAddressesOnly($ret['to']);
			}
			// else
			foreach ($ieAccountsFull as $k => $v) {
				$storedOptions = unserialize(base64_decode($v->stored_options));
				if (array_search_insensitive($storedOptions['from_addr'], $toArray)) {
					if ($v->is_personal) {
						$foundInPersonalAccounts = true;
						break;
					} else {
						$foundInGroupAccounts = true;
					}
					// else
				}
				// if
			}
			// foreach
			
			$oe = new OutboundEmail();
			$system = $oe->getSystemMailerSettings();
			
			$return = $current_user->getUsersNameAndEmail();
			$return['name'] = from_html($return['name']);
			$useMyAccountString = true;
			
			if (empty($return['email'])) {
				$systemReturn = $current_user->getSystemDefaultNameAndEmail();
				$return['email'] = $systemReturn['email'];
				$return['name'] = from_html($systemReturn['name']);
				$useMyAccountString = false;
			}
			// if
			
			$myAccountString = '';
			if ($useMyAccountString) {
				$myAccountString = " - {$app_strings['LBL_MY_ACCOUNT']}";
			}
			// if
			
			if (!empty($system->id)) {
				
				$admin = new Administration();
				$admin->retrieveSettings(); //retrieve all admin settings.
				if (in_array(trim($return['email']), $toArray)) {
					$foundInSystemAccounts = true;
				}
				// if
			}
			// if
			
			if (!$foundInPersonalAccounts && !$foundInGroupAccounts && !$foundInSystemAccounts) {
				$ret['fromAccounts']['status'] = false;
				return $ret;
			}
			// if
			
			$ieAccountsFrom = array();
			foreach ($ieAccountsFull as $k => $v) {
				$storedOptions = unserialize(base64_decode($v->stored_options));
				$storedOptionsName = from_html($storedOptions['from_name']);
				
				$selected = false;
				if (array_search_insensitive($storedOptions['from_addr'], $toArray)) {
					//if ($ret['to'] == $storedOptions['from_addr']) {
					$selected = true;
				}
				// if
				if ($foundInPersonalAccounts) {
					if ($v->is_personal) {
						$ieAccountsFrom[] = array("value" => $v->id, "selected" => $selected, "text" => "{$storedOptionsName} ({$storedOptions['from_addr']})");
					}
					// if
				} else {
					$ieAccountsFrom[] = array("value" => $v->id, "selected" => $selected, "text" => "{$storedOptionsName} ({$storedOptions['from_addr']}) - {$app_strings['LBL_EMAIL_UPPER_CASE_GROUP']}");
				}
				// else
			}
			// foreach
			
			if (!empty($system->id)) {
				if (!$foundInPersonalAccounts && !$foundInGroupAccounts && $foundInSystemAccounts) {
					$ieAccountsFrom[] = array("value" => $system->id, "selected" => true, "text" => 
						"{$return['name']} ({$return['email']}){$myAccountString}");
				} else {
					$ieAccountsFrom[] = array("value" => $system->id, "text" => 
						"{$return['name']} ({$return['email']}){$myAccountString}");
				}
				// else
			}
			// if
			
			$ret['fromAccounts']['status'] = ($foundInPersonalAccounts || $foundInGroupAccounts || $foundInSystemAccounts) ? true : false;
			$ret['fromAccounts']['data'] = $ieAccountsFrom;
			return $ret;
		}
		// fn
		
		
		/**
	 * takes an array and creates XML
	 * @param array Array to convert
	 * @param string Name to wrap highest level items in array
	 * @return string XML
	 */
		function arrayToXML($a, $paramName)
		{
			if (!is_array($a)) 
				return '';
			
			$bad = array("<", ">", "'", '"', "&");
			$good = array("&lt;", "&gt;", "&#39;", "&quot;", "&amp;");
			
			$ret = "";
			
			for($i = 0; $i < count($a); $i++) {
				$email = $a[$i];
				$ret .= "\n<{$paramName}>";
				
				foreach ($email as $k => $v) {
					$ret .= "\n\t<{$k}>" . str_replace($bad, $good, $v) . "</{$k}>";
				}
				$ret .= "\n</{$paramName}>";
			}
			return $ret;
		}
		
		/**
	 * Re-used option getter for Show Accounts multiselect pane
	 */
		function getShowAccountsOptions(&$ie)
		{
			global $current_user;
			global $app_strings;
			global $mod_strings;
			
			$ieAccountsFull = $ie->retrieveAllByGroupId($current_user->id);
			$ieAccountsShowOptionsMeta = array();
			$showFolders = unserialize(base64_decode($current_user->getPreference('showFolders', 'Emails')));
			
			$defaultIEAccount = $ie->getUsersDefaultOutboundServerId($current_user);
			
			foreach ($ieAccountsFull as $k => $v) {
				$selected = (!empty($showFolders) && in_array($v->id, $showFolders)) ? true : false;
				$default = ($defaultIEAccount == $v->id) ? TRUE : FALSE;
				$has_groupfolder = !empty($v->groupfolder_id) ? TRUE : FALSE;
				$type = ($v->is_personal) ? $mod_strings['LBL_MAILBOX_TYPE_PERSONAL'] : $mod_strings['LBL_MAILBOX_TYPE_GROUP'];
				
				$ieAccountsShowOptionsMeta[] = array("id" => $v->id, "name" => $v->name, 'is_active' => $selected, 
					'server_url' => $v->server_url, 'is_group' => !$v->is_personal, 'group_id' => $v->group_id, 
					'is_default' => $default, 'has_groupfolder' => $has_groupfolder, 'type' => $type);
			}
			
			//Retrieve the grou folders
			$f = new SugarFolder();
			$groupFolders = $f->getGroupFoldersForSettings($current_user);
			
			foreach ($groupFolders as $singleGroup) {
				//Retrieve the related IE accounts.
				$relatedIEAccounts = $ie->retrieveByGroupFolderId($singleGroup['id']);
				
				if (count($relatedIEAccounts) == 0) 
					$server_url = $app_strings['LBL_EMAIL_MULT_GROUP_FOLDER_ACCOUNTS_EMPTY'];
				 else 
					if (count($relatedIEAccounts) == 1) {
						if ($relatedIEAccounts[0]->status != 'Active' || $relatedIEAccounts[0]->mailbox_type == 'bounce') 
							continue;
						
						$server_url = $relatedIEAccounts[0]->server_url;
					} else 
						$server_url = $app_strings['LBL_EMAIL_MULT_GROUP_FOLDER_ACCOUNTS'];
				
				$type = $mod_strings['LBL_MAILBOX_TYPE_GROUP_FOLDER'];
				$ieAccountsShowOptionsMeta[] = array("id" => $singleGroup['id'], "name" => $singleGroup['origName'], 'is_active' => $singleGroup['selected'], 
					'server_url' => $server_url, 'is_group' => true, 'group_id' => $singleGroup['id'], 
					'is_default' => FALSE, 'has_groupfolder' => true, 'type' => $type);
			}
			
			
			return $ieAccountsShowOptionsMeta;
		}
		
		function getShowAccountsOptionsForSearch(&$ie)
		{
			global $current_user;
			global $app_strings;
			
			$ieAccountsFull = $ie->retrieveAllByGroupId($current_user->id);
			//$ieAccountsShowOptions = "<option value=''>{$app_strings['LBL_NONE']}</option>\n";
			$ieAccountsShowOptionsMeta = array();
			$ieAccountsShowOptionsMeta[] = array("value" => "", "text" => $app_strings['LBL_NONE'], 'selected' => '');
			$showFolders = unserialize(base64_decode($current_user->getPreference('showFolders', 'Emails')));
			
			foreach ($ieAccountsFull as $k => $v) {
				if (!in_array($v->id, $showFolders)) {
					continue;
				}
				$group = (!$v->is_personal) ? $app_strings['LBL_EMAIL_GROUP'] . "." : "";
				$ieAccountsShowOptionsMeta[] = array("value" => $v->id, "text" => $group . $v->name, 'protocol' => $v->protocol);
			}
			
			return $ieAccountsShowOptionsMeta;
		}
		/**
	 * Formats a display message on successful async call
	 * @param string $type Type of message to display
	 */
		function displaySuccessMessage($type)
		{
			global $app_strings;
			
			switch ($type) {
				case "delete":
					$message = $app_strings['LBL_EMAIL_DELETE_SUCCESS'];
					break;
				
				default:
					$message = "NOOP: invalid type";
					break;
			}
			
			$this->smarty->assign('app_strings', $app_strings);
			$this->smarty->assign('message', $message);
			echo $this->smarty->fetch("modules/Emails/templates/successMessage.tpl");
		}
		
		/**
	 * Validates existence and expiration of a cache file
	 * @param string $ieId
	 * @param string $type Type of cache file: folders, messages, etc.
	 * @param string $file The cachefile name
	 * @param int refreshOffset Refresh time in secs.
	 * @return mixed.
	 */
		function validCacheFileExists($ieId, $type, $file, $refreshOffset = -1)
		{
			global $sugar_config;
			
			if ($refreshOffset == -1) {
				$refreshOffset = $this->cacheTimeouts[$type]; // use defaults
			}
			
			$cacheFilePath = sugar_cached("modules/Emails/{$ieId}/{$type}/{$file}");
			if (file_exists($cacheFilePath)) {
				return true;
			}
			
			return false;
		}
		
		/**
	 * retrieves the cached value
	 * @param string $ieId
	 * @param string $type Type of cache file: folders, messages, etc.
	 * @param string $file The cachefile name
	 * @param string $key name of cache value
	 * @return mixed
	 */
		function getCacheValue($ieId, $type, $file, $key)
		{
			//TODO (MT): specify type of proc. parameters
			global $sugar_config;
			
			$cleanIeId = cleanDirName($ieId);
			$cleanType = cleanDirName($type);
			$cleanFile = cleanFileName($file);
			$cacheFilePath = sugar_cached("modules/Emails/{$cleanIeId}/{$cleanType}/{$cleanFile}");
			$cacheFile = array();
			
			if (file_exists($cacheFilePath)) {
				include ($cacheFilePath); // provides $cacheFile
				
				if (isset($cacheFile[$key])) {
					$ret = unserialize($cacheFile[$key]);
					return $ret;
				}
			} else {
				$GLOBALS['log']->debug("EMAILUI: cache file not found [ {$cacheFilePath} ] - creating blank cache file");
				$this->writeCacheFile('retArr', array(), $ieId, $type, $file);
			}
			
			return null;
		}
		
		/**
	 * retrieves the cache file last touched time
	 * @param string $ieId
	 * @param string $type Type of cache file: folders, messages, etc.
	 * @param string $file The cachefile name
	 * @return string
	 */
		function getCacheTimestamp($ieId, $type, $file)
		{
			//TODO (MT): specify type of proc. parameters
			global $sugar_config;
			
			$cacheFilePath = sugar_cached("modules/Emails/{$ieId}/{$type}/{$file}");
			$cacheFile = array();
			
			if (file_exists($cacheFilePath)) {
				include ($cacheFilePath); // provides $cacheFile['timestamp']
				
				if (isset($cacheFile['timestamp'])) {
					$GLOBALS['log']->debug("EMAILUI: found timestamp [ {$cacheFile['timestamp']} ]");
					return $cacheFile['timestamp'];
				}
			}
			
			return '';
		}
		
		/**
	 * Updates the timestamp for a cache file - usually to mark a "check email"
	 * process
	 * @param string $ieId
	 * @param string $type Type of cache file: folders, messages, etc.
	 * @param string $file The cachefile name
	 */
		function setCacheTimestamp($ieId, $type, $file)
		{
			//TODO (MT): specify type of proc. parameters
			global $sugar_config;
			
			$cacheFilePath = sugar_cached("modules/Emails/{$ieId}/{$type}/{$file}");
			$cacheFile = array();
			
			if (file_exists($cacheFilePath)) {
				include ($cacheFilePath); // provides $cacheFile['timestamp']
				
				if (isset($cacheFile['timestamp'])) {
					$cacheFile['timestamp'] = strtotime('now');
					$GLOBALS['log']->debug("EMAILUI: setting updated timestamp [ {$cacheFile['timestamp']} ]");
					return $this->_writeCacheFile($cacheFile, $cacheFilePath);
				}
			}
		}
		
		
		/**
	 * Writes caches to flat file in cache dir.
	 * @param string $key Key to the main cache entry (not timestamp)
	 * @param mixed $var Variable to be cached
	 * @param string $ieId I-E focus ID
	 * @param string $type Folder in cache
	 * @param string $file Cache file name
	 */
		function writeCacheFile($key, $var, $ieId, $type, $file)
		{
			//TODO (MT): specify type of proc. parameters
			global $sugar_config;
			
			$cleanIeId = cleanDirName($ieId);
			$cleanType = cleanDirName($type);
			$cleanFile = cleanFileName($file);
			$the_file = sugar_cached("modules/Emails/{$cleanIeId}/{$cleanType}/{$cleanFile}");
			$timestamp = strtotime('now');
			$array = array();
			$array['timestamp'] = $timestamp;
			$array[$key] = serialize($var); // serialized since varexport_helper() can't handle PHP objects
			
			return $this->_writeCacheFile($array, $the_file);
		}
		
		/**
	 * Performs the actual file write.  Abstracted from writeCacheFile() for
	 * flexibility
	 * @param array $array The array to write to the cache
	 * @param string $file Full path (relative) with cache file name
	 * @return bool
	 */
		function _writeCacheFile($array, $file)
		{
			//TODO (MT): specify type of proc. parameters
			global $sugar_config;
			
			$arrayString = var_export_helper($array);
			
			$date = date("r");
			$the_string = <<<eoq
<?php // created: {$date}
	\$cacheFile = {$arrayString};
//?>
eoq;
			if ($fh = @sugar_fopen($file, "w")) {
				fputs($fh, $the_string);
				fclose($fh);
				return true;
			} else {
				$GLOBALS['log']->debug("EMAILUI: Could not write cache file [ {$file} ]");
				return false;
			}
		}
		
		/**
	 * Generate JSON encoded data to be consumed by yui datatable.
	 *
	 * @param array $data
	 * @param string $resultsParam The resultsList name
	 * @return string
	 */
		function jsonOuput($data, $resultsParam, $count = 0, $fromCache = true, $unread = -1)
		{
			global $app_strings;
			
			$count = ($count > 0) ? $count : 0;
			
			if (isset($a['fromCache'])) 
				$cached = ($a['fromCache'] == 1) ? 1 : 0;
			 else 
				$cached = ($fromCache) ? 1 : 0;
			
			if ($data['mbox'] == 'undefined' || empty($data['mbox'])) 
				$data['mbox'] = $app_strings['LBL_NONE'];
			
			$jsonOut = array('TotalCount' => $count, 'FromCache' => $cached, 'UnreadCount' => $unread, $resultsParam => $data['out']);
			
			return json_encode($jsonOut);
		}
		/**
	 * generates XML output from an array
	 * @param array
	 * @param string master list Item
	 * @return string
	 */
		function xmlOutput($a, $paramName, $count = 0, $fromCache = true, $unread = -1)
		{
			global $app_strings;
			$count = ($count > 0) ? $count : 0;
			
			if (isset($a['fromCache'])) {
				$cached = ($a['fromCache'] == 1) ? 1 : 0;
			} else {
				$cached = ($fromCache) ? 1 : 0;
			}
			
			if ($a['mbox'] == 'undefined' || empty($a['mbox'])) {
				$a['mbox'] = $app_strings['LBL_NONE'];
			}
			
			$xml = $this->arrayToXML($a['out'], $paramName);
			
			$ret = <<<eoq
<?xml version="1.0" encoding="UTF-8"?>
<EmailPage>
<TotalCount>{$count}</TotalCount>
<UnreadCount>{$unread}</UnreadCount>
<FromCache> {$cached} </FromCache>
<{$paramName}s>
{$xml}
</{$paramName}s>
</EmailPage>
eoq;
			return $ret;
		}
		
		/**
     * Generate to/cc addresses string in email detailview.
     *
     * @param string $str
     * @param string $target values: to, cc
     * @param int $defaultNum
     * @return string $str
     */
		function generateExpandableAddrs($str)
		{
			global $mod_strings;
			$tempStr = $str . ',';
			$tempStr = html_entity_decode($tempStr);
			$tempStr = $this->unifyEmailString($tempStr);
			$defaultNum = 2;
			$pattern = '/@.*,/U';
			preg_match_all($pattern, $tempStr, $matchs);
			$totalCount = count($matchs[0]);
			
			if (!empty($matchs[0]) && $totalCount > $defaultNum) {
				$position = strpos($tempStr, $matchs[0][$defaultNum]);
				$hiddenCount = $totalCount - $defaultNum;
				$frontStr = substr($tempStr, 0, $position);
				$backStr = substr($tempStr, $position, -1);
				$str = htmlentities($frontStr) . '<a class="utilsLink" onclick="javascript: SUGAR.email2.detailView.displayAllAddrs(this);">...[' . $mod_strings['LBL_EMAIL_DETAIL_VIEW_SHOW'] . $hiddenCount . $mod_strings['LBL_EMAIL_DETAIL_VIEW_MORE'] . ']</a><span style="display: none;">' . htmlentities($backStr) . '</span>';
			}
			
			return $str;
		}
		
		/**
     * Unify the seperator as ,
     *
     * @param String $str email address string
     * @return String converted string
     */
		function unifyEmailString($str)
		{
			preg_match_all('/@.*;/U', $str, $matches);
			if (!empty($matches[0])) {
				foreach ($matches[0] as $key => $value) {
					$new[] = str_replace(";", ",", $value);
				}
				return str_replace($matches[0], $new, $str);
			}
			return $str;
		}
	}
	// end class def
	
	
	//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

  * Description:
  * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc. All Rights
  * Reserved. Contributor(s): ______________________________________..
  *********************************************************************************/
	//increate timeout for phpo script execution
	ini_set('max_execution_time', 300);
	//ajaxInit();
	
	
	require_once ("include/OutboundEmail/OutboundEmail.php");
	require_once ("include/ytree/Tree.php");
	require_once ("include/ytree/ExtNode.php");
	
	$email = new Email();
	$email->email2init();
	$ie = new InboundEmail();
	$ie->email = $email;
	$json = getJSONobj();
	
	
	$showFolders = unserialize(base64_decode($current_user->getPreference('showFolders', 'Emails')));
	
	if (isset($_REQUEST['emailUIAction'])) {
		switch ($_REQUEST['emailUIAction']) {
			
			
			///////////////////////////////////////////////////////////////////////////
			////    COMPOSE REPLY FORWARD
			// this is used in forward/reply
			case "composeEmail":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: composeEmail");
				if (isset($_REQUEST['sugarEmail']) && $_REQUEST['sugarEmail'] == 'true' && isset($_REQUEST['uid']) && !empty($_REQUEST['uid'])) {
					$ie->email->retrieve($_REQUEST['uid']);
					$ie->email->from_addr = $ie->email->from_addr_name;
					$ie->email->to_addrs = to_html($ie->email->to_addrs_names);
					$ie->email->cc_addrs = to_html($ie->email->cc_addrs_names);
					$ie->email->bcc_addrs = $ie->email->bcc_addrs_names;
					$ie->email->from_name = $ie->email->from_addr;
					$email = $ie->email->et->handleReplyType($ie->email, $_REQUEST['composeType']);
					$ret = $ie->email->et->displayComposeEmail($email);
					$ret['description'] = empty($email->description_html) ? str_replace("\n", "\n<BR/>", $email->description) : $email->description_html;
					//get the forward header and add to description
					$forward_header = $email->getForwardHeader();
					
					$ret['description'] = $forward_header . $ret['description'];
					if ($_REQUEST['composeType'] == 'forward') {
						$ret = $ie->email->et->getDraftAttachments($ret);
					}
					$ret = $ie->email->et->getFromAllAccountsArray($ie, $ret);
					$ret['from'] = from_html($ret['from']);
					$ret['name'] = from_html($ret['name']);
					$out = $json->encode($ret, true);
					echo $out;
				} elseif (isset($_REQUEST['uid']) && !empty($_REQUEST['uid']) && isset($_REQUEST['ieId']) && !empty($_REQUEST['ieId'])) {
					$ie->retrieve($_REQUEST['ieId']);
					$ie->mailbox = $_REQUEST['mbox'];
					global $timedate;
					$ie->setEmailForDisplay($_REQUEST['uid']);
					$ie->email->date_start = $timedate->to_display_date($ie->email->date_sent);
					$ie->email->time_start = $timedate->to_display_time($ie->email->date_sent);
					$ie->email->date_sent = $timedate->to_display_date_time($ie->email->date_sent);
					$email = $ie->email->et->handleReplyType($ie->email, $_REQUEST['composeType']);
					$ret = $ie->email->et->displayComposeEmail($email);
					if ($_REQUEST['composeType'] == 'forward') {
						$ret = $ie->email->et->createCopyOfInboundAttachment($ie, $ret, $_REQUEST['uid']);
					}
					$ret = $ie->email->et->getFromAllAccountsArray($ie, $ret);
					$ret['from'] = from_html($ret['from']);
					$ret['name'] = from_html($ret['name']);
					$ret['ieId'] = $_REQUEST['ieId'];
					$ret['mbox'] = $_REQUEST['mbox'];
					$out = $json->encode($ret, true);
					echo $out;
				}
				break;
			
			/**
         * sendEmail handles both send and save draft duties
         */
			case "sendEmail":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: sendEmail");
				
				
				$sea = new SugarEmailAddress();
				
				$email->type = 'out';
				$email->status = 'sent';
				
				if (isset($_REQUEST['email_id']) && !empty($_REQUEST['email_id'])) {
					// && isset($_REQUEST['saveDraft']) && !empty($_REQUEST['saveDraft'])) {
					$email->retrieve($_REQUEST['email_id']); // uid is GUID in draft cases
				}
				if (isset($_REQUEST['uid']) && !empty($_REQUEST['uid'])) {
					$email->uid = $_REQUEST['uid'];
				}
				
				if ($email->email2Send($_REQUEST)) {
					$ret = array(
						'composeLayoutId' => $_REQUEST['composeLayoutId']);
					$out = $json->encode($ret, true);
					echo $out; // async call to close the proper compose tab
				}
				break;
			
			case "uploadAttachment":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: uploadAttachment");
				$metadata = $email->email2saveAttachment();
				
				if (!empty($metadata)) {
					$out = $json->encode($metadata);
					echo $out;
				}
				break;
			
			case "removeUploadedAttachment":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: removeUploadedAttachment");
				$fileFromRequest = from_html($_REQUEST['file']);
				$fileGUID = substr($fileFromRequest, 0, 36);
				// Bug52727: sanitize fileGUID to remove path components: /\.
				$fileGUID = cleanDirName($fileGUID);
				$fileName = $email->et->userCacheDir . "/" . $fileGUID;
				$filePath = clean_path($fileName);
				unlink($filePath);
				break;
			
			case "fillComposeCache":
				// fills client-side compose email cache with signatures and email templates
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: fillComposeCache");
				$out = array();
				$email_templates_arr = $email->et->getEmailTemplatesArray();
				natcasesort($email_templates_arr);
				$out['emailTemplates'] = $email_templates_arr;
				$sigs = $current_user->getSignaturesArray();
				// clean "none"
				foreach ($sigs as $k => $v) {
					if ($k == "") {
						$sigs[$k] = $app_strings['LBL_NONE'];
					} else 
						if (is_array($v) && isset($v['name'])) {
							$sigs[$k] = $v['name'];
						} else {
							$sigs[$k] = $v;
						}
				}
				$out['signatures'] = $sigs;
				$out['fromAccounts'] = $email->et->getFromAccountsArray($ie);
				$out['errorArray'] = array();
				
				$oe = new OutboundEmail();
				if ($oe->doesUserOverrideAccountRequireCredentials($current_user->id)) {
					$overideAccount = $oe->getUsersMailerForSystemOverride($current_user->id);
					//If the user override account has not been created yet, create it for the user.
					if ($overideAccount == null) 
						$overideAccount = $oe->createUserSystemOverrideAccount($current_user->id);
					
					$out['errorArray'] = array($overideAccount->id => $app_strings['LBL_EMAIL_WARNING_MISSING_USER_CREDS']);
				}
				
				$ret = $json->encode($out);
				echo $ret;
				break;
			
			case "getSignature":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: getSignature");
				if (isset($_REQUEST['id'])) {
					$signature = $current_user->getSignature($_REQUEST['id']);
					$signature['signature_html'] = from_html($signature['signature_html']);
					$out = $json->encode($signature);
					echo $out;
				} else {
					die();
				}
				break;
			
			case "deleteSignature":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: deleteSignature");
				if (isset($_REQUEST['id'])) {
					require_once ("modules/Users/UserSignature.php");
					$us = new UserSignature();
					$us->mark_deleted($_REQUEST['id']);
					$signatureArray = $current_user->getSignaturesArray();
					// clean "none"
					foreach ($signatureArray as $k => $v) {
						if ($k == "") {
							$sigs[$k] = $app_strings['LBL_NONE'];
						} else 
							if (is_array($v) && isset($v['name'])) {
								$sigs[$k] = $v['name'];
							} else {
								$sigs[$k] = $v;
							}
					}
					$out['signatures'] = $signatureArray;
					$ret = $json->encode($out);
					echo $ret;
				} else {
					die();
				}
				break;
			case "getTemplateAttachments":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: getTemplateAttachments");
				if (isset($_REQUEST['parent_id']) && !empty($_REQUEST['parent_id'])) {
					
					
					$where = "parent_id='{$_REQUEST['parent_id']}'";
					$order = "";
					$seed = new Note();
					$fullList = $seed->get_full_list($order, $where, '');
					$all_fields = array_merge($seed->column_fields, $seed->additional_column_fields);
					
					$js_fields_arr = array();
					
					$i = 1; // js doesn't like 0 index?
					if (!empty($fullList)) {
						foreach ($fullList as $note) {
							$js_fields_arr[$i] = array();
							
							foreach ($all_fields as $field) {
								if (isset($note->$field)) {
									$note->$field = from_html($note->$field);
									$note->$field = preg_replace('/\r\n/', '<BR>', $note->$field);
									$note->$field = preg_replace('/\n/', '<BR>', $note->$field);
									$js_fields_arr[$i][$field] = addslashes($note->$field);
								}
							}
							$i++;
						}
					}
					
					$out = $json->encode($js_fields_arr);
					echo $out;
				}
				break;
			////    END COMPOSE REPLY FORWARD
			///////////////////////////////////////////////////////////////////////////
			
			
			
			///////////////////////////////////////////////////////////////////////////
			////    MESSAGE HANDLING
			case "displayView":
				$ret = array();
				$ie->retrieve($_REQUEST['ieId']);
				$ie->mailbox = $_REQUEST['mailbox'];
				$ie->connectMailserver();
				
				switch ($_REQUEST['type']) {
					case "headers":
						$title = "{$app_strings['LBL_EMAIL_VIEW_HEADERS']}";
						$text = $ie->getFormattedHeaders($_REQUEST['uid']);
						break;
					
					case "raw":
						$title = "{$app_strings['LBL_EMAIL_VIEW_RAW']}";
						$text = $ie->getFormattedRawSource($_REQUEST['uid']);
						break;
					
					case "printable":
						
						break;
				}
				
				$ret['html'] = $text;
				$ret['title'] = $title;
				
				$out = $json->encode($ret, true);
				echo $out;
				break;
			
			case "getQuickCreateForm":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: getQuickCreateForm");
				if (isset($_REQUEST['qc_module']) && !empty($_REQUEST['qc_module'])) {
					if (!ACLController::checkAccess($_REQUEST['qc_module'], 'edit', true)) {
						echo trim($json->encode(array('html' => translate('LBL_NO_ACCESS', 'ACL')), true));
						break;
					}
					$people = array("Users", "Contacts", "Leads", "Prospects");
					$showSaveToAddressBookButton = false; //(in_array($_REQUEST['qc_module'], $people)) ? true : false;
					
					if (isset($_REQUEST['sugarEmail']) && !empty($_REQUEST['sugarEmail'])) {
						$ie->email->retrieve($_REQUEST['uid']); // uid is a sugar GUID in this case
					} else {
						$ie->retrieve($_REQUEST['ieId']);
						$ie->mailbox = $_REQUEST['mailbox'];
						$ie->setEmailForDisplay($_REQUEST['uid']);
					}
					$ret = $email->et->getQuickCreateForm($_REQUEST, $ie->email, $showSaveToAddressBookButton);
					$ret['ieId'] = $_REQUEST['ieId'];
					$ret['mbox'] = $_REQUEST['mailbox'];
					$ret['uid'] = $_REQUEST['uid'];
					$ret['module'] = $_REQUEST['qc_module'];
					if (!isset($_REQUEST['iframe'])) {
						$out = trim($json->encode($ret, false));
					} else {
						$out = $ret['html'];
					}
					echo $out;
				}
				break;
			
			case 'saveQuickCreate':
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: saveQuickCreate");
				require_once ('include/MVC/Controller/ControllerFactory.php');
				if (isset($_REQUEST['qcmodule'])) {
					$GLOBALS['log']->debug("********** QCmodule was set: {$_REQUEST['qcmodule']}");
				}
				$controller = ControllerFactory::getController($_REQUEST['qcmodule']);
				$controller->loadBean();
				$controller->pre_save();
				$GLOBALS['log']->debug("********** saving new {$controller->module}");
				$controller->action_save();
				//Relate the email to the new bean
				if (isset($_REQUEST['sugarEmail']) && $_REQUEST['sugarEmail'] == 'true' && isset($_REQUEST['uid']) && !empty($_REQUEST['uid'])) {
					$ie->email->retrieve($_REQUEST['uid']);
				} elseif (isset($_REQUEST['uid']) && !empty($_REQUEST['uid']) && isset($_REQUEST['ieId']) && !empty($_REQUEST['ieId'])) {
					$GLOBALS['log']->debug("********** Quick Create from non-imported message");
					$ie->retrieve($_REQUEST['ieId']);
					$ie->mailbox = $_REQUEST['mbox'];
					$ie->connectMailserver();
					$uid = $_REQUEST['uid'];
					if ($ie->protocol == 'imap') {
						$_REQUEST['uid'] = imap_msgno($ie->conn, $_REQUEST['uid']);
					} else {
						$_REQUEST['uid'] = $ie->getCorrectMessageNoForPop3($_REQUEST['uid']);
					}
					
					if (!$ie->importOneEmail($_REQUEST['uid'], $uid)) {
						$ie->getDuplicateEmailId($_REQUEST['uid'], $uid);
					}
					// id
					$ie->email->retrieve($ie->email->id);
					$GLOBALS['log']->debug("**********Imported Email");
					$ie->email->assigned_user_id = $controller->bean->assigned_user_id;
					$ie->email->assigned_user_name = $controller->bean->assigned_user_name;
				}
				if (isset($ie->email->id)) {
					if (empty($ie->email->parent_id)) {
						$ie->email->parent_id = $controller->bean->id;
						$ie->email->parent_type = $controller->module;
					}
					// if
					$ie->email->status = 'read';
					$ie->email->save();
					$mod = strtolower($controller->module);
					$ie->email->load_relationship($mod);
					$ie->email->$mod->add($controller->bean->id);
					if ($controller->bean->load_relationship('emails')) {
						$controller->bean->emails->add($ie->email->id);
					}
					if ($controller->bean->module_dir == 'Cases') {
						if ($controller->bean->load_relationship('contacts')) {
							$emailAddressWithName = $ie->email->from_addr;
							if (!empty($ie->email->reply_to_addr)) {
								$emailAddressWithName = $ie->email->reply_to_addr;
							}
							// if
							
							$emailAddress = SugarEmailAddress::_cleanAddress($emailAddressWithName);
							$contactIds = $ie->email->emailAddress->getRelatedId($emailAddress, 'contacts');
							if (!empty($contactIds)) {
								$controller->bean->contacts->add($contactIds);
							}
							// if
						}
						// if
					}
					// if
					echo $json->encode(array('id' => $ie->email->id));
				}
				break;
			
			case "getImportForm":
				$ie->retrieve($_REQUEST['ieId']);
				//            $ie->mailbox = $_REQUEST['mailbox'];
				$ie->setEmailForDisplay($_REQUEST['uid']);
				$ret = $email->et->getImportForm($_REQUEST, $ie->email);
				$out = trim($json->encode($ret, false));
				echo $out;
				break;
			
			case "getRelateForm":
				if (isset($_REQUEST['uid']) && !empty($_REQUEST['uid'])) {
					$uids = $json->decode(from_html($_REQUEST['uid']));
					$email->retrieve($uids[0]);
					$ret = $email->et->getImportForm(array('showTeam' => false, 'showAssignTo' => false, 'showDelete' => false), $email, 'RelateEditView');
					$out = trim($json->encode($ret, false));
					echo $out;
				}
				break;
			
			case "getEmail2DetailView":
				if (isset($_REQUEST['uid']) && !empty($_REQUEST['uid'])) {
					$ret = $email->et->getDetailViewForEmail2($_REQUEST['uid']);
					if (!isset($_REQUEST['print']) || $_REQUEST['print'] === FALSE) {
						$out = trim($json->encode($ret, false));
						echo $out;
					} else 
						echo $ret['html'];
					
				}
				break;
			
			case "relateEmails":
				if (isset($_REQUEST['uid']) && !empty($_REQUEST['uid']) && isset($_REQUEST['parent_id']) && !empty($_REQUEST['parent_id']) && isset($_REQUEST['parent_type']) && !empty($_REQUEST['parent_type'])) {
					$uids = explode($app_strings['LBL_EMAIL_DELIMITER'], $_REQUEST['uid']);
					$mod = strtolower($_REQUEST['parent_type']);
					$modId = $_REQUEST['parent_id'];
					foreach ($uids as $id) {
						$email = new Email();
						$email->retrieve($id);
						$email->parent_id = $modId;
						$email->parent_type = $_REQUEST['parent_type'];
						$email->status = 'read';
						
						// BUG FIX BEGIN
						// Bug 50979 - relating a message in group inbox removes message
						if (empty($email->assigned_user_id)) {
							$email->setFieldNullable('assigned_user_id');
						}
						$email->save();
						// Bug 50979 - reset assigned_user_id field defs
						if (empty($email->assigned_user_id)) {
							$email->revertFieldNullable('assigned_user_id');
						}
						// BUG FIX END
						
						$email->load_relationship($mod);
						$email->$mod->add($modId);
					}
				}
				break;
			
			
			case "getAssignmentDialogContent":
				$out = $email->distributionForm("");
				$out = trim($json->encode($out, false));
				echo $out;
				break;
			case "doAssignmentAssign":
				$out = $email->et->doAssignment($_REQUEST['distribute_method'], $_REQUEST['ieId'], $_REQUEST['folder'], $_REQUEST['uids'], $_REQUEST['users']);
				echo $out;
				break;
			case "doAssignmentDelete":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: doAssignmentDelete");
				if (isset($_REQUEST['uids']) && !empty($_REQUEST['uids']) && isset($_REQUEST['ieId']) && !empty($_REQUEST['ieId']) && isset($_REQUEST['folder']) && !empty($_REQUEST['folder'])) {
					$email->et->markEmails("deleted", $_REQUEST['ieId'], $_REQUEST['folder'], $_REQUEST['uids']);
				}
				break;
			case "markEmail":
				global $app_strings;
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: markEmail");
				if (isset($_REQUEST['uids']) && !empty($_REQUEST['uids']) && isset($_REQUEST['type']) && !empty($_REQUEST['type']) && isset($_REQUEST['folder']) && !empty($_REQUEST['folder']) && isset($_REQUEST['ieId']) && (!empty($_REQUEST['ieId']) || (empty($_REQUEST['ieId']) && strpos($_REQUEST['folder'], 'sugar::') !== false))) {
					$uid = $json->decode(from_html($_REQUEST['uids']));
					$uids = array();
					if (is_array($uid)) {
						$uids = $uid;
					} else {
						if (strpos($uid, $app_strings['LBL_EMAIL_DELIMITER']) !== false) {
							$uids = explode($app_strings['LBL_EMAIL_DELIMITER'], $uid);
						} else {
							$uids[] = $uid;
						}
					}
					// else
					$uids = implode($app_strings['LBL_EMAIL_DELIMITER'], $uids);
					$GLOBALS['log']->debug("********** EMAIL 2.0 - Marking emails $uids as {$_REQUEST['type']}");
					
					$ret = array();
					if (strpos($_REQUEST['folder'], 'sugar::') !== false && ($_REQUEST['type'] == 'deleted') && !ACLController::checkAccess('Emails', 'delete')) {
						$ret['status'] = false;
						$ret['message'] = $app_strings['LBL_EMAIL_DELETE_ERROR_DESC'];
					} else {
						$email->et->markEmails($_REQUEST['type'], $_REQUEST['ieId'], $_REQUEST['folder'], $uids);
						$ret['status'] = true;
					}
					$out = trim($json->encode($ret, false));
					echo $out;
				}
				break;
			
			case "checkEmail2":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: checkEmail2");
				
				$showFolders = unserialize(base64_decode($current_user->getPreference('showFolders', 'Emails')));
				
				$ret = array();
				$ret['numberAccounts'] = count($showFolders);
				
				$GLOBALS['log']->info("EMAIL2.0: async checkEmail - found [ " . $ret['numberAccounts'] . " ] accounts to check");
				
				if (!empty($showFolders) && is_array($showFolders)) {
					foreach ($showFolders as $ieId) {
						$ieId = trim($ieId);
						
						if (!empty($ieId)) {
							$GLOBALS['log']->info("INBOUNDEMAIL: trying to check email for GUID [ {$ieId} ]");
							$ie->disconnectMailserver();
							$ie->retrieve($ieId);
							
							$ret[$ieId] = $ie->checkEmail2_meta();
						}
					}
				} else {
					$GLOBALS['log']->info("EMAIL2.0: at checkEmail() async call - not subscribed accounts to check.");
				}
				
				
				
				$out = $json->encode($ret, true);
				echo $out;
				break;
			
			case "checkEmail":
				$GLOBALS['log']->info("[EMAIL] - Start checkEmail action for user [{$current_user->user_name}]");
				if (isset($_REQUEST['ieId']) && !empty($_REQUEST['ieId'])) {
					$ie->retrieve($_REQUEST['ieId']);
					$ie->mailbox = (isset($_REQUEST['mbox']) && !empty($_REQUEST['mbox'])) ? $_REQUEST['mbox'] : "INBOX";
					$ie->checkEmail(false);
				} elseif (isset($_REQUEST['all']) && !empty($_REQUEST['all'])) {
					$showFolders = unserialize(base64_decode($current_user->getPreference('showFolders', 'Emails')));
					
					$GLOBALS['log']->info("[EMAIL] - checkEmail found " . count($showFolders) . " accounts to check for user [{$current_user->user_name}]");
					
					if (!empty($showFolders) && is_array($showFolders)) {
						foreach ($showFolders as $ieId) {
							$ieId = trim($ieId);
							if (!empty($ieId)) {
								$GLOBALS['log']->info("[EMAIL] - Start checking email for GUID [{$ieId}] for user [{$current_user->user_name}]");
								$ie->disconnectMailserver();
								// If I-E not exist - skip check
								if (is_null($ie->retrieve($ieId))) {
									$GLOBALS['log']->info("[EMAIL] - Inbound with GUID [{$ieId}] not exist");
									continue;
								}
								$ie->checkEmail(false);
								$GLOBALS['log']->info("[EMAIL] - Done checking email for GUID [{$ieId}] for user [{$current_user->user_name}]");
							}
						}
					} else {
						$GLOBALS['log']->info("EMAIL2.0: at checkEmail() async call - not subscribed accounts to check.");
					}
				}
				
				$tree = $email->et->getMailboxNodes(true); // preserve cache files
				$return = $tree->generateNodesRaw();
				$out = $json->encode($return);
				echo $out;
				$GLOBALS['log']->info("[EMAIL] - Done checkEmail action for user [{$current_user->user_name}]");
				break;
			
			case "checkEmailProgress":
				$GLOBALS['log']->info("[EMAIL] - Start checkEmail action for user [{$current_user->user_name}]");
				if (isset($_REQUEST['ieId']) && !empty($_REQUEST['ieId'])) {
					$ie->retrieve($_REQUEST['ieId']);
					$ie->mailbox = (isset($_REQUEST['mbox']) && !empty($_REQUEST['mbox'])) ? $_REQUEST['mbox'] : "INBOX";
					$synch = (isset($_REQUEST['synch']) && ($_REQUEST['synch'] == "true"));
					if (!$ie->is_personal) {
						$return = array('status' => "done");
					} else {
						if ($ie->protocol == "pop3") {
							$return = $ie->pop3_checkPartialEmail($synch);
						} else {
							$return = $ie->checkEmailIMAPPartial(false, $synch);
						}
						// else
					}
					// if
					$return['ieid'] = $ie->id;
					$return['synch'] = $synch;
					if (isset($_REQUEST['totalcount']) && !empty($_REQUEST['totalcount']) && $_REQUEST['totalcount'] >= 0) {
						if ($ie->protocol == "pop3") {
							$return['totalcount'] = $_REQUEST['totalcount'];
						}
						// else
					}
					echo $json->encode($return);
				}
				// if
				break;
			
			case "getAllFoldersTree":
				$tree = $email->et->getMailboxNodes(true); // preserve cache files
				$return = $tree->generateNodesRaw();
				$out = $json->encode($return);
				echo $out;
				$GLOBALS['log']->info("[EMAIL] - Done checkEmail action for user [{$current_user->user_name}]");
				break;
			
			case "synchronizeEmail":
				$GLOBALS['log']->info("[EMAIL] Start action synchronizeEmail for user [{$current_user->user_name}]");
				$ie->syncEmail(true);
				$tree = $email->et->getMailboxNodes(true);
				$return = $tree->generateNodesRaw();
				$out = $json->encode($return);
				echo $out;
				$GLOBALS['log']->info("[EMAIL] Done action synchronizeEmail for user [{$current_user->user_name}]");
				break;
			
			case "importEmail":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: importEmail");
				$ie->retrieve($_REQUEST['ieId']);
				$ie->mailbox = $_REQUEST['mbox'];
				$ie->connectMailserver();
				$return = array();
				$status = true;
				$count = 1;
				if (strpos($_REQUEST['uid'], $app_strings['LBL_EMAIL_DELIMITER']) !== false) {
					$exUids = explode($app_strings['LBL_EMAIL_DELIMITER'], $_REQUEST['uid']);
					foreach ($exUids as $msgNo) {
						$uid = $msgNo;
						if ($ie->protocol == 'imap') {
							$msgNo = imap_msgno($ie->conn, $msgNo);
							$status = $ie->importOneEmail($msgNo, $uid);
						} else {
							$status = $ie->importOneEmail($ie->getCorrectMessageNoForPop3($msgNo), $uid);
						}
						// else
						$return[] = $app_strings['LBL_EMAIL_MESSAGE_NO'] . " " . $count . ", " . $app_strings['LBL_STATUS'] . " " . ($status ? $app_strings['LBL_EMAIL_IMPORT_SUCCESS'] : $app_strings['LBL_EMAIL_IMPORT_FAIL']);
						$count++;
						if (($_REQUEST['delete'] == 'true') && $status && ($current_user->is_admin == 1 || $ie->group_id == $current_user->id)) {
							$ie->deleteMessageOnMailServer($uid);
							$ie->deleteMessageFromCache($uid);
						}
						// if
					}
					// for
				} else {
					$msgNo = $_REQUEST['uid'];
					if ($ie->protocol == 'imap') {
						$msgNo = imap_msgno($ie->conn, $_REQUEST['uid']);
						$status = $ie->importOneEmail($msgNo, $_REQUEST['uid']);
					} else {
						$status = $ie->importOneEmail($ie->getCorrectMessageNoForPop3($msgNo), $_REQUEST['uid']);
					}
					// else
					$return[] = $app_strings['LBL_EMAIL_MESSAGE_NO'] . " " . $count . ", " . $app_strings['LBL_STATUS'] . " " . ($status ? $app_strings['LBL_EMAIL_IMPORT_SUCCESS'] : $app_strings['LBL_EMAIL_IMPORT_FAIL']);
					
					if (($_REQUEST['delete'] == 'true') && $status && ($current_user->is_admin == 1 || $ie->group_id == $current_user->id)) {
						$ie->deleteMessageOnMailServer($_REQUEST['uid']);
						$ie->deleteMessageFromCache($_REQUEST['uid']);
					}
					// if
				}
				// else
				echo $json->encode($return);
				break;
			
			case "setReadFlag":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: setReadFlag");
				$ie->retrieve($_REQUEST['ieId']);
				$ie->setReadFlagOnFolderCache($_REQUEST['mbox'], $_REQUEST['uid']);
				$email->et->getListEmails($_REQUEST['ieId'], $_REQUEST['mbox'], 0, 'true');
				//unlink("{$cacheRoot}/{$_REQUEST['ieId']}/folders/{$_REQUEST['mbox']}.php");
				break;
			
			case "deleteMessage":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: deleteMessage");
				if (isset($_REQUEST['uid']) && !empty($_REQUEST['uid']) && isset($_REQUEST['ieId']) && !empty($_REQUEST['ieId'])) {
					$ie->retrieve($_REQUEST['ieId']);
					$ie->mailbox = $_REQUEST['mbox'];
					
					if ($current_user->is_admin == 1 || $ie->group_id == $current_user->id) {
						$ie->deleteMessageOnMailServer($_REQUEST['uid']);
						$ie->deleteMessageFromCache($_REQUEST['uid']);
					} else {
						$GLOBALS['log']->debug("*** ERROR: tried to delete an email for an account for which {$current_user->full_name} is not the owner!");
						echo "NOOP: error see log";
					}
				} else {
					echo "error: missing credentials";
				}
				break;
			
			case "getSingleMessage":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: getSingleMessage");
				if (isset($_REQUEST['uid']) && !empty($_REQUEST['uid']) && isset($_REQUEST['ieId']) && !empty($_REQUEST['ieId'])) {
					// this method needs to guarantee UTF-8 charset - encoding detection
					// and conversion is unreliable, and can break valid UTF-8 text
					$out = $email->et->getSingleMessage($ie);
					
					echo $json->encode($out);
				} else {
					echo "error: no UID";
				}
				break;
			
			case "getSingleMessageFromSugar":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: getSingleMessageFromSugar");
				if (isset($_REQUEST['uid']) && !empty($_REQUEST['uid'])) {
					$email->retrieve($_REQUEST['uid']);
					//$email->description_html = from_html($email->description_html);
					$ie->email = $email;
					
					if ($email->status == 'draft' || $email->type == 'draft') {
						// forcing an editview since we are looking at a draft message
						global $current_user;
						$ret = $ie->email->et->displayComposeEmail($email);
						
						$ret = $ie->email->et->getDraftAttachments($ret, $ie);
						$ret = $ie->email->et->getFromAllAccountsArray($ie, $ret);
						
						
						$out = $json->encode($ret, true);
						echo $out;
					} else {
						$out = $ie->displayOneEmail($_REQUEST['uid'], $_REQUEST['mbox']);
						$out['meta']['email']['description'] = empty($email->description_html) ? str_replace("\n", "\n<BR/>", $email->description) : $email->description_html;
						$out['meta']['email']['date_start'] = $email->date_start;
						$out['meta']['email']['time_start'] = $email->time_start;
						$out['meta']['ieId'] = $_REQUEST['ieId'];
						$out['meta']['mbox'] = $_REQUEST['mbox'];
						$out['meta']['email']['toaddrs'] = $email->et->generateExpandableAddrs($out['meta']['email']['toaddrs']);
						if (!empty($out['meta']['email']['cc_addrs'])) {
							$ccs = $email->et->generateExpandableAddrs($out['meta']['email']['cc_addrs']);
							$out['meta']['email']['cc'] = <<<eoq
        				<tr>
        					<td NOWRAP valign="top" class="displayEmailLabel">
        						{$app_strings['LBL_EMAIL_CC']}:
        					</td>
        					<td class="displayEmailValue">
        						{$ccs}
        					</td>
        				</tr>
eoq;
						}
						echo $json->encode($out);
					}
				} else {
					echo "error: no UID";
				}
				break;
			
			case "getMultipleMessages":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: getMultipleMessages");
				if (isset($_REQUEST['uid']) && !empty($_REQUEST['uid']) && isset($_REQUEST['ieId']) && !empty($_REQUEST['ieId'])) {
					$exUids = explode(",", $_REQUEST['uid']);
					
					$out = array();
					foreach ($exUids as $k => $uid) {
						if ($email->et->validCacheFileExists($_REQUEST['ieId'], 'messages', $_REQUEST['mbox'] . $uid . ".php")) {
							$msg = $email->et->getCacheValue($_REQUEST['ieId'], 'messages', $_REQUEST['mbox'] . $uid . ".php", 'out');
						} else {
							$ie->retrieve($_REQUEST['ieId']);
							$ie->mailbox = $_REQUEST['mbox'];
							$ie->setEmailForDisplay($uid, false, true);
							$msg = $ie->displayOneEmail($uid, $_REQUEST['mbox']);
							$email->et->writeCacheFile('out', $msg, $_REQUEST['ieId'], 'messages', "{$_REQUEST['mbox']}{$uid}.php");
						}
						
						$out[] = $msg;
					}
					echo $json->encode($out);
				} else {
					echo "error: no UID";
				}
				break;
			
			case "getMultipleMessagesFromSugar":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: getMultipleMessagesFromSugar");
				if (isset($_REQUEST['uid']) && !empty($_REQUEST['uid'])) {
					$exIds = explode(",", $_REQUEST['uid']);
					$out = array();
					
					foreach ($exIds as $id) {
						$e = new Email();
						$e->retrieve($id);
						$e->description_html = from_html($e->description_html);
						$ie->email = $e;
						$out[] = $ie->displayOneEmail($id, $_REQUEST['mbox']);
					}
					
					echo $json->encode($out);
				}
				
				break;
			////    END MESSAGE HANDLING
			///////////////////////////////////////////////////////////////////////////
			
			
			
			///////////////////////////////////////////////////////////////////////////
			////    LIST VIEW
			case "getMessageCount":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: getMessageCount");
				
				$out = $ie->getCacheCount($_REQUEST['mbox']);
				echo $json->encode($out);
				break;
			
			case "getMessageList":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: getMessageListJSON");
				if (isset($_REQUEST['ieId']) && !empty($_REQUEST['ieId'])) {
					// user view preferences
					$email->et->saveListView($_REQUEST['ieId'], $_REQUEST['mbox']);
					// list output
					$ie->retrieve($_REQUEST['ieId']);
					if (isset($_REQUEST['start']) && isset($_REQUEST['limit'])) {
						$page = ceil($_REQUEST['start'] / $_REQUEST['limit']) + 1;
					} else {
						$page = 1;
					}
					$list = $ie->displayFolderContents($_REQUEST['mbox'], $_REQUEST['forceRefresh'], $page);
					$count = $ie->getCacheCount($_REQUEST['mbox']);
					$unread = $ie->getCacheUnread($_REQUEST['mbox']);
					$out = $email->et->jsonOuput($list, 'Email', $count, true, $unread);
					
					@ob_end_clean();
					ob_start();
					echo $out;
					ob_end_flush();
					//die();
				} else {
					echo "error: no ieID";
				}
				break;
			
			case "getMessageListSugarFolders":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: getMessageListSugarFoldersJSON");
				if (isset($_REQUEST['ieId']) && !empty($_REQUEST['ieId'])) {
					// user view preferences
					$email->et->saveListView($_REQUEST['ieId'], "SUGAR.{$_REQUEST['mbox']}");
					if (isset($_REQUEST['start']) && isset($_REQUEST['limit'])) {
						$page = ceil($_REQUEST['start'] / $_REQUEST['limit']) + 1;
					} else {
						$page = 1;
					}
					if (!isset($_REQUEST['sort']) || !isset($_REQUEST['dir'])) {
						$_REQUEST['sort'] = '';
						$_REQUEST['dir'] = '';
					}
					$emailSettings = $current_user->getPreference('emailSettings', 'Emails');
					// cn: default to a low number until user specifies otherwise
					if (empty($emailSettings['showNumInList'])) {
						$emailSettings['showNumInList'] = 20;
					}
					
					// jchi #9424, get sort and direction from user preference
					$sort = 'date';
					$direction = 'desc';
					$sortSerial = $current_user->getPreference('folderSortOrder', 'Emails');
					if (!empty($sortSerial) && !empty($_REQUEST['ieId']) && !empty($_REQUEST['mbox'])) {
						$sortArray = unserialize($sortSerial);
						$GLOBALS['log']->debug("********** EMAIL 2.0********** ary=" . print_r($sortArray, true) . ' id=' . $_REQUEST['ieId'] . '; box=' . $_REQUEST['mbox']);
						$sort = $sortArray[$_REQUEST['ieId']][$_REQUEST['mbox']]['current']['sort'];
						$direction = $sortArray[$_REQUEST['ieId']][$_REQUEST['mbox']]['current']['direction'];
					}
					//set sort and direction to user predference
					if (!empty($_REQUEST['sort']) && !empty($_REQUEST['dir'])) {
						$email->et->saveListViewSortOrder($_REQUEST['ieId'], $_REQUEST['mbox'], $_REQUEST['sort'], $_REQUEST['dir']);
						$sort = $_REQUEST['sort'];
						$direction = $_REQUEST['dir'];
					} else {
						$_REQUEST['sort'] = '';
						$_REQUEST['dir'] = '';
					}
					//end
					
					$metalist = $email->et->folder->getListItemsForEmailXML($_REQUEST['ieId'], $page, $emailSettings['showNumInList'], $sort, $direction);
					$count = $email->et->folder->getCountItems($_REQUEST['ieId']);
					
					if (!empty($_REQUEST['getUnread'])) {
						$out = $email->et->jsonOuput($metalist, 'Email', $count, false);
					} else {
						$unread = $email->et->folder->getCountUnread($_REQUEST['ieId']);
						$out = $email->et->jsonOuput($metalist, 'Email', $count, false, $unread);
					}
					
					@ob_end_clean();
					ob_start();
					echo $out;
					ob_end_flush();
				} else {
					echo "error: no ieID";
				}
				break;
			////    END LIST VIEW
			///////////////////////////////////////////////////////////////////////////
			
			
			
			///////////////////////////////////////////////////////////////////////////
			////    FOLDER ACTIONS
			case "emptyTrash":
				$email->et->emptyTrash($ie);
				break;
			
			case "clearInboundAccountCache":
				$email->et->clearInboundAccountCache($_REQUEST['ieId']);
				break;
			
			case "updateSubscriptions":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: updateSubscriptions");
				if (isset($_REQUEST['subscriptions']) && !empty($_REQUEST['subscriptions'])) {
					$subs = explode("::", $_REQUEST['subscriptions']);
					$childrenSubs = array();
					//Find all children of the group folder subscribed to and add
					//them to the list of folders to show.
					foreach ($subs as $singleSub) 
						$email->et->folder->findAllChildren($singleSub, $childrenSubs);
					
					$subs = array_merge($subs, $childrenSubs);
					$email->et->folder->setSubscriptions($subs);
				} elseif (empty($_REQUEST['subscriptions'])) {
					$email->et->folder->clearSubscriptions();
				}
				break;
			
			case "refreshSugarFolders":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: refreshSugarFolders");
				$rootNode = new ExtNode('', '');
				$folderOpenState = $current_user->getPreference('folderOpenState', 'Emails');
				$folderOpenState = (empty($folderOpenState)) ? "" : $folderOpenState;
				$ret = $email->et->folder->getUserFolders($rootNode, unserialize($folderOpenState), $current_user, true);
				$out = $json->encode($ret);
				echo $out;
				break;
			
			
			
			case "getFoldersForSettings":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: getFoldersForSettings");
				$ret = $email->et->folder->getFoldersForSettings($current_user);
				$out = $json->encode($ret);
				echo $out;
				break;
			
			case "moveEmails":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: moveEmails");
				$ie->moveEmails($_REQUEST['sourceIeId'], $_REQUEST['sourceFolder'], $_REQUEST['destinationIeId'], $_REQUEST['destinationFolder'], $_REQUEST['emailUids']);
				break;
			
			case "saveNewFolder":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: saveNewFolder");
				if (isset($_REQUEST['folderType']) && !empty($_REQUEST['folderType'])) {
					switch ($_REQUEST['folderType']) {
						case "sugar":
							$ret = $email->et->saveNewFolder($_REQUEST['nodeLabel'], $_REQUEST['parentId']);
							$out = $json->encode($ret);
							echo $out;
							break;
						
						case "imap":
							$ie->retrieve($_REQUEST['ieId']);
							$ie->connectMailserver();
							$ie->saveNewFolder($_REQUEST['newFolderName'], $_REQUEST['mbox']);
							break;
					}
				} else {
					echo "NOOP: no folderType defined";
				}
				break;
			
			case "setFolderViewSelection":
				// flows into next case statement
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: setFolderViewSelection");
				$viewFolders = $_REQUEST['ieIdShow'];
				$current_user->setPreference('showFolders', base64_encode(serialize($viewFolders)), '', 'Emails');
				$tree = $email->et->getMailboxNodes(false);
				$return = $tree->generateNodesRaw();
				$out = $json->encode($return);
				echo $out;
				break;
			
			case "deleteFolder":
				$v = $app_strings['LBL_NONE'];
				$return = array();
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: deleteFolder");
				if (isset($_REQUEST['folderType']) && !empty($_REQUEST['folderType'])) {
					switch ($_REQUEST['folderType']) {
						case "sugar":
							$status = $email->et->folder->deleteChildrenCascade($_REQUEST['folder_id']);
							if ($status == true) {
								$return['status'] = true;
								$return['folder_id'] = $_REQUEST['folder_id'];
							} else {
								$return['status'] = false;
								$return['errorMessage'] = $app_strings['LBL_EMAIL_ERROR_DELETE_GROUP_FOLDER'];
							}
							break;
						
						case "imap":
							$ie->retrieve($_REQUEST['ieId']);
							$ie->connectMailserver();
							$returnArray = $ie->deleteFolder($_REQUEST['mbox']);
							$status = $returnArray['status'];
							$errorMessage = $returnArray['errorMessage'];
							if ($status == true) {
								$return['status'] = true;
								$return['mbox'] = $_REQUEST['mbox'];
								$return['ieId'] = $_REQUEST['ieId'];
							} else {
								$return['status'] = false;
								$return['errorMessage'] = $errorMessage;
							}
							break;
					}
				} else {
					$return['status'] = false;
					$return['errorMessage'] = "NOOP: no folderType defined";
				}
				$out = $json->encode($return);
				echo $out;
				break;
			case "renameFolder":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: renameFolder");
				
				if (isset($_REQUEST['ieId']) && isset($_REQUEST['oldFolderName']) && !empty($_REQUEST['oldFolderName']) 
					&& isset($_REQUEST['newFolderName']) && !empty($_REQUEST['newFolderName'])) {
					$ie->retrieve($_REQUEST['ieId']);
					$ie->renameFolder($_REQUEST['oldFolderName'], $_REQUEST['newFolderName']);
				} elseif (isset($_REQUEST['folderId']) && !empty($_REQUEST['folderId']) && isset($_REQUEST['newFolderName']) && !empty($_REQUEST['newFolderName'])) {
					if (is_guid($_REQUEST['folderId'])) {
						$email->et->folder->retrieve($_REQUEST['folderId']);
						$email->et->folder->name = $_REQUEST['newFolderName'];
						$email->et->folder->save();
					} else {
						echo "NOOP - not a Sugar Folder";
					}
				}
			case "moveFolder":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: moveFolder");
				if (isset($_REQUEST['folderId']) && !empty($_REQUEST['folderId']) && isset($_REQUEST['newParentId']) && !empty($_REQUEST['newParentId']) && $_REQUEST['newParentId'] != $_REQUEST['folderId']) {
					if (is_guid($_REQUEST['folderId']) && is_guid($_REQUEST['newParentId'])) {
						$email->et->folder->retrieve($_REQUEST['folderId']);
						$email->et->folder->updateFolder(array(
							"record" => $_REQUEST['folderId'], 
							"name" => $email->et->folder->name, 
							"parent_folder" => $_REQUEST['newParentId'], 
							"team_id" => $email->et->folder->team_id, 
							"team_set_id" => $email->et->folder->team_set_id));
					} else {
						echo "NOOP - not a Sugar Folder";
					}
				}
				break;
			case "getGroupFolder":
				$email->et->folder->retrieve($_REQUEST['folderId']);
				$_REQUEST['record'] = $_REQUEST['folderId'];
				$ret = array();
				$ret['folderId'] = $email->et->folder->id;
				$ret['folderName'] = $email->et->folder->name;
				$ret['parentFolderId'] = $email->et->folder->parent_folder;
				$out = $json->encode($ret);
				echo $out;
				break;
			
			
			case "rebuildFolders":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: rebuildFolders");
				$tree = $email->et->getMailboxNodes(false);
				$return = $tree->generateNodesRaw();
				$out = $json->encode($return);
				echo $out;
				break;
			
			case "setFolderOpenState":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: setFolderOpenState");
				$email->et->saveFolderOpenState($_REQUEST['focusFolder'], $_REQUEST['focusFolderOpen']);
				break;
			
			case "saveListViewSortOrder":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: saveListViewSortOrder");
				$email->et->saveListViewSortOrder($_REQUEST['ieId'], $_REQUEST['focusFolder'], $_REQUEST['sortBy'], $_REQUEST['reverse']);
				break;
			////    END FOLDER ACTIONS
			///////////////////////////////////////////////////////////////////////////
			
			///////////////////////////////////////////////////////////////////////////
			////    INBOUND EMAIL ACCOUNTS
			
			case "retrieveAllOutbound":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: retrieveAllOutbound");
				global $current_user;
				$oe = new OutboundEmail();
				$outbounds = $oe->getUserMailers($current_user);
				$results = array('outbound_account_list' => $outbounds, 'count' => count($outbounds));
				$out = $json->encode($results, false);
				echo $out;
				
				break;
			
			case "editOutbound":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: editOutbound");
				if (isset($_REQUEST['outbound_email']) && !empty($_REQUEST['outbound_email'])) {
					$oe = new OutboundEmail();
					$oe->retrieve($_REQUEST['outbound_email']);
					
					$ret = array();
					
					foreach ($oe->field_defs as $def) {
						$ret[$def] = $oe->$def;
					}
					$ret['mail_smtppass'] = ''; // don't send back the password
					$ret['has_password'] = isset($oe->mail_smtppass);
					
					$out = $json->encode($ret, true);
					echo $out;
					
				} else {
					echo "NOOP";
				}
				break;
			
			case "deleteOutbound":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: deleteOutbound");
				if (isset($_REQUEST['outbound_email']) && !empty($_REQUEST['outbound_email'])) {
					$oe = new OutboundEmail();
					global $current_user;
					$oe->retrieve($_REQUEST['outbound_email']);
					$affectedInboundAccounts = $oe->getAssociatedInboundAccounts($current_user);
					
					//Check if the user has confirmed he wants to delete the email account even if associated to an inbound accnt.
					$confirmedDelete = (isset($_REQUEST['confirm']) && $_REQUEST['confirm']) ? TRUE : FALSE;
					
					if (count($affectedInboundAccounts) > 0 && !$confirmedDelete) {
						$results = array('is_error' => true, 'error_message' => $app_strings['LBL_EMAIL_REMOVE_SMTP_WARNING'], 'outbound_email' => $_REQUEST['outbound_email']);
					} else {
						$oe->delete();
						$results = array('is_error' => false, 'error_message' => '');
					}
					
					$out = $json->encode($results);
					@ob_end_clean();
					ob_start();
					echo $out;
					ob_end_flush();
					die();
				} else {
					echo "NOOP";
				}
				break;
			
			case "saveOutbound":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: saveOutbound");
				
				$oe = new OutboundEmail();
				$oe->id = $_REQUEST['mail_id'];
				$oe->retrieve($oe->id);
				$oe->name = $_REQUEST['mail_name'];
				$type = empty($_REQUEST['type']) ? 'user' : $_REQUEST['type'];
				$oe->type = $type;
				$oe->user_id = $current_user->id;
				$oe->mail_sendtype = "SMTP";
				$oe->mail_smtptype = $_REQUEST['mail_smtptype'];
				$oe->mail_smtpserver = $_REQUEST['mail_smtpserver'];
				$oe->mail_smtpport = $_REQUEST['mail_smtpport'];
				$oe->mail_smtpssl = $_REQUEST['mail_smtpssl'];
				$oe->mail_smtpauth_req = isset($_REQUEST['mail_smtpauth_req']) ? 1 : 0;
				$oe->mail_smtpuser = $_REQUEST['mail_smtpuser'];
				if (!empty($_REQUEST['mail_smtppass'])) {
					$oe->mail_smtppass = $_REQUEST['mail_smtppass'];
				}
				$oe = $oe->save();
				echo $oe->id;
				break;
			
			case "saveDefaultOutbound":
				global $current_user;
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: saveDefaultOutbound");
				$outbound_id = empty($_REQUEST['id']) ? "" : $_REQUEST['id'];
				$ie = new InboundEmail();
				$ie->setUsersDefaultOutboundServerId($current_user, $outbound_id);
				break;
			case "testOutbound":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: testOutbound");
				
				$pass = '';
				if (!empty($_REQUEST['mail_smtppass'])) {
					$pass = $_REQUEST['mail_smtppass'];
				} elseif (isset($_REQUEST['mail_name'])) {
					$oe = new OutboundEmail();
					$oe = $oe->getMailerByName($current_user, $_REQUEST['mail_name']);
					if (!empty($oe)) {
						$pass = $oe->mail_smtppass;
					}
				}
				$out = $email->sendEmailTest($_REQUEST['mail_smtpserver'], $_REQUEST['mail_smtpport'], $_REQUEST['mail_smtpssl'], (isset($_REQUEST['mail_smtpauth_req']) ? 1 : 0), $_REQUEST['mail_smtpuser'], $pass, $_REQUEST['outboundtest_from_address'], $_REQUEST['outboundtest_from_address']);
				
				$out = $json->encode($out);
				echo $out;
				break;
			
			case "rebuildShowAccount":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: rebuildShowAccount");
				$ret = $email->et->getShowAccountsOptions($ie);
				$results = array('account_list' => $ret, 'count' => count($ret));
				$out = $json->encode($results);
				echo $out;
				break;
			
			case "rebuildShowAccountForSearch":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: rebuildShowAccount");
				$ret = $email->et->getShowAccountsOptionsForSearch($ie);
				$out = $json->encode($ret);
				echo $out;
				break;
			
			case "deleteIeAccount":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: deleteIeAccount");
				if (isset($_REQUEST['group_id']) && $_REQUEST['group_id'] == $current_user->id) {
					$ret = array();
					$updateFolders = array();
					$ret['id'] = $_REQUEST['ie_id'];
					$out = $json->encode($ret);
					$ie->hardDelete($_REQUEST['ie_id']);
					$out = $json->encode(array('id' => $_REQUEST['ie_id']));
					echo $out;
					
					foreach ($showFolders as $id) {
						if ($id != $_REQUEST['ie_id']) {
							$updateFolders[] = $id;
						}
					}
					
					$showStore = base64_encode(serialize($updateFolders));
					$current_user->setPreference('showFolders', $showStore, 0, 'Emails');
				}
				break;
			
			case "saveIeAccount":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: saveIeAccount");
				if (isset($_REQUEST['server_url']) && !empty($_REQUEST['server_url'])) {
					if (false === $ie->savePersonalEmailAccount($current_user->id, $current_user->user_name, false)) {
						$ret = array('error' => 'error');
						$out = $json->encode($ret);
						echo $out;
					} else {
						$ie->retrieve($_REQUEST['ie_id']);
						if (!isset($ie->created_by_link)) {
							$ie->created_by_link = null;
						}
						if (!isset($ie->modified_user_id_link)) {
							$ie->modified_user_id_link = null;
						}
						if (!is_array($showFolders)) {
							$showFolders = array();
						}
						if (!in_array($ie->id, $showFolders)) {
							$showFolders[] = $ie->id;
							$showStore = base64_encode(serialize($showFolders));
							$current_user->setPreference('showFolders', $showStore, 0, 'Emails');
						}
						
						foreach ($ie->field_defs as $k => $v) {
							if (isset($v['type']) && ($v['type'] == 'link')) {
								continue;
							}
							if ($k == 'stored_options') {
								$ie->$k = unserialize(base64_decode($ie->$k));
								if (isset($ie->stored_options['from_name'])) {
									$ie->stored_options['from_name'] = from_html($ie->stored_options['from_name']);
								}
							} elseif ($k == 'service') {
								$service = explode("::", $ie->$k);
								$retService = array();
								
								foreach ($service as $serviceK => $serviceV) {
									if (!empty($serviceV)) {
										$retService[$serviceK] = $serviceV;
									}
								}
								
								$ie->$k = $retService;
							}
							
							if (isset($ie->$k)) 
								$ret[$k] = $ie->$k;
						}
						
						$out = $json->encode($ret);
						echo $out;
					}
					
					//If the user is saving the username/password then we need to update the outbound account.
					$outboundMailUser = (isset($_REQUEST['mail_smtpuser'])) ? $_REQUEST['mail_smtpuser'] : "";
					$outboundMailPass = (isset($_REQUEST['mail_smtppass'])) ? $_REQUEST['mail_smtppass'] : "";
					$outboundMailId = (isset($_REQUEST['outbound_email'])) ? $_REQUEST['outbound_email'] : "";
					
					if (!empty($outboundMailUser) && !empty($outboundMailPass) && !empty($outboundMailId)) {
						$oe = new OutboundEmail();
						$oe->retrieve($outboundMailId);
						$oe->mail_smtpuser = $outboundMailUser;
						$oe->mail_smtppass = $outboundMailPass;
						$oe->save();
					}
					
				} else {
					echo "NOOP";
				}
				break;
			
			case "getIeAccount":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: getIeAccount");
				$ie->retrieve($_REQUEST['ieId']);
				if ($ie->group_id == $current_user->id) {
					$ret = array();
					
					foreach ($ie->field_defs as $k => $v) {
						if ($k == 'stored_options') {
							$ie->$k = unserialize(base64_decode($ie->$k));
							if (isset($ie->stored_options['from_name'])) {
								$ie->stored_options['from_name'] = from_html($ie->stored_options['from_name']);
							}
						} elseif ($k == 'service') {
							$service = explode("::", $ie->$k);
							$retService = array();
							foreach ($service as $serviceK => $serviceV) {
								if (!empty($serviceV)) {
									$retService[$serviceK] = $serviceV;
								}
							}
							
							$ie->$k = $retService;
						}
						
						$ret[$k] = $ie->$k;
					}
					unset($ret['email_password']); // no need to send the password out
					
					$out = $json->encode($ret);
				} else {
					$out = "NOOP: ID mismatch";
				}
				echo $out;
				break;
			////    END INBOUND EMAIL ACCOUNTS
			///////////////////////////////////////////////////////////////////////////
			
			
			
			///////////////////////////////////////////////////////////////////////////
			////    SEARCH
			case "search":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: search");
				if (isset($_REQUEST['subject']) && !empty($_REQUEST['subject']) && isset($_REQUEST['ieId']) && !empty($_REQUEST['ieId'])) {
					$metalist = $ie->search($_REQUEST['ieId'], $_REQUEST['subject']);
					if (!isset($_REQUEST['page'])) {
						$_REQUEST['page'] = 1;
					}
					$_REQUEST['pageSize'] = count($metalist['out']);
					$out = $email->et->xmlOutput($metalist, 'Email', false);
					@ob_end_clean();
					ob_start();
					header("Content-type: text/xml");
					echo $out;
					ob_end_flush();
					die();
				} else {
					echo "NOOP: no search criteria found";
				}
				break;
			
			case "searchAdvanced":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: searchAdvanced");
				
				$metalist = $email->searchImportedEmails();
				$out = $email->et->jsonOuput($metalist, 'Email', $metalist['totalCount']);
				
				@ob_end_clean();
				ob_start();
				echo $out;
				ob_end_flush();
				die();
				
				break;
			////    END SEARCH
			///////////////////////////////////////////////////////////////////////////
			
			
			
			///////////////////////////////////////////////////////////////////////////
			////    SETTINGS
			case "loadPreferences":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: loadPreferences");
				$prefs = $email->et->getUserPrefsJS();
				$out = $json->encode($prefs);
				echo $out;
				break;
			
			case "saveSettingsGeneral":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: saveSettingsGeneral");
				$emailSettings = array();
				$emailSettings['emailCheckInterval'] = $_REQUEST['emailCheckInterval'];
				//$emailSettings['autoImport'] = isset($_REQUEST['autoImport']) ? '1' : '0';
				$emailSettings['alwaysSaveOutbound'] = '1';
				$emailSettings['sendPlainText'] = isset($_REQUEST['sendPlainText']) ? '1' : '0';
				$emailSettings['showNumInList'] = $_REQUEST['showNumInList'];
				$emailSettings['defaultOutboundCharset'] = $_REQUEST['default_charset'];
				$current_user->setPreference('emailSettings', $emailSettings, '', 'Emails');
				
				// signature
				$current_user->setPreference('signature_default', $_REQUEST['signature_id']);
				$current_user->setPreference('signature_prepend', (isset($_REQUEST['signature_prepend'])) ? true : false);
				
				$out = $json->encode($email->et->getUserPrefsJS());
				echo $out;
				break;
			
			case "setPreference":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: setPreference");
				if (isset($_REQUEST['prefName']) && isset($_REQUEST['prefValue'])) {
					// handle potential JSON encoding
					if (isset($_REQUEST['decode'])) {
						$_REQUEST['prefValue'] = $json->decode(from_html($_REQUEST['prefValue']));
					}
					
					$current_user->setPreference($_REQUEST['prefName'], $_REQUEST['prefValue'], '', 'Emails');
				}
				break;
			////    END SETTINGS
			///////////////////////////////////////////////////////////////////////////
			
			
			
			
			///////////////////////////////////////////////////////////////////////////
			////    ADDRESS BOOK
			
			case "editContact":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: editContact");
				if (isset($_REQUEST['id']) && !empty($_REQUEST['id'])) {
					$module = "Contacts";
					$ret = $email->et->getEditContact($_REQUEST['id'], $module);
				}
				$out = $json->encode($ret);
				echo $out;
				break;
			
			
			/* The four calls below all have the same return signature */
			case "removeContact":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: removeContacts");
				if (strpos($_REQUEST['ids'], "::") !== false) {
					$removeIds = explode("::", $_REQUEST['ids']);
				} else {
					$removeIds[] = $_REQUEST['ids'];
				}
				$email->et->removeContacts($removeIds);
			
			case "saveContactEdit":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: saveContactEdit");
				if (isset($_REQUEST['args']) && !empty($_REQUEST['args'])) {
					$email->et->saveContactEdit($_REQUEST['args']);
				}
			// flow into getUserContacts();
			case "addContact":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: addContacts");
				$contacts = array();
				
				if (isset($_REQUEST['bean_module']) && !empty($_REQUEST['bean_module']) && isset($_REQUEST['bean_id']) && !empty($_REQUEST['bean_id'])) {
					$contacts[$_REQUEST['bean_id']] = array(
						'id' => $_REQUEST['bean_id'], 
						'module' => $_REQUEST['bean_module']);
					$email->et->setContacts($contacts);
				}
			
			case "addContactsMultiple":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: addContacts");
				if (isset($_REQUEST['contactData'])) {
					$contacts = $json->decode(from_HTML($_REQUEST['contactData']));
					if ($contacts) {
						//_ppd($contacts);
						$email->et->setContacts($contacts);
					}
				}
			
			case "getUserContacts":
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: getUserContacts");
				$contacts = $email->et->getContacts();
				
				if (is_array($contacts) && !empty($contacts)) {
					$ret = $email->et->getUserContacts($contacts, $current_user);
					$out = $json->encode($ret);
					echo $out;
				} else {
					echo "{}";
				}
				break;
			
			
			// address book search
			case "getUnionData":
				$wheres = array();
				$person;
				if (isset($_REQUEST['first_name']) && !empty($_REQUEST['first_name'])) {
					$wheres['first_name'] = $_REQUEST['first_name'];
				}
				if (isset($_REQUEST['last_name']) && !empty($_REQUEST['last_name'])) {
					$wheres['last_name'] = $_REQUEST['last_name'];
				}
				if (isset($_REQUEST['email_address']) && !empty($_REQUEST['email_address'])) {
					$wheres['email_address'] = $_REQUEST['email_address'];
				}
				if (isset($_REQUEST['person']) && !empty($_REQUEST['person'])) {
					$person = $_REQUEST['person'];
				}
				$q = $email->et->_getPeopleUnionQuery($wheres, $person);
				$r = $ie->db->limitQuery($q, 0, 25, true);
				
				$out = array();
				while ($a = $ie->db->fetchByAssoc($r)) {
					$person = array();
					$person['id'] = $a['id'];
					$person['module'] = $a['module'];
					$person['full_name'] = $locale->getLocaleFormattedName($a['first_name'], $a['last_name'], '');
					$person['email_address'] = $a['email_address'];
					$out[] = $person;
				}
				
				$ret = $json->encode($out, true);
				echo $ret;
				break;
			
			case "getAddressSearchResults":
				$wheres = array();
				$person = 'contacts';
				$relatedBeanInfo = '';
				if (isset($_REQUEST['related_bean_id']) && !empty($_REQUEST['related_bean_id'])) {
					$isRelatedSearch = true;
					$relatedBeanInfo['related_bean_id'] = $_REQUEST['related_bean_id'];
					$relatedBeanInfo['related_bean_type'] = ucfirst($_REQUEST['related_bean_type']);
				}
				
				if (isset($_REQUEST['search_field'])) {
					$wheres['first_name'] = $_REQUEST['search_field'];
					$wheres['last_name'] = $_REQUEST['search_field'];
					$wheres['email_address'] = $_REQUEST['search_field'];
				}
				
				if (isset($_REQUEST['person']) && !empty($_REQUEST['person'])) {
					$person = $_REQUEST['person'];
				}
				if (!empty($_REQUEST['start'])) {
					$start = intval($_REQUEST['start']);
				} else {
					$start = 0;
				}
				
				$qArray = $email->et->getRelatedEmail($person, $wheres, $relatedBeanInfo);
				$out = array();
				$count = 0;
				if (!empty($qArray['query'])) {
					$countq = $qArray['countQuery'];
					$time = microtime(true);
					$r = $ie->db->query($countq);
					$GLOBALS['log']->debug("***QUERY counted in " . (microtime(true) - $time) . " milisec\n");
					if ($row = $GLOBALS['db']->fetchByAssoc($r)) {
						$count = $row['c'];
					}
					$time = microtime(true);
					
					//Handle sort and order requests
					$sort = !empty($_REQUEST['sort']) ? $ie->db->getValidDBName($_REQUEST['sort']) : "id";
					$sort = ($sort == 'bean_id') ? 'id' : $sort;
					$sort = ($sort == 'email') ? 'email_address' : $sort;
					$sort = ($sort == 'name') ? 'last_name' : $sort;
					$direction = !empty($_REQUEST['dir']) && in_array(strtolower($_REQUEST['dir']), array("asc", "desc")) ? $_REQUEST['dir'] : "asc";
					$order = (!empty($sort) && !empty($direction)) ? " ORDER BY {$sort} {$direction}" : "";
					
					$r = $ie->db->limitQuery($qArray['query'] . " $order ", $start, 25, true);
					$GLOBALS['log']->debug("***QUERY Got results in " . (microtime(true) - $time) . " milisec\n");
					
					
					while ($a = $ie->db->fetchByAssoc($r)) {
						$person = array();
						$person['bean_id'] = $a['id'];
						$person['bean_module'] = $a['module'];
						$person['name'] = $locale->getLocaleFormattedName($a['first_name'], $a['last_name'], '');
						$person['email'] = $a['email_address'];
						$out[] = $person;
					}
				}
				$ret = $email->et->jsonOuput(array('out' => $out), 'Person', $count);
				
				@ob_end_clean();
				ob_start();
				echo $ret;
				ob_end_flush();
				break;
			
			////    END ADDRESS BOOK
			///////////////////////////////////////////////////////////////////////////
			
			
			
			///////////////////////////////////////////////////////////////////////////
			////    MISC
			
			default:
				$GLOBALS['log']->debug("********** EMAIL 2.0 - Asynchronous - at: default");
				echo "NOOP";
				break;
		}
		// switch
	}
	// if
	
	
	//<?php
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	
	/**
 * Render the quick compose frame needed by the UI.  The data is returned as a JSOn
 * object and consumed by the client in an ajax call.
 */
	
	require_once ('modules/Emails/EmailUI.php');
	$em = new EmailUI();
	$out = $em->displayQuickComposeEmailFrame();
	
	@ob_end_clean();
	ob_start();
	echo $out;
	ob_end_flush();
	
	//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	
	
	global $current_user;
	
	
	$focus = new Email();
	// Get Group User IDs
	$groupUserQuery = 'SELECT name, group_id FROM inbound_email ie INNER JOIN users u ON (ie.group_id = u.id AND u.is_group = 1)';
	_pp($groupUserQuery);
	$r = $focus->db->query($groupUserQuery);
	$groupIds = '';
	while ($a = $focus->db->fetchByAssoc($r)) {
		$groupIds .= "'" . $a['group_id'] . "', ";
	}
	$groupIds = substr($groupIds, 0, (strlen($groupIds) - 2));
	
	$query = 'SELECT emails.id AS id FROM emails';
	$query .= " WHERE emails.deleted = 0 AND emails.status = 'unread' AND emails.assigned_user_id IN ({$groupIds})";
	//$query .= ' LIMIT 1';
	
	//_ppd($query);
	$r2 = $focus->db->query($query);
	$count = 0;
	$a2 = $focus->db->fetchByAssoc($r2);
	
	$focus->retrieve($a2['id']);
	$focus->assigned_user_id = $current_user->id;
	$focus->save();
	
	if (!empty($a2['id'])) {
		header('Location: index.php?module=Emails&action=ListView&type=inbound&assigned_user_id=' . $current_user->id);
	} else {
		header('Location: index.php?module=Emails&action=ListView&show_error=true&type=inbound&assigned_user_id=' . $current_user->id);
	}
	
?>



//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description:
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc. All Rights
 * Reserved. Contributor(s): ______________________________________..
 *********************************************************************************/
	
	
	$focus = new Email();
	$focus->email2init();
	$focus->et->preflightUser($current_user);
	$out = $focus->et->displayEmailFrame();
	echo $out;
	echo "<script>var composePackage = null;</script>";
	
	$skipFooters = true;
	
	//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
	
	
	
	
	
	
	
	require_once ('modules/MySettings/StoreQuery.php');
	
	global $app_strings;
	global $app_list_strings;
	global $mod_strings;
	global $urlPrefix;
	global $currentModule;
	
	global $theme;
	global $focus_list; // focus_list is the means of passing data to a ListView.
	
	$focus = new Email();
	$header_text = '';
	$where = '';
	$type = '';
	$assigned_user_id = '';
	$group = '';
	$search_adv = '';
	$whereClauses = array();
	$error = '';
	
	///////////////////////////////////////////////////////////////////////////////
	////
	////	SEARCH FORM FUNCTIONALITY
	////	SEARCH QUERY GENERATION
	$storeQuery = new StoreQuery();
	
	if (!isset($_REQUEST['query'])) {
		//_pp('loading: '.$currentModule.'Group');
		//_pp($current_user->user_preferences[$currentModule.'GroupQ']);
		$storeQuery->loadQuery($currentModule . 'Group');
		$storeQuery->populateRequest();
	} else {
		//_pp($current_user->user_preferences[$currentModule.'GroupQ']);
		//_pp('saving: '.$currentModule.'Group');
		$storeQuery->saveFromGet($currentModule . 'Group');
	}
	
	if (isset($_REQUEST['query'])) {
		// we have a query
		if (isset($_REQUEST['email_type'])) 
			$email_type = $_REQUEST['email_type'];
		if (isset($_REQUEST['assigned_to'])) 
			$assigned_to = $_REQUEST['assigned_to'];
		if (isset($_REQUEST['status'])) 
			$status = $_REQUEST['status'];
		if (isset($_REQUEST['name'])) 
			$name = $_REQUEST['name'];
		if (isset($_REQUEST['contact_name'])) 
			$contact_name = $_REQUEST['contact_name'];
		
		if (isset($email_type) && $email_type != "") 
			$whereClauses['emails.type'] = "emails.type = '" . $GLOBALS['db']->quote($email_type) . "'";
		if (isset($assigned_to) && $assigned_to != "") 
			$whereClauses['emails.assigned_user_id'] = "emails.assigned_user_id = '" . $GLOBALS['db']->quote($assigned_to) . "'";
		if (isset($status) && $status != "") 
			$whereClauses['emails.status'] = "emails.status = '" . $GLOBALS['db']->quote($status) . "'";
		if (isset($name) && $name != "") 
			$whereClauses['emails.name'] = "emails.name like '" . $GLOBALS['db']->quote($name) . "%'";
		if (isset($contact_name) && $contact_name != '') {
			$contact_names = explode(" ", $contact_name);
			foreach ($contact_names as $name) {
				$whereClauses['contacts.name'] = "(contacts.first_name like '" . $GLOBALS['db']->quote($name) . "%' OR contacts.last_name like '" . $GLOBALS['db']->quote($name) . "%')";
			}
		}
		
		$focus->custom_fields->setWhereClauses($whereClauses);
		
		$GLOBALS['log']->info("Here is the where clause for the list view: $where");
	}
	// end isset($_REQUEST['query'])
	
	
	
	////	OUTPUT GENERATION
	
	if (!isset($_REQUEST['search_form']) || $_REQUEST['search_form'] != 'false') {
		// ASSIGNMENTS pre-processing
		$email_type_sel = '';
		$assigned_to_sel = '';
		$status_sel = '';
		if (isset($_REQUEST['email_type'])) 
			$email_type_sel = $_REQUEST['email_type'];
		if (isset($_REQUEST['assigned_to'])) 
			$assigned_to_sel = $_REQUEST['assigned_to'];
		if (isset($_REQUEST['status'])) 
			$status_sel = $_REQUEST['status'];
		if (isset($_REQUEST['search'])) 
			$search_adv = $_REQUEST['search'];
		
		// drop-downs values
		$r = $focus->db->query("SELECT id, user_name FROM users WHERE deleted = 0 AND status = 'Active' OR users.is_group = 1 ORDER BY status");
		$users[] = '';
		while ($a = $focus->db->fetchByAssoc($r)) {
			$users[$a['id']] = $a['user_name'];
		}
		
		$email_types[] = '';
		$email_types = array_merge($email_types, $app_list_strings['dom_email_types']);
		$email_status[] = '';
		$email_status = array_merge($email_status, $app_list_strings['dom_email_status']);
		$types = get_select_options_with_id($email_types, $email_type_sel);
		$assigned_to = get_select_options_with_id($users, $assigned_to_sel);
		$email_status = get_select_options_with_id($email_status, $status_sel);
		
		// ASSIGNMENTS AND OUTPUT
		$search_form = new XTemplate('modules/Emails/SearchFormGroupInbox.html');
		$search_form->assign('MOD', $mod_strings);
		$search_form->assign('APP', $app_strings);
		$search_form->assign('ADVANCED_SEARCH_PNG', SugarThemeRegistry::current()->getImage('advanced_search', 'border="0"', null, null, '.gif', $app_strings['LNK_ADVANCED_SEARCH']));
		$search_form->assign('BASIC_SEARCH_PNG', SugarThemeRegistry::current()->getImage('basic_search', 'border="0"', null, null, '.gif', $app_strings['LNK_BASIC_SEARCH']));
		$search_form->assign('TYPE_OPTIONS', $types);
		$search_form->assign('ASSIGNED_TO_OPTIONS', $assigned_to);
		$search_form->assign('STATUS_OPTIONS', $email_status);
		$search_form->assign('ADV_URL', $_SERVER['REQUEST_URI']);
		$search_form->assign('SEARCH_ADV', $search_adv);
		$search_form->assign('SEARCH_ACTION', 'ListViewGroup');
		
		if (isset($_REQUEST['name'])) 
			$search_form->assign('NAME', $_REQUEST['name']);
		if (isset($_REQUEST['contact_name'])) 
			$search_form->assign('CONTACT_NAME', $_REQUEST['contact_name']);
		if (isset($current_user_only)) 
			$search_form->assign('CURRENT_USER_ONLY', "checked");
		
		// adding custom fields:
		$focus->custom_fields->populateXTPL($search_form, 'search');
		
		if (!empty($get['assigned_user_id'])) {
			$search_form->assign('ASSIGNED_USER_ID', $get['assigned_user_id']);
		}
		$search_form->assign('JAVASCRIPT', $focus->u_get_clear_form_js());
	}
	////	END SEARCH FORM FUNCTIONALITY
	////	
	///////////////////////////////////////////////////////////////////////////////
	
	
	
	///////////////////////////////////////////////////////////////////////////////
	////	NAVIGATION HACK
	$_SESSION['emailStartAction'] = ''; // empty this value to allow new writes
	////	END NAVIGATION HACK
	///////////////////////////////////////////////////////////////////////////////
	
	
	
	///////////////////////////////////////////////////////////////////////////////
	////
	////	INBOX FUNCTIONALITY
	// for Inbox
	$r = $focus->db->query('SELECT count(id) AS c FROM users WHERE users.is_group = 1 AND deleted = 0');
	$a = $focus->db->fetchByAssoc($r);
	
	$or = 'emails.assigned_user_id IN (\'abc\''; // must have a dummy entry to force group inboxes to not show personal emails
	if ($a['c'] > 0) {
		
		$r = $focus->db->query('SELECT id FROM users WHERE users.is_group = 1 AND deleted = 0');
		while ($a = $focus->db->fetchByAssoc($r)) {
			$or .= ',\'' . $a['id'] . '\'';
		}
	}
	$or .= ') ';
	$whereClauses['emails.assigned_user_id'] = $or;
	$whereClauses['emails.type'] = 'emails.type = \'inbound\'';
	
	$display_title = $mod_strings['LBL_LIST_TITLE_GROUP_INBOX'];
	////	END INBOX FUNCTIONALITY
	////
	///////////////////////////////////////////////////////////////////////////////
	
	
	
	///////////////////////////////////////////////////////////////////////////////
	////	OUTPUT
	///////////////////////////////////////////////////////////////////////////////
	
	echo getClassicModuleTitle("Emails", array($mod_strings['LBL_MODULE_TITLE'] . $display_title), true);
	// admin-edit
	if (is_admin($current_user) && $_REQUEST['module'] != 'DynamicLayout' && !empty($_SESSION['editinplace'])) {
		$header_text = "&nbsp;<a href='index.php?action=index&module=DynamicLayout&from_action=SearchForm&from_module=" . $_REQUEST['module'] . "'>" . SugarThemeRegistry::current()->getImage("EditLayout", "border='0' align='bottom'", null, null, '.gif', $mod_strings['LBL_EDIT_LAYOUT']) . "</a>";
	}
	// search form
	echo get_form_header($mod_strings['LBL_SEARCH_FORM_TITLE'] . $header_text, "", false);
	// ADVANCED SEARCH
	if (isset($_REQUEST['search']) && $_REQUEST['search'] == 'advanced') {
		$search_form->parse('adv');
		$search_form->out('adv');
		
	} else {
		$search_form->parse('main');
		$search_form->out('main');
	}
	echo $focus->rolloverStyle; // for email previews
	if (!empty($_REQUEST['error'])) {
		$error = $app_list_strings['dom_email_errors'][$_REQUEST['error']];
	}
	//_pp($where);
	if (!empty($assigned_to_sel)) {
		$whereClauses['emails.assigned_user_id'] = 'emails.assigned_user_id = \'' . $assigned_to_sel . '\'';
	}
	
	//_pp($whereClauses);
	
	// CONSTRUCT WHERE STRING FROM WHERECLAUSE ARRAY
	foreach ($whereClauses as $clause) {
		if ($where != '') {
			$where .= ' AND ';
		}
		$where .= $clause;
	}
	//echo $focus->quickCreateJS();
	
	$ListView = new ListView();
	// group distributionforms
	echo $focus->distributionForm($where);
	
	$ListView->shouldProcess = true;
	$ListView->show_mass_update = true;
	$ListView->show_mass_update_form = false;
	$ListView->initNewXTemplate('modules/Emails/ListViewGroupInbox.html', $mod_strings);
	$ListView->xTemplateAssign('ATTACHMENT_HEADER', SugarThemeRegistry::current()->getImage('attachment', "", "", "", '.gif', $mod_strings['LBL_ATTACHMENT']));
	$ListView->xTemplateAssign('ERROR', $error);
	$ListView->xTemplateAssign('CHECK_MAIL', $focus->checkInbox('group'));
	$ListView->setHeaderTitle($display_title . $header_text);
	$ListView->setQuery($where, '', 'date_sent, date_entered DESC', 'EMAIL');
	$ListView->setAdditionalDetails();
	$ListView->processListView($focus, 'main', 'EMAIL');
	
?>



//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
	global $theme;
	global $sugar_config;
	global $current_language;
	$currentMax = $sugar_config['list_max_entries_per_page'];
	$sugar_config['list_max_entries_per_page'] = 10;
	
	
	
	
	
	
	
	$current_mod_strings = return_module_language($current_language, 'Emails');
	$focus = new Email();
	$ListView = new ListView();
	$display_title = $current_mod_strings['LBL_LIST_TITLE_MY_INBOX'] . ': ' . $current_mod_strings['LBL_UNREAD_HOME'];
	$where = 'emails.deleted = 0 AND emails.assigned_user_id = \'' . $current_user->id . '\' AND emails.type = \'inbound\' AND emails.status = \'unread\'';
	$limit = 10;
	///////////////////////////////////////////////////////////////////////////////
	////	OUTPUT
	///////////////////////////////////////////////////////////////////////////////
	echo $focus->rolloverStyle;
	$ListView->initNewXTemplate('modules/Emails/ListViewHome.html', $current_mod_strings);
	$ListView->xTemplateAssign('ATTACHMENT_HEADER', SugarThemeRegistry::current()->getImage('attachment', "", "", "", '.gif', $mod_strings['LBL_ATTACHMENT']));
	$ListView->setHeaderTitle($display_title);
	$ListView->setQuery($where, '', 'date_sent, date_entered DESC', "EMAIL");
	$ListView->setAdditionalDetails();
	$ListView->processListView($focus, 'main', 'EMAIL');
	
	//echo $focus->quickCreateJS();
	
	$sugar_config['list_max_entries_per_page'] = $currentMax;
?>


//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	
	if (!empty($_REQUEST['grabbed'])) {
		
		$focus = new Email();
		
		$emailIds = array();
		// CHECKED ONLY:
		$grabEx = explode('::', $_REQUEST['grabbed']);
		
		foreach ($grabEx as $k => $emailId) {
			if ($emailId != "undefined") {
				$focus->mark_deleted($emailId);
			}
		}
		
		header('Location: index.php?module=Emails&action=ListViewGroup');
	} else {
		global $mod_strings;
		// error
		$error = $mod_strings['LBL_MASS_DELETE_ERROR'];
		header('Location: index.php?module=Emails&action=ListViewGroup&error=' . $error);
	}
	
?>



//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description:
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc. All Rights
 * Reserved. Contributor(s): ______________________________________..
 *********************************************************************************/
	
	global $mod_strings;
	global $current_user;
	
	$default = 'index.php?module=Emails&action=ListView&assigned_user_id=' . $current_user->id;
	
	$e = new Email();
	
	// my inbox
	if (ACLController::checkAccess('Emails', 'edit', true)) {
		$module_menu[] = array('index.php?module=Emails&action=index', $mod_strings['LNK_VIEW_MY_INBOX'], "EmailFolder", "Emails");
	}
	// create email template
	if (ACLController::checkAccess('EmailTemplates', 'edit', true)) 
		$module_menu[] = array("index.php?module=EmailTemplates&action=EditView&return_module=EmailTemplates&return_action=DetailView", $mod_strings['LNK_NEW_EMAIL_TEMPLATE'], "CreateEmails", "Emails");
	// email templates
	if (ACLController::checkAccess('EmailTemplates', 'list', true)) 
		$module_menu[] = array("index.php?module=EmailTemplates&action=index", $mod_strings['LNK_EMAIL_TEMPLATE_LIST'], "EmailFolder", 'Emails');
?>



//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	global $mod_strings;
	global $locale;
	$userName = $mod_strings['LBL_UNKNOWN'];
	
	if (isset($_REQUEST['user'])) {
		
		$user = new User();
		$user->retrieve($_REQUEST['user']);
		$userName = $locale->getLocaleFormattedName($user->first_name, $user->last_name);
	}
	
	
	// NEXT FREE
	if (isset($_REQUEST['next_free']) && $_REQUEST['next_free'] == true) {
		
		$next = new Email();
		$rG = $next->db->query('SELECT count(id) AS c FROM users WHERE deleted = 0 AND users.is_group = 1');
		$aG = $next->db->fetchByAssoc($rG);
		if ($rG['c'] > 0) {
			$rG = $next->db->query('SELECT id FROM users WHERE deleted = 0 AND users.is_group = 1');
			$aG = $next->db->fetchByAssoc($rG);
			while ($aG = $next->db->fetchByAssoc($rG)) {
				$ids[] = $aG['id'];
			}
			$in = ' IN (';
			foreach ($ids as $k => $id) {
				$in .= '"' . $id . '", ';
			}
			$in = substr($in, 0, (strlen($in) - 2));
			$in .= ') ';
			
			$team = '';
			
			$qE = 'SELECT count(id) AS c FROM emails WHERE deleted = 0 AND assigned_user_id' . $in . $team . 'LIMIT 1';
			$rE = $next->db->query($qE);
			$aE = $next->db->fetchByAssoc($rE);
			
			if ($aE['c'] > 0) {
				$qE = 'SELECT id FROM emails WHERE deleted = 0 AND assigned_user_id' . $in . $team . 'LIMIT 1';
				$rE = $next->db->query($qE);
				$aE = $next->db->fetchByAssoc($rE);
				$next->retrieve($aE['id']);
				$next->assigned_user_id = $current_user->id;
				$next->save();
				
				header('Location: index.php?module=Emails&action=DetailView&record=' . $next->id);
				
			} else {
				// no free items
				header('Location: index.php?module=Emails&action=ListView&type=inbound&group=true');
			}
		} else {
			// no groups
			header('Location: index.php?module=Emails&action=ListView&type=inbound&group=true');
		}
	}
?>

<table width="100%" cellpadding="12" cellspacing="0" border="0">
	<tr>
		<td valign="middle" align="center" colspan="2">
			<?php
	echo $mod_strings['LBL_LOCK_FAIL_DESC'];
?>
			<br>
			<?php
	echo $userName . $mod_strings['LBL_LOCK_FAIL_USER'];
?>
		</td>
	</tr>
	<tr>
		<td valign="middle" align="right" width="50%">
			<a href="index.php?module=Emails&action=ListView&type=inbound&group=true"><?php
	echo $mod_strings['LBL_BACK_TO_GROUP'];
?>
</a>
		</td>
		<td valign="middle" align="left">
			<a href="index.php?module=Emails&action=PessimisticLock&next_free=true"><?php
	echo $mod_strings['LBL_NEXT_EMAIL'];
?>
</a>
		</td>
	</tr>
</table>


//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	
	
	
	global $theme;
	
	
	
	
	
	
	
	
	
	class Popup_Picker
	{
		/*
	 * 
	 */
		function Popup_Picker()
		{
		}
		
		/*
	 * 
	 */
		function _get_where_clause()
		{
			$where = '';
			if (isset($_REQUEST['query'])) {
				$where_clauses = array();
				append_where_clause($where_clauses, "name", "emails.name");
				append_where_clause($where_clauses, "contact_name", "contacts.last_name");
				
				$where = generate_where_statement($where_clauses);
			}
			
			return $where;
		}
		
		/**
	 *
	 */
		function process_page()
		{
			global $theme;
			global $mod_strings;
			global $app_strings;
			global $currentModule;
			
			$output_html = '';
			$where = '';
			
			$where = $this->_get_where_clause();
			
			
			
			$name = empty($_REQUEST['name']) ? '' : $_REQUEST['name'];
			$contact_name = empty($_REQUEST['contact_name']) ? '' : $_REQUEST['contact_name'];
			$request_data = empty($_REQUEST['request_data']) ? '' : $_REQUEST['request_data'];
			$hide_clear_button = empty($_REQUEST['hide_clear_button']) ? false : true;
			
			$button = "<form action='index.php' method='post' name='form' id='form'>\n";
			
			if (!$hide_clear_button) {
				$button .= "<input type='button' name='button' class='button' onclick=\"send_back('','');\" title='" . $app_strings['LBL_CLEAR_BUTTON_TITLE'] . "' value='  " . $app_strings['LBL_CLEAR_BUTTON_LABEL'] . "  ' />\n";
			}
			$button .= "<input type='submit' name='button' class='button' onclick=\"window.close();\" title='" . $app_strings['LBL_CANCEL_BUTTON_TITLE'] . "' accesskey='" . $app_strings['LBL_CANCEL_BUTTON_KEY'] . "' value='  " . $app_strings['LBL_CANCEL_BUTTON_LABEL'] . "  ' />\n";
			$button .= "</form>\n";
			
			$form = new XTemplate('modules/Emails/Popup_picker.html');
			$form->assign('MOD', $mod_strings);
			$form->assign('APP', $app_strings);
			$form->assign('THEME', $theme);
			$form->assign('MODULE_NAME', $currentModule);
			$form->assign('NAME', $name);
			$form->assign('CONTACT_NAME', $contact_name);
			$form->assign('request_data', $request_data);
			
			ob_start();
			insert_popup_header($theme);
			$output_html .= ob_get_contents();
			ob_end_clean();
			
			$output_html .= get_form_header($mod_strings['LBL_SEARCH_FORM_TITLE'], '', false);
			
			$form->parse('main.SearchHeader');
			$output_html .= $form->text('main.SearchHeader');
			
			// Reset the sections that are already in the page so that they do not print again later.
			$form->reset('main.SearchHeader');
			
			// create the listview
			$seed_bean = new Email();
			$ListView = new ListView();
			$ListView->show_export_button = false;
			$ListView->process_for_popups = true;
			$ListView->setXTemplate($form);
			$ListView->setHeaderTitle($mod_strings['LBL_LIST_FORM_TITLE']);
			$ListView->setHeaderText($button);
			$ListView->setQuery($where, '', 'name', 'EMAIL');
			$ListView->setModStrings($mod_strings);
			
			ob_start();
			$ListView->processListView($seed_bean, 'main', 'EMAIL');
			$output_html .= ob_get_contents();
			ob_end_clean();
			
			$output_html .= insert_popup_footer();
			return $output_html;
		}
	}
	// end of class Popup_Picker
?>


//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	
	if (isset($_REQUEST['mode']) && $_REQUEST['mode'] == 'show_raw') {
		$email = new Email();
		$email->retrieve($_REQUEST['metadata']);
		echo nl2br($email->raw_source);
	} else {
		require_once ('include/Popups/Popup_picker.php');
		$popup = new Popup_Picker();
		echo $popup->process_page();
	}
	
?>



//<?php
	//_pp($_REQUEST);
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
	require_once ('modules/Documents/Popup_picker.php');
	$popup = new Popup_Picker();
	
	global $theme;
	global $current_mod_strings;
	global $app_strings;
	global $currentModule;
	global $sugar_version, $sugar_config;
	global $app_list_strings;
	
	$current_mod_strings = return_module_language($current_language, 'Documents');
	$output_html = '';
	$where = '';
	
	$where = $popup->_get_where_clause();
	
	// We can't attach remote documents to emails because we can't necessarialy fetch a copy of them to include.
	if (!empty($where)) {
		$where .= ' AND ';
	}
	$where .= "documents.doc_type IN ( '', 'Sugar')";
	
	$name = empty($_REQUEST['name']) ? '' : $_REQUEST['name'];
	$document_name = empty($_REQUEST['document_name']) ? '' : $_REQUEST['document_name'];
	$category_id = empty($_REQUEST['category_id']) ? '' : $_REQUEST['category_id'];
	$subcategory_id = empty($_REQUEST['subcategory_id']) ? '' : $_REQUEST['subcategory_id'];
	$template_type = empty($_REQUEST['template_type']) ? '' : $_REQUEST['template_type'];
	$is_template = empty($_REQUEST['is_template']) ? '' : $_REQUEST['is_template'];
	$document_revision_id = empty($_REQUEST['document_revision_id']) ? '' : $_REQUEST['document_revision_id'];
	
	//$request_data = empty($_REQUEST['request_data']) ? '' : $_REQUEST['request_data'];
	
	$hide_clear_button = empty($_REQUEST['hide_clear_button']) ? false : true;
	$button = "<form action='index.php' method='post' name='form' id='form'>\n";
	if (!$hide_clear_button) {
		$button .= "<input type='button' name='button' class='button' onclick=\"send_back('','');\" title='" . $app_strings['LBL_CLEAR_BUTTON_TITLE'] . "' value='  " . $app_strings['LBL_CLEAR_BUTTON_LABEL'] . "  ' />\n";
	}
	$button .= "<input type='submit' name='button' class='button' onclick=\"window.close();\" title='" . $app_strings['LBL_CANCEL_BUTTON_TITLE'] . "' accesskey='" . $app_strings['LBL_CANCEL_BUTTON_KEY'] . "' value='  " . $app_strings['LBL_CANCEL_BUTTON_LABEL'] . "  ' />\n";
	$button .= "</form>\n";
	
	$form = new XTemplate('modules/Emails/PopupDocuments.html');
	$form->assign('MOD', $current_mod_strings);
	$form->assign('APP', $app_strings);
	$form->assign('THEME', $theme);
	$form->assign('MODULE_NAME', $currentModule);
	$form->assign('NAME', $name);
	$form->assign('DOCUMENT_NAME', $document_name);
	$form->assign('DOCUMENT_TARGET', $_REQUEST['target']);
	$form->assign('DOCUMENT_REVISION_ID', $document_revision_id);
	
	//$form->assign('request_data', $request_data);
	$form->assign("CATEGORY_OPTIONS", get_select_options_with_id($app_list_strings['document_category_dom'], $category_id));
	$form->assign("SUB_CATEGORY_OPTIONS", get_select_options_with_id($app_list_strings['document_subcategory_dom'], $subcategory_id));
	$form->assign("IS_TEMPLATE_OPTIONS", get_select_options_with_id($app_list_strings['checkbox_dom'], $is_template));
	$form->assign("TEMPLATE_TYPE_OPTIONS", get_select_options_with_id($app_list_strings['document_template_type_dom'], $template_type));
	
	
	
	ob_start();
	insert_popup_header($theme);
	$output_html .= ob_get_contents();
	ob_end_clean();
	
	$output_html .= get_form_header($current_mod_strings['LBL_SEARCH_FORM_TITLE'], '', false);
	
	$form->parse('main.SearchHeader');
	$output_html .= $form->text('main.SearchHeader');
	
	// Reset the sections that are already in the page so that they do not print again later.
	$form->reset('main.SearchHeader');
	
	// create the listview
	$seed_bean = new Document();
	$ListView = new ListView();
	$ListView->show_export_button = false;
	$ListView->process_for_popups = true;
	$ListView->setXTemplate($form);
	$ListView->setHeaderTitle($current_mod_strings['LBL_LIST_FORM_TITLE']);
	$ListView->setHeaderText($button);
	$ListView->setQuery($where, '', 'document_name', 'DOCUMENT');
	$ListView->setModStrings($current_mod_strings);
	
	ob_start();
	$ListView->processListView($seed_bean, 'main', 'DOCUMENT');
	$output_html .= ob_get_contents();
	ob_end_clean();
	
	$output_html .= insert_popup_footer();
	
	
	echo $output_html;
	
	
	
	
	
	
	
	
	
	
?>


//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	
	///////////////////////////////////////////////////////////////////////////////
	////	EMAIL SEND/SAVE SETUP
	$focus = new Email();
	
	if (!isset($prefix)) {
		$prefix = '';
	}
	if (isset($_POST[$prefix . 'meridiem']) && !empty($_POST[$prefix . 'meridiem'])) {
		$_POST[$prefix . 'time_start'] = $timedate->merge_time_meridiem($_POST[$prefix . 'time_start'], $timedate->get_time_format(), $_POST[$prefix . 'meridiem']);
	}
	//retrieve the record
	if (isset($_POST['record']) && !empty($_POST['record'])) {
		$focus->retrieve($_POST['record']);
		
	}
	if (isset($_REQUEST['user_id'])) {
		$focus->assigned_user_id = $_REQUEST['user_id'];
	}
	if (!$focus->ACLAccess('Save')) {
		ACLController::displayNoAccess(true);
		sugar_cleanup(true);
	}
	if (!empty($_POST['assigned_user_id']) && ($focus->assigned_user_id != $_POST['assigned_user_id']) && ($_POST['assigned_user_id'] != $current_user->id)) {
		$check_notify = TRUE;
	}
	//populate the fields of this Email
	$allfields = array_merge($focus->column_fields, $focus->additional_column_fields);
	foreach ($allfields as $field) {
		if (isset($_POST[$field])) {
			$value = $_POST[$field];
			$focus->$field = $value;
		}
	}
	
	$focus->description = $_REQUEST['description_html'];
	$focus->description_html = $_REQUEST['description_html'];
	
	if (!isset($_REQUEST['to_addrs'])) {
		$_REQUEST['to_addrs'] = "";
	}
	if (!isset($_REQUEST['to_addrs_ids'])) {
		$_REQUEST['to_addrs_ids'] = "";
	}
	if (!isset($_REQUEST['to_addrs_names'])) {
		$_REQUEST['to_addrs_names'] = "";
	}
	if (!isset($_REQUEST['to_addrs_emails'])) {
		$_REQUEST['to_addrs_emails'] = "";
	}
	
	//compare the 3 fields and return list of contact_ids to link:
	$focus->to_addrs_arr = $focus->parse_addrs($_REQUEST['to_addrs'], $_REQUEST['to_addrs_ids'], $_REQUEST['to_addrs_names'], $_REQUEST['to_addrs_emails']);
	
	// make sure the cc_* and bcc_* fields are at least empty if not set
	$fields_to_check = array(
		'cc_addrs', 
		'cc_addrs_ids', 
		'bcc_addrs', 
		'bcc_addrs_ids', 
		'cc_addrs_names', 
		'cc_addrs_emails', 
		'bcc_addrs_emails');
	foreach ($fields_to_check as $field_to_check) {
		if (!isset($_REQUEST[$field_to_check])) {
			$_REQUEST[$field_to_check] = '';
		}
	}
	
	$focus->cc_addrs_arr = $focus->parse_addrs($_REQUEST['cc_addrs'], $_REQUEST['cc_addrs_ids'], $_REQUEST['cc_addrs_names'], $_REQUEST['cc_addrs_emails']);
	$focus->bcc_addrs_arr = $focus->parse_addrs($_REQUEST['bcc_addrs'], $_REQUEST['bcc_addrs_ids'], $_REQUEST['to_addrs_names'], $_REQUEST['bcc_addrs_emails']);
	
	
	if (!empty($_REQUEST['type'])) {
		$focus->type = $_REQUEST['type'];
	} elseif (empty($focus->type)) {
		// cn: from drafts/quotes
		$focus->type = 'archived';
	}
	
	///////////////////////////////////////////////////////////////////////////////
	////	TEMPLATE PARSING
	// cn: bug 7244 - need to pass an empty bean to parse email templates
	$object_arr = array();
	if (!empty($focus->parent_id)) {
		$object_arr[$focus->parent_type] = $focus->parent_id;
	}
	if (isset($focus->to_addrs_arr[0]['contact_id'])) {
		$object_arr['Contacts'] = $focus->to_addrs_arr[0]['contact_id'];
	}
	if (empty($object_arr)) {
		$object_arr = array('Contacts' => '123');
	}
	
	// do not parse email templates if the email is being saved as draft....
	if ($focus->type != 'draft' && count($object_arr) > 0) {
		require_once ($beanFiles['EmailTemplate']);
		$focus->name = EmailTemplate::parse_template($focus->name, $object_arr);
		$focus->description = EmailTemplate::parse_template($focus->description, $object_arr);
		$focus->description_html = EmailTemplate::parse_template($focus->description_html, $object_arr);
	}
	////	END TEMPLATE PARSING
	///////////////////////////////////////////////////////////////////////////////
	
	///////////////////////////////////////////////////////////////////////////////
	////	PREP FOR ATTACHMENTS
	if (empty($focus->id)) {
		$focus->id = create_guid();
		$focus->new_with_id = true;
	}
	///////////////////////////////////////////////////////////////////////////////
	
	///////////////////////////////////////////////////////////////////////////////
	////	ATTACHMENT HANDLING
	$focus->handleAttachments();
	////	END ATTACHMENT HANDLING
	///////////////////////////////////////////////////////////////////////////////
	$focus->status = 'draft';
	if ($focus->type == 'archived') {
		$focus->status = 'archived';
		$focus->date_start = $_REQUEST['date_start'];
		$focus->time_start = $_REQUEST['time_start'] . $_REQUEST['meridiem'];
	} elseif (($focus->type == 'out' || $focus->type == 'forward') && isset($_REQUEST['send']) && $_REQUEST['send'] == '1') {
		///////////////////////////////////////////////////////////////////////////
		////	REPLY PROCESSING
		$old = array('&lt;', '&gt;');
		$new = array('<', '>');
		
		if ($_REQUEST['from_addr'] != $_REQUEST['from_addr_name'] . ' &lt;' . $_REQUEST['from_addr_email'] . '&gt;') {
			if (false === strpos($_REQUEST['from_addr'], '&lt;')) {
				// we have an email only?
				$focus->from_addr = $_REQUEST['from_addr'];
				$focus->from_name = '';
			} else {
				// we have a compound string
				$newFromAddr = str_replace($old, $new, $_REQUEST['from_addr']);
				$focus->from_addr = substr($newFromAddr, (1 + strpos($newFromAddr, '<')), (strpos($newFromAddr, '>') - strpos($newFromAddr, '<')) - 1);
				$focus->from_name = substr($newFromAddr, 0, (strpos($newFromAddr, '<') - 1));
			}
		} elseif (!empty($_REQUEST['from_addr_email']) && isset($_REQUEST['from_addr_email'])) {
			$focus->from_addr = $_REQUEST['from_addr_email'];
			$focus->from_name = $_REQUEST['from_addr_name'];
		} else {
			$focus->from_addr = $focus->getSystemDefaultEmail();
		}
		////	REPLY PROCESSING
		///////////////////////////////////////////////////////////////////////////
		if ($focus->send()) {
			$focus->status = 'sent';
		} else {
			$focus->status = 'send_error';
		}
	}
	$focus->to_addrs = $_REQUEST['to_addrs'];
	$focus->cc_addrs = $_REQUEST['cc_addrs'];
	$focus->bcc_addrs = $_REQUEST['bcc_addrs'];
	$focus->from_addr = $_REQUEST['from_addr'];
	
	// delete the existing relationship of all the email addresses with this email
	$query = "update emails_email_addr_rel set deleted = 1 WHERE email_id = '{$focus->id}'";
	$focus->db->query($query);
	
	// delete al the relationship of this email with all the beans
	//$query = "update emails_beans set deleted = 1, bean_id = '', bean_module = '' WHERE email_id = '{$focus->id}'";
	//$focus->db->query($query);
	if (!empty($_REQUEST['to_addrs_ids'])) {
		$exContactIds = explode(';', $_REQUEST['to_addrs_ids']);
	} else {
		$exContactIds = array();
	}
	
	if (isset($_REQUEST['object_type']) && !empty($_REQUEST['object_type']) && isset($_REQUEST['object_id']) && !empty($_REQUEST['object_id'])) {
		//run linking code only if the object_id has not been linked as part of the contacts above and it is an OOB relationship
		$GLOBALS['log']->debug("CESELY" . $_REQUEST['object_type']);
		if (!in_array($_REQUEST['object_id'], $exContactIds)) {
			$rel = strtolower($_REQUEST['object_type']);
			if ($focus->load_relationship($rel)) {
				$focus->$rel->add($_REQUEST['object_id']);
				$GLOBALS['log']->debug("CESELY LOADED" . $_REQUEST['object_type']);
			}
		}
	}
	////    END RELATIONSHIP LINKING
	///////////////////////////////////////////////////////////////////////////////
	
	// If came from email archiving edit view, this would have been set from form input.
	if (!isset($focus->date_start)) {
		$timedate = TimeDate::getInstance();
		list($focus->date_start, $focus->time_start) = $timedate->split_date_time($timedate->now());
	}
	
	$focus->date_sent = "";
	
	require_once ('include/formbase.php');
	$focus = populateFromPost('', $focus);
	
	//CCL - Bug: 40168 Fix ability to change date sent from saved emails
	if ($focus->type == 'archived') {
		$focus->date_start = $_REQUEST['date_start'];
		$focus->time_start = $_REQUEST['time_start'] . $_REQUEST['meridiem'];
		$focus->date_sent = '';
	}
	
	$focus->save(false);
	////	END EMAIL SAVE/SEND SETUP
	///////////////////////////////////////////////////////////////////////////////
	
	///////////////////////////////////////////////////////////////////////////////
	////	RELATIONSHIP LINKING
	$focus->load_relationship('users');
	$focus->users->add($current_user->id);
	
	if (!empty($exContactIds)) {
		$focus->load_relationship('contacts');
		foreach ($exContactIds as $contactId) {
			$contactId = trim($contactId);
			$focus->contacts->add($contactId);
		}
	}
	
	///////////////////////////////////////////////////////////////////////////////
	////	PAGE REDIRECTION
	///////////////////////////////////////////////////////////////////////////////
	$return_id = $focus->id;
	
	if (empty($_POST['return_module'])) {
		$return_module = "Emails";
	} else {
		$return_module = $_POST['return_module'];
	}
	if (empty($_POST['return_action'])) {
		$return_action = "DetailView";
	} else {
		$return_action = $_POST['return_action'];
	}
	$GLOBALS['log']->debug("Saved record with id of " . $return_id);
	
	if ($focus->type == 'draft') {
		if ($return_module == 'Emails') {
			header("Location: index.php?module=$return_module&action=ListViewDrafts");
		} else {
			handleRedirect($return_id, 'Emails');
		}
	} elseif ($focus->type == 'out') {
		if ($return_module == 'Home') {
			header('Location: index.php?module=' . $return_module . '&action=index');
		}
		if (!empty($_REQUEST['return_id'])) {
			$return_id = $_REQUEST['return_id'];
		}
		header('Location: index.php?action=' . $return_action . '&module=' . $return_module . '&record=' . $return_id . '&assigned_user_id=' . $current_user->id . '&type=inbound');
	} elseif (isset($_POST['return_id']) && $_POST['return_id'] != "") {
		$return_id = $_POST['return_id'];
	}
	header("Location: index.php?action=$return_action&module=$return_module&record=$return_id");
?>



//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
	
	
	global $mod_strings;
	global $app_strings;
	
	$focus = new Email();
	
	if (!empty($_REQUEST['record'])) {
		$result = $focus->retrieve($_REQUEST['record']);
		if ($result == null) {
			sugar_die($app_strings['ERROR_NO_RECORD']);
		}
	} else {
		header("Location: index.php?module=Emails&action=index");
	}
	
	//needed when creating a new email with default values passed in
	if (isset($_REQUEST['contact_name']) && is_null($focus->contact_name)) {
		$focus->contact_name = $_REQUEST['contact_name'];
	}
	if (isset($_REQUEST['contact_id']) && is_null($focus->contact_id)) {
		$focus->contact_id = $_REQUEST['contact_id'];
	}
	echo getClassicModuleTitle($mod_strings['LBL_SEND'], array($mod_strings['LBL_SEND']), true);
	
	$GLOBALS['log']->info("Email detail view");
	
	$xtpl = new XTemplate('modules/Emails/Status.html');
	$xtpl->assign("MOD", $mod_strings);
	$xtpl->assign("APP", $app_strings);
	
	$xtpl->assign("GRIDLINE", $gridline);
	$xtpl->assign("PRINT_URL", "index.php?" . $GLOBALS['request_string']);
	$xtpl->assign("ID", $focus->id);
	$xtpl->assign("PARENT_NAME", $focus->parent_name);
	if (isset($focus->parent_type)) {
		$xtpl->assign("PARENT_MODULE", $focus->parent_type);
		$xtpl->assign("PARENT_TYPE", $app_list_strings['record_type_display'][$focus->parent_type]);
	}
	$xtpl->assign("PARENT_ID", $focus->parent_id);
	$xtpl->assign("NAME", $focus->name);
	//$xtpl->assign("SENT_BY_USER_NAME", $focus->sent_by_user_name);
	$xtpl->assign("DATE_SENT", $focus->date_start . " " . $focus->time_start);
	if ($focus->status == 'sent') {
		$xtpl->assign("STATUS", $mod_strings['LBL_MESSAGE_SENT']);
	} else {
		$xtpl->assign("STATUS", "<font color=red>" . $mod_strings['LBL_ERROR_SENDING_EMAIL'] . "</font>");
	}
	
	global $current_user;
	if (is_admin($current_user) && $_REQUEST['module'] != 'DynamicLayout' && !empty($_SESSION['editinplace'])) {
		
		$xtpl->assign("ADMIN_EDIT", "<a href='index.php?action=index&module=DynamicLayout&from_action=" . $_REQUEST['action'] . "&from_module=" . $_REQUEST['module'] . "&record=" . $_REQUEST['record'] . "'>" . SugarThemeRegistry::current()->getImage("EditLayout", "border='0' align='bottom'", null, null, '.gif', $mod_strings['LBL_EDIT_LAYOUT']) . "</a>");
	}
	
	// adding custom fields:
	require_once ('modules/DynamicFields/templates/Files/DetailView.php');
	
	$xtpl->parse("main");
	$xtpl->out("main");
	
?>



//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
	
	
	
	
	global $currentModule;
	
	global $mod_strings;
	global $app_strings;
	
	global $theme;
	global $focus;
	global $action;
	
	// focus_list is the means of passing data to a SubPanelView.
	global $focus_list;
	
	$button = "<table cellspacing='0' cellpadding='1' border='0'><form border='0' action='index.php' method='post' name='form' id='form'>\n";
	$button .= "<input type='hidden' name='module' value='Contacts'>\n";
	if ($currentModule == 'Accounts') 
		$button .= "<input type='hidden' name='account_id' value='$focus->id'>\n<input type='hidden' name='account_name' value='$focus->name'>\n";
	$button .= "<input type='hidden' name='return_module' value='" . $currentModule . "'>\n";
	$button .= "<input type='hidden' name='return_action' value='" . $action . "'>\n";
	$button .= "<input type='hidden' name='return_id' value='" . $focus->id . "'>\n";
	$button .= "<input type='hidden' name='action'>\n";
	$button .= "<tr><td>&nbsp;</td>";
	if ($focus->parent_type == "Accounts") 
		$button .= "<td><input title='" . $app_strings['LBL_SELECT_CONTACT_BUTTON_TITLE'] . "' type='button' class='button' value='" . $app_strings['LBL_SELECT_CONTACT_BUTTON_LABEL'] . "' name='button' LANGUAGE=javascript onclick='window.open(\"index.php?module=Contacts&action=Popup&html=Popup_picker&form=DetailView&form_submit=true&query=true&account_id=$focus->parent_id&account_name=" . urlencode($focus->parent_name) . "\",\"new\",\"width=600,height=400,resizable=1,scrollbars=1\");'></td>\n";
	 else 
		$button .= "<td><input title='" . $app_strings['LBL_SELECT_CONTACT_BUTTON_TITLE'] . "' type='button' class='button' value='" . $app_strings['LBL_SELECT_CONTACT_BUTTON_LABEL'] . "' name='button' LANGUAGE=javascript onclick='window.open(\"index.php?module=Contacts&action=Popup&html=Popup_picker&form=DetailView&form_submit=true\",\"new\",\"width=600,height=400,resizable=1,scrollbars=1\");'></td>\n";
	$button .= "<td><input title='" . $app_strings['LBL_SELECT_USER_BUTTON_TITLE'] . "' type='button' class='button' value='" . $app_strings['LBL_SELECT_USER_BUTTON_LABEL'] . "' name='button' LANGUAGE=javascript onclick='window.open(\"index.php?module=Users&action=Popup&html=Popup_picker&form=DetailView&form_submit=true\",\"new\",\"width=600,height=400,resizable=1,scrollbars=1\");'></td>\n";
	$button .= "</tr></form></table>\n";
	
	// Stick the form header out there.
	echo get_form_header($mod_strings['LBL_INVITEE'], $button, false);
	$xtpl = new XTemplate('modules/Emails/SubPanelViewRecipients.html');
	$xtpl->assign("MOD", $mod_strings);
	$xtpl->assign("APP", $app_strings);
	$xtpl->assign("RETURN_URL", "&return_module=$currentModule&return_action=DetailView&return_id=$focus->id");
	$xtpl->assign("EMAIL_ID", $focus->id);
	$xtpl->assign("DELETE_INLINE_PNG", SugarThemeRegistry::current()->getImage('delete_inline', 'align="absmiddle" border="0"', null, null, '.gif', $app_strings['LNK_REMOVE']));
	$xtpl->assign("EDIT_INLINE_PNG", SugarThemeRegistry::current()->getImage('edit_inline', 'align="absmiddle" border="0"', null, null, '.gif', $app_strings['LNK_EDIT']));
	$oddRow = true;
	foreach ($focus_users_list as $user) {
		$user_fields = array(
			'USER_NAME' => $user->user_name, 
			'FULL_NAME' => $user->first_name . " " . $user->last_name, 
			'ID' => $user->id, 
			'EMAIL' => $user->email1, 
			'PHONE_WORK' => $user->phone_work);
		
		$xtpl->assign("USER", $user_fields);
		
		if ($oddRow) {
			//todo move to themes
			$xtpl->assign("ROW_COLOR", 'oddListRow');
		} else {
			//todo move to themes
			$xtpl->assign("ROW_COLOR", 'evenListRow');
		}
		$oddRow = !$oddRow;
		
		$xtpl->parse("users.row");
		// Put the rows in.
	}
	
	$xtpl->parse("users");
	$xtpl->out("users");
	
	$oddRow = true;
	print count($focus_contacts_list);
	
	foreach ($focus_contacts_list as $contact) {
		$contact_fields = array(
			'FIRST_NAME' => $contact->first_name, 
			'LAST_NAME' => $contact->last_name, 
			'ACCOUNT_NAME' => $contact->account_name, 
			'ID' => $contact->id, 
			'EMAIL' => $contact->email1, 
			'PHONE_WORK' => $contact->phone_work);
		
		$xtpl->assign("CONTACT", $contact_fields);
		
		if ($oddRow) {
			//todo move to themes
			$xtpl->assign("ROW_COLOR", 'oddListRow');
		} else {
			//todo move to themes
			$xtpl->assign("ROW_COLOR", 'evenListRow');
		}
		$oddRow = !$oddRow;
		
		$xtpl->parse("contacts.row");
		// Put the rows in.
	}
	
	$xtpl->parse("contacts");
	$xtpl->out("contacts");
	
?>



//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description:
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc. All Rights
 * Reserved. Contributor(s): ______________________________________..
 *********************************************************************************/
	require_once ("include/SugarRouting/SugarRouting.php");
	
	$ie = new InboundEmail();
	$json = getJSONobj();
	$rules = new SugarRouting($ie, $current_user);
	
	switch ($_REQUEST['routingAction']) {
		case "setRuleStatus":
			$rules->setRuleStatus($_REQUEST['rule_id'], $_REQUEST['status']);
			break;
		
		case "saveRule":
			$rules->save($_REQUEST);
			break;
		
		case "deleteRule":
			$rules->deleteRule($_REQUEST['rule_id']);
			break;
		
		/* returns metadata to construct actions */
		case "getActions":
			require_once ("include/SugarDependentDropdown/SugarDependentDropdown.php");
			
			$sdd = new SugarDependentDropdown();
			$sdd->init("include/SugarDependentDropdown/metadata/dependentDropdown.php");
			$out = $json->encode($sdd->metadata, true);
			echo $out;
			break;
		
		/* returns metadata to construct a rule */
		case "getRule":
			$ret = '';
			if (isset($_REQUEST['rule_id']) && !empty($_REQUEST['rule_id']) && isset($_REQUEST['bean']) && !empty($_REQUEST['bean'])) {
				if (!isset($beanList)) 
					include ("include/modules.php");
				
				$class = $beanList[$_REQUEST['bean']];
				//$beanList['Groups'] = 'Group';
				if (isset($beanList[$_REQUEST['bean']])) {
					require_once ("modules/{$_REQUEST['bean']}/{$class}.php");
					$bean = new $class();
					
					$rule = $rules->getRule($_REQUEST['rule_id'], $bean);
					
					$ret = array(
						'bean' => $_REQUEST['bean'], 
						'rule' => $rule);
				}
			} else {
				$bean = new SugarBean();
				$rule = $rules->getRule('', $bean);
				
				$ret = array(
					'bean' => $_REQUEST['bean'], 
					'rule' => $rule);
			}
			
			//_ppd($ret);
			
			$out = $json->encode($ret, true);
			echo $out;
			break;
		
		case "getStrings":
			$ret = $rules->getStrings();
			$out = $json->encode($ret, true);
			echo $out;
			break;
		
		
		default:
			echo "NOOP";
	}
	
	//<?php
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description:
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc. All Rights
 * Reserved. Contributor(s): ______________________________________..
 *********************************************************************************/
	$view_config = array(
		'actions' => array(
		'index' => array(
		'show_header' => true, 
		'show_subpanels' => false, 
		'show_search' => false, 
		'show_footer' => true, 
		'show_javascript' => true), 
		'Compose' => array(
		'show_header' => true, 
		'show_subpanels' => false, 
		'show_search' => false, 
		'show_footer' => true, 
		'show_javascript' => true)));
	
	
	//<?php
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	require_once ('include/MVC/View/views/view.modulelistmenu.php');
	
	class EmailsViewModulelistmenu extends ViewModulelistmenu
	{
		public function display()
		{
			$tracker = new Tracker();
			$history = $tracker->get_recently_viewed($GLOBALS['current_user']->id, array('Emails', 'EmailTemplates'));
			foreach ($history as $key => $row) {
				$history[$key]['item_summary_short'] = getTrackerSubstring($row['item_summary']);
				$history[$key]['image'] = SugarThemeRegistry::current()->getImage($row['module_name'], 'border="0" align="absmiddle"', null, null, '.gif', $row['item_summary']);
				
			}
			$this->ss->assign('LAST_VIEWED', $history);
			
			$this->ss->display('include/MVC/View/tpls/modulelistmenu.tpl');
		}
	}
?>



//<?php
	//FILE SUGARCRM flav=pro || flav=sales
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	require_once ('include/MVC/View/views/view.quickcreate.php');
	require_once ('modules/Emails/EmailUI.php');
	
	class EmailsViewQuickcreate extends ViewQuickcreate
	{
		/**
     * @see ViewQuickcreate::display()
     */
		public function display()
		{
			$userPref = $GLOBALS['current_user']->getPreference('email_link_type');
			$defaultPref = $GLOBALS['sugar_config']['email_default_client'];
			if ($userPref != '') 
				$client = $userPref;
			 else 
				$client = $defaultPref;
			
			if ($client == 'sugar') {
				$eUi = new EmailUI();

				if (!empty($this->bean->id) && !in_array($this->bean->object_name, array('EmailMan'))) {
					//TODO (MT EXTEND Code): retrieve and use the TENANT_ID
					$fullComposeUrl = "index.php?module=Emails&action=Compose&parent_id={$this->bean->id}&parent_type={$this->bean->module_dir}";
					$composeData = array('parent_id' => $this->bean->id, 'parent_type' => $this->bean->module_dir);
				} else {
					$fullComposeUrl = "index.php?module=Emails&action=Compose";
					$composeData = array('parent_id' => '', 'parent_type' => '');
				}
				
				$j_quickComposeOptions = $eUi->generateComposePackageForQuickCreate($composeData, $fullComposeUrl);
				$json_obj = getJSONobj();
				$opts = $json_obj->decode($j_quickComposeOptions);
				$opts['menu_id'] = 'dccontent';
				
				$ss = new Sugar_Smarty();
				$ss->assign('json_output', $json_obj->encode($opts));
				$ss->display('modules/Emails/templates/dceMenuQuickCreate.tpl');
			} else {
				$emailAddress = '';
				if (!empty($this->bean->id) && !in_array($this->bean->object_name, array('EmailMan')) 
					&& !is_null($this->bean->emailAddress)) {
					$emailAddress = $this->bean->emailAddress->getPrimaryAddress($this->bean);
				}
				echo "<script>document.location.href='mailto:$emailAddress';lastLoadedMenu=undefined;DCMenu.closeOverlay();</script>";
				die();
			}
		}
	}
	
	//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	//Request object must have these property values:
	//		Module: module name, this module should have a file called TreeData.php
	//		Function: name of the function to be called in TreeData.php, the function will be called statically.
	//		PARAM prefixed properties: array of these property/values will be passed to the function as parameter.
	
	
	require_once ('include/JSON.php');
	require_once ('include/upload_file.php');
	
	if (!is_dir($cachedir = sugar_cached('images/'))) 
		mkdir_recursive($cachedir);
	
	// cn: bug 11012 - fixed some MIME types not getting picked up.  Also changed array iterator.
	$imgType = array('image/gif', 'image/png', 'image/x-png', 'image/bmp', 'image/jpeg', 'image/jpg', 'image/pjpeg');
	
	$ret = array();
	
	foreach ($_FILES as $k => $file) {
		if (in_array(strtolower($_FILES[$k]['type']), $imgType) && $_FILES[$k]['size'] > 0) {
			$upload_file = new UploadFile($k);
			// check the file
			
			
			if ($upload_file->confirm_upload()) {
				$dest = $cachedir . basename($upload_file->get_stored_file_name()); // target name
				$guid = create_guid();
				if ($upload_file->final_move($guid)) {
					// move to uploads
					$path = $upload_file->get_upload_path($guid);
					// if file is OK, copy to cache
					if (verify_uploaded_image($path) && copy($path, $dest)) {
						$ret[] = $dest;
					}
					// remove temp file
					unlink($path);
				}
			}
		}
	}
	
	if (!empty($ret)) {
		$json = getJSONobj();
		echo $json->encode($ret);
		//return the parameters
	}
	
	//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
	
	require_once ('modules/EmailTemplates/EmailTemplate.php');
	
	$focus = new EmailTemplate();
	if ($_REQUEST['from'] == 'DetailView') {
		if (!isset($_REQUEST['record'])) 
			sugar_die("A record number must be specified to delete the template.");
		$focus->retrieve($_REQUEST['record']);
		if (check_email_template_in_use($focus)) {
			echo 'true';
			return;
		}
		echo 'false';
	} else 
		if ($_REQUEST['from'] == 'ListView') {
			$returnString = '';
			$idArray = explode(',', $_REQUEST['records']);
			foreach ($idArray as $key => $value) {
				if ($focus->retrieve($value)) {
					if (check_email_template_in_use($focus)) {
						$returnString .= $focus->name . ',';
					}
				}
			}
			$returnString = substr($returnString, 0, -1);
			echo $returnString;
		} else {
			echo '';
		}
	
	function check_email_template_in_use($focus)
	{
		if ($focus->is_used_by_email_marketing()) {
			return true;
		}
		$system = $GLOBALS['sugar_config']['passwordsetting'];
		if ($focus->id == $system['generatepasswordtmpl'] || $focus->id == $system['lostpasswordtmpl']) {
			return true;
		}
		return false;
	}
	
	
	//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
	
	
	
	
	
	$focus = new EmailTemplate();
	
	if (!isset($_REQUEST['record'])) 
		sugar_die("A record number must be specified to delete the template.");
	$focus->retrieve($_REQUEST['record']);
	if (!$focus->ACLAccess('Delete')) {
		ACLController::displayNoAccess(true);
		sugar_cleanup(true);
	}
	sugar_cache_clear('select_array:' . $focus->object_name . 'namebase_module=\'' . $focus->base_module . '\'name');
	$focus->mark_deleted($_REQUEST['record']);
	
	header("Location: index.php?module=" . $_REQUEST['return_module'] . "&action=" . $_REQUEST['return_action'] . "&record=" . $_REQUEST['return_id']);
?>



//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
	
	
	require_once ('include/upload_file.php');
	require_once ('include/DetailView/DetailView.php');
	
	//Old DetailView compares wrong session variable against new view.list.  Need to sync so that
	//the pagination on the DetailView page will show.
	if (isset($_SESSION['EMAILTEMPLATE_FROM_LIST_VIEW'])) 
		$_SESSION['EMAIL_TEMPLATE_FROM_LIST_VIEW'] = $_SESSION['EMAILTEMPLATE_FROM_LIST_VIEW'];
	
	global $app_strings;
	global $mod_strings;
	
	$focus = new EmailTemplate();
	
	$detailView = new DetailView();
	$offset = 0;
	if (isset($_REQUEST['offset']) or isset($_REQUEST['record'])) {
		$result = $detailView->processSugarBean("EMAIL_TEMPLATE", $focus, $offset);
		if ($result == null) {
			sugar_die($app_strings['ERROR_NO_RECORD']);
		}
		$focus = $result;
	} else {
		header("Location: index.php?module=Accounts&action=index");
	}
	if (isset($_REQUEST['isDuplicate']) && $_REQUEST['isDuplicate'] == 'true') {
		$focus->id = "";
	}
	
	//needed when creating a new note with default values passed in
	if (isset($_REQUEST['contact_name']) && is_null($focus->contact_name)) {
		$focus->contact_name = $_REQUEST['contact_name'];
	}
	if (isset($_REQUEST['contact_id']) && is_null($focus->contact_id)) {
		$focus->contact_id = $_REQUEST['contact_id'];
	}
	if (isset($_REQUEST['opportunity_name']) && is_null($focus->parent_name)) {
		$focus->parent_name = $_REQUEST['opportunity_name'];
	}
	if (isset($_REQUEST['opportunity_id']) && is_null($focus->parent_id)) {
		$focus->parent_id = $_REQUEST['opportunity_id'];
	}
	if (isset($_REQUEST['account_name']) && is_null($focus->parent_name)) {
		$focus->parent_name = $_REQUEST['account_name'];
	}
	if (isset($_REQUEST['account_id']) && is_null($focus->parent_id)) {
		$focus->parent_id = $_REQUEST['account_id'];
	}
	
	$params = array();
	$params[] = $focus->name;
	
	echo getClassicModuleTitle($focus->module_dir, $params, true);
	
	
	$GLOBALS['log']->info("EmailTemplate detail view");
	
	$xtpl = new XTemplate('modules/EmailTemplates/DetailView.html');
	$xtpl->assign("MOD", $mod_strings);
	$xtpl->assign("APP", $app_strings);
	$buttons = array(<<<EOD
            <input type="submit" class="button" id="editEmailTemplatesButton" title="{$app_strings['LBL_EDIT_BUTTON_TITLE']}" accessKey="{$app_strings['LBL_EDIT_BUTTON_KEY']}" onclick="this.form.return_module.value='EmailTemplates'; this.form.return_action.value='DetailView'; this.form.return_id.value='{$focus->id}'; this.form.action.value='EditView'" value="{$app_strings['LBL_EDIT_BUTTON_LABEL']}">
EOD
	, <<<EOD
            <input title="{$app_strings['LBL_DUPLICATE_BUTTON_TITLE']}" accessKey="{$app_strings['LBL_DUPLICATE_BUTTON_KEY']}" class="button" onclick="this.form.return_module.value='EmailTemplates'; this.form.return_action.value='index'; this.form.isDuplicate.value=true; this.form.action.value='EditView'" type="submit" name="button" value="{$app_strings['LBL_DUPLICATE_BUTTON_LABEL']}">
EOD
	, <<<EOD
            <input title="{$app_strings['LBL_DELETE_BUTTON_TITLE']}" accessKey="{$app_strings['LBL_DELETE_BUTTON_KEY']}" class="button" onclick="check_deletable_EmailTemplate();" type="button" name="button" value="{$app_strings['LBL_DELETE_BUTTON_LABEL']}">
EOD
	);
	require_once ('include/Smarty/plugins/function.sugar_action_menu.php');
	$action_button = smarty_function_sugar_action_menu(array(
		'id' => 'detail_header_action_menu', 
		'buttons' => $buttons, 
		'class' => 'clickMenu fancymenu'), $xtpl);
	
	$xtpl->assign("ACTION_BUTTON", $action_button);
	
	if (isset($_REQUEST['return_module'])) 
		$xtpl->assign("RETURN_MODULE", $_REQUEST['return_module']);
	if (isset($_REQUEST['return_action'])) 
		$xtpl->assign("RETURN_ACTION", $_REQUEST['return_action']);
	if (isset($_REQUEST['return_id'])) 
		$xtpl->assign("RETURN_ID", $_REQUEST['return_id']);
	$xtpl->assign("GRIDLINE", $gridline);
	$xtpl->assign("PRINT_URL", "index.php?" . $GLOBALS['request_string']);
	$xtpl->assign("ID", $focus->id);
	$xtpl->assign("CREATED_BY", $focus->created_by_name);
	$xtpl->assign("MODIFIED_BY", $focus->modified_by_name);
	//if text only is set to true, then make sure input is checked and value set to 1
	if (isset($focus->text_only) && $focus->text_only) {
		$xtpl->assign("TEXT_ONLY_CHECKED", "CHECKED");
	}
	$xtpl->assign("NAME", $focus->name);
	$xtpl->assign("DESCRIPTION", $focus->description);
	$xtpl->assign("SUBJECT", $focus->subject);
	$xtpl->assign("BODY", $focus->body);
	$xtpl->assign("BODY_HTML", json_encode(from_html($focus->body_html)));
	$xtpl->assign("DATE_MODIFIED", $focus->date_modified);
	$xtpl->assign("DATE_ENTERED", $focus->date_entered);
	$xtpl->assign("ASSIGNED_USER_NAME", $focus->assigned_user_name);
	
	$xtpl->assign("TYPE", $app_list_strings['emailTemplates_type_list'][$focus->type]);
	
	if ($focus->ACLAccess('EditView')) {
		$xtpl->parse("main.edit");
		//$xtpl->out("EDIT");
		
	}
	if (!empty($focus->body)) {
		$xtpl->assign('ALT_CHECKED', 'CHECKED');
	} else 
		$xtpl->assign('ALT_CHECKED', '');
	if ($focus->published == 'on') {
		$xtpl->assign("PUBLISHED", "CHECKED");
	}
	
	
	///////////////////////////////////////////////////////////////////////////////
	////	NOTES (attachements, etc.)
	///////////////////////////////////////////////////////////////////////////////
	$note = new Note();
	$where = "notes.parent_id='{$focus->id}'";
	$notes_list = $note->get_full_list("notes.name", $where, true);
	
	if (!isset($notes_list)) {
		$notes_list = array();
	}
	
	$attachments = '';
	for($i = 0; $i < count($notes_list); $i++) {
		$the_note = $notes_list[$i];
		$attachments .= "<a href=\"index.php?entryPoint=download&id={$the_note->id}&type=Notes\">" . $the_note->name . "</a><br />";
	}
	
	$xtpl->assign("ATTACHMENTS", $attachments);
	
	
	global $current_user;
	if (is_admin($current_user) && $_REQUEST['module'] != 'DynamicLayout' && !empty($_SESSION['editinplace'])) {
		
		$xtpl->assign("ADMIN_EDIT", "<a href='index.php?action=index&module=DynamicLayout&from_action=" . $_REQUEST['action'] . "&from_module=" . $_REQUEST['module'] . "&record=" . $_REQUEST['record'] . "'>" . SugarThemeRegistry::current()->getImage("EditLayout", "border='0' align='bottom'", null, null, '.gif', $mod_strings['LBL_EDIT_LAYOUT']) . "</a>");
	}
	
	$xtpl->assign("DESCRIPTION", $focus->description);
	
	$detailView->processListNavigation($xtpl, "EMAIL_TEMPLATE", $offset);
	// adding custom fields:
	require_once ('modules/DynamicFields/templates/Files/DetailView.php');
	
	
	$xtpl->parse("main");
	
	$xtpl->out("main");
	
?>



//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description: TODO:  To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
	
	require_once ('modules/Campaigns/utils.php');
	
	//if campaign_id is passed then we assume this is being invoked from the campaign module and in a popup.
	$has_campaign = true;
	$inboundEmail = true;
	if (!isset($_REQUEST['campaign_id']) || empty($_REQUEST['campaign_id'])) {
		$has_campaign = false;
	}
	if (!isset($_REQUEST['inboundEmail']) || empty($_REQUEST['inboundEmail'])) {
		$inboundEmail = false;
	}
	$focus = new EmailTemplate();
	
	if (isset($_REQUEST['record'])) {
		$focus->retrieve($_REQUEST['record']);
	}
	
	$old_id = '';
	if (isset($_REQUEST['isDuplicate']) && $_REQUEST['isDuplicate'] == 'true') {
		$old_id = $focus->id; // for attachments down below
		$focus->id = "";
	}
	
	
	
	//setting default flag value so due date and time not required
	if (!isset($focus->id)) 
		$focus->date_due_flag = 1;
	
	//needed when creating a new case with default values passed in
	if (isset($_REQUEST['contact_name']) && is_null($focus->contact_name)) {
		$focus->contact_name = $_REQUEST['contact_name'];
	}
	if (isset($_REQUEST['contact_id']) && is_null($focus->contact_id)) {
		$focus->contact_id = $_REQUEST['contact_id'];
	}
	if (isset($_REQUEST['parent_name']) && is_null($focus->parent_name)) {
		$focus->parent_name = $_REQUEST['parent_name'];
	}
	if (isset($_REQUEST['parent_id']) && is_null($focus->parent_id)) {
		$focus->parent_id = $_REQUEST['parent_id'];
	}
	if (isset($_REQUEST['parent_type'])) {
		$focus->parent_type = $_REQUEST['parent_type'];
	} elseif (!isset($focus->parent_type)) {
		$focus->parent_type = $app_list_strings['record_type_default_key'];
	}
	if (isset($_REQUEST['filename']) && $_REQUEST['isDuplicate'] != 'true') {
		$focus->filename = $_REQUEST['filename'];
	}
	
	if ($has_campaign || $inboundEmail) {
		insert_popup_header($theme);
	}
	
	
	$params = array();
	
	if (empty($focus->id)) {
		$params[] = $GLOBALS['app_strings']['LBL_CREATE_BUTTON_LABEL'];
	} else {
		$params[] = "<a href='index.php?module={$focus->module_dir}&action=DetailView&record={$focus->id}'>{$focus->name}</a>";
		$params[] = $GLOBALS['app_strings']['LBL_EDIT_BUTTON_LABEL'];
	}
	
	echo getClassicModuleTitle($focus->module_dir, $params, true);
	
	if (!$focus->ACLAccess('EditView')) {
		ACLController::displayNoAccess(true);
		sugar_cleanup(true);
	}
	
	$GLOBALS['log']->info("EmailTemplate detail view");
	
	if ($has_campaign || $inboundEmail) {
		$xtpl = new XTemplate('modules/EmailTemplates/EditView.html');
	} else {
		$xtpl = new XTemplate('modules/EmailTemplates/EditViewMain.html');
	}
	// else
	$xtpl->assign("MOD", $mod_strings);
	$xtpl->assign("APP", $app_strings);
	
	$xtpl->assign("LBL_ACCOUNT", $app_list_strings['moduleList']['Accounts']);
	$xtpl->parse("main.variable_option");
	
	$returnAction = 'index';
	if (isset($_REQUEST['return_module'])) 
		$xtpl->assign("RETURN_MODULE", $_REQUEST['return_module']);
	if (isset($_REQUEST['return_action'])) {
		$xtpl->assign("RETURN_ACTION", $_REQUEST['return_action']);
		$returnAction = $_REQUEST['return_action'];
	}
	if (isset($_REQUEST['return_id'])) 
		$xtpl->assign("RETURN_ID", $_REQUEST['return_id']);
	// handle Create $module then Cancel
	if (empty($_REQUEST['return_id'])) {
		$xtpl->assign("RETURN_ACTION", 'index');
	}
	
	if ($has_campaign || $inboundEmail) {
		$cancel_script = "window.close();";
	} else {
		$cancel_script = "this.form.action.value='{$returnAction}'; this.form.module.value='{$_REQUEST['return_module']}';
    this.form.record.value=";
		if (empty($_REQUEST['return_id'])) {
			$cancel_script = "this.form.action.value='index'; this.form.module.value='{$_REQUEST['return_module']}';this.form.name.value='';this.form.description.value=''";
		} else {
			$cancel_script .= "'{$_REQUEST['return_id']}'";
		}
	}
	
	//Setup assigned user name
	$popup_request_data = array(
		'call_back_function' => 'set_return', 
		'form_name' => 'EditView', 
		'field_to_name_array' => array(
		'id' => 'assigned_user_id', 
		'user_name' => 'assigned_user_name'));
	$json = getJSONobj();
	$xtpl->assign('encoded_assigned_users_popup_request_data', $json->encode($popup_request_data));
	if (!empty($focus->assigned_user_name)) 
		$xtpl->assign("ASSIGNED_USER_NAME", $focus->assigned_user_name);
	
	$xtpl->assign("assign_user_select", SugarThemeRegistry::current()->getImage('id-ff-select', '', null, null, '.png', $mod_strings['LBL_SELECT']));
	$xtpl->assign("assign_user_clear", SugarThemeRegistry::current()->getImage('id-ff-clear', '', null, null, '.gif', $mod_strings['LBL_ID_FF_CLEAR']));
	//Assign qsd script
	require_once ('include/QuickSearchDefaults.php');
	$qsd = QuickSearchDefaults::getQuickSearchDefaults();
	$sqs_objects = array('EditView_assigned_user_name' => $qsd->getQSUser());
	$quicksearch_js = '<script type="text/javascript" language="javascript">sqs_objects = ' . $json->encode($sqs_objects) . '; enableQS();</script>';
	
	$xtpl->assign("CANCEL_SCRIPT", $cancel_script);
	$xtpl->assign("PRINT_URL", "index.php?" . $GLOBALS['request_string']);
	$xtpl->assign("JAVASCRIPT", get_set_focus_js() . $quicksearch_js);
	
	if (!is_file(sugar_cached('jsLanguage/') . $GLOBALS['current_language'] . '.js')) {
		require_once ('include/language/jsLanguage.php');
		jsLanguage::createAppStringsCache($GLOBALS['current_language']);
	}
	$jsLang = getVersionedScript("cache/jsLanguage/{$GLOBALS['current_language']}.js", $GLOBALS['sugar_config']['js_lang_version']);
	$xtpl->assign("JSLANG", $jsLang);
	
	$xtpl->assign("ID", $focus->id);
	if (isset($focus->name)) 
		$xtpl->assign("NAME", $focus->name);
	 else 
		$xtpl->assign("NAME", "");
	
	//Bug45632
	if (isset($focus->assigned_user_id)) 
		$xtpl->assign("ASSIGNED_USER_ID", $focus->assigned_user_id);
	 else 
		$xtpl->assign("ASSIGNED_USER_ID", "");
	//Bug45632
	
	if (isset($focus->description)) 
		$xtpl->assign("DESCRIPTION", $focus->description);
	 else 
		$xtpl->assign("DESCRIPTION", "");
	if (isset($focus->subject)) 
		$xtpl->assign("SUBJECT", $focus->subject);
	 else 
		$xtpl->assign("SUBJECT", "");
	if ($focus->published == 'on') {
		$xtpl->assign("PUBLISHED", "CHECKED");
	}
	//if text only is set to true, then make sure input is checked and value set to 1
	if (isset($focus->text_only) && $focus->text_only) {
		$xtpl->assign("TEXTONLY_CHECKED", "CHECKED");
		$xtpl->assign("TEXTONLY_VALUE", "1");
	} else {
		//set value to 0
		$xtpl->assign("TEXTONLY_VALUE", "0");
	}
	
	
	
	$xtpl->assign("FIELD_DEFS_JS", $focus->generateFieldDefsJS());
	$xtpl->assign("LBL_CONTACT", $app_list_strings['moduleList']['Contacts']);
	
	global $current_user;
	if (is_admin($current_user) && $_REQUEST['module'] != 'DynamicLayout' && !empty($_SESSION['editinplace'])) {
		$record = '';
		if (!empty($_REQUEST['record'])) {
			$record = $_REQUEST['record'];
		}
		
		$xtpl->assign("ADMIN_EDIT", "<a href='index.php?action=index&module=DynamicLayout&from_action=" . $_REQUEST['action'] . "&from_module=" . $_REQUEST['module'] . "&record=" . $record . "'>" . SugarThemeRegistry::current()->getImage("EditLayout", "border='0' align='bottom'", null, null, '.gif', $mod_strings['LBL_EDIT_LAYOUT']) . "</a>");
		
	}
	if (isset($focus->parent_type) && $focus->parent_type != "") {
		$change_parent_button = "<input title='" . $app_strings['LBL_SELECT_BUTTON_TITLE'] . 
			"' 
tabindex='3' type='button' class='button' value='" . $app_strings['LBL_SELECT_BUTTON_LABEL'] . 
			"' name='button' LANGUAGE=javascript onclick='return
window.open(\"index.php?module=\"+ document.EditView.parent_type.value +
\"&action=Popup&html=Popup_picker&form=TasksEditView\",\"test\",\"width=600,height=400,resizable=1,scrollbars=1\");'>";
		$xtpl->assign("CHANGE_PARENT_BUTTON", $change_parent_button);
	}
	if ($focus->parent_type == "Account") {
		$xtpl->assign("DEFAULT_SEARCH", "&query=true&account_id=$focus->parent_id&account_name=" . urlencode($focus->parent_name));
	}
	
	$xtpl->assign("DESCRIPTION", $focus->description);
	$xtpl->assign("TYPE_OPTIONS", get_select_options_with_id($app_list_strings['record_type_display'], $focus->parent_type));
	//$xtpl->assign("DEFAULT_MODULE","Accounts");
	
	if (isset($focus->body)) 
		$xtpl->assign("BODY", $focus->body);
	 else 
		$xtpl->assign("BODY", "");
	if (isset($focus->body_html)) 
		$xtpl->assign("BODY_HTML", $focus->body_html);
	 else 
		$xtpl->assign("BODY_HTML", "");
	
	
	if (true) {
		if (!isTouchScreen()) {
			require_once ("include/SugarTinyMCE.php");
			$tiny = new SugarTinyMCE();
			$tiny->defaultConfig['cleanup_on_startup'] = true;
			$tiny->defaultConfig['height'] = 600;
			$tiny->defaultConfig['plugins'] .= ",fullpage";
			$tinyHtml = $tiny->getInstance();
			$xtpl->assign("tiny", $tinyHtml);
		}
		///////////////////////////////////////
		////	MACRO VARS
		$xtpl->assign("INSERT_VARIABLE_ONCLICK", "insert_variable(document.EditView.variable_text.value)");
		
		// bug 37255, included without condition
		$xtpl->parse("main.NoInbound.variable_button");
		
		///////////////////////////////////////
		////	CAMPAIGNS
		if ($has_campaign || $inboundEmail) {
			$xtpl->assign("INPOPUPWINDOW", 'true');
			$xtpl->assign("INSERT_URL_ONCLICK", "insert_variable_html_link(document.EditView.tracker_url.value)");
			if ($has_campaign) {
				$campaign_urls = get_campaign_urls($_REQUEST['campaign_id']);
			}
			if (!empty($campaign_urls)) {
				$xtpl->assign("DEFAULT_URL_TEXT", key($campaign_urls));
			}
			if ($has_campaign) {
				$xtpl->assign("TRACKER_KEY_OPTIONS", get_select_options_with_id($campaign_urls, null));
				$xtpl->parse("main.NoInbound.tracker_url");
			}
		}
		
		// create option of "Contact/Lead/Task" from corresponding module
		// translations
		$lblContactAndOthers = implode('/', array(isset($app_list_strings['moduleListSingular']['Contacts']) ? $app_list_strings['moduleListSingular']['Contacts'] : 'Contact', isset($app_list_strings['moduleListSingular']['Leads']) ? $app_list_strings['moduleListSingular']['Leads'] : 'Lead', isset($app_list_strings['moduleListSingular']['Prospects']) ? $app_list_strings['moduleListSingular']['Prospects'] : 'Target'));
		
		// The insert variable drodown should be conditionally displayed.
		// If it's campaign then hide the Account.
		if ($has_campaign) {
			$dropdown = 
				"<option value='Contacts'>
						" . $lblContactAndOthers . 
				"
			       </option>";
			$xtpl->assign("DROPDOWN", $dropdown);
			$xtpl->assign("DEFAULT_MODULE", 'Contacts');
			//$xtpl->assign("CAMPAIGN_POPUP_JS", '<script type="text/javascript" src="include/javascript/sugar_3.js"></script>');
		} else {
			$dropdown = 
				"<option value='Accounts'>
						" . $app_list_strings['moduleListSingular']['Accounts'] . 
				"
		  	       </option>
			       <option value='Contacts'>
						" . $lblContactAndOthers . 
				"
			       </option>
			       <option value='Users'>
						" . $app_list_strings['moduleListSingular']['Users'] . 
				"
			       </option>";
			$xtpl->assign("DROPDOWN", $dropdown);
			$xtpl->assign("DEFAULT_MODULE", 'Accounts');
		}
		////	END CAMPAIGNS
		///////////////////////////////////////
		
		///////////////////////////////////////
		////    ATTACHMENTS
		$attachments = '';
		if (!empty($focus->id)) {
			$etid = $focus->id;
		} elseif (!empty($old_id)) {
			$xtpl->assign('OLD_ID', $old_id);
			$etid = $old_id;
		}
		if (!empty($etid)) {
			$note = new Note();
			$where = "notes.parent_id='{$etid}' AND notes.filename IS NOT NULL";
			$notes_list = $note->get_full_list("", $where, true);
			
			if (!isset($notes_list)) {
				$notes_list = array();
			}
			for($i = 0; $i < count($notes_list); $i++) {
				$the_note = $notes_list[$i];
				if (empty($the_note->filename)) {
					continue;
				}
				$secureLink = 'index.php?entryPoint=download&id=' . $the_note->id . '&type=Notes';
				$attachments .= '<input type="checkbox" name="remove_attachment[]" value="' . $the_note->id . '"> ' . $app_strings['LNK_REMOVE'] . '&nbsp;&nbsp;';
				$attachments .= '<a href="' . $secureLink . '" target="_blank">' . $the_note->filename . '</a><br>';
			}
		}
		$attJs = '<script type="text/javascript">';
		$attJs .= 'var lnk_remove = "' . $app_strings['LNK_REMOVE'] . '";';
		$attJs .= '</script>';
		$xtpl->assign('ATTACHMENTS', $attachments);
		$xtpl->assign('ATTACHMENTS_JAVASCRIPT', $attJs);
		
		////    END ATTACHMENTS
		///////////////////////////////////////
		$templateType = !empty($focus->type) ? $focus->type : '';
		if ($has_campaign) {
			if (empty($_REQUEST['record'])) {
				// new record, default to campaign
				$xtpl->assign("TYPEDROPDOWN", get_select_options_with_id($app_list_strings['emailTemplates_type_list_campaigns'], 'campaign'));
			} else {
				$xtpl->assign("TYPEDROPDOWN", get_select_options_with_id($app_list_strings['emailTemplates_type_list_campaigns'], $templateType));
			}
		} else {
			// if the type is workflow, we will show it
			// otherwise we don't allow user to select workflow type because workflow type email template
			// should be created from within workflow module because it requires more fields (such as base module, etc)
			if ($templateType == 'workflow') {
				$xtpl->assign("TYPEDROPDOWN", get_select_options_with_id($app_list_strings['emailTemplates_type_list'], $templateType));
			} else {
				$xtpl->assign("TYPEDROPDOWN", get_select_options_with_id($app_list_strings['emailTemplates_type_list_no_workflow'], $templateType));
			}
		}
		// done and parse
		$xtpl->parse("main.textarea");
	}
	
	//Add Custom Fields
	require_once ('modules/DynamicFields/templates/Files/EditView.php');
	$xtpl->parse("main.NoInbound");
	if (!$inboundEmail) {
		$xtpl->parse("main.NoInbound1");
		$xtpl->parse("main.NoInbound2");
		$xtpl->parse("main.NoInbound3");
	}
	$xtpl->parse("main.NoInbound4");
	$xtpl->parse("main.NoInbound5");
	$xtpl->parse("main");
	
	$xtpl->out("main");
	
	$javascript = new javascript();
	$javascript->setFormName('EditView');
	$javascript->setSugarBean($focus);
	$javascript->addAllFields('');
	echo $javascript->getScript();
?>



//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
	
	
	
	
	
	
	// EmailTemplate is used to store email email_template information.
	class EmailTemplate extends SugarBean
	{
		var $field_name_map = array();
		// Stored fields
		var $id;
		var $date_entered;
		var $date_modified;
		var $modified_user_id;
		var $created_by;
		var $created_by_name;
		var $modified_by_name;
		var $assigned_user_id;
		var $assigned_user_name;
		var $name;
		var $published;
		var $description;
		var $body;
		var $body_html;
		var $subject;
		var $attachments;
		var $from_name;
		var $from_address;
		var $table_name = "email_templates";
		var $object_name = "EmailTemplate";
		var $module_dir = "EmailTemplates";
		var $new_schema = true;
		// This is used to retrieve related fields from form posts.
		var $additional_column_fields = array();
		// add fields here that would not make sense in an email template
		// User objects
		var $badFields = array(
			'account_description', 
			'contact_id', 
			'lead_id', 
			'opportunity_amount', 
			'opportunity_id', 
			'opportunity_name', 
			'opportunity_role_id', 
			'opportunity_role_fields', 
			'opportunity_role', 
			'campaign_id', 
			'id', 
			'date_entered', 
			'date_modified', 
			'user_preferences', 
			'accept_status', 
			'user_hash', 
			'authenticate_id', 
			'sugar_login', 
			'reports_to_id', 
			'reports_to_name', 
			'is_admin', 
			'receive_notifications', 
			'modified_user_id', 
			'modified_by_name', 
			'created_by', 
			'created_by_name', 
			'accept_status_id', 
			'accept_status_name');
		
		/**
     * @var array temp storage for template variables while cleanBean
     */
		protected $storedVariables = array();
		
		function EmailTemplate()
		{
			parent::SugarBean();
		}
		
		/**
	 * Generates the extended field_defs for creating macros
	 * @param object $bean SugarBean
	 * @param string $prefix "contact_", "user_" etc.
	 * @return
	 */
		function generateFieldDefsJS()
		{
			global $current_user;
			
			
			
			
			
			$contact = new Contact();
			$account = new Account();
			$lead = new Lead();
			$prospect = new Prospect();
			
			
			$loopControl = array(
				'Contacts' => array(
				'Contacts' => $contact, 
				'Leads' => $lead, 
				'Prospects' => $prospect), 
				'Accounts' => array(
				'Accounts' => $account), 
				'Users' => array(
				'Users' => $current_user));
			
			$prefixes = array(
				'Contacts' => 'contact_', 
				'Accounts' => 'account_', 
				'Users' => 'contact_user_');
			
			$collection = array();
			foreach ($loopControl as $collectionKey => $beans) {
				$collection[$collectionKey] = array();
				foreach ($beans as $beankey => $bean) {
					
					foreach ($bean->field_defs as $key => $field_def) {
						if (($field_def['type'] == 'relate' && empty($field_def['custom_type'])) || ($field_def['type'] == 'assigned_user_name' || $field_def['type'] == 'link') || ($field_def['type'] == 'bool') || (in_array($field_def['name'], $this->badFields))) {
							continue;
						}
						if (!isset($field_def['vname'])) {
							//echo $key;
						}
						// valid def found, process
						$optionKey = strtolower("{$prefixes[$collectionKey]}{$key}");
						$optionLabel = preg_replace('/:$/', "", translate($field_def['vname'], $beankey));
						$dup = 1;
						foreach ($collection[$collectionKey] as $value) {
							if ($value['name'] == $optionKey) {
								$dup = 0;
								break;
							}
						}
						if ($dup) 
							$collection[$collectionKey][] = array("name" => $optionKey, "value" => $optionLabel);
					}
				}
			}
			
			$json = getJSONobj();
			$ret = "var field_defs = ";
			$ret .= $json->encode($collection, false);
			$ret .= ";";
			return $ret;
		}
		
		function get_summary_text()
		{
			return "$this->name";
		}
		
		function create_export_query(&$order_by, &$where)
		{
			return $this->create_new_list_query($order_by, $where);
		}
		
		function fill_in_additional_list_fields()
		{
			$this->fill_in_additional_parent_fields();
		}
		
		function fill_in_additional_detail_fields()
		{
			if (empty($this->body) && !empty($this->body_html)) {
				global $sugar_config;
				$this->body = strip_tags(html_entity_decode($this->body_html, ENT_COMPAT, $sugar_config['default_charset']));
			}
			$this->created_by_name = get_assigned_user_name($this->created_by);
			$this->modified_by_name = get_assigned_user_name($this->modified_user_id);
			$this->assigned_user_name = get_assigned_user_name($this->assigned_user_id);
			$this->fill_in_additional_parent_fields();
		}
		
		function fill_in_additional_parent_fields()
		{
		}
		
		function get_list_view_data()
		{
			global $app_list_strings, $focus, $action, $currentModule;
			$fields = $this->get_list_view_array();
			$fields["DATE_MODIFIED"] = substr($fields["DATE_MODIFIED"], 0, 10);
			return $fields;
		}
		
		//function all string that match the pattern {.} , also catches the list of found strings.
		//the cache will get refreshed when the template bean instance changes.
		//The found url key patterns are replaced with name value pairs provided as function parameter. $tracked_urls.
		//$url_template is used to construct the url for the email message. the template should have place holder for 1 variable parameter, represented by %1
		//$template_text_array is a list of text strings that need to be searched. usually the subject, html body and text body of the email message.
		//$removeme_url_template, if the url has is_optout property checked then use this template.
		function parse_tracker_urls($template_text_array, $url_template, $tracked_urls, $removeme_url_template)
		{
			global $beanFiles, $beanList, $app_list_strings, $sugar_config;
			if (!isset($this->parsed_urls)) 
				$this->parsed_urls = array();
			
			$return_array = $template_text_array;
			if (count($tracked_urls) > 0) {
				//parse the template and find all the dynamic strings that need replacement.
				foreach ($template_text_array as $key => $template_text) {
					if (!empty($template_text)) {
						
						if (!isset($this->parsed_urls[$key]) || $this->parsed_urls[$key]['text'] != $template_text) {
							// Fix for bug52014.
							$template_text = urldecode($template_text);
							$matches = $this->_preg_match_tracker_url($template_text);
							$count = count($matches[0]);
							$this->parsed_urls[$key] = array('matches' => $matches, 'text' => $template_text);
						} else {
							$matches = $this->parsed_urls[$key]['matches'];
							if (!empty($matches[0])) {
								$count = count($matches[0]);
							} else {
								$count = 0;
							}
						}
						
						//navigate thru all the matched keys and replace the keys with actual strings.
						
						if ($count > 0) {
							for($i = ($count - 1); $i >= 0; $i--) {
								$url_key_name = $matches[0][$i][0];
								if (!empty($tracked_urls[$url_key_name])) {
									if ($tracked_urls[$url_key_name]['is_optout'] == 1) {
										$tracker_url = $removeme_url_template;
									} else {
										$tracker_url = sprintf($url_template, $tracked_urls[$url_key_name]['id']);
									}
								}
								if (!empty($tracker_url) && !empty($template_text) && !empty($matches[0][$i][0]) && !empty($tracked_urls[$matches[0][$i][0]])) {
									$template_text = substr_replace($template_text, $tracker_url, $matches[0][$i][1], strlen($matches[0][$i][0]));
									$template_text = str_replace($sugar_config['site_url'] . '/' . $sugar_config['site_url'], $sugar_config['site_url'], $template_text);
								}
							}
						}
					}
					$return_array[$key] = $template_text;
				}
			}
			return $return_array;
		}
		
		/**
     *
     * Method for replace "preg_match_all" in method "parse_tracker_urls"
     * @param $text string String in which we need to search all string that match the pattern {.}
     * @return array result of search
     */
		private function _preg_match_tracker_url($text)
		{
			$result = array();
			$ind = 0;
			$switch = false;
			for($i = 0; $i < strlen($text); $i++) {
				if ($text[$i] == '{') {
					$ind = $i;
					$switch = true;
				} elseif ($text[$i] == '}' && $switch === true) {
					$switch = false;
					array_push($result, array(substr($text, $ind, $i - $ind + 1), $ind));
				}
			}
			return array($result);
		}
		
		function parse_email_template($template_text_array, $focus_name, $focus, &$macro_nv)
		{
			
			
			global $beanFiles, $beanList, $app_list_strings;
			
			// generate User instance that owns this "Contact" for contact_user_* macros
			$user = new User();
			if (isset($focus->assigned_user_id) && !empty($focus->assigned_user_id)) {
				$user->retrieve($focus->assigned_user_id);
			}
			
			if (!isset($this->parsed_entities)) 
				$this->parsed_entities = array();
			
			//parse the template and find all the dynamic strings that need replacement.
			// Bug #48111 It's strange why prefix for User module is contact_user (see self::generateFieldDefsJS method)
			if ($beanList[$focus_name] == 'User') {
				$pattern_prefix = '$contact_user_';
			} else {
				$pattern_prefix = '$' . strtolower($beanList[$focus_name]) . '_';
			}
			$pattern_prefix_length = strlen($pattern_prefix);
			$pattern = '/\\' . $pattern_prefix . '[A-Za-z_0-9]*/';
			
			foreach ($template_text_array as $key => $template_text) {
				if (!isset($this->parsed_entities[$key])) {
					$matches = array();
					$count = preg_match_all($pattern, $template_text, $matches, PREG_OFFSET_CAPTURE);
					
					if ($count != 0) {
						for($i = ($count - 1); $i >= 0; $i--) {
							if (!isset($matches[0][$i][2])) {
								//find the field name in the bean.
								$matches[0][$i][2] = substr($matches[0][$i][0], $pattern_prefix_length, strlen($matches[0][$i][0]) - $pattern_prefix_length);
								
								//store the localized strings if the field is of type enum..
								if (isset($focus->field_defs[$matches[0][$i][2]]) && $focus->field_defs[$matches[0][$i][2]]['type'] == 'enum' && isset($focus->field_defs[$matches[0][$i][2]]['options'])) {
									$matches[0][$i][3] = $focus->field_defs[$matches[0][$i][2]]['options'];
								}
							}
						}
					}
					$this->parsed_entities[$key] = $matches;
				} else {
					$matches = $this->parsed_entities[$key];
					if (!empty($matches[0])) {
						$count = count($matches[0]);
					} else {
						$count = 0;
					}
				}
				
				for($i = ($count - 1); $i >= 0; $i--) {
					$field_name = $matches[0][$i][2];
					
					// cn: feel for User object attribute key and assign as found
					if (strpos($field_name, "user_") === 0) {
						$userFieldName = substr($field_name, 5);
						$value = $user->$userFieldName;
						//_pp($userFieldName."[{$value}]");
					} else {
						$value = $focus->{$field_name};
					}
					
					//check dom
					if (isset($matches[0][$i][3])) {
						if (isset($app_list_strings[$matches[0][$i][3]][$value])) {
							$value = $app_list_strings[$matches[0][$i][3]][$value];
						}
					}
					
					//generate name value pair array of macros and corresponding values for the targed.
					$macro_nv[$matches[0][$i][0]] = $value;
					
					$template_text = substr_replace($template_text, $value, $matches[0][$i][1], strlen($matches[0][$i][0]));
				}
				
				//parse the template for tracker url strings. patter for these strings in {[a-zA-Z_0-9]+}
				
				$return_array[$key] = $template_text;
			}
			
			return $return_array;
		}
		
		/**
     * Convenience method to convert raw value into appropriate type format
     * @param string $type
     * @param string $value
     * @return string
     */
		function _convertToType($type, $value)
		{
			switch ($type) {
				case 'currency':
					return currency_format_number($value);
				default:
					return $value;
			}
		}
		
		/**
	 * Convenience method to parse for user's values in a template
	 * @param array $repl_arr
	 * @param object $user
	 * @return array
	 */
		function _parseUserValues($repl_arr, &$user)
		{
			foreach ($user->field_defs as $field_def) {
				if (($field_def['type'] == 'relate' && empty($field_def['custom_type'])) || $field_def['type'] == 'assigned_user_name') {
					continue;
				}
				
				if ($field_def['type'] == 'enum') {
					$translated = translate($field_def['options'], 'Users', $user->{$field_def['name']});
					
					if (isset($translated) && !is_array($translated)) {
						$repl_arr["contact_user_" . $field_def['name']] = $translated;
					} else {
						// unset enum field, make sure we have a match string to replace with ""
						$repl_arr["contact_user_" . $field_def['name']] = '';
					}
				} else {
					if (isset($user->{$field_def['name']})) {
						// bug 47647 - allow for fields to translate before adding to template
						$repl_arr["contact_user_" . $field_def['name']] = self::_convertToType($field_def['type'], $user->{$field_def['name']});
					} else {
						$repl_arr["contact_user_" . $field_def['name']] = "";
					}
				}
			}
			
			return $repl_arr;
		}
		
		
		function parse_template_bean($string, $bean_name, &$focus)
		{
			global $current_user;
			global $beanFiles, $beanList;
			$repl_arr = array();
			
			// cn: bug 9277 - create a replace array with empty strings to blank-out invalid vars
			$acct = new Account();
			$contact = new Contact();
			$lead = new Lead();
			$prospect = new Prospect();
			
			foreach ($lead->field_defs as $field_def) {
				if (($field_def['type'] == 'relate' && empty($field_def['custom_type'])) || $field_def['type'] == 'assigned_user_name') {
					continue;
				}
				$repl_arr = EmailTemplate::add_replacement($repl_arr, $field_def, array(
					'contact_' . $field_def['name'] => '', 
					'contact_account_' . $field_def['name'] => ''));
			}
			foreach ($prospect->field_defs as $field_def) {
				if (($field_def['type'] == 'relate' && empty($field_def['custom_type'])) || $field_def['type'] == 'assigned_user_name') {
					continue;
				}
				$repl_arr = EmailTemplate::add_replacement($repl_arr, $field_def, array(
					'contact_' . $field_def['name'] => '', 
					'contact_account_' . $field_def['name'] => ''));
			}
			foreach ($contact->field_defs as $field_def) {
				if (($field_def['type'] == 'relate' && empty($field_def['custom_type'])) || $field_def['type'] == 'assigned_user_name') {
					continue;
				}
				$repl_arr = EmailTemplate::add_replacement($repl_arr, $field_def, array(
					'contact_' . $field_def['name'] => '', 
					'contact_account_' . $field_def['name'] => ''));
			}
			foreach ($acct->field_defs as $field_def) {
				if (($field_def['type'] == 'relate' && empty($field_def['custom_type'])) || $field_def['type'] == 'assigned_user_name') {
					continue;
				}
				$repl_arr = EmailTemplate::add_replacement($repl_arr, $field_def, array(
					'account_' . $field_def['name'] => '', 
					'account_contact_' . $field_def['name'] => ''));
			}
			// cn: end bug 9277 fix
			
			
			// feel for Parent account, only for Contacts traditionally, but written for future expansion
			if (isset($focus->account_id) && !empty($focus->account_id)) {
				$acct->retrieve($focus->account_id);
			}
			
			if ($bean_name == 'Contacts') {
				// cn: bug 9277 - email templates not loading account/opp info for templates
				if (!empty($acct->id)) {
					foreach ($acct->field_defs as $field_def) {
						if (($field_def['type'] == 'relate' && empty($field_def['custom_type'])) || $field_def['type'] == 'assigned_user_name') {
							continue;
						}
						
						if ($field_def['type'] == 'enum') {
							$translated = translate($field_def['options'], 'Accounts', $acct->{$field_def['name']});
							
							if (isset($translated) && !is_array($translated)) {
								$repl_arr = EmailTemplate::add_replacement($repl_arr, $field_def, array(
									'account_' . $field_def['name'] => $translated, 
									'contact_account_' . $field_def['name'] => $translated));
							} else {
								// unset enum field, make sure we have a match string to replace with ""
								$repl_arr = EmailTemplate::add_replacement($repl_arr, $field_def, array(
									'account_' . $field_def['name'] => '', 
									'contact_account_' . $field_def['name'] => ''));
							}
						} else {
							// bug 47647 - allow for fields to translate before adding to template
							$translated = self::_convertToType($field_def['type'], $acct->{$field_def['name']});
							$repl_arr = EmailTemplate::add_replacement($repl_arr, $field_def, array(
								'account_' . $field_def['name'] => $translated, 
								'contact_account_' . $field_def['name'] => $translated));
						}
					}
				}
				
				if (!empty($focus->assigned_user_id)) {
					$user = new User();
					$user->retrieve($focus->assigned_user_id);
					$repl_arr = EmailTemplate::_parseUserValues($repl_arr, $user);
				}
			} elseif ($bean_name == 'Users') {
				/**
			 * This section of code will on do work when a blank Contact, Lead,
			 * etc. is passed in to parse the contact_* vars.  At this point,
			 * $current_user will be used to fill in the blanks.
			 */
				$repl_arr = EmailTemplate::_parseUserValues($repl_arr, $current_user);
			} else {
				// assumed we have an Account in focus
				foreach ($contact->field_defs as $field_def) {
					if (($field_def['type'] == 'relate' && empty($field_def['custom_type'])) || $field_def['type'] == 'assigned_user_name' || $field_def['type'] == 'link') {
						continue;
					}
					
					if ($field_def['type'] == 'enum') {
						$translated = translate($field_def['options'], 'Accounts', $contact->{$field_def['name']});
						
						if (isset($translated) && !is_array($translated)) {
							$repl_arr = EmailTemplate::add_replacement($repl_arr, $field_def, array(
								'contact_' . $field_def['name'] => $translated, 
								'contact_account_' . $field_def['name'] => $translated));
						} else {
							// unset enum field, make sure we have a match string to replace with ""
							$repl_arr = EmailTemplate::add_replacement($repl_arr, $field_def, array(
								'contact_' . $field_def['name'] => '', 
								'contact_account_' . $field_def['name'] => ''));
						}
					} else {
						if (isset($contact->{$field_def['name']})) {
							// bug 47647 - allow for fields to translate before adding to template
							$translated = self::_convertToType($field_def['type'], $contact->{$field_def['name']});
							$repl_arr = EmailTemplate::add_replacement($repl_arr, $field_def, array(
								'contact_' . $field_def['name'] => $translated, 
								'contact_account_' . $field_def['name'] => $translated));
						}
						// if
					}
				}
			}
			
			///////////////////////////////////////////////////////////////////////
			////	LOAD FOCUS DATA INTO REPL_ARR
			foreach ($focus->field_defs as $field_def) {
				if (isset($focus->{$field_def['name']})) {
					if (($field_def['type'] == 'relate' && empty($field_def['custom_type'])) || $field_def['type'] == 'assigned_user_name') {
						continue;
					}
					
					if ($field_def['type'] == 'enum' && isset($field_def['options'])) {
						$translated = translate($field_def['options'], $bean_name, $focus->{$field_def['name']});
						
						if (isset($translated) && !is_array($translated)) {
							$repl_arr = EmailTemplate::add_replacement($repl_arr, $field_def, array(strtolower($beanList[$bean_name]) . "_" . $field_def['name'] => $translated));
						} else {
							// unset enum field, make sure we have a match string to replace with ""
							$repl_arr = EmailTemplate::add_replacement($repl_arr, $field_def, array(strtolower($beanList[$bean_name]) . "_" . $field_def['name'] => ''));
						}
					} else {
						// bug 47647 - translate currencies to appropriate values
						$repl_arr = EmailTemplate::add_replacement($repl_arr, $field_def, array(strtolower($beanList[$bean_name]) . "_" . $field_def['name'] => self::_convertToType($field_def['type'], $focus->{$field_def['name']})));
					}
				} else {
					if ($field_def['name'] == 'full_name') {
						$repl_arr = EmailTemplate::add_replacement($repl_arr, $field_def, array(strtolower($beanList[$bean_name]) . '_full_name' => $focus->get_summary_text()));
					} else {
						$repl_arr = EmailTemplate::add_replacement($repl_arr, $field_def, array(strtolower($beanList[$bean_name]) . "_" . $field_def['name'] => ''));
					}
				}
			}
			// end foreach()
			
			krsort($repl_arr);
			reset($repl_arr);
			//20595 add nl2br() to respect the multi-lines formatting
			if (isset($repl_arr['contact_primary_address_street'])) {
				$repl_arr['contact_primary_address_street'] = nl2br($repl_arr['contact_primary_address_street']);
			}
			if (isset($repl_arr['contact_alt_address_street'])) {
				$repl_arr['contact_alt_address_street'] = nl2br($repl_arr['contact_alt_address_street']);
			}
			
			foreach ($repl_arr as $name => $value) {
				if ($value != '' && is_string($value)) {
					$string = str_replace("\$$name", $value, $string);
				} else {
					$string = str_replace("\$$name", ' ', $string);
				}
			}
			
			return $string;
		}
		
		/**
     * Add replacement(s) to the collection based on field definition
     *
     * @param array $data
     * @param array $field_def
     * @param array $replacement
     * @return array
     */
		protected static function add_replacement($data, $field_def, $replacement)
		{
			foreach ($replacement as $key => $value) {
				// @see defect #48641
				if ('multienum' == $field_def['type']) {
					$value = implode(', ', unencodeMultienum($value));
				}
				$data[$key] = $value;
			}
			return $data;
		}
		
		function parse_template($string, &$bean_arr)
		{
			global $beanFiles, $beanList;
			
			foreach ($bean_arr as $bean_name => $bean_id) {
				require_once ($beanFiles[$beanList[$bean_name]]);
				
				$focus = new $beanList[$bean_name];
				$result = $focus->retrieve($bean_id);
				
				if ($bean_name == 'Leads' || $bean_name == 'Prospects') {
					$bean_name = 'Contacts';
				}
				
				if (isset($this) && isset($this->module_dir) && $this->module_dir == 'EmailTemplates') {
					$string = $this->parse_template_bean($string, $bean_name, $focus);
				} else {
					$string = EmailTemplate::parse_template_bean($string, $bean_name, $focus);
				}
			}
			return $string;
		}
		
		function bean_implements($interface)
		{
			switch ($interface) {
				case 'ACL':
					return true;
			}
			return false;
		}
		
		static function getTypeOptionsForSearch()
		{
			$template = new EmailTemplate();
			$optionKey = $template->field_defs['type']['options'];
			$options = $GLOBALS['app_list_strings'][$optionKey];
			if (!is_admin($GLOBALS['current_user']) && isset($options['workflow'])) 
				unset($options['workflow']);
			
			return $options;
		}
		
		function is_used_by_email_marketing()
		{
			$query = "select id from email_marketing where template_id='$this->id' and deleted=0";
			$result = $this->db->query($query);
			if ($this->db->fetchByAssoc($result)) {
				return true;
			}
			return false;
		}
		
		/**
     * Allows us to save variables of template as they are
     */
		public function cleanBean()
		{
			$this->storedVariables = array();
			$this->body_html = preg_replace_callback('/\{::[^}]+::\}/', array($this, 'storeVariables'), $this->body_html);
			parent::cleanBean();
			$this->body_html = str_replace(array_values($this->storedVariables), array_keys($this->storedVariables), $this->body_html);
		}
		
		/**
     * Replacing variables of templates by their md5 hash
     *
     * @param array $text result of preg_replace_callback
     * @return string md5 hash of result
     */
		protected function storeVariables($text)
		{
			if (isset($this->storedVariables[$text[0]]) == false) {
				$this->storedVariables[$text[0]] = md5($text[0]);
			}
			return $this->storedVariables[$text[0]];
		}
	}
?>



//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description:  Base Form For Notes
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
	
	
	class EmailTemplateFormBase
	{
		
		function __construct()
		{
			
		}
		
		function getFormBody($prefix, $mod = '', $formname = '', $size = '30')
		{
			
			
			global $mod_strings;
			
			$temp_strings = $mod_strings;
			
			if (!empty($mod)) {
				global $current_language;
				$mod_strings = return_module_language($current_language, $mod);
			}
			global $app_strings;
			global $app_list_strings;
			
			$lbl_required_symbol = $app_strings['LBL_REQUIRED_SYMBOL'];
			$lbl_subject = $mod_strings['LBL_NOTE_SUBJECT'];
			$lbl_description = $mod_strings['LBL_NOTE'];
			$default_parent_type = $app_list_strings['record_type_default_key'];
			
			$form = <<<EOF
				<input type="hidden" name="${prefix}record" value="">
				<input type="hidden" name="${prefix}parent_type" value="${default_parent_type}">
				<p>
				<table cellspacing="0" cellpadding="0" border="0">
				<tr>
				    <td scope="row">$lbl_subject <span class="required">$lbl_required_symbol</span></td>
				</tr>
				<tr>
				    <td ><input name='${prefix}name' size='${size}' maxlength='255' type="text" value=""></td>
				</tr>
				<tr>
				    <td scope="row">$lbl_description</td>
				</tr>
				<tr>
				    <td ><textarea name='${prefix}description' cols='${size}' rows='4' ></textarea></td>
				</tr>
				</table></p>
EOF;
			
			$javascript = new javascript();
			$javascript->setFormName($formname);
			$javascript->setSugarBean(new EmailTemplate());
			$javascript->addRequiredFields($prefix);
			$form .= $javascript->getScript();
			$mod_strings = $temp_strings;
			return $form;
		}
		
		function getForm($prefix, $mod = '')
		{
			//TODO (MT): specify type of proc. parameters
			if (!empty($mod)) {
				global $current_language;
				$mod_strings = return_module_language($current_language, $mod);
			} else 
				global $mod_strings;
			global $app_strings;
			global $app_list_strings;
			
			$lbl_save_button_title = $app_strings['LBL_SAVE_BUTTON_TITLE'];
			$lbl_save_button_key = $app_strings['LBL_SAVE_BUTTON_KEY'];
			$lbl_save_button_label = $app_strings['LBL_SAVE_BUTTON_LABEL'];
			
			
			$the_form = get_left_form_header($mod_strings['LBL_NEW_FORM_TITLE']);
			$the_form .= <<<EOQ

				<form name="${prefix}EmailTemplateSave" onSubmit="return check_form('${prefix}EmailTemplateSave')" method="POST" action="index.php">
					<input type="hidden" name="${prefix}module" value="EmailTemplates">
					<input type="hidden" name="${prefix}action" value="Save">
EOQ;
			$the_form .= $this->getFormBody($prefix, $mod, "${prefix}EmailTemplateSave", "20");
			$the_form .= <<<EOQ
				<p><input title="$lbl_save_button_title" accessKey="$lbl_save_button_key" class="button" type="submit" name="button" value="  $lbl_save_button_label  " ></p>
				</form>

EOQ;
			
			$the_form .= get_left_form_footer();
			$the_form .= get_validate_record_js();
			
			
			return $the_form;
		}
		
		
		function handleSave($prefix, $redirect = true, $useRequired = false)
		{
			//TODO (MT): specify type of proc. parameters
			require_once ('include/formbase.php');
			require_once ('include/upload_file.php');
			global $upload_maxsize;
			global $mod_strings;
			global $sugar_config;
			
			$focus = new EmailTemplate();

			if ($useRequired && !checkRequired($prefix, array_keys($focus->required_fields))) {
				//TODO (MT EXTEND Code): retrieve and use the TENANT_ID
				return null;
			}
			$focus = populateFromPost($prefix, $focus);
			//process the text only flag
			if (isset($_POST['text_only']) && ($_POST['text_only'] == '1')) {
				$focus->text_only = 1;
			} else {
				$focus->text_only = 0;
			}
			if (!$focus->ACLAccess('Save')) {
				ACLController::displayNoAccess(true);
				sugar_cleanup(true);
			}
			if (!isset($_REQUEST['published'])) 
				$focus->published = 'off';
			
			$preProcessedImages = array();
			$emailTemplateBodyHtml = from_html($focus->body_html);
			if (strpos($emailTemplateBodyHtml, '"cache/images/')) {
				$matches = array();
				preg_match_all('#<img[^>]*[\s]+src[^=]*=[\s]*["\']cache/images/(.+?)["\']#si', $emailTemplateBodyHtml, $matches);
				foreach ($matches[1] as $match) {
					$filename = urldecode($match);
					if ($filename != pathinfo($filename, PATHINFO_BASENAME)) {
						// don't allow paths there
						$emailTemplateBodyHtml = str_replace("cache/images/$match", "", $emailTemplateBodyHtml);
						continue;
					}
					$file_location = sugar_cached("images/{$filename}");
					$mime_type = pathinfo($filename, PATHINFO_EXTENSION);
					
					if (file_exists($file_location)) {
						$id = create_guid();
						$newFileLocation = "upload://$id";
						if (!copy($file_location, $newFileLocation)) {
							$GLOBALS['log']->debug("EMAIL Template could not copy attachment to $newFileLocation");
						} else {
							$secureLink = "index.php?entryPoint=download&type=Notes&id={$id}";
							$emailTemplateBodyHtml = str_replace("cache/images/$match", $secureLink, $emailTemplateBodyHtml);
							unlink($file_location);
							$preProcessedImages[$filename] = $id;
						}
					}
					// if
				}
				// foreach
			}
			// if
			if (isset($GLOBALS['check_notify'])) {
				$check_notify = $GLOBALS['check_notify'];
			} else {
				$check_notify = FALSE;
			}
			$focus->body_html = $emailTemplateBodyHtml;
			$return_id = $focus->save($check_notify);
			///////////////////////////////////////////////////////////////////////////////
			////	ATTACHMENT HANDLING
			
			///////////////////////////////////////////////////////////////////////////
			////	ADDING NEW ATTACHMENTS
			
			$max_files_upload = count($_FILES);
			
			if (!empty($focus->id)) {
				$note = new Note();
				$where = "notes.parent_id='{$focus->id}'";
				if (!empty($_REQUEST['old_id'])) {
					// to support duplication of email templates
					$where .= " OR notes.parent_id='" . $_REQUEST['old_id'] . "'";
				}
				$notes_list = $note->get_full_list("", $where, true);
			}
			
			if (!isset($notes_list)) {
				$notes_list = array();
			}
			
			if (!is_array($focus->attachments)) {
				// PHP5 does not auto-create arrays(). Need to initialize it here.
				$focus->attachments = array();
			}
			$focus->attachments = array_merge($focus->attachments, $notes_list);
			
			
			
			//for($i = 0; $i < $max_files_upload; $i++) {
			
			foreach ($_FILES as $key => $file) {
				$note = new Note();
				
				//Images are presaved above so we need to prevent duplicate files from being created.
				if (isset($preProcessedImages[$file['name']])) {
					$oldId = $preProcessedImages[$file['name']];
					$note->id = $oldId;
					$note->new_with_id = TRUE;
					$GLOBALS['log']->debug("Image {$file['name']} has already been processed.");
				}
				
				$i = preg_replace("/email_attachment(.+)/", '$1', $key);
				$upload_file = new UploadFile($key);
				
				if (isset($_FILES[$key]) && $upload_file->confirm_upload() && preg_match("/^email_attachment/", $key)) {
					$note->filename = $upload_file->get_stored_file_name();
					$note->file = $upload_file;
					$note->name = $mod_strings['LBL_EMAIL_ATTACHMENT'] . ': ' . $note->file->original_file_name;
					if (isset($_REQUEST['embedded' . $i]) && !empty($_REQUEST['embedded' . $i])) {
						if ($_REQUEST['embedded' . $i] == 'true') {
							$note->embed_flag = true;
						} else {
							$note->embed_flag = false;
						}
					}
					array_push($focus->attachments, $note);
				}
				
			}
			
			$focus->saved_attachments = array();
			foreach ($focus->attachments as $note) {
				if (!empty($note->id) && $note->new_with_id === FALSE) {
					if (empty($_REQUEST['old_id'])) 
						array_push($focus->saved_attachments, $note); // to support duplication of email templates
					 else {
						// we're duplicating a template with attachments
						// dupe the file, create a new note, assign the note to the new template
						$newNote = new Note();
						$newNote->retrieve($note->id);
						$newNote->id = create_guid();
						$newNote->parent_id = $focus->id;
						$newNote->new_with_id = true;
						$newNote->date_modified = '';
						$newNote->date_entered = '';
						$newNoteId = $newNote->save();
						
						UploadFile::duplicate_file($note->id, $newNoteId, $note->filename);
					}
					continue;
				}
				$note->parent_id = $focus->id;
				$note->parent_type = 'Emails';
				$note->file_mime_type = $note->file->mime_type;
				$note_id = $note->save();
				array_push($focus->saved_attachments, $note);
				$note->id = $note_id;
				
				if ($note->new_with_id === FALSE) 
					$note->file->final_move($note->id);
				 else 
					$GLOBALS['log']->debug("Not performing final move for note id {$note->id} as it has already been processed");
			}
			
			////	END NEW ATTACHMENTS
			///////////////////////////////////////////////////////////////////////////
			
			///////////////////////////////////////////////////////////////////////////
			////	ATTACHMENTS FROM DOCUMENTS
			$count = '';
			//_pp($_REQUEST);
			//_ppd(count($_REQUEST['document']));
			if (!empty($_REQUEST['document'])) {
				$count = count($_REQUEST['document']);
			} else {
				$count = 10;
			}
			
			for($i = 0; $i < $count; $i++) {
				if (isset($_REQUEST['documentId' . $i]) && !empty($_REQUEST['documentId' . $i])) {
					$doc = new Document();
					$docRev = new DocumentRevision();
					$docNote = new Note();
					
					$doc->retrieve($_REQUEST['documentId' . $i]);
					$docRev->retrieve($doc->document_revision_id);
					
					array_push($focus->saved_attachments, $docRev);
					
					$docNote->name = $doc->document_name;
					$docNote->filename = $docRev->filename;
					$docNote->description = $doc->description;
					$docNote->parent_id = $focus->id;
					$docNote->parent_type = 'Emails';
					$docNote->file_mime_type = $docRev->file_mime_type;
					$docId = $docNote = $docNote->save();
					
					UploadFile::duplicate_file($docRev->id, $docId, $docRev->filename);
				}
				
			}
			
			////	END ATTACHMENTS FROM DOCUMENTS
			///////////////////////////////////////////////////////////////////////////
			
			///////////////////////////////////////////////////////////////////////////
			////	REMOVE ATTACHMENTS
			
			if (isset($_REQUEST['remove_attachment']) && !empty($_REQUEST['remove_attachment'])) {
				foreach ($_REQUEST['remove_attachment'] as $noteId) {
					$q = 'UPDATE notes SET deleted = 1 WHERE id = \'' . $noteId . '\'';
					$focus->db->query($q);
				}
				
			}
			
			////	END REMOVE ATTACHMENTS
			///////////////////////////////////////////////////////////////////////////
			////	END ATTACHMENT HANDLING
			///////////////////////////////////////////////////////////////////////////////
			
			clear_register_value('select_array', $focus->object_name);
			
			if ($redirect) {
				$GLOBALS['log']->debug("Saved record with id of " . $return_id);
				handleRedirect($return_id, "EmailTemplates");
			} else {
				return $focus;
			}
		}
	}
	
?>


//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description:  TODO To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
	
	include ('modules/Emails/Menu.php');
?>


//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description:  TODO: To be written.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
	require_once ('modules/Documents/Popup_picker.php');
	$popup = new Popup_Picker();
	
	global $theme;
	global $current_mod_strings;
	global $app_strings;
	global $currentModule;
	global $sugar_version, $sugar_config;
	global $app_list_strings;
	
	$current_mod_strings = return_module_language($current_language, 'Documents');
	$output_html = '';
	$where = '';
	
	$where = $popup->_get_where_clause();
	
	
	
	$name = empty($_REQUEST['name']) ? '' : $_REQUEST['name'];
	$document_name = empty($_REQUEST['document_name']) ? '' : $_REQUEST['document_name'];
	$category_id = empty($_REQUEST['category_id']) ? '' : $_REQUEST['category_id'];
	$subcategory_id = empty($_REQUEST['subcategory_id']) ? '' : $_REQUEST['subcategory_id'];
	$template_type = empty($_REQUEST['template_type']) ? '' : $_REQUEST['template_type'];
	$is_template = empty($_REQUEST['is_template']) ? '' : $_REQUEST['is_template'];
	$document_revision_id = empty($_REQUEST['document_revision_id']) ? '' : $_REQUEST['document_revision_id'];
	
	
	$hide_clear_button = empty($_REQUEST['hide_clear_button']) ? false : true;
	$button = "<form action='index.php' method='post' name='form' id='form'>\n";
	if (!$hide_clear_button) {
		$button .= "<input type='button' name='button' class='button' onclick=\"send_back('','');\" title='" . $app_strings['LBL_CLEAR_BUTTON_TITLE'] . "' value='  " . $app_strings['LBL_CLEAR_BUTTON_LABEL'] . "  ' />\n";
	}
	$button .= "<input type='submit' name='button' class='button' onclick=\"window.close();\" title='" . $app_strings['LBL_CANCEL_BUTTON_TITLE'] . "' accesskey='" . $app_strings['LBL_CANCEL_BUTTON_KEY'] . "' value='  " . $app_strings['LBL_CANCEL_BUTTON_LABEL'] . "  ' />\n";
	$button .= "</form>\n";
	
	
	$form = new XTemplate('modules/EmailTemplates/PopupDocumentsCampaignTemplate.html');
	$form->assign('MOD', $current_mod_strings);
	$form->assign('APP', $app_strings);
	$form->assign('THEME', $theme);
	$form->assign('MODULE_NAME', $currentModule);
	$form->assign('NAME', $name);
	$form->assign('DOCUMENT_NAME', $document_name);
	if (isset($_REQUEST['target'])) 
		$form->assign('DOCUMENT_TARGET', $_REQUEST['target']);
	 else 
		$form->assign('DOCUMENT_TARGET', '');
	
	$form->assign('DOCUMENT_REVISION_ID', $document_revision_id);
	
	$form->assign("CATEGORY_OPTIONS", get_select_options_with_id($app_list_strings['document_category_dom'], $category_id));
	$form->assign("SUB_CATEGORY_OPTIONS", get_select_options_with_id($app_list_strings['document_subcategory_dom'], $subcategory_id));
	$form->assign("IS_TEMPLATE_OPTIONS", get_select_options_with_id($app_list_strings['checkbox_dom'], $is_template));
	$form->assign("TEMPLATE_TYPE_OPTIONS", get_select_options_with_id($app_list_strings['document_template_type_dom'], $template_type));
	
	
	ob_start();
	insert_popup_header($theme);
	$output_html .= ob_get_contents();
	ob_end_clean();
	
	$output_html .= get_form_header($current_mod_strings['LBL_SEARCH_FORM_TITLE'], '', false);
	
	$form->parse('main.SearchHeader');
	$output_html .= $form->text('main.SearchHeader');
	
	// Reset the sections that are already in the page so that they do not print again later.
	$form->reset('main.SearchHeader');
	
	// create the listview
	$seed_bean = new Document();
	$ListView = new ListView();
	$ListView->show_export_button = false;
	$ListView->process_for_popups = true;
	$ListView->setXTemplate($form);
	$ListView->setHeaderTitle($current_mod_strings['LBL_LIST_FORM_TITLE']);
	$ListView->setHeaderText($button);
	$ListView->setQuery($where, '', 'document_name', 'DOCUMENT');
	$ListView->setModStrings($current_mod_strings);
	
	ob_start();
	$ListView->processListView($seed_bean, 'main', 'DOCUMENT');
	$output_html .= ob_get_contents();
	ob_end_clean();
	
	$output_html .= insert_popup_footer();
	
	
	echo $output_html;
	
	
	
	
	
	
	
	
	
	
?>



//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/*********************************************************************************

 * Description:  Saves an Account record and then redirects the browser to the
 * defined return URL.
 * Portions created by SugarCRM are Copyright (C) SugarCRM, Inc.
 * All Rights Reserved.
 * Contributor(s): ______________________________________..
 ********************************************************************************/
	
	$focus = new EmailTemplate();
	require_once ('include/formbase.php');
	$focus = populateFromPost('', $focus);
	
	require_once ('modules/EmailTemplates/EmailTemplateFormBase.php');
	$form = new EmailTemplateFormBase();
	sugar_cache_clear('select_array:' . $focus->object_name . 'namebase_module=\'' . $focus->base_module . '\'name');
	if (isset($_REQUEST['inpopupwindow']) and $_REQUEST['inpopupwindow'] == true) {
		$focus = $form->handleSave('', false, false); //do not redirect.
		$body1 = 
			"
		<script type='text/javascript'>
			function refreshTemplates() {
				window.opener.refresh_email_template_list('$focus->id','$focus->name')
				window.close();
			}

			refreshTemplates();
		</script>";
		echo $body1;
	} else {
		$form->handleSave('', true, false);
	}
?>


//<?php
	if (!defined('sugarEntry') || !sugarEntry) 
		die('Not A Valid Entry Point');
	/*********************************************************************************
 * SugarCRM Community Edition is a customer relationship management program developed by
 * SugarCRM, Inc. Copyright (C) 2004-2013 SugarCRM Inc.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 as published by the
 * Free Software Foundation with the addition of the following permission added
 * to Section 15 as permitted in Section 7(a): FOR ANY PART OF THE COVERED WORK
 * IN WHICH THE COPYRIGHT IS OWNED BY SUGARCRM, SUGARCRM DISCLAIMS THE WARRANTY
 * OF NON INFRINGEMENT OF THIRD PARTY RIGHTS.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU Affero General Public License along with
 * this program; if not, see http://www.gnu.org/licenses or write to the Free
 * Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301 USA.
 * 
 * You can contact SugarCRM, Inc. headquarters at 10050 North Wolfe Road,
 * SW2-130, Cupertino, CA 95014, USA. or at email address contact@sugarcrm.com.
 * 
 * The interactive user interfaces in modified source and object code versions
 * of this program must display Appropriate Legal Notices, as required under
 * Section 5 of the GNU Affero General Public License version 3.
 * 
 * In accordance with Section 7(b) of the GNU Affero General Public License version 3,
 * these Appropriate Legal Notices must retain the display of the "Powered by
 * SugarCRM" logo. If the display of the logo is not reasonably feasible for
 * technical reasons, the Appropriate Legal Notices must display the words
 * "Powered by SugarCRM".
 ********************************************************************************/
	
	/**
 * Class for separate storage of Email texts
 */
	class EmailText extends SugarBean
	{
		var $disable_row_level_security = true;
		var $table_name = 'emails_text';
		var $module_name = "EmailText";
		var $module_dir = 'EmailText';
		var $object_name = 'EmailText';
		var $disable_custom_fields = true;
	}
		
	
?>

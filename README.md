# DetectTenantIsolationInterference

Static analysis tool for detecting tenant data isolation interference in multi-tenant PHP code.
Paper title: A Practical Approach for Detecting Multi-Tenancy Data Interference. 
To be published in Science of Computer Programming.

Requires: PHC version 0.3.0


Runs as plugin for PHC compiler.